/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/keyset/search_keyset.hh"
#include "src/util/sql_exec_in_thread.hh"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(TestSearchKeyset)

BOOST_AUTO_TEST_CASE(search)
{
    const std::set<Fred::Registry::Keyset::KeysetItem> searched_items =
            {
                Fred::Registry::Keyset::KeysetItem::keyset_handle
            };
    int idx = -1;
    for (const char* const query : {"Test", "Kontakt", "První", "Testovací"})
    {
        ++idx;
        Fred::Registry::Keyset::SearchKeysetRequest request;
        request.query_values.push_back(query);
        request.limit = 100;
        request.searched_items = searched_items;
        const auto answer = Fred::Registry::Keyset::search_keyset(request);
        BOOST_CHECK(answer.result_count.lower_estimate <= answer.result_count.upper_estimate);
        switch (idx)
        {
            case 0:
                BOOST_CHECK_LE(answer.result_count.lower_estimate, 1);
                BOOST_CHECK_GE(answer.result_count.upper_estimate, 1);
                break;
            case 1:
                BOOST_CHECK_LE(answer.result_count.lower_estimate, 2);
                BOOST_CHECK_GE(answer.result_count.upper_estimate, 2);
                break;
            case 2:
                BOOST_CHECK_LE(answer.result_count.lower_estimate, 1);
                BOOST_CHECK_GE(answer.result_count.upper_estimate, 1);
                break;
            case 3:
                BOOST_CHECK_LE(answer.result_count.lower_estimate, 1);
                BOOST_CHECK_GE(answer.result_count.upper_estimate, 1);
                break;
        }
    }
}

BOOST_AUTO_TEST_CASE(sql_exec_in_thread)
{
    auto solver1 = Fred::Registry::Util::sql_exec_in_thread(
            "SELECT MAX(id) FROM object_registry",
            Fred::Registry::Util::repeatable_read_read_only_transaction());
    auto solver2 = Fred::Registry::Util::sql_exec_in_thread(
            "SELECT MAX(id) FROM keyset",
            Fred::Registry::Util::repeatable_read_read_only_transaction());
    auto solver3 = Fred::Registry::Util::sql_exec_in_thread(
            "SELECT COUNT(*) FROM object_registry",
            Fred::Registry::Util::repeatable_read_read_only_transaction());
    auto solver4 = Fred::Registry::Util::sql_exec_in_thread(
            "SELECT COUNT(*) FROM keyset WHERE $1::BIGINT <= id",
            Database::query_param_list(0),
            Fred::Registry::Util::repeatable_read_read_only_transaction());
    const Fred::Registry::Util::SqlInThreadSolver::Result result1(std::move(solver1));
    const Fred::Registry::Util::SqlInThreadSolver::Result result2(std::move(solver2));
    const Fred::Registry::Util::SqlInThreadSolver::Result result3(std::move(solver3));
    const Fred::Registry::Util::SqlInThreadSolver::Result result4(std::move(solver4));
    const auto max_obr_id = static_cast<std::size_t>(result1.get_result()[0][0]);
    const auto max_keyset_id = static_cast<std::size_t>(result2.get_result()[0][0]);
    const auto obr_count = static_cast<std::size_t>(result3.get_result()[0][0]);
    const auto keyset_count = static_cast<std::size_t>(result4.get_result()[0][0]);
    BOOST_CHECK_LE(max_keyset_id, max_obr_id);
    BOOST_CHECK_LE(obr_count, max_obr_id);
    BOOST_CHECK_LE(keyset_count, max_keyset_id);
    BOOST_CHECK_LE(keyset_count, obr_count);
    bool solver_started = false;
    try
    {
        auto solver = Fred::Registry::Util::sql_exec_in_thread(
                "SELECT MAX(id) FROM obr",
                Fred::Registry::Util::repeatable_read_read_only_transaction());
        Fred::Registry::Util::SqlInThreadSolver::Result result;
        solver_started = true;
        result = std::move(solver);
        BOOST_CHECK(false);
    }
    catch (const Database::ResultFailed&)
    {
        BOOST_CHECK(solver_started);
    }
    catch (...)
    {
        BOOST_CHECK(false);
    }
    Fred::Registry::Util::sql_exec_in_thread(
            "SELECT MAX(id) FROM enum_reason",
            Fred::Registry::Util::repeatable_read_read_only_transaction());
    const Fred::Registry::Util::SqlInThreadSolver::Result result(
            Fred::Registry::Util::sql_exec_in_thread(
                    "SELECT MAX(id) FROM enum_reason",
                    Fred::Registry::Util::repeatable_read_read_only_transaction()));
    Fred::Registry::Util::sql_exec_in_thread(
            "SELECT MAX(id) FROM obr",
            Fred::Registry::Util::repeatable_read_read_only_transaction());
}

BOOST_AUTO_TEST_SUITE_END()//TestKeysetSearch
