/*
 * Copyright (C) 2019-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CFG_HH_8DE523A3E9A1B43DB45514EBBA96D250//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CFG_HH_8DE523A3E9A1B43DB45514EBBA96D250

/**
 * config file example:

#Logging options
[log]
device = file

#Logging into file options
[log.file]
file_name = registry.log
min_severity = trace

#FRED options
#registrar_originator - which registrar does modifying operations
[fred]
registrar_originator = REG-CZNIC

#Database options
[database]
host    = localhost
port    = 11112
user    = fred
dbname  = fred
password = password
 */

#include "src/util/strong_type.hh"

#include "util/log/log.hh"

#include <boost/blank.hpp>
#include <boost/optional.hpp>
#include <boost/program_options.hpp>
#include <boost/variant.hpp>

#include <stdexcept>
#include <string>
#include <utility>

namespace Test {
namespace Setup {
namespace Fixture {
namespace Cfg {

class HelpRequested : public std::exception
{
public:
    explicit HelpRequested(const std::string& msg);
    const char* what() const noexcept override;
private:
    const std::string msg_;
};

struct Exception : std::runtime_error
{
    explicit Exception(const std::string& msg);
};

struct UnknownOption : Exception
{
    explicit UnknownOption(const std::string& msg);
};

struct MissingOption : Exception
{
    explicit MissingOption(const std::string& msg);
};

enum class LogSeverity
{
    emerg,
    alert,
    crit,
    err,
    warning,
    notice,
    info,
    debug,
    trace
};

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(int argc, const char* const* argv);

    boost::optional<std::string> config_file_name;
    struct Database
    {
        boost::optional<std::string> host;
        boost::optional<int> port;
        boost::optional<std::string> user;
        boost::optional<std::string> dbname;
        boost::optional<std::string> password;
    };
    Database database;
    struct Fred
    {
        boost::optional<std::string> registrar_originator;
    } fred;
    struct Console
    {
        LogSeverity min_severity;
    };
    struct LogFile
    {
        std::string file_name;
        LogSeverity min_severity;
    };
    struct SysLog
    {
        int facility;
        LogSeverity min_severity;
    };
    boost::variant<boost::blank, Console, LogFile, SysLog> log;
private:
    Options(int argc, const char* const* argv);
};

const std::string& get_prefix();

template <typename Handle, typename Src>
decltype(auto) make_unique_handle(Src&& value)
{
    return Fred::Registry::Util::make_strong<Handle>(get_prefix() + std::forward<Src>(value));
}

}//namespace Test::Setup::Fixture::Cfg
}//namespace Test::Setup::Fixture
}//namespace Test::Setup
}//namespace Test

#endif//CFG_HH_8DE523A3E9A1B43DB45514EBBA96D250
