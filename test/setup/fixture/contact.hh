/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_HH_A9DA05F476FF258971F5819F007678A0//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_HH_A9DA05F476FF258971F5819F007678A0

#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/info_contact.hh"

#include "src/util/strong_type.hh"

#include "test/setup/fixture/registrar.hh"

#include <boost/optional.hpp>

#include <string>
#include <utility>
#include <vector>

namespace Test {

class Contact
{
private:
    template <typename T, typename N>
    using PrintableStrongType = Fred::Registry::Util::StrongType<T, N, Fred::Registry::Util::Skill::Printable>;
public:
    using Handle = PrintableStrongType<std::string, struct ContactHandle_>;
    static Handle make_handle(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<Handle>(raw_value);
    }
    using Name = PrintableStrongType<std::string, struct ContactName_>;
    static Name make_name(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<Name>(raw_value);
    }
    using Organization = PrintableStrongType<std::string, struct ContactOrganization_>;
    static Organization make_organization(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<Organization>(raw_value);
    }
    using Authinfo = PrintableStrongType<std::string, struct ContactAuthinfo_>;
    static Authinfo make_authinfo(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<Authinfo>(raw_value);
    }
    using PlaceAddress = PrintableStrongType<LibFred::Contact::PlaceAddress, struct ContactPlaceAddress_>;
    static PlaceAddress make_place_address(const LibFred::Contact::PlaceAddress& raw_value)
    {
        return Fred::Registry::Util::make_strong<PlaceAddress>(raw_value);
    }
    using Telephone = PrintableStrongType<std::string, struct ContactTelephone_>;
    static Telephone make_telephone(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<Telephone>(raw_value);
    }
    using Fax = PrintableStrongType<std::string, struct ContactFax_>;
    static Fax make_fax(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<Fax>(raw_value);
    }
    using Email = PrintableStrongType<std::vector<std::string>, struct ContactEmail_>;
    static Email make_email(const std::vector<std::string>& raw_value)
    {
        return Fred::Registry::Util::make_strong<Email>(raw_value);
    }
    using NotifyEmail = PrintableStrongType<std::vector<std::string>, struct ContactNotifyEmail_>;
    static NotifyEmail make_notify_email(const std::vector<std::string>& raw_value)
    {
        return Fred::Registry::Util::make_strong<NotifyEmail>(raw_value);
    }
    using VatIdentificationNumber = PrintableStrongType<std::string, struct ContactVatIdentificationNumber_>;
    static VatIdentificationNumber make_vat_identification_number(const std::string& raw_value)
    {
        return Fred::Registry::Util::make_strong<VatIdentificationNumber>(raw_value);
    }
    using PersonalIdUnion = PrintableStrongType<LibFred::PersonalIdUnion, struct ContactPersonalIdUnion_>;
    static PersonalIdUnion make_personal_id(const LibFred::PersonalIdUnion& raw_value)
    {
        return Fred::Registry::Util::make_strong<PersonalIdUnion>(raw_value);
    }
    using ContactAddressList = PrintableStrongType<LibFred::ContactAddressList, struct ContactContactAddressList_>;
    static ContactAddressList make_contact_address_list(const LibFred::ContactAddressList& raw_value)
    {
        return Fred::Registry::Util::make_strong<ContactAddressList>(raw_value);
    }
    using SendWarningLetter = PrintableStrongType<boost::optional<bool>, struct ContactSendWarningLetter_>;
    static SendWarningLetter make_send_warning_letter(const boost::optional<bool>& raw_value)
    {
        return Fred::Registry::Util::make_strong<SendWarningLetter>(raw_value);
    }
    template <typename FlagName>
    using DiscloseFlagType = PrintableStrongType<bool, FlagName>;
    struct DiscloseFlag
    {
        using Name = DiscloseFlagType<struct ContactDiscloseName_>;
        using Organization = DiscloseFlagType<struct ContactDiscloseOrganization_>;
        using Address = DiscloseFlagType<struct ContactDiscloseAddress_>;
        using Telephone = DiscloseFlagType<struct ContactDiscloseTelephone_>;
        using Fax = DiscloseFlagType<struct ContactDiscloseFax_>;
        using Email = DiscloseFlagType<struct ContactDiscloseEmail_>;
        using NotifyEmail = DiscloseFlagType<struct ContactDiscloseNotifyEmail_>;
        using VatIdentificationNumber = DiscloseFlagType<struct ContactDiscloseVatIdentificationNumber_>;
        using Ident = DiscloseFlagType<struct ContactDiscloseIdent_>;
    };
    template <bool default_value>
    class MakeDiscloseFlag
    {
    public:
        static constexpr decltype(auto) name(bool set_default = true)
        {
            return make<DiscloseFlag::Name>(set_default);
        }
        static constexpr decltype(auto) organization(bool set_default = true)
        {
            return make<DiscloseFlag::Organization>(set_default);
        }
        static constexpr decltype(auto) address(bool set_default = true)
        {
            return make<DiscloseFlag::Address>(set_default);
        }
        static constexpr decltype(auto) telephone(bool set_default = true)
        {
            return make<DiscloseFlag::Telephone>(set_default);
        }
        static constexpr decltype(auto) fax(bool set_default = true)
        {
            return make<DiscloseFlag::Fax>(set_default);
        }
        static constexpr decltype(auto) email(bool set_default = true)
        {
            return make<DiscloseFlag::Email>(set_default);
        }
        static constexpr decltype(auto) notify_email(bool set_default = true)
        {
            return make<DiscloseFlag::NotifyEmail>(set_default);
        }
        static constexpr decltype(auto) vat_identification_number(bool set_default = true)
        {
            return make<DiscloseFlag::VatIdentificationNumber>(set_default);
        }
        static constexpr decltype(auto) ident(bool set_default = true)
        {
            return make<DiscloseFlag::Ident>(set_default);
        }
    private:
        template <typename Flag>
        static constexpr Flag make(bool set_default) noexcept
        {
            return Fred::Registry::Util::template make_strong<Flag>(set_default ? default_value : !default_value);
        }
    };
    using Disclose = MakeDiscloseFlag<true>;
    using Hide = MakeDiscloseFlag<false>;
    const LibFred::InfoContactData& get_data() const;
    template <typename ...T>
    static Contact create(
            LibFred::OperationContext& ctx,
            const Handle& handle,
            const Registrar::Handle& sponsoring_registrar,
            T&& ...values)
    {
        LibFred::CreateContact create_contact_op(get_raw_value_from(handle), get_raw_value_from(sponsoring_registrar));
        set_bunch_of_attributes(create_contact_op, std::forward<T>(values)...);
        const auto create_contact_result = create_contact_op.exec(ctx);
        return Contact{
                LibFred::InfoContactById(create_contact_result.create_object_result.object_id).exec(ctx).info_contact_data};
    }
private:
    explicit Contact(const LibFred::InfoContactData&);
    template <typename T>
    static void set_attribute(LibFred::CreateContact&, const T&);
    template <typename ...Ts>
    static void set_bunch_of_attributes(const LibFred::CreateContact&, const Ts&...)
    {
        static_assert(sizeof...(Ts) == 0, "must be used with an empty bunch");
    }
    template <typename T, typename ...Ts>
    static void set_bunch_of_attributes(LibFred::CreateContact& create_contact_op, const T& value, const Ts& ...bunch)
    {
        set_attribute(create_contact_op, value);
        set_bunch_of_attributes(create_contact_op, bunch...);
    }
    LibFred::InfoContactData data_;
};

}//namespace Test

#endif//CONTACT_HH_A9DA05F476FF258971F5819F007678A0
