/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixture/contact.hh"

#include <stdexcept>

namespace Test {

Contact::Contact(const LibFred::InfoContactData& src)
    : data_{src}
{
}

const LibFred::InfoContactData& Contact::get_data() const
{
    return data_;
}

template<>
void Contact::set_attribute<Contact::Name>(
        LibFred::CreateContact& create_contact_op,
        const Name& name)
{
    create_contact_op.set_name(get_raw_value_from(name));
}

template<>
void Contact::set_attribute<Contact::Organization>(
        LibFred::CreateContact& create_contact_op,
        const Organization& organization)
{
    create_contact_op.set_organization(get_raw_value_from(organization));
}

template<>
void Contact::set_attribute<Contact::Authinfo>(
        LibFred::CreateContact& create_contact_op,
        const Authinfo& authinfo)
{
    create_contact_op.set_authinfo(get_raw_value_from(authinfo));
}

template<>
void Contact::set_attribute<Contact::PlaceAddress>(
        LibFred::CreateContact& create_contact_op,
        const PlaceAddress& place)
{
    create_contact_op.set_place(get_raw_value_from(place));
}

template<>
void Contact::set_attribute<Contact::Telephone>(
        LibFred::CreateContact& create_contact_op,
        const Telephone& telephone)
{
    create_contact_op.set_telephone(get_raw_value_from(telephone));
}

template<>
void Contact::set_attribute<Contact::Fax>(
        LibFred::CreateContact& create_contact_op,
        const Fax& fax)
{
    create_contact_op.set_fax(get_raw_value_from(fax));
}

namespace {

std::string join_emails(const std::vector<std::string>& emails)
{
    std::string sum;
    for (const auto& one_email : emails)
    {
        if (sum.empty())
        {
            sum = one_email;
        }
        else
        {
            sum += "," + one_email;
        }
    }
    return sum;
}

}

template<>
void Contact::set_attribute<Contact::Email>(
        LibFred::CreateContact& create_contact_op,
        const Email& email)
{
    create_contact_op.set_email(join_emails(get_raw_value_from(email)));
}

template<>
void Contact::set_attribute<Contact::NotifyEmail>(
        LibFred::CreateContact& create_contact_op,
        const NotifyEmail& notify_email)
{
    create_contact_op.set_notifyemail(join_emails(get_raw_value_from(notify_email)));
}

template<>
void Contact::set_attribute<Contact::VatIdentificationNumber>(
        LibFred::CreateContact& create_contact_op,
        const VatIdentificationNumber& vat_identification_number)
{
    create_contact_op.set_vat(get_raw_value_from(vat_identification_number));
}

template<>
void Contact::set_attribute<Contact::PersonalIdUnion>(
        LibFred::CreateContact& create_contact_op,
        const PersonalIdUnion& personal_id)
{
    if (!get_raw_value_from(personal_id).get_type().empty() ||
        !get_raw_value_from(personal_id).get().empty())
    {
        create_contact_op.set_ssntype(get_raw_value_from(personal_id).get_type());
        create_contact_op.set_ssn(get_raw_value_from(personal_id).get());
    }
}

template<>
void Contact::set_attribute<Contact::ContactAddressList>(
        LibFred::CreateContact& create_contact_op,
        const ContactAddressList& addresses)
{
    create_contact_op.set_addresses(get_raw_value_from(addresses));
}

template<>
void Contact::set_attribute<Contact::SendWarningLetter>(
        LibFred::CreateContact& create_contact_op,
        const SendWarningLetter& preference)
{
    if (get_raw_value_from(preference) == boost::none)
    {
        create_contact_op.set_domain_expiration_warning_letter_enabled(Nullable<bool>());
    }
    else
    {
        create_contact_op.set_domain_expiration_warning_letter_enabled(*get_raw_value_from(preference));
    }
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Name>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Name& flag)
{
    create_contact_op.set_disclosename(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Organization>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Organization& flag)
{
    create_contact_op.set_discloseorganization(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Address>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Address& flag)
{
    create_contact_op.set_discloseaddress(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Telephone>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Telephone& flag)
{
    create_contact_op.set_disclosetelephone(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Fax>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Fax& flag)
{
    create_contact_op.set_disclosefax(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Email>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Email& flag)
{
    create_contact_op.set_discloseemail(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::NotifyEmail>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::NotifyEmail& flag)
{
    create_contact_op.set_disclosenotifyemail(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::VatIdentificationNumber>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::VatIdentificationNumber& flag)
{
    create_contact_op.set_disclosevat(get_raw_value_from(flag));
}

template<>
void Contact::set_attribute<Contact::DiscloseFlag::Ident>(
        LibFred::CreateContact& create_contact_op,
        const DiscloseFlag::Ident& flag)
{
    create_contact_op.set_discloseident(get_raw_value_from(flag));
}

}//namespace Test
