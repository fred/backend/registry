/*
 * Copyright (C) 2019-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixture/registrar.hh"

#include "libfred/registrar/create_registrar.hh"

#include <stdexcept>

namespace Test {

namespace {

template <typename T>
Registrar::Handle make_registrar_handle(T&& raw_value)
{
    return Fred::Registry::Util::make_strong<Registrar::Handle>(std::forward<T>(raw_value));
}

template <Registrar::Type>
auto create_registrar(LibFred::OperationContext&);

template <>
auto create_registrar<Registrar::system>(LibFred::OperationContext& ctx)
{
    const auto new_sys_registrar_handle = make_registrar_handle(std::string("SYSREG-FOR-UNIT-TEST"));
    LibFred::CreateRegistrar{
            get_raw_value_from(new_sys_registrar_handle),
            "Name",
            "Organization",
            {"Street"},
            "City",
            "PostalCode",
            "Telephone",
            "Email",
            "Url",
            "Dic"}.set_system(true).exec(ctx);
    return new_sys_registrar_handle;
}

template <>
auto create_registrar<Registrar::non_system>(LibFred::OperationContext& ctx)
{
    const auto new_registrar_handle = make_registrar_handle(std::string("NON-SYSREG-FOR-UNIT-TEST"));
    LibFred::CreateRegistrar(
            get_raw_value_from(new_registrar_handle),
            "Name",
            "Organization",
            {"Street"},
            "City",
            "PostalCode",
            "Telephone",
            "Email",
            "Url",
            "Dic").set_system(false).exec(ctx);
    return new_registrar_handle;
}

template <Registrar::Type type>
const Registrar* singleton_ptr = nullptr;

template <Registrar::Type type>
constexpr const char* const sql_registrar_handle = nullptr;

template <>
constexpr const char* const sql_registrar_handle<Registrar::system> =
        "SELECT handle FROM registrar WHERE system ORDER BY id LIMIT 1";
template <>
constexpr const char* const sql_registrar_handle<Registrar::non_system> =
        "SELECT handle FROM registrar WHERE NOT system ORDER BY id LIMIT 1";

}//namespace Test::{anonymous}

const Registrar::Handle& Registrar::get_handle() const
{
    return handle_;
}

template <Registrar::Type type>
const Registrar& Registrar::get()
{
    if (singleton_ptr<type> != nullptr)
    {
        return *singleton_ptr<type>;
    }
    throw std::runtime_error("singleton must be initialized before the first usage");
}

template <Registrar::Type type>
const Registrar& Registrar::init(LibFred::OperationContext& ctx)
{
    if (singleton_ptr<type> != nullptr)
    {
        throw std::runtime_error("singleton can be initialized just once");
    }
    static Registrar singleton;
    const auto dbres = ctx.get_conn().exec(sql_registrar_handle<type>);
    if (dbres.size() == 1)
    {
        singleton.handle_ = make_registrar_handle(dbres[0][0].template as<std::string>());
    }
    else if (dbres.size() < 1)
    {
        singleton.handle_ = create_registrar<type>(ctx);
    }
    else
    {
        throw std::runtime_error("too many rows");
    }
    singleton_ptr<type> = &singleton;
    return *singleton_ptr<type>;
}

template const Registrar& Registrar::get<Registrar::system>();
template const Registrar& Registrar::init<Registrar::system>(LibFred::OperationContext&);

template const Registrar& Registrar::get<Registrar::non_system>();
template const Registrar& Registrar::init<Registrar::non_system>(LibFred::OperationContext&);

}//namespace Test
