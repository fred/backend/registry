/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef REGISTRAR_HH_23FDFAE889561AC59E06307617BA007A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_HH_23FDFAE889561AC59E06307617BA007A

#include "libfred/opcontext.hh"

#include "src/util/strong_type.hh"

#include <string>

namespace Test {

class Registrar
{
public:
    template <typename T, typename N>
    using PrintableStrongType = Fred::Registry::Util::StrongType<T, N, Fred::Registry::Util::Skill::Printable>;
    using Handle = PrintableStrongType<std::string, struct RegistrarHandle_>;
    const Handle& get_handle() const;
    enum Type
    {
        system,
        non_system
    };
    template <Type type = non_system>
    static const Registrar& get();
    template <Type type = non_system>
    static const Registrar& init(LibFred::OperationContext&);
private:
    Handle handle_;
};

}//namespace Test

#endif//REGISTRAR_HH_23FDFAE889561AC59E06307617BA007A
