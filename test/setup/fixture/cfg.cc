/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixture/cfg.hh"

#include "util/random/random.hh"

#include <boost/test/unit_test.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "test-registry.conf"
#endif

namespace Test {
namespace Setup {
namespace Fixture {
namespace Cfg {

namespace {

LogSeverity make_severity(const std::string& str)
{
    if (str == "emerg")
    {
        return LogSeverity::emerg;
    }
    if (str == "alert")
    {
        return LogSeverity::alert;
    }
    if (str == "crit")
    {
        return LogSeverity::crit;
    }
    if (str == "err")
    {
        return LogSeverity::err;
    }
    if (str == "warning")
    {
        return LogSeverity::warning;
    }
    if (str == "notice")
    {
        return LogSeverity::notice;
    }
    if (str == "info")
    {
        return LogSeverity::info;
    }
    if (str == "debug")
    {
        return LogSeverity::debug;
    }
    if (str == "trace")
    {
        return LogSeverity::trace;
    }
    throw Exception("not a valid severity \"" + str + "\"");
}

void required_one_of(const boost::program_options::variables_map& variables_map, const char* variable)
{
    if (variables_map.count(variable) == 0)
    {
        throw MissingOption("missing one of options: '" + std::string(variable) + "'");
    }
}

template <typename ...Ts>
void required_one_of(
        const boost::program_options::variables_map& variables_map,
        const char* first_variable,
        const Ts* ...other_variables)
{
    if (0 < variables_map.count(first_variable))
    {
        return;
    }
    try
    {
        required_one_of(variables_map, other_variables...);
    }
    catch (const MissingOption& e)
    {
        throw MissingOption(e.what() + std::string(", '") + first_variable + "'");
    }
}

constexpr int invalid_argc = -1;
constexpr const char* const invalid_argv[0] = { };

}//namespace Test::Setup::Fixture::Cfg::{anonymous}

const Options& Options::get()
{
    return init(invalid_argc, invalid_argv);
}

const Options& Options::init(int argc, const char* const* argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != invalid_argc) || (argv != invalid_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            throw std::runtime_error("First call of Test::Setup::Fixture::Cfg::Options::init must contain valid arguments");
        }
        static const Options singleton(argc, argv);
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        throw std::runtime_error("Only first call of Test::Setup::Fixture::Cfg::Options::init can contain valid arguments");
    }
    return *singleton_ptr;
}

Options::Options(int argc, const char* const* argv)
{
    boost::program_options::options_description generic_options("Generic options");
    std::string opt_config_file_name;
    generic_options.add_options()
        ("help,h", "produce help message")
        ("config,c",
#ifdef DEFAULT_CONFIG_FILE
         boost::program_options::value<std::string>(&opt_config_file_name)->default_value(std::string(DEFAULT_CONFIG_FILE)),
#else
         boost::program_options::value<std::string>(&opt_config_file_name),
#endif
         "name of a file of a configuration.");

    boost::program_options::options_description database_options("Database options");
    std::string host;
    int port;
    std::string user;
    std::string dbname;
    std::string password;
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>(&host), "name of host to connect to")
        ("database.port", boost::program_options::value<int>(&port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>(&user), "PostgreSQL user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>(&dbname), "the database name")
        ("database.password", boost::program_options::value<std::string>(&password), "password used for password authentication.");

    boost::program_options::options_description log_options("Logging options");
    std::string device;
    std::string default_min_severity;
    log_options.add_options()
        ("log.device", boost::program_options::value<std::string>(&device), "where to log (console/file/syslog)")
        ("log.min_severity", boost::program_options::value<std::string>(&default_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_console_options("Logging on console options");
    std::string log_console_min_severity;
    log_console_options.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>(&log_console_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_file_options("Logging into file options");
    std::string log_file_name;
    std::string log_file_min_severity;
    log_file_options.add_options()
        ("log.file.file_name",
         boost::program_options::value<std::string>(&log_file_name),
         "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>(&log_file_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_syslog_options("Logging into syslog options");
    int facility;
    std::string log_syslog_min_severity;
    log_syslog_options.add_options()
        ("log.syslog.facility",
         boost::program_options::value<int>(&facility)->default_value(2),
         "what facility to log with (in range 0..7)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>(&log_syslog_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description fred_options("FRED options");
    std::string registrar_originator;
    fred_options.add_options()
        ("fred.registrar_originator", boost::program_options::value<std::string>(&registrar_originator), "which registrar does modifying operations");

    boost::program_options::options_description command_line_options("test-fred-registry-services options");
    command_line_options
        .add(generic_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(fred_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(fred_options);

    boost::program_options::variables_map variables_map;
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(argc, argv)
                    .options(command_line_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        throw UnknownOption(out.str());
    }
    boost::program_options::notify(variables_map);

    if (0 < variables_map.count("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        throw HelpRequested(out.str());
    }
    const bool config_file_name_presents = 0 < variables_map.count("config");
    if (config_file_name_presents)
    {
        this->config_file_name = opt_config_file_name;
        std::ifstream config_file(opt_config_file_name);
        if (!config_file)
        {
            throw Exception("can not open config file \"" + opt_config_file_name + "\"");
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            throw UnknownOption(out.str());
        }
        boost::program_options::notify(variables_map);
    }

    if (0 < variables_map.count("database.host"))
    {
        this->database.host = host;
    }
    if (0 < variables_map.count("database.port"))
    {
        this->database.port = port;
    }
    if (0 < variables_map.count("database.user"))
    {
        this->database.user = user;
    }
    if (0 < variables_map.count("database.dbname"))
    {
        this->database.dbname = dbname;
    }
    if (0 < variables_map.count("database.password"))
    {
        this->database.password = password;
    }

    if (0 < variables_map.count("log.device"))
    {
        boost::optional<LogSeverity> log_min_severity = LogSeverity::emerg;
        if (0 < variables_map.count("log.min_severity"))
        {
            log_min_severity = make_severity(default_min_severity);
        }
        else
        {
            log_min_severity = boost::none;
        }
        if (device == "console")
        {
            required_one_of(variables_map, "log.min_severity", "log.console.min_severity");
            Console console;
            if (0 < variables_map.count("log.console.min_severity"))
            {
                console.min_severity = make_severity(log_console_min_severity);
            }
            else
            {
                console.min_severity = *log_min_severity;
            }
            this->log = console;
        }
        else if (device == "file")
        {
            required_one_of(variables_map, "log.file.file_name");
            required_one_of(variables_map, "log.min_severity", "log.file.min_severity");
            LogFile log_file;
            log_file.file_name = log_file_name;
            if (0 < variables_map.count("log.file.min_severity"))
            {
                log_file.min_severity = make_severity(log_file_min_severity);
            }
            else
            {
                log_file.min_severity = *log_min_severity;
            }
            this->log = log_file;
        }
        else if (device == "syslog")
        {
            required_one_of(variables_map, "log.syslog.facility");
            required_one_of(variables_map, "log.min_severity", "log.syslog.min_severity");
            if ((facility < 0) || (7 < facility))
            {
                throw Exception("option 'log.syslog.facility' out of range [0, 7]");
            }
            SysLog sys_log;
            sys_log.facility = facility;
            if (0 < variables_map.count("log.syslog.min_severity"))
            {
                sys_log.min_severity = make_severity(log_syslog_min_severity);
            }
            else
            {
                sys_log.min_severity = *log_min_severity;
            }
            this->log = sys_log;
        }
        else
        {
            throw Exception("Invalid value of log.device");
        }
    }

    if (0 < variables_map.count("fred.registrar_originator"))
    {
        this->fred.registrar_originator = registrar_originator;
    }
    else
    {
        this->fred.registrar_originator = boost::none;
    }
}

HelpRequested::HelpRequested(const std::string& msg)
    : msg_(msg)
{ }

const char* HelpRequested::what() const noexcept
{
    return msg_.c_str();
}

Exception::Exception(const std::string& msg)
    : std::runtime_error(msg)
{ }

UnknownOption::UnknownOption(const std::string& msg)
    : Exception(msg)
{ }

MissingOption::MissingOption(const std::string& msg)
    : Exception(msg)
{ }

const std::string& get_prefix()
{
    static const auto prefix = []()
    {
        auto prefix = Random::Generator{}.get_seq("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 10) + "-";
        BOOST_TEST_MESSAGE("prefix: " << prefix);
        return prefix;
    }();
    return prefix;
}

}//namespace Test::Setup::Fixture::Cfg
}//namespace Test::Setup::Fixture
}//namespace Test::Setup
}//namespace Test
