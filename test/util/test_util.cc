/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/into.hh"
#include "src/util/rfc3339_time.hh"

#include <boost/test/unit_test.hpp>

#include <algorithm>
#include <type_traits>

static_assert(std::is_same<std::chrono::system_clock::duration, std::chrono::nanoseconds>::value);

namespace {

auto make_time_point(std::chrono::nanoseconds fraction)
{
    static const auto t_sec = std::chrono::system_clock::from_time_t(1652451931); // 2022-05-13 14:25:31
    return t_sec + fraction;
}

}//namespace {anonymous}

BOOST_AUTO_TEST_SUITE(TestUtil)

BOOST_AUTO_TEST_CASE(time_point_nsec_into_string)
{
    const auto t = make_time_point(std::chrono::nanoseconds{123'456'789});
    const auto str_time_point = Fred::Registry::Util::Into<std::string>::from(t);
    BOOST_CHECK_EQUAL(str_time_point, "2022-05-13 14:25:31.123456789");
}

BOOST_AUTO_TEST_CASE(time_point_usec_cropped_into_string)
{
    const auto t = make_time_point(std::chrono::nanoseconds{123'456'789});
    const auto t_usec = std::chrono::time_point_cast<std::chrono::microseconds>(t);
    const auto str_time_point = Fred::Registry::Util::Into<std::string>::from(t_usec);
    BOOST_CHECK_EQUAL(str_time_point, "2022-05-13 14:25:31.123456");
}

BOOST_AUTO_TEST_CASE(time_point_usec_zero_into_string)
{
    const auto t = make_time_point(std::chrono::nanoseconds{123'000'000});
    const auto t_usec = std::chrono::time_point_cast<std::chrono::microseconds>(t);
    const auto str_time_point = Fred::Registry::Util::Into<std::string>::from(t_usec);
    BOOST_CHECK_EQUAL(str_time_point, "2022-05-13 14:25:31.123000");
}

BOOST_AUTO_TEST_CASE(seconds_from_rfc3339_formated_string_ok)
{
    static const auto t0 = std::chrono::time_point_cast<std::chrono::seconds>(
            make_time_point(std::chrono::nanoseconds{100'000'000}));
    (void)t0; // prevent warning unused variable
    static constexpr auto ok_strings = {
        "2022-05-13T16:25:31.1+02:00",
        "2022-05-13T16:25:31.987654321+02:00",
        "2022-05-13T16:25:31+02:00",
        "2022-05-13T16:55:31+02:30",
        "2022-05-13T12:25:31-02:00",
        "2022-05-13T14:25:31+00:00",
        "2022-05-13T14:25:31Z",
        "2022-05-13t14:25:31Z",
        "2022-05-13 14:25:31Z",
        "2022-05-13T14:25:31z",
        "2022-05-13t14:25:31z",
        "2022-05-13 14:25:31z",
        "2022-05-13T15:25:31+01:00",
        "2022-05-14T02:25:31+12:00",
        "2022-05-14T03:25:31+13:00",
        "2022-05-14T04:25:31+14:00",
        "2022-05-13T02:25:31-12:00"
    };
    std::for_each(begin(ok_strings), end(ok_strings), [](auto&& str)
    {
        BOOST_TEST_CONTEXT(str)
        {
            const auto t1 = Fred::Registry::Util::seconds_from_rfc3339_formated_string(str);
            BOOST_CHECK(t0 == t1);
        }
    });
}

BOOST_AUTO_TEST_CASE(seconds_from_rfc3339_formated_string_exception)
{
    static constexpr auto bad_strings = {
        "2022-05-13T16:25:31",
        "2022-05-13T16:25:31.1",
        "2022-05-13T16:25:31.Z",
        "2022-05-13T16:25:31q",
        "20022-05-13T16:25:31+02:00",
        "2022-25-13T16:25:31+02:00",
        "2022-02-29T16:25:31+02:00",
        "2022-05-32T16:25:31+02:00",
        "2022-05-13T24:25:31+02:00",
        "2022-05-13T16:65:31+02:00",
        "2022-05-13T16:25:61+02:00",
        "2022-05-13T16:25:31+15:00",
        "2022-05-13T16:25:31-13:00",
        "2022-05-13T16:25:31-00:00"
    };
    std::for_each(begin(bad_strings), end(bad_strings), [](auto&& str)
    {
        BOOST_TEST_CONTEXT(str)
        {
            BOOST_CHECK_EXCEPTION(
                    Fred::Registry::Util::seconds_from_rfc3339_formated_string(str),
                    Fred::Registry::Util::NotRfc3339Compliant,
                    [&str](const Fred::Registry::Util::NotRfc3339Compliant& e)
                    {
                        BOOST_TEST_MESSAGE(e.what());
                        return true;
                    });
        }
    });
}

BOOST_AUTO_TEST_SUITE_END()//TestUtil
