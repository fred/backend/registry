/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixture/cfg.hh"
#include "test/setup/fixture/contact.hh"
#include "test/setup/fixture/registrar.hh"

#include "src/contact/contact_info.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/delete_contact.hh"

#include <boost/test/unit_test.hpp>

#include <tuple>

BOOST_AUTO_TEST_SUITE(TestContactInfo)

namespace {

template <typename Src>
decltype(auto) make_address(Src&& street)
{
    LibFred::ContactAddress address;
    address.street1 = std::forward<Src>(street);
    address.city = "Praha 1";
    address.postalcode = "111 50";
    address.country = "CZ";
    return address;
}

template <typename Dst, typename Src>
Dst strong_cast(Src&& src)
{
    return Fred::Registry::Util::make_strong<Dst>(get_raw_value_from(std::forward<Src>(src)));
}

class TestContact
{
private:
    LibFred::OperationContextCreator ctx_;
public:
    explicit TestContact(const std::string& src_handle)
        : registrar{Test::Registrar::get<Test::Registrar::non_system>().get_handle()},
          handle{Test::Setup::Fixture::Cfg::make_unique_handle<Test::Contact::Handle>(src_handle)},
          name{Test::Contact::make_name("Test Kontakt")},
          organization{Test::Contact::make_organization("První Testovací a.s.")},
          place{[]()
                {
                    LibFred::Contact::PlaceAddress place;
                    place.street1 = "Kontaktní 1";
                    place.city = "Praha 1";
                    place.postalcode = "111 50";
                    place.country = "CZ";
                    return Test::Contact::make_place_address(place);
                }()},
          addresses_by_type{Test::Contact::make_contact_address_list({
                  {LibFred::ContactAddressType::MAILING, make_address("Korespondenční 1")},
                  {LibFred::ContactAddressType::BILLING, make_address("Fakturační 1")},
                  {LibFred::ContactAddressType::SHIPPING, make_address("Doručovací 1")},
                  {LibFred::ContactAddressType::SHIPPING_2, make_address("Doručovací 2")},
                  {LibFred::ContactAddressType::SHIPPING_3, make_address("Doručovací 3")}})},
          telephone{Test::Contact::make_telephone("+420.123456789")},
          fax{Test::Contact::make_fax("+420.987654321")},
          email{Test::Contact::make_email({"prvni@email.com"})},
          notify_email{Test::Contact::make_notify_email({"prvni@email.com", "druhy@email.com"})},
          vat_identification_number{Test::Contact::make_vat_identification_number("123454321")},
          ident{Test::Contact::make_personal_id(LibFred::PersonalIdUnion::get_BIRTHDAY("31.1.1970"))},
          disclose_name{Test::Contact::Disclose::name()},
          disclose_organization{Test::Contact::Disclose::organization()},
          disclose_address{Test::Contact::Disclose::address()},
          disclose_telephone{Test::Contact::Hide::telephone()},
          disclose_fax{Test::Contact::Hide::fax()},
          disclose_email{Test::Contact::Hide::email()},
          disclose_notify_email{Test::Contact::Hide::notify_email()},
          disclose_vat_identification_number{Test::Contact::Hide::vat_identification_number()},
          disclose_ident{Test::Contact::Hide::ident()},
          authinfo{Test::Contact::make_authinfo("AbCdEfGhIjK1")},
          warning_letter{Test::Contact::make_send_warning_letter(boost::none)},
          on_create_{[&]()
                     {
                         const auto info = Test::Contact::create(
                                 ctx_,
                                 handle,
                                 registrar,
                                 name,
                                 organization,
                                 place,
                                 addresses_by_type,
                                 telephone,
                                 fax,
                                 email,
                                 notify_email,
                                 vat_identification_number,
                                 ident,
                                 disclose_name,
                                 disclose_organization,
                                 disclose_address,
                                 disclose_telephone,
                                 disclose_fax,
                                 disclose_email,
                                 disclose_notify_email,
                                 disclose_vat_identification_number,
                                 disclose_ident,
                                 authinfo,
                                 warning_letter);

                         BOOST_CHECK_EQUAL(info.get_data().crhistoryid, info.get_data().historyid);
                         BOOST_CHECK_EQUAL(get_raw_value_from(registrar), info.get_data().create_registrar_handle);
                         BOOST_CHECK_EQUAL(get_raw_value_from(handle), info.get_data().handle);
                         BOOST_CHECK_EQUAL(get_raw_value_from(name), info.get_data().name.get_value_or(""));
                         BOOST_CHECK_EQUAL(get_raw_value_from(organization), info.get_data().organization.get_value_or(""));
                         BOOST_CHECK_EQUAL(get_raw_value_from(authinfo), info.get_data().authinfopw);
                         BOOST_CHECK_EQUAL(get_raw_value_from(warning_letter) == boost::none, info.get_data().warning_letter.isnull());
                         if (get_raw_value_from(warning_letter) != boost::none)
                         {
                             BOOST_CHECK_EQUAL(*get_raw_value_from(warning_letter), info.get_data().warning_letter.get_value());
                         }
                         BOOST_REQUIRE(!info.get_data().place.isnull());
                         BOOST_CHECK_EQUAL(get_raw_value_from(place).street1, info.get_data().place.get_value().street1);
                         BOOST_CHECK(!get_raw_value_from(place).street2.is_set());
                         BOOST_CHECK(!info.get_data().place.get_value().street2.is_set());
                         BOOST_CHECK(!get_raw_value_from(place).street3.is_set());
                         BOOST_CHECK(!info.get_data().place.get_value().street3.is_set());
                         BOOST_CHECK_EQUAL(get_raw_value_from(place).city, info.get_data().place.get_value().city);
                         BOOST_CHECK(!get_raw_value_from(place).stateorprovince.is_set());
                         BOOST_CHECK(!info.get_data().place.get_value().stateorprovince.is_set());
                         BOOST_CHECK_EQUAL(get_raw_value_from(place).postalcode, info.get_data().place.get_value().postalcode);
                         BOOST_CHECK_EQUAL(get_raw_value_from(place).country, info.get_data().place.get_value().country);
                         BOOST_CHECK_EQUAL(get_raw_value_from(addresses_by_type).size(), 5);
                         BOOST_CHECK_EQUAL(get_raw_value_from(addresses_by_type).size(), info.get_data().addresses.size());
                         for (const auto src_addr_iter : get_raw_value_from(addresses_by_type))
                         {
                             const auto dst_addr_iter = info.get_data().addresses.find(src_addr_iter.first);
                             BOOST_REQUIRE(dst_addr_iter != info.get_data().addresses.end());
                             BOOST_CHECK_EQUAL(src_addr_iter.second.street1, dst_addr_iter->second.street1);
                             BOOST_CHECK(!src_addr_iter.second.street2.is_set());
                             BOOST_CHECK(!dst_addr_iter->second.street2.is_set());
                             BOOST_CHECK(!src_addr_iter.second.street3.is_set());
                             BOOST_CHECK(!dst_addr_iter->second.street3.is_set());
                             BOOST_CHECK_EQUAL(src_addr_iter.second.city, dst_addr_iter->second.city);
                             BOOST_CHECK(!src_addr_iter.second.stateorprovince.is_set());
                             BOOST_CHECK(!dst_addr_iter->second.stateorprovince.is_set());
                             BOOST_CHECK_EQUAL(src_addr_iter.second.postalcode, dst_addr_iter->second.postalcode);
                             BOOST_CHECK_EQUAL(src_addr_iter.second.country, dst_addr_iter->second.country);
                         }
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_name), info.get_data().disclosename);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_organization), info.get_data().discloseorganization);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_address), info.get_data().discloseaddress);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_telephone), info.get_data().disclosetelephone);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_fax), info.get_data().disclosefax);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_email), info.get_data().discloseemail);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_notify_email), info.get_data().disclosenotifyemail);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_vat_identification_number), info.get_data().disclosevat);
                         BOOST_CHECK_EQUAL(get_raw_value_from(disclose_ident), info.get_data().discloseident);

                         return OnCreate{std::make_tuple(info.get_data().id,
                                                         strong_cast<Fred::Registry::Contact::ContactId>(info.get_data().uuid)),
                                         std::make_tuple(info.get_data().crhistoryid,
                                                         strong_cast<Fred::Registry::Contact::ContactHistoryId>(info.get_data().history_uuid))};
                    }()}
    {
        ctx_.commit_transaction();
    }
    decltype(auto) get_id() const
    {
        return std::get<Fred::Registry::Contact::ContactId>(on_create_.id);
    }
    decltype(auto) get_numeric_id() const
    {
        return std::get<unsigned long long>(on_create_.id);
    }
    decltype(auto) get_history_id() const
    {
        return std::get<Fred::Registry::Contact::ContactHistoryId>(on_create_.history_id);
    }
    decltype(auto) get_numeric_history_id() const
    {
        return std::get<unsigned long long>(on_create_.history_id);
    }
    const Test::Registrar::Handle registrar;
    const Test::Contact::Handle handle;
    Test::Contact::Name name;
    Test::Contact::Organization organization;
    Test::Contact::PlaceAddress place;
    Test::Contact::ContactAddressList addresses_by_type;
    Test::Contact::Telephone telephone;
    Test::Contact::Fax fax;
    Test::Contact::Email email;
    Test::Contact::NotifyEmail notify_email;
    Test::Contact::VatIdentificationNumber vat_identification_number;
    Test::Contact::PersonalIdUnion ident;
    Test::Contact::DiscloseFlag::Name disclose_name;
    Test::Contact::DiscloseFlag::Organization disclose_organization;
    Test::Contact::DiscloseFlag::Address disclose_address;
    Test::Contact::DiscloseFlag::Telephone disclose_telephone;
    Test::Contact::DiscloseFlag::Fax disclose_fax;
    Test::Contact::DiscloseFlag::Email disclose_email;
    Test::Contact::DiscloseFlag::NotifyEmail disclose_notify_email;
    Test::Contact::DiscloseFlag::VatIdentificationNumber disclose_vat_identification_number;
    Test::Contact::DiscloseFlag::Ident disclose_ident;
    Test::Contact::Authinfo authinfo;
    Test::Contact::SendWarningLetter warning_letter;
private:
    struct OnCreate
    {
        std::tuple<unsigned long long, Fred::Registry::Contact::ContactId> id;
        std::tuple<unsigned long long, Fred::Registry::Contact::ContactHistoryId> history_id;
    } const on_create_;
};

template <typename Src, typename SrcDiscloseFlagTag, typename Dst, typename DiscloseFlagCheck, typename ValueCheck, typename NoValue>
void check_equality(
            const Src& src_data,
            const Fred::Registry::Util::StrongType<bool, SrcDiscloseFlagTag, Fred::Registry::Util::Skill::Printable>& src_disclose_flag,
            const Fred::Registry::Contact::PrivacyControlled<Dst>& dst_data,
            DiscloseFlagCheck disclose_flag_check,
            ValueCheck value_check,
            NoValue on_no_value)
{
    class VisitData : public boost::static_visitor<>
    {
    public:
        explicit VisitData(
                    const Src& src_data,
                    bool src_disclose_flag,
                    bool default_disclose_flag,
                    DiscloseFlagCheck disclose_flag_check,
                    ValueCheck value_check,
                    NoValue on_no_value)
            : src_data_{src_data},
              src_disclose_flag_{src_disclose_flag},
              default_disclose_flag_{default_disclose_flag},
              disclose_flag_check_{disclose_flag_check},
              value_check_{value_check},
              on_no_value_{on_no_value}
        { }
        void operator()(const Fred::Registry::Contact::PublicData<Dst>& data) const
        {
            disclose_flag_check_(src_disclose_flag_, true);
            value_check_(src_data_, get_data_from(data));
        }
        void operator()(const Fred::Registry::Contact::NonPublicData<Dst>& data) const
        {
            disclose_flag_check_(src_disclose_flag_, false);
            value_check_(src_data_, get_data_from(data));
        }
        void operator()(const Fred::Registry::Contact::DefaultPublicityData<Dst>& data) const
        {
            disclose_flag_check_(src_disclose_flag_, default_disclose_flag_);
            value_check_(src_data_, get_data_from(data));
        }
        void operator()(const boost::blank&) const
        {
            on_no_value_();
        }
        const Src& src_data_;
        bool src_disclose_flag_;
        bool default_disclose_flag_;
        DiscloseFlagCheck disclose_flag_check_;
        ValueCheck value_check_;
        NoValue on_no_value_;
    };
    boost::apply_visitor(VisitData{src_data,
                                   get_raw_value_from(src_disclose_flag),
                                   false,
                                   disclose_flag_check,
                                   value_check,
                                   on_no_value},
                         dst_data);
}

template <typename T>
bool equals(const T& lhs, const T& rhs, bool& miss)
{
    bool success = lhs == rhs;
    miss |= !success;
    return success;
}

bool operator==(const LibFred::Contact::PlaceAddress& lhs, const Fred::Registry::PlaceAddress& rhs)
{
    bool predicate_failure = false;
    const auto is_true = [&](bool value) { return equals(value, true, predicate_failure); };
    const auto is_equal = [&](const std::string& lhs, const std::string& rhs) { return is_true(lhs == rhs); };
    const auto is_lower = [&](std::size_t lhs, std::size_t rhs) { return is_true(lhs < rhs); };
    switch (rhs.street.size())
    {
        case 0:
            BOOST_CHECK_PREDICATE(is_true, (lhs.street1.empty()));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street2.get_value_or("").empty()));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street3.get_value_or("").empty()));
            break;
        case 1:
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street1)(rhs.street[0]));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street2.get_value_or("").empty()));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street3.get_value_or("").empty()));
            break;
        case 2:
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street1)(rhs.street[0]));
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street2.get_value_or(""))(rhs.street[1]));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street3.get_value_or("").empty()));
            break;
        case 3:
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street1)(rhs.street[0]));
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street2.get_value_or(""))(rhs.street[1]));
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street3.get_value_or(""))(rhs.street[2]));
            break;
        default:
            BOOST_CHECK_PREDICATE(is_lower, (rhs.street.size())(4));
            break;
    }
    BOOST_CHECK_PREDICATE(is_equal, (lhs.city)(rhs.city));
    BOOST_CHECK_PREDICATE(is_equal, (lhs.stateorprovince.get_value_or(""))(rhs.state_or_province));
    BOOST_CHECK_PREDICATE(is_equal, (lhs.postalcode)(get_raw_value_from(rhs.postal_code)));
    BOOST_CHECK_PREDICATE(is_equal, (lhs.country)(get_raw_value_from(rhs.country_code)));
    return !predicate_failure;
}

template <typename ValueCheck>
void check_equality(
            const Test::Contact::PersonalIdUnion& src_data,
            const Fred::Registry::Contact::ContactAdditionalIdentifier& dst_data,
            ValueCheck value_check)
{
    class VisitData : public boost::static_visitor<>
    {
    public:
        explicit VisitData(
                    const Test::Contact::PersonalIdUnion& src_data,
                    ValueCheck value_check)
            : src_data_{src_data},
              value_check_{value_check}
        { }
        void operator()(const Fred::Registry::Contact::NationalIdentityNumber& data)const
        {
            value_check_(get_raw_value_from(src_data_), LibFred::PersonalIdUnion::get_RC(get_raw_value_from(data)));
        }
        void operator()(const Fred::Registry::Contact::NationalIdentityCard& data)const
        {
            value_check_(get_raw_value_from(src_data_), LibFred::PersonalIdUnion::get_OP(get_raw_value_from(data)));
        }
        void operator()(const Fred::Registry::Contact::PassportNumber& data)const
        {
            value_check_(get_raw_value_from(src_data_), LibFred::PersonalIdUnion::get_PASS(get_raw_value_from(data)));
        }
        void operator()(const Fred::Registry::CompanyRegistrationNumber& data)const
        {
            value_check_(get_raw_value_from(src_data_), LibFred::PersonalIdUnion::get_ICO(get_raw_value_from(data)));
        }
        void operator()(const Fred::Registry::Contact::SocialSecurityNumber& data)const
        {
            value_check_(get_raw_value_from(src_data_), LibFred::PersonalIdUnion::get_MPSV(get_raw_value_from(data)));
        }
        void operator()(const Fred::Registry::Contact::Birthdate& data)const
        {
            value_check_(get_raw_value_from(src_data_), LibFred::PersonalIdUnion::get_BIRTHDAY(get_raw_value_from(data)));
        }
        const Test::Contact::PersonalIdUnion& src_data_;
        const ValueCheck value_check_;
    };
    boost::apply_visitor(VisitData{src_data, value_check}, dst_data);
}

decltype(auto) get_contact_info(const TestContact& contact)
{
    Fred::Registry::Contact::ContactInfoRequest request;
    request.contact_id = contact.get_id();
    return contact_info(request);
}

bool operator==(const LibFred::ContactAddress& lhs, const Fred::Registry::Contact::ContactAddress& rhs)
{
    bool predicate_failure = false;
    const auto is_true = [&](bool value) { return equals(value, true, predicate_failure); };
    const auto is_equal = [&](const std::string& lhs, const std::string& rhs) { return is_true(lhs == rhs); };
    const auto is_lower = [&](std::size_t lhs, std::size_t rhs) { return is_true(lhs < rhs); };
    BOOST_CHECK_PREDICATE(is_equal, (lhs.company_name.get_value_or(""))(rhs.company));
    switch (rhs.street.size())
    {
        case 0:
            BOOST_CHECK_PREDICATE(is_true, (lhs.street1.empty()));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street2.get_value_or("").empty()));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street3.get_value_or("").empty()));
            break;
        case 1:
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street1)(rhs.street[0]));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street2.get_value_or("").empty()));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street3.get_value_or("").empty()));
            break;
        case 2:
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street1)(rhs.street[0]));
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street2.get_value_or(""))(rhs.street[1]));
            BOOST_CHECK_PREDICATE(is_true, (lhs.street3.get_value_or("").empty()));
            break;
        case 3:
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street1)(rhs.street[0]));
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street2.get_value_or(""))(rhs.street[1]));
            BOOST_CHECK_PREDICATE(is_equal, (lhs.street3.get_value_or(""))(rhs.street[2]));
            break;
        default:
            BOOST_CHECK_PREDICATE(is_lower, (rhs.street.size())(4));
            break;
    }
    BOOST_CHECK_PREDICATE(is_equal, (lhs.city)(rhs.city));
    BOOST_CHECK_PREDICATE(is_equal, (lhs.stateorprovince.get_value_or(""))(rhs.state_or_province));
    BOOST_CHECK_PREDICATE(is_equal, (lhs.postalcode)(get_raw_value_from(rhs.postal_code)));
    BOOST_CHECK_PREDICATE(is_equal, (lhs.country)(get_raw_value_from(rhs.country_code)));
    return !predicate_failure;
}

bool equals_addresses(const Test::Contact::ContactAddressList& src,
                      const boost::optional<Fred::Registry::Contact::ContactAddress>& dst_mailing,
                      const boost::optional<Fred::Registry::Contact::ContactAddress>& dst_billing,
                      const std::array<boost::optional<Fred::Registry::Contact::ContactAddress>, 3>& dst_shipping)
{
    bool predicate_failure = false;
    const auto src_addresses = get_raw_value_from(src);
    const auto is_true = [&](bool value) { return equals(value, true, predicate_failure); };
    const auto is_equal = [&](const LibFred::ContactAddressList::const_iterator& lhs,
                              const boost::optional<Fred::Registry::Contact::ContactAddress>& rhs)
    {
        if (rhs == boost::none)
        {
            BOOST_CHECK_PREDICATE(is_true, (lhs == src_addresses.end()));
            return lhs == src_addresses.end();
        }
        BOOST_CHECK_PREDICATE(is_true, (lhs != src_addresses.end()));
        BOOST_CHECK_PREDICATE(is_true, ((lhs != src_addresses.end()) && (lhs->second == *rhs)));
        return (lhs != src_addresses.end()) && (lhs->second == *rhs);
    };
    BOOST_CHECK(is_equal(src_addresses.find(LibFred::ContactAddressType::MAILING), dst_mailing));
    BOOST_CHECK(is_equal(src_addresses.find(LibFred::ContactAddressType::BILLING), dst_billing));
    BOOST_CHECK(is_equal(src_addresses.find(LibFred::ContactAddressType::SHIPPING), dst_shipping[0]));
    BOOST_CHECK(is_equal(src_addresses.find(LibFred::ContactAddressType::SHIPPING_2), dst_shipping[1]));
    BOOST_CHECK(is_equal(src_addresses.find(LibFred::ContactAddressType::SHIPPING_3), dst_shipping[2]));
    return !predicate_failure;
}

bool operator==(const Test::Contact::SendWarningLetter& lhs, const Fred::Registry::Contact::WarningLetter& rhs)
{
    bool predicate_failure = false;
    const auto is_true = [&](bool value) { return equals(value, true, predicate_failure); };
    const auto lhs_to_send = get_raw_value_from(lhs);
    if (lhs_to_send == boost::none)
    {
        BOOST_CHECK_PREDICATE(is_true, (rhs.preference == Fred::Registry::Contact::WarningLetter::SendingPreference::not_specified));
    }
    else if (*lhs_to_send)
    {
        BOOST_CHECK_PREDICATE(is_true, (rhs.preference == Fred::Registry::Contact::WarningLetter::SendingPreference::to_send));
    }
    else
    {
        BOOST_CHECK_PREDICATE(is_true, (rhs.preference == Fred::Registry::Contact::WarningLetter::SendingPreference::not_to_send));
    }
    return !predicate_failure;
}

bool is_corresponding(const TestContact& contact, const Fred::Registry::Contact::ContactInfoReply::Data& info)
{
    bool predicate_failure = false;
    const auto equals_bool = [&](bool lhs, bool rhs) { return equals(lhs, rhs, predicate_failure); };
    const auto equals_string = [&](const std::string& lhs, const std::string& rhs) { return equals(lhs, rhs, predicate_failure); };
    const auto equals_size = [&](std::size_t lhs, std::size_t rhs) { return equals(lhs, rhs, predicate_failure); };
    const auto is_true = [&](bool value) { return equals(value, true, predicate_failure); };
    check_equality(contact.name, contact.disclose_name, info.name,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::Name& lhs, const std::string& rhs) { BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs))(rhs)); },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.organization, contact.disclose_organization, info.organization,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::Organization& lhs, const std::string& rhs) { BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs))(rhs)); },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.place, contact.disclose_address, info.place,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::PlaceAddress& lhs, const Fred::Registry::PlaceAddress& rhs)
                   {
                       BOOST_CHECK_PREDICATE(is_true, (get_raw_value_from(lhs) == rhs));
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.telephone, contact.disclose_telephone, info.telephone,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::Telephone& lhs, const boost::optional<Fred::Registry::PhoneNumber>& rhs)
                   {
                       if (rhs == boost::none)
                       {
                           BOOST_CHECK_PREDICATE(is_true, (get_raw_value_from(lhs).empty()));
                       }
                       else
                       {
                           BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs))(get_raw_value_from(*rhs)));
                       }
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.fax, contact.disclose_fax, info.fax,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::Fax& lhs, const boost::optional<Fred::Registry::PhoneNumber>& rhs)
                   {
                       if (rhs == boost::none)
                       {
                           BOOST_CHECK_PREDICATE(is_true, (get_raw_value_from(lhs).empty()));
                       }
                       else
                       {
                           BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs))(get_raw_value_from(*rhs)));
                       }
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.email, contact.disclose_email, info.emails,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::Email& lhs, const std::vector<Fred::Registry::EmailAddress>& rhs)
                   {
                       BOOST_CHECK_PREDICATE(equals_size, (get_raw_value_from(lhs).size())(rhs.size()));
                       for (std::size_t idx = 0; idx < get_raw_value_from(lhs).size(); ++idx)
                       {
                           if (rhs.size() <= idx)
                           {
                               break;
                           }
                           BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs)[idx])(get_raw_value_from(rhs[idx])));
                       }
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.notify_email, contact.disclose_notify_email, info.notify_emails,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::NotifyEmail& lhs, const std::vector<Fred::Registry::EmailAddress>& rhs)
                   {
                       BOOST_CHECK_PREDICATE(equals_size, (get_raw_value_from(lhs).size())(rhs.size()));
                       for (std::size_t idx = 0; idx < get_raw_value_from(lhs).size(); ++idx)
                       {
                           if (rhs.size() <= idx)
                           {
                               break;
                           }
                           BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs)[idx])(get_raw_value_from(rhs[idx])));
                       }
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.vat_identification_number, contact.disclose_vat_identification_number, info.vat_identification_number,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::VatIdentificationNumber& lhs, const boost::optional<Fred::Registry::VatIdentificationNumber>& rhs)
                   {
                       if (rhs == boost::none)
                       {
                           BOOST_CHECK_PREDICATE(is_true, (get_raw_value_from(lhs).empty()));
                       }
                       else
                       {
                           BOOST_CHECK_PREDICATE(equals_string, (get_raw_value_from(lhs))(get_raw_value_from(*rhs)));
                       }
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    check_equality(contact.ident, contact.disclose_ident, info.additional_identifier,
                   [&](bool lhs, bool rhs) { BOOST_CHECK_PREDICATE(equals_bool, (lhs)(rhs)); },
                   [&](const Test::Contact::PersonalIdUnion& lhs, const boost::optional<Fred::Registry::Contact::ContactAdditionalIdentifier>& rhs)
                   {
                       if (rhs == boost::none)
                       {
                           BOOST_CHECK_PREDICATE(is_true, (get_raw_value_from(lhs).get_type().empty()));
                           BOOST_CHECK_PREDICATE(is_true, (get_raw_value_from(lhs).get().empty()));
                       }
                       else
                       {
                           check_equality(lhs, *rhs,
                                          [&](const LibFred::PersonalIdUnion& lhs, const LibFred::PersonalIdUnion& rhs)
                                          {
                                              BOOST_CHECK_PREDICATE(equals_string, (lhs.get_type())(rhs.get_type()));
                                              BOOST_CHECK_PREDICATE(equals_string, (lhs.get())(rhs.get()));
                                          });
                       }
                   },
                   [&]() { BOOST_CHECK_PREDICATE(is_true, (false)); });
    BOOST_CHECK_PREDICATE(is_true, (equals_addresses(contact.addresses_by_type, info.mailing_address, info.billing_address, info.shipping_address)));
    BOOST_CHECK_PREDICATE(is_true, (contact.warning_letter == info.warning_letter));
    return !predicate_failure;
}

}//namespace {anonymous}

BOOST_AUTO_TEST_CASE(info)
{
    const TestContact contact{"TEST-KONTAKT-INFO-0001"};
    const auto reply = get_contact_info(contact);
    BOOST_CHECK_EQUAL(get_raw_value_from(contact.handle), get_raw_value_from(reply.contact_handle));
    BOOST_CHECK_EQUAL(get_raw_value_from(contact.get_id()), get_raw_value_from(reply.contact_id));
    BOOST_CHECK_EQUAL(get_raw_value_from(contact.get_history_id()), get_raw_value_from(reply.contact_history_id));
    BOOST_CHECK(is_corresponding(contact, reply));
    {
        LibFred::OperationContextCreator ctx;
        LibFred::DeleteContactById{contact.get_numeric_id()}.exec(ctx);
        ctx.commit_transaction();
    }
    BOOST_CHECK_EXCEPTION(get_contact_info(contact), Fred::Registry::Contact::ContactInfoReply::Exception::ContactDoesNotExist, [](const std::exception&) { return true; });
}

BOOST_AUTO_TEST_SUITE_END()//TestContactInfo
