/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/setup/fixture/cfg.hh"
#include "test/setup/fixture/contact.hh"
#include "test/setup/fixture/registrar.hh"

#include "src/contact/contact_handle_history.hh"
#include "src/contact/contact_info.hh"
#include "src/contact/update_contact.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/delete_contact.hh"

#include <boost/test/unit_test.hpp>

#include <tuple>

BOOST_AUTO_TEST_SUITE(TestContactHandleHistory)

namespace {

template <typename Src>
decltype(auto) make_address(Src&& street)
{
    LibFred::ContactAddress address;
    address.street1 = std::forward<Src>(street);
    address.city = "Praha 1";
    address.postalcode = "111 50";
    address.country = "CZ";
    return address;
}

template <typename Dst, typename Src>
Dst strong_cast(Src&& src)
{
    return Fred::Registry::Util::make_strong<Dst>(get_raw_value_from(std::forward<Src>(src)));
}

class TestContact
{
private:
    LibFred::OperationContextCreator ctx_;
public:
    explicit TestContact(const std::string& src_handle)
        : registrar_originator{Fred::Registry::Util::make_strong<Test::Registrar::Handle>(*Test::Setup::Fixture::Cfg::Options::get().fred.registrar_originator)},
          handle{Test::Setup::Fixture::Cfg::make_unique_handle<Test::Contact::Handle>(src_handle)},
          name{Test::Contact::make_name("Test Kontakt")},
          organization{Test::Contact::make_organization("První Testovací a.s.")},
          place{[]()
                {
                    LibFred::Contact::PlaceAddress place;
                    place.street1 = "Kontaktní 1";
                    place.city = "Praha 1";
                    place.postalcode = "111 50";
                    place.country = "CZ";
                    return Test::Contact::make_place_address(place);
                }()},
          addresses_by_type{Test::Contact::make_contact_address_list({
                  {LibFred::ContactAddressType::MAILING, make_address("Korespondenční 1")},
                  {LibFred::ContactAddressType::BILLING, make_address("Fakturační 1")},
                  {LibFred::ContactAddressType::SHIPPING, make_address("Doručovací 1")},
                  {LibFred::ContactAddressType::SHIPPING_2, make_address("Doručovací 2")},
                  {LibFred::ContactAddressType::SHIPPING_3, make_address("Doručovací 3")}})},
          telephone{Test::Contact::make_telephone("")},
          fax{Test::Contact::make_fax("")},
          email{Test::Contact::make_email({"prvni@email.com"})},
          notify_email{Test::Contact::make_notify_email({})},
          vat_identification_number{Test::Contact::make_vat_identification_number("")},
          ident{Test::Contact::make_personal_id(LibFred::PersonalIdUnion::get_any_type("", ""))},
          disclose_name{Test::Contact::Disclose::name()},
          disclose_organization{Test::Contact::Disclose::organization()},
          disclose_address{Test::Contact::Disclose::address()},
          disclose_telephone{Test::Contact::Hide::telephone()},
          disclose_fax{Test::Contact::Hide::fax()},
          disclose_email{Test::Contact::Hide::email()},
          disclose_notify_email{Test::Contact::Hide::notify_email()},
          disclose_vat_identification_number{Test::Contact::Hide::vat_identification_number()},
          disclose_ident{Test::Contact::Hide::ident()},
          authinfo{Test::Contact::make_authinfo("AbCdEfGhIjK1")},
          warning_letter{Test::Contact::make_send_warning_letter(boost::none)},
          on_create_{[&]()
                     {
                         const auto info = Test::Contact::create(
                                 ctx_,
                                 handle,
                                 registrar_originator,
                                 name,
                                 organization,
                                 place,
                                 addresses_by_type,
                                 telephone,
                                 fax,
                                 email,
                                 notify_email,
                                 vat_identification_number,
                                 ident,
                                 disclose_name,
                                 disclose_organization,
                                 disclose_address,
                                 disclose_telephone,
                                 disclose_fax,
                                 disclose_email,
                                 disclose_notify_email,
                                 disclose_vat_identification_number,
                                 disclose_ident,
                                 authinfo,
                                 warning_letter);

                         return OnCreate{std::make_tuple(info.get_data().id,
                                                         strong_cast<Fred::Registry::Contact::ContactId>(info.get_data().uuid)),
                                         std::make_tuple(info.get_data().crhistoryid,
                                                         strong_cast<Fred::Registry::Contact::ContactHistoryId>(info.get_data().history_uuid))};
                    }()}
    {
        ctx_.commit_transaction();
    }
    decltype(auto) get_id() const
    {
        return std::get<Fred::Registry::Contact::ContactId>(on_create_.id);
    }
    decltype(auto) get_numeric_id() const
    {
        return std::get<unsigned long long>(on_create_.id);
    }
    decltype(auto) get_history_id() const
    {
        return std::get<Fred::Registry::Contact::ContactHistoryId>(on_create_.history_id);
    }
    decltype(auto) get_numeric_history_id() const
    {
        return std::get<unsigned long long>(on_create_.history_id);
    }
    const Test::Registrar::Handle registrar_originator;
    const Test::Registrar::Handle unauthorized_registrar;
    const Test::Contact::Handle handle;
    Test::Contact::Name name;
    Test::Contact::Organization organization;
    Test::Contact::PlaceAddress place;
    Test::Contact::ContactAddressList addresses_by_type;
    Test::Contact::Telephone telephone;
    Test::Contact::Fax fax;
    Test::Contact::Email email;
    Test::Contact::NotifyEmail notify_email;
    Test::Contact::VatIdentificationNumber vat_identification_number;
    Test::Contact::PersonalIdUnion ident;
    Test::Contact::DiscloseFlag::Name disclose_name;
    Test::Contact::DiscloseFlag::Organization disclose_organization;
    Test::Contact::DiscloseFlag::Address disclose_address;
    Test::Contact::DiscloseFlag::Telephone disclose_telephone;
    Test::Contact::DiscloseFlag::Fax disclose_fax;
    Test::Contact::DiscloseFlag::Email disclose_email;
    Test::Contact::DiscloseFlag::NotifyEmail disclose_notify_email;
    Test::Contact::DiscloseFlag::VatIdentificationNumber disclose_vat_identification_number;
    Test::Contact::DiscloseFlag::Ident disclose_ident;
    Test::Contact::Authinfo authinfo;
    Test::Contact::SendWarningLetter warning_letter;
private:
    struct OnCreate
    {
        std::tuple<unsigned long long, Fred::Registry::Contact::ContactId> id;
        std::tuple<unsigned long long, Fred::Registry::Contact::ContactHistoryId> history_id;
    } const on_create_;
};

decltype(auto) get_contact_handle_history(const Fred::Registry::Contact::ContactHandle& contact_handle)
{
    const Fred::Registry::Contact::ContactHandleHistoryRequest request{contact_handle};
    return contact_handle_history(request);
}

decltype(auto) get_contact_handle_history(const TestContact& contact)
{
    return get_contact_handle_history(strong_cast<Fred::Registry::Contact::ContactHandle>(contact.handle));
}

}//namespace {anonymous}

BOOST_AUTO_TEST_CASE(history)
{
    const std::string handle = "TEST-KONTAKT-HANDLE-HISTORY-0001";
    const auto unique_handle = Test::Setup::Fixture::Cfg::make_unique_handle<Fred::Registry::Contact::ContactHandle>(handle);
    const auto history_0 = get_contact_handle_history(unique_handle);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_0.contact_handle), get_raw_value_from(unique_handle));
    BOOST_CHECK_EQUAL(history_0.timeline.size(), 0);
    const TestContact contact{handle};
    const auto history_1 = get_contact_handle_history(contact);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_1.contact_handle), get_raw_value_from(contact.handle));
    BOOST_REQUIRE_EQUAL(history_1.timeline.size(), 1);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_1.timeline[0].contact_id), get_raw_value_from(contact.get_id()));
    BOOST_CHECK_EQUAL(get_raw_value_from(history_1.timeline[0].begin.contact_history_id), get_raw_value_from(contact.get_history_id()));
    BOOST_CHECK(history_1.timeline[0].end == boost::none);
    Fred::Registry::Contact::UpdateContactRequest request;
    request.contact_id = contact.get_id();
    request.contact_history_id = contact.get_history_id();
    request.registrar_originator = Fred::Registry::Util::make_strong<Fred::Registry::Registrar::RegistrarHandle>(*Test::Setup::Fixture::Cfg::Options::get().fred.registrar_originator);
    request.set_name = "Testovka Kontaktová";
    update_contact(request);
    const auto history_2 = get_contact_handle_history(contact);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_2.contact_handle), get_raw_value_from(contact.handle));
    BOOST_REQUIRE_EQUAL(history_2.timeline.size(), 1);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_2.timeline[0].contact_id), get_raw_value_from(contact.get_id()));
    BOOST_CHECK_EQUAL(get_raw_value_from(history_2.timeline[0].begin.contact_history_id), get_raw_value_from(contact.get_history_id()));
    BOOST_CHECK(history_2.timeline[0].end == boost::none);
    {
        LibFred::OperationContextCreator ctx;
        LibFred::DeleteContactById{contact.get_numeric_id()}.exec(ctx);
        ctx.commit_transaction();
    }
    const auto history_3 = get_contact_handle_history(contact);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_3.contact_handle), get_raw_value_from(contact.handle));
    BOOST_REQUIRE_EQUAL(history_3.timeline.size(), 1);
    BOOST_CHECK_EQUAL(get_raw_value_from(history_3.timeline[0].contact_id), get_raw_value_from(contact.get_id()));
    BOOST_CHECK_EQUAL(get_raw_value_from(history_3.timeline[0].begin.contact_history_id), get_raw_value_from(contact.get_history_id()));
    BOOST_REQUIRE(history_3.timeline[0].end != boost::none);
    BOOST_CHECK(history_3.timeline[0].begin.timestamp <= history_3.timeline[0].end->timestamp);
}

BOOST_AUTO_TEST_SUITE_END()//TestContactHandleHistory
