/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact/contact_state_flags.hh"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(TestContactStateFlags)

BOOST_AUTO_TEST_CASE(info)
{
    const auto contact_state_flags_info = Fred::Registry::Contact::get_contact_state_flags_info();
    BOOST_CHECK_EQUAL(contact_state_flags_info.size(), 13);
}

BOOST_AUTO_TEST_SUITE_END()//TestContactStateFlags
