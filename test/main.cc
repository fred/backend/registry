/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestBackendRegistry

#include "config.h"

#include "test/setup/fixture/cfg.hh"
#include "test/setup/fixture/contact.hh"
#include "test/setup/fixture/registrar.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrar/get_registrar_handles.hh"

#include "util/db/manager.hh"
#include "util/log/log.hh"

#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

// dynamic library version
#include <boost/test/unit_test.hpp>
#include <boost/assign/list_of.hpp>

#include <cstdlib>
#include <iostream>

namespace {

enum class Logger
{
    is_running,
    is_not_running
};

auto severity_to_level(Test::Setup::Fixture::Cfg::LogSeverity severity)
{
    switch (severity)
    {
        case Test::Setup::Fixture::Cfg::LogSeverity::emerg:
            return LibLog::Level::critical;
        case Test::Setup::Fixture::Cfg::LogSeverity::alert:
            return LibLog::Level::critical;
        case Test::Setup::Fixture::Cfg::LogSeverity::crit:
            return LibLog::Level::critical;
        case Test::Setup::Fixture::Cfg::LogSeverity::err:
            return LibLog::Level::error;
        case Test::Setup::Fixture::Cfg::LogSeverity::warning:
            return LibLog::Level::warning;
        case Test::Setup::Fixture::Cfg::LogSeverity::notice:
            return LibLog::Level::info;
        case Test::Setup::Fixture::Cfg::LogSeverity::info:
            return LibLog::Level::info;
        case Test::Setup::Fixture::Cfg::LogSeverity::debug:
            return LibLog::Level::debug;
        case Test::Setup::Fixture::Cfg::LogSeverity::trace:
            return LibLog::Level::trace;
    }
    return LibLog::Level::trace;
}

struct SetupLogging : boost::static_visitor<Logger>
{
    Logger operator()(const boost::blank&) const
    {
        return Logger::is_not_running;
    }
    Logger operator()(const Test::Setup::Fixture::Cfg::Options::Console& option) const
    {
        LibLog::Sink::ConsoleSinkConfig config;
        LibLog::LogConfig log_config;
        config.set_level(severity_to_level(option.min_severity));
        config.set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr);
        config.set_color_mode(LibLog::ColorMode::never);
        log_config.add_sink_config(config);
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        return Logger::is_running;
    }
    Logger operator()(const Test::Setup::Fixture::Cfg::Options::LogFile& option) const
    {
        LibLog::Sink::FileSinkConfig config{option.file_name};
        LibLog::LogConfig log_config;
        config.set_level(severity_to_level(option.min_severity));
        log_config.add_sink_config(config);
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        return Logger::is_running;
    }
    Logger operator()(const Test::Setup::Fixture::Cfg::Options::SysLog& option) const
    {
        LibLog::Sink::SyslogSinkConfig config;
        LibLog::LogConfig log_config;
        config.set_level(severity_to_level(option.min_severity));
        config.set_syslog_facility(static_cast<int>(option.facility));
        log_config.add_sink_config(config);
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        return Logger::is_running;
    }
};

std::string make_connect_string(const Test::Setup::Fixture::Cfg::Options::Database& option)
{
    std::string result;
    if (option.host != boost::none)
    {
        result = "host=" + *option.host;
    }
    if (option.port != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "port=" + std::to_string(*option.port);
    }
    if (option.user != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "user=" + *option.user;
    }
    if (option.dbname != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "dbname=" + *option.dbname;
    }
    if (option.password != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "password=" + *option.password;
    }
    return result;
}

struct GlobalConfigFixture
{
    GlobalConfigFixture()
    {
        try
        {
            Test::Setup::Fixture::Cfg::Options::init(boost::unit_test::framework::master_test_suite().argc,
                                                     boost::unit_test::framework::master_test_suite().argv);
        }
        catch (const Test::Setup::Fixture::Cfg::HelpRequested& help)
        {
            std::cout << help.what() << std::endl;
            std::exit(EXIT_SUCCESS);
        }
        catch (const Test::Setup::Fixture::Cfg::UnknownOption& unknown_option)
        {
            std::cerr << unknown_option.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (const Test::Setup::Fixture::Cfg::MissingOption& missing_option)
        {
            std::cerr << missing_option.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (const Test::Setup::Fixture::Cfg::Exception& e)
        {
            std::cerr << "Configuration problem: " << e.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (const std::exception& e)
        {
            std::cerr << "Exception caught: " << e.what() << std::endl;
            std::exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "Unexpected exception caught" << std::endl;
            std::exit(EXIT_FAILURE);
        }

        switch (boost::apply_visitor(SetupLogging{}, Test::Setup::Fixture::Cfg::Options::get().log))
        {
            case Logger::is_running:
                FREDLOG_DEBUG("test-fred-registry-services configuration successfully loaded");
                break;
            case Logger::is_not_running:
                std::cout << "test-fred-registry-services configuration successfully loaded" << std::endl;
                break;
        }
    }
};

struct GlobalDatabaseFixture : GlobalConfigFixture
{
    GlobalDatabaseFixture()
    {
        Database::emplace_default_manager<Database::StandaloneManager>(
                make_connect_string(Test::Setup::Fixture::Cfg::Options::get().database));
    }
};

struct GlobalRegistryDataFixture : GlobalDatabaseFixture
{
    GlobalRegistryDataFixture()
    {
        LibFred::OperationContextCreator ctx;
        Test::Registrar::init<Test::Registrar::system>(ctx);
        Test::Registrar::init<Test::Registrar::non_system>(ctx);
        LibFred::Contact::PlaceAddress place;
        place.street1 = "Kontaktní 1";
        place.city = "Praha 1";
        place.postalcode = "111 50";
        place.country = "CZ";
        LibFred::ContactAddressList addresses_by_type;
        LibFred::ContactAddress address;
        address.street1 = "Korespondenční 1";
        address.city = "Praha 1";
        address.postalcode = "111 50";
        address.country = "CZ";
        addresses_by_type[LibFred::ContactAddressType::MAILING] = address;
        address.street1 = "Fakturační 1";
        addresses_by_type[LibFred::ContactAddressType::BILLING] = address;
        address.street1 = "Doručovací 1";
        addresses_by_type[LibFred::ContactAddressType::SHIPPING] = address;
        address.street1 = "Doručovací 2";
        addresses_by_type[LibFred::ContactAddressType::SHIPPING_2] = address;
        address.street1 = "Doručovací 3";
        addresses_by_type[LibFred::ContactAddressType::SHIPPING_3] = address;
        Test::Contact::create(
                ctx,
                Test::Setup::Fixture::Cfg::make_unique_handle<Test::Contact::Handle>("TEST-KONTAKT"),
                Test::Registrar::get<Test::Registrar::system>().get_handle(),
                Test::Contact::make_name("Test Kontakt"),
                Test::Contact::make_organization("První Testovací a.s."),
                Test::Contact::make_authinfo("AbCdEfGhIjK1"),
                Test::Contact::make_place_address(place),
                Test::Contact::make_contact_address_list(addresses_by_type),
                Test::Contact::Disclose::name(),
                Test::Contact::Disclose::organization(),
                Test::Contact::Disclose::address(),
                Test::Contact::Hide::telephone(),
                Test::Contact::Hide::fax(),
                Test::Contact::Hide::email(),
                Test::Contact::Hide::notify_email(),
                Test::Contact::Hide::vat_identification_number(),
                Test::Contact::Hide::ident());
        ctx.commit_transaction();
    }
};

struct GlobalFixture : GlobalRegistryDataFixture
{
    GlobalFixture()
    {
    }
};

}//namespace::{anonymous}

BOOST_GLOBAL_FIXTURE(GlobalFixture);
