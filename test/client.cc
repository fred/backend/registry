/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fred_api/registry/contact/service_contact_grpc.pb.h"
#include "fred_api/registry/contact/service_contact_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.grpc.pb.h"

#include <google/protobuf/util/json_util.h>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <stdexcept>

namespace {

std::string to_json(const google::protobuf::Message& msg)
{
    std::string json;
    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;
    options.preserve_proto_field_names = true;
    const auto json_status = google::protobuf::util::MessageToJsonString(msg, &json, options);
    if (json_status.ok())
    {
        return json;
    }
    throw std::runtime_error{"unable convert to json"};
}

int get_domains_by_contact(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Domain::NewStub(channel);
    Fred::Registry::Api::Domain::DomainsByContactRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Domain::DomainsByContactReply reply;
    grpc::ClientContext context;
    const auto reader = stub->get_domains_by_contact(&context, request);
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int get_domains_outzone_notify_info(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Admin::NewStub(channel);
    Fred::Registry::Api::Domain::GetDomainsOutzoneNotifyInfoRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Domain::GetDomainsNotifyInfoReply reply;
    grpc::ClientContext context;
    const auto reader = stub->get_domains_outzone_notify_info(&context, request);
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int update_domains_outzone_additional_notify_info(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Admin::NewStub(channel);
    Fred::Registry::Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Domain::UpdateDomainsAdditionalNotifyInfoReply reply;
    grpc::ClientContext context;
    const auto stream = stub->update_domains_outzone_additional_notify_info(&context, &reply);
    if (!stream->Write(request))
    {
        std::cerr << "write failure: stream broken" << std::endl;
        return EXIT_FAILURE;
    }
    stream->WritesDone();
    const auto status = stream->Finish();
    if (status.ok())
    {
        std::cout << to_json(reply) << std::endl;
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int get_domain_life_cycle_stage(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Domain::NewStub(channel);
    Fred::Registry::Api::Domain::DomainLifeCycleStageRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Domain::DomainLifeCycleStageReply reply;
    grpc::ClientContext context;
    const auto status = stub->get_domain_life_cycle_stage(&context, request, &reply);
    if (status.ok())
    {
        std::cout << to_json(reply) << std::endl;
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int update_contact_state(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Contact::Contact::NewStub(channel);
    Fred::Registry::Api::Contact::UpdateContactStateRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Contact::UpdateContactStateReply reply;
    grpc::ClientContext context;
    const auto status = stub->update_contact_state(&context, request, &reply);
    if (status.ok())
    {
        std::cout << to_json(reply) << std::endl;
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int create_contact(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Contact::Contact::NewStub(channel);
    Fred::Registry::Api::Contact::CreateContactRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Contact::CreateContactReply reply;
    grpc::ClientContext context;
    const auto status = stub->create_contact(&context, request, &reply);
    if (status.ok())
    {
        std::cout << to_json(reply) << std::endl;
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int list_keysets_by_contact(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Keyset::Keyset::NewStub(channel);
    Fred::Registry::Api::Keyset::ListKeysetsByContactRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Keyset::ListKeysetsByContactReply reply;
    grpc::ClientContext context;
    const auto reader = stub->list_keysets_by_contact(&context, request);
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int list_nssets_by_contact(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Nsset::Nsset::NewStub(channel);
    Fred::Registry::Api::Nsset::ListNssetsByContactRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Nsset::ListNssetsByContactReply reply;
    grpc::ClientContext context;
    const auto reader = stub->list_nssets_by_contact(&context, request);
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int batch_domain_info(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Domain::NewStub(channel);
    Fred::Registry::Api::Domain::BatchDomainInfoRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Domain::BatchDomainInfoReply reply;
    grpc::ClientContext context;
    const auto stream = stub->batch_domain_info(&context);
    if (!stream->Write(request))
    {
        std::cerr << "write failure: stream broken" << std::endl;
        return EXIT_FAILURE;
    }
    stream->WritesDone();
    while (stream->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = stream->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int batch_keyset_info(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Keyset::Keyset::NewStub(channel);
    Fred::Registry::Api::Keyset::BatchKeysetInfoRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Keyset::BatchKeysetInfoReply reply;
    grpc::ClientContext context;
    const auto stream = stub->batch_keyset_info(&context);
    if (!stream->Write(request))
    {
        std::cerr << "write failure: stream broken" << std::endl;
        return EXIT_FAILURE;
    }
    stream->WritesDone();
    while (stream->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = stream->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int list_domains_by_keyset(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Domain::NewStub(channel);
    Fred::Registry::Api::Domain::ListDomainsByKeysetRequest request;
    {
        const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
        if (!json_status.ok())
        {
            std::cerr << "not a json: " << json_status.message() << std::endl;
            return EXIT_FAILURE;
        }
    }
    Fred::Registry::Api::Domain::ListDomainsByKeysetReply reply;
    grpc::ClientContext context;
    std::unique_ptr<grpc::ClientReader<Fred::Registry::Api::Domain::ListDomainsByKeysetReply>> reader(
            stub->list_domains_by_keyset(&context, request));
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int list_domains_by_nsset(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Domain::NewStub(channel);
    Fred::Registry::Api::Domain::ListDomainsByNssetRequest request;
    {
        const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
        if (!json_status.ok())
        {
            std::cerr << "not a json: " << json_status.message() << std::endl;
            return EXIT_FAILURE;
        }
    }
    Fred::Registry::Api::Domain::ListDomainsByNssetReply reply;
    grpc::ClientContext context;
    std::unique_ptr<grpc::ClientReader<Fred::Registry::Api::Domain::ListDomainsByNssetReply>> reader(
            stub->list_domains_by_nsset(&context, request));
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int list_domains_by_contact(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Domain::Domain::NewStub(channel);
    Fred::Registry::Api::Domain::ListDomainsByContactRequest request;
    {
        const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
        if (!json_status.ok())
        {
            std::cerr << "not a json: " << json_status.message() << std::endl;
            return EXIT_FAILURE;
        }
    }
    Fred::Registry::Api::Domain::ListDomainsByContactReply reply;
    grpc::ClientContext context;
    std::unique_ptr<grpc::ClientReader<Fred::Registry::Api::Domain::ListDomainsByContactReply>> reader(
            stub->list_domains_by_contact(&context, request));
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int batch_nsset_info(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Nsset::Nsset::NewStub(channel);
    Fred::Registry::Api::Nsset::BatchNssetInfoRequest request;
    {
        const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
        if (!json_status.ok())
        {
            std::cerr << "not a json: " << json_status.message() << std::endl;
            return EXIT_FAILURE;
        }
    }
    Fred::Registry::Api::Nsset::BatchNssetInfoReply reply;
    grpc::ClientContext context;
    const auto stream = stub->batch_nsset_info(&context);
    if (!stream->Write(request))
    {
        std::cerr << "write failure: stream broken" << std::endl;
        return EXIT_FAILURE;
    }
    stream->WritesDone();
    while (stream->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = stream->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int list_merge_candidates(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Contact::Contact::NewStub(channel);
    Fred::Registry::Api::Contact::ListMergeCandidatesRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Contact::ListMergeCandidatesReply reply;
    grpc::ClientContext context;
    std::unique_ptr<grpc::ClientReader<Fred::Registry::Api::Contact::ListMergeCandidatesReply>> reader(
            stub->list_merge_candidates(&context, request));
    while (reader->Read(&reply))
    {
        std::cout << to_json(reply) << std::endl;
    }
    const auto status = reader->Finish();
    if (status.ok())
    {
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

int merge_contacts(const char* endpoint, const char* json_request)
{
    auto channel = grpc::CreateChannel(endpoint, grpc::InsecureChannelCredentials());
    auto stub = Fred::Registry::Api::Contact::Contact::NewStub(channel);
    Fred::Registry::Api::Contact::MergeContactsRequest request;
    const auto json_status = google::protobuf::util::JsonStringToMessage(json_request, &request);
    if (!json_status.ok())
    {
        std::cerr << "not a json: " << json_status.message() << std::endl;
        return EXIT_FAILURE;
    }
    Fred::Registry::Api::Contact::MergeContactsReply reply;
    grpc::ClientContext context;
    const auto status = stub->merge_contacts(&context, request, &reply);
    if (status.ok())
    {
        std::cout << to_json(reply) << std::endl;
        return EXIT_SUCCESS;
    }
    std::cerr << "failure: " << status.error_message() << std::endl;
    return EXIT_FAILURE;
}

using GrpcFnc = int(*)(const char*, const char*);

const std::map<std::string, GrpcFnc> methods = {
        {"get_domains_by_contact", get_domains_by_contact},
        {"get_domains_outzone_notify_info", get_domains_outzone_notify_info},
        {"update_domains_outzone_additional_notify_info", update_domains_outzone_additional_notify_info},
        {"get_domain_life_cycle_stage", get_domain_life_cycle_stage},
        {"update_contact_state", update_contact_state},
        {"create_contact", create_contact},
        {"list_keysets_by_contact", list_keysets_by_contact},
        {"batch_domain_info", batch_domain_info},
        {"batch_keyset_info", batch_keyset_info},
        {"batch_nsset_info", batch_nsset_info},
        {"list_nssets_by_contact", list_nssets_by_contact},
        {"list_domains_by_keyset", list_domains_by_keyset},
        {"list_domains_by_nsset", list_domains_by_nsset},
        {"list_domains_by_contact", list_domains_by_contact},
        {"list_merge_candidates", list_merge_candidates},
        {"merge_contacts", merge_contacts}};

}//namespace {anonymous}

int main(int argc, char* argv[])
{
    if (argc != 4)
    {
        std::cerr << "bad number of arguments; 3 arguments expected\n"
                     "usage: " << argv[0] << " <grpc server endpoint> <method> <request in json format>\n"
                     "    where methods are:";
        std::for_each(cbegin(methods), cend(methods), [](auto&& method_fnc_pair)
        {
            std::cerr << "\n        " << method_fnc_pair.first;
        });
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    char*& endpoint = argv[1];
    char*& requested_method = argv[2];
    char*& json_request = argv[3];
    const auto methods_iter = methods.find(requested_method);
    if (methods_iter == end(methods))
    {
        std::cerr << "unsupported method: " << requested_method << std::endl;
        return EXIT_FAILURE;
    }
    return methods_iter->second(endpoint, json_request);
}
