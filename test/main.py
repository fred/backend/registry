#
# Copyright (C) 2018-2019  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

"""The Python implementation of the gRPC FredAdmin client."""

from __future__ import print_function

import grpc
from google.protobuf import empty_pb2
from google.protobuf import timestamp_pb2
from datetime import datetime

from fred_api.registry.diagnostics import service_diagnostics_grpc_pb2_grpc
from fred_api.registry.diagnostics import about_types_pb2
from fred_api.registry.diagnostics import ping_types_pb2

from fred_api.registry.contact import service_contact_grpc_pb2_grpc
from fred_api.registry.domain import service_domain_grpc_pb2_grpc
from fred_api.registry.keyset import service_keyset_grpc_pb2_grpc
from fred_api.registry.nsset import service_nsset_grpc_pb2_grpc

from fred_api.registry import common_types_pb2

from fred_api.registry.contact import contact_common_types_pb2
from fred_api.registry.contact import contact_history_types_pb2
from fred_api.registry.contact import contact_handle_history_types_pb2
from fred_api.registry.contact import contact_info_types_pb2
# from fred_api.registry.contact import contact_state_flags_types_pb2
from fred_api.registry.contact import contact_state_history_types_pb2
from fred_api.registry.contact import contact_state_types_pb2

from fred_api.registry.domain import domain_common_types_pb2
from fred_api.registry.domain import domain_history_types_pb2
from fred_api.registry.domain import domain_info_types_pb2
# from fred_api.registry.domain import domain_state_flags_types_pb2
from fred_api.registry.domain import domain_state_history_types_pb2
from fred_api.registry.domain import domain_state_types_pb2
from fred_api.registry.domain import fqdn_history_types_pb2

from fred_api.registry.keyset import keyset_common_types_pb2
from fred_api.registry.keyset import keyset_history_types_pb2
from fred_api.registry.keyset import keyset_handle_history_types_pb2
from fred_api.registry.keyset import keyset_info_types_pb2
# from fred_api.registry.keyset import keyset_state_flags_types_pb2
from fred_api.registry.keyset import keyset_state_history_types_pb2
from fred_api.registry.keyset import keyset_state_types_pb2

from fred_api.registry.nsset import nsset_common_types_pb2
from fred_api.registry.nsset import nsset_history_types_pb2
from fred_api.registry.nsset import nsset_handle_history_types_pb2
from fred_api.registry.nsset import nsset_info_types_pb2
# from fred_api.registry.nsset import nsset_state_flags_types_pb2
from fred_api.registry.nsset import nsset_state_history_types_pb2
from fred_api.registry.nsset import nsset_state_types_pb2

from fred_api.registry.registrar import registrar_common_types_pb2
from fred_api.registry.registrar import registrar_info_types_pb2
from fred_api.registry.registrar import registrar_list_types_pb2
from fred_api.registry.registrar import service_registrar_grpc_pb2
from fred_api.registry.registrar import service_registrar_grpc_pb2_grpc


class ProtoException(BaseException):
    def __init__(self, exception):
        self.what = exception


def get_data_or_throw_exception(proto_result):
    if proto_result.HasField('data'):
        return proto_result.data
    raise ProtoException(proto_result.exception)


def print_optional_string(data, field_name, printed_name=None):
    if printed_name is None:
        printed_name = field_name
    value = getattr(data, field_name)
    if not value:
        print('%s:' % printed_name)
    else:
        print('%s: "%s"' % (printed_name, value))


def print_optional_phone_number(data, field_name, printed_name=None):
    if printed_name is None:
        printed_name = field_name
    if data.HasField(field_name):
        value = '"%s"' % getattr(data, field_name).value
    else:
        value = 'none'
    print('%s: %s' % (printed_name, value))


def print_email_addresses(data, field_name, printed_name = None):
    if printed_name is None:
        printed_name = field_name
    value = ''
    for item in getattr(data, field_name):
        value += '\n    "%s"' % item.value
    print('%s: %s' % (printed_name, value))


def print_optional_vat_identification_number(data, field_name, printed_name=None):
    if printed_name is None:
        printed_name = field_name
    if data.HasField(field_name):
        value = '"%s"' % getattr(data, field_name).value
    else:
        value = 'none'
    print('%s: %s' % (printed_name, value))


def print_place_address(data, name):
    print('%s:' % name)
    for street in data.street:
        print('    "%s"' % street)
    if data.state_or_province:
        print('    "%s" "%s", "%s"' % (data.postal_code.value, data.city, data.state_or_province))
    else:
        print('    "%s" "%s"' % (data.postal_code.value, data.city))
    print('    "%s"' % data.country_code.value)


def additional_identifier_to_string_tuple(data):
    if data.HasField('national_identity_number'):
        name = 'national identity number'
        value = '"%s"' % data.national_identity_number.value
    elif data.HasField('national_identity_card'):
        name = 'national identity card'
        value = '"%s"' % data.national_identity_card.value
    elif data.HasField('passport_number'):
        name = 'passport number'
        value = '"%s"' % data.passport_number.value
    elif data.HasField('company_registration_number'):
        name = 'company registration number'
        value = '"%s"' % data.company_registration_number.value
    elif data.HasField('social_security_number'):
        name = 'social security number'
        value = '"%s"' % data.social_security_number.value
    elif data.HasField('birthdate'):
        name = 'birthdate'
        value = '"%s"' % data.passport_number.value
    return (name, value)


def print_optional_additional_identifier(data, field_name, alternative_name='additional identifier'):
    if alternative_name is None:
        alternative_name = field_name
    if data.HasField(field_name):
        printed_name, value = additional_identifier_to_string_tuple(getattr(data, field_name))
    else:
        printed_name, value = (alternative_name, 'none')
    print('%s: %s' % (printed_name, value))


def print_optional_contact_address(data, item, name):
    if not data.HasField(item):
        print('%s: none' % name)
        return
    if item == 'mailing_address':
        data = data.mailing_address
    elif item == 'billing_address':
        data = data.billing_address
    elif item == 'shipping_address1':
        data = data.shipping_address1
    elif item == 'shipping_address2':
        data = data.shipping_address2
    elif item == 'shipping_address3':
        data = data.shipping_address3
    print('%s:' % name)
    if data.HasField('company'):
        print('    "%s"' % data.company)
    for street in data.street:
        print('    "%s"' % street)
    if data.HasField('state_or_province'):
        print('    "%s" "%s", "%s"' % (data.postal_code.value, data.city, data.state_or_province))
    else:
        print('    "%s" "%s"' % (data.postal_code.value, data.city))
    print('    "%s"' % data.country_code.value)


def print_warning_letter_preference(data, name):
    if data.preference == contact_info_types_pb2.WarningLetter.SendingPreference.to_send:
        value = 'to send'
    elif data.preference == contact_info_types_pb2.WarningLetter.SendingPreference.not_to_send:
        value = 'not to send'
    if data.preference == contact_info_types_pb2.WarningLetter.SendingPreference.not_specified:
        value = 'not specified'
    print('%s: %s' % (name, value))


def print_disclose_flags(data, name):
    print('%s:' % name)
    for flag_name, to_publish in sorted(data.items()):
        if to_publish:
            value = 'to show publicly'
        else:
            value = 'not to show publicly'
        print('    %s: %s' % (flag_name, value))


def print_auth_info(data, name):
    print('%s: password = "%s"' % (name, data.password))


def print_state_flags(data, name):
    print('%s:' % name)
    for flag_name, flag_traits in sorted(data.items()):
        if flag_traits.how_to_set == common_types_pb2.StateFlag.Manipulation.automatic:
            value = 'automatic'
        elif flag_traits.how_to_set == common_types_pb2.StateFlag.Manipulation.manual:
            value = 'manual'
        else:
            value = '???'
        if flag_traits.visibility == common_types_pb2.StateFlag.Visibility.external:
            value = value + ' external'
        elif flag_traits.how_to_set == common_types_pb2.StateFlag.Visibility.internal:
            value = value + ' internal'
        else:
            value = value + ' ???'
        print('    %s: %s' % (flag_name, value))


def print_contact_state(data):
    print('    contact_id: %s' % data.contact_id.uuid.value)
    for flag_name, flag_presents in sorted(data.state.flags.items()):
        if flag_presents:
            value = 'presents'
        else:
            value = 'absents'
        print('    %s: %s' % (flag_name, value))


def get_google_timestamp(t):
    return '%s.%09d' % (datetime.utcfromtimestamp(t.seconds).strftime('%Y-%m-%d %H:%M:%S'), t.nanos)


def print_object_events(data, name):
    print('%s:' % name)
    print('    registered at %s by %s' %
          (get_google_timestamp(data.registered.timestamp), data.registered.registrar_handle.value))
    print('    transferred at %s by %s' %
          (get_google_timestamp(data.transferred.timestamp), data.transferred.registrar_handle.value))
    print('    updated at %s by %s' %
          (get_google_timestamp(data.updated.timestamp), data.updated.registrar_handle.value))
    if data.HasField('unregistered'):
        print('    unregistered at %s' % get_google_timestamp(data.unregistered.timestamp))
    else:
        print('    is still registered')


def get_flags_names(flags):
    result = ''
    idx = 0
    for flag_name, flag_presents in sorted(flags.items()):
        result += ' %d:%s' % (idx, flag_name)
        idx += 1
    return result


def get_flags_presence(flags):
    result = ''
    for flag_name, flag_presents in sorted(flags.items()):
        if flag_presents:
            result = result + '*'
        else:
            result = result + '.'
    return result


def print_contact_state_history_record(data):
    print('    from %s: %s' % (get_google_timestamp(data.valid_from), get_flags_presence(data.flags)))


def print_contact_state_history(data):
    print("contact state history:")
    print('    contact_id: %s' % data.contact_id.uuid.value)
    if 0 < len(data.history.timeline):
        print('    flags: {%s }' % get_flags_names(data.history.timeline[0].flags))
        for record in data.history.timeline:
            print_contact_state_history_record(record)
        if data.history.HasField('valid_to'):
            print('    valid to: %s' % get_google_timestamp(data.history.valid_to))


def get_contact_lifetime_timespec(data):
    return '{ history id: %s at %s }' % (data.contact_history_id.uuid.value, get_google_timestamp(data.timestamp))


def print_contact_history_record(data):
    print('    [ contact history id: %s, valid from: %s ]' %
          (data.contact_history_id.uuid.value, get_google_timestamp(data.valid_from)))


def print_contact_history(data):
    print("contact history:")
    print('    contact_id: %s' % data.contact_id.uuid.value)
    for record in data.timeline:
        print_contact_history_record(record)
    if data.HasField('valid_to'):
        print('    valid to: %s' % get_google_timestamp(data.valid_to))
    else:
        print('    valid to: unlimited')


def print_contact_handle_history_record(data):
    if data.HasField('registered_to'):
        end = get_contact_lifetime_timespec(data.registered_to)
    else:
        end = 'unlimited'
    print('    contact id: %s <%s, %s)' %
          (data.contact_id.uuid.value, get_contact_lifetime_timespec(data.registered_from), end))


def print_contact_handle_history(data):
    print("contact handle history:")
    print('handle: %s' % data.contact_handle.value)
    for record in data.timeline:
        print_contact_handle_history_record(record)


def get_contact_info(stub, request):
    if request.HasField('contact_history_id'):
        print('client: get_contact_info(contact_id="%s", contact_history_id="%s")' %
              (request.contact_id.uuid.value, request.contact_history_id.uuid.value))
    else:
        print('client: get_contact_info(contact_id="%s")' % request.contact_id.uuid.value)
    result = stub.get_contact_info(request)
    return get_data_or_throw_exception(result)


def get_contact_state_flags(stub):
    print('client: get_contact_state_flags()')
    request = empty_pb2.Empty()
    result = stub.get_contact_state_flags(request)
    return result.data


def get_contact_state(stub, request):
    print('client: get_contact_state(contact_id="%s")' % request.contact_id.uuid.value)
    result = stub.get_contact_state(request)
    return get_data_or_throw_exception(result)


def get_contact_state_history(stub, request):
    print('client: get_contact_state_history(contact_id="%s", history="%s")' % (request.contact_id.uuid.value,
                                                                                request.history))
    result = stub.get_contact_state_history(request)
    return get_data_or_throw_exception(result)


def get_contact_history(stub, request):
    print('client: get_contact_history(contact_id="%s", history="%s")' % (request.contact_id.uuid.value,
                                                                          request.history))
    result = stub.get_contact_history(request)
    return get_data_or_throw_exception(result)


def get_contact_handle_history(stub, request):
    print('client: get_contact_handle_history(contact_handle="%s")' % request.contact_handle.value)
    result = stub.get_contact_handle_history(request)
    return get_data_or_throw_exception(result)


def print_contact_info(data):
    print('contact_handle: "%s"' % data.contact_handle.value)
    print('contact_id: "%s"' % data.contact_id.uuid.value)
    print('contact_history_id: "%s"' % data.contact_history_id.uuid.value)
    print('name: "%s"' % data.name)
    print_optional_string(data, 'organization')
    print_place_address(data.place, 'place')
    print_optional_phone_number(data, 'telephone')
    print_optional_phone_number(data, 'fax')
    print_email_addresses(data, 'emails')
    print_email_addresses(data, 'notify_emails', 'notify emails')
    print_optional_vat_identification_number(data, 'vat_identification_number')
    print_optional_additional_identifier(data, 'additional_identifier', 'additional identifier')
    print_disclose_flags(data.publish, 'publish')
    print_optional_contact_address(data, 'mailing_address', 'mailing address')
    print_optional_contact_address(data, 'billing_address', 'billing address')
    print_optional_contact_address(data, 'shipping_address1', 'shipping address 1')
    print_optional_contact_address(data, 'shipping_address2', 'shipping address 2')
    print_optional_contact_address(data, 'shipping_address3', 'shipping address 3')
    print_warning_letter_preference(data.warning_letter, 'warning letter preference')
    print_auth_info(data.auth_info, 'authorization information')
    print_object_events(data.events, "events")
    print()


def show_contact_history(stub, contact_lifetime):
    try:
        history_from = contact_common_types_pb2.ContactHistoryInterval.Limit(
            timestamp=contact_lifetime.registered_from.timestamp)
        request = contact_history_types_pb2.ContactHistoryRequest(
            contact_id=contact_lifetime.contact_id,
            history=contact_common_types_pb2.ContactHistoryInterval(lower_limit=history_from))
        result = get_contact_history(stub, request)
        # print("%s" % result)
        print_contact_history(result)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()


def print_optional(data, field_name, printed_name=None):
    if printed_name is None:
        printed_name = field_name
    if data.HasField(field_name):
        value = '"%s"' % getattr(data, field_name).value
    else:
        value = 'none'
    print('%s: %s' % (printed_name, value))


def print_domain_state(data):
    print('    domain_id: %s' % data.domain_id.uuid.value)
    for flag_name, flag_presents in sorted(data.state.flags.items()):
        if flag_presents:
            value = 'presents'
        else:
            value = 'absents'
        print('    %s: %s' % (flag_name, value))


def print_domain_state_history_record(data):
    print('    from %s: %s' % (get_google_timestamp(data.valid_from), get_flags_presence(data.flags)))


def print_domain_state_history(data):
    print("domain state history:")
    print('    domain_id: %s' % data.domain_id.uuid.value)
    if 0 < len(data.history.timeline):
        print('    flags: {%s }' % get_flags_names(data.history.timeline[0].flags))
        for record in data.history.timeline:
            print_domain_state_history_record(record)
        if data.history.HasField('valid_to'):
            print('    valid to: %s' % get_google_timestamp(data.history.valid_to))


def get_domain_lifetime_timespec(data):
    return '{ history id: %s at %s }' % (data.domain_history_id.uuid.value, get_google_timestamp(data.timestamp))


def print_domain_history_record(data):
    print('    [ domain history id: %s, valid from: %s ]' %
          (data.domain_history_id.uuid.value, get_google_timestamp(data.valid_from)))


def print_domain_history(data):
    print("domain history:")
    print('    domain_id: %s' % data.domain_id.uuid.value)
    for record in data.timeline:
        print_domain_history_record(record)
    if data.HasField('valid_to'):
        print('    valid to: %s' % get_google_timestamp(data.valid_to))
    else:
        print('    valid to: unlimited')


def print_fqdn_history_record(data):
    if data.HasField('registered_to'):
        end = get_domain_lifetime_timespec(data.registered_to)
    else:
        end = 'unlimited'
    print('    domain id: %s <%s, %s)' %
          (data.domain_id.uuid.value, get_domain_lifetime_timespec(data.registered_from), end))


def print_fqdn_history(data):
    print("domain handle history:")
    print('handle: %s' % data.fqdn.value)
    for record in data.timeline:
        print_fqdn_history_record(record)


def get_domain_info(stub, request):
    if request.HasField('domain_history_id'):
        print('client: get_domain_info(domain_id="%s", domain_history_id="%s")' %
              (request.domain_id.uuid.value, request.domain_history_id.uuid.value))
    else:
        print('client: get_domain_info(domain_id="%s")' % request.domain_id.uuid.value)
    result = stub.get_domain_info(request)
    return get_data_or_throw_exception(result)


def get_domain_state_flags(stub):
    print('client: get_domain_state_flags()')
    request = empty_pb2.Empty()
    result = stub.get_domain_state_flags(request)
    return result.data


def get_domain_state(stub, request):
    print('client: get_domain_state(domain_id="%s")' % request.domain_id.uuid.value)
    result = stub.get_domain_state(request)
    return get_data_or_throw_exception(result)


def get_domain_state_history(stub, request):
    print('client: get_domain_state_history(domain_id="%s", history="%s")' %
          (request.domain_id.uuid.value, request.history))
    result = stub.get_domain_state_history(request)
    return get_data_or_throw_exception(result)


def get_domain_history(stub, request):
    print('client: get_domain_history(domain_id="%s", history="%s")' %
          (request.domain_id.uuid.value, request.history))
    result = stub.get_domain_history(request)
    return get_data_or_throw_exception(result)


def get_fqdn_history(stub, request):
    print('client: get_fqdn_history(fqdn="%s")' % request.fqdn.value)
    result = stub.get_fqdn_history(request)
    return get_data_or_throw_exception(result)


def print_domain_info(data):
    print('fqdn: "%s"' % data.fqdn.value)
    print('domain_id: "%s"' % data.domain_id.uuid.value)
    print('domain_history_id: "%s"' % data.domain_history_id.uuid.value)
    print_optional(data, 'nsset')
    print_optional(data, 'keyset')
    print('registrant: "%s"' % data.registrant)
    print('sponsoring_registrar: "%s"' % data.sponsoring_registrar)
    print('administrative_contacts: "%s"' % data.administrative_contacts)
    print_optional(data, 'expiration_date')
    print_auth_info(data.auth_info, 'authorization information')
    print_object_events(data.events, "events")
    print()


def show_domain_history(stub, domain_lifetime):
    try:
        history_from = domain_common_types_pb2.DomainHistoryInterval.Limit(
            timestamp=domain_lifetime.registered_from.timestamp)
        request = domain_history_types_pb2.DomainHistoryRequest(
            domain_id=domain_lifetime.domain_id,
            history=domain_common_types_pb2.DomainHistoryInterval(lower_limit=history_from))
        result = get_domain_history(stub, request)
        # print("%s" % result)
        print_domain_history(result)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

def print_keyset_state(data):
    print('    keyset_id: %s' % data.keyset_id.uuid.value)
    for flag_name, flag_presents in sorted(data.state.flags.items()):
        if flag_presents:
            value = 'presents'
        else:
            value = 'absents'
        print('    %s: %s' % (flag_name, value))


def print_keyset_state_history_record(data):
    print('    from %s: %s' % (get_google_timestamp(data.valid_from), get_flags_presence(data.flags)))


def print_keyset_state_history(data):
    print("keyset state history:")
    print('    keyset_id: %s' % data.keyset_id.uuid.value)
    if 0 < len(data.history.timeline):
        print('    flags: {%s }' % get_flags_names(data.history.timeline[0].flags))
        for record in data.history.timeline:
            print_keyset_state_history_record(record)
        if data.history.HasField('valid_to'):
            print('    valid to: %s' % get_google_timestamp(data.history.valid_to))


def get_keyset_lifetime_timespec(data):
    return '{ history id: %s at %s }' % (data.keyset_history_id.uuid.value, get_google_timestamp(data.timestamp))


def print_keyset_history_record(data):
    print('    [ keyset history id: %s, valid from: %s ]' %
          (data.keyset_history_id.uuid.value, get_google_timestamp(data.valid_from)))


def print_keyset_history(data):
    print("keyset history:")
    print('    keyset_id: %s' % data.keyset_id.uuid.value)
    for record in data.timeline:
        print_keyset_history_record(record)
    if data.HasField('valid_to'):
        print('    valid to: %s' % get_google_timestamp(data.valid_to))
    else:
        print('    valid to: unlimited')


def print_keyset_handle_history_record(data):
    if data.HasField('registered_to'):
        end = get_keyset_lifetime_timespec(data.registered_to)
    else:
        end = 'unlimited'
    print('    keyset id: %s <%s, %s)' %
          (data.keyset_id.uuid.value, get_keyset_lifetime_timespec(data.registered_from), end))


def print_keyset_handle_history(data):
    print("keyset handle history:")
    print('handle: %s' % data.keyset_handle.value)
    for record in data.timeline:
        print_keyset_handle_history_record(record)


def get_keyset_info(stub, request):
    if request.HasField('keyset_history_id'):
        print('client: get_keyset_info(keyset_id="%s", keyset_history_id="%s")' %
              (request.keyset_id.uuid.value, request.keyset_history_id.uuid.value))
    else:
        print('client: get_keyset_info(keyset_id="%s")' % request.keyset_id.uuid.value)
    result = stub.get_keyset_info(request)
    return get_data_or_throw_exception(result)


def get_keyset_state_flags(stub):
    print('client: get_keyset_state_flags()')
    request = empty_pb2.Empty()
    result = stub.get_keyset_state_flags(request)
    return result.data


def get_keyset_state(stub, request):
    print('client: get_keyset_state(keyset_id="%s")' % request.keyset_id.uuid.value)
    result = stub.get_keyset_state(request)
    return get_data_or_throw_exception(result)


def get_keyset_state_history(stub, request):
    print('client: get_keyset_state_history(keyset_id="%s", history="%s")' %
          (request.keyset_id.uuid.value, request.history))
    result = stub.get_keyset_state_history(request)
    return get_data_or_throw_exception(result)


def get_keyset_history(stub, request):
    print('client: get_keyset_history(keyset_id="%s", history="%s")' %
          (request.keyset_id.uuid.value, request.history))
    result = stub.get_keyset_history(request)
    return get_data_or_throw_exception(result)


def get_keyset_handle_history(stub, request):
    print('client: get_keyset_handle_history(keyset_handle="%s")' % request.keyset_handle.value)
    result = stub.get_keyset_handle_history(request)
    return get_data_or_throw_exception(result)


def print_keyset_info(data):
    print('handle: "%s"' % data.keyset_handle.value)
    print('keyset_id: "%s"' % data.keyset_id.uuid.value)
    print('keyset_history_id: "%s"' % data.keyset_history_id.uuid.value)
    print('dns_keys: "%s"' % data.dns_keys)
    print('technical_contacts: "%s"' % data.technical_contacts)
    print_auth_info(data.auth_info, 'authorization information')
    print_object_events(data.events, "events")
    print()


def show_keyset_history(stub, keyset_lifetime):
    try:
        history_from = keyset_common_types_pb2.KeysetHistoryInterval.Limit(
            timestamp=keyset_lifetime.registered_from.timestamp)
        request = keyset_history_types_pb2.KeysetHistoryRequest(
            keyset_id=keyset_lifetime.keyset_id,
            history=keyset_common_types_pb2.KeysetHistoryInterval(lower_limit=history_from))
        result = get_keyset_history(stub, request)
        # print("%s" % result)
        print_keyset_history(result)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)


def print_nsset_state(data):
    print('    nsset_id: %s' % data.nsset_id.uuid.value)
    for flag_name, flag_presents in sorted(data.state.flags.items()):
        if flag_presents:
            value = 'presents'
        else:
            value = 'absents'
        print('    %s: %s' % (flag_name, value))


def print_nsset_state_history_record(data):
    print('    from %s: %s' % (get_google_timestamp(data.valid_from), get_flags_presence(data.flags)))


def print_nsset_state_history(data):
    print("nsset state history:")
    print('    nsset_id: %s' % data.nsset_id.uuid.value)
    if 0 < len(data.history.timeline):
        print('    flags: {%s }' % get_flags_names(data.history.timeline[0].flags))
        for record in data.history.timeline:
            print_nsset_state_history_record(record)
        if data.history.HasField('valid_to'):
            print('    valid to: %s' % get_google_timestamp(data.history.valid_to))


def get_nsset_lifetime_timespec(data):
    return '{ history id: %s at %s }' % (data.nsset_history_id.uuid.value, get_google_timestamp(data.timestamp))


def print_nsset_history_record(data):
    print('    [ nsset history id: %s, valid from: %s ]' %
          (data.nsset_history_id.uuid.value, get_google_timestamp(data.valid_from)))


def print_nsset_history(data):
    print("nsset history:")
    print('    nsset_id: %s' % data.nsset_id.uuid.value)
    for record in data.timeline:
        print_nsset_history_record(record)
    if data.HasField('valid_to'):
        print('    valid to: %s' % get_google_timestamp(data.valid_to))
    else:
        print('    valid to: unlimited')


def print_nsset_handle_history_record(data):
    if data.HasField('registered_to'):
        end = get_nsset_lifetime_timespec(data.registered_to)
    else:
        end = 'unlimited'
    print('    nsset id: %s <%s, %s)' %
          (data.nsset_id.uuid.value, get_nsset_lifetime_timespec(data.registered_from), end))


def print_nsset_handle_history(data):
    print("nsset handle history:")
    print('handle: %s' % data.nsset_handle.value)
    for record in data.timeline:
        print_nsset_handle_history_record(record)


def get_nsset_info(stub, request):
    if request.HasField('nsset_history_id'):
        print('client: get_nsset_info(nsset_id="%s", nsset_history_id="%s")' %
              (request.nsset_id.uuid.value, request.nsset_history_id.uuid.value))
    else:
        print('client: get_nsset_info(nsset_id="%s")' % request.nsset_id.uuid.value)
    result = stub.get_nsset_info(request)
    return get_data_or_throw_exception(result)


def get_nsset_state_flags(stub):
    print('client: get_nsset_state_flags()')
    request = empty_pb2.Empty()
    result = stub.get_nsset_state_flags(request)
    return result.data


def get_nsset_state(stub, request):
    print('client: get_nsset_state(nsset_id="%s")' % request.nsset_id.uuid.value)
    result = stub.get_nsset_state(request)
    return get_data_or_throw_exception(result)


def get_nsset_state_history(stub, request):
    print('client: get_nsset_state_history(nsset_id="%s", history="%s")' %
          (request.nsset_id.uuid.value, request.history))
    result = stub.get_nsset_state_history(request)
    return get_data_or_throw_exception(result)


def get_nsset_history(stub, request):
    print('client: get_nsset_history(nsset_id="%s", history="%s")' %
          (request.nsset_id.uuid.value, request.history))
    result = stub.get_nsset_history(request)
    return get_data_or_throw_exception(result)


def get_nsset_handle_history(stub, request):
    print('client: get_nsset_handle_history(nsset_handle="%s")' % request.nsset_handle.value)
    result = stub.get_nsset_handle_history(request)
    return get_data_or_throw_exception(result)


def print_nsset_info(data):
    print('handle: "%s"' % data.nsset_handle.value)
    print('nsset_id: "%s"' % data.nsset_id.uuid.value)
    print('nsset_history_id: "%s"' % data.nsset_history_id.uuid.value)
    print('dns_hosts: "%s"' % data.dns_hosts)
    print('technical_contacts: "%s"' % data.technical_contacts)
    print_auth_info(data.auth_info, 'authorization information')
    print_object_events(data.events, "events")
    print()


def show_nsset_history(stub, nsset_lifetime):
    try:
        history_from = nsset_common_types_pb2.NssetHistoryInterval.Limit(
            timestamp=nsset_lifetime.registered_from.timestamp)
        request = nsset_history_types_pb2.NssetHistoryRequest(
            nsset_id=nsset_lifetime.nsset_id,
            history=nsset_common_types_pb2.NssetHistoryInterval(lower_limit=history_from))
        result = get_nsset_history(stub, request)
        # print("%s" % result)
        print_nsset_history(result)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)


def get_registrar_handles(stub):
    print('client: get_registrar_handles()')
    request = empty_pb2.Empty()
    result = stub.get_registrar_handles(request)
    return result.data


def get_registrar_info(stub, request):
    print('client: get_registrar_info(registrar_handle="%s")' % request.registrar_handle.value)
    result = stub.get_registrar_info(request)
    return get_data_or_throw_exception(result)


def print_registrar_handles(data, name):
    print('%s:' % name)
    for handle in data.registrar_handles:
        print('    "%s"' % handle.value)


def print_optional_url(data, field_name, printed_name = None):
    if printed_name is None:
        printed_name = field_name
    if data.HasField(field_name):
        value = '"%s"' % getattr(data, field_name).value
    else:
        value = 'none'
    print('%s: %s' % (printed_name, value))


def print_bool(data, field_name, printed_name = None):
    if printed_name is None:
        printed_name = field_name
    if getattr(data, field_name):
        value = 'true'
    else:
        value = 'false'
    print('%s: %s' % (printed_name, value))


def print_int(data, field_name, printed_name = None):
    if printed_name is None:
        printed_name = field_name
    print('        %s: %s' % (printed_name, getattr(data, field_name)))


def print_optional_company_registration_number(data, field_name, printed_name = None):
    if printed_name is None:
        printed_name = field_name
    if data.HasField(field_name):
        value = '"%s"' % getattr(data, field_name).value
    else:
        value = 'none'
    print('%s: %s' % (printed_name, value))


def print_optional_vat_identification_number(data, field_name, printed_name = None):
    if printed_name is None:
        printed_name = field_name
    if data.HasField(field_name):
        value = '"%s"' % getattr(data, field_name).value
    else:
        value = 'none'
    print('%s: %s' % (printed_name, value))


def print_registrar_info(data):
    print('registrar handle: "%s"' % data.registrar_handle.value)
    print('name: "%s"' % data.name)
    print_optional_string(data, 'organization')
    print_place_address(data.place, 'place')
    print_optional_phone_number(data, 'telephone')
    print_optional_phone_number(data, 'fax')
    print_email_addresses(data, 'emails')
    print_optional_url(data, 'url')
    print_bool(data, 'is_system_registrar', 'is system registrar')
    print_optional_company_registration_number(data, 'company_registration_number', 'company registration number')
    print_optional_vat_identification_number(data, 'vat_identification_number', 'vat identification number')
    print_optional_string(data, 'variable_symbol')
    print_optional_string(data, 'payment_memo_regex')
    print_bool(data, 'is_vat_payer')
    print()


def print_version(data, desc):
    print('    %s: "%s"' % (desc, data.full))
    print_int(data, 'major')
    print_int(data, 'minor')
    print_int(data, 'patch')
    if data.HasField('rc'):
        print('        rc: %d' % data.rc)
    if data.HasField('commit'):
        print('        distance: %d' % data.commit.distance)
        print('        hash: %s' % data.commit.hash)


def print_about(data):
    print('about:')
    print_version(data.api_version, 'api')
    print_version(data.server_version, 'server')


def print_ping(data):
    if data.HasField('database_is_accessible'):
        if data.database_is_accessible:
            print('    database is accessible')
        else:
            print('    database is inaccessible')


def get_about(stub):
    print('client: about()')
    request = empty_pb2.Empty()
    result = stub.about(request)
    return result.data


def get_ping(stub, check_database_accessibility):
    if check_database_accessibility:
        print('client: ping(check database accessibility)')
    else:
        print('client: ping(do not check database accessibility)')
    request = ping_types_pb2.PingRequest(check_database_accessibility=check_database_accessibility)
    result = stub.ping(request)
    return result.data


def run_diagnostics(channel):
    stub = service_diagnostics_grpc_pb2_grpc.DiagnosticsStub(channel)

    try:
        about_me = get_about(stub)
        print_about(about_me)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    try:
        ping = get_ping(stub, False)
        print_ping(ping)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    try:
        ping = get_ping(stub, True)
        print_ping(ping)
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()


def run_contact(channel):
    stub = service_contact_grpc_pb2_grpc.ContactStub(channel)

    try:
        result = get_contact_state_flags(stub)
#        print("%s" % result)
        print_state_flags(result.traits, "contact state flags")
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    history_from = contact_common_types_pb2.ContactHistoryInterval.Limit(timestamp=timestamp_pb2.Timestamp())
    history_from.timestamp.seconds = 1000000000
    history_from.timestamp.nanos = 987654321
    for contact_uuid in ("412bcbed-ff43-40da-9ef3-bff3d43612a7", "07da957e-962e-4e19-a6ca-8ec7421aca65"):
        for history_uuid in (None, "0a38ffef-21d2-48ee-a805-c78e8574433e"):
            if history_uuid is None:
                request = contact_info_types_pb2.ContactInfoRequest(
                    contact_id=contact_common_types_pb2.ContactId(uuid=common_types_pb2.Uuid(value=contact_uuid)))
            else:
                request = contact_info_types_pb2.ContactInfoRequest(
                    contact_id=contact_common_types_pb2.ContactId(uuid=common_types_pb2.Uuid(value=contact_uuid)),
                    contact_history_id=contact_common_types_pb2.ContactHistoryId(
                        uuid=common_types_pb2.Uuid(value=history_uuid)))
            try:
                result = get_contact_info(stub, request)
                # print("%s" % result)
                print_contact_info(result)
            except ProtoException as e:
                print("ProtoException caught:\n%s" % e.what)
            except BaseException as e:
                print("BaseException caught:\n%s\n" % e)

            if history_uuid is None:
                request = contact_state_types_pb2.ContactStateRequest(
                    contact_id=contact_common_types_pb2.ContactId(uuid=common_types_pb2.Uuid(value=contact_uuid)))
                try:
                    result = get_contact_state(stub, request)
                    # print("%s" % result)
                    print_contact_state(result)
                except ProtoException as e:
                    print("ProtoException caught:\n%s" % e.what)
                except BaseException as e:
                    print("BaseException caught:\n%s\n" % e)
                print()

        try:
            request = contact_state_history_types_pb2.ContactStateHistoryRequest(
                contact_id=contact_common_types_pb2.ContactId(uuid=common_types_pb2.Uuid(value=contact_uuid)),
                history=contact_common_types_pb2.ContactHistoryInterval(lower_limit=history_from))
            result = get_contact_state_history(stub, request)
            # print("%s" % result)
            print_contact_state_history(result)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()

    for contact_handle in ("KONTAKT", "KONTRAKT"):
        try:
            request = contact_handle_history_types_pb2.ContactHandleHistoryRequest(
                contact_handle=contact_common_types_pb2.ContactHandle(value=contact_handle))
            result = get_contact_handle_history(stub, request)
            # print("%s" % result)
            print_contact_handle_history(result)
            for contact_lifetime in result.timeline:
                show_contact_history(stub, contact_lifetime)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()


def run_domain(channel):
    stub = service_domain_grpc_pb2_grpc.DomainStub(channel)

    try:
        result = get_domain_state_flags(stub)
        # print("%s" % result)
        print_state_flags(result.traits, "domain state flags")
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    history_from = domain_common_types_pb2.DomainHistoryInterval.Limit(timestamp=timestamp_pb2.Timestamp())
    history_from.timestamp.seconds = 1000000000
    history_from.timestamp.nanos = 987654321
    for domain_uuid in ("65d7af56-e672-4f9f-8198-01b22697ef97", "65d7af56-e672-4f9f-8198-01b22697ef98"):
        for history_uuid in (None, "5349dc3c-4dc7-443e-bfb4-5460f3eb0bdb"):
            if history_uuid is None:
                request = domain_info_types_pb2.DomainInfoRequest(
                    domain_id=domain_common_types_pb2.DomainId(uuid=common_types_pb2.Uuid(value=domain_uuid)))
            else:
                request = domain_info_types_pb2.DomainInfoRequest(
                    domain_id=domain_common_types_pb2.DomainId(uuid=common_types_pb2.Uuid(value=domain_uuid)),
                    domain_history_id=domain_common_types_pb2.DomainHistoryId(
                        uuid=common_types_pb2.Uuid(value=history_uuid)))
            try:
                result = get_domain_info(stub, request)
                # print("%s" % result)
                print_domain_info(result)
            except ProtoException as e:
                print("ProtoException caught:\n%s" % e.what)
            except BaseException as e:
                print("BaseException caught:\n%s\n" % e)

            if history_uuid is None:
                request = domain_state_types_pb2.DomainStateRequest(
                    domain_id=domain_common_types_pb2.DomainId(uuid=common_types_pb2.Uuid(value=domain_uuid)))
                try:
                    result = get_domain_state(stub, request)
                    # print("%s" % result)
                    print_domain_state(result)
                except ProtoException as e:
                    print("ProtoException caught:\n%s" % e.what)
                except BaseException as e:
                    print("BaseException caught:\n%s\n" % e)
                print()

        try:
            request = domain_state_history_types_pb2.DomainStateHistoryRequest(
                domain_id=domain_common_types_pb2.DomainId(uuid=common_types_pb2.Uuid(value=domain_uuid)),
                history=domain_common_types_pb2.DomainHistoryInterval(lower_limit=history_from))
            result = get_domain_state_history(stub, request)
            # print("%s" % result)
            print_domain_state_history(result)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()

    for fqdn in ("fred.cz", "ferd.cz"):
        try:
            request = fqdn_history_types_pb2.FqdnHistoryRequest(
                fqdn=domain_common_types_pb2.Fqdn(value=fqdn))
            result = get_fqdn_history(stub, request)
            # print("%s" % result)
            print_fqdn_history(result)
            for domain_lifetime in result.timeline:
                show_domain_history(stub, domain_lifetime)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()


def run_keyset(channel):
    stub = service_keyset_grpc_pb2_grpc.KeysetStub(channel)

    try:
        result = get_keyset_state_flags(stub)
        # print("%s" % result)
        print_state_flags(result.traits, "keyset state flags")
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    history_from = keyset_common_types_pb2.KeysetHistoryInterval.Limit(timestamp=timestamp_pb2.Timestamp())
    history_from.timestamp.seconds = 1000000000
    history_from.timestamp.nanos = 987654321
    for keyset_uuid in ("764788f3-bbec-41ac-95fb-66761c9ac26a", "764788f3-bbec-41ac-95fb-66761c9ac26b"):
        for history_uuid in (None, "8c9f047b-7054-4609-93ed-11ab9d11488c"):
            if history_uuid is None:
                request = keyset_info_types_pb2.KeysetInfoRequest(
                    keyset_id=keyset_common_types_pb2.KeysetId(uuid=common_types_pb2.Uuid(value=keyset_uuid)))
            else:
                request = keyset_info_types_pb2.KeysetInfoRequest(
                    keyset_id=keyset_common_types_pb2.KeysetId(uuid=common_types_pb2.Uuid(value=keyset_uuid)),
                    keyset_history_id=keyset_common_types_pb2.KeysetHistoryId(
                        uuid=common_types_pb2.Uuid(value=history_uuid)))
            try:
                result = get_keyset_info(stub, request)
                # print("%s" % result)
                print_keyset_info(result)
            except ProtoException as e:
                print("ProtoException caught:\n%s" % e.what)
            except BaseException as e:
                print("BaseException caught:\n%s\n" % e)

            if history_uuid is None:
                request = keyset_state_types_pb2.KeysetStateRequest(
                    keyset_id=keyset_common_types_pb2.KeysetId(uuid=common_types_pb2.Uuid(value=keyset_uuid)))
                try:
                    result = get_keyset_state(stub, request)
                    # print("%s" % result)
                    print_keyset_state(result)
                except ProtoException as e:
                    print("ProtoException caught:\n%s" % e.what)
                except BaseException as e:
                    print("BaseException caught:\n%s\n" % e)
                print()

        try:
            request = keyset_state_history_types_pb2.KeysetStateHistoryRequest(
                keyset_id=keyset_common_types_pb2.KeysetId(uuid=common_types_pb2.Uuid(value=keyset_uuid)),
                history=keyset_common_types_pb2.KeysetHistoryInterval(lower_limit=history_from))
            result = get_keyset_state_history(stub, request)
            # print("%s" % result)
            print_keyset_state_history(result)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()

    for keyset_handle in ("KEYSID-1", "XSSET-1"):
        try:
            request = keyset_handle_history_types_pb2.KeysetHandleHistoryRequest(
                keyset_handle=keyset_common_types_pb2.KeysetHandle(value=keyset_handle))
            result = get_keyset_handle_history(stub, request)
            # print("%s" % result)
            print_keyset_handle_history(result)
            for keyset_lifetime in result.timeline:
                show_keyset_history(stub, keyset_lifetime)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()


def run_nsset(channel):
    stub = service_nsset_grpc_pb2_grpc.NssetStub(channel)

    try:
        result = get_nsset_state_flags(stub)
        # print("%s" % result)
        print_state_flags(result.traits, "nsset state flags")
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    history_from = nsset_common_types_pb2.NssetHistoryInterval.Limit(timestamp=timestamp_pb2.Timestamp())
    history_from.timestamp.seconds = 1000000000
    history_from.timestamp.nanos = 987654321
    for nsset_uuid in ("8113b262-270d-48e0-9df0-5edc7c26e79c", "8113b262-270d-48e0-9df0-5edc7c26e79d"):
        for history_uuid in (None, "ceb7a168-7dd3-44ad-a5b3-7eceddd50738"):
            if history_uuid is None:
                request = nsset_info_types_pb2.NssetInfoRequest(
                    nsset_id=nsset_common_types_pb2.NssetId(uuid=common_types_pb2.Uuid(value=nsset_uuid)))
            else:
                request = nsset_info_types_pb2.NssetInfoRequest(
                    nsset_id=nsset_common_types_pb2.NssetId(uuid=common_types_pb2.Uuid(value=nsset_uuid)),
                    nsset_history_id=nsset_common_types_pb2.NssetHistoryId(
                        uuid=common_types_pb2.Uuid(value=history_uuid)))
            try:
                result = get_nsset_info(stub, request)
                # print("%s" % result)
                print_nsset_info(result)
            except ProtoException as e:
                print("ProtoException caught:\n%s" % e.what)
            except BaseException as e:
                print("BaseException caught:\n%s\n" % e)

            if history_uuid is None:
                request = nsset_state_types_pb2.NssetStateRequest(
                    nsset_id=nsset_common_types_pb2.NssetId(uuid=common_types_pb2.Uuid(value=nsset_uuid)))
                try:
                    result = get_nsset_state(stub, request)
                    # print("%s" % result)
                    print_nsset_state(result)
                except ProtoException as e:
                    print("ProtoException caught:\n%s" % e.what)
                except BaseException as e:
                    print("BaseException caught:\n%s\n" % e)
                print()

        try:
            request = nsset_state_history_types_pb2.NssetStateHistoryRequest(
                nsset_id=nsset_common_types_pb2.NssetId(uuid=common_types_pb2.Uuid(value=nsset_uuid)),
                history=nsset_common_types_pb2.NssetHistoryInterval(lower_limit=history_from))
            result = get_nsset_state_history(stub, request)
            # print("%s" % result)
            print_nsset_state_history(result)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()

    for nsset_handle in ("NSSET-1", "XSSET-1"):
        try:
            request = nsset_handle_history_types_pb2.NssetHandleHistoryRequest(
                nsset_handle=nsset_common_types_pb2.NssetHandle(value=nsset_handle))
            result = get_nsset_handle_history(stub, request)
            # print("%s" % result)
            print_nsset_handle_history(result)
            for nsset_lifetime in result.timeline:
                show_nsset_history(stub, nsset_lifetime)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()


def run_registrar(channel):
    stub = service_registrar_grpc_pb2_grpc.RegistrarStub(channel)

    try:
        list = get_registrar_handles(stub)
        print_registrar_handles(list, "registrar handles")
    except ProtoException as e:
        print("ProtoException caught:\n%s" % e.what)
    except BaseException as e:
        print("BaseException caught:\n%s\n" % e)
    print()

    for registrar in list.registrar_handles:
        try:
            request = registrar_info_types_pb2.RegistrarInfoRequest(
                registrar_handle=registrar)
            result = get_registrar_info(stub, request)
            print_registrar_info(result)
        except ProtoException as e:
            print("ProtoException caught:\n%s" % e.what)
        except BaseException as e:
            print("BaseException caught:\n%s\n" % e)
        print()


def run(server):
    channel = grpc.insecure_channel(server)
    run_diagnostics(channel)
    run_contact(channel)
    run_domain(channel)
    run_keyset(channel)
    run_nsset(channel)
    run_registrar(channel)


if __name__ == '__main__':
    run('localhost:50051')
