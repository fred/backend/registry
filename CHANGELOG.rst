ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

* Implement ``batch_nsset_info`` in ``Nsset`` service to retrieve more nssets info at once
* Implement ``batch_keyset_info`` in ``Keyset`` service to retrieve more keysets info at once
* Implement ``list_keysets_by_contact`` in ``Keyset`` service to list keysets connected to given contact
* Implement ``list_nssets_by_contact`` in ``Nsset`` service to list nssets connected to given contact
* Add pagination to ``list_domains_by_nsset`` in ``Domain`` service to list domains connected to given nsset
* Implement ``list_domains_by_keyset`` in ``Domain`` service to list domains connected to given keyset
* Implement ``list_domains_by_contact`` in ``Domain`` service to list domains connected to given contact
* Add filter to ``list_nssets`` to optionally list only nssets linked to some domain
* Implement ``batch_domain_info`` in ``Domain`` service to retrieve more domains info at once
* Implement ``list_merge_candidates`` in ``Contact`` service to retrieve list of contacts which can be merged
* Implement ``merge_contacts`` in ``Contact`` service to merge source contacts into target contact

2.23.0 (2024-09-16)
-------------------

* ``fred-auction-warehouse`` calls  PUT request with ``collect_datetime`` attribute (timestamp of domain registration)
* ``fred-auction-warehouse`` calls  POST request with ``auction_id`` attribute (uuid generated in FRED backend)

2.22.0 (2024-09-13)
-------------------

* Implement ``create_contact`` in ``Contact`` service to create new contact object in registry
* Use new ``InvalidData`` exception in ListDomainsByNsset response
* Unimplement ``update_contact``

2.21.0 (2024-07-17)
-------------------

* Add ``list_nssets`` to ``Nsset`` service to get list of registered nssets

2.20.0 (2024-07-03)
-------------------

* Improve ``fred-auction-warehouse``
  * add --finished-before argument
  * logging
  * quick termination on signal

2.19.2 (2024-05-14)
-------------------

* Fix creating poll messages in batch_delete_domains

2.19.1 (2024-04-30)
-------------------

* Fix (freeze) dependencies
* Fix info operations returning internal error

2.19.0 (2024-04-30)
-------------------

* Return more specific return code for database errors
* Improve ``fred-auction-warehouse``
  * logging
  * option `timeout` on connection with auction server
  * process repeatedly (as a service)


2.18.0 (2024-03-11)
-------------------

* Fix get_domain_life_cycle_stage which returned DomainDoesNotExist in case of internal error
* Database issues are reported returning gRPC stats code INTERNAL with description
* Update registrar accepts handle value as an identifier so id was renamed to handle
* Registrar info now returns registrar's UUID
* Add ``DomainBlacklist`` service
* Add ``list_domains`` to ``Domain`` service to get list of registered domains in given zone


2.17.0 (2023-11-30)
-------------------

* Add is_blacklisted to check_domain reply
* Fix info operations which returned that object does not exist if it was updated concurrently
* Implement methods for registrars management:
  * ``add_registrar_certification``
  * ``update_registrar_certification``
  * ``delete_registrar_certification``


2.16.0 (2023-10-17)
-------------------

* Add support for domain auctions

  * Add ``in_auction`` response stage to ``get_domain_life_cycle_stage``


2.15.2 (2023-09-18)
-------------------

* Fix multiple chunk handling in manage_domain_state_flags (requests from last chunk was created)


2.15.1 (2023-07-13)
-------------------

* Fix cancelling of domain state request (domain states were not updated)


2.15.0 (2023-06-29)
-------------------

* Implement ``ContactRepresentative`` service with methods:

  * ``get_contact_representative_info``
  * ``create_contact_representative``
  * ``update_contact_representative``
  * ``delete_contact_representative``


2.14.0 (2023-05-18)
-------------------

* Implement method of ``Contact`` service:

  * ``update_contact_state``

 
2.13.1 (2023-03-29)
-------------------

* Change example configuration to jinja2 template


2.13.0 (2023-02-06)
-------------------

* Add objects light infos

  * Add `ContactLightInfo` class
  * Add `DomainLightInfo` class
  * Add `KeysetLightInfo` class
  * Add `NssetLightInfo` class

* ``get_domains_by_contact`` optionally also returns deleted domains

* Add actual event times to scheduled event times


2.12.1 (2023-01-17)
-------------------

* Implement method for WHOIS/RDAP:

  * ``get_domain_life_cycle_stage``

2.12.0 (2023-01-09)
-------------------

* Add ``validation_expires_at`` into ``DomainInfoReply``

2.11.0 (2022-12-12)
-------------------

* Implement methods for registrars management:

  * ``get_registrar_epp_credentials``
  * ``get_registrar_groups``
  * ``get_registrar_groups_membership``
  * ``get_registrar_certifications``
  * ``create_registrar``
  * ``update_registrar``
  * ``add_registrar_zone_access``
  * ``delete_registrar_zone_access``
  * ``update_registrar_zone_access``
  * ``add_registrar_epp_credentials``
  * ``delete_registrar_epp_credentials``
  * ``update_registrar_epp_credentials``
  * ``add_registrar_group_membership``
  * ``delete_registrar_group_membership``

* Implement method for a host name of domain name server checking:

  * ``check_dns_host``

* Switch to ``libfred@7.0.0`` to support registrars management feature

  * use new ``libfred`` logging

* Use ``registrar.is_internal`` flag


2.10.1 (2022-09-21)
-------------------

* Fix registrar epp auth operations in ``libfred`` (bump deps)


2.10.0 (2022-09-14)
-------------------

* Switch to ``libfred@5.3.0`` to support authinfo with ttl feature


2.9.0 (2022-07-13)
------------------

* Implement methods for additional contact notifications of domain delete warning status:

  * ``get_domains_delete_notify_info``
  * ``update_domains_delete_additional_notify_info``


2.8.0 (2022-06-15)
------------------

* Implement methods for additional contact notifications of domain outzone status:

  * ``get_domains_outzone_notify_info``
  * ``update_domains_outzone_additional_notify_info``


2.7.0 (2022-05-13)
------------------

* Add ``get_domains_by_contact`` method to domain service to get domain list linked with given contact by specified roles
* Add sponsoring registrar handle to info responses for all registrable objects
* Add methods for translation of ``handle`` to ``id`` and ``history_id`` for all registrable objects
* Modify ``manage_domain_state_flags``

  * Make ``history_id`` optional
  * Fix object state flags update procedure


2.6.0 (2022-02-18)
------------------

* Add ``update_contact`` method to contact service
* Add ``get_domains_by_holder`` method to domain service
* Add domain admin service

  * Add ``batch_delete_domains`` method to delete multiple domains
  * Add ``manage_domain_state_flags`` method to create and cancel domain state flag requests

* Add ``LogEntryId`` to registrable objects history records
* Add contact service tests
* Fix transfer and update timestamps for registrable objects
* Replace internal diagnostics api with ``libdiagnostics``
* Improve code formatting
* Update CI
