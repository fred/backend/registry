/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/proto_unwrap.hh"

#include "src/cfg.hh"
#include "src/contact/list_merge_candidates_reply.hh"
#include "src/contact_representative/contact_representative_info.hh"
#include "src/domain/domain_common_types.hh"
#include "src/domain/list_domains_by_contact_reply.hh"
#include "src/domain/list_domains_by_keyset_reply.hh"
#include "src/domain/list_domains_by_nsset_reply.hh"
#include "src/domain_blacklist/block_info.hh"
#include "src/domain_blacklist/create_block.hh"
#include "src/domain_blacklist/delete_block.hh"
#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/keyset/list_keysets_by_contact_reply.hh"
#include "src/nsset/list_nssets.hh"
#include "src/nsset/list_nssets_by_contact_reply.hh"
#include "src/util/deletable.hh"
#include "src/util/into.hh"

#include "util/log/log.hh"

#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <algorithm>
#include <exception>
#include <iterator>
#include <type_traits>
#include <utility>

namespace Fred {
namespace Registry {

template <>
struct ProtoTypeTraits<std::string>
{
    using RegistryType = std::string;
};

template <typename T>
typename ProtoTypeTraits<T>::RegistryType proto_unwrap(const T& src)
{
    static_assert(std::is_same<typename ProtoTypeTraits<T>::RegistryType, T>::value, "no conversion available");
    return src;
}

template <>
struct ProtoTypeTraits<Registry::Api::Uuid>
{
    using RegistryType = boost::uuids::uuid;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactId>
{
    using RegistryType = Contact::ContactId;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactHistoryId>
{
    using RegistryType = Contact::ContactHistoryId;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactHandle>
{
    using RegistryType = Contact::ContactHandle;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactHistoryInterval>
{
    using RegistryType = Contact::ContactHistoryInterval;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainId>
{
    using RegistryType = Domain::DomainId;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainHistoryId>
{
    using RegistryType = Domain::DomainHistoryId;
};

template <>
struct ProtoTypeTraits<Api::Domain::Fqdn>
{
    using RegistryType = Domain::Fqdn;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainHistoryInterval>
{
    using RegistryType = Domain::DomainHistoryInterval;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainStateFlagRequest>
{
    using RegistryType = Domain::DomainStateFlagRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::UpdateDomainsDeleteAdditionalNotifyInfoRequest::DomainAdditionalInfo>
{
    using RegistryType = Domain::UpdateDomainsAdditionalNotifyInfoRequest::DomainAdditionalInfo;
};

template <>
struct ProtoTypeTraits<Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest::DomainAdditionalInfo>
{
    using RegistryType = Domain::UpdateDomainsAdditionalNotifyInfoRequest::DomainAdditionalInfo;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetId>
{
    using RegistryType = Keyset::KeysetId;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetHistoryId>
{
    using RegistryType = Keyset::KeysetHistoryId;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetHandle>
{
    using RegistryType = Keyset::KeysetHandle;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetHistoryInterval>
{
    using RegistryType = Keyset::KeysetHistoryInterval;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetId>
{
    using RegistryType = Nsset::NssetId;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetHistoryId>
{
    using RegistryType = Nsset::NssetHistoryId;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetHandle>
{
    using RegistryType = Nsset::NssetHandle;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetHistoryInterval>
{
    using RegistryType = Nsset::NssetHistoryInterval;
};

template <>
AuthInfo proto_unwrap(const Api::AuthInfo& src)
{
    return Util::make_strong<AuthInfo>(proto_unwrap(src.password()));
}

template <>
boost::uuids::uuid proto_unwrap(const Registry::Api::Uuid& src)
{
    auto uuid = boost::uuids::string_generator()(src.value());
    if (uuid.version() == boost::uuids::uuid::version_unknown)
    {
        throw std::runtime_error("invalid uuid");
    }
    return uuid;
}

template <>
Contact::ContactId proto_unwrap(const Api::Contact::ContactId& src)
{
    return Util::make_strong<Contact::ContactId>(proto_unwrap(src.uuid()));
}

template <>
Contact::ContactHistoryId proto_unwrap(const Api::Contact::ContactHistoryId& src)
{
    return Util::make_strong<Contact::ContactHistoryId>(proto_unwrap(src.uuid()));
}

template <>
Contact::ContactInfoRequest proto_unwrap(const Api::Contact::ContactInfoRequest& src)
{
    Contact::ContactInfoRequest dst;
    dst.contact_id = proto_unwrap(src.contact_id());
    if (src.has_contact_history_id())
    {
        dst.contact_history_id = proto_unwrap(src.contact_history_id());
    }
    return dst;
}

template <>
Contact::ContactHandle proto_unwrap(const Api::Contact::ContactHandle& src)
{
    return Util::make_strong<Contact::ContactHandle>(proto_unwrap(src.value()));
}

template <>
struct ProtoTypeTraits<Api::PaginationRequest>
{
    using RegistryType = PaginationRequest;
};

template <>
struct ProtoTypeTraits<Api::OrderByDirection>
{
    using RegistryType = OrderByDirection;
};

template <>
Contact::ContactIdRequest proto_unwrap(const Api::Contact::ContactIdRequest& src)
{
    Contact::ContactIdRequest dst;
    dst.contact_handle = proto_unwrap(src.contact_handle());
    return dst;
}

namespace {

Contact::ContactHistoryInterval::HistoryId unwrap_contact_history_interval_history_id(const Api::Contact::ContactHistoryId& src)
{
    return Util::make_strong<Contact::ContactHistoryInterval::HistoryId>(
            Util::get_raw_value_from(proto_unwrap(src)));
}

std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> unwrap_system_clock_nanoseconds(
        const google::protobuf::Timestamp& src)
{
    const std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> dst(
            std::chrono::nanoseconds(1'000'000'000 * src.seconds() + src.nanos()));
    return dst;
}

}//namespace Fred::Registry::{anonymous}

template <>
Contact::ContactHistoryInterval proto_unwrap(const Api::Contact::ContactHistoryInterval& src)
{
    auto lower_limit = Contact::ContactHistoryInterval::LowerLimit(Contact::ContactHistoryInterval::NoLimit());
    if (src.has_lower_limit())
    {
        if (src.lower_limit().has_contact_history_id())
        {
            lower_limit.value = unwrap_contact_history_interval_history_id(src.lower_limit().contact_history_id());
        }
        else if (src.lower_limit().has_timestamp())
        {
            lower_limit.value = unwrap_system_clock_nanoseconds(src.lower_limit().timestamp());
        }
    }
    auto upper_limit = Contact::ContactHistoryInterval::UpperLimit(Contact::ContactHistoryInterval::NoLimit());
    if (src.has_upper_limit())
    {
        if (src.upper_limit().has_contact_history_id())
        {
            upper_limit.value = unwrap_contact_history_interval_history_id(src.upper_limit().contact_history_id());
        }
        else if (src.upper_limit().has_timestamp())
        {
            upper_limit.value = unwrap_system_clock_nanoseconds(src.upper_limit().timestamp());
        }
    }
    return Contact::ContactHistoryInterval(lower_limit, upper_limit);
}

namespace {

decltype(auto) get_disclose_flag_operation(bool to_publish)
{
    return to_publish ? Contact::publicly_available_data()
                      : !Contact::publicly_available_data();
}

decltype(auto) unwrap_set_email_addresses(decltype(std::declval<const Api::Contact::UpdateContactRequest&>().set_emails()) src)
{
    Contact::SetEmailAddresses dst;
    dst.values.reserve(src.values().size());
    for (const auto& email : src.values())
    {
        dst.values.emplace_back(Util::make_strong<EmailAddress>(email.value()));
    }
    return dst;
}

decltype(auto) unwrap_set_email_addresses(decltype(std::declval<const Api::Registrar::UpdateRegistrarRequest&>().set_emails()) src)
{
    Registrar::SetEmailAddresses dst;
    dst.values.reserve(src.values().size());
    for (const auto& email : src.values())
    {
        dst.values.emplace_back(Util::make_strong<EmailAddress>(email.value()));
    }
    return dst;
}

template <typename T>
decltype(auto) unwrap_email_addresses_into(decltype(std::declval<const Api::Registrar::CreateRegistrarRequest&>().emails()) src)
{
    std::vector<T> dst;
    dst.reserve(src.size());
    for (const auto& email : src)
    {
        dst.emplace_back(Util::make_strong<T>(email.value()));
    }
    return dst;
}

decltype(auto) unwrap_set_additional_identifier(const Api::Contact::ContactAdditionalIdentifier& src)
{
    if (src.has_national_identity_number())
    {
        return Util::to_set(Contact::ContactAdditionalIdentifier{
                Util::make_strong<Contact::NationalIdentityNumber>(
                        src.national_identity_number().value())});
    }
    if (src.has_national_identity_card())
    {
        return Util::to_set(Contact::ContactAdditionalIdentifier{
                Util::make_strong<Contact::NationalIdentityCard>(
                        src.national_identity_card().value())});
    }
    if (src.has_passport_number())
    {
        return Util::to_set(Contact::ContactAdditionalIdentifier{
                Util::make_strong<Contact::PassportNumber>(
                        src.passport_number().value())});
    }
    if (src.has_company_registration_number())
    {
        return Util::to_set(Contact::ContactAdditionalIdentifier{
                Util::make_strong<CompanyRegistrationNumber>(
                        src.company_registration_number().value())});
    }
    if (src.has_social_security_number())
    {
        return Util::to_set(Contact::ContactAdditionalIdentifier{
                Util::make_strong<Contact::SocialSecurityNumber>(
                        src.social_security_number().value())});
    }
    if (src.has_birthdate())
    {
        return Util::to_set(Contact::ContactAdditionalIdentifier{
                Util::make_strong<Contact::Birthdate>(
                        src.birthdate().value())});
    }
    return Util::to_delete<Contact::ContactAdditionalIdentifier>();
}

template <typename Dst>
Dst proto_unwrap_into(const Api::PlaceAddress& src)
{
    Dst dst;
    dst.street.reserve(src.street().size());
    for (const auto& street : src.street())
    {
        dst.street.emplace_back(street);
    }
    dst.city = src.city();
    dst.state_or_province = src.state_or_province();
    if (src.has_postal_code())
    {
        dst.postal_code = Util::make_strong<decltype(dst.postal_code)>(src.postal_code().value());
    }
    if (src.has_country_code())
    {
        dst.country_code = Util::make_strong<decltype(dst.country_code)>(src.country_code().value());
    }
    return dst;
}

template <typename Dst>
Dst proto_unwrap_into(const Api::PhoneNumber& src)
{
    return Util::make_strong<Dst>(src.value());
}

}//namespace Fred::Registry::{anonymous}

template <>
Contact::ContactAddress proto_unwrap(const Api::Contact::ContactAddress& src)
{
    Contact::ContactAddress dst;
    dst.company = src.company();
    dst.street.reserve(src.street().size());
    for (const auto& street : src.street())
    {
        dst.street.emplace_back(street);
    }
    dst.city = src.city();
    dst.state_or_province = src.state_or_province();
    dst.postal_code = Util::make_strong<PostalCode>(src.postal_code().value());
    dst.country_code = Util::make_strong<CountryCode>(src.country_code().value());
    return dst;
}

template <>
Contact::WarningLetter proto_unwrap(const Api::Contact::WarningLetter& src)
{
    Contact::WarningLetter dst;
    switch (src.preference())
    {
        case Api::Contact::WarningLetter::SendingPreference::to_send:
            dst.preference = Contact::WarningLetter::SendingPreference::to_send;
            return dst;
        case Api::Contact::WarningLetter::SendingPreference::not_to_send:
            dst.preference = Contact::WarningLetter::SendingPreference::not_to_send;
            return dst;
        case Api::Contact::WarningLetter::SendingPreference::not_specified:
            dst.preference = Contact::WarningLetter::SendingPreference::not_specified;
            return dst;
        default:
        {
            struct UnknownPreference : std::exception
            {
                const char* what() const noexcept override { return "unknown preference"; }
            };
            throw UnknownPreference{};
        }
    }
}

template <>
struct ProtoTypeTraits<Api::Registrar::RegistrarHandle>
{
    using RegistryType = Registrar::RegistrarHandle;
};

template <>
Registrar::RegistrarHandle proto_unwrap(const Api::Registrar::RegistrarHandle& src)
{
    return Util::make_strong<Registrar::RegistrarHandle>(proto_unwrap(src.value()));
}

template <>
Contact::UpdateContactRequest proto_unwrap(const Api::Contact::UpdateContactRequest& src)
{
    Contact::UpdateContactRequest dst;
    dst.contact_id = proto_unwrap(src.contact_id());
    dst.contact_history_id = proto_unwrap(src.contact_history_id());
    if (src.SetName_case() == Api::Contact::UpdateContactRequest::kSetName)
    {
        dst.set_name = src.set_name();
    }
    if (src.SetOrganization_case() == Api::Contact::UpdateContactRequest::kSetOrganization)
    {
        dst.set_organization = src.set_organization();
    }
    if (src.has_set_place())
    {
        dst.set_place = proto_unwrap_into<PlaceAddress>(src.set_place());
    }
    if (src.has_set_telephone())
    {
        dst.set_telephone = proto_unwrap_into<PhoneNumber>(src.set_telephone());
    }
    if (src.has_set_fax())
    {
        dst.set_fax = proto_unwrap_into<PhoneNumber>(src.set_fax());
    }
    if (src.has_set_emails())
    {
        dst.set_emails = unwrap_set_email_addresses(src.set_emails());
    }
    if (src.has_set_notify_emails())
    {
        dst.set_notify_emails = unwrap_set_email_addresses(src.set_notify_emails());
    }
    if (src.has_set_vat_identification_number())
    {
        dst.set_vat_identification_number = Util::make_strong<VatIdentificationNumber>(src.set_vat_identification_number().value());
    }
    if (src.SetContactAdditionalIdentifier_case() == Api::Contact::UpdateContactRequest::kSetAdditionalIdentifier)
    {
        dst.set_additional_identifier = unwrap_set_additional_identifier(src.set_additional_identifier());
    }
    if (src.SetMailingAddress_case() == Api::Contact::UpdateContactRequest::kSetMailingAddress)
    {
        dst.set_mailing_address = Util::to_set(proto_unwrap(src.set_mailing_address()));
    }
    else if (src.SetMailingAddress_case() == Api::Contact::UpdateContactRequest::kDeleteMailingAddress)
    {
        dst.set_mailing_address = Util::to_delete<Contact::ContactAddress>();
    }
    if (src.SetBillingAddress_case() == Api::Contact::UpdateContactRequest::kSetBillingAddress)
    {
        dst.set_billing_address = Util::to_set(proto_unwrap(src.set_billing_address()));
    }
    else if (src.SetBillingAddress_case() == Api::Contact::UpdateContactRequest::kDeleteBillingAddress)
    {
        dst.set_billing_address = Util::to_delete<Contact::ContactAddress>();
    }
    if (src.SetShippingAddress1_case() == Api::Contact::UpdateContactRequest::kSetShippingAddress1)
    {
        dst.set_shipping_address[0] = Util::to_set(proto_unwrap(src.set_shipping_address1()));
    }
    else if (src.SetShippingAddress1_case() == Api::Contact::UpdateContactRequest::kDeleteShippingAddress1)
    {
        dst.set_shipping_address[0] = Util::to_delete<Contact::ContactAddress>();
    }
    if (src.SetShippingAddress2_case() == Api::Contact::UpdateContactRequest::kSetShippingAddress2)
    {
        dst.set_shipping_address[1] = Util::to_set(proto_unwrap(src.set_shipping_address2()));
    }
    else if (src.SetShippingAddress2_case() == Api::Contact::UpdateContactRequest::kDeleteShippingAddress2)
    {
        dst.set_shipping_address[1] = Util::to_delete<Contact::ContactAddress>();
    }
    if (src.SetShippingAddress3_case() == Api::Contact::UpdateContactRequest::kSetShippingAddress3)
    {
        dst.set_shipping_address[2] = Util::to_set(proto_unwrap(src.set_shipping_address3()));
    }
    else if (src.SetShippingAddress3_case() == Api::Contact::UpdateContactRequest::kDeleteShippingAddress3)
    {
        dst.set_shipping_address[2] = Util::to_delete<Contact::ContactAddress>();
    }
    if (src.has_set_warning_letter())
    {
        dst.set_warning_letter = proto_unwrap(src.set_warning_letter());
    }
    if (src.has_set_auth_info())
    {
        if (!src.set_auth_info().password().empty())
        {
            dst.set_auth_info = Util::to_set(proto_unwrap(src.set_auth_info()));
        }
        else
        {
            dst.set_auth_info = Util::to_delete<AuthInfo>();
        }
    }
    for (const auto& set_publish : src.set_publish())
    {
        if (set_publish.first == "name")
        {
            dst.set_name_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "organization")
        {
            dst.set_organization_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "place")
        {
            dst.set_place_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "telephone")
        {
            dst.set_telephone_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "fax")
        {
            dst.set_fax_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "emails")
        {
            dst.set_emails_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "notify_emails")
        {
            dst.set_notify_emails_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "vat_identification_number")
        {
            dst.set_vat_identification_number_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else if (set_publish.first == "additional_identifier")
        {
            dst.set_additional_identifier_privacy = get_disclose_flag_operation(set_publish.second);
        }
        else
        {
            struct UnknownDiscloseFlagName : std::exception
            {
                const char* what() const noexcept override { return "unknown disclose flag name"; }
            };
            throw UnknownDiscloseFlagName{};
        }
    }
    if (src.has_log_entry_id())
    {
        dst.log_entry_id = Util::make_strong<LogEntryId>(src.log_entry_id().value());
    }
    const bool registrar_configured = Cfg::Options::get().fred.registrar_originator != boost::none;
    if (!registrar_configured)
    {
        struct NoRegistrarConfigured : std::exception
        {
            const char* what() const noexcept override { return "No registrar-originator configured"; }
        };
        throw NoRegistrarConfigured{};
    }
    dst.registrar_originator = Util::make_strong<Registrar::RegistrarHandle>(*Cfg::Options::get().fred.registrar_originator);
    return dst;
}

template <>
Contact::UpdateContactStateRequest proto_unwrap(const Api::Contact::UpdateContactStateRequest& src)
{
    Contact::UpdateContactStateRequest dst;
    dst.contact_id = proto_unwrap(src.contact_id());
    dst.contact_history_id = proto_unwrap(src.contact_history_id());
    for (const auto& flag_to_set : src.set_flags())
    {
        dst.set_flags[flag_to_set.first] = flag_to_set.second;
    }
    const bool registrar_configured = Cfg::Options::get().fred.registrar_originator != boost::none;
    if (registrar_configured)
    {
        dst.originator = Util::make_strong<Registrar::RegistrarHandle>(*Cfg::Options::get().fred.registrar_originator);
    }
    if (src.has_log_entry_id())
    {
        dst.log_entry_id = Util::make_strong<LogEntryId>(src.log_entry_id().value());
    }
    return dst;
}

template <>
struct ProtoTypeTraits<Api::Contact::ListMergeCandidatesRequest::OrderByField>
{
    using RegistryType = Contact::ListMergeCandidatesRequest::OrderByField;
};

template <>
struct ProtoTypeTraits<Api::Contact::ListMergeCandidatesRequest::OrderBy>
{
    using RegistryType = Contact::ListMergeCandidatesRequest::OrderBy;
};

template <>
Contact::ListMergeCandidatesRequest::OrderByField proto_unwrap(const Api::Contact::ListMergeCandidatesRequest::OrderByField& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_unspecified:
            return Contact::ListMergeCandidatesRequest::OrderByField::unspecified;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_crdate:
            return Contact::ListMergeCandidatesRequest::OrderByField::crdate;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_handle:
            return Contact::ListMergeCandidatesRequest::OrderByField::handle;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_name:
            return Contact::ListMergeCandidatesRequest::OrderByField::name;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_organization:
            return Contact::ListMergeCandidatesRequest::OrderByField::organization;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_domain_count:
            return Contact::ListMergeCandidatesRequest::OrderByField::domain_count;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_nsset_count:
            return Contact::ListMergeCandidatesRequest::OrderByField::nsset_count;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_keyset_count:
            return Contact::ListMergeCandidatesRequest::OrderByField::keyset_count;
        case Api::Contact::ListMergeCandidatesRequest::order_by_field_sponsoring_registrar_handle:
            return Contact::ListMergeCandidatesRequest::OrderByField::sponsoring_registrar_handle;
    }
    return Contact::ListMergeCandidatesRequest::OrderByField::unspecified;
}

template <>
OrderByDirection proto_unwrap(const Api::OrderByDirection& src)
{
    switch (static_cast<int>(src))
    {
        case Api::order_by_direction_ascending:
            return OrderByDirection::ascending;
        case Api::order_by_direction_descending:
            return OrderByDirection::descending;
    }
    return OrderByDirection::ascending;
}

template <>
PaginationRequest proto_unwrap(const Api::PaginationRequest& src)
{
    PaginationRequest dst;
    dst.page_size = src.page_size();
    dst.page_token = proto_unwrap(src.page_token());
    return dst;
}

template <>
Contact::ListMergeCandidatesRequest::OrderBy proto_unwrap(const Api::Contact::ListMergeCandidatesRequest::OrderBy& src)
{
    return Contact::ListMergeCandidatesRequest::OrderBy{
            proto_unwrap(src.field()),
            proto_unwrap(src.direction())};
}

template <>
Contact::ListMergeCandidatesRequest proto_unwrap(const Api::Contact::ListMergeCandidatesRequest& src)
{
    Contact::ListMergeCandidatesRequest dst;

    try
    {
        dst.contact_id = proto_unwrap(src.contact_id());
    }
    catch (...)
    {
        throw Contact::ListMergeCandidatesReply::Exception::InvalidData(dst, {"contact_id"});
    }
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by.reserve(src.order_by().size());
    std::for_each(src.order_by().begin(), src.order_by().end(), [&dst](auto&& src_item)
    {
        dst.order_by.push_back(proto_unwrap(src_item));
    });
    return dst;
}

template <>
Contact::MergeContactsRequest proto_unwrap(const Api::Contact::MergeContactsRequest& src)
{
    Contact::MergeContactsRequest dst;
    dst.target_contact_id = proto_unwrap(src.target_contact_id());
    dst.source_contact_ids.reserve(src.source_contact_ids().size());
    std::for_each(src.source_contact_ids().begin(), src.source_contact_ids().end(), [&dst](auto&& src_item)
    {
        dst.source_contact_ids.push_back(proto_unwrap(src_item));
    });
    if (src.has_log_entry_id())
    {
        dst.log_entry_id = Util::make_strong<LogEntryId>(src.log_entry_id().value());
    }
    return dst;
}

template <>
Contact::SearchContactRequest proto_unwrap(const Api::Contact::SearchContactRequest& src)
{
    Contact::SearchContactRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Contact::SearchContactRequest::LIMIT_NOT_SET)
    {
        dst.limit = src.limit();
    }
    else
    {
        dst.limit = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Contact::ContactItem::contact_handle))
        {
            dst.searched_items.insert(Contact::ContactItem::contact_handle);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::name))
        {
            dst.searched_items.insert(Contact::ContactItem::name);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::organization))
        {
            dst.searched_items.insert(Contact::ContactItem::organization);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_street))
        {
            dst.searched_items.insert(Contact::ContactItem::place_street);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_city))
        {
            dst.searched_items.insert(Contact::ContactItem::place_city);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_state_or_province))
        {
            dst.searched_items.insert(Contact::ContactItem::place_state_or_province);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_postal_code))
        {
            dst.searched_items.insert(Contact::ContactItem::place_postal_code);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::telephone))
        {
            dst.searched_items.insert(Contact::ContactItem::telephone);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::fax))
        {
            dst.searched_items.insert(Contact::ContactItem::fax);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::email))
        {
            dst.searched_items.insert(Contact::ContactItem::email);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::notify_email))
        {
            dst.searched_items.insert(Contact::ContactItem::notify_email);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::vat_identification_number))
        {
            dst.searched_items.insert(Contact::ContactItem::vat_identification_number);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::additional_identifier))
        {
            dst.searched_items.insert(Contact::ContactItem::additional_identifier);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_company))
        {
            dst.searched_items.insert(Contact::ContactItem::address_company);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_street))
        {
            dst.searched_items.insert(Contact::ContactItem::address_street);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_city))
        {
            dst.searched_items.insert(Contact::ContactItem::address_city);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_state_or_province))
        {
            dst.searched_items.insert(Contact::ContactItem::address_state_or_province);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_postal_code))
        {
            dst.searched_items.insert(Contact::ContactItem::address_postal_code);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchContactRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items.insert(Contact::ContactItem::contact_handle);
        dst.searched_items.insert(Contact::ContactItem::name);
        dst.searched_items.insert(Contact::ContactItem::organization);
        dst.searched_items.insert(Contact::ContactItem::place_street);
        dst.searched_items.insert(Contact::ContactItem::place_city);
        dst.searched_items.insert(Contact::ContactItem::place_state_or_province);
        dst.searched_items.insert(Contact::ContactItem::place_postal_code);
        dst.searched_items.insert(Contact::ContactItem::telephone);
        dst.searched_items.insert(Contact::ContactItem::fax);
        dst.searched_items.insert(Contact::ContactItem::email);
        dst.searched_items.insert(Contact::ContactItem::notify_email);
        dst.searched_items.insert(Contact::ContactItem::vat_identification_number);
        dst.searched_items.insert(Contact::ContactItem::additional_identifier);
        dst.searched_items.insert(Contact::ContactItem::address_company);
        dst.searched_items.insert(Contact::ContactItem::address_street);
        dst.searched_items.insert(Contact::ContactItem::address_city);
        dst.searched_items.insert(Contact::ContactItem::address_state_or_province);
        dst.searched_items.insert(Contact::ContactItem::address_postal_code);
    }
    return dst;
}

template <>
Contact::SearchContactHistoryRequest proto_unwrap(const Api::Contact::SearchContactHistoryRequest& src)
{
    Contact::SearchContactHistoryRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Contact::SearchContactHistoryRequest::LIMIT_NOT_SET)
    {
        dst.max_number_of_contacts = src.max_number_of_contacts();
    }
    else
    {
        dst.max_number_of_contacts = boost::none;
    }
    if (src.has_data_valid_from())
    {
        dst.data_valid_from = unwrap_system_clock_nanoseconds(src.data_valid_from());
    }
    else
    {
        dst.data_valid_from = boost::none;
    }
    if (src.has_data_valid_to())
    {
        dst.data_valid_to = unwrap_system_clock_nanoseconds(src.data_valid_to());
    }
    else
    {
        dst.data_valid_to = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Contact::ContactItem::contact_handle))
        {
            dst.searched_items.insert(Contact::ContactItem::contact_handle);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::name))
        {
            dst.searched_items.insert(Contact::ContactItem::name);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::organization))
        {
            dst.searched_items.insert(Contact::ContactItem::organization);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_street))
        {
            dst.searched_items.insert(Contact::ContactItem::place_street);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_city))
        {
            dst.searched_items.insert(Contact::ContactItem::place_city);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_state_or_province))
        {
            dst.searched_items.insert(Contact::ContactItem::place_state_or_province);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::place_postal_code))
        {
            dst.searched_items.insert(Contact::ContactItem::place_postal_code);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::telephone))
        {
            dst.searched_items.insert(Contact::ContactItem::telephone);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::fax))
        {
            dst.searched_items.insert(Contact::ContactItem::fax);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::email))
        {
            dst.searched_items.insert(Contact::ContactItem::email);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::notify_email))
        {
            dst.searched_items.insert(Contact::ContactItem::notify_email);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::vat_identification_number))
        {
            dst.searched_items.insert(Contact::ContactItem::vat_identification_number);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::additional_identifier))
        {
            dst.searched_items.insert(Contact::ContactItem::additional_identifier);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_company))
        {
            dst.searched_items.insert(Contact::ContactItem::address_company);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_street))
        {
            dst.searched_items.insert(Contact::ContactItem::address_street);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_city))
        {
            dst.searched_items.insert(Contact::ContactItem::address_city);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_state_or_province))
        {
            dst.searched_items.insert(Contact::ContactItem::address_state_or_province);
        }
        else if (item == Util::Into<std::string>::from(Contact::ContactItem::address_postal_code))
        {
            dst.searched_items.insert(Contact::ContactItem::address_postal_code);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchContactHistoryRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items.insert(Contact::ContactItem::contact_handle);
        dst.searched_items.insert(Contact::ContactItem::name);
        dst.searched_items.insert(Contact::ContactItem::organization);
        dst.searched_items.insert(Contact::ContactItem::place_street);
        dst.searched_items.insert(Contact::ContactItem::place_city);
        dst.searched_items.insert(Contact::ContactItem::place_state_or_province);
        dst.searched_items.insert(Contact::ContactItem::place_postal_code);
        dst.searched_items.insert(Contact::ContactItem::telephone);
        dst.searched_items.insert(Contact::ContactItem::fax);
        dst.searched_items.insert(Contact::ContactItem::email);
        dst.searched_items.insert(Contact::ContactItem::notify_email);
        dst.searched_items.insert(Contact::ContactItem::vat_identification_number);
        dst.searched_items.insert(Contact::ContactItem::additional_identifier);
        dst.searched_items.insert(Contact::ContactItem::address_company);
        dst.searched_items.insert(Contact::ContactItem::address_street);
        dst.searched_items.insert(Contact::ContactItem::address_city);
        dst.searched_items.insert(Contact::ContactItem::address_state_or_province);
        dst.searched_items.insert(Contact::ContactItem::address_postal_code);
    }
    return dst;
}

template <>
Contact::ContactDataHistoryRequest proto_unwrap(const Api::Contact::ContactHistoryRequest& src)
{
    return Contact::ContactDataHistoryRequest(proto_unwrap(src.contact_id()),
                                              proto_unwrap(src.history()));
}

namespace {

template <typename T, typename F, typename N>
Contact::PrivacyControlled<std::decay_t<T>> make_privacy_controlled(T&& value, F& discloseflags, N&& name)
{
    const auto discloseflag_iter = discloseflags.find(name);
    if (discloseflag_iter != end(discloseflags))
    {
        const bool to_disclose = discloseflag_iter->second;
        discloseflags.erase(discloseflag_iter);
        if (to_disclose)
        {
            return Contact::make_public_data(std::forward<T>(value));
        }
        return Contact::make_non_public_data(std::forward<T>(value));
    }
    return Contact::make_default_publicity_data(std::forward<T>(value));
}

template <typename T, typename F, typename N>
auto make_privacy_controlled_optional(
        bool present,
        T&& get_value,
        F& discloseflags,
        N&& name) -> Contact::PrivacyControlledOptional<std::decay_t<decltype(get_value())>>
{
    using DataType = std::decay_t<decltype(get_value())>;
    const auto discloseflag_iter = discloseflags.find(name);
    if (discloseflag_iter != end(discloseflags))
    {
        const bool to_disclose = discloseflag_iter->second;
        discloseflags.erase(discloseflag_iter);
        if (to_disclose)
        {
            if (present)
            {
                return Contact::make_public_data(boost::make_optional(get_value()));
            }
            return Contact::make_public_data(boost::optional<DataType>{});
        }
        if (present)
        {
            return Contact::make_non_public_data(boost::make_optional(get_value()));
        }
        return Contact::make_non_public_data(boost::optional<DataType>{});
    }
    if (present)
    {
        return Contact::make_default_publicity_data(boost::make_optional(get_value()));
    }
    return Contact::make_default_publicity_data(boost::optional<DataType>{});
}

Contact::ContactAdditionalIdentifier unwrap_additional_identifier(const Api::Contact::ContactAdditionalIdentifier& src)
{
    if (src.has_national_identity_number())
    {
        return Util::make_strong<Contact::NationalIdentityNumber>(src.national_identity_number().value());
    }
    if (src.has_national_identity_card())
    {
        return Util::make_strong<Contact::NationalIdentityCard>(src.national_identity_card().value());
    }
    if (src.has_passport_number())
    {
        return Util::make_strong<Contact::PassportNumber>(src.passport_number().value());
    }
    if (src.has_company_registration_number())
    {
        return Util::make_strong<CompanyRegistrationNumber>(src.company_registration_number().value());
    }
    if (src.has_social_security_number())
    {
        return Util::make_strong<Contact::SocialSecurityNumber>(src.social_security_number().value());
    }
    if (src.has_birthdate())
    {
        return Util::make_strong<Contact::Birthdate>(src.birthdate().value());
    }
    throw std::runtime_error{"no variant of ContactAdditionalIdentifier"};
}

}//namespace Fred::Registry::{anonymous}

template <>
Contact::CreateContactRequest proto_unwrap(const Api::Contact::CreateContactRequest& src)
{
    std::map<std::string, bool> discloseflags = {};
    std::transform(begin(src.disclose()), end(src.disclose()), std::inserter(discloseflags, end(discloseflags)), [](auto&& item)
    {
        return std::make_pair(item.first, item.second);
    });
    auto result = Contact::CreateContactRequest{
            proto_unwrap(src.contact_handle()),
            make_privacy_controlled(proto_unwrap(src.name()), discloseflags, "name"),
            make_privacy_controlled(proto_unwrap(src.organization()), discloseflags, "organization"),
            make_privacy_controlled(proto_unwrap_into<PlaceAddress>(src.place()), discloseflags, "place"),
            make_privacy_controlled_optional(
                    src.has_telephone(),
                    [&src]() { return proto_unwrap_into<PhoneNumber>(src.telephone()); },
                    discloseflags,
                    "telephone"),
            make_privacy_controlled_optional(
                    src.has_fax(),
                    [&src]() { return proto_unwrap_into<PhoneNumber>(src.fax()); },
                    discloseflags,
                    "fax"),
            make_privacy_controlled(unwrap_email_addresses_into<EmailAddress>(src.emails()), discloseflags, "emails"),
            make_privacy_controlled(unwrap_email_addresses_into<EmailAddress>(src.notify_emails()), discloseflags, "notify_emails"),
            make_privacy_controlled_optional(
                    src.has_vat_identification_number(),
                    [&src]()
                    {
                        return Util::make_strong<VatIdentificationNumber>(
                                src.vat_identification_number().value());
                    },
                    discloseflags,
                    "vat_identification_number"),
            make_privacy_controlled_optional(
                    src.has_additional_identifier(),
                    [&src]() { return unwrap_additional_identifier(src.additional_identifier()); },
                    discloseflags,
                    "additional_identifier"),
            src.has_mailing_address() ? boost::make_optional(proto_unwrap(src.mailing_address()))
                                      : boost::none,
            src.has_billing_address() ? boost::make_optional(proto_unwrap(src.billing_address()))
                                      : boost::none,
            {
                src.has_shipping_address1() ? boost::make_optional(proto_unwrap(src.shipping_address1()))
                                            : boost::none,
                src.has_shipping_address2() ? boost::make_optional(proto_unwrap(src.shipping_address2()))
                                            : boost::none,
                src.has_shipping_address3() ? boost::make_optional(proto_unwrap(src.shipping_address3()))
                                            : boost::none
            },
            src.has_warning_letter() ? proto_unwrap(src.warning_letter())
                                     : []()
                                       {
                                            Contact::WarningLetter value;
                                            value.preference = Contact::WarningLetter::SendingPreference::not_specified;
                                            return value;
                                       }(),
            src.has_log_entry_id() ? boost::make_optional(Util::make_strong<LogEntryId>(src.log_entry_id().value()))
                                   : boost::none,
            proto_unwrap(src.by_registrar())};
    std::transform(
            begin(discloseflags),
            end(discloseflags),
            std::inserter(result.unused_discloseflags, end(result.unused_discloseflags)),
            [](auto&& item) { return item.first; });
    return result;
}

template <>
Contact::ContactHandleHistoryRequest proto_unwrap(const Api::Contact::ContactHandleHistoryRequest& src)
{
    return Contact::ContactHandleHistoryRequest(proto_unwrap(src.contact_handle()));
}

template <>
Contact::ContactStateRequest proto_unwrap(const Api::Contact::ContactStateRequest& src)
{
    Contact::ContactStateRequest dst;
    dst.contact_id = proto_unwrap(src.contact_id());
    return dst;
}

template <>
Contact::ContactStateHistoryRequest proto_unwrap(const Api::Contact::ContactStateHistoryRequest& src)
{
    return Contact::ContactStateHistoryRequest(proto_unwrap(src.contact_id()), proto_unwrap(src.history()));
}

template <>
Keyset::KeysetId proto_unwrap(const Api::Keyset::KeysetId& src)
{
    return Util::make_strong<Keyset::KeysetId>(proto_unwrap(src.uuid()));
}

template <>
Keyset::KeysetHistoryId proto_unwrap(const Api::Keyset::KeysetHistoryId& src)
{
    return Util::make_strong<Keyset::KeysetHistoryId>(proto_unwrap(src.uuid()));
}

template <>
Keyset::KeysetInfoRequest proto_unwrap(const Api::Keyset::KeysetInfoRequest& src)
{
    Keyset::KeysetInfoRequest dst;
    dst.keyset_id = proto_unwrap(src.keyset_id());
    if (src.has_keyset_history_id())
    {
        dst.keyset_history_id = proto_unwrap(src.keyset_history_id());
    }
    dst.include_domains_count = src.include_domains_count();
    return dst;
}

void proto_unwrap_back(const Api::Keyset::BatchKeysetInfoRequest& src, Keyset::BatchKeysetInfoRequest& dst)
{
    dst.requests.reserve(dst.requests.size() + src.requests().size());
    std::transform(src.requests().cbegin(), src.requests().cend(), std::back_inserter(dst.requests), [](auto&& src)
    {
        Keyset::BatchKeysetInfoRequest::Request dst;
        dst.keyset_id = src.keyset_id().uuid().value();
        if (src.has_keyset_history_id())
        {
            dst.keyset_history_id = src.keyset_history_id().uuid().value();
        }
        dst.include_domains_count = src.include_domains_count();
        return dst;
    });
}

template <>
Keyset::KeysetHandle proto_unwrap(const Api::Keyset::KeysetHandle& src)
{
    return Util::make_strong<Keyset::KeysetHandle>(proto_unwrap(src.value()));
}

template <>
Keyset::KeysetIdRequest proto_unwrap(const Api::Keyset::KeysetIdRequest& src)
{
    Keyset::KeysetIdRequest dst;
    dst.keyset_handle = proto_unwrap(src.keyset_handle());
    return dst;
}

namespace {

Keyset::KeysetHistoryInterval::HistoryId unwrap_keyset_history_interval_history_id(const Api::Keyset::KeysetHistoryId& src)
{
    return Util::make_strong<Keyset::KeysetHistoryInterval::HistoryId>(
            Util::get_raw_value_from(proto_unwrap(src)));
}

}//namespace Fred::Registry::{anonymous}

template <>
Keyset::KeysetHistoryInterval proto_unwrap(const Api::Keyset::KeysetHistoryInterval& src)
{
    auto lower_limit = Keyset::KeysetHistoryInterval::LowerLimit(Keyset::KeysetHistoryInterval::NoLimit());
    if (src.has_lower_limit())
    {
        if (src.lower_limit().has_keyset_history_id())
        {
            lower_limit.value = unwrap_keyset_history_interval_history_id(src.lower_limit().keyset_history_id());
        }
        else if (src.lower_limit().has_timestamp())
        {
            lower_limit.value = unwrap_system_clock_nanoseconds(src.lower_limit().timestamp());
        }
    }
    auto upper_limit = Keyset::KeysetHistoryInterval::UpperLimit(Keyset::KeysetHistoryInterval::NoLimit());
    if (src.has_upper_limit())
    {
        if (src.upper_limit().has_keyset_history_id())
        {
            upper_limit.value = unwrap_keyset_history_interval_history_id(src.upper_limit().keyset_history_id());
        }
        else if (src.upper_limit().has_timestamp())
        {
            upper_limit.value = unwrap_system_clock_nanoseconds(src.upper_limit().timestamp());
        }
    }
    return Keyset::KeysetHistoryInterval(lower_limit, upper_limit);
}

template <>
Keyset::SearchKeysetRequest proto_unwrap(const Api::Keyset::SearchKeysetRequest& src)
{
    Keyset::SearchKeysetRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Keyset::SearchKeysetRequest::LIMIT_NOT_SET)
    {
        dst.limit = src.limit();
    }
    else
    {
        dst.limit = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Keyset::KeysetItem::keyset_handle))
        {
            dst.searched_items.insert(Keyset::KeysetItem::keyset_handle);
        }
        else if (item == Util::Into<std::string>::from(Keyset::KeysetItem::public_key))
        {
            dst.searched_items.insert(Keyset::KeysetItem::public_key);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchKeysetRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items = { Keyset::KeysetItem::keyset_handle,
                               Keyset::KeysetItem::public_key };
    }
    return dst;
}

template <>
Keyset::SearchKeysetHistoryRequest proto_unwrap(const Api::Keyset::SearchKeysetHistoryRequest& src)
{
    Keyset::SearchKeysetHistoryRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Keyset::SearchKeysetHistoryRequest::LIMIT_NOT_SET)
    {
        dst.max_number_of_keysets = src.max_number_of_keysets();
    }
    else
    {
        dst.max_number_of_keysets = boost::none;
    }
    if (src.has_data_valid_from())
    {
        dst.data_valid_from = unwrap_system_clock_nanoseconds(src.data_valid_from());
    }
    else
    {
        dst.data_valid_from = boost::none;
    }
    if (src.has_data_valid_to())
    {
        dst.data_valid_to = unwrap_system_clock_nanoseconds(src.data_valid_to());
    }
    else
    {
        dst.data_valid_to = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Keyset::KeysetItem::keyset_handle))
        {
            dst.searched_items.insert(Keyset::KeysetItem::keyset_handle);
        }
        else if (item == Util::Into<std::string>::from(Keyset::KeysetItem::public_key))
        {
            dst.searched_items.insert(Keyset::KeysetItem::public_key);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchKeysetHistoryRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items = { Keyset::KeysetItem::keyset_handle,
                               Keyset::KeysetItem::public_key };
    }
    return dst;
}

template <>
Keyset::KeysetDataHistoryRequest proto_unwrap(const Api::Keyset::KeysetHistoryRequest& src)
{
    return Keyset::KeysetDataHistoryRequest(
            proto_unwrap(src.keyset_id()),
            proto_unwrap(src.history()));
}

template <>
Keyset::KeysetHandleHistoryRequest proto_unwrap(const Api::Keyset::KeysetHandleHistoryRequest& src)
{
    return Keyset::KeysetHandleHistoryRequest(proto_unwrap(src.keyset_handle()));
}

template <>
struct ProtoTypeTraits<Api::Keyset::ListKeysetsByContactRequest::OrderByField>
{
    using RegistryType = Keyset::ListKeysetsByContactRequest::OrderByField;
};

template <>
struct ProtoTypeTraits<Api::Keyset::ListKeysetsByContactRequest::OrderBy>
{
    using RegistryType = Keyset::ListKeysetsByContactRequest::OrderBy;
};

template <>
Keyset::ListKeysetsByContactRequest::OrderByField proto_unwrap(const Api::Keyset::ListKeysetsByContactRequest::OrderByField& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Keyset::ListKeysetsByContactRequest::order_by_field_unspecified:
            return Keyset::ListKeysetsByContactRequest::OrderByField::unspecified;
        case Api::Keyset::ListKeysetsByContactRequest::order_by_field_crdate:
            return Keyset::ListKeysetsByContactRequest::OrderByField::crdate;
        case Api::Keyset::ListKeysetsByContactRequest::order_by_field_handle:
            return Keyset::ListKeysetsByContactRequest::OrderByField::handle;
        case Api::Keyset::ListKeysetsByContactRequest::order_by_field_domains_count:
            return Keyset::ListKeysetsByContactRequest::OrderByField::domains_count;
        case Api::Keyset::ListKeysetsByContactRequest::order_by_field_sponsoring_registrar_handle:
            return Keyset::ListKeysetsByContactRequest::OrderByField::sponsoring_registrar_handle;
    }
    return Keyset::ListKeysetsByContactRequest::OrderByField::unspecified;
}

template <>
Keyset::ListKeysetsByContactRequest::OrderBy proto_unwrap(const Api::Keyset::ListKeysetsByContactRequest::OrderBy& src)
{
    return Keyset::ListKeysetsByContactRequest::OrderBy{
            proto_unwrap(src.field()),
            proto_unwrap(src.direction())};
}

template <>
Keyset::ListKeysetsByContactRequest proto_unwrap(const Api::Keyset::ListKeysetsByContactRequest& src)
{
    Keyset::ListKeysetsByContactRequest dst;
    boost::optional<Api::InvalidData> exception;
    try
    {
        dst.contact_id = proto_unwrap(src.contact_id());
    }
    catch (...)
    {
        throw Keyset::ListKeysetsByContactReply::Exception::InvalidData{dst, {"contact_id"}};
    }
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by.reserve(src.order_by().size());
    std::for_each(src.order_by().begin(), src.order_by().end(), [&dst](auto&& src_item)
    {
        dst.order_by.push_back(proto_unwrap(src_item));
    });
    dst.aggregate_entire_history = src.aggregate_entire_history();
    return dst;
}

template <>
Keyset::KeysetStateRequest proto_unwrap(const Api::Keyset::KeysetStateRequest& src)
{
    Keyset::KeysetStateRequest dst;
    dst.keyset_id = proto_unwrap(src.keyset_id());
    return dst;
}

template <>
Keyset::KeysetStateHistoryRequest proto_unwrap(const Api::Keyset::KeysetStateHistoryRequest& src)
{
    return Keyset::KeysetStateHistoryRequest(proto_unwrap(src.keyset_id()), proto_unwrap(src.history()));
}

template <>
Nsset::NssetId proto_unwrap(const Api::Nsset::NssetId& src)
{
    return Util::make_strong<Nsset::NssetId>(proto_unwrap(src.uuid()));
}

template <>
Nsset::NssetHistoryId proto_unwrap(const Api::Nsset::NssetHistoryId& src)
{
    return Util::make_strong<Nsset::NssetHistoryId>(proto_unwrap(src.uuid()));
}

template <>
Nsset::ListNssetsRequest proto_unwrap(const Api::Nsset::ListNssetsRequest& src)
{
    Nsset::ListNssetsRequest dst;
    dst.exclude_nssets_without_domain = src.exclude_nssets_without_domain();
    return dst;
}

template <>
struct ProtoTypeTraits<Api::Nsset::ListNssetsByContactRequest::OrderByField>
{
    using RegistryType = Nsset::ListNssetsByContactRequest::OrderByField;
};

template <>
struct ProtoTypeTraits<Api::Nsset::ListNssetsByContactRequest::OrderBy>
{
    using RegistryType = Nsset::ListNssetsByContactRequest::OrderBy;
};

template <>
Nsset::ListNssetsByContactRequest::OrderByField proto_unwrap(const Api::Nsset::ListNssetsByContactRequest::OrderByField& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Nsset::ListNssetsByContactRequest::order_by_field_unspecified:
            return Nsset::ListNssetsByContactRequest::OrderByField::unspecified;
        case Api::Nsset::ListNssetsByContactRequest::order_by_field_crdate:
            return Nsset::ListNssetsByContactRequest::OrderByField::crdate;
        case Api::Nsset::ListNssetsByContactRequest::order_by_field_handle:
            return Nsset::ListNssetsByContactRequest::OrderByField::handle;
        case Api::Nsset::ListNssetsByContactRequest::order_by_field_domains_count:
            return Nsset::ListNssetsByContactRequest::OrderByField::domains_count;
        case Api::Nsset::ListNssetsByContactRequest::order_by_field_sponsoring_registrar_handle:
            return Nsset::ListNssetsByContactRequest::OrderByField::sponsoring_registrar_handle;
    }
    return Nsset::ListNssetsByContactRequest::OrderByField::unspecified;
}

template <>
Nsset::ListNssetsByContactRequest::OrderBy proto_unwrap(const Api::Nsset::ListNssetsByContactRequest::OrderBy& src)
{
    return Nsset::ListNssetsByContactRequest::OrderBy{
            proto_unwrap(src.field()),
            proto_unwrap(src.direction())};
}

template <>
Nsset::ListNssetsByContactRequest proto_unwrap(const Api::Nsset::ListNssetsByContactRequest& src)
{
    Nsset::ListNssetsByContactRequest dst;
    try
    {
        dst.contact_id = proto_unwrap(src.contact_id());
    }
    catch (...)
    {
        throw Nsset::ListNssetsByContactReply::Exception::InvalidData{dst, {"contact_id"}};
    }
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by.reserve(src.order_by().size());
    std::for_each(src.order_by().begin(), src.order_by().end(), [&dst](auto&& src_item)
    {
        dst.order_by.push_back(proto_unwrap(src_item));
    });
    dst.aggregate_entire_history = src.aggregate_entire_history();
    return dst;
}

template <>
Nsset::NssetInfoRequest proto_unwrap(const Api::Nsset::NssetInfoRequest& src)
{
    Nsset::NssetInfoRequest dst;
    dst.nsset_id = proto_unwrap(src.nsset_id());
    if (src.has_nsset_history_id())
    {
        dst.nsset_history_id = proto_unwrap(src.nsset_history_id());
    }
    dst.include_domains_count = src.include_domains_count();
    return dst;
}

void proto_unwrap_back(const Api::Nsset::BatchNssetInfoRequest& src, Nsset::BatchNssetInfoRequest& dst)
{
    dst.requests.reserve(dst.requests.size() + src.requests().size());
    std::transform(src.requests().cbegin(), src.requests().cend(), std::back_inserter(dst.requests), [](auto&& src)
    {
        Nsset::BatchNssetInfoRequest::Request dst;
        dst.nsset_id = src.nsset_id().uuid().value();
        if (src.has_nsset_history_id())
        {
            dst.nsset_history_id = src.nsset_history_id().uuid().value();
        }
        dst.include_domains_count = src.include_domains_count();
        return dst;
    });
}

template <>
Nsset::NssetHandle proto_unwrap(const Api::Nsset::NssetHandle& src)
{
    return Util::make_strong<Nsset::NssetHandle>(proto_unwrap(src.value()));
}

template <>
Nsset::NssetIdRequest proto_unwrap(const Api::Nsset::NssetIdRequest& src)
{
    Nsset::NssetIdRequest dst;
    dst.nsset_handle = proto_unwrap(src.nsset_handle());
    return dst;
}

namespace {

Nsset::NssetHistoryInterval::HistoryId unwrap_nsset_history_interval_history_id(const Api::Nsset::NssetHistoryId& src)
{
    return Util::make_strong<Nsset::NssetHistoryInterval::HistoryId>(
            Util::get_raw_value_from(proto_unwrap(src)));
}

}//namespace Fred::Registry::{anonymous}

template <>
Nsset::NssetHistoryInterval proto_unwrap(const Api::Nsset::NssetHistoryInterval& src)
{
    auto lower_limit = Nsset::NssetHistoryInterval::LowerLimit(Nsset::NssetHistoryInterval::NoLimit());
    if (src.has_lower_limit())
    {
        if (src.lower_limit().has_nsset_history_id())
        {
            lower_limit.value = unwrap_nsset_history_interval_history_id(src.lower_limit().nsset_history_id());
        }
        else if (src.lower_limit().has_timestamp())
        {
            lower_limit.value = unwrap_system_clock_nanoseconds(src.lower_limit().timestamp());
        }
    }
    auto upper_limit = Nsset::NssetHistoryInterval::UpperLimit(Nsset::NssetHistoryInterval::NoLimit());
    if (src.has_upper_limit())
    {
        if (src.upper_limit().has_nsset_history_id())
        {
            upper_limit.value = unwrap_nsset_history_interval_history_id(src.upper_limit().nsset_history_id());
        }
        else if (src.upper_limit().has_timestamp())
        {
            upper_limit.value = unwrap_system_clock_nanoseconds(src.upper_limit().timestamp());
        }
    }
    return Nsset::NssetHistoryInterval(lower_limit, upper_limit);
}

template <>
Nsset::SearchNssetRequest proto_unwrap(const Api::Nsset::SearchNssetRequest& src)
{
    Nsset::SearchNssetRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Nsset::SearchNssetRequest::LIMIT_NOT_SET)
    {
        dst.limit = src.limit();
    }
    else
    {
        dst.limit = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Nsset::NssetItem::nsset_handle))
        {
            dst.searched_items.insert(Nsset::NssetItem::nsset_handle);
        }
        else if (item == Util::Into<std::string>::from(Nsset::NssetItem::dns_hosts_fqdn))
        {
            dst.searched_items.insert(Nsset::NssetItem::dns_hosts_fqdn);
        }
        else if (item == Util::Into<std::string>::from(Nsset::NssetItem::dns_hosts_ip_addresses))
        {
            dst.searched_items.insert(Nsset::NssetItem::dns_hosts_ip_addresses);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchNssetRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items = { Nsset::NssetItem::nsset_handle,
                               Nsset::NssetItem::dns_hosts_fqdn,
                               Nsset::NssetItem::dns_hosts_ip_addresses };
    }
    return dst;
}

template <>
Nsset::SearchNssetHistoryRequest proto_unwrap(const Api::Nsset::SearchNssetHistoryRequest& src)
{
    Nsset::SearchNssetHistoryRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Nsset::SearchNssetHistoryRequest::LIMIT_NOT_SET)
    {
        dst.max_number_of_nssets = src.max_number_of_nssets();
    }
    else
    {
        dst.max_number_of_nssets = boost::none;
    }
    if (src.has_data_valid_from())
    {
        dst.data_valid_from = unwrap_system_clock_nanoseconds(src.data_valid_from());
    }
    else
    {
        dst.data_valid_from = boost::none;
    }
    if (src.has_data_valid_to())
    {
        dst.data_valid_to = unwrap_system_clock_nanoseconds(src.data_valid_to());
    }
    else
    {
        dst.data_valid_to = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Nsset::NssetItem::nsset_handle))
        {
            dst.searched_items.insert(Nsset::NssetItem::nsset_handle);
        }
        else if (item == Util::Into<std::string>::from(Nsset::NssetItem::dns_hosts_fqdn))
        {
            dst.searched_items.insert(Nsset::NssetItem::dns_hosts_fqdn);
        }
        else if (item == Util::Into<std::string>::from(Nsset::NssetItem::dns_hosts_ip_addresses))
        {
            dst.searched_items.insert(Nsset::NssetItem::dns_hosts_ip_addresses);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchNssetHistoryRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items = { Nsset::NssetItem::nsset_handle,
                               Nsset::NssetItem::dns_hosts_fqdn,
                               Nsset::NssetItem::dns_hosts_ip_addresses };
    }
    return dst;
}

template <>
Nsset::NssetDataHistoryRequest proto_unwrap(const Api::Nsset::NssetHistoryRequest& src)
{
    return Nsset::NssetDataHistoryRequest(
            proto_unwrap(src.nsset_id()),
            proto_unwrap(src.history()));
}

template <>
Nsset::NssetHandleHistoryRequest proto_unwrap(const Api::Nsset::NssetHandleHistoryRequest& src)
{
    return Nsset::NssetHandleHistoryRequest(proto_unwrap(src.nsset_handle()));
}

template <>
Nsset::NssetStateRequest proto_unwrap(const Api::Nsset::NssetStateRequest& src)
{
    Nsset::NssetStateRequest dst;
    dst.nsset_id = proto_unwrap(src.nsset_id());
    return dst;
}

template <>
Nsset::NssetStateHistoryRequest proto_unwrap(const Api::Nsset::NssetStateHistoryRequest& src)
{
    return Nsset::NssetStateHistoryRequest(proto_unwrap(src.nsset_id()), proto_unwrap(src.history()));
}

template <>
Nsset::CheckDnsHostRequest proto_unwrap(const Api::Nsset::CheckDnsHostRequest& src)
{
    return Nsset::CheckDnsHostRequest{proto_unwrap(src.fqdn())};
}

template <>
Domain::DomainId proto_unwrap(const Api::Domain::DomainId& src)
{
    return Util::make_strong<Domain::DomainId>(proto_unwrap(src.uuid()));
}

template <>
Domain::DomainHistoryId proto_unwrap(const Api::Domain::DomainHistoryId& src)
{
    return Util::make_strong<Domain::DomainHistoryId>(proto_unwrap(src.uuid()));
}

template <>
Domain::DomainInfoRequest proto_unwrap(const Api::Domain::DomainInfoRequest& src)
{
    Domain::DomainInfoRequest dst;
    dst.domain_id = proto_unwrap(src.domain_id());
    if (src.has_domain_history_id())
    {
        dst.domain_history_id = proto_unwrap(src.domain_history_id());
    }
    return dst;
}

void proto_unwrap_back(const Api::Domain::BatchDomainInfoRequest& src, Domain::BatchDomainInfoRequest& dst)
{
    dst.requests.reserve(dst.requests.size() + src.requests().size());
    std::transform(src.requests().cbegin(), src.requests().cend(), std::back_inserter(dst.requests), [](auto&& src)
    {
        Domain::BatchDomainInfoRequest::Request dst;
        dst.domain_id = src.domain_id().uuid().value();
        if (src.has_domain_history_id())
        {
            dst.domain_history_id = src.domain_history_id().uuid().value();
        }
        return dst;
    });
}

template <>
Domain::Fqdn proto_unwrap(const Api::Domain::Fqdn& src)
{
    return Util::make_strong<Domain::Fqdn>(proto_unwrap(src.value()));
}

template <>
Domain::DomainIdRequest proto_unwrap(const Api::Domain::DomainIdRequest& src)
{
    Domain::DomainIdRequest dst;
    dst.fqdn = proto_unwrap(src.fqdn());
    return dst;
}

template <>
Domain::DomainStateFlagRequest proto_unwrap(const Api::Domain::DomainStateFlagRequest& src)
{
    return Util::make_strong<Domain::DomainStateFlagRequest>(proto_unwrap(src.uuid()));
}

namespace {

Domain::DomainHistoryInterval::HistoryId unwrap_domain_history_interval_history_id(const Api::Domain::DomainHistoryId& src)
{
    return Util::make_strong<Domain::DomainHistoryInterval::HistoryId>(
            Util::get_raw_value_from(proto_unwrap(src)));
}

Domain::BatchDeleteDomainsRequest::Domain proto_unwrap_domain(const Api::Domain::BatchDeleteDomainsRequest::Domain& src)
{
    Domain::BatchDeleteDomainsRequest::Domain dst{};
    dst.domain_id = proto_unwrap(src.domain_id());
    dst.domain_history_id = proto_unwrap(src.domain_history_id());
    return dst;
}

Domain::ManageDomainStateFlagsRequest::Domain proto_unwrap_domain(const Api::Domain::ManageDomainStateFlagsRequest::Domain& src)
{
    Domain::ManageDomainStateFlagsRequest::Domain dst{};
    dst.domain_id = proto_unwrap(src.domain_id());
    if (src.has_domain_history_id())
    {
        dst.domain_history_id = proto_unwrap(src.domain_history_id());
    }
    else
    {
        dst.domain_history_id = boost::none;
    }
    return dst;
}

Domain::ManageDomainStateFlagsRequest::Validity proto_unwrap_validity(const Api::Domain::ManageDomainStateFlagsRequest::Validity& src)
{
    Domain::ManageDomainStateFlagsRequest::Validity dst{};
    if (src.has_valid_from())
    {
        dst.valid_from = unwrap_system_clock_nanoseconds(src.valid_from());
    }
    else
    {
        dst.valid_from = boost::none;
    }
    if (src.has_valid_to())
    {
        dst.valid_to = unwrap_system_clock_nanoseconds(src.valid_to());
    }
    else
    {
        dst.valid_to = boost::none;
    }
    return dst;
}

Domain::ManageDomainStateFlagsRequest::ToCreate proto_unwrap_request_to_create(const Api::Domain::ManageDomainStateFlagsRequest::ToCreate& src)
{
    Domain::ManageDomainStateFlagsRequest::ToCreate dst{};
    std::for_each(begin(src.state_flags()), end(src.state_flags()), [&](auto&& state_flag)
    {
        dst.state_flags[state_flag.first] = proto_unwrap_validity(state_flag.second);
    });
    dst.domains.reserve(dst.domains.size() + src.domains().size());
    for (int idx = 0; idx < src.domains().size(); ++idx)
    {
        dst.domains.push_back(proto_unwrap_domain(src.domains(idx)));
    }
    return dst;
}

}//namespace Fred::Registry::{anonymous}

void proto_unwrap_back(const Api::Domain::BatchDeleteDomainsRequest& src, Domain::BatchDeleteDomainsRequest& dst)
{
    dst.domains.reserve(dst.domains.size() + src.domains().size());
    for (int idx = 0; idx < src.domains().size(); ++idx)
    {
        dst.domains.push_back(proto_unwrap_domain(src.domains(idx)));
    }
}

void proto_unwrap_back(const Api::Domain::ManageDomainStateFlagsRequest& src, Domain::ManageDomainStateFlagsRequest& dst)
{
    if (!src.state_flag_requests_to_create().domains().empty())
    {
        dst.state_flag_requests_to_create.emplace_back(
                proto_unwrap_request_to_create(src.state_flag_requests_to_create()));
    }
    dst.state_flag_requests_to_close.reserve(dst.state_flag_requests_to_close.size() + src.state_flag_requests_to_close().size());
    for (int idx = 0; idx < src.state_flag_requests_to_close().size(); ++idx)
    {
        dst.state_flag_requests_to_close.push_back(proto_unwrap(src.state_flag_requests_to_close(idx)));
    }
}

template <>
Domain::UpdateDomainsAdditionalNotifyInfoRequest::DomainAdditionalInfo proto_unwrap(const Api::Domain::UpdateDomainsDeleteAdditionalNotifyInfoRequest::DomainAdditionalInfo& src)
{
    Domain::UpdateDomainsAdditionalNotifyInfoRequest::DomainAdditionalInfo dst;
    dst.domain_id = proto_unwrap(src.domain_id());
    dst.event_time.lower_bound = unwrap_system_clock_nanoseconds(src.delete_from());
    dst.event_time.upper_bound = unwrap_system_clock_nanoseconds(src.delete_to());
    for (int idx = 0; idx < src.additional_contacts().size(); ++idx)
    {
        switch (src.additional_contacts(idx).Info_case())
        {
            case Api::Domain::DomainContactInfo::kEmail:
                dst.additional_emails.emplace_back(Util::make_strong<Domain::EmailAddress>(src.additional_contacts(idx).email()));
                break;
            case Api::Domain::DomainContactInfo::kTelephone:
                dst.additional_phones.emplace_back(Util::make_strong<Domain::PhoneNumber>(src.additional_contacts(idx).telephone()));
                break;
            case Api::Domain::DomainContactInfo::INFO_NOT_SET:
                break;
        }
    }
    return dst;
}

template <>
Domain::UpdateDomainsAdditionalNotifyInfoRequest::DomainAdditionalInfo proto_unwrap(const Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest::DomainAdditionalInfo& src)
{
    Domain::UpdateDomainsAdditionalNotifyInfoRequest::DomainAdditionalInfo dst;
    dst.domain_id = proto_unwrap(src.domain_id());
    dst.event_time.lower_bound = unwrap_system_clock_nanoseconds(src.outzone_from());
    dst.event_time.upper_bound = unwrap_system_clock_nanoseconds(src.outzone_to());
    for (int idx = 0; idx < src.additional_contacts().size(); ++idx)
    {
        switch (src.additional_contacts(idx).Info_case())
        {
            case Api::Domain::DomainContactInfo::kEmail:
                dst.additional_emails.emplace_back(Util::make_strong<Domain::EmailAddress>(src.additional_contacts(idx).email()));
                break;
            case Api::Domain::DomainContactInfo::kTelephone:
                dst.additional_phones.emplace_back(Util::make_strong<Domain::PhoneNumber>(src.additional_contacts(idx).telephone()));
                break;
            case Api::Domain::DomainContactInfo::INFO_NOT_SET:
                break;
        }
    }
    return dst;
}

void proto_unwrap_back(const Api::Domain::UpdateDomainsDeleteAdditionalNotifyInfoRequest& src, Domain::UpdateDomainsAdditionalNotifyInfoRequest& dst)
{
    dst.event = Domain::LifecycleEvent::domain_delete;
    dst.domains_additional_info.reserve(dst.domains_additional_info.size() + src.domains_additional_info().size());
    for (int idx = 0; idx < src.domains_additional_info().size(); ++idx)
    {
        dst.domains_additional_info.push_back(proto_unwrap(src.domains_additional_info(idx)));
    }
}

void proto_unwrap_back(const Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest& src, Domain::UpdateDomainsAdditionalNotifyInfoRequest& dst)
{
    dst.event = Domain::LifecycleEvent::domain_outzone;
    dst.domains_additional_info.reserve(dst.domains_additional_info.size() + src.domains_additional_info().size());
    for (int idx = 0; idx < src.domains_additional_info().size(); ++idx)
    {
        dst.domains_additional_info.push_back(proto_unwrap(src.domains_additional_info(idx)));
    }
}

template <>
Domain::DomainHistoryInterval proto_unwrap(const Api::Domain::DomainHistoryInterval& src)
{
    auto lower_limit = Domain::DomainHistoryInterval::LowerLimit(Domain::DomainHistoryInterval::NoLimit());
    if (src.has_lower_limit())
    {
        if (src.lower_limit().has_domain_history_id())
        {
            lower_limit.value = unwrap_domain_history_interval_history_id(src.lower_limit().domain_history_id());
        }
        else if (src.lower_limit().has_timestamp())
        {
            lower_limit.value = unwrap_system_clock_nanoseconds(src.lower_limit().timestamp());
        }
    }
    auto upper_limit = Domain::DomainHistoryInterval::UpperLimit(Domain::DomainHistoryInterval::NoLimit());
    if (src.has_upper_limit())
    {
        if (src.upper_limit().has_domain_history_id())
        {
            upper_limit.value = unwrap_domain_history_interval_history_id(src.upper_limit().domain_history_id());
        }
        else if (src.upper_limit().has_timestamp())
        {
            upper_limit.value = unwrap_system_clock_nanoseconds(src.upper_limit().timestamp());
        }
    }
    return Domain::DomainHistoryInterval(lower_limit, upper_limit);
}

template <>
Domain::SearchDomainRequest proto_unwrap(const Api::Domain::SearchDomainRequest& src)
{
    Domain::SearchDomainRequest dst;
    for (const auto& item : src.query_values())
    {
        dst.query_values.push_back(item);
    }

    if (src.Limit_case() != Api::Domain::SearchDomainRequest::LIMIT_NOT_SET)
    {
        dst.limit = src.limit();
    }
    else
    {
        dst.limit = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Domain::DomainItem::fqdn))
        {
            dst.searched_items.insert(Domain::DomainItem::fqdn);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchDomainRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items.insert(Domain::DomainItem::fqdn);
    }
    return dst;
}

template <>
Domain::SearchDomainHistoryRequest proto_unwrap(const Api::Domain::SearchDomainHistoryRequest& src)
{
    Domain::SearchDomainHistoryRequest dst;
    dst.query_values.reserve(src.query_values_size());
    for (const auto& value : src.query_values())
    {
        dst.query_values.push_back(value);
    }
    if (src.Limit_case() != Api::Domain::SearchDomainHistoryRequest::LIMIT_NOT_SET)
    {
        dst.max_number_of_domains = src.max_number_of_domains();
    }
    else
    {
        dst.max_number_of_domains = boost::none;
    }
    if (src.has_data_valid_from())
    {
        dst.data_valid_from = unwrap_system_clock_nanoseconds(src.data_valid_from());
    }
    else
    {
        dst.data_valid_from = boost::none;
    }
    if (src.has_data_valid_to())
    {
        dst.data_valid_to = unwrap_system_clock_nanoseconds(src.data_valid_to());
    }
    else
    {
        dst.data_valid_to = boost::none;
    }
    for (const auto& item : src.searched_items())
    {
        if (item == Util::Into<std::string>::from(Domain::DomainItem::fqdn))
        {
            dst.searched_items.insert(Domain::DomainItem::fqdn);
        }
        else
        {
            FREDLOG_WARNING("unknown item \"" + item + "\" in SearchDomainHistoryRequest");
        }
    }
    if (dst.searched_items.empty())
    {
        dst.searched_items.insert(Domain::DomainItem::fqdn);
    }
    return dst;
}

template <>
Domain::DomainDataHistoryRequest proto_unwrap(const Api::Domain::DomainHistoryRequest& src)
{
    return Domain::DomainDataHistoryRequest(
            proto_unwrap(src.domain_id()),
            proto_unwrap(src.history()));
}

template <>
Domain::FqdnHistoryRequest proto_unwrap(const Api::Domain::FqdnHistoryRequest& src)
{
    return Domain::FqdnHistoryRequest(proto_unwrap(src.fqdn()));
}

template <>
Domain::DomainStateRequest proto_unwrap(const Api::Domain::DomainStateRequest& src)
{
    Domain::DomainStateRequest dst;
    dst.domain_id = proto_unwrap(src.domain_id());
    return dst;
}

template <>
Domain::DomainStateHistoryRequest proto_unwrap(const Api::Domain::DomainStateHistoryRequest& src)
{
    return Domain::DomainStateHistoryRequest(proto_unwrap(src.domain_id()), proto_unwrap(src.history()));
}

template <>
Domain::GetDomainsByContactRequest proto_unwrap(const Api::Domain::DomainsByContactRequest& src)
{
    Domain::GetDomainsByContactRequest dst;
    dst.contact_id = proto_unwrap(src.contact_id());
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by = proto_unwrap(src.order_by());
    dst.fqdn_filter = proto_unwrap(src.fqdn_filter());
    dst.include_holder = src.include_holder();
    dst.include_admin_contact = src.include_admin_contact();
    dst.include_keyset_tech_contact = src.include_keyset_tech_contact();
    dst.include_nsset_tech_contact = src.include_nsset_tech_contact();
    dst.include_deleted_domains = src.include_deleted_domains();
    return dst;
}

template <>
Domain::DomainLifeCycleStageRequest proto_unwrap(const Api::Domain::DomainLifeCycleStageRequest& src)
{
    Domain::DomainLifeCycleStageRequest dst;
    dst.fqdn = proto_unwrap(src.fqdn());
    if (src.has_time())
    {
        dst.time = unwrap_system_clock_nanoseconds(src.time());
    }
    return dst;
}

template <>
Domain::GetDomainsNotifyInfoRequest proto_unwrap(const Api::Domain::GetDomainsOutzoneNotifyInfoRequest& src)
{
    Domain::GetDomainsNotifyInfoRequest dst;
    dst.event_time.lower_bound = unwrap_system_clock_nanoseconds(src.outzone_from());
    dst.event_time.upper_bound = unwrap_system_clock_nanoseconds(src.outzone_to());
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.event = Domain::LifecycleEvent::domain_outzone;
    return dst;
}

template <>
Domain::GetDomainsNotifyInfoRequest proto_unwrap(const Api::Domain::GetDomainsDeleteNotifyInfoRequest& src)
{
    Domain::GetDomainsNotifyInfoRequest dst;
    dst.event_time.lower_bound = unwrap_system_clock_nanoseconds(src.delete_from());
    dst.event_time.upper_bound = unwrap_system_clock_nanoseconds(src.delete_to());
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.event = Domain::LifecycleEvent::domain_delete;
    return dst;
}

template <>
Domain::ListDomainsRequest proto_unwrap(const Api::Domain::ListDomainsRequest& src)
{
    Domain::ListDomainsRequest dst;
    dst.zone = proto_unwrap(src.zone());
    return dst;
}

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByNssetRequest::OrderByField>
{
    using RegistryType = Domain::ListDomainsByNssetRequest::OrderByField;
};

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByNssetRequest::OrderBy>
{
    using RegistryType = Domain::ListDomainsByNssetRequest::OrderBy;
};

template <>
Domain::ListDomainsByNssetRequest::OrderByField proto_unwrap(const Api::Domain::ListDomainsByNssetRequest::OrderByField& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Domain::ListDomainsByNssetRequest::order_by_field_unspecified:
            return Domain::ListDomainsByNssetRequest::OrderByField::unspecified;
        case Api::Domain::ListDomainsByNssetRequest::order_by_field_crdate:
            return Domain::ListDomainsByNssetRequest::OrderByField::crdate;
        case Api::Domain::ListDomainsByNssetRequest::order_by_field_exdate:
            return Domain::ListDomainsByNssetRequest::OrderByField::exdate;
        case Api::Domain::ListDomainsByNssetRequest::order_by_field_fqdn:
            return Domain::ListDomainsByNssetRequest::OrderByField::fqdn;
        case Api::Domain::ListDomainsByNssetRequest::order_by_field_sponsoring_registrar_handle:
            return Domain::ListDomainsByNssetRequest::OrderByField::sponsoring_registrar_handle;
    }
    return Domain::ListDomainsByNssetRequest::OrderByField::unspecified;
}

template <>
Domain::ListDomainsByNssetRequest::OrderBy proto_unwrap(const Api::Domain::ListDomainsByNssetRequest::OrderBy& src)
{
    return Domain::ListDomainsByNssetRequest::OrderBy{
            proto_unwrap(src.field()),
            proto_unwrap(src.direction())};
}

template <>
Domain::ListDomainsByNssetRequest proto_unwrap(const Api::Domain::ListDomainsByNssetRequest& src)
{
    Domain::ListDomainsByNssetRequest dst;

    try
    {
        dst.nsset_id = proto_unwrap(src.nsset_id());
    }
    catch (...)
    {
        throw Domain::ListDomainsByNssetReply::Exception::InvalidData(dst, {"nsset_id"});
    }
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by.reserve(src.order_by().size());
    std::for_each(src.order_by().begin(), src.order_by().end(), [&dst](auto&& src_item)
    {
        dst.order_by.push_back(proto_unwrap(src_item));
    });
    dst.aggregate_entire_history = src.aggregate_entire_history();
    return dst;
}

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByKeysetRequest::OrderByField>
{
    using RegistryType = Domain::ListDomainsByKeysetRequest::OrderByField;
};

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByKeysetRequest::OrderBy>
{
    using RegistryType = Domain::ListDomainsByKeysetRequest::OrderBy;
};

template <>
Domain::ListDomainsByKeysetRequest::OrderByField proto_unwrap(const Api::Domain::ListDomainsByKeysetRequest::OrderByField& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Domain::ListDomainsByKeysetRequest::order_by_field_unspecified:
            return Domain::ListDomainsByKeysetRequest::OrderByField::unspecified;
        case Api::Domain::ListDomainsByKeysetRequest::order_by_field_crdate:
            return Domain::ListDomainsByKeysetRequest::OrderByField::crdate;
        case Api::Domain::ListDomainsByKeysetRequest::order_by_field_exdate:
            return Domain::ListDomainsByKeysetRequest::OrderByField::exdate;
        case Api::Domain::ListDomainsByKeysetRequest::order_by_field_fqdn:
            return Domain::ListDomainsByKeysetRequest::OrderByField::fqdn;
        case Api::Domain::ListDomainsByKeysetRequest::order_by_field_sponsoring_registrar_handle:
            return Domain::ListDomainsByKeysetRequest::OrderByField::sponsoring_registrar_handle;
    }
    return Domain::ListDomainsByKeysetRequest::OrderByField::unspecified;
}

template <>
Domain::ListDomainsByKeysetRequest::OrderBy proto_unwrap(const Api::Domain::ListDomainsByKeysetRequest::OrderBy& src)
{
    return Domain::ListDomainsByKeysetRequest::OrderBy{
            proto_unwrap(src.field()),
            proto_unwrap(src.direction())};
}

template <>
Domain::ListDomainsByKeysetRequest proto_unwrap(const Api::Domain::ListDomainsByKeysetRequest& src)
{
    Domain::ListDomainsByKeysetRequest dst;

    try
    {
        dst.keyset_id = proto_unwrap(src.keyset_id());
    }
    catch (...)
    {
        throw Domain::ListDomainsByKeysetReply::Exception::InvalidData(dst, {"keyset_id"});
    }
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by.reserve(src.order_by().size());
    std::for_each(src.order_by().begin(), src.order_by().end(), [&dst](auto&& src_item)
    {
        dst.order_by.push_back(proto_unwrap(src_item));
    });
    dst.aggregate_entire_history = src.aggregate_entire_history();
    return dst;
}

template <>
struct ProtoTypeTraits<Api::Domain::DomainContactRole>
{
    using RegistryType = Domain::DomainContactRole;
};

template <>
Domain::DomainContactRole proto_unwrap(const Api::Domain::DomainContactRole& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Domain::role_unspecified:
            return Domain::DomainContactRole::unspecified;
        case Api::Domain::role_registrant: // holder
            return Domain::DomainContactRole::registrant;
        case Api::Domain::role_administrative:
            return Domain::DomainContactRole::administrative;
        case Api::Domain::role_keyset_technical:
            return Domain::DomainContactRole::keyset_technical;
        case Api::Domain::role_nsset_technical:
            return Domain::DomainContactRole::nsset_technical;
    }
    return Domain::DomainContactRole::unspecified;
}

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByContactRequest::OrderByField>
{
    using RegistryType = Domain::ListDomainsByContactRequest::OrderByField;
};

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByContactRequest::OrderBy>
{
    using RegistryType = Domain::ListDomainsByContactRequest::OrderBy;
};

template <>
Domain::ListDomainsByContactRequest::OrderByField proto_unwrap(const Api::Domain::ListDomainsByContactRequest::OrderByField& src)
{
    switch (static_cast<int>(src))
    {
        case Api::Domain::ListDomainsByContactRequest::order_by_field_unspecified:
            return Domain::ListDomainsByContactRequest::OrderByField::unspecified;
        case Api::Domain::ListDomainsByContactRequest::order_by_field_crdate:
            return Domain::ListDomainsByContactRequest::OrderByField::crdate;
        case Api::Domain::ListDomainsByContactRequest::order_by_field_exdate:
            return Domain::ListDomainsByContactRequest::OrderByField::exdate;
        case Api::Domain::ListDomainsByContactRequest::order_by_field_fqdn:
            return Domain::ListDomainsByContactRequest::OrderByField::fqdn;
        case Api::Domain::ListDomainsByContactRequest::order_by_field_sponsoring_registrar_handle:
            return Domain::ListDomainsByContactRequest::OrderByField::sponsoring_registrar_handle;
    }
    return Domain::ListDomainsByContactRequest::OrderByField::unspecified;
}

template <>
Domain::ListDomainsByContactRequest::OrderBy proto_unwrap(const Api::Domain::ListDomainsByContactRequest::OrderBy& src)
{
    return Domain::ListDomainsByContactRequest::OrderBy{
            proto_unwrap(src.field()),
            proto_unwrap(src.direction())};
}

template <>
Domain::ListDomainsByContactRequest proto_unwrap(const Api::Domain::ListDomainsByContactRequest& src)
{
    Domain::ListDomainsByContactRequest dst;

    try
    {
        dst.contact_id = proto_unwrap(src.contact_id());
    }
    catch (...)
    {
        throw Domain::ListDomainsByContactReply::Exception::InvalidData(dst, {"contact_id"});
    }
    dst.fqdn_filter = src.fqdn_filter();
    dst.role_filters.reserve(src.role_filters().size());
    std::for_each(src.role_filters().begin(), src.role_filters().end(), [&dst](auto&& src_item)
    {
        dst.role_filters.push_back(proto_unwrap(static_cast<Api::Domain::DomainContactRole>(src_item)));
    });
    if (src.has_pagination())
    {
        dst.pagination = proto_unwrap(src.pagination());
    }
    dst.order_by.reserve(src.order_by().size());
    std::for_each(src.order_by().begin(), src.order_by().end(), [&dst](auto&& src_item)
    {
        dst.order_by.push_back(proto_unwrap(src_item));
    });
    dst.aggregate_entire_history = src.aggregate_entire_history();
    return dst;
}

template <>
Registrar::RegistrarCreditRequest proto_unwrap(const Api::Registrar::RegistrarCreditRequest& src)
{
    Registrar::RegistrarCreditRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    return dst;
}

template <>
Registrar::RegistrarInfoRequest proto_unwrap(const Api::Registrar::RegistrarInfoRequest& src)
{
    Registrar::RegistrarInfoRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    return dst;
}

template <>
Registrar::RegistrarZoneAccessRequest proto_unwrap(const Api::Registrar::RegistrarZoneAccessRequest& src)
{
    Registrar::RegistrarZoneAccessRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    return dst;
}

template <>
Registrar::CreateRegistrarRequest proto_unwrap(const Api::Registrar::CreateRegistrarRequest& src)
{
    Registrar::CreateRegistrarRequest dst;
    if (src.has_registrar_handle())
    {
        dst.registrar_handle = proto_unwrap(src.registrar_handle());
    }
    dst.name = proto_unwrap(src.name());
    dst.organization = proto_unwrap(src.organization());
    if (src.has_place())
    {
        dst.place = proto_unwrap_into<PlaceAddress>(src.place());
    }
    if (src.has_telephone())
    {
        dst.telephone = proto_unwrap_into<PhoneNumber>(src.telephone());
    }
    if (src.has_fax())
    {
        dst.fax = proto_unwrap_into<PhoneNumber>(src.fax());
    }
    dst.emails = unwrap_email_addresses_into<EmailAddress>(src.emails());
    if (src.has_url())
    {
        dst.url = Util::make_strong<Url>(src.url().value());
    }
    dst.is_system_registrar = src.is_system_registrar();
    if (src.has_company_registration_number())
    {
        dst.company_registration_number = Util::make_strong<CompanyRegistrationNumber>(src.company_registration_number().value());
    }
    if (src.has_vat_identification_number())
    {
        dst.vat_identification_number = Util::make_strong<VatIdentificationNumber>(src.vat_identification_number().value());
    }
    dst.variable_symbol = proto_unwrap(src.variable_symbol());
    dst.payment_memo_regex = proto_unwrap(src.payment_memo_regex());
    dst.is_vat_payer = src.is_vat_payer();
    dst.is_internal = src.is_internal();
    return dst;
}

template <>
Registrar::UpdateRegistrarRequest proto_unwrap(const Api::Registrar::UpdateRegistrarRequest& src)
{
    Registrar::UpdateRegistrarRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    if (src.SetName_case() == Api::Registrar::UpdateRegistrarRequest::kSetName)
    {
        dst.set_name = src.set_name();
    }
    if (src.SetOrganization_case() == Api::Registrar::UpdateRegistrarRequest::kSetOrganization)
    {
        dst.set_organization = src.set_organization();
    }
    if (src.has_set_place())
    {
        dst.set_place = proto_unwrap_into<PlaceAddress>(src.set_place());
    }
    if (src.has_set_telephone())
    {
        dst.set_telephone = proto_unwrap_into<PhoneNumber>(src.set_telephone());
    }
    if (src.has_set_fax())
    {
        dst.set_fax = proto_unwrap_into<PhoneNumber>(src.set_fax());
    }
    if (src.has_set_emails())
    {
        dst.set_emails = unwrap_set_email_addresses(src.set_emails());
    }
    if (src.has_set_url())
    {
        dst.set_url = Util::make_strong<Url>(src.set_url().value());
    }
    if (src.SetIsSystemRegistrar_case() == Api::Registrar::UpdateRegistrarRequest::kSetIsSystemRegistrar)
    {
        dst.set_is_system_registrar = src.set_is_system_registrar();
    }
    if (src.has_set_company_registration_number())
    {
        dst.set_company_registration_number = Util::make_strong<CompanyRegistrationNumber>(src.set_company_registration_number().value());
    }
    if (src.has_set_vat_identification_number())
    {
        dst.set_vat_identification_number = Util::make_strong<VatIdentificationNumber>(src.set_vat_identification_number().value());
    }
    if (src.SetVariableSymbol_case() == Api::Registrar::UpdateRegistrarRequest::kSetVariableSymbol)
    {
        dst.set_variable_symbol = src.set_variable_symbol();
    }
    if (src.SetPaymentMemoRegex_case() == Api::Registrar::UpdateRegistrarRequest::kSetPaymentMemoRegex)
    {
        dst.set_payment_memo_regex = src.set_payment_memo_regex();
    }
    if (src.SetIsVatPayer_case() == Api::Registrar::UpdateRegistrarRequest::kSetIsVatPayer)
    {
        dst.set_is_vat_payer = src.set_is_vat_payer();
    }
    if (src.SetIsInternal_case() == Api::Registrar::UpdateRegistrarRequest::kSetIsInternal)
    {
        dst.set_is_internal = src.set_is_internal();
    }
    if (src.has_set_registrar_handle())
    {
        dst.set_registrar_handle = Util::make_strong<Registrar::RegistrarHandle>(src.set_registrar_handle().value());
    }
    return dst;
}

template <>
Registrar::AddRegistrarZoneAccessRequest proto_unwrap(const Api::Registrar::AddRegistrarZoneAccessRequest& src)
{
    Registrar::AddRegistrarZoneAccessRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    dst.zone = proto_unwrap(src.zone());
    if (src.has_valid_from())
    {
        dst.valid_from = unwrap_system_clock_nanoseconds(src.valid_from());
    }
    if (src.has_valid_to())
    {
        dst.valid_to = unwrap_system_clock_nanoseconds(src.valid_to());
    }
    return dst;
}

template <>
Registrar::DeleteRegistrarZoneAccessRequest proto_unwrap(const Api::Registrar::DeleteRegistrarZoneAccessRequest& src)
{
    return Registrar::DeleteRegistrarZoneAccessRequest{Registrar::RegistrarZoneAccessId{
            proto_unwrap(src.registrar_handle()),
            proto_unwrap(src.zone()),
            unwrap_system_clock_nanoseconds(src.valid_from())}};
}

template <>
Registrar::UpdateRegistrarZoneAccessRequest proto_unwrap(const Api::Registrar::UpdateRegistrarZoneAccessRequest& src)
{
    Registrar::UpdateRegistrarZoneAccessRequest dst{Registrar::RegistrarZoneAccessId{
            proto_unwrap(src.registrar_handle()),
            proto_unwrap(src.zone()),
            unwrap_system_clock_nanoseconds(src.valid_from())}};
    if (src.has_set_valid_from())
    {
        dst.set_valid_from = unwrap_system_clock_nanoseconds(src.set_valid_from());
    }
    if (src.has_set_valid_to())
    {
        dst.set_valid_to = unwrap_system_clock_nanoseconds(src.set_valid_to());
    }
    return dst;
}

template <>
Registrar::GetRegistrarEppCredentialsRequest proto_unwrap(const Api::Registrar::GetRegistrarEppCredentialsRequest& src)
{
    Registrar::GetRegistrarEppCredentialsRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    return dst;
}

namespace {

Registrar::SslCertificate proto_unwrap(const Api::Registrar::SslCertificate& src)
{
    Registrar::SslCertificate dst;
    dst.fingerprint.reserve(src.fingerprint().size());
    std::transform(begin(src.fingerprint()), end(src.fingerprint()), std::back_inserter(dst.fingerprint), [](char c)
    {
        return static_cast<unsigned char>(c);
    });
    dst.cert_data_pem = src.cert_data_pem();
    return dst;
}

template <typename Dst, typename Src>
std::decay_t<Dst> proto_unwrap_uuid(const Src& src)
{
    return Util::make_strong<std::decay_t<Dst>>(boost::uuids::string_generator{}(src.value()));
}

template <typename Dst, typename Src, typename Fnc>
std::enable_if_t<!std::is_convertible<decltype(std::declval<Fnc>()(std::declval<const std::exception&>())), std::decay_t<Dst>>::value, std::decay_t<Dst>>
proto_unwrap_uuid(const Src& src, Fnc&& on_exception)
{
    try
    {
        return proto_unwrap_uuid<Dst>(src);
    }
    catch (const std::exception& e)
    {
        std::forward<Fnc>(on_exception)(e);
        throw;
    }
}

template <typename Dst, typename Src, typename Fnc>
std::enable_if_t<std::is_convertible<decltype(std::declval<Fnc>()(std::declval<const std::exception&>())), std::decay_t<Dst>>::value, std::decay_t<Dst>>
proto_unwrap_uuid(const Src& src, Fnc&& on_exception)
{
    try
    {
        return proto_unwrap_uuid<Dst>(src);
    }
    catch (const std::exception& e)
    {
        return std::forward<Fnc>(on_exception)(e);
    }
}

Registrar::RegistrarEppCredentialsId proto_unwrap(const Api::Registrar::RegistrarEppCredentialsId& src)
{
    return Util::make_strong<Registrar::RegistrarEppCredentialsId>(boost::uuids::string_generator{}(src.value()));
}

}//namespace Fred::Registry::{anonymous}

template <>
Registrar::AddRegistrarEppCredentialsRequest proto_unwrap(const Api::Registrar::AddRegistrarEppCredentialsRequest& src)
{
    Registrar::AddRegistrarEppCredentialsRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    dst.certificate = proto_unwrap(src.certificate());
    if (src.has_template_credentials_id())
    {
        dst.password = proto_unwrap(src.template_credentials_id());
    }
    else
    {
        dst.password = src.password();
    }
    return dst;
}

template <>
Registrar::DeleteRegistrarEppCredentialsRequest proto_unwrap(const Api::Registrar::DeleteRegistrarEppCredentialsRequest& src)
{
    Registrar::DeleteRegistrarEppCredentialsRequest dst;
    dst.credentials_id = proto_unwrap_uuid<decltype(dst.credentials_id)>(src.credentials_id());
    return dst;
}

template <>
Registrar::UpdateRegistrarEppCredentialsRequest proto_unwrap(const Api::Registrar::UpdateRegistrarEppCredentialsRequest& src)
{
    Registrar::UpdateRegistrarEppCredentialsRequest dst;
    dst.credentials_id = proto_unwrap_uuid<decltype(dst.credentials_id)>(src.credentials_id());
    dst.password = src.password();
    return dst;
}

template <>
Registrar::GetRegistrarGroupsMembershipRequest proto_unwrap(const Api::Registrar::GetRegistrarGroupsMembershipRequest& src)
{
    Registrar::GetRegistrarGroupsMembershipRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    return dst;
}

template <>
Registrar::AddRegistrarGroupMembershipRequest proto_unwrap(const Api::Registrar::AddRegistrarGroupMembershipRequest& src)
{
    Registrar::AddRegistrarGroupMembershipRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    dst.group = Util::make_strong<Registrar::GroupName>(proto_unwrap(src.group()));
    return dst;
}

template <>
Registrar::DeleteRegistrarGroupMembershipRequest proto_unwrap(const Api::Registrar::DeleteRegistrarGroupMembershipRequest& src)
{
    Registrar::DeleteRegistrarGroupMembershipRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    dst.group = Util::make_strong<Registrar::GroupName>(proto_unwrap(src.group()));
    return dst;
}

template <>
Registrar::GetRegistrarCertificationsRequest proto_unwrap(const Api::Registrar::GetRegistrarCertificationsRequest& src)
{
    Registrar::GetRegistrarCertificationsRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    return dst;
}

template <>
Registrar::AddRegistrarCertificationRequest proto_unwrap(const Api::Registrar::AddRegistrarCertificationRequest& src)
{
    Registrar::AddRegistrarCertificationRequest dst;
    dst.registrar_handle = proto_unwrap(src.registrar_handle());
    if (src.has_valid_from())
    {
        dst.valid_from = unwrap_system_clock_nanoseconds(src.valid_from());
    }
    else
    {
        dst.valid_from = boost::none;
    }
    if (src.has_valid_to())
    {
        dst.valid_to = unwrap_system_clock_nanoseconds(src.valid_to());
    }
    else
    {
        dst.valid_to = boost::none;
    }
    dst.classification = src.classification();
    dst.file_id = proto_unwrap_uuid<decltype(dst.file_id)>(src.file_id());
    return dst;
}

template <>
Registrar::DeleteRegistrarCertificationRequest proto_unwrap(const Api::Registrar::DeleteRegistrarCertificationRequest& src)
{
    Registrar::DeleteRegistrarCertificationRequest dst;
    dst.certification_id = proto_unwrap_uuid<decltype(dst.certification_id)>(src.certification_id());
    return dst;
}

template <>
Registrar::UpdateRegistrarCertificationRequest proto_unwrap(const Api::Registrar::UpdateRegistrarCertificationRequest& src)
{
    Registrar::UpdateRegistrarCertificationRequest dst;
    dst.certification_id = proto_unwrap_uuid<decltype(dst.certification_id)>(src.certification_id());
    if (src.has_set_valid_to())
    {
        dst.set_valid_to = unwrap_system_clock_nanoseconds(src.set_valid_to());
    }
    else
    {
        dst.set_valid_to = boost::none;
    }
    if (src.has_set_classification())
    {
        dst.set_classification = src.set_classification();
    }
    else
    {
        dst.set_classification = boost::none;
    }
    if (src.has_set_file_id())
    {
        dst.set_file_id = proto_unwrap_uuid<decltype(*dst.set_file_id)>(src.set_file_id());
    }
    else
    {
        dst.set_file_id = boost::none;
    }
    return dst;
}

template <>
struct ProtoTypeTraits<Api::ContactRepresentative::ContactRepresentativeId>
{
    using RegistryType = ContactRepresentative::ContactRepresentativeId;
};

template <>
struct ProtoTypeTraits<Api::ContactRepresentative::ContactRepresentativeHistoryId>
{
    using RegistryType = ContactRepresentative::ContactRepresentativeHistoryId;
};

template <>
ContactRepresentative::ContactRepresentativeId proto_unwrap(const Api::ContactRepresentative::ContactRepresentativeId& src)
{
    return Util::make_strong<ContactRepresentative::ContactRepresentativeId>(src.value());
}

template <>
ContactRepresentative::ContactRepresentativeHistoryId proto_unwrap(const Api::ContactRepresentative::ContactRepresentativeHistoryId& src)
{
    return Util::make_strong<ContactRepresentative::ContactRepresentativeHistoryId>(src.value());
}

template<>
ContactRepresentative::ContactRepresentativeInfoRequest proto_unwrap(const Api::ContactRepresentative::ContactRepresentativeInfoRequest& src)
{
    ContactRepresentative::ContactRepresentativeInfoRequest dst;
    if (src.Id_case() == Api::ContactRepresentative::ContactRepresentativeInfoRequest::kId)
    {
        dst.contact_representative_ref = proto_unwrap(src.id());
    }
    else if (src.Id_case() == Api::ContactRepresentative::ContactRepresentativeInfoRequest::kHistoryId)
    {
        dst.contact_representative_ref = proto_unwrap(src.history_id());
    }
    else if (src.Id_case() == Api::ContactRepresentative::ContactRepresentativeInfoRequest::kContactId)
    {
        dst.contact_representative_ref = Util::make_strong<ContactRepresentative::ContactId>(src.contact_id().uuid().value());
    }
    else
    {
        throw std::logic_error("unexpected oneof case in ContactRepresentativeInfoRequest");
    }
    return dst;
}

template<>
ContactRepresentative::CreateContactRepresentativeRequest proto_unwrap(const Api::ContactRepresentative::CreateContactRepresentativeRequest& src)
{
    ContactRepresentative::CreateContactRepresentativeRequest dst;

    dst.contact_id = Util::make_strong<ContactRepresentative::ContactId>(src.contact_id().uuid().value());
    dst.name = proto_unwrap(src.name());
    dst.organization = proto_unwrap(src.organization());
    if (src.has_place())
    {
        dst.place = proto_unwrap_into<PlaceAddress>(src.place());
    }
    if (src.has_telephone())
    {
        dst.telephone = proto_unwrap_into<PhoneNumber>(src.telephone());
    }
    if (src.has_email())
    {
        dst.email = Util::make_strong<EmailAddress>(src.email().value());
    }
    if (src.has_log_entry_id())
    {
        dst.log_entry_id = Util::make_strong<LogEntryId>(src.log_entry_id().value());
    }
    return dst;
}

template<>
ContactRepresentative::UpdateContactRepresentativeRequest proto_unwrap(const Api::ContactRepresentative::UpdateContactRepresentativeRequest& src)
{
    ContactRepresentative::UpdateContactRepresentativeRequest dst;
    dst.id = proto_unwrap(src.id());
    dst.name = proto_unwrap(src.name());
    dst.organization = proto_unwrap(src.organization());
    if (src.has_place())
    {
        dst.place = proto_unwrap_into<PlaceAddress>(src.place());
    }
    if (src.has_telephone())
    {
        dst.telephone = proto_unwrap_into<PhoneNumber>(src.telephone());
    }
    if (src.has_email())
    {
        dst.email = Util::make_strong<EmailAddress>(src.email().value());
    }
    if (src.has_log_entry_id())
    {
        dst.log_entry_id = Util::make_strong<LogEntryId>(src.log_entry_id().value());
    }
    return dst;
}

template<>
ContactRepresentative::DeleteContactRepresentativeRequest proto_unwrap(const Api::ContactRepresentative::DeleteContactRepresentativeRequest& src)
{
    ContactRepresentative::DeleteContactRepresentativeRequest dst;
    dst.id = proto_unwrap(src.id());
    if (src.has_log_entry_id())
    {
        dst.log_entry_id = Util::make_strong<LogEntryId>(src.log_entry_id().value());
    }
    return dst;
}

template <>
struct ProtoTypeTraits<Api::DomainBlacklist::BlockId>
{
    using RegistryType = DomainBlacklist::BlockId;
};

template <>
struct ProtoTypeTraits<Api::DomainBlacklist::RequestedBy>
{
    using RegistryType = DomainBlacklist::RequestedBy;
};

template <>
struct ProtoTypeTraits<Api::DomainBlacklist::CreateBlockData>
{
    using RegistryType = DomainBlacklist::CreateBlockData;
};

template <>
DomainBlacklist::BlockId proto_unwrap(const Api::DomainBlacklist::BlockId& src)
{
    return proto_unwrap_uuid<DomainBlacklist::BlockId>(src, [](const std::exception& e){throw std::runtime_error{std::string{"invalid block id: "} + e.what()}; });
}

template <>
DomainBlacklist::RequestedBy proto_unwrap(const Api::DomainBlacklist::RequestedBy& src)
{
    return Util::make_strong<DomainBlacklist::RequestedBy>(src.value());
}

DomainBlacklist::CreateBlockData proto_unwrap(const Api::DomainBlacklist::CreateBlockData& src, std::set<std::string>& invalid_fields)
{
    DomainBlacklist::CreateBlockData dst;

    if (src.Label_case() == Api::DomainBlacklist::CreateBlockData::kFqdn)
    {
        dst.fqdn = proto_unwrap(src.fqdn());
        dst.is_regex = false;
    }
    else if (src.Label_case() == Api::DomainBlacklist::CreateBlockData::kRegex)
    {
        dst.fqdn = proto_unwrap(src.regex());
        dst.is_regex = true;
    }
    else
    {
        invalid_fields.insert("fqdn");
        invalid_fields.insert("regex");
    }
    if (src.has_requested_by())
    {
        dst.requested_by = proto_unwrap(src.requested_by());
    }
    else
    {
        invalid_fields.insert("requested_by");
    }

    if (src.has_id())
    {
        try
        {
            dst.block_id = proto_unwrap(src.id());
        }
        catch (...)
        {
            invalid_fields.insert("id");
        }
    }
    if (src.has_valid_from())
    {
        dst.valid_from = unwrap_system_clock_nanoseconds(src.valid_from());
    }
    if (src.has_valid_to())
    {
        dst.valid_to = unwrap_system_clock_nanoseconds(src.valid_to());
    }
    dst.reason = proto_unwrap(src.reason());
    return dst;
}

template<>
DomainBlacklist::BlockInfoRequest proto_unwrap(const Api::DomainBlacklist::BlockInfoRequest& src)
{
    DomainBlacklist::BlockInfoRequest dst;

    try
    {
        dst.block_id = proto_unwrap(src.id());
    }
    catch (...)
    {
        std::set<std::string> invalid_fields{"id"};
        throw DomainBlacklist::BlockInfoReply::Exception::InvalidData(dst, invalid_fields);
    }
    return dst;
}

template<>
DomainBlacklist::CreateBlockRequest proto_unwrap(const Api::DomainBlacklist::CreateBlockRequest& src)
{
    DomainBlacklist::CreateBlockRequest dst;
    std::set<std::string> invalid_fields;

    if (src.has_block())
    {
        dst.block = proto_unwrap(src.block(), invalid_fields);
    }

    if (!invalid_fields.empty())
    {
        throw DomainBlacklist::CreateBlockReply::Exception::InvalidData(dst, invalid_fields);
    }
    return dst;
}

template<>
DomainBlacklist::DeleteBlockRequest proto_unwrap(const Api::DomainBlacklist::DeleteBlockRequest& src)
{
    DomainBlacklist::DeleteBlockRequest dst;
    std::set<std::string> invalid_fields;

    if (src.has_id())
    {
        try
        {
            dst.block_id = proto_unwrap(src.id());
        }
        catch (...)
        {
            invalid_fields.insert("id");
        }
    }
    else
    {
        invalid_fields.insert("id");
    }
    if (src.has_requested_by())
    {
        dst.requested_by = proto_unwrap(src.requested_by());
    }
    else
    {
        invalid_fields.insert("requested_by");
    }

    if (!invalid_fields.empty())
    {
        throw DomainBlacklist::DeleteBlockReply::Exception::InvalidData(dst, invalid_fields);
    }
    return dst;
}

}//namespace Fred::Registry
}//namespace Fred
