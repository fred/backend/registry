/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_groups_membership.hh"
#include "src/registrar/get_registrar_groups_membership.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/registrar/info_registrar.hh"
#include "util/db/db_exceptions.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

RegistrarGroupsMembership::RegistrarGroupsMembership(
        RegistrarHandle registrar_handle,
        std::set<GroupName> groups)
    : registrar_handle{std::move(registrar_handle)},
      groups{std::move(groups)}
{ }

std::string RegistrarGroupsMembership::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("groups", groups)
                                 .finish();
}

std::string GetRegistrarGroupsMembershipRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

GetRegistrarGroupsMembershipReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(
        const GetRegistrarGroupsMembershipRequest& src)
    : Registrar::Exception("registrar " + src.registrar_handle.to_string() + " does not exist")
{ }
}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

GetRegistrarGroupsMembershipReply::Data Fred::Registry::Registrar::get_registrar_groups_membership(
        const GetRegistrarGroupsMembershipRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto registrar_id = LibFred::InfoRegistrarByHandle{
                get_raw_value_from(request.registrar_handle)}
                        .exec(ctx).info_registrar_data.id;
        auto result = get_registrar_groups_membership(
                ctx,
                request.registrar_handle,
                registrar_id);
        ctx.commit_transaction();
        return result;
    }
    catch (const LibFred::InfoRegistrarByHandle::Exception& e)
    {
        if (e.is_set_unknown_registrar_handle())
        {
            FREDLOG_INFO(boost::format{"registrar %1% not found"} % e.get_unknown_registrar_handle());
            throw GetRegistrarGroupsMembershipReply::Exception::RegistrarDoesNotExist{request};
        }
        FREDLOG_INFO(boost::format{"InfoRegistrarByHandle::Exception caught: %1%"} % e.what());
        throw InternalServerError("InfoRegistrarByHandle::Exception");
    }
    catch (const RegistrarDoesNotExist& e)
    {
        FREDLOG_INFO(boost::format{"registrar %1% not found: %2%"} % get_raw_value_from(request.registrar_handle) % e.what());
        throw GetRegistrarGroupsMembershipReply::Exception::RegistrarDoesNotExist{request};
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_INFO(boost::format{"Database::Exception caught: %1%"} % e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(boost::format{"std::exception caught: %1%"} % e.what());
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        FREDLOG_INFO("unknown exception caught");
        throw InternalServerError("unknown exception");
    }
}
