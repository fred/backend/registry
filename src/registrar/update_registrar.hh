/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_REGISTRAR_HH_50101F4BA5539A3F6F38BAEA3D0D4E9C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_REGISTRAR_HH_50101F4BA5539A3F6F38BAEA3D0D4E9C

#include "src/exceptions.hh"
#include "src/object/object_common_types.hh"
#include "src/util/printable.hh"
#include "src/registrar/registrar_common_types.hh"

#include <boost/optional.hpp>

#include <set>
#include <vector>

namespace Fred {
namespace Registry {
namespace Registrar {

struct SetEmailAddresses : Util::Printable<SetEmailAddresses>
{
    std::vector<EmailAddress> values;
    std::string to_string() const;
};

struct UpdateRegistrarRequest : Util::Printable<UpdateRegistrarRequest>
{
    RegistrarHandle registrar_handle;

    boost::optional<std::string> set_name;
    boost::optional<std::string> set_organization;
    boost::optional<PlaceAddress> set_place;
    boost::optional<PhoneNumber> set_telephone;
    boost::optional<PhoneNumber> set_fax;
    boost::optional<SetEmailAddresses> set_emails;
    boost::optional<Url> set_url;
    boost::optional<bool> set_is_system_registrar;
    boost::optional<CompanyRegistrationNumber> set_company_registration_number;
    boost::optional<VatIdentificationNumber> set_vat_identification_number;
    boost::optional<std::string> set_variable_symbol;
    boost::optional<std::string> set_payment_memo_regex;
    boost::optional<bool> set_is_vat_payer;
    boost::optional<bool> set_is_internal;
    boost::optional<RegistrarHandle> set_registrar_handle;

    std::string to_string() const;
};

struct UpdateRegistrarReply
{
    struct Data : Util::Printable<Data>
    {
        RegistrarHandle registrar_handle;

        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registry::Exception
        {
            explicit RegistrarDoesNotExist(const UpdateRegistrarRequest& src);
        };
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const UpdateRegistrarRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

UpdateRegistrarReply::Data update_registrar(const UpdateRegistrarRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//UPDATE_REGISTRAR_HH_50101F4BA5539A3F6F38BAEA3D0D4E9C
