/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_epp_credentials.hh"

#include "src/fred_unwrap.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "util/db/db_exceptions.hh"

#include "libfred/registrar/epp_auth/get_registrar_epp_auth.hh"

#include <algorithm>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string GetRegistrarEppCredentialsRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

std::string GetRegistrarEppCredentialsReply::Data::Credentials::to_string() const
{
    return Util::StructToString().add("credentials_id", credentials_id)
                                 .add("create_time", create_time)
                                 .add("certificate", certificate)
                                 .finish();
}

std::string GetRegistrarEppCredentialsReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("records", credentials)
                                 .finish();
}

GetRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const GetRegistrarEppCredentialsRequest& src)
    : Registrar::Exception("registrar " + src.to_string() + " does not exist")
{
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

GetRegistrarEppCredentialsReply::Data Fred::Registry::Registrar::registrar_epp_credentials(const GetRegistrarEppCredentialsRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto epp_auth_list = LibFred::Registrar::EppAuth::GetRegistrarEppAuth{
                get_raw_value_from(request.registrar_handle)}.exec(ctx);
        GetRegistrarEppCredentialsReply::Data result;
        result.credentials.reserve(epp_auth_list.epp_auth_records.size());
        std::for_each(begin(epp_auth_list.epp_auth_records), end(epp_auth_list.epp_auth_records), [&](auto&& record)
        {
            result.credentials.push_back(fred_unwrap(record));
        });
        result.registrar_handle = Util::make_strong<RegistrarHandle>(epp_auth_list.registrar_handle);
        return result;
    }
    catch (const LibFred::OperationException& e)
    {
        FREDLOG_INFO(boost::format{"LibFred::OperationException caught: %1%"} % e.what());
        throw GetRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist(request);
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_INFO(boost::format{"Database::Exception caught: %1%"} % e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(boost::format{"std::exception caught: %1%"} % e.what());
        throw InternalServerError("std::exception caught");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception caught");
    }
}
