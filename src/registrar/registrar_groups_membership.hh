/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRAR_GROUPS_MEMBERSHIP_HH_1A5E5C3A9FBDDF7ABBB078467E042924//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_GROUPS_MEMBERSHIP_HH_1A5E5C3A9FBDDF7ABBB078467E042924

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

using GroupName = Util::StrongString<struct GroupName_Tag, Util::Skill::Printable, Util::Skill::HasOperatorLess>;

struct RegistrarGroupsMembership : Util::Printable<RegistrarGroupsMembership>
{
    explicit RegistrarGroupsMembership(
            RegistrarHandle registrar_handle,
            std::set<GroupName> groups);
    RegistrarHandle registrar_handle;
    std::set<GroupName> groups;
    std::string to_string() const;
};

struct GetRegistrarGroupsMembershipRequest : Util::Printable<GetRegistrarGroupsMembershipRequest>
{
    RegistrarHandle registrar_handle;
    std::string to_string() const;
};

struct GetRegistrarGroupsMembershipReply
{
    using Data = RegistrarGroupsMembership;
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const GetRegistrarGroupsMembershipRequest& src);
        };
    };
};

GetRegistrarGroupsMembershipReply::Data get_registrar_groups_membership(const GetRegistrarGroupsMembershipRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_GROUPS_MEMBERSHIP_HH_1A5E5C3A9FBDDF7ABBB078467E042924
