/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRAR_EPP_CREDENTIALS_HH_67A4A14F9339133219AF04B80B79DDD7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_EPP_CREDENTIALS_HH_67A4A14F9339133219AF04B80B79DDD7

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <vector>

namespace Fred {
namespace Registry {
namespace Registrar {

struct GetRegistrarEppCredentialsRequest : Util::Printable<GetRegistrarEppCredentialsRequest>
{
    RegistrarHandle registrar_handle;
    std::string to_string() const;
};

struct GetRegistrarEppCredentialsReply
{
    struct Data : Util::Printable<Data>
    {
        struct Credentials : Util::Printable<Credentials>
        {
            using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
            RegistrarEppCredentialsId credentials_id;
            boost::optional<TimePoint> create_time;
            SslCertificate certificate;
            std::string to_string() const;
        };
        RegistrarHandle registrar_handle;
        std::vector<Credentials> credentials;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const GetRegistrarEppCredentialsRequest& src);
        };
    };
};

GetRegistrarEppCredentialsReply::Data registrar_epp_credentials(const GetRegistrarEppCredentialsRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_EPP_CREDENTIALS_HH_67A4A14F9339133219AF04B80B79DDD7
