/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_REGISTRAR_HH_F957BBED19C731FF3E67D418C04D6AF0//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CREATE_REGISTRAR_HH_F957BBED19C731FF3E67D418C04D6AF0

#include "src/exceptions.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <vector>

namespace Fred {
namespace Registry {
namespace Registrar {

struct CreateRegistrarRequest : Util::Printable<CreateRegistrarRequest>
{
    RegistrarHandle registrar_handle;
    std::string name;
    std::string organization;
    PlaceAddress place;
    boost::optional<PhoneNumber> telephone;
    boost::optional<PhoneNumber> fax;
    std::vector<EmailAddress> emails;
    Url url;
    bool is_system_registrar;
    boost::optional<CompanyRegistrationNumber> company_registration_number;
    VatIdentificationNumber vat_identification_number;
    std::string variable_symbol;
    std::string payment_memo_regex;
    bool is_vat_payer;
    bool is_internal;
    std::string to_string() const;
};

struct CreateRegistrarReply
{
    struct Data : Util::Printable<Data>
    {
        RegistrarHandle registrar_handle;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarAlreadyExists : Registry::Exception
        {
            explicit RegistrarAlreadyExists(const CreateRegistrarRequest& src);
        };
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const CreateRegistrarRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

CreateRegistrarReply::Data create_registrar(const CreateRegistrarRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//CREATE_REGISTRAR_HH_F957BBED19C731FF3E67D418C04D6AF0
