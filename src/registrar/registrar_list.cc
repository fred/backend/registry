/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_list.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/registrar/get_registrar_handles.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Registrar {

std::string RegistrarListReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handles", registrar_handles)
                                 .finish();
}

namespace {

template <typename T, typename S>
T fred_unwrap_into(const S&);

template <>
std::vector<RegistrarHandle> fred_unwrap_into(const std::vector<std::string>& src)
{
    std::vector<RegistrarHandle> dst;
    dst.reserve(src.size());
    for (const auto& handle : src)
    {
        dst.push_back(Util::make_strong<RegistrarHandle>(handle));
    }
    return dst;
}

}//namespace Fred::Registry::Registrar::{anonymous}

RegistrarListReply::Data registrar_list()
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx(ctx_provider);
        RegistrarListReply::Data result;
        result.registrar_handles = fred_unwrap_into<std::vector<RegistrarHandle>>(
                LibFred::Registrar::GetRegistrarHandles().exec(ctx));
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError("expected unknown exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred
