/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/delete_registrar_epp_credentials.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/epp_auth/delete_registrar_epp_auth.hh"
#include "libfred/registrar/epp_auth/exceptions.hh"
#include "util/db/db_exceptions.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string DeleteRegistrarEppCredentialsRequest::to_string() const
{
    return Util::StructToString().add("credentials_id", credentials_id)
                                 .finish();
}

DeleteRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist::RegistrarEppCredentialsDoesNotExist(const DeleteRegistrarEppCredentialsRequest& src)
    : Registrar::Exception("registrar epp credentials " + src.credentials_id.to_string() + " does not exist")
{
}

DeleteRegistrarEppCredentialsReply::Exception::InvalidData::InvalidData(
        const DeleteRegistrarEppCredentialsRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

void Fred::Registry::Registrar::delete_registrar_epp_credentials(
        const DeleteRegistrarEppCredentialsRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        LibFred::Registrar::EppAuth::DeleteRegistrarEppAuth{fred_wrap(request.credentials_id)}.exec(ctx);
        ctx.commit_transaction();
    }
    catch (const LibFred::Registrar::EppAuth::NonexistentRegistrarEppAuth&)
    {
        throw DeleteRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist{request};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
