/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_credit.hh"

#include "src/fred_unwrap.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "util/db/db_exceptions.hh"

#include "libfred/registrar/get_registrar_zone_credit.hh"

namespace Fred {
namespace Registry {
namespace Registrar {

std::string RegistrarCreditRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

std::string RegistrarCreditReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("credit_by_zone", credit_by_zone)
                                 .finish();
}

RegistrarCreditReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const RegistrarCreditRequest& src)
    : Registrar::Exception("registrar " + src.to_string() + " does not exist")
{
}

RegistrarCreditReply::Data registrar_credit(const RegistrarCreditRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        RegistrarCreditReply::Data result;
        result.credit_by_zone =
                fred_unwrap(LibFred::GetRegistrarZoneCredit().exec(ctx, get_raw_value_from(request.registrar_handle)));
        for (auto& zone_credit : result.credit_by_zone)
        {
            if (zone_credit.second == boost::none)
            {
                static const auto no_credit_as_zero_value = Util::make_strong<Credit>(std::string("0.0"));
                zone_credit.second = no_credit_as_zero_value;
            }
        }
        result.registrar_handle = request.registrar_handle;
        return result;
    }
    catch (const LibFred::OperationException& e)
    {
        throw RegistrarCreditReply::Exception::RegistrarDoesNotExist(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError("expected unknown exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred
