/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/update_registrar_zone_access.hh"
#include "src/registrar/info_registrar_zone_access.hh"

#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/zone_access/exceptions.hh"
#include "libfred/registrar/zone_access/update_registrar_zone_access.hh"
#include "util/db/db_exceptions.hh"

#include <boost/date_time/gregorian/gregorian.hpp>

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

UpdateRegistrarZoneAccessRequest::UpdateRegistrarZoneAccessRequest(RegistrarZoneAccessId access_id)
    : access_id{std::move(access_id)},
      set_valid_from{boost::none},
      set_valid_to{boost::none}
{ }

std::string UpdateRegistrarZoneAccessRequest::to_string() const
{
    return Util::StructToString().add("access_id", access_id)
                                 .add("set_valid_from", set_valid_from)
                                 .add("set_valid_to", set_valid_to)
                                 .finish();
}

UpdateRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist::RegistrarZoneAccessDoesNotExist(
        const UpdateRegistrarZoneAccessRequest& src)
    : Registrar::Exception("registrar zone access " + src.access_id.to_string() + " does not exist")
{
}

UpdateRegistrarZoneAccessReply::Exception::HistoryChangeProhibited::HistoryChangeProhibited(
        const UpdateRegistrarZoneAccessRequest& src)
    : Registrar::Exception("history " + src.access_id.to_string() + " change prohibited")
{
}

UpdateRegistrarZoneAccessReply::Exception::InvalidData::InvalidData(
        const UpdateRegistrarZoneAccessRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

namespace {

auto make_access(
        const LibFred::OperationContextCreator& ctx,
        const UpdateRegistrarZoneAccessRequest& request,
        unsigned long long access_id)
{
    std::string sql =
            "WITH tz AS "
            "("
                "SELECT val AS value, "
                       "(NOW() AT TIME ZONE val)::DATE AS today "
                  "FROM enum_parameters "
                 "WHERE name = 'regular_day_procedure_zone'"
            "), "
            "access AS "
            "("
                "SELECT ";
    Database::QueryParams params{access_id};
    if (request.set_valid_from == boost::none)
    {
        sql += "tz.today AS valid_from, ";
    }
    else
    {
        params.push_back(Fred::Registry::Util::Into<std::string>::from(*request.set_valid_from));
        sql += "($2::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE tz.value)::DATE AS valid_from, ";
    }
    if (request.set_valid_to != boost::none)
    {
        params.push_back(Fred::Registry::Util::Into<std::string>::from(*request.set_valid_to));
        sql += "($" + std::to_string(params.size()) + "::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE tz.value)::DATE - 1 AS valid_to ";
    }
    else
    {
        sql += "NULL::DATE AS valid_to ";
    }
    sql += "FROM tz) "
            "SELECT access.valid_from, "
                   "access.valid_to, "
                   "ri.fromdate < tz.today AND access.valid_from != ri.fromdate OR "
                   "ri.todate IS NOT NULL AND ri.todate < tz.today, "
                   "access.valid_from <= access.valid_to "
              "FROM tz, "
                   "access, "
                   "registrarinvoice ri "
             "WHERE ri.id = $1::INT";
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    if (dbres.size() != 1)
    {
        struct UnexpectedResult : std::exception
        {
            const char* what() const noexcept override { return "query should return just 1 row"; }
        };
        throw UnexpectedResult{};
    }
    const auto history_change_violation = static_cast<bool>(dbres[0][2]);
    if (history_change_violation)
    {
        throw UpdateRegistrarZoneAccessReply::Exception::HistoryChangeProhibited{request};
    }
    const auto borders_are_in_chronological_order = dbres[0][3].isnull() || static_cast<bool>(dbres[0][3]);
    if (!borders_are_in_chronological_order)
    {
        throw UpdateRegistrarZoneAccessReply::Exception::InvalidData{request, {"set_valid_from & set_valid_to"}};
    }
    struct Access
    {
        boost::gregorian::date from_date;
        boost::optional<boost::gregorian::date> to_date;
    };
    return Access{
            boost::gregorian::from_string(static_cast<std::string>(dbres[0][0])),
            dbres[0][1].isnull() ? boost::optional<boost::gregorian::date>{}
                                 : boost::gregorian::from_string(static_cast<std::string>(dbres[0][1]))};
}

}//namespace {anonymous}

UpdateRegistrarZoneAccessReply::Data Fred::Registry::Registrar::update_registrar_zone_access(
        const UpdateRegistrarZoneAccessRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto access_id = info_registrar_zone_access(ctx, request.access_id).id;
        const auto access = make_access(ctx, request, access_id);
        LibFred::Registrar::ZoneAccess::UpdateRegistrarZoneAccess{access_id}
                .set_from_date(access.from_date)
                .set_to_date(access.to_date)
                .exec(ctx);
        auto result = info_registrar_zone_access(ctx, access_id);
        ctx.commit_transaction();
        return result;
    }
    catch (const RegistrarZoneAccessDoesNotExist&)
    {
        throw UpdateRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist{request};
    }
    catch (const UpdateRegistrarZoneAccessReply::Exception::HistoryChangeProhibited&)
    {
        throw;
    }
    catch (const UpdateRegistrarZoneAccessReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
