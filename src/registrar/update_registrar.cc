/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/update_registrar.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/contact_verification/django_email_format.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrar/exceptions.hh"
#include "libfred/registrar/info_registrar.hh"
#include "libfred/registrar/update_registrar.hh"
#include "util/db/db_exceptions.hh"

#include "util/log/log.hh"

#include <cstring>
#include <map>
#include <numeric>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

namespace {

template <typename T, typename Fnc>
void exec_if_is_present(const boost::optional<T>& data, Fnc&& fnc);

template <typename T, typename N, template <typename> class ...Skills, typename Fnc>
void exec_if_is_present(const boost::optional<Fred::Registry::Util::StrongType<T, N, Skills...>>& value, Fnc&& fnc);

template <typename T, typename Fnc>
void exec_if_is_present(const boost::optional<T>& data, Fnc&& fnc)
{
    if (data != boost::none)
    {
        std::forward<Fnc>(fnc)(*data);
    }
}

template <typename T, typename N, template <typename> class ...Skills, typename Fnc>
void exec_if_is_present(const boost::optional<Fred::Registry::Util::StrongType<T, N, Skills...>>& data, Fnc&& fnc)
{
    if (data != boost::none)
    {
        std::forward<Fnc>(fnc)(get_raw_value_from(*data));
    }
}

template <typename T, typename Fnc>
void exec_if_not_empty(T&& value, Fnc&& fnc);

template <typename T, typename Fnc>
void exec_if_not_empty(const boost::optional<T>& value, Fnc&& fnc);

template <typename T, typename N, template <typename> class ...Skills, typename Fnc>
void exec_if_not_empty(const Fred::Registry::Util::StrongType<T, N, Skills...>& value, Fnc&& fnc);

template <typename T, typename Fnc>
void exec_if_not_empty(T&& value, Fnc&& fnc)
{
    if (!value.empty())
    {
        std::forward<Fnc>(fnc)(std::forward<T>(value));
    }
}

template <typename T, typename Fnc>
void exec_if_not_empty(const boost::optional<T>& value, Fnc&& fnc)
{
    if (value != boost::none)
    {
        exec_if_not_empty(*value, std::forward<Fnc>(fnc));
    }
}

template <typename T, typename N, template <typename> class ...Skills, typename Fnc>
void exec_if_not_empty(const Fred::Registry::Util::StrongType<T, N, Skills...>& value, Fnc&& fnc)
{
    exec_if_not_empty(get_raw_value_from(value), std::forward<Fnc>(fnc));
}

struct InvalidEmail : std::exception
{
    const char* what() const noexcept override { return "invalid email format"; }
};

struct NoEmail : std::exception
{
    const char* what() const noexcept override { return "no email"; }
};

const std::string& get_valid_email(const EmailAddress& email)
{
    const auto& raw_email = get_raw_value_from(email);
    static DjangoEmailFormat django_email_format{};//constructor is not constexpr and method check is not const!
    const bool email_is_valid = django_email_format.check(raw_email);
    if (!email_is_valid)
    {
        FREDLOG_INFO("email \"" + raw_email + "\" is not valid");
        throw InvalidEmail{};
    }
    return raw_email;
}

decltype(auto) join_emails(const std::vector<EmailAddress>& addresses)
{
    std::string result;
    auto comma_delimited_emails = std::accumulate(
            next(begin(addresses)),
            end(addresses),
            get_valid_email(addresses.front()),
            [](std::string sum, auto&& a)
            {
                return std::move(sum) + ", " + get_valid_email(a);
            });
    return comma_delimited_emails;
}

[[ noreturn ]] void throw_invalid_data(
        const UpdateRegistrarRequest& request,
        const LibFred::Registrar::InvalidAttribute& e)
{
    struct CStringLess
    {
        bool operator()(const char* lhs, const char* rhs) const noexcept { return std::strcmp(lhs, rhs) < 0; }
    };
    using StringToString = std::map<const char*, const char*, CStringLess>;
    static const StringToString translate = {
            {"handle", "set_registrar_handle"},
            {"dic", "set_vat_identification_number"},
            {"name", "set_name"},
            {"organization", "set_organization"},
            {"street", "set_place.street"},
            {"city", "set_place.city"},
            {"postal_code", "set_place.postal_code"},
            {"telephone", "set_telephone"},
            {"email", "set_emails"},
            {"url", "set_url"}};
    const auto iter = translate.find(e.attribute_name());
    if (iter != end(translate))
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {iter->second});
    }
    throw UpdateRegistrarReply::Exception::InvalidData(request, {});
}

}//namespace Fred::Registry::Registrar::{anonymous}

std::string SetEmailAddresses::to_string() const
{
    return Util::StructToString().add("values", values)
        .finish();
}

std::string UpdateRegistrarRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("set_name", set_name)
                                 .add("set_organization", set_organization)
                                 .add("set_place", set_place)
                                 .add("set_telephone", set_telephone)
                                 .add("set_fax", set_fax)
                                 .add("set_emails", set_emails)
                                 .add("set_url", set_url)
                                 .add("set_is_system_registrar", set_is_system_registrar)
                                 .add("set_company_registration_number", set_company_registration_number)
                                 .add("set_vat_identification_number", set_vat_identification_number)
                                 .add("set_variable_symbol", set_variable_symbol)
                                 .add("set_payment_memo_regex", set_payment_memo_regex)
                                 .add("set_is_vat_payer", set_is_vat_payer)
                                 .add("set_is_internal", set_is_internal)
                                 .add("set_registrar_handle", set_registrar_handle)
                                 .finish();
}

std::string UpdateRegistrarReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

UpdateRegistrarReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const UpdateRegistrarRequest& src)
    : Registry::Exception("registrar " + get_raw_value_from(src.registrar_handle) + " does not exist")
{ }

UpdateRegistrarReply::Exception::InvalidData::InvalidData(const UpdateRegistrarRequest&, std::set<std::string> fields)
    : Registry::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

UpdateRegistrarReply::Data Fred::Registry::Registrar::update_registrar(const UpdateRegistrarRequest& request)
{
    FREDLOG_DEBUG("update_registrar call");
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto registrar_id = LibFred::InfoRegistrarByHandle{get_raw_value_from(request.registrar_handle)}
                .exec(ctx).info_registrar_data.id;
        LibFred::Registrar::UpdateRegistrarById update_registrar_op{registrar_id};
        exec_if_is_present(request.set_name, [&](const std::string& value)
        {
            update_registrar_op.set_name(value);
        });
        exec_if_is_present(request.set_organization, [&](const std::string& value)
        {
            update_registrar_op.set_organization(value);
        });
        exec_if_is_present(request.set_place, [&](const PlaceAddress& address)
        {
            try
            {
                update_registrar_op.set_street(address.street);
            }
            catch (const LibFred::Registrar::UpdateRegistrarException&)
            {
                throw UpdateRegistrarReply::Exception::InvalidData(request, {"set_place.street"});
            }
            exec_if_not_empty(address.city, [&](auto&& value)
            {
                update_registrar_op.set_city(value);
            });
            exec_if_not_empty(address.state_or_province, [&](auto&& value)
            {
                update_registrar_op.set_state_or_province(value);
            });
            exec_if_not_empty(address.postal_code, [&](auto&& value)
            {
                update_registrar_op.set_postal_code(value);
            });
            exec_if_not_empty(address.country_code, [&](auto&& value)
            {
                update_registrar_op.set_country(value);
            });
        });
        exec_if_is_present(request.set_telephone, [&](const std::string& value)
        {
            update_registrar_op.set_telephone(value);
        });
        exec_if_is_present(request.set_fax, [&](const std::string& value)
        {
            update_registrar_op.set_fax(value);
        });
        exec_if_is_present(request.set_emails, [&](const SetEmailAddresses& to_set)
        {
            if (to_set.values.empty())
            {
                throw NoEmail{};
            }
            update_registrar_op.set_email(join_emails(to_set.values));
        });
        exec_if_is_present(request.set_url, [&](const std::string& value)
        {
            update_registrar_op.set_url(value);
        });
        exec_if_is_present(request.set_is_system_registrar, [&](bool value)
        {
            update_registrar_op.set_system(value);
        });
        exec_if_is_present(request.set_company_registration_number, [&](const std::string& value)
        {
            update_registrar_op.set_ico(value);
        });
        exec_if_is_present(request.set_vat_identification_number, [&](const std::string& value)
        {
            update_registrar_op.set_dic(value);
        });
        exec_if_is_present(request.set_variable_symbol, [&](const std::string& value)
        {
            update_registrar_op.set_variable_symbol(value);
        });
        exec_if_is_present(request.set_payment_memo_regex, [&](const std::string& value)
        {
            update_registrar_op.set_payment_memo_regex(value);
        });
        exec_if_is_present(request.set_is_vat_payer, [&](bool value)
        {
            update_registrar_op.set_vat_payer(value);
        });
        exec_if_is_present(request.set_is_internal, [&](bool value)
        {
            update_registrar_op.set_internal(value);
        });
        exec_if_is_present(request.set_registrar_handle, [&](const std::string& value)
        {
            update_registrar_op.set_handle(value);
        });
        update_registrar_op.exec(ctx);
        UpdateRegistrarReply::Data result;
        result.registrar_handle = request.registrar_handle;
        ctx.commit_transaction();
        return result;
    }
    catch (const UpdateRegistrarReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const InvalidEmail&)
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {"set_emails.values"});
    }
    catch (const NoEmail&)
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {"set_emails"});
    }
    catch (const LibFred::InfoRegistrarByHandle::Exception& e)
    {
        if (e.is_set_unknown_registrar_handle())
        {
            throw UpdateRegistrarReply::Exception::RegistrarDoesNotExist(request);
        }
        throw InternalServerError("unexpected problem signalized by LibFred::InfoRegistrarByHandle::Exception");
    }
    catch (const LibFred::Registrar::InvalidAttribute& e)
    {
        throw_invalid_data(request, e);
    }
    catch (const LibFred::Registrar::NoUpdateData&)
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {});
    }
    catch (const LibFred::Registrar::UnknownCountryCode&)
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {"set_place.country_code"});
    }
    catch (const LibFred::Registrar::NonExistentRegistrar&)
    {
        throw UpdateRegistrarReply::Exception::RegistrarDoesNotExist(request);
    }
    catch (const LibFred::Registrar::RegistrarHandleAlreadyExists&)
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {"set_registrar_handle"});
    }
    catch (const LibFred::Registrar::VariableSymbolAlreadyExists&)
    {
        throw UpdateRegistrarReply::Exception::InvalidData(request, {"set_variable_symbol"});
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_INFO(boost::format{"Database::Exception caught: %1%"} % e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(std::string{"std::exception: "} + e.what());
        throw InternalServerError("unknown exception derived from std::exception");
    }
    catch (...)
    {
        FREDLOG_INFO("unknown exception");
        throw InternalServerError("unknown exception");
    }
}
