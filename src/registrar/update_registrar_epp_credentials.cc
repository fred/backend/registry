/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/update_registrar_epp_credentials.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/epp_auth/exceptions.hh"
#include "libfred/registrar/epp_auth/update_registrar_epp_auth.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string UpdateRegistrarEppCredentialsRequest::to_string() const
{
    return Util::StructToString().add("credentials_id", credentials_id)
                                 .add("password", password)
                                 .finish();
}

UpdateRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist::RegistrarEppCredentialsDoesNotExist(const UpdateRegistrarEppCredentialsRequest& src)
    : Registrar::Exception("registrar epp credentials " + src.credentials_id.to_string() + " does not exist")
{
}

UpdateRegistrarEppCredentialsReply::Exception::InvalidData::InvalidData(
        const UpdateRegistrarEppCredentialsRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

void Fred::Registry::Registrar::update_registrar_epp_credentials(const UpdateRegistrarEppCredentialsRequest& request)
{
    if (request.password.empty())
    {
        throw UpdateRegistrarEppCredentialsReply::Exception::InvalidData{request, {"password"}};
    }
    try
    {
        LibFred::OperationContextCreator ctx;
        LibFred::Registrar::EppAuth::update_registrar_epp_auth(
                ctx,
                fred_wrap(request.credentials_id),
                boost::none,  // certificate_fingerprint
                request.password,
                boost::none); // certificate_data_pem
        ctx.commit_transaction();
    }
    catch (const LibFred::Registrar::EppAuth::NonexistentRegistrarEppAuth&)
    {
        throw UpdateRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist{request};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
