/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRAR_CERTIFICATIONS_HH_EBE8C79E2522F81C453DEE1C32078ADA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_CERTIFICATIONS_HH_EBE8C79E2522F81C453DEE1C32078ADA

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"

#include <vector>

namespace Fred {
namespace Registry {
namespace Registrar {

struct GetRegistrarCertificationsRequest : Util::Printable<GetRegistrarCertificationsRequest>
{
    RegistrarHandle registrar_handle;
    std::string to_string() const;
};

struct GetRegistrarCertificationsReply
{
    struct Data : Util::Printable<Data>
    {
        std::vector<RegistrarCertification> certifications;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const GetRegistrarCertificationsRequest& src);
        };
    };
};

GetRegistrarCertificationsReply::Data get_registrar_certifications(const GetRegistrarCertificationsRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_CERTIFICATIONS_HH_EBE8C79E2522F81C453DEE1C32078ADA
