/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INFO_REGISTRAR_ZONE_ACCESS_HH_A1642CD73BC65C8B850125D8076F5FDC//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define INFO_REGISTRAR_ZONE_ACCESS_HH_A1642CD73BC65C8B850125D8076F5FDC

#include "src/registrar/registrar_zone_access.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include "libfred/opcontext.hh"

#include <exception>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct InfoRegistrarZoneAccess : Util::Printable<InfoRegistrarZoneAccess>
{
    explicit InfoRegistrarZoneAccess(
            unsigned long long id,
            RegistrarHandle registrar_handle,
            std::string zone,
            Period access,
            bool valid_before_now,
            bool valid_now);
    unsigned long long id;
    RegistrarHandle registrar_handle;
    std::string zone;
    Period access;
    bool valid_before_now;
    bool valid_now;
    std::string to_string() const;
};

struct RegistrarZoneAccessId : Util::Printable<RegistrarZoneAccessId>
{
    explicit RegistrarZoneAccessId(
            RegistrarHandle registrar_handle,
            std::string zone,
            Period::TimePoint valid_from);
    RegistrarHandle registrar_handle;
    std::string zone;
    Period::TimePoint valid_from;
    std::string to_string() const;
};

struct RegistrarZoneAccessDoesNotExist : std::exception
{
    const char* what() const noexcept override;
};

RegistrarZoneAccess info_registrar_zone_access(
        const LibFred::OperationContextCreator& ctx,
        unsigned long long access_id);

InfoRegistrarZoneAccess info_registrar_zone_access(
        const LibFred::OperationContextCreator& ctx,
        const RegistrarZoneAccessId& access_id);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//INFO_REGISTRAR_ZONE_ACCESS_HH_A1642CD73BC65C8B850125D8076F5FDC
