/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_info.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/registrar/info_registrar.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Registrar {

std::string RegistrarInfoRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

std::string RegistrarInfoReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("registrar_id", registrar_id)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .add("place", place)
                                 .add("telephone", telephone)
                                 .add("fax", fax)
                                 .add("emails", emails)
                                 .add("url", url)
                                 .add("is_system_registrar", is_system_registrar)
                                 .add("company_registration_number", company_registration_number)
                                 .add("vat_identification_number", vat_identification_number)
                                 .add("variable_symbol", variable_symbol)
                                 .add("payment_memo_regex", payment_memo_regex)
                                 .add("is_vat_payer", is_vat_payer)
                                 .add("is_internal", is_internal)
                                 .finish();
}

RegistrarInfoReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const RegistrarInfoRequest& src)
    : Registry::Exception("registrar " + src.to_string() + " does not exist")
{
}

RegistrarInfoReply::Data registrar_info(const RegistrarInfoRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx(ctx_provider);
        return fred_unwrap(LibFred::InfoRegistrarByHandle(get_raw_value_from(request.registrar_handle)).exec(ctx));
    }
    catch (const LibFred::InfoRegistrarByHandle::Exception& e)
    {
        if (e.is_set_unknown_registrar_handle())
        {
            throw RegistrarInfoReply::Exception::RegistrarDoesNotExist(request);
        }
        throw InternalServerError("unexpected problem signalized by LibFred::InfoRegistrarByHandle::Exception");
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError("expected unknown exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred
