/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_REGISTRAR_EPP_CREDENTIALS_HH_E20A1D381804746F944DE44077074098//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_REGISTRAR_EPP_CREDENTIALS_HH_E20A1D381804746F944DE44077074098

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct UpdateRegistrarEppCredentialsRequest : Util::Printable<UpdateRegistrarEppCredentialsRequest>
{
    RegistrarEppCredentialsId credentials_id;
    std::string password;
    std::string to_string() const;
};

struct UpdateRegistrarEppCredentialsReply
{
    struct Exception
    {
        struct RegistrarEppCredentialsDoesNotExist : Registrar::Exception
        {
            explicit RegistrarEppCredentialsDoesNotExist(const UpdateRegistrarEppCredentialsRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const UpdateRegistrarEppCredentialsRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

void update_registrar_epp_credentials(const UpdateRegistrarEppCredentialsRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//UPDATE_REGISTRAR_EPP_CREDENTIALS_HH_E20A1D381804746F944DE44077074098
