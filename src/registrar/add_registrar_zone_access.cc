/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/add_registrar_zone_access.hh"
#include "src/registrar/info_registrar_zone_access.hh"

#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/zone_access/add_registrar_zone_access.hh"
#include "libfred/registrar/zone_access/exceptions.hh"
#include "util/db/db_exceptions.hh"

#include <boost/date_time/gregorian/gregorian.hpp>

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string AddRegistrarZoneAccessRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("zone", zone)
                                 .add("valid_from", valid_from)
                                 .add("valid_to", valid_to)
                                 .finish();
}

AddRegistrarZoneAccessReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const AddRegistrarZoneAccessRequest& src)
    : Registrar::Exception("registrar " + src.registrar_handle.to_string() + " does not exist")
{
}

AddRegistrarZoneAccessReply::Exception::ZoneDoesNotExist::ZoneDoesNotExist(const AddRegistrarZoneAccessRequest& src)
    : Registrar::Exception("zone " + src.zone + " does not exist")
{
}

AddRegistrarZoneAccessReply::Exception::InvalidData::InvalidData(
        const AddRegistrarZoneAccessRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

namespace {

auto make_access(const LibFred::OperationContextCreator& ctx, const AddRegistrarZoneAccessRequest& request)
{
    std::string sql =
            "WITH tz AS "
            "("
                "SELECT val AS value "
                  "FROM enum_parameters "
                 "WHERE name = 'regular_day_procedure_zone'"
            "), "
            "access AS "
            "("
                "SELECT ";
    Database::QueryParams params;
    if (request.valid_from == boost::none)
    {
        sql += "(NOW() AT TIME ZONE tz.value)::DATE";
    }
    else
    {
        params.push_back(Fred::Registry::Util::Into<std::string>::from(*request.valid_from));
        sql += "($1::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE tz.value)::DATE";
    }
    sql += " AS valid_from";
    if (request.valid_to != boost::none)
    {
        params.push_back(Fred::Registry::Util::Into<std::string>::from(*request.valid_to));
        sql += ", ($" + std::to_string(params.size()) + "::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE tz.value)::DATE - 1";
    }
    else
    {
        sql += ", NULL::DATE";
    }
    sql += " AS valid_to "
                  "FROM tz"
            ") "
            "SELECT valid_from, "
                   "valid_to, "
                   "COALESCE(valid_from <= valid_to, true) "
              "FROM access";
    const auto dbres = params.empty() ? ctx.get_conn().exec(sql)
                                      : ctx.get_conn().exec_params(sql, params);
    if (dbres.size() != 1)
    {
        struct UnexpectedResult : std::exception
        {
            const char* what() const noexcept override { return "query should return just 1 row"; }
        };
        throw UnexpectedResult{};
    }
    const bool access_period_is_valid = static_cast<bool>(dbres[0][2]);
    if (!access_period_is_valid)
    {
        throw AddRegistrarZoneAccessReply::Exception::InvalidData{request, {"valid_from & valid_to"}};
    }
    struct Access
    {
        boost::gregorian::date from_date;
        boost::optional<boost::gregorian::date> to_date;
    };
    return Access{
            boost::gregorian::from_string(static_cast<std::string>(dbres[0][0])),
            request.valid_to == boost::none ? boost::optional<boost::gregorian::date>{}
                                            : boost::gregorian::from_string(static_cast<std::string>(dbres[0][1]))};
}

}//namespace {anonymous}

AddRegistrarZoneAccessReply::Data Fred::Registry::Registrar::add_registrar_zone_access(
        const AddRegistrarZoneAccessRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto access = make_access(ctx, request);
        const auto access_id = LibFred::Registrar::ZoneAccess::AddRegistrarZoneAccess{
                get_raw_value_from(request.registrar_handle),
                request.zone,
                access.from_date}.set_to_date(access.to_date).exec(ctx);
        auto result = info_registrar_zone_access(ctx, access_id);
        ctx.commit_transaction();
        return result;
    }
    catch (const LibFred::Registrar::ZoneAccess::NonexistentRegistrar&)
    {
        throw AddRegistrarZoneAccessReply::Exception::RegistrarDoesNotExist{request};
    }
    catch (const LibFred::Registrar::ZoneAccess::NonexistentZone&)
    {
        throw AddRegistrarZoneAccessReply::Exception::ZoneDoesNotExist{request};
    }
    catch (const LibFred::Registrar::ZoneAccess::OverlappingZoneAccessRange&)
    {
        throw AddRegistrarZoneAccessReply::Exception::InvalidData{request, {"valid_from & valid_to"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
