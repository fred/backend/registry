/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/delete_registrar_zone_access.hh"
#include "src/registrar/info_registrar_zone_access.hh"

#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/zone_access/delete_registrar_zone_access.hh"
#include "util/db/db_exceptions.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

DeleteRegistrarZoneAccessRequest::DeleteRegistrarZoneAccessRequest(RegistrarZoneAccessId access_id)
    : access_id{std::move(access_id)}
{ }

std::string DeleteRegistrarZoneAccessRequest::to_string() const
{
    return Util::StructToString().add("access_id", access_id)
                                 .finish();
}

DeleteRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist::RegistrarZoneAccessDoesNotExist(
        const DeleteRegistrarZoneAccessRequest& src)
    : Registrar::Exception("registrar zone access " + src.access_id.to_string() + " does not exist")
{
}

DeleteRegistrarZoneAccessReply::Exception::HistoryChangeProhibited::HistoryChangeProhibited(
        const DeleteRegistrarZoneAccessRequest& src)
    : Registrar::Exception("history " + src.access_id.to_string() + " change prohibited")
{
}

DeleteRegistrarZoneAccessReply::Exception::InvalidData::InvalidData(
        const DeleteRegistrarZoneAccessRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

void Fred::Registry::Registrar::delete_registrar_zone_access(
        const DeleteRegistrarZoneAccessRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto zone_access_data = info_registrar_zone_access(ctx, request.access_id);
        if (zone_access_data.valid_before_now)
        {
            throw DeleteRegistrarZoneAccessReply::Exception::HistoryChangeProhibited{request};
        }
        LibFred::Registrar::ZoneAccess::DeleteRegistrarZoneAccess{zone_access_data.id}.exec(ctx);
        ctx.commit_transaction();
        return;
    }
    catch (const RegistrarZoneAccessDoesNotExist&)
    {
        throw DeleteRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist{request};
    }
    catch (const DeleteRegistrarZoneAccessReply::Exception::HistoryChangeProhibited&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
