/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef REGISTRAR_INFO_HH_61CD7344F393E2EA26DB687C873A09A9//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_INFO_HH_61CD7344F393E2EA26DB687C873A09A9

#include "src/exceptions.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <vector>

namespace Fred {
namespace Registry {
namespace Registrar {

struct RegistrarInfoRequest : Util::Printable<RegistrarInfoRequest>
{
    RegistrarHandle registrar_handle;
    std::string to_string() const;
};

struct RegistrarInfoReply
{
    struct Data : Util::Printable<Data>
    {
        RegistrarHandle registrar_handle;
        RegistrarId registrar_id;
        std::string name;
        std::string organization;
        boost::optional<PlaceAddress> place;
        boost::optional<PhoneNumber> telephone;
        boost::optional<PhoneNumber> fax;
        std::vector<EmailAddress> emails;
        boost::optional<Url> url;
        bool is_system_registrar;
        boost::optional<CompanyRegistrationNumber> company_registration_number;
        boost::optional<VatIdentificationNumber> vat_identification_number;
        std::string variable_symbol;
        std::string payment_memo_regex;
        bool is_vat_payer;
        bool is_internal;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registry::Exception
        {
            explicit RegistrarDoesNotExist(const RegistrarInfoRequest& src);
        };
    };
};

RegistrarInfoReply::Data registrar_info(const RegistrarInfoRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_INFO_HH_61CD7344F393E2EA26DB687C873A09A9
