/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_REGISTRAR_ZONE_ACCESS_HH_BC8F221C95CDBEC1C963BDCDC7D95B84//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_REGISTRAR_ZONE_ACCESS_HH_BC8F221C95CDBEC1C963BDCDC7D95B84

#include "src/registrar/info_registrar_zone_access.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct UpdateRegistrarZoneAccessRequest : Util::Printable<UpdateRegistrarZoneAccessRequest>
{
    UpdateRegistrarZoneAccessRequest(RegistrarZoneAccessId access_id);
    RegistrarZoneAccessId access_id;
    boost::optional<Period::TimePoint> set_valid_from;
    boost::optional<Period::TimePoint> set_valid_to;
    std::string to_string() const;
};

struct UpdateRegistrarZoneAccessReply
{
    using Data = RegistrarZoneAccess;
    struct Exception
    {
        struct RegistrarZoneAccessDoesNotExist : Registrar::Exception
        {
            explicit RegistrarZoneAccessDoesNotExist(const UpdateRegistrarZoneAccessRequest& src);
        };
        struct HistoryChangeProhibited : Registrar::Exception
        {
            explicit HistoryChangeProhibited(const UpdateRegistrarZoneAccessRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const UpdateRegistrarZoneAccessRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

UpdateRegistrarZoneAccessReply::Data update_registrar_zone_access(const UpdateRegistrarZoneAccessRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//UPDATE_REGISTRAR_ZONE_ACCESS_HH_BC8F221C95CDBEC1C963BDCDC7D95B84
