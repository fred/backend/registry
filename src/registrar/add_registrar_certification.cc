/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/add_registrar_certification.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/certification/create_registrar_certification.hh"
#include "libfred/registrar/certification/exceptions.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string AddRegistrarCertificationRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("valid_from", valid_from)
                                 .add("valid_to", valid_to)
                                 .add("classification", classification)
                                 .add("file_id", file_id)
                                 .finish();
}

AddRegistrarCertificationReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const AddRegistrarCertificationRequest& src)
    : Registrar::Exception("registrar " + src.registrar_handle.to_string() + " does not exist")
{
}

AddRegistrarCertificationReply::Exception::InvalidData::InvalidData(
        const AddRegistrarCertificationRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

namespace {

auto get_today(const LibFred::OperationContext& ctx)
{
    return boost::gregorian::from_simple_string(static_cast<std::string>(ctx.get_conn().exec("SELECT CURRENT_DATE")[0][0]));
}

auto make_validity(const boost::gregorian::date& not_before, const boost::gregorian::date& not_after)
{
    Period result;
    result.valid_from = Fred::Registry::Util::Into<Period::TimePoint>::from(not_before);
    if (!not_after.is_special())
    {
        result.valid_to = Fred::Registry::Util::Into<Period::TimePoint>::from(not_after);
    }
    return result;
}

}//namespace {anonymous}

AddRegistrarCertificationReply::Data Fred::Registry::Registrar::add_registrar_certification(
        const AddRegistrarCertificationRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto not_before_date = request.valid_from == boost::none
                ? get_today(ctx)
                : Util::Into<boost::gregorian::date>::from(*request.valid_from);
        const auto not_after_date = request.valid_to == boost::none
                ? boost::gregorian::date{boost::gregorian::pos_infin}
                : Util::Into<boost::gregorian::date>::from(*request.valid_to);
        const auto certification = LibFred::Registrar::create_registrar_certification(
                ctx,
                get_raw_value_from(request.registrar_handle),
                not_before_date,
                not_after_date,
                request.classification,
                LibFred::Registrar::FileUuid{get_raw_value_from(request.file_id)});
        ctx.commit_transaction();
        return RegistrarCertification{
                Util::make_strong<RegistrarCertificationId>(certification.uuid.value),
                Util::make_strong<RegistrarHandle>(get_raw_value_from(request.registrar_handle)),
                make_validity(certification.valid_from, certification.valid_until),
                certification.classification,
                Util::make_strong<FileId>(certification.eval_file_uuid.value)};
    }
    catch (const ::RegistrarNotFound&)
    {
        throw AddRegistrarCertificationReply::Exception::RegistrarDoesNotExist{request};
    }
    catch (const ::InvalidDateFrom&)
    {
        throw AddRegistrarCertificationReply::Exception::InvalidData{request, {"valid_from"}};
    }
    catch (const ::CertificationInPast&)
    {
        throw AddRegistrarCertificationReply::Exception::InvalidData{request, {"valid_from"}};
    }
    catch (const ::OverlappingRange&)
    {
        throw AddRegistrarCertificationReply::Exception::InvalidData{request, {"valid_from"}};
    }
    catch (const ::WrongIntervalOrder&)
    {
        throw AddRegistrarCertificationReply::Exception::InvalidData{request, {"valid_from & valid_to"}};
    }
    catch (const ::ScoreOutOfRange&)
    {
        throw AddRegistrarCertificationReply::Exception::InvalidData{request, {"classification"}};
    }
    catch (const ::FileDoesNotExist&)
    {
        throw AddRegistrarCertificationReply::Exception::InvalidData{request, {"file_id"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
