/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRAR_COMMON_TYPES_HH_83A2B9C2A941EBD79135DB16CE0D9310//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_COMMON_TYPES_HH_83A2B9C2A941EBD79135DB16CE0D9310

#include "src/common_types.hh"
#include "src/util/strong_type.hh"
#include "src/exceptions.hh"

#include <boost/optional.hpp>
#include <boost/uuid/uuid.hpp>

#include <chrono>
#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Registrar {

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

using RegistrarHandle = Util::StrongString<struct RegistrarHandle_Tag, Util::Skill::Printable>;
using RegistrarId = Util::StrongType<boost::uuids::uuid, struct RegistrarId_Tag, Util::Skill::Printable>;
using RegistrarEppCredentialsId = Util::StrongType<boost::uuids::uuid, struct RegistrarEppCredentialsId_Tag, Util::Skill::Printable>;
using RegistrarCertificationId = Util::StrongType<boost::uuids::uuid, struct RegistrarCertificationId_Tag, Util::Skill::Printable>;
using Credit = Util::StrongString<struct Credit_Tag, Util::Skill::Printable>;

struct RegistrarLightInfo : Util::Printable<RegistrarLightInfo>
{
    RegistrarId id;
    RegistrarHandle handle;
    std::string name;
    std::string organization;
    std::string to_string() const;
};

struct Period;
bool operator<(const Period&, const Period&) noexcept;

struct Period : Util::Printable<Period>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    TimePoint valid_from;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
    friend bool operator<(const Period&, const Period&) noexcept;
};

struct ZoneAccess : Util::Printable<ZoneAccess>
{
    bool has_access_now;
    std::set<Period> history;
    std::string to_string() const;
};

struct SslCertificate : Util::Printable<SslCertificate>
{
    using Bytes = std::vector<unsigned char>;
    Bytes fingerprint;
    std::string cert_data_pem;
    std::string to_string() const;
};

struct RegistrarCertification : Util::Printable<RegistrarCertification>
{
    explicit RegistrarCertification(
            RegistrarCertificationId certification_id,
            RegistrarHandle registrar_handle,
            Period validity,
            int classification,
            FileId file_id);
    RegistrarCertificationId certification_id;
    RegistrarHandle registrar_handle;
    Period validity;
    int classification;
    FileId file_id;
    std::string to_string() const;
};

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_COMMON_TYPES_HH_83A2B9C2A941EBD79135DB16CE0D9310
