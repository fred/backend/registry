/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_certifications.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/certification/exceptions.hh"
#include "libfred/registrar/certification/get_registrar_certifications.hh"
#include "libfred/registrar/info_registrar.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string GetRegistrarCertificationsRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

std::string GetRegistrarCertificationsReply::Data::to_string() const
{
    return Util::StructToString().add("certifications", certifications)
                                 .finish();
}

GetRegistrarCertificationsReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(
        const GetRegistrarCertificationsRequest& src)
    : Registrar::Exception("registrar " + src.registrar_handle.to_string() + " does not exist")
{ }

namespace {

auto make_validity(const LibFred::Registrar::RegistrarCertification& data)
{
    Period result;
    result.valid_from = Util::Into<Period::TimePoint>::from(data.valid_from);
    if (!data.valid_until.is_special())
    {
        result.valid_to = Util::Into<Period::TimePoint>::from(data.valid_until);
    }
    return result;
}

}//namespace Fred::Registry::Registrar::{anonymous}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

GetRegistrarCertificationsReply::Data Fred::Registry::Registrar::get_registrar_certifications(
        const GetRegistrarCertificationsRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto registrar_id = LibFred::InfoRegistrarByHandle{
                get_raw_value_from(request.registrar_handle)}
                        .exec(ctx).info_registrar_data.id;
        const auto certifications = LibFred::Registrar::GetRegistrarCertifications{registrar_id}.exec(ctx);
        ctx.commit_transaction();
        GetRegistrarCertificationsReply::Data result;
        result.certifications.reserve(certifications.size());
        std::transform(
                begin(certifications),
                end(certifications),
                back_inserter(result.certifications),
                [&](const LibFred::Registrar::RegistrarCertification& data)
                {
                    return RegistrarCertification{
                            Util::make_strong<RegistrarCertificationId>(data.uuid.value),
                            request.registrar_handle,
                            make_validity(data),
                            data.classification,
                            Util::make_strong<FileId>(data.eval_file_uuid.value)};
                });
        return result;
    }
    catch (const LibFred::InfoRegistrarByHandle::Exception& e)
    {
        if (e.is_set_unknown_registrar_handle())
        {
            FREDLOG_INFO(boost::format{"registrar %1% not found"} % e.get_unknown_registrar_handle());
            throw GetRegistrarCertificationsReply::Exception::RegistrarDoesNotExist{request};
        }
        FREDLOG_INFO(boost::format{"InfoRegistrarByHandle::Exception caught: %1%"} % e.what());
        throw InternalServerError("InfoRegistrarByHandle::Exception");
    }
    catch (const RegistrarNotFound&)
    {
        FREDLOG_INFO(boost::format{"registrar %1% not found"} % get_raw_value_from(request.registrar_handle));
        throw GetRegistrarCertificationsReply::Exception::RegistrarDoesNotExist{request};
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_INFO(boost::format{"Database::Exception caught: %1%"} % e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(boost::format{"std::exception caught: %1%"} % e.what());
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        FREDLOG_INFO("unknown exception caught");
        throw InternalServerError("unknown exception");
    }
}
