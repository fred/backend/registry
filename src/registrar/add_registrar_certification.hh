/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADD_REGISTRAR_CERTIFICATION_HH_30F663C1139C071933852F80B99A1ED3//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ADD_REGISTRAR_CERTIFICATION_HH_30F663C1139C071933852F80B99A1ED3

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct AddRegistrarCertificationRequest : Util::Printable<AddRegistrarCertificationRequest>
{
    RegistrarHandle registrar_handle;
    using TimePoint = Period::TimePoint;
    boost::optional<TimePoint> valid_from;
    boost::optional<TimePoint> valid_to;
    int classification;
    FileId file_id;
    std::string to_string() const;
};

struct AddRegistrarCertificationReply
{
    using Data = RegistrarCertification;
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const AddRegistrarCertificationRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const AddRegistrarCertificationRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

AddRegistrarCertificationReply::Data add_registrar_certification(const AddRegistrarCertificationRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//ADD_REGISTRAR_CERTIFICATION_HH_30F663C1139C071933852F80B99A1ED3
