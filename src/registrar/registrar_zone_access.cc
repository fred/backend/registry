/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_zone_access.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/registrar/zone_access/exceptions.hh"
#include "libfred/registrar/zone_access/get_zone_access_history.hh"
#include "util/db/db_exceptions.hh"

#include <boost/date_time/gregorian/gregorian.hpp>

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

RegistrarZoneAccess::RegistrarZoneAccess(
        RegistrarHandle registrar_handle,
        std::string zone,
        Period access)
    : registrar_handle{std::move(registrar_handle)},
      zone{std::move(zone)},
      access{std::move(access)}
{ }

std::string RegistrarZoneAccess::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("zone", zone)
                                 .add("access", access)
                                 .finish();
}

std::string RegistrarZoneAccessRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

std::string RegistrarZoneAccessReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("access_by_zone", access_by_zone)
                                 .finish();
}

RegistrarZoneAccessReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const RegistrarZoneAccessRequest& src)
    : Registrar::Exception("registrar " + src.to_string() + " does not exist")
{
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

namespace {

auto get_zone_access_history(
        const LibFred::OperationContextCreator& ctx,
        const LibFred::Registrar::ZoneAccess::RegistrarZoneAccessHistory::InvoiceIdByDate& invoice_id_by_date)
{
    std::string sql =
            "WITH tz AS "
            "("
                "SELECT val AS value "
                  "FROM enum_parameters "
                 "WHERE name = 'regular_day_procedure_zone'"
            ") "
            "SELECT ";
    Database::QueryParams params;
    std::string delimiter;
    std::for_each(begin(invoice_id_by_date), end(invoice_id_by_date), [&](auto&& interval_id_pair)
    {
        sql += delimiter +
               "$" + std::to_string(params.size() + 1) + "::DATE::TIMESTAMP AT TIME ZONE tz.value AT TIME ZONE 'UTC', "
               "($" + std::to_string(params.size() + 2) + "::DATE + '1DAY'::INTERVAL) AT TIME ZONE tz.value AT TIME ZONE 'UTC'";
        if (delimiter.empty())
        {
            delimiter = ", ";
        }
        params.push_back(interval_id_pair.first.from_date);
        if (interval_id_pair.first.to_date != boost::none)
        {
            params.push_back(*interval_id_pair.first.to_date);
        }
        else
        {
            params.push_back(Nullable<boost::gregorian::date>{});
        }
    });
    sql += " FROM tz";
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    if (dbres.size() != 1)
    {
        struct UnexpectedResult : std::exception
        {
            const char* what() const noexcept override { return "query should return just 1 row"; }
        };
        throw UnexpectedResult{};
    }
    std::set<Period> history;
    for (unsigned idx = 0; idx < invoice_id_by_date.size(); ++idx)
    {
        Period record;
        record.valid_from = static_cast<Period::TimePoint>(dbres[0][2 * idx]);
        if (!dbres[0][2 * idx + 1].isnull())
        {
            record.valid_to = static_cast<Period::TimePoint>(dbres[0][2 * idx + 1]);
        }
        history.insert(std::move(record));
    }
    return history;
}

}//namespace {anonymous}

RegistrarZoneAccessReply::Data Fred::Registry::Registrar::registrar_zone_access(const RegistrarZoneAccessRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto zone_access_history = LibFred::Registrar::ZoneAccess::GetZoneAccessHistory{
                get_raw_value_from(request.registrar_handle)}.exec(ctx);
        const auto today = boost::gregorian::day_clock::local_day();
        RegistrarZoneAccessReply::Data result;
        const auto& invoices_by_zone = zone_access_history.invoices_by_zone;
        std::for_each(begin(invoices_by_zone), end(invoices_by_zone), [&](auto&& zone_invoices)
        {
            const auto& zone_fqdn = zone_invoices.first;
            ZoneAccess zone_access;
            zone_access.has_access_now = has_access(zone_access_history, zone_fqdn, today);
            zone_access.history = get_zone_access_history(ctx, zone_invoices.second);
            result.access_by_zone.insert({zone_fqdn, std::move(zone_access)});
        });
        result.registrar_handle = request.registrar_handle;
        return result;
    }
    catch (const LibFred::Registrar::ZoneAccess::NonexistentRegistrar&)
    {
        throw RegistrarZoneAccessReply::Exception::RegistrarDoesNotExist(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("expected unknown exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}
