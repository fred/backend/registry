/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/registrar/info_registrar_zone_access.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"

#include <exception>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

InfoRegistrarZoneAccess::InfoRegistrarZoneAccess(
        unsigned long long id,
        RegistrarHandle registrar_handle,
        std::string zone,
        Period access,
        bool valid_before_now,
        bool valid_now)
    : id{id},
      registrar_handle{std::move(registrar_handle)},
      zone{std::move(zone)},
      access{std::move(access)},
      valid_before_now{valid_before_now},
      valid_now{valid_now}
{ }

std::string InfoRegistrarZoneAccess::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .add("registrar_handle", registrar_handle)
                                 .add("zone", zone)
                                 .add("access", access)
                                 .add("valid_before_now", valid_before_now)
                                 .add("valid_now", valid_now)
                                 .finish();
}

RegistrarZoneAccessId::RegistrarZoneAccessId(
        RegistrarHandle registrar_handle,
        std::string zone,
        Period::TimePoint valid_from)
    : registrar_handle{std::move(registrar_handle)},
      zone{std::move(zone)},
      valid_from{std::move(valid_from)}
{ }

std::string RegistrarZoneAccessId::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("zone", zone)
                                 .add("valid_from", valid_from)
                                 .finish();
}

const char* RegistrarZoneAccessDoesNotExist::what() const noexcept
{
    return "registrar zone access does not exist";
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

RegistrarZoneAccess Fred::Registry::Registrar::info_registrar_zone_access(
        const LibFred::OperationContextCreator& ctx,
        unsigned long long access_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "WITH tz AS "
            "("
                "SELECT val AS value "
                  "FROM enum_parameters "
                 "WHERE name = 'regular_day_procedure_zone'"
            ") "
            "SELECT UPPER(r.handle), "
                   "LOWER(z.fqdn), "
                   "ri.fromdate::TIMESTAMP AT TIME ZONE tz.value AT TIME ZONE 'UTC', "
                   "(ri.todate + '1DAY'::INTERVAL) AT TIME ZONE tz.value AT TIME ZONE 'UTC' "
              "FROM tz, "
                   "registrarinvoice ri "
              "JOIN registrar r ON r.id = ri.registrarid "
              "JOIN zone z ON z.id = ri.zone "
             "WHERE ri.id = $1::INT",
            Database::QueryParams({access_id}));
    if (dbres.size() != 1)
    {
        struct UnexpectedResult : RegistrarZoneAccessDoesNotExist
        {
            const char* what() const noexcept override { return "query should return just 1 row"; }
        };
        throw UnexpectedResult{};
    }
    const auto make_access = [&]()
    {
        Period access;
        access.valid_from = static_cast<Period::TimePoint>(dbres[0][2]);
        if (!dbres[0][3].isnull())
        {
            access.valid_to = static_cast<Period::TimePoint>(dbres[0][3]);
        }
        return access;
    };
    return RegistrarZoneAccess{
            Fred::Registry::Util::make_strong<RegistrarHandle>(static_cast<std::string>(dbres[0][0])),
            static_cast<std::string>(dbres[0][1]),
            make_access()};
}

InfoRegistrarZoneAccess Fred::Registry::Registrar::info_registrar_zone_access(
        const LibFred::OperationContextCreator& ctx,
        const RegistrarZoneAccessId& access_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "WITH tz AS "
            "("
                "SELECT val AS value "
                  "FROM enum_parameters "
                 "WHERE name = 'regular_day_procedure_zone'"
            ") "
            "SELECT ri.id, "
                   "UPPER(r.handle), "
                   "LOWER(z.fqdn), "
                   "ri.fromdate::TIMESTAMP AT TIME ZONE tz.value AT TIME ZONE 'UTC', "
                   "(ri.todate + '1DAY'::INTERVAL) AT TIME ZONE tz.value AT TIME ZONE 'UTC', "
                   "ri.fromdate < (NOW() AT TIME ZONE tz.value)::DATE, "
                   "ri.fromdate <= (NOW() AT TIME ZONE tz.value)::DATE AND "
                   "COALESCE((NOW() AT TIME ZONE tz.value)::DATE <= ri.todate, true) "
              "FROM tz, "
                   "registrarinvoice ri "
              "JOIN registrar r ON r.id = ri.registrarid "
              "JOIN zone z ON z.id = ri.zone "
             "WHERE UPPER(r.handle) = UPPER($1::VARCHAR) AND "
                   "LOWER(z.fqdn) = LOWER($2::VARCHAR) AND "
                   "ri.fromdate::TIMESTAMP AT TIME ZONE tz.value AT TIME ZONE 'UTC' = $3::TIMESTAMP "
               "FOR UPDATE OF ri",
            Database::QueryParams({
                    get_raw_value_from(access_id.registrar_handle),
                    access_id.zone,
                    Fred::Registry::Util::Into<std::string>::from(access_id.valid_from)}));
    if (dbres.size() != 1)
    {
        if (dbres.size() <= 0)
        {
            throw RegistrarZoneAccessDoesNotExist{};
        }
        struct UnexpectedResult : RegistrarZoneAccessDoesNotExist
        {
            const char* what() const noexcept override { return "query should return just 1 row"; }
        };
        throw UnexpectedResult{};
    }
    const auto make_access = [&]()
    {
        Period access;
        access.valid_from = static_cast<Period::TimePoint>(dbres[0][3]);
        if (!dbres[0][4].isnull())
        {
            access.valid_to = static_cast<Period::TimePoint>(dbres[0][4]);
        }
        return access;
    };
    return InfoRegistrarZoneAccess{
            static_cast<unsigned long long>(dbres[0][0]),
            Fred::Registry::Util::make_strong<RegistrarHandle>(static_cast<std::string>(dbres[0][1])),
            static_cast<std::string>(dbres[0][2]),
            make_access(),
            static_cast<bool>(dbres[0][5]),
            static_cast<bool>(dbres[0][6])};
}
