/*
 * Copyright (C) 2019-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRAR_ZONE_ACCESS_HH_D62085A4CE932F2C8E59DD3EFE880CA4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_ZONE_ACCESS_HH_D62085A4CE932F2C8E59DD3EFE880CA4

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct RegistrarZoneAccess : Util::Printable<RegistrarZoneAccess>
{
    explicit RegistrarZoneAccess(
            RegistrarHandle registrar_handle,
            std::string zone,
            Period access);
    RegistrarHandle registrar_handle;
    std::string zone;
    Period access;
    std::string to_string() const;
};

struct RegistrarZoneAccessRequest : Util::Printable<RegistrarZoneAccessRequest>
{
    RegistrarHandle registrar_handle;
    std::string to_string() const;
};

struct RegistrarZoneAccessReply
{
    struct Data : Util::Printable<Data>
    {
        RegistrarHandle registrar_handle;
        std::map<std::string, ZoneAccess> access_by_zone;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const RegistrarZoneAccessRequest& src);
        };
    };
};

RegistrarZoneAccessReply::Data registrar_zone_access(const RegistrarZoneAccessRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_ZONE_ACCESS_HH_D62085A4CE932F2C8E59DD3EFE880CA4
