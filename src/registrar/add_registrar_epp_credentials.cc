/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/add_registrar_epp_credentials.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/epp_auth/add_registrar_epp_auth.hh"
#include "libfred/registrar/epp_auth/clone_registrar_epp_auth.hh"
#include "libfred/registrar/epp_auth/exceptions.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string AddRegistrarEppCredentialsRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("certificate", certificate)
                                 .add("password", password)
                                 .finish();
}

AddRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const AddRegistrarEppCredentialsRequest& src)
    : Registrar::Exception("registrar " + src.registrar_handle.to_string() + " does not exist")
{
}

AddRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist::RegistrarEppCredentialsDoesNotExist(const AddRegistrarEppCredentialsRequest& src)
    : Registrar::Exception("registrar epp credentials " + Fred::Registry::Util::Into<std::string>::from(src.password) + " does not exist")
{
}

std::string AddRegistrarEppCredentialsReply::Data::to_string() const
{
    return Util::StructToString().add("credentials_id", credentials_id)
                                 .finish();
}

AddRegistrarEppCredentialsReply::Exception::InvalidData::InvalidData(
        const AddRegistrarEppCredentialsRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

namespace {

constexpr char byte_to_hex(unsigned char byte)
{
    if (byte < 10)
    {
        return static_cast<char>('0' + byte);
    }
    return static_cast<char>('A' + byte - 10);
}

static_assert(byte_to_hex(0x0) == '0');
static_assert(byte_to_hex(0x9) == '9');
static_assert(byte_to_hex(0xa) == 'A');
static_assert(byte_to_hex(0xf) == 'F');

std::string binary_to_hex(const std::vector<unsigned char>& binary)
{
    std::string hex;
    std::for_each(begin(binary), end(binary), [&](unsigned char byte)
    {
        if (!hex.empty())
        {
            hex.append({':', byte_to_hex(byte >> 4), byte_to_hex(byte & 0x0f)});
        }
        else
        {
            hex.append({byte_to_hex(byte >> 4), byte_to_hex(byte & 0x0f)});
        }
    });
    return hex;
}

class Exec : boost::static_visitor<LibFred::Registrar::EppAuth::EppAuthRecord>
{
public:
    explicit Exec(
            LibFred::OperationContext& ctx,
            const RegistrarHandle& registrar_handle,
            const SslCertificate& certificate)
        : ctx_{ctx},
          registrar_handle_{registrar_handle},
          certificate_{certificate}
    { }
    auto operator()(const std::string& password) const
    {
        return LibFred::Registrar::EppAuth::add_registrar_epp_auth(
                ctx_,
                get_raw_value_from(registrar_handle_),
                binary_to_hex(certificate_.fingerprint),
                password,
                certificate_.cert_data_pem);
    }
    auto operator()(const RegistrarEppCredentialsId& password_source) const
    {
        return LibFred::Registrar::EppAuth::clone_registrar_epp_auth(
                ctx_,
                Fred::Registry::fred_wrap(password_source),
                binary_to_hex(certificate_.fingerprint),
                certificate_.cert_data_pem);
    }
private:
    LibFred::OperationContext& ctx_;
    const RegistrarHandle& registrar_handle_;
    const SslCertificate& certificate_;
};

}//namespace {anonymous}

AddRegistrarEppCredentialsReply::Data Fred::Registry::Registrar::add_registrar_epp_credentials(
        const AddRegistrarEppCredentialsRequest& request)
{
    if (request.password.empty())
    {
        throw AddRegistrarEppCredentialsReply::Exception::InvalidData{request, {"password"}};
    }
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto record_data = boost::apply_visitor(
                Exec{ctx, request.registrar_handle, request.certificate},
                request.password);
        ctx.commit_transaction();
        AddRegistrarEppCredentialsReply::Data result;
        result.credentials_id = fred_unwrap(record_data.uuid);
        return result;
    }
    catch (const LibFred::Registrar::EppAuth::NonexistentRegistrar&)
    {
        throw AddRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist{request};
    }
    catch (const LibFred::Registrar::EppAuth::NonexistentRegistrarEppAuth&)
    {
        throw AddRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist{request};
    }
    catch (const LibFred::Registrar::EppAuth::DuplicateCertificate&)
    {
        throw AddRegistrarEppCredentialsReply::Exception::InvalidData{request, {"certificate.fingerprint"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
