/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADD_REGISTRAR_ZONE_ACCESS_HH_E4B55EDB316731AF124FBC2A2ED0CA32//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ADD_REGISTRAR_ZONE_ACCESS_HH_E4B55EDB316731AF124FBC2A2ED0CA32

#include "src/registrar/registrar_zone_access.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct AddRegistrarZoneAccessRequest : Util::Printable<AddRegistrarZoneAccessRequest>
{
    RegistrarHandle registrar_handle;
    std::string zone;
    boost::optional<Period::TimePoint> valid_from;
    boost::optional<Period::TimePoint> valid_to;
    std::string to_string() const;
};

struct AddRegistrarZoneAccessReply
{
    using Data = RegistrarZoneAccess;
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const AddRegistrarZoneAccessRequest& src);
        };
        struct ZoneDoesNotExist : Registrar::Exception
        {
            explicit ZoneDoesNotExist(const AddRegistrarZoneAccessRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const AddRegistrarZoneAccessRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

AddRegistrarZoneAccessReply::Data add_registrar_zone_access(const AddRegistrarZoneAccessRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//ADD_REGISTRAR_ZONE_ACCESS_HH_E4B55EDB316731AF124FBC2A2ED0CA32
