/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/update_registrar_certification.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/certification/exceptions.hh"
#include "libfred/registrar/certification/update_registrar_certification.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string UpdateRegistrarCertificationRequest::to_string() const
{
    return Util::StructToString().add("certification_id", certification_id)
                                 .add("set_valid_to", set_valid_to)
                                 .add("set_classification", set_classification)
                                 .add("set_file_id", set_file_id)
                                 .finish();
}

UpdateRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist::RegistrarCertificationDoesNotExist(const UpdateRegistrarCertificationRequest& src)
    : Registrar::Exception("registrar certification " + src.certification_id.to_string() + " does not exist")
{
}

UpdateRegistrarCertificationReply::Exception::InvalidData::InvalidData(
        const UpdateRegistrarCertificationRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

namespace {

auto get_registrar_handle(
        const LibFred::OperationContext& ctx,
        const RegistrarCertificationId& certification_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "SELECT r.handle "
              "FROM registrar_certification rc "
              "JOIN registrar r ON r.id = rc.registrar_id "
             "WHERE rc.uuid = $1::UUID",
            Database::QueryParams{certification_id});
    if (dbres.size() == 1)
    {
        return Fred::Registry::Util::make_strong<RegistrarHandle>(static_cast<std::string>(dbres[0][0]));
    }
    if (dbres.size() == 0)
    {
        throw RegistrarNotFound{};
    }
    throw std::runtime_error{"unexpected number of rows"};
}

auto make_validity(const boost::gregorian::date& not_before, const boost::gregorian::date& not_after)
{
    Period result;
    result.valid_from = Fred::Registry::Util::Into<Period::TimePoint>::from(not_before);
    if (!not_after.is_special())
    {
        result.valid_to = Fred::Registry::Util::Into<Period::TimePoint>::from(not_after);
    }
    return result;
}

}//namespace {anonymous}

UpdateRegistrarCertificationReply::Data Fred::Registry::Registrar::update_registrar_certification(
        const UpdateRegistrarCertificationRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto certification = LibFred::Registrar::update_registrar_certification(
                ctx,
                LibFred::Registrar::RegistrarCertificationUuid{get_raw_value_from(request.certification_id)},
                request.set_valid_to == boost::none
                        ? boost::none
                        : boost::make_optional(Util::Into<boost::gregorian::date>::from(*request.set_valid_to)),
                request.set_classification,
                request.set_file_id == boost::none
                        ? boost::none
                        : boost::make_optional(
                                LibFred::Registrar::UpdateRegistrarCertification::FileIdVariant{
                                        LibFred::Registrar::FileUuid{get_raw_value_from(*request.set_file_id)}}));
        auto result = RegistrarCertification{
                Util::make_strong<RegistrarCertificationId>(certification.uuid.value),
                get_registrar_handle(ctx, request.certification_id),
                make_validity(certification.valid_from, certification.valid_until),
                certification.classification,
                Util::make_strong<FileId>(certification.eval_file_uuid.value)};
        ctx.commit_transaction();
        return result;
    }
    catch (const ::RegistrarCertificationDoesNotExist&)
    {
        throw UpdateRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist{request};
    }
    catch (const ::RegistrarNotFound&)
    {
        throw UpdateRegistrarCertificationReply::Exception::InvalidData{request, {"certification_id"}};
    }
    catch (const ::CertificationInPast&)
    {
        throw UpdateRegistrarCertificationReply::Exception::InvalidData{request, {"certification_id"}};
    }
    catch (const ::WrongIntervalOrder&)
    {
        throw UpdateRegistrarCertificationReply::Exception::InvalidData{request, {"certification_id & set_valid_to"}};
    }
    catch (const ::ScoreOutOfRange&)
    {
        throw UpdateRegistrarCertificationReply::Exception::InvalidData{request, {"set_classification"}};
    }
    catch (const ::FileDoesNotExist&)
    {
        throw UpdateRegistrarCertificationReply::Exception::InvalidData{request, {"set_file_id"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
