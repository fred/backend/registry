/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADD_REGISTRAR_GROUP_MEMBERSHIP_HH_C2B3EA1C7ADE2AD60E2F59D0C5EA3E15//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ADD_REGISTRAR_GROUP_MEMBERSHIP_HH_C2B3EA1C7ADE2AD60E2F59D0C5EA3E15

#include "src/registrar/registrar_common_types.hh"
#include "src/registrar/registrar_groups_membership.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct AddRegistrarGroupMembershipRequest : Util::Printable<AddRegistrarGroupMembershipRequest>
{
    RegistrarHandle registrar_handle;
    GroupName group;
    std::string to_string() const;
};

struct AddRegistrarGroupMembershipReply
{
    using Data = RegistrarGroupsMembership;
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const AddRegistrarGroupMembershipRequest& src);
        };
        struct GroupDoesNotExist : Registrar::Exception
        {
            explicit GroupDoesNotExist(const AddRegistrarGroupMembershipRequest& src);
        };
    };
};

AddRegistrarGroupMembershipReply::Data add_registrar_group_membership(const AddRegistrarGroupMembershipRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//ADD_REGISTRAR_GROUP_MEMBERSHIP_HH_C2B3EA1C7ADE2AD60E2F59D0C5EA3E15
