/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef REGISTRAR_CREDIT_HH_69361B87DD446C8ECCAF5DEDCC50362C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_CREDIT_HH_69361B87DD446C8ECCAF5DEDCC50362C

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct RegistrarCreditRequest : Util::Printable<RegistrarCreditRequest>
{
    RegistrarHandle registrar_handle;
    std::string to_string() const;
};

struct RegistrarCreditReply
{
    struct Data : Util::Printable<Data>
    {
        RegistrarHandle registrar_handle;
        std::map<std::string, boost::optional<Credit>> credit_by_zone;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const RegistrarCreditRequest& src);
        };
    };
};

RegistrarCreditReply::Data registrar_credit(const RegistrarCreditRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_CREDIT_HH_69361B87DD446C8ECCAF5DEDCC50362C
