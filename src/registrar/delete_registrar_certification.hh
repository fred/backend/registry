/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETE_REGISTRAR_CERTIFICATION_HH_E795C493654E42BC38A250F60DA69544//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DELETE_REGISTRAR_CERTIFICATION_HH_E795C493654E42BC38A250F60DA69544

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct DeleteRegistrarCertificationRequest : Util::Printable<DeleteRegistrarCertificationRequest>
{
    RegistrarCertificationId certification_id;
    std::string to_string() const;
};

struct DeleteRegistrarCertificationReply
{
    struct Exception
    {
        struct RegistrarCertificationDoesNotExist : Registrar::Exception
        {
            explicit RegistrarCertificationDoesNotExist(const DeleteRegistrarCertificationRequest& src);
        };
        struct HistoryChangeProhibited : Registrar::Exception
        {
            explicit HistoryChangeProhibited(const DeleteRegistrarCertificationRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const DeleteRegistrarCertificationRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

void delete_registrar_certification(const DeleteRegistrarCertificationRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//DELETE_REGISTRAR_CERTIFICATION_HH_E795C493654E42BC38A250F60DA69544
