/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/add_registrar_group_membership.hh"
#include "src/registrar/get_registrar_groups_membership.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/registrar/group/membership/create_registrar_group_membership.hh"
#include "libfred/registrar/info_registrar.hh"
#include "util/db/db_exceptions.hh"

#include <exception>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string AddRegistrarGroupMembershipRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("group", group)
                                 .finish();
}

AddRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(
        const AddRegistrarGroupMembershipRequest& src)
    : Registrar::Exception("registrar " + src.registrar_handle.to_string() + " does not exist")
{ }

AddRegistrarGroupMembershipReply::Exception::GroupDoesNotExist::GroupDoesNotExist(
        const AddRegistrarGroupMembershipRequest& src)
    : Registrar::Exception("registrar group " + src.group.to_string() + " does not exist")
{ }

namespace {

struct RegistrarNotFoundByHandle : RegistrarDoesNotExist
{
    const char* what() const noexcept override { return "registrar not found by handle"; }
};

struct GroupDoesNotExist : std::exception
{
    const char* what() const noexcept override { return "registrar group does not exist"; }
};

auto get_info(
        LibFred::OperationContext& ctx,
        const RegistrarHandle& registrar_handle,
        const GroupName& group)
{
    ctx.get_conn().exec("LOCK TABLE registrar_group_map IN ACCESS EXCLUSIVE MODE");
    const auto dbres = ctx.get_conn().exec_params(
            "WITH info AS "
            "("
                "SELECT id AS registrar_id, "
                       "(SELECT id "
                          "FROM registrar_group "
                         "WHERE short_name = $2::TEXT AND "
                               "cancelled IS NULL) AS group_id, "
                       "CURRENT_DATE AS today "
                  "FROM registrar "
                 "WHERE handle = UPPER($1::TEXT)"
            ") "
            "SELECT registrar_id, "
                   "group_id, "
                   "today, "
                   "EXISTS(SELECT "
                            "FROM registrar_group_map rgm "
                           "WHERE info.registrar_id = rgm.registrar_id AND "
                                 "info.group_id = rgm.registrar_group_id AND "
                                 "rgm.member_from <= CURRENT_DATE AND "
                                 "(CURRENT_DATE < rgm.member_until OR rgm.member_until IS NULL)) "
              "FROM info",
            Database::QueryParams{get_raw_value_from(registrar_handle),
                                  get_raw_value_from(group)});
    if (dbres.size() == 0)
    {
        throw RegistrarNotFoundByHandle{};
    }
    if (dbres.size() != 1)
    {
        struct UnexpectedNumberOfRows : std::exception
        {
            const char* what() const noexcept override { return "expected just one row"; }
        };
        throw UnexpectedNumberOfRows{};
    }
    if (dbres[0][1].isnull())
    {
        throw GroupDoesNotExist{};
    }
    struct Info
    {
        unsigned long long registrar_id;
        unsigned long long group_id;
        boost::gregorian::date today;
        bool already_member;
    };
    return Info{
            static_cast<unsigned long long>(dbres[0][0]),
            static_cast<unsigned long long>(dbres[0][1]),
            boost::gregorian::from_simple_string(static_cast<std::string>(dbres[0][2])),
            static_cast<bool>(dbres[0][3])};
}

}//namespace Fred::Registry::Registrar::{anonymous}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

AddRegistrarGroupMembershipReply::Data Fred::Registry::Registrar::add_registrar_group_membership(
        const AddRegistrarGroupMembershipRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto info = get_info(ctx, request.registrar_handle, request.group);
        if (!info.already_member)
        {
            LibFred::Registrar::CreateRegistrarGroupMembership{
                    info.registrar_id,
                    info.group_id,
                    info.today}.exec(ctx);
        }
        auto result = get_registrar_groups_membership(
                ctx,
                request.registrar_handle,
                info.registrar_id);
        ctx.commit_transaction();
        return result;
    }
    catch (const LibFred::InfoRegistrarByHandle::Exception& e)
    {
        if (e.is_set_unknown_registrar_handle())
        {
            FREDLOG_INFO(boost::format{"registrar %1% not found"} % e.get_unknown_registrar_handle());
            throw AddRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist{request};
        }
        FREDLOG_INFO(boost::format{"InfoRegistrarByHandle::Exception caught: %1%"} % e.what());
        throw InternalServerError{"InfoRegistrarByHandle::Exception"};
    }
    catch (const RegistrarDoesNotExist& e)
    {
        FREDLOG_INFO(boost::format{"registrar %1% not found: %2%"}
                % get_raw_value_from(request.registrar_handle)
                % e.what());
        throw AddRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist{request};
    }
    catch (const GroupDoesNotExist& e)
    {
        FREDLOG_INFO(boost::format{"group %1% not found: %2%"}
                % get_raw_value_from(request.group)
                % e.what());
        throw AddRegistrarGroupMembershipReply::Exception::GroupDoesNotExist{request};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(boost::format{"std::exception caught: %1%"} % e.what());
        throw InternalServerError{"std::exception"};
    }
    catch (...)
    {
        FREDLOG_INFO("unknown exception caught");
        throw InternalServerError{"unknown exception"};
    }
}
