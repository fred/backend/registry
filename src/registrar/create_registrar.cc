/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/create_registrar.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/registrar/create_registrar.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <cctype>
#include <numeric>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string CreateRegistrarRequest::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .add("place", place)
                                 .add("telephone", telephone)
                                 .add("fax", fax)
                                 .add("emails", emails)
                                 .add("url", url)
                                 .add("is_system_registrar", is_system_registrar)
                                 .add("company_registration_number", company_registration_number)
                                 .add("vat_identification_number", vat_identification_number)
                                 .add("variable_symbol", variable_symbol)
                                 .add("payment_memo_regex", payment_memo_regex)
                                 .add("is_vat_payer", is_vat_payer)
                                 .add("is_internal", is_internal)
                                 .finish();
}

std::string CreateRegistrarReply::Data::to_string() const
{
    return Util::StructToString().add("registrar_handle", registrar_handle)
                                 .finish();
}

CreateRegistrarReply::Exception::RegistrarAlreadyExists::RegistrarAlreadyExists(const CreateRegistrarRequest& src)
    : Registry::Exception{"registrar " + get_raw_value_from(src.registrar_handle) + " already exists"}
{ }

CreateRegistrarReply::Exception::InvalidData::InvalidData(const CreateRegistrarRequest&, std::set<std::string> fields)
    : Registry::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

namespace {

template <typename T, typename Fnc>
void exec_if_not_empty(T&& value, Fnc&& fnc);

template <typename T, typename Fnc>
void exec_if_not_empty(const boost::optional<T>& value, Fnc&& fnc);

template <typename T, typename N, template <typename> class ...Skills, typename Fnc>
void exec_if_not_empty(const Fred::Registry::Util::StrongType<T, N, Skills...>& value, Fnc&& fnc);

template <typename T, typename Fnc>
void exec_if_not_empty(T&& value, Fnc&& fnc)
{
    if (!value.empty())
    {
        std::forward<Fnc>(fnc)(std::forward<T>(value));
    }
}

template <typename T, typename Fnc>
void exec_if_not_empty(const boost::optional<T>& value, Fnc&& fnc)
{
    if (value != boost::none)
    {
        exec_if_not_empty(*value, std::forward<Fnc>(fnc));
    }
}

template <typename T, typename N, template <typename> class ...Skills, typename Fnc>
void exec_if_not_empty(const Fred::Registry::Util::StrongType<T, N, Skills...>& value, Fnc&& fnc)
{
    exec_if_not_empty(get_raw_value_from(value), std::forward<Fnc>(fnc));
}

bool is_empty(const std::string& value)
{
    return value.empty() || std::all_of(begin(value), end(value), [](unsigned char c) { return std::isspace(c); });
}

template <typename N, template <typename> class ...Skills>
bool is_empty(const Fred::Registry::Util::StrongType<std::string, N, Skills...>& value)
{
    return is_empty(get_raw_value_from(value));
}

template <typename T, typename Fnc, typename ...Args>
T nonempty(T value, Fnc&& make_exception, Args&& ...args)
{
    if (is_empty(value))
    {
        throw std::forward<Fnc>(make_exception)(std::forward<Args>(args)...);
    }
    return value;
}

template <typename T, typename N, template <typename> class ...Skills, typename Fnc, typename ...Args>
T nonempty(Fred::Registry::Util::StrongType<T, N, Skills...> value, Fnc&& make_exception, Args&& ...args)
{
    return nonempty(get_raw_value_from(value), std::forward<Fnc>(make_exception), std::forward<Args>(args)...);
}

}//namespace {anonymous}

using namespace Fred::Registry::Registrar;

CreateRegistrarReply::Data Fred::Registry::Registrar::create_registrar(const CreateRegistrarRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto make_invalid_data_exception = [&](const std::string& attribute_name)
        {
            return CreateRegistrarReply::Exception::InvalidData(request, {attribute_name});
        };
        const auto nonempty_street = [&](std::vector<std::string> street)
        {
            static constexpr auto max_number_of_streets = 3u;
            if ((street.size() < 1) || (max_number_of_streets < street.size()))
            {
                throw make_invalid_data_exception("place.street");
            }
            std::for_each(begin(street), end(street), [&](auto&& item)
            {
                nonempty(item, make_invalid_data_exception, "place.street");
            });
            return street;
        };
        const auto nonempty_email = [&](std::vector<EmailAddress> emails)
        {
            if (emails.empty())
            {
                throw make_invalid_data_exception("emails");
            }
            std::for_each(begin(emails), end(emails), [&](auto&& item)
            {
                nonempty(item, make_invalid_data_exception, "emails");
            });
            auto comma_delimited_emails = std::accumulate(
                    next(begin(emails)),
                    end(emails),
                    get_raw_value_from(emails.front()),
                    [](std::string sum, auto&& a)
                    {
                        return std::move(sum) + ", " + get_raw_value_from(a);
                    });
            return comma_delimited_emails;
        };
        LibFred::CreateRegistrar create_registrar_operation{
                nonempty(request.registrar_handle, make_invalid_data_exception, "registrar_handle"),
                nonempty(request.name, make_invalid_data_exception, "name"),
                nonempty(request.organization, make_invalid_data_exception, "organization"),
                nonempty_street(request.place.street),
                nonempty(request.place.city, make_invalid_data_exception, "place.city"),
                nonempty(request.place.country_code, make_invalid_data_exception, "place.country_code"),
                nonempty_email(request.emails),
                nonempty(request.url, make_invalid_data_exception, "url"),
                nonempty(request.vat_identification_number, make_invalid_data_exception, "vat_identification_number"),
                request.is_system_registrar,
                request.is_internal};
        exec_if_not_empty(request.place.state_or_province, [&](const auto& value)
        {
            create_registrar_operation.set_stateorprovince(value);
        });
        exec_if_not_empty(request.place.postal_code, [&](const auto& value)
        {
            create_registrar_operation.set_postalcode(value);
        });
        exec_if_not_empty(request.telephone, [&](const auto& value)
        {
            create_registrar_operation.set_telephone(value);
        });
        exec_if_not_empty(request.fax, [&](const auto& value)
        {
            create_registrar_operation.set_fax(value);
        });
        exec_if_not_empty(request.company_registration_number, [&](const auto& value)
        {
            create_registrar_operation.set_ico(value);
        });
        exec_if_not_empty(request.variable_symbol, [&](const auto& value)
        {
            create_registrar_operation.set_variable_symbol(value);
        });
        exec_if_not_empty(request.payment_memo_regex, [&](const auto& value)
        {
            create_registrar_operation.set_payment_memo_regex(value);
        });
        create_registrar_operation.set_vat_payer(request.is_vat_payer)
                                  .exec(ctx);
        ctx.commit_transaction();
        CreateRegistrarReply::Data result;
        result.registrar_handle = request.registrar_handle;
        return result;
    }
    catch (const CreateRegistrarReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const LibFred::CreateRegistrar::Exception& e)
    {
        if (e.is_set_invalid_registrar_handle())
        {
            throw CreateRegistrarReply::Exception::RegistrarAlreadyExists(request);
        }
        if (e.is_set_invalid_registrar_varsymb())
        {
            throw CreateRegistrarReply::Exception::InvalidData(request, {"variable_symbol"});
        }
        if (e.is_set_unknown_country())
        {
            throw CreateRegistrarReply::Exception::InvalidData(request, {"place.country_code"});
        }
        if (e.is_set_missing_mandatory_attribute())
        {
            throw CreateRegistrarReply::Exception::InvalidData(request, {"missing unexpected attribute"});
        }
        if (e.is_set_too_many_streets())
        {
            throw CreateRegistrarReply::Exception::InvalidData(request, {"place.street"});
        }
        throw InternalServerError("unexpected problem signalized by LibFred::CreateRegistrar::Exception");
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError("unexpected std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}
