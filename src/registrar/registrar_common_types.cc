/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/registrar/registrar_common_types.hh"
#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

Exception::Exception(const std::string& msg)
    : Registry::Exception(msg)
{
}

std::string RegistrarLightInfo::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .add("handle", handle)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .finish();
}

std::string Period::to_string() const
{
    return Util::StructToString().add("valid_from", valid_from)
                                 .add("valid_to", valid_to)
                                 .finish();
}

std::string ZoneAccess::to_string() const
{
    return Util::StructToString().add("has_access_now", has_access_now)
                                 .add("history", history)
                                 .finish();
}

std::string SslCertificate::to_string() const
{
    return Util::StructToString().add("fingerprint", fingerprint)
                                 .add("cert_data_pem", cert_data_pem)
                                 .finish();
}

RegistrarCertification::RegistrarCertification(
            RegistrarCertificationId certification_id,
            RegistrarHandle registrar_handle,
            Period validity,
            int classification,
            FileId file_id)
    : certification_id{std::move(certification_id)},
      registrar_handle{std::move(registrar_handle)},
      validity{std::move(validity)},
      classification{classification},
      file_id{std::move(file_id)}
{ }

std::string RegistrarCertification::to_string() const
{
    return Util::StructToString().add("certification_id", certification_id)
                                 .add("registrar_handle", registrar_handle)
                                 .add("validity", validity)
                                 .add("classification", classification)
                                 .add("file_id", file_id)
                                 .finish();
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

bool Fred::Registry::Registrar::operator<(const Period& lhs, const Period& rhs) noexcept
{
    return lhs.valid_from < rhs.valid_from;
}
