/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETE_REGISTRAR_GROUP_MEMBERSHIP_HH_B6AD6B99C5D2080BA5D07685D094640A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DELETE_REGISTRAR_GROUP_MEMBERSHIP_HH_B6AD6B99C5D2080BA5D07685D094640A

#include "src/registrar/registrar_common_types.hh"
#include "src/registrar/registrar_groups_membership.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct DeleteRegistrarGroupMembershipRequest : Util::Printable<DeleteRegistrarGroupMembershipRequest>
{
    RegistrarHandle registrar_handle;
    GroupName group;
    std::string to_string() const;
};

struct DeleteRegistrarGroupMembershipReply
{
    using Data = RegistrarGroupsMembership;
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const DeleteRegistrarGroupMembershipRequest& src);
        };
        struct GroupDoesNotExist : Registrar::Exception
        {
            explicit GroupDoesNotExist(const DeleteRegistrarGroupMembershipRequest& src);
        };
    };
};

DeleteRegistrarGroupMembershipReply::Data delete_registrar_group_membership(
        const DeleteRegistrarGroupMembershipRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//DELETE_REGISTRAR_GROUP_MEMBERSHIP_HH_B6AD6B99C5D2080BA5D07685D094640A
