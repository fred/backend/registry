/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRAR_GROUPS_HH_67200641A8694AA7A51B152CEA496FAA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRAR_GROUPS_HH_67200641A8694AA7A51B152CEA496FAA

#include "src/registrar/registrar_groups_membership.hh"
#include "src/util/printable.hh"

#include <set>

namespace Fred {
namespace Registry {
namespace Registrar {

struct GetRegistrarGroupsReply
{
    struct Data : Util::Printable<Data>
    {
        std::set<GroupName> groups;
        std::string to_string() const;
    };
};

GetRegistrarGroupsReply::Data registrar_groups();

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRAR_GROUPS_HH_67200641A8694AA7A51B152CEA496FAA
