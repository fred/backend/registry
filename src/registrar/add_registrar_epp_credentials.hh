/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADD_REGISTRAR_EPP_CREDENTIALS_HH_1B7A07ABCAAB3D8FA710A5BABF72B86F//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ADD_REGISTRAR_EPP_CREDENTIALS_HH_1B7A07ABCAAB3D8FA710A5BABF72B86F

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/variant.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct AddRegistrarEppCredentialsRequest : Util::Printable<AddRegistrarEppCredentialsRequest>
{
    RegistrarHandle registrar_handle;
    SslCertificate certificate;
    using Password = boost::variant<std::string, RegistrarEppCredentialsId>;
    Password password;
    std::string to_string() const;
};

struct AddRegistrarEppCredentialsReply
{
    struct Data : Util::Printable<Data>
    {
        RegistrarEppCredentialsId credentials_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct RegistrarDoesNotExist : Registrar::Exception
        {
            explicit RegistrarDoesNotExist(const AddRegistrarEppCredentialsRequest& src);
        };
        struct RegistrarEppCredentialsDoesNotExist : Registrar::Exception
        {
            explicit RegistrarEppCredentialsDoesNotExist(const AddRegistrarEppCredentialsRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const AddRegistrarEppCredentialsRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

AddRegistrarEppCredentialsReply::Data add_registrar_epp_credentials(const AddRegistrarEppCredentialsRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//ADD_REGISTRAR_EPP_CREDENTIALS_HH_1B7A07ABCAAB3D8FA710A5BABF72B86F
