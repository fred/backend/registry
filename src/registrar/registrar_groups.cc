/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/registrar_groups.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/registrar/group/exceptions.hh"
#include "libfred/registrar/group/get_registrar_groups.hh"
#include "util/db/db_exceptions.hh"

#include <boost/date_time.hpp>

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string GetRegistrarGroupsReply::Data::to_string() const
{
    return Util::StructToString().add("groups", groups)
                                 .finish();
}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

GetRegistrarGroupsReply::Data Fred::Registry::Registrar::registrar_groups()
{
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto utc_current_time = boost::posix_time::second_clock::universal_time();
        const auto groups = LibFred::Registrar::GetRegistrarGroups{}.exec(ctx);
        ctx.commit_transaction();
        GetRegistrarGroupsReply::Data result;
        std::for_each(begin(groups), end(groups), [&](auto&& group)
        {
            if (group.cancelled.is_special() || utc_current_time < group.cancelled)
            {
                result.groups.insert(Util::make_strong<GroupName>(group.name));
            }
        });
        return result;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_INFO(boost::format{"Database::Exception caught: %1%"} % e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(boost::format{"std::exception caught: %1%"} % e.what());
        throw InternalServerError("std::exception caught");
    }
    catch (...)
    {
        FREDLOG_INFO("unknown exception caught");
        throw InternalServerError("unknown exception caught");
    }
}
