/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_REGISTRAR_GROUPS_MEMBERSHIP_HH_25FD9C9BD869E88E0BA570ABD9ADC1A4//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_REGISTRAR_GROUPS_MEMBERSHIP_HH_25FD9C9BD869E88E0BA570ABD9ADC1A4

#include "src/registrar/registrar_groups_membership.hh"

#include "libfred/opcontext.hh"

#include <exception>

namespace Fred {
namespace Registry {
namespace Registrar {

RegistrarGroupsMembership get_registrar_groups_membership(
        LibFred::OperationContext& ctx,
        const RegistrarHandle& registrar_handle,
        unsigned long long registrar_id);

struct RegistrarDoesNotExist : std::exception { };

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//GET_REGISTRAR_GROUPS_MEMBERSHIP_HH_25FD9C9BD869E88E0BA570ABD9ADC1A4
