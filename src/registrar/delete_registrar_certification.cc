/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/delete_registrar_certification.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/registrar/certification/delete_registrar_certification.hh"
#include "libfred/registrar/certification/exceptions.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

std::string DeleteRegistrarCertificationRequest::to_string() const
{
    return Util::StructToString().add("certification_id", certification_id)
                                 .finish();
}

DeleteRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist::RegistrarCertificationDoesNotExist(const DeleteRegistrarCertificationRequest& src)
    : Registrar::Exception("registrar certification " + src.certification_id.to_string() + " does not exist")
{
}

DeleteRegistrarCertificationReply::Exception::InvalidData::InvalidData(
        const DeleteRegistrarCertificationRequest&,
        std::set<std::string> fields)
    : Registrar::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

void Fred::Registry::Registrar::delete_registrar_certification(
        const DeleteRegistrarCertificationRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        LibFred::Registrar::delete_registrar_certification(
                ctx,
                LibFred::Registrar::RegistrarCertificationUuid{get_raw_value_from(request.certification_id)});
        ctx.commit_transaction();
        return;
    }
    catch (const ::RegistrarCertificationDoesNotExist&)
    {
        throw DeleteRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist{request};
    }
    catch (const ::RegistrarNotFound&)
    {
        throw DeleteRegistrarCertificationReply::Exception::InvalidData{request, {"certification_id"}};
    }
    catch (const ::CertificationInPast&)
    {
        throw DeleteRegistrarCertificationReply::Exception::InvalidData{request, {"certification_id"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception&)
    {
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
