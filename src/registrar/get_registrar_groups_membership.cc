/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/registrar/get_registrar_groups_membership.hh"

#include "libfred/registrar/group/get_registrar_groups.hh"
#include "libfred/registrar/group/membership/info_group_membership_by_registrar.hh"
#include "libfred/registrar/info_registrar.hh"

#include <algorithm>
#include <map>
#include <utility>

namespace Fred {
namespace Registry {
namespace Registrar {

namespace {

class GetGroupNameByGroupId
{
public:
    explicit GetGroupNameByGroupId(LibFred::OperationContext& ctx)
        : group_names_by_group_id_{[](LibFred::OperationContext& ctx)
            {
                auto groups = LibFred::Registrar::GetRegistrarGroups{}.exec(ctx);
                std::map<unsigned long long, GroupName> vocabulary;
                std::for_each(begin(groups), end(groups), [&](auto&& group)
                {
                    vocabulary.insert({group.id, Util::make_strong<GroupName>(std::move(group.name))});
                });
                return vocabulary;
            }(ctx)}
    { }
    GroupName operator[](unsigned long long group_id) const
    {
        const auto iter = group_names_by_group_id_.find(group_id);
        if (iter != end(group_names_by_group_id_))
        {
            return iter->second;
        }
        struct InvalidGroupId : std::exception
        {
            const char* what() const noexcept override { return "group id not found"; }
        };
        throw InvalidGroupId{};
    }
private:
    std::map<unsigned long long, GroupName> group_names_by_group_id_;
};

}//namespace Fred::Registry::Registrar::{anonymous}

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Registrar;

RegistrarGroupsMembership Fred::Registry::Registrar::get_registrar_groups_membership(
        LibFred::OperationContext& ctx,
        const RegistrarHandle& registrar_handle,
        unsigned long long registrar_id)
{
    const auto data_mismatch = [&]()
    {
        try
        {
            return LibFred::InfoRegistrarById{registrar_id}.exec(ctx).info_registrar_data.handle !=
                   get_raw_value_from(registrar_handle);
        }
        catch (const LibFred::InfoRegistrarById::Exception& e)
        {
            if (e.is_set_unknown_registrar_id())
            {
                struct RegistrarNotFoundById : RegistrarDoesNotExist
                {
                    const char* what() const noexcept override { return "registrar not found by id"; }
                };
                throw RegistrarNotFoundById{};
            }
            throw;
        }
    }();
    if (data_mismatch)
    {
        struct DataMismatch : RegistrarDoesNotExist
        {
            const char* what() const noexcept override { return "registrar id does not correspond to registrar handle"; }
        };
        throw DataMismatch{};
    }
    const auto record_data = LibFred::Registrar::InfoGroupMembershipByRegistrar{registrar_id}.exec(ctx);
    const auto group_names_by_group_id = GetGroupNameByGroupId{ctx};
    std::set<GroupName> groups;
    const auto utc_today = boost::posix_time::second_clock::universal_time().date();
    std::for_each(begin(record_data), end(record_data), [&](auto&& membership)
    {
        if (membership.member_from <= utc_today && utc_today < membership.member_until)
        {
            groups.insert(group_names_by_group_id[membership.group_id]);
        }
    });
    return GetRegistrarGroupsMembershipReply::Data{registrar_handle, std::move(groups)};
}
