/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETE_REGISTRAR_ZONE_ACCESS_HH_E8489A155C25F76773AC212CDB0D61F7//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DELETE_REGISTRAR_ZONE_ACCESS_HH_E8489A155C25F76773AC212CDB0D61F7

#include "src/registrar/info_registrar_zone_access.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct DeleteRegistrarZoneAccessRequest : Util::Printable<DeleteRegistrarZoneAccessRequest>
{
    DeleteRegistrarZoneAccessRequest(RegistrarZoneAccessId access_id);
    RegistrarZoneAccessId access_id;
    std::string to_string() const;
};

struct DeleteRegistrarZoneAccessReply
{
    struct Exception
    {
        struct RegistrarZoneAccessDoesNotExist : Registrar::Exception
        {
            explicit RegistrarZoneAccessDoesNotExist(const DeleteRegistrarZoneAccessRequest& src);
        };
        struct HistoryChangeProhibited : Registrar::Exception
        {
            explicit HistoryChangeProhibited(const DeleteRegistrarZoneAccessRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const DeleteRegistrarZoneAccessRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

void delete_registrar_zone_access(const DeleteRegistrarZoneAccessRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//DELETE_REGISTRAR_ZONE_ACCESS_HH_E8489A155C25F76773AC212CDB0D61F7
