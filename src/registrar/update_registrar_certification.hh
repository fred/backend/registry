/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_REGISTRAR_CERTIFICATION_HH_B1369FEB8B5415AF45F70D64EE560BF2//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_REGISTRAR_CERTIFICATION_HH_B1369FEB8B5415AF45F70D64EE560BF2

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Registrar {

struct UpdateRegistrarCertificationRequest : Util::Printable<UpdateRegistrarCertificationRequest>
{
    RegistrarCertificationId certification_id;
    using TimePoint = Period::TimePoint;
    boost::optional<TimePoint> set_valid_to;
    boost::optional<int> set_classification;
    boost::optional<FileId> set_file_id;
    std::string to_string() const;
};

struct UpdateRegistrarCertificationReply
{
    using Data = RegistrarCertification;
    struct Exception
    {
        struct RegistrarCertificationDoesNotExist : Registrar::Exception
        {
            explicit RegistrarCertificationDoesNotExist(const UpdateRegistrarCertificationRequest& src);
        };
        struct HistoryChangeProhibited : Registrar::Exception
        {
            explicit HistoryChangeProhibited(const UpdateRegistrarCertificationRequest& src);
        };
        struct InvalidData : Registrar::Exception
        {
            explicit InvalidData(const UpdateRegistrarCertificationRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

UpdateRegistrarCertificationReply::Data update_registrar_certification(const UpdateRegistrarCertificationRequest& request);

}//namespace Fred::Registry::Registrar
}//namespace Fred::Registry
}//namespace Fred

#endif//UPDATE_REGISTRAR_CERTIFICATION_HH_B1369FEB8B5415AF45F70D64EE560BF2
