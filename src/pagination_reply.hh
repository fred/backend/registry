/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PAGINATION_REPLY_HH_69C2C30E9616115AB5DC056EBC6BD054//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PAGINATION_REPLY_HH_69C2C30E9616115AB5DC056EBC6BD054

#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <cstdint>
#include <string>


namespace Fred {
namespace Registry {

struct PaginationReply : Util::Printable<PaginationReply>
{
    std::string next_page_token;
    boost::optional<std::uint64_t> items_left;
    std::string to_string() const;
};

PaginationReply last_page();

} // namespace Fred::Registry
} // namespace Fred

#endif//PAGINATION_REPLY_HH_69C2C30E9616115AB5DC056EBC6BD054
