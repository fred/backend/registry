/*
 * Copyright (C) 2019-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FRED_WRAP_HH_9B7AFBF8797BEC34E132B83EA82586BF//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define FRED_WRAP_HH_9B7AFBF8797BEC34E132B83EA82586BF

#include "src/contact/contact_history_interval.hh"
#include "src/domain/domain_history_interval.hh"
#include "src/keyset/keyset_history_interval.hh"
#include "src/nsset/nsset_history_interval.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/registry_type_traits.hh"

#include "libfred/registrable_object/history_interval.hh"
#include "libfred/registrar/epp_auth/registrar_epp_auth_data.hh"

namespace Fred {
namespace Registry {

template <typename T>
typename RegistryTypeTraits<T>::LibFredType fred_wrap(const T&);

template <>
struct RegistryTypeTraits<Contact::ContactHistoryInterval>
{
    using LibFredType = LibFred::RegistrableObject::HistoryInterval;
};

template <>
struct RegistryTypeTraits<Domain::DomainHistoryInterval>
{
    using LibFredType = LibFred::RegistrableObject::HistoryInterval;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetHistoryInterval>
{
    using LibFredType = LibFred::RegistrableObject::HistoryInterval;
};

template <>
struct RegistryTypeTraits<Nsset::NssetHistoryInterval>
{
    using LibFredType = LibFred::RegistrableObject::HistoryInterval;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarEppCredentialsId>
{
    using LibFredType = LibFred::Registrar::EppAuth::EppAuthRecordUuid;
};

}//namespace Fred::Registry
}//namespace Fred

#endif//FRED_WRAP_HH_9B7AFBF8797BEC34E132B83EA82586BF
