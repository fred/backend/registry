/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/keyset/keyset_info.hh"

#include "src/fred_unwrap.hh"
#include "src/util/strong_type.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/is_registered.hh"
#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

#include <algorithm>
#include <iterator>

namespace Fred {
namespace Registry {
namespace Keyset {

namespace {

bool does_history_continue(const LibFred::InfoKeysetOutput& info)
{
    return !info.next_historyid.isnull();
}

bool does_exist(const LibFred::InfoKeysetData& data)
{
    return data.delete_time.isnull();
}

KeysetInfoRequest get_checked_request_data(const BatchKeysetInfoRequest::Request& data)
{
    KeysetInfoRequest dst;
    try
    {
        dst.keyset_id = Util::make_strong<KeysetId>(boost::uuids::string_generator{}(data.keyset_id));
    }
    catch (const std::exception&)
    {
        throw KeysetInfoReply::Exception::InvalidData{dst, {"keyset_id"}};
    }
    if (!data.keyset_history_id.empty())
    {
        try
        {
            dst.keyset_history_id = Util::make_strong<KeysetHistoryId>(boost::uuids::string_generator{}(data.keyset_history_id));
        }
        catch (const std::exception&)
        {
            throw KeysetInfoReply::Exception::InvalidData{dst, {"keyset_history_id"}};
        }
    }
    dst.include_domains_count = data.include_domains_count;
    return dst;
}

struct KeysetDoesNotExist {};

std::int64_t get_number_of_connected_domains(const LibFred::OperationContext& ctx, const KeysetId& keyset_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "SELECT (SELECT COUNT(DISTINCT d.id) "
                      "FROM domain d "
                     "WHERE d.keyset = obr.id) "
              "FROM object_registry obr "
             "WHERE obr.type = get_object_type_id('keyset') AND "
                   "obr.erdate IS NULL AND "
                   "obr.uuid = $1::UUID",
            Database::QueryParams{get_raw_value_from(keyset_id)}
    );
    if (dbres.size() == 1)
    {
        return static_cast<std::int64_t>(dbres[0][0]);
    }
    if (dbres.size() == 0)
    {
        throw KeysetDoesNotExist{};
    }
    struct DatabaseMustBeCrazy : std::exception
    {
        const char* what() const noexcept override { return "unexpected number of keysets"; }
    };
    throw DatabaseMustBeCrazy{};
}

template <LibFred::DbLock locking>
KeysetInfoReply::Data keyset_info(
        const LibFred::OperationContextUsing<locking>& ctx,
        const KeysetInfoRequest& request)
{
    try
    {
        try
        {
            auto result = [&]()
            {
                if (request.keyset_history_id != boost::none)
                {
                    const auto keyset_history_uuid = LibFred::RegistrableObject::make_history_uuid_of<LibFred::Object_Type::keyset>(
                            get_raw_value_from(*request.keyset_history_id));
                    auto info = LibFred::InfoKeysetByHistoryUuid{keyset_history_uuid}.exec(ctx);
                    if (request.include_domains_count)
                    {
                        if (does_history_continue(info) ||
                            !does_exist(info.info_keyset_data))
                        {
                            throw KeysetInfoReply::Exception::InvalidData{request, {"include_domains_count"}};
                        }
                    }
                    return fred_unwrap(info);
                }
                const auto keyset_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::keyset>(
                        get_raw_value_from(request.keyset_id));
                return fred_unwrap(LibFred::InfoKeysetByUuid(keyset_uuid).exec(ctx));
            }();
            if (request.include_domains_count)
            {
                result.domains_count = get_number_of_connected_domains(ctx, result.keyset_id);
            }
            return result;
        }
        catch (const LibFred::InfoKeysetByUuid::Exception& e)
        {
            if (e.is_set_unknown_keyset_uuid())
            {
                if (LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::keyset, e.get_unknown_keyset_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered keyset");
                }
                throw KeysetInfoReply::Exception::KeysetDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::KeysetInfoByUuid::Exception");
        }
        catch (const LibFred::InfoKeysetByHistoryUuid::Exception& e)
        {
            if (e.is_set_unknown_keyset_history_uuid())
            {
                if (LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::keyset, e.get_unknown_keyset_history_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered keyset");
                }
                throw KeysetInfoReply::Exception::KeysetDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoKeysetByHistoryUuid::Exception");
        }
        catch (const KeysetDoesNotExist&)
        {
            throw KeysetInfoReply::Exception::KeysetDoesNotExist(request);
        }
    }
    catch (const KeysetInfoReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const KeysetInfoReply::Exception::KeysetDoesNotExist&)
    {
        throw;
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError{"unknown exception"};
    }
}

template <LibFred::DbLock locking>
BatchKeysetInfoReply::Data::BatchReply get_batch_reply(
        const LibFred::OperationContextUsing<locking>& ctx,
        const BatchKeysetInfoRequest::Request& request)
{
    BatchKeysetInfoReply::Data::BatchReply reply;
    try
    {
        reply.data_or_error = keyset_info(ctx, get_checked_request_data(request));
    }
    catch (KeysetInfoReply::Exception::InvalidData& exception)
    {
        reply.data_or_error = BatchKeysetInfoReply::Error{
                request,
                KeysetInfoReply::Exception{std::move(exception)}};
    }
    catch (KeysetInfoReply::Exception::KeysetDoesNotExist& exception)
    {
        reply.data_or_error = BatchKeysetInfoReply::Error{
                request,
                KeysetInfoReply::Exception{std::move(exception)}};
    }
    return reply;
}

}//namespace Fred::Registry::Keyset

}//namespace Fred::Registry::Keyset
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Keyset;

KeysetInfoReply::Data Fred::Registry::Keyset::keyset_info(const KeysetInfoRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx{ctx_provider};
        auto result = keyset_info(ctx, request);
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
}

BatchKeysetInfoReply::Data Fred::Registry::Keyset::batch_keyset_info(const BatchKeysetInfoRequest& batch_request)
{
    try
    {
        const auto& requests = batch_request.requests;
        BatchKeysetInfoReply::Data result;
        if (requests.empty())
        {
            return result;
        }
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx{ctx_provider};
        std::transform(cbegin(requests), cend(requests), std::back_inserter(result.replies), [&ctx](auto&& request)
        {
            return get_batch_reply(ctx, request);
        });
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
}

KeysetIdReply::Data Fred::Registry::Keyset::get_keyset_id(const KeysetIdRequest& request)
{
    try
    {
        const auto db_res = [&request]()
        {
            LibFred::OperationContextCreator ctx;
            const auto db_res = ctx.get_conn().exec_params(
                    "SELECT obr.uuid, h.uuid "
                    "FROM object_registry obr "
                    "JOIN history h ON h.id = obr.historyid "
                    "WHERE obr.erdate IS NULL AND "
                          "obr.type = get_object_type_id('keyset') AND "
                          "UPPER(obr.name) = UPPER($1::TEXT) "
                    "FOR SHARE OF obr",
                    Database::QueryParams{get_raw_value_from(request.keyset_handle)});
            ctx.commit_transaction();
            return db_res;
        }();

        if (db_res.size() == 0)
        {
            throw KeysetIdReply::Exception::KeysetDoesNotExist(request);
        }
        if (1 < db_res.size())
        {
            throw Registry::InternalServerError("too many keysets selected by handle");
        }
        auto result = KeysetIdReply::Data{};
        result.keyset_id = Util::make_strong<KeysetId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][0])));
        result.keyset_history_id = Util::make_strong<KeysetHistoryId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][1])));
        return result;
    }
    catch (const KeysetIdReply::Exception::KeysetDoesNotExist&)
    {
        throw;
    }
    catch (const Registry::DatabaseError&)
    {
        throw;
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
