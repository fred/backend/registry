/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/keyset/keyset_common_types.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

Exception::Exception(const std::string& msg)
    : Registry::Exception(msg)
{
}

std::string KeysetLightInfo::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .add("handle", handle)
                                 .finish();
}

std::string DnsKey::to_string() const
{
    return Util::StructToString().add("flags", flags)
                                 .add("protocol", protocol)
                                 .add("alg", alg)
                                 .add("key", key)
                                 .finish();
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
