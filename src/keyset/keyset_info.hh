/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYSET_INFO_HH_3D1A6167560E42D39882D2E857259CC3
#define KEYSET_INFO_HH_3D1A6167560E42D39882D2E857259CC3

#include "src/keyset/keyset_info_request.hh"
#include "src/keyset/keyset_info_reply.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

KeysetInfoReply::Data keyset_info(const KeysetInfoRequest& request);
BatchKeysetInfoReply::Data batch_keyset_info(const BatchKeysetInfoRequest& request);

KeysetIdReply::Data get_keyset_id(const KeysetIdRequest& request);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
