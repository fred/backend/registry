/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_KEYSETS_BY_CONTACT_REQUEST_HH_C6FAF8C598C539C2F0C6AF794DF7B9B0//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_KEYSETS_BY_CONTACT_REQUEST_HH_C6FAF8C598C539C2F0C6AF794DF7B9B0

#include "src/common_types.hh"
#include "src/contact/contact_common_types.hh"
#include "src/pagination_request.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

struct ListKeysetsByContactRequest : Util::Printable<ListKeysetsByContactRequest>
{
    enum class OrderByField
    {
        unspecified,
        crdate,
        handle,
        domains_count,
        sponsoring_registrar_handle
    };
    struct OrderBy : Util::Printable<OrderBy>
    {
        explicit OrderBy(OrderByField, OrderByDirection) noexcept;
        OrderByField field;
        OrderByDirection direction;
        std::string to_string() const;
    };
    Contact::ContactId contact_id;
    boost::optional<PaginationRequest> pagination;
    std::vector<OrderBy> order_by;
    bool aggregate_entire_history;
    std::string to_string() const;
};

std::string to_string(ListKeysetsByContactRequest::OrderByField);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif//LIST_KEYSETS_BY_CONTACT_REQUEST_HH_C6FAF8C598C539C2F0C6AF794DF7B9B0
