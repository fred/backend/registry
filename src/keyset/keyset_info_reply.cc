/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/keyset/keyset_info_reply.hh"

#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Keyset {

std::string KeysetInfoReply::Data::to_string() const
{
    return Util::StructToString().add("keyset_handle", keyset_handle)
                                 .add("keyset_id", keyset_id)
                                 .add("keyset_history_id", keyset_history_id)
                                 .add("dns_keys", dns_keys)
                                 .add("technical_contacts", technical_contacts)
                                 .add("representative_events", representative_events)
                                 .add("sponsoring_registrar", sponsoring_registrar)
                                 .add("domains_count", domains_count)
                                 .finish();
}

KeysetInfoReply::Exception::InvalidData::InvalidData(
        const KeysetInfoRequest&,
        std::set<std::string> fields)
    : Registry::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

KeysetInfoReply::Exception::KeysetDoesNotExist::KeysetDoesNotExist(const KeysetInfoRequest& src)
    : Registry::Exception("keyset " + src.to_string() + " does not exist")
{
}

std::string KeysetIdReply::Data::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("keyset_history_id", keyset_history_id)
                                 .finish();
}

KeysetIdReply::Exception::KeysetDoesNotExist::KeysetDoesNotExist(const KeysetIdRequest& src)
    : Registry::Exception("keyset " + src.to_string() + " does not exist")
{
}

std::string KeysetInfoReply::Exception::InvalidData::to_string() const
{
    return Util::StructToString().add("fields", fields)
                                 .finish();
}

KeysetInfoReply::Exception::Exception(InvalidData src)
    : reasons{std::move(src)}
{ }

KeysetInfoReply::Exception::Exception(KeysetDoesNotExist src)
    : reasons{std::move(src)}
{ }

std::string KeysetInfoReply::Exception::to_string() const
{
    struct Printer : boost::static_visitor<std::string>
    {
        std::string operator()(const KeysetDoesNotExist&) const
        {
            return "keyset_does_not_exist";
        }
        std::string operator()(const InvalidData& invalid_data) const
        {
            return Util::StructToString{}.add("invalid_data", invalid_data)
                                         .finish();
        }
    };
    return boost::apply_visitor(Printer{}, reasons);
}

std::string BatchKeysetInfoReply::Data::BatchReply::to_string() const
{
    struct Printer : boost::static_visitor<std::string>
    {
        std::string operator()(const KeysetInfoReply::Data& data) const
        {
            return Util::StructToString{}.add("data", data)
                                         .finish();
        }
        std::string operator()(const Error& error) const
        {
            return Util::StructToString{}.add("error", error)
                                         .finish();
        }
    };
    return boost::apply_visitor(Printer{}, data_or_error);
}

BatchKeysetInfoReply::Error::Error(BatchKeysetInfoRequest::Request request, KeysetInfoReply::Exception exception)
    : request{std::move(request)},
      exception{std::move(exception)}
{ }

std::string BatchKeysetInfoReply::Error::to_string() const
{
    return Util::StructToString{}.add("request", request)
                                 .add("exception", exception)
                                 .finish();
}

std::string BatchKeysetInfoReply::Data::to_string() const
{
    return Util::StructToString{}.add("replies", replies)
                                 .finish();
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
