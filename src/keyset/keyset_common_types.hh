/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYSET_COMMON_TYPES_HH_7A4374FCFB9A4DC396B1B12C751F5015
#define KEYSET_COMMON_TYPES_HH_7A4374FCFB9A4DC396B1B12C751F5015

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/util/printable.hh"
#include "src/uuid.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Keyset {

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

using KeysetHandle = Util::StrongString<struct KeysetHandle_Tag, Util::Skill::Printable>;

using KeysetId = Uuid<struct KeysetIdTag>;
using KeysetHistoryId = Uuid<struct KeysetHistoryIdTag>;

struct KeysetLightInfo : Util::Printable<KeysetLightInfo>
{
    KeysetId id;
    KeysetHandle handle;

    std::string to_string() const;
};

struct DnsKey: Util::Printable<DnsKey>
{
    uint32_t flags;
    uint32_t protocol;
    uint32_t alg;
    std::string key;

    std::string to_string() const;
};

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
