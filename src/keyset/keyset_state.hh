/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYSET_STATE_HH_B827F199C478499DA2A6A185F9694483
#define KEYSET_STATE_HH_B827F199C478499DA2A6A185F9694483

#include "src/exceptions.hh"
#include "src/keyset/keyset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Keyset {

struct KeysetStateRequest : Util::Printable<KeysetStateRequest>
{
    KeysetId keyset_id;
    std::string to_string() const;
};

struct KeysetState : Util::Printable<KeysetState>
{
    std::map<std::string, bool> flag_presents;
    std::string to_string() const;
};

struct KeysetStateReply
{
    struct Data : Util::Printable<Data>
    {
        KeysetId keyset_id;
        boost::optional<KeysetHistoryId> history_id;
        KeysetState state;
        std::string to_string() const;
    };
    struct Exception
    {
        struct KeysetDoesNotExist : Registry::Exception
        {
            explicit KeysetDoesNotExist(const KeysetStateRequest& request);
        };
    };
};

KeysetStateReply::Data keyset_state(const KeysetStateRequest& request);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
