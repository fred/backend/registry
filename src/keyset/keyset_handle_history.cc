/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/keyset/keyset_handle_history.hh"

#include "src/fred_unwrap.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/keyset/get_keyset_handle_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

KeysetHandleHistoryRequest::KeysetHandleHistoryRequest(const KeysetHandle& _keyset_handle)
    : keyset_handle(_keyset_handle)
{
}

std::string KeysetHandleHistoryRequest::to_string() const
{
    return Util::StructToString().add("keyset_handle", keyset_handle)
                                 .finish();
}

std::string KeysetLifetime::TimeSpec::to_string() const
{
    return Util::StructToString().add("keyset_history_id", keyset_history_id)
                                 .add("timestamp", timestamp)
                                 .finish();
}

std::string KeysetLifetime::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("begin", begin)
                                 .add("end", end)
                                 .finish();
}

std::string KeysetHandleHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("keyset_handle", keyset_handle)
                                 .add("timeline", timeline)
                                 .finish();
}

KeysetHandleHistoryReply::Data keyset_handle_history(const KeysetHandleHistoryRequest& request)
{
    try
    {
        using GetKeysetHandleHistory = LibFred::RegistrableObject::Keyset::GetKeysetHandleHistory;
        LibFred::OperationContextCreator ctx;
        return fred_unwrap(GetKeysetHandleHistory(get_raw_value_from(request.keyset_handle)).exec(ctx));
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
