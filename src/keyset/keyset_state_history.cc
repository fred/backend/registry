/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/keyset/keyset_state_history.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/keyset/get_keyset_state_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

KeysetStateHistoryRequest::KeysetStateHistoryRequest(
        const KeysetId& _keyset_id,
        const KeysetHistoryInterval& _history)
    : keyset_id(_keyset_id),
      history(_history)
{
}

std::string KeysetStateHistoryRequest::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("history", history)
                                 .finish();
}

std::string KeysetStateHistory::to_string() const
{
    return Util::StructToString().add("flags_names", flags_names)
                                 .add("timeline", timeline)
                                 .finish();
}

std::string KeysetStateHistory::Record::to_string() const
{
    return Util::StructToString().add("valid_from", valid_from)
                                 .add("presents", presents)
                                 .finish();
}

std::string KeysetStateHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("history", history)
                                 .finish();
}

KeysetStateHistoryReply::Exception::KeysetDoesNotExist::KeysetDoesNotExist(const KeysetStateHistoryRequest& src)
    : Registry::Exception("keyset " + src.to_string() + " does not exist")
{
}

KeysetStateHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const KeysetStateHistoryRequest& src)
    : Registry::Exception("invalid keyset history interval " + src.to_string())
{
}

KeysetStateHistoryReply::Data keyset_state_history(const KeysetStateHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetKeysetStateHistoryByUuid = LibFred::RegistrableObject::Keyset::GetKeysetStateHistoryByUuid;
        KeysetStateHistoryReply::Data result;
        result.history = fred_unwrap(
                GetKeysetStateHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::keyset>(
                                get_raw_value_from(request.keyset_id)))
                    .exec(ctx, fred_wrap(request.history)));
        result.keyset_id = request.keyset_id;
        return result;
    }
    catch (const LibFred::RegistrableObject::Keyset::GetKeysetStateHistoryByUuid::DoesNotExist&)
    {
        throw KeysetStateHistoryReply::Exception::KeysetDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Keyset::GetKeysetStateHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw KeysetStateHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
