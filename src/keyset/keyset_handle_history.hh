/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef KEYSET_HANDLE_HISTORY_HH_8135422C9AD44210AE4C5DB3BE687999
#define KEYSET_HANDLE_HISTORY_HH_8135422C9AD44210AE4C5DB3BE687999

#include "src/keyset/keyset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

struct KeysetHandleHistoryRequest : Util::Printable<KeysetHandleHistoryRequest>
{
    explicit KeysetHandleHistoryRequest(const KeysetHandle& keyset_handle);
    KeysetHandle keyset_handle;
    std::string to_string() const;
};

struct KeysetLifetime : Util::Printable<KeysetLifetime>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    struct TimeSpec : Util::Printable<TimeSpec>
    {
        KeysetHistoryId keyset_history_id;
        TimePoint timestamp;
        std::string to_string() const;
    };
    KeysetId keyset_id;
    TimeSpec begin;
    boost::optional<TimeSpec> end;
    std::string to_string() const;
};

struct KeysetHandleHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        KeysetHandle keyset_handle;
        std::vector<KeysetLifetime> timeline;
        std::string to_string() const;
    };
};

KeysetHandleHistoryReply::Data keyset_handle_history(const KeysetHandleHistoryRequest& request);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
