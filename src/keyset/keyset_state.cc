/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/keyset/keyset_state.hh"

#include "src/fred_unwrap.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/keyset/get_keyset_state.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

std::string KeysetStateRequest::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .finish();
}

std::string KeysetState::to_string() const
{
    if (flag_presents.empty())
    {
        return "{ }";
    }
    std::string buffer;
    for (const auto& presents : flag_presents)
    {
        if (!buffer.empty())
        {
            buffer += ", ";
        }
        buffer += "'" + presents.first + "': " + (presents.second ? "presents" : "absents");
    }
    return "{ " + buffer + " }";
}

std::string KeysetStateReply::Data::to_string() const
{
    return Util::StructToString().add("state", state).finish();
}

KeysetStateReply::Exception::KeysetDoesNotExist::KeysetDoesNotExist(const KeysetStateRequest& src)
    : Registry::Exception("keyset " + src.to_string() + " does not exist")
{
}

KeysetStateReply::Data keyset_state(const KeysetStateRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        KeysetStateReply::Data result;
        const auto keyset_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::keyset>(
                Util::get_raw_value_from(request.keyset_id));
        result.state = fred_unwrap(LibFred::RegistrableObject::Keyset::GetKeysetStateByUuid(keyset_uuid).exec(ctx));
        result.keyset_id = request.keyset_id;
        return result;
    }
    catch (const LibFred::RegistrableObject::Keyset::GetKeysetStateByHandle::DoesNotExist& e)
    {
        throw KeysetStateReply::Exception::KeysetDoesNotExist(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
