/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEARCH_KEYSET_HH_76BEE8B5C32248E4B0443660BDCB2E47
#define SEARCH_KEYSET_HH_76BEE8B5C32248E4B0443660BDCB2E47

#include "src/keyset/keyset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <set>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

enum class KeysetItem
{
    keyset_handle,
    public_key
};

struct SearchKeysetRequest : Util::Printable<SearchKeysetRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> limit;
    std::set<KeysetItem> searched_items;
    std::string to_string() const;
};

struct SearchKeysetReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            KeysetId keyset_id;
            std::set<KeysetItem> matched_items;
            std::string to_string() const;
        };
        std::vector<Result> most_similar_keysets;
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::set<KeysetItem> searched_items;
        std::string to_string() const;
    };
    Data data;
};

SearchKeysetReply::Data search_keyset(const SearchKeysetRequest& request);

struct SearchKeysetHistoryRequest : Util::Printable<SearchKeysetHistoryRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> max_number_of_keysets;
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    boost::optional<TimePoint> data_valid_from;
    boost::optional<TimePoint> data_valid_to;
    std::set<KeysetItem> searched_items;
    std::string to_string() const;
};

struct SearchKeysetHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            struct HistoryPeriod : Util::Printable<HistoryPeriod>
            {
                using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
                std::set<KeysetItem> matched_items;
                TimePoint valid_from;
                boost::optional<TimePoint> valid_to;
                std::vector<KeysetHistoryId> keyset_history_ids;
                std::string to_string() const;
            };
            KeysetId object_id;
            std::vector<HistoryPeriod> histories;
            std::string to_string() const;
        };
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::vector<Result> results;
        std::set<KeysetItem> searched_items;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ChronologyViolation : Registry::Exception
        {
            explicit ChronologyViolation(const SearchKeysetHistoryRequest& request);
        };
    };
};

SearchKeysetHistoryReply::Data search_keyset(const SearchKeysetHistoryRequest& request);

}//namespace Fred::Registry::Keyset
}//namespace Fred::Registry
}//namespace Fred

#endif//SEARCH_KEYSET_HH_76BEE8B5C32248E4B0443660BDCB2E47
