/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/keyset/list_keysets_by_contact_request.hh"
#include "src/util/struct_to_string.hh"

#include <stdexcept>

namespace Fred {
namespace Registry {
namespace Keyset {

ListKeysetsByContactRequest::OrderBy::OrderBy(OrderByField field, OrderByDirection direction) noexcept
    : field{field},
      direction{direction}
{ }

std::string ListKeysetsByContactRequest::OrderBy::to_string() const
{
    return Util::StructToString().add("field", Keyset::to_string(field))
                                 .add("direction", Registry::to_string(direction))
                                 .finish();
}

std::string ListKeysetsByContactRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("pagination", pagination)
                                 .add("order_by", order_by)
                                 .add("aggregate_entire_history", aggregate_entire_history)
                                 .finish();
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::Keyset;

std::string Fred::Registry::Keyset::to_string(ListKeysetsByContactRequest::OrderByField field)
{
    switch (field)
    {
        case ListKeysetsByContactRequest::OrderByField::unspecified:
            return "unspecified";
        case ListKeysetsByContactRequest::OrderByField::crdate:
            return "crdate";
        case ListKeysetsByContactRequest::OrderByField::handle:
            return "handle";
        case ListKeysetsByContactRequest::OrderByField::domains_count:
            return "domains_count";
        case ListKeysetsByContactRequest::OrderByField::sponsoring_registrar_handle:
            return "sponsoring_registrar_handle";
    }
    throw std::runtime_error{"unknown ListKeysetsByContactRequest::OrderByField item"};
}
