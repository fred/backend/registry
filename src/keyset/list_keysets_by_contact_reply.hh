/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_KEYSETS_BY_CONTACT_REPLY_HH_A13222E3666CC6568DEC45375DABC87A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_KEYSETS_BY_CONTACT_REPLY_HH_A13222E3666CC6568DEC45375DABC87A

#include "src/contact/contact_common_types.hh"
#include "src/exceptions.hh"
#include "src/keyset/list_keysets_by_contact_request.hh"
#include "src/keyset/keyset_common_types.hh"
#include "src/object/object_common_types.hh"
#include "src/pagination_reply.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

struct ListKeysetsByContactReply
{
    struct Data : Util::Printable<Data>
    {
        struct Keyset : Util::Printable<Keyset>
        {
            KeysetLightInfo keyset;
            KeysetHistoryId keyset_history_id;
            bool is_deleted;
            std::string to_string() const;
        };
        std::vector<Keyset> keysets;
        boost::optional<PaginationReply> pagination;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ListKeysetsByContactRequest& src);
        };
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const ListKeysetsByContactRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif//LIST_KEYSETS_BY_CONTACT_REPLY_HH_A13222E3666CC6568DEC45375DABC87A
