/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYSET_DATA_HISTORY_HH_638473B258FB4CB686B4C3DAEF1546F9
#define KEYSET_DATA_HISTORY_HH_638473B258FB4CB686B4C3DAEF1546F9

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/keyset/keyset_common_types.hh"
#include "src/keyset/keyset_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>


namespace Fred {
namespace Registry {
namespace Keyset {

struct KeysetDataHistoryRequest : Util::Printable<KeysetDataHistoryRequest>
{
    KeysetDataHistoryRequest(
            const KeysetId& keyset_id,
            const KeysetHistoryInterval& history);
    KeysetId keyset_id;
    KeysetHistoryInterval history;
    std::string to_string() const;
};

struct KeysetDataHistory : Util::Printable<KeysetDataHistory>
{
    using TimePoint = KeysetHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        KeysetHistoryId keyset_history_id;
        TimePoint valid_from;
        LogEntryId log_entry_id;
        std::string to_string() const;
    };
    KeysetId keyset_id;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct KeysetDataHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        KeysetDataHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct KeysetDoesNotExist : Registry::Exception
        {
            explicit KeysetDoesNotExist(const KeysetDataHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const KeysetDataHistoryRequest& request);
        };
    };
};

KeysetDataHistoryReply::Data keyset_data_history(const KeysetDataHistoryRequest& request);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
