/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_KEYSETS_BY_CONTACT_HH_148CC0CB7BA1B8D44DF702E6BC7FAC41//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_KEYSETS_BY_CONTACT_HH_148CC0CB7BA1B8D44DF702E6BC7FAC41

#include "src/keyset/list_keysets_by_contact_reply.hh"
#include "src/keyset/list_keysets_by_contact_request.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

ListKeysetsByContactReply::Data list_keysets_by_contact(const ListKeysetsByContactRequest& request);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif//LIST_KEYSETS_BY_CONTACT_HH_148CC0CB7BA1B8D44DF702E6BC7FAC41
