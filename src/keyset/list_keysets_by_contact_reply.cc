/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/keyset/list_keysets_by_contact_reply.hh"
#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Keyset {

std::string ListKeysetsByContactReply::Data::Keyset::to_string() const
{
    return Util::StructToString().add("keyset", keyset)
                                 .add("keyset_history_id", keyset_history_id)
                                 .add("is_deleted", is_deleted)
                                 .finish();
}

std::string ListKeysetsByContactReply::Data::to_string() const
{
    return Util::StructToString().add("keysets", keysets)
                                 .add("pagination", pagination)
                                 .finish();
}

ListKeysetsByContactReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ListKeysetsByContactRequest& src)
    : Registry::Exception{"contact " + src.to_string() + " does not exist"}
{ }

ListKeysetsByContactReply::Exception::InvalidData::InvalidData(const ListKeysetsByContactRequest& src, std::set<std::string> fields)
    : Registry::Exception{"invalid data: " + src.to_string()},
      fields{std::move(fields)}
{ }

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
