/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef KEYSET_STATE_HISTORY_HH_438633F6115E44A3BCE1B75AB31C5E00
#define KEYSET_STATE_HISTORY_HH_438633F6115E44A3BCE1B75AB31C5E00

#include "src/keyset/keyset_common_types.hh"
#include "src/keyset/keyset_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

struct KeysetStateHistoryRequest : Util::Printable<KeysetStateHistoryRequest>
{
    KeysetStateHistoryRequest(
            const KeysetId& _keyset_id,
            const KeysetHistoryInterval& _history);
    KeysetId keyset_id;
    KeysetHistoryInterval history;
    std::string to_string() const;
};

struct KeysetStateHistory : Util::Printable<KeysetStateHistory>
{
    using TimePoint = KeysetHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        TimePoint valid_from;
        std::vector<bool> presents;
        std::string to_string() const;
    };
    std::vector<std::string> flags_names;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct KeysetStateHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        KeysetId keyset_id;
        KeysetStateHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct KeysetDoesNotExist : Registry::Exception
        {
            explicit KeysetDoesNotExist(const KeysetStateHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const KeysetStateHistoryRequest& request);
        };
    };
};

KeysetStateHistoryReply::Data keyset_state_history(const KeysetStateHistoryRequest& request);

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
