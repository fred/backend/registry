/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYSET_INFO_REPLY_HH_1DD0CDDA65D44C96AC5D7DECAEE57865
#define KEYSET_INFO_REPLY_HH_1DD0CDDA65D44C96AC5D7DECAEE57865

#include "src/contact/contact_common_types.hh"
#include "src/exceptions.hh"
#include "src/keyset/keyset_common_types.hh"
#include "src/keyset/keyset_info_request.hh"
#include "src/object/object_common_types.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <cstdint>
#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

struct KeysetInfoReply
{
    struct Data : Util::Printable<Data>
    {
        KeysetHandle keyset_handle;
        KeysetId keyset_id;
        KeysetHistoryId keyset_history_id;
        std::vector<DnsKey> dns_keys;
        std::vector<Contact::ContactLightInfo> technical_contacts;
        Object::ObjectEvents representative_events;
        Registrar::RegistrarHandle sponsoring_registrar;
        boost::optional<std::int64_t> domains_count;
        std::string to_string() const;
    };
    struct Exception : Util::Printable<Exception>
    {
        struct KeysetDoesNotExist : Registry::Exception
        {
            explicit KeysetDoesNotExist(const KeysetInfoRequest& src);
        };
        struct InvalidData : Registry::Exception, Util::Printable<Exception>
        {
            explicit InvalidData(const KeysetInfoRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
            std::string to_string() const;
        };
        explicit Exception(InvalidData);
        explicit Exception(KeysetDoesNotExist);
        using Reasons = boost::variant<InvalidData, KeysetDoesNotExist>;
        Reasons reasons;
        std::string to_string() const;
    };
    using Result = boost::variant<Data, Exception::InvalidData, Exception::KeysetDoesNotExist>;
};

struct BatchKeysetInfoReply
{
    struct Error : Util::Printable<Error>
    {
        explicit Error(BatchKeysetInfoRequest::Request, KeysetInfoReply::Exception);
        BatchKeysetInfoRequest::Request request;
        KeysetInfoReply::Exception exception;
        std::string to_string() const;
    };
    using DataOrError = boost::variant<KeysetInfoReply::Data, Error>;
    struct Data : Util::Printable<Data>
    {
        struct BatchReply : Util::Printable<BatchReply>
        {
            DataOrError data_or_error;
            std::string to_string() const;
        };
        std::vector<BatchReply> replies;
        std::string to_string() const;
    };
};

struct KeysetIdReply
{
    struct Data : Util::Printable<Data>
    {
        KeysetId keyset_id;
        KeysetHistoryId keyset_history_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct KeysetDoesNotExist : Registry::Exception
        {
            explicit KeysetDoesNotExist(const KeysetIdRequest& src);
        };
    };
};

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
