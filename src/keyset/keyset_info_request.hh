/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KEYSET_INFO_REQUEST_HH_AD0CFB82D01142AB8DB77AB2529A1DE4
#define KEYSET_INFO_REQUEST_HH_AD0CFB82D01142AB8DB77AB2529A1DE4

#include "src/keyset/keyset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Keyset {

struct KeysetInfoRequest : Util::Printable<KeysetInfoRequest>
{
    KeysetId keyset_id;
    boost::optional<KeysetHistoryId> keyset_history_id;
    bool include_domains_count;
    std::string to_string() const;
};

struct BatchKeysetInfoRequest : Util::Printable<BatchKeysetInfoRequest>
{
    struct Request : Util::Printable<Request>
    {
        std::string keyset_id;
        std::string keyset_history_id;
        bool include_domains_count;
        std::string to_string() const;
    };
    std::vector<Request> requests;
    std::string to_string() const;
};

struct KeysetIdRequest : Util::Printable<KeysetIdRequest>
{
    KeysetHandle keyset_handle;
    std::string to_string() const;
};

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred

#endif
