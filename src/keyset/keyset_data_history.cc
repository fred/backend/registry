/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/keyset/keyset_data_history.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/opcontext.hh"
#include "libfred/registrable_object/keyset/get_keyset_data_history.hh"
#include "libfred/registrable_object/uuid.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Keyset {

KeysetDataHistoryRequest::KeysetDataHistoryRequest(
        const KeysetId& _keyset_id,
        const KeysetHistoryInterval& _history)
    : keyset_id(_keyset_id),
      history(_history)
{
}

std::string KeysetDataHistoryRequest::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("history", history)
                                 .finish();
}

std::string KeysetDataHistory::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("timeline", timeline)
                                 .add("valid_to", valid_to)
                                 .finish();
}

std::string KeysetDataHistory::Record::to_string() const
{
    return Util::StructToString().add("keyset_history_id", keyset_history_id)
                                 .add("valid_from", valid_from)
                                 .add("log_entry_id", log_entry_id)
                                 .finish();
}

std::string KeysetDataHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("history", history).finish();
}

KeysetDataHistoryReply::Exception::KeysetDoesNotExist::KeysetDoesNotExist(const KeysetDataHistoryRequest& src)
    : Registry::Exception("keyset " + src.to_string() + " does not exist")
{
}

KeysetDataHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const KeysetDataHistoryRequest& src)
    : Registry::Exception("invalid keyset history interval " + src.to_string())
{
}

KeysetDataHistoryReply::Data keyset_data_history(const KeysetDataHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetKeysetDataHistoryByUuid = LibFred::RegistrableObject::Keyset::GetKeysetDataHistoryByUuid;
        return fred_unwrap(
                GetKeysetDataHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::keyset>(
                                get_raw_value_from(request.keyset_id)))
                        .exec(ctx, fred_wrap(request.history)));
    }
    catch (const LibFred::RegistrableObject::Keyset::GetKeysetDataHistoryByUuid::DoesNotExist&)
    {
        throw KeysetDataHistoryReply::Exception::KeysetDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Keyset::GetKeysetDataHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw KeysetDataHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
    throw KeysetDataHistoryReply::Exception::KeysetDoesNotExist(request);
}

} // namespace Fred::Registry::Keyset
} // namespace Fred::Registry
} // namespace Fred
