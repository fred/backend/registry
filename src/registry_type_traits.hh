/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef REGISTRY_TYPE_TRAITS_HH_8938DA10CF9055AAEA9820BDF08D5F01//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define REGISTRY_TYPE_TRAITS_HH_8938DA10CF9055AAEA9820BDF08D5F01

namespace Fred {
namespace Registry {

template <typename>
struct RegistryTypeTraits;

}//namespace Fred::Registry
}//namespace Fred

#endif//REGISTRY_TYPE_TRAITS_HH_8938DA10CF9055AAEA9820BDF08D5F01
