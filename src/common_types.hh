/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_TYPES_HH_DF4E9B709F4A027E480CF692F67059E5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMMON_TYPES_HH_DF4E9B709F4A027E480CF692F67059E5

#include "src/util/strong_type.hh"

#include <boost/uuid/uuid.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {

using AuthInfo = Util::StrongString<struct AuthInfo_Tag, Util::Skill::Printable>;
using FileId = Util::StrongType<boost::uuids::uuid, struct FileId_Tag, Util::Skill::Printable>;
using PhoneNumber = Util::StrongString<struct PhoneNumber_Tag, Util::Skill::Printable>;
using EmailAddress = Util::StrongString<struct EmailAddress_Tag, Util::Skill::Printable>;
using PostalCode = Util::StrongString<struct PostalCode_Tag, Util::Skill::Printable>;
using CountryCode = Util::StrongString<struct CountryCode_Tag, Util::Skill::Printable>;
using CompanyRegistrationNumber = Util::StrongString<struct CompanyRegistrationNumber_Tag, Util::Skill::Printable>;
using VatIdentificationNumber = Util::StrongString<struct VatIdentificationNumber_Tag, Util::Skill::Printable>;
using Url = Util::StrongString<struct Url_Tag, Util::Skill::Printable>;
using LogEntryId = Util::StrongType<std::string, struct LogEntryIdTag_, Util::Skill::Printable>;

struct PlaceAddress : Util::Printable<PlaceAddress>
{
    std::vector<std::string> street;
    std::string city;
    std::string state_or_province;
    PostalCode postal_code;
    CountryCode country_code;
    std::string to_string() const;
};

enum class OrderByDirection
{
    ascending,
    descending
};

std::string to_string(OrderByDirection);

}//namespace Fred::Registry
}//namespace Fred

#endif//COMMON_TYPES_HH_DF4E9B709F4A027E480CF692F67059E5
