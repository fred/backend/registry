/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/cfg.hh"

#include <syslog.h>

#include <iostream>
#include <fstream>
#include <sstream>

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "registry.conf"
#endif

namespace Cfg {

namespace {

LogSeverity make_severity(const std::string& str)
{
    if (str == "emerg")
    {
        return LogSeverity::emerg;
    }
    if (str == "alert")
    {
        return LogSeverity::alert;
    }
    if (str == "crit")
    {
        return LogSeverity::crit;
    }
    if (str == "err")
    {
        return LogSeverity::err;
    }
    if (str == "warning")
    {
        return LogSeverity::warning;
    }
    if (str == "notice")
    {
        return LogSeverity::notice;
    }
    if (str == "info")
    {
        return LogSeverity::info;
    }
    if (str == "debug")
    {
        return LogSeverity::debug;
    }
    if (str == "trace")
    {
        return LogSeverity::trace;
    }
    throw Exception("not a valid severity \"" + str + "\"");
}

void required_one_of(const boost::program_options::variables_map& variables_map, const char* variable)
{
    if (variables_map.count(variable) == 0)
    {
        throw MissingOption("missing one of options: '" + std::string(variable) + "'");
    }
}

template <typename ...Ts>
void required_one_of(
        const boost::program_options::variables_map& variables_map,
        const char* first_variable,
        const Ts* ...other_variables)
{
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    if (has_option(first_variable))
    {
        return;
    }
    try
    {
        required_one_of(variables_map, other_variables...);
    }
    catch (const MissingOption& e)
    {
        throw MissingOption(e.what() + std::string(", '") + first_variable + "'");
    }
}

constexpr int invalid_argc = -1;
constexpr const char* const invalid_argv[0] = { };

}//namespace Cfg::{anonymous}

const Options& Options::get()
{
    return init(invalid_argc, invalid_argv);
}

const Options& Options::init(int argc, const char* const* argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != invalid_argc) || (argv != invalid_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            throw std::runtime_error("First call of Cfg::Options::init must contain valid arguments");
        }
        static const Options singleton(argc, argv);
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        throw std::runtime_error("Only first call of Cfg::Options::init can contain valid arguments");
    }
    return *singleton_ptr;
}

Options::Options(int argc, const char* const* argv)
{
    boost::program_options::options_description generic_options("Generic options");
    std::string opt_config_file_name;
    generic_options.add_options()
        ("help,h", "produce help message")
        ("version,V", "display version information")
        ("config,c",
#ifdef DEFAULT_CONFIG_FILE
         boost::program_options::value<std::string>(&opt_config_file_name)->default_value(std::string(DEFAULT_CONFIG_FILE)),
#else
         boost::program_options::value<std::string>(&opt_config_file_name),
#endif
         "name of a file of a configuration.");

    boost::program_options::options_description database_options("Database options");
    std::string host;
    int port;
    std::string user;
    std::string dbname;
    std::string password;
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>(&host), "name of host to connect to")
        ("database.port", boost::program_options::value<int>(&port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>(&user), "PostgreSQL user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>(&dbname), "the database name")
        ("database.password", boost::program_options::value<std::string>(&password), "password used for password authentication.");

    boost::program_options::options_description log_options("Logging options");
    std::string device;
    std::string default_min_severity;
    log_options.add_options()
        ("log.device", boost::program_options::value<std::string>(&device), "where to log (console/file/syslog)")
        ("log.min_severity", boost::program_options::value<std::string>(&default_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_console_options("Logging on console options");
    std::string log_console_min_severity;
    log_console_options.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>(&log_console_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_file_options("Logging into file options");
    std::string log_file_name;
    std::string log_file_min_severity;
    log_file_options.add_options()
        ("log.file.file_name",
         boost::program_options::value<std::string>(&log_file_name),
         "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>(&log_file_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_syslog_options("Logging into syslog options");
    std::string ident;
    int facility_local_offset;
    std::string log_syslog_min_severity;
    log_syslog_options.add_options()
        ("log.syslog.ident", boost::program_options::value<std::string>(&ident), "what ident to log with (default is empty string)")
        ("log.syslog.facility",
         boost::program_options::value<int>(&facility_local_offset),
         "what LOG_LOCALx facility to log with (x in range 0..7, default means facility LOG_USER)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>(&log_syslog_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description fred_options("FRED options");
    std::string registrar_originator;
    fred_options.add_options()
        ("fred.registrar_originator", boost::program_options::value<std::string>(&registrar_originator), "which registrar does modifying operations");

    boost::program_options::options_description server_options("Server options");
    std::string listen_on;
    server_options.add_options()
        ("registry.listen", boost::program_options::value<std::string>(&listen_on), "server's listen address.");

    boost::program_options::options_description command_line_options("fred-registry-services options");
    command_line_options
        .add(generic_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(fred_options)
        .add(server_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(fred_options)
        .add(server_options);

    boost::program_options::variables_map variables_map;
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(argc, argv)
                    .options(command_line_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        throw UnknownOption(out.str());
    }
    boost::program_options::notify(variables_map);

    if (has_option("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        throw AllDone(out.str());
    }
    if (has_option("version"))
    {
        std::ostringstream out;
        out << "Server Version: " << PACKAGE_VERSION << std::endl;
        out << "Registry API Version: " << API_REGISTRY_VERSION << std::endl;
        out << "Diagnostics API Version: " << API_DIAGNOSTICS_VERSION << std::endl;
        out << "LibDiagnostics Version: " << LIBDIAGNOSTICS_VERSION << std::endl;
        out << "LibFred Version: " << LIBFRED_VERSION << std::endl;
        throw AllDone(out.str());
    }
    const bool config_file_name_presents = has_option("config");
    if (config_file_name_presents)
    {
        this->config_file_name = opt_config_file_name;
        std::ifstream config_file(opt_config_file_name);
        if (!config_file)
        {
            throw Exception("can not open config file \"" + opt_config_file_name + "\"");
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            throw UnknownOption(out.str());
        }
        boost::program_options::notify(variables_map);
    }

    if (has_option("database.host"))
    {
        this->database.host = host;
    }
    if (has_option("database.port"))
    {
        this->database.port = port;
    }
    if (has_option("database.user"))
    {
        this->database.user = user;
    }
    if (has_option("database.dbname"))
    {
        this->database.dbname = dbname;
    }
    if (has_option("database.password"))
    {
        this->database.password = password;
    }

    if (has_option("log.device"))
    {
        boost::optional<LogSeverity> log_min_severity = LogSeverity::emerg;
        if (has_option("log.min_severity"))
        {
            log_min_severity = make_severity(default_min_severity);
        }
        else
        {
            log_min_severity = boost::none;
        }
        if (device == "console")
        {
            required_one_of(variables_map, "log.min_severity", "log.console.min_severity");
            Console console;
            if (has_option("log.console.min_severity"))
            {
                console.min_severity = make_severity(log_console_min_severity);
            }
            else
            {
                console.min_severity = *log_min_severity;
            }
            this->log = console;
        }
        else if (device == "file")
        {
            required_one_of(variables_map, "log.file.file_name");
            required_one_of(variables_map, "log.min_severity", "log.file.min_severity");
            LogFile log_file;
            log_file.file_name = log_file_name;
            if (has_option("log.file.min_severity"))
            {
                log_file.min_severity = make_severity(log_file_min_severity);
            }
            else
            {
                log_file.min_severity = *log_min_severity;
            }
            this->log = log_file;
        }
        else if (device == "syslog")
        {
            SysLog sys_log;
            if (has_option("log.syslog.ident"))
            {
                sys_log.ident = ident;
            }
            static constexpr int default_facility = LOG_USER;
            static const auto make_facility = [](int offset)
            {
                switch (offset)
                {
                    case 0: return LOG_LOCAL0;
                    case 1: return LOG_LOCAL1;
                    case 2: return LOG_LOCAL2;
                    case 3: return LOG_LOCAL3;
                    case 4: return LOG_LOCAL4;
                    case 5: return LOG_LOCAL5;
                    case 6: return LOG_LOCAL6;
                    case 7: return LOG_LOCAL7;
                }
                return default_facility;
            };
            if (has_option("log.syslog.facility"))
            {
                if ((facility_local_offset < 0) || (7 < facility_local_offset))
                {
                    throw Exception("option 'log.syslog.facility' out of range [0, 7]");
                }
                sys_log.facility = make_facility(facility_local_offset);
            }
            else
            {
                sys_log.facility = default_facility;
            }
            required_one_of(variables_map, "log.min_severity", "log.syslog.min_severity");
            if (has_option("log.syslog.min_severity"))
            {
                sys_log.min_severity = make_severity(log_syslog_min_severity);
            }
            else
            {
                sys_log.min_severity = *log_min_severity;
            }
            this->log = sys_log;
        }
        else
        {
            throw Exception("Invalid value of log.device");
        }
    }

    if (has_option("fred.registrar_originator"))
    {
        this->fred.registrar_originator = registrar_originator;
    }
    else
    {
        this->fred.registrar_originator = boost::none;
    }

    required_one_of(variables_map, "registry.listen");
    this->registry.listen_on = listen_on;
}

AllDone::AllDone(const std::string& msg)
    : msg_(msg)
{ }

const char* AllDone::what() const noexcept
{
    return msg_.c_str();
}

Exception::Exception(const std::string& msg)
    : std::runtime_error(msg)
{ }

UnknownOption::UnknownOption(const std::string& msg)
    : Exception(msg)
{ }

MissingOption::MissingOption(const std::string& msg)
    : Exception(msg)
{ }

}//namespace Cfg
