/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_COMMON_TYPES_HH_0C3212FF030730B8890AA7B96CB07643//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define OBJECT_COMMON_TYPES_HH_0C3212FF030730B8890AA7B96CB07643

#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>

namespace Fred {
namespace Registry {
namespace Object {

struct ObjectEvents : Util::Printable<ObjectEvents>
{
    struct EventData : Util::Printable<EventData>
    {
        using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
        explicit EventData(
                boost::optional<TimePoint> at = boost::none,
                boost::optional<Registrar::RegistrarHandle> by_registrar = boost::none);
        boost::optional<TimePoint> at;
        boost::optional<Registrar::RegistrarHandle> by_registrar;
        std::string to_string() const;
    };
    EventData registered;
    boost::optional<EventData> transferred;
    boost::optional<EventData> updated;
    boost::optional<EventData> unregistered;
    std::string to_string() const;
};

}//namespace Fred::Registry::Object
}//namespace Fred::Registry
}//namespace Fred

#endif//OBJECT_COMMON_TYPES_HH_0C3212FF030730B8890AA7B96CB07643
