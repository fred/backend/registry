/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/object/object_common_types.hh"
#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Object {

std::string ObjectEvents::to_string() const
{
    return Util::StructToString().add("registered", registered)
                                 .add("transferred", transferred)
                                 .add("updated", updated)
                                 .add("unregistered", unregistered)
                                 .finish();
}

std::string ObjectEvents::EventData::to_string() const
{
    return Util::StructToString().add("at", at)
                                 .add("by_registrar", by_registrar)
                                 .finish();
}

ObjectEvents::EventData::EventData(
        boost::optional<TimePoint> at,
        boost::optional<Registrar::RegistrarHandle> by_registrar)
    : at{std::move(at)},
      by_registrar{std::move(by_registrar)}
{ }

}//namespace Fred::Registry::Object
}//namespace Fred::Registry
}//namespace Fred
