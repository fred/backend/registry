/*
 * Copyright (C) 2019-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/fred_wrap.hh"

namespace Fred {
namespace Registry {

namespace {

template <typename T>
struct ContactHistoryIntervalVisitor : boost::static_visitor<T>
{
    T operator()(const Contact::ContactHistoryInterval::NoLimit&) const
    {
        return T(LibFred::RegistrableObject::HistoryInterval::NoLimit());
    }
    T operator()(const Contact::ContactHistoryInterval::HistoryId& src) const
    {
        return T(::Util::make_strong<LibFred::RegistrableObject::ObjectHistoryUuid>(get_raw_value_from(src)));
    }
    T operator()(const Contact::ContactHistoryInterval::TimePoint& src) const
    {
        return T(src);
    }
};

template <typename T>
struct DomainHistoryIntervalVisitor : boost::static_visitor<T>
{
    T operator()(const Domain::DomainHistoryInterval::NoLimit&) const
    {
        return T(LibFred::RegistrableObject::HistoryInterval::NoLimit());
    }
    T operator()(const Domain::DomainHistoryInterval::HistoryId& src) const
    {
        return T(::Util::make_strong<LibFred::RegistrableObject::ObjectHistoryUuid>(get_raw_value_from(src)));
    }
    T operator()(const Domain::DomainHistoryInterval::TimePoint& src) const
    {
        return T(src);
    }
};

template <typename T>
struct KeysetHistoryIntervalVisitor : boost::static_visitor<T>
{
    T operator()(const Keyset::KeysetHistoryInterval::NoLimit&) const
    {
        return T(LibFred::RegistrableObject::HistoryInterval::NoLimit());
    }
    T operator()(const Keyset::KeysetHistoryInterval::HistoryId& src) const
    {
        return T(::Util::make_strong<LibFred::RegistrableObject::ObjectHistoryUuid>(get_raw_value_from(src)));
    }
    T operator()(const Keyset::KeysetHistoryInterval::TimePoint& src) const
    {
        return T(src);
    }
};

template <typename T>
struct NssetHistoryIntervalVisitor : boost::static_visitor<T>
{
    T operator()(const Nsset::NssetHistoryInterval::NoLimit&) const
    {
        return T(LibFred::RegistrableObject::HistoryInterval::NoLimit());
    }
    T operator()(const Nsset::NssetHistoryInterval::HistoryId& src) const
    {
        return T(::Util::make_strong<LibFred::RegistrableObject::ObjectHistoryUuid>(get_raw_value_from(src)));
    }
    T operator()(const Nsset::NssetHistoryInterval::TimePoint& src) const
    {
        return T(src);
    }
};

LibFred::RegistrableObject::HistoryInterval::LowerLimit fred_wrap_contact_history_interval_lower_limit(
        const Contact::ContactHistoryInterval::Limit& src)
{
    return boost::apply_visitor(ContactHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::LowerLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::UpperLimit fred_wrap_contact_history_interval_upper_limit(
        const Contact::ContactHistoryInterval::Limit& src)
{
    return boost::apply_visitor(ContactHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::UpperLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::LowerLimit fred_wrap_domain_history_interval_lower_limit(
        const Domain::DomainHistoryInterval::Limit& src)
{
    return boost::apply_visitor(DomainHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::LowerLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::UpperLimit fred_wrap_domain_history_interval_upper_limit(
        const Domain::DomainHistoryInterval::Limit& src)
{
    return boost::apply_visitor(DomainHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::UpperLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::LowerLimit fred_wrap_keyset_history_interval_lower_limit(
        const Keyset::KeysetHistoryInterval::Limit& src)
{
    return boost::apply_visitor(KeysetHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::LowerLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::UpperLimit fred_wrap_keyset_history_interval_upper_limit(
        const Keyset::KeysetHistoryInterval::Limit& src)
{
    return boost::apply_visitor(KeysetHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::UpperLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::LowerLimit fred_wrap_nsset_history_interval_lower_limit(
        const Nsset::NssetHistoryInterval::Limit& src)
{
    return boost::apply_visitor(NssetHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::LowerLimit>(), src);
}

LibFred::RegistrableObject::HistoryInterval::UpperLimit fred_wrap_nsset_history_interval_upper_limit(
        const Nsset::NssetHistoryInterval::Limit& src)
{
    return boost::apply_visitor(NssetHistoryIntervalVisitor<LibFred::RegistrableObject::HistoryInterval::UpperLimit>(), src);
}

}//namespace Fred::Registry::{anonymous}

template <>
LibFred::RegistrableObject::HistoryInterval fred_wrap(const Contact::ContactHistoryInterval& src)
{
    return LibFred::RegistrableObject::HistoryInterval(
            fred_wrap_contact_history_interval_lower_limit(src.lower_limit),
            fred_wrap_contact_history_interval_upper_limit(src.upper_limit));
}

template <>
LibFred::RegistrableObject::HistoryInterval fred_wrap(const Domain::DomainHistoryInterval& src)
{
    return LibFred::RegistrableObject::HistoryInterval(
            fred_wrap_domain_history_interval_lower_limit(src.lower_limit),
            fred_wrap_domain_history_interval_upper_limit(src.upper_limit));
}

template <>
LibFred::RegistrableObject::HistoryInterval fred_wrap(const Keyset::KeysetHistoryInterval& src)
{
    return LibFred::RegistrableObject::HistoryInterval(
            fred_wrap_keyset_history_interval_lower_limit(src.lower_limit),
            fred_wrap_keyset_history_interval_upper_limit(src.upper_limit));
}

template <>
LibFred::RegistrableObject::HistoryInterval fred_wrap(const Nsset::NssetHistoryInterval& src)
{
    return LibFred::RegistrableObject::HistoryInterval(
            fred_wrap_nsset_history_interval_lower_limit(src.lower_limit),
            fred_wrap_nsset_history_interval_upper_limit(src.upper_limit));
}

template <>
LibFred::Registrar::EppAuth::EppAuthRecordUuid fred_wrap(const Registrar::RegistrarEppCredentialsId& src)
{
    return LibFred::Registrar::EppAuth::EppAuthRecordUuid{get_raw_value_from(src)};
}

} // namespace Fred::Registry
} // namespace Fred
