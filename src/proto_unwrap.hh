/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_UNWRAP_HH_1E3622FA61F8CFDE5B96210B52BDD131//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PROTO_UNWRAP_HH_1E3622FA61F8CFDE5B96210B52BDD131

#include "src/common_types.hh"

#include "src/contact/contact_data_history.hh"
#include "src/contact/contact_handle_history.hh"
#include "src/contact/contact_info.hh"
#include "src/contact/contact_state.hh"
#include "src/contact/contact_state_history.hh"
#include "src/contact/create_contact.hh"
#include "src/contact/list_merge_candidates_request.hh"
#include "src/contact/merge_contacts_request.hh"
#include "src/contact/search_contact.hh"
#include "src/contact/update_contact.hh"
#include "src/contact/update_contact_state.hh"

#include "src/contact_representative/contact_representative_info.hh"
#include "src/contact_representative/create_contact_representative.hh"
#include "src/contact_representative/update_contact_representative.hh"
#include "src/contact_representative/delete_contact_representative.hh"

#include "src/domain/batch_delete_domains_request.hh"
#include "src/domain/domain_data_history.hh"
#include "src/domain/domain_info_request.hh"
#include "src/domain/domain_life_cycle_stage_request.hh"
#include "src/domain/domain_state.hh"
#include "src/domain/domain_state_history.hh"
#include "src/domain/fqdn_history.hh"
#include "src/domain/get_domains_by_contact_request.hh"
#include "src/domain/get_domains_notify_info_request.hh"
#include "src/domain/list_domains_by_contact_request.hh"
#include "src/domain/list_domains_by_keyset_request.hh"
#include "src/domain/list_domains_by_nsset_request.hh"
#include "src/domain/list_domains_request.hh"
#include "src/domain/manage_domain_state_flags_request.hh"
#include "src/domain/search_domain.hh"
#include "src/domain/update_domains_additional_notify_info_request.hh"

#include "src/domain_blacklist/block_info.hh"
#include "src/domain_blacklist/create_block.hh"
#include "src/domain_blacklist/delete_block.hh"

#include "src/keyset/keyset_data_history.hh"
#include "src/keyset/keyset_handle_history.hh"
#include "src/keyset/keyset_info_request.hh"
#include "src/keyset/keyset_state.hh"
#include "src/keyset/keyset_state_history.hh"
#include "src/keyset/keyset_info.hh"
#include "src/keyset/list_keysets_by_contact_request.hh"
#include "src/keyset/search_keyset.hh"

#include "src/nsset/check_dns_host_request.hh"
#include "src/nsset/list_nssets.hh"
#include "src/nsset/list_nssets_by_contact_request.hh"
#include "src/nsset/nsset_data_history.hh"
#include "src/nsset/nsset_handle_history.hh"
#include "src/nsset/nsset_info_request.hh"
#include "src/nsset/nsset_state.hh"
#include "src/nsset/nsset_state_history.hh"
#include "src/nsset/nsset_info.hh"
#include "src/nsset/search_nsset.hh"

#include "src/registrar/add_registrar_certification.hh"
#include "src/registrar/add_registrar_epp_credentials.hh"
#include "src/registrar/add_registrar_group_membership.hh"
#include "src/registrar/add_registrar_zone_access.hh"
#include "src/registrar/create_registrar.hh"
#include "src/registrar/delete_registrar_certification.hh"
#include "src/registrar/delete_registrar_epp_credentials.hh"
#include "src/registrar/delete_registrar_group_membership.hh"
#include "src/registrar/delete_registrar_zone_access.hh"
#include "src/registrar/registrar_certifications.hh"
#include "src/registrar/registrar_credit.hh"
#include "src/registrar/registrar_epp_credentials.hh"
#include "src/registrar/registrar_groups_membership.hh"
#include "src/registrar/registrar_info.hh"
#include "src/registrar/registrar_zone_access.hh"
#include "src/registrar/update_registrar_certification.hh"
#include "src/registrar/update_registrar_epp_credentials.hh"
#include "src/registrar/update_registrar.hh"
#include "src/registrar/update_registrar_zone_access.hh"

#include "fred_api/registry/contact/contact_common_types.pb.h"
#include "fred_api/registry/contact/contact_handle_history_types.pb.h"
#include "fred_api/registry/contact/contact_history_types.pb.h"
#include "fred_api/registry/contact/contact_info_types.pb.h"
#include "fred_api/registry/contact/contact_state_flags_types.pb.h"
#include "fred_api/registry/contact/contact_state_history_types.pb.h"
#include "fred_api/registry/contact/contact_state_types.pb.h"
#include "fred_api/registry/contact/list_merge_candidates_types.pb.h"
#include "fred_api/registry/contact/update_contact_types.pb.h"
#include "fred_api/registry/contact/update_contact_state_types.pb.h"
#include "fred_api/registry/contact/service_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact/service_contact_grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.pb.h"

#include "fred_api/registry/contact_representative/contact_representative_common_types.pb.h"
#include "fred_api/registry/contact_representative/service_contact_representative_grpc.pb.h"

#include "fred_api/registry/domain/domain_common_types.pb.h"
#include "fred_api/registry/domain/domain_history_types.pb.h"
#include "fred_api/registry/domain/domain_info_types.pb.h"
#include "fred_api/registry/domain/domain_state_flags_types.pb.h"
#include "fred_api/registry/domain/domain_state_history_types.pb.h"
#include "fred_api/registry/domain/domain_state_types.pb.h"
#include "fred_api/registry/domain/fqdn_history_types.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.pb.h"

#include "fred_api/registry/domain_blacklist/domain_blacklist_common_types.pb.h"
#include "fred_api/registry/domain_blacklist/service_domain_blacklist_grpc.pb.h"

#include "fred_api/registry/keyset/keyset_common_types.pb.h"
#include "fred_api/registry/keyset/keyset_handle_history_types.pb.h"
#include "fred_api/registry/keyset/keyset_history_types.pb.h"
#include "fred_api/registry/keyset/keyset_info_types.pb.h"
#include "fred_api/registry/keyset/keyset_state_flags_types.pb.h"
#include "fred_api/registry/keyset/keyset_state_history_types.pb.h"
#include "fred_api/registry/keyset/keyset_state_types.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.pb.h"

#include "fred_api/registry/nsset/nsset_common_types.pb.h"
#include "fred_api/registry/nsset/nsset_handle_history_types.pb.h"
#include "fred_api/registry/nsset/nsset_history_types.pb.h"
#include "fred_api/registry/nsset/nsset_info_types.pb.h"
#include "fred_api/registry/nsset/nsset_state_flags_types.pb.h"
#include "fred_api/registry/nsset/nsset_state_history_types.pb.h"
#include "fred_api/registry/nsset/nsset_state_types.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.pb.h"

#include "fred_api/registry/registrar/registrar_certification_types.pb.h"
#include "fred_api/registry/registrar/registrar_credit_types.pb.h"
#include "fred_api/registry/registrar/registrar_epp_credentials_types.pb.h"
#include "fred_api/registry/registrar/registrar_group_types.pb.h"
#include "fred_api/registry/registrar/registrar_info_types.pb.h"
#include "fred_api/registry/registrar/registrar_zone_access_types.pb.h"
#include "fred_api/registry/registrar/service_admin_grpc.pb.h"

namespace Fred {
namespace Registry {

template <typename>
struct ProtoTypeTraits;

template <typename T>
typename ProtoTypeTraits<T>::RegistryType proto_unwrap(const T&);

template <>
struct ProtoTypeTraits<Api::Contact::ContactInfoRequest>
{
    using RegistryType = Contact::ContactInfoRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactIdRequest>
{
    using RegistryType = Contact::ContactIdRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactHistoryRequest>
{
    using RegistryType = Contact::ContactDataHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::CreateContactRequest>
{
    using RegistryType = Contact::CreateContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactHandleHistoryRequest>
{
    using RegistryType = Contact::ContactHandleHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactStateRequest>
{
    using RegistryType = Contact::ContactStateRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactStateHistoryRequest>
{
    using RegistryType = Contact::ContactStateHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ContactAddress>
{
    using RegistryType = Contact::ContactAddress;
};

template <>
struct ProtoTypeTraits<Api::Contact::WarningLetter>
{
    using RegistryType = Contact::WarningLetter;
};

template <>
struct ProtoTypeTraits<Api::AuthInfo>
{
    using RegistryType = AuthInfo;
};

template <>
struct ProtoTypeTraits<Api::Contact::UpdateContactRequest>
{
    using RegistryType = Contact::UpdateContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::UpdateContactStateRequest>
{
    using RegistryType = Contact::UpdateContactStateRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::ListMergeCandidatesRequest>
{
    using RegistryType = Contact::ListMergeCandidatesRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::MergeContactsRequest>
{
    using RegistryType = Contact::MergeContactsRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::SearchContactRequest>
{
    using RegistryType = Contact::SearchContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Contact::SearchContactHistoryRequest>
{
    using RegistryType = Contact::SearchContactHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainHistoryRequest>
{
    using RegistryType = Domain::DomainDataHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::FqdnHistoryRequest>
{
    using RegistryType = Domain::FqdnHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainInfoRequest>
{
    using RegistryType = Domain::DomainInfoRequest;
};

void proto_unwrap_back(const Api::Domain::BatchDomainInfoRequest&, Domain::BatchDomainInfoRequest&);

template <>
struct ProtoTypeTraits<Api::Domain::DomainIdRequest>
{
    using RegistryType = Domain::DomainIdRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainStateRequest>
{
    using RegistryType = Domain::DomainStateRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainStateHistoryRequest>
{
    using RegistryType = Domain::DomainStateHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::SearchDomainRequest>
{
    using RegistryType = Domain::SearchDomainRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::SearchDomainHistoryRequest>
{
    using RegistryType = Domain::SearchDomainHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainsByContactRequest>
{
    using RegistryType = Domain::GetDomainsByContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::DomainLifeCycleStageRequest>
{
    using RegistryType = Domain::DomainLifeCycleStageRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::GetDomainsOutzoneNotifyInfoRequest>
{
    using RegistryType = Domain::GetDomainsNotifyInfoRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::GetDomainsDeleteNotifyInfoRequest>
{
    using RegistryType = Domain::GetDomainsNotifyInfoRequest;
};

void proto_unwrap_back(const Api::Domain::BatchDeleteDomainsRequest& src, Domain::BatchDeleteDomainsRequest& dst);

void proto_unwrap_back(const Api::Domain::ManageDomainStateFlagsRequest& src, Domain::ManageDomainStateFlagsRequest& dst);

void proto_unwrap_back(const Api::Domain::UpdateDomainsDeleteAdditionalNotifyInfoRequest& src, Domain::UpdateDomainsAdditionalNotifyInfoRequest& dst);

void proto_unwrap_back(const Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest& src, Domain::UpdateDomainsAdditionalNotifyInfoRequest& dst);

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsRequest>
{
    using RegistryType = Domain::ListDomainsRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByNssetRequest>
{
    using RegistryType = Domain::ListDomainsByNssetRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByKeysetRequest>
{
    using RegistryType = Domain::ListDomainsByKeysetRequest;
};

template <>
struct ProtoTypeTraits<Api::Domain::ListDomainsByContactRequest>
{
    using RegistryType = Domain::ListDomainsByContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetInfoRequest>
{
    using RegistryType = Keyset::KeysetInfoRequest;
};

void proto_unwrap_back(const Api::Keyset::BatchKeysetInfoRequest&, Keyset::BatchKeysetInfoRequest&);

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetIdRequest>
{
    using RegistryType = Keyset::KeysetIdRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetHistoryRequest>
{
    using RegistryType = Keyset::KeysetDataHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetHandleHistoryRequest>
{
    using RegistryType = Keyset::KeysetHandleHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::ListKeysetsByContactRequest>
{
    using RegistryType = Keyset::ListKeysetsByContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetStateRequest>
{
    using RegistryType = Keyset::KeysetStateRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::KeysetStateHistoryRequest>
{
    using RegistryType = Keyset::KeysetStateHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::SearchKeysetRequest>
{
    using RegistryType = Keyset::SearchKeysetRequest;
};

template <>
struct ProtoTypeTraits<Api::Keyset::SearchKeysetHistoryRequest>
{
    using RegistryType = Keyset::SearchKeysetHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::ListNssetsRequest>
{
    using RegistryType = Nsset::ListNssetsRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::ListNssetsByContactRequest>
{
    using RegistryType = Nsset::ListNssetsByContactRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetInfoRequest>
{
    using RegistryType = Nsset::NssetInfoRequest;
};

void proto_unwrap_back(const Api::Nsset::BatchNssetInfoRequest& src, Nsset::BatchNssetInfoRequest& dst);

template <>
struct ProtoTypeTraits<Api::Nsset::NssetIdRequest>
{
    using RegistryType = Nsset::NssetIdRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetHistoryRequest>
{
    using RegistryType = Nsset::NssetDataHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetHandleHistoryRequest>
{
    using RegistryType = Nsset::NssetHandleHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetStateRequest>
{
    using RegistryType = Nsset::NssetStateRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::NssetStateHistoryRequest>
{
    using RegistryType = Nsset::NssetStateHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::CheckDnsHostRequest>
{
    using RegistryType = Nsset::CheckDnsHostRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::SearchNssetRequest>
{
    using RegistryType = Nsset::SearchNssetRequest;
};

template <>
struct ProtoTypeTraits<Api::Nsset::SearchNssetHistoryRequest>
{
    using RegistryType = Nsset::SearchNssetHistoryRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::RegistrarInfoRequest>
{
    using RegistryType = Registrar::RegistrarInfoRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::RegistrarCreditRequest>
{
    using RegistryType = Registrar::RegistrarCreditRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::RegistrarZoneAccessRequest>
{
    using RegistryType = Registrar::RegistrarZoneAccessRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::CreateRegistrarRequest>
{
    using RegistryType = Registrar::CreateRegistrarRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::UpdateRegistrarRequest>
{
    using RegistryType = Registrar::UpdateRegistrarRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::AddRegistrarZoneAccessRequest>
{
    using RegistryType = Registrar::AddRegistrarZoneAccessRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::DeleteRegistrarZoneAccessRequest>
{
    using RegistryType = Registrar::DeleteRegistrarZoneAccessRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::UpdateRegistrarZoneAccessRequest>
{
    using RegistryType = Registrar::UpdateRegistrarZoneAccessRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::GetRegistrarEppCredentialsRequest>
{
    using RegistryType = Registrar::GetRegistrarEppCredentialsRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::AddRegistrarEppCredentialsRequest>
{
    using RegistryType = Registrar::AddRegistrarEppCredentialsRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::DeleteRegistrarEppCredentialsRequest>
{
    using RegistryType = Registrar::DeleteRegistrarEppCredentialsRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::UpdateRegistrarEppCredentialsRequest>
{
    using RegistryType = Registrar::UpdateRegistrarEppCredentialsRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::GetRegistrarGroupsMembershipRequest>
{
    using RegistryType = Registrar::GetRegistrarGroupsMembershipRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::AddRegistrarGroupMembershipRequest>
{
    using RegistryType = Registrar::AddRegistrarGroupMembershipRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::DeleteRegistrarGroupMembershipRequest>
{
    using RegistryType = Registrar::DeleteRegistrarGroupMembershipRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::GetRegistrarCertificationsRequest>
{
    using RegistryType = Registrar::GetRegistrarCertificationsRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::AddRegistrarCertificationRequest>
{
    using RegistryType = Registrar::AddRegistrarCertificationRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::DeleteRegistrarCertificationRequest>
{
    using RegistryType = Registrar::DeleteRegistrarCertificationRequest;
};

template <>
struct ProtoTypeTraits<Api::Registrar::UpdateRegistrarCertificationRequest>
{
    using RegistryType = Registrar::UpdateRegistrarCertificationRequest;
};

template <>
struct ProtoTypeTraits<Api::ContactRepresentative::ContactRepresentativeInfoRequest>
{
    using RegistryType = ContactRepresentative::ContactRepresentativeInfoRequest;
};

template <>
struct ProtoTypeTraits<Api::ContactRepresentative::CreateContactRepresentativeRequest>
{
    using RegistryType = ContactRepresentative::CreateContactRepresentativeRequest;
};

template <>
struct ProtoTypeTraits<Api::ContactRepresentative::UpdateContactRepresentativeRequest>
{
    using RegistryType = ContactRepresentative::UpdateContactRepresentativeRequest;
};

template <>
struct ProtoTypeTraits<Api::ContactRepresentative::DeleteContactRepresentativeRequest>
{
    using RegistryType = ContactRepresentative::DeleteContactRepresentativeRequest;
};

template <>
struct ProtoTypeTraits<Api::DomainBlacklist::BlockInfoRequest>
{
    using RegistryType = DomainBlacklist::BlockInfoRequest;
};

template <>
struct ProtoTypeTraits<Api::DomainBlacklist::CreateBlockRequest>
{
    using RegistryType = DomainBlacklist::CreateBlockRequest;
};

template <>
struct ProtoTypeTraits<Api::DomainBlacklist::DeleteBlockRequest>
{
    using RegistryType = DomainBlacklist::DeleteBlockRequest;
};

}//namespace Fred::Registry
}//namespace Fred

#endif//PROTO_UNWRAP_HH_1E3622FA61F8CFDE5B96210B52BDD131
