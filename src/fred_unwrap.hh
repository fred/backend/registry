/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FRED_UNWRAP_HH_431CD4E1851EC67F979C982AA02DEC31//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define FRED_UNWRAP_HH_431CD4E1851EC67F979C982AA02DEC31

#include "src/contact/contact_data_history.hh"
#include "src/contact/contact_handle_history.hh"
#include "src/contact/contact_state.hh"
#include "src/contact/contact_state_history.hh"
#include "src/contact/contact_info.hh"

#include "src/domain/domain_data_history.hh"
#include "src/domain/domain_info_reply.hh"
#include "src/domain/domain_state.hh"
#include "src/domain/domain_state_history.hh"
#include "src/domain/fqdn_history.hh"

#include "src/keyset/keyset_data_history.hh"
#include "src/keyset/keyset_info_reply.hh"
#include "src/keyset/keyset_handle_history.hh"
#include "src/keyset/keyset_state.hh"
#include "src/keyset/keyset_state_history.hh"
#include "src/keyset/keyset_info.hh"

#include "src/nsset/nsset_data_history.hh"
#include "src/nsset/nsset_info_reply.hh"
#include "src/nsset/nsset_handle_history.hh"
#include "src/nsset/nsset_state.hh"
#include "src/nsset/nsset_state_history.hh"
#include "src/nsset/nsset_info.hh"

#include "src/registrar/registrar_credit.hh"
#include "src/registrar/registrar_epp_credentials.hh"
#include "src/registrar/registrar_info.hh"

#include "libfred/registrable_object/contact/contact_data_history.hh"
#include "libfred/registrable_object/contact/contact_handle_history.hh"
#include "libfred/registrable_object/contact/contact_state.hh"
#include "libfred/registrable_object/contact/contact_state_history.hh"
#include "libfred/registrable_object/contact/info_contact_output.hh"

#include "libfred/registrable_object/domain/domain_data_history.hh"
#include "libfred/registrable_object/domain/domain_state.hh"
#include "libfred/registrable_object/domain/domain_state_history.hh"
#include "libfred/registrable_object/domain/fqdn_history.hh"
#include "libfred/registrable_object/domain/info_domain_output.hh"

#include "libfred/registrable_object/keyset/info_keyset_output.hh"
#include "libfred/registrable_object/keyset/keyset_data_history.hh"
#include "libfred/registrable_object/keyset/keyset_handle_history.hh"
#include "libfred/registrable_object/keyset/keyset_state.hh"
#include "libfred/registrable_object/keyset/keyset_state_history.hh"

#include "libfred/registrable_object/nsset/info_nsset_output.hh"
#include "libfred/registrable_object/nsset/nsset_data_history.hh"
#include "libfred/registrable_object/nsset/nsset_handle_history.hh"
#include "libfred/registrable_object/nsset/nsset_state.hh"
#include "libfred/registrable_object/nsset/nsset_state_history.hh"

#include "libfred/registrar/epp_auth/get_registrar_epp_auth.hh"
#include "libfred/registrar/info_registrar_output.hh"
#include "libfred/registrar/registrar_zone_credit.hh"

#include <boost/optional.hpp>

#include <exception>
#include <map>
#include <string>

namespace Fred {
namespace Registry {

template <typename>
struct LibFredTypeTraits;

template <typename T>
typename LibFredTypeTraits<T>::RegistryType fred_unwrap(const T&);

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Contact::ContactDataHistory>
{
    using RegistryType = Contact::ContactDataHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Contact::ContactHandleHistory>
{
    using RegistryType = Contact::ContactHandleHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Contact::ContactState>
{
    using RegistryType = Contact::ContactState;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Contact::ContactStateHistory>
{
    using RegistryType = Contact::ContactStateHistory;
};

template <>
struct LibFredTypeTraits<LibFred::InfoContactOutput>
{
    using RegistryType = Contact::ContactInfoReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::InfoContactData>
{
    using RegistryType = Object::ObjectEvents;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Domain::DomainDataHistory>
{
    using RegistryType = Domain::DomainDataHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Domain::FqdnHistory>
{
    using RegistryType = Domain::FqdnHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Domain::DomainState>
{
    using RegistryType = Domain::DomainState;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Domain::DomainStateHistory>
{
    using RegistryType = Domain::DomainStateHistory;
};

Domain::DomainInfoReply::Data fred_unwrap(const LibFred::OperationContext& ctx, const LibFred::InfoDomainOutput& src);

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Keyset::KeysetDataHistory>
{
    using RegistryType = Keyset::KeysetDataHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Keyset::KeysetHandleHistory>
{
    using RegistryType = Keyset::KeysetHandleHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Keyset::KeysetState>
{
    using RegistryType = Keyset::KeysetState;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Keyset::KeysetStateHistory>
{
    using RegistryType = Keyset::KeysetStateHistory;
};

template <>
struct LibFredTypeTraits<LibFred::InfoKeysetOutput>
{
    using RegistryType = Keyset::KeysetInfoReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Nsset::NssetDataHistory>
{
    using RegistryType = Nsset::NssetDataHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Nsset::NssetHandleHistory>
{
    using RegistryType = Nsset::NssetHandleHistoryReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Nsset::NssetState>
{
    using RegistryType = Nsset::NssetState;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrableObject::Nsset::NssetStateHistory>
{
    using RegistryType = Nsset::NssetStateHistory;
};

template <>
struct LibFredTypeTraits<LibFred::InfoNssetOutput>
{
    using RegistryType = Nsset::NssetInfoReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::InfoRegistrarOutput>
{
    using RegistryType = Registrar::RegistrarInfoReply::Data;
};

template <>
struct LibFredTypeTraits<LibFred::RegistrarZoneCredit>
{
    using RegistryType = std::map<std::string, boost::optional<Registrar::Credit>>;
};

template <>
struct LibFredTypeTraits<LibFred::Registrar::EppAuth::EppAuthRecordUuid>
{
    using RegistryType = Registrar::RegistrarEppCredentialsId;
};

template <>
struct LibFredTypeTraits<LibFred::Registrar::EppAuth::EppAuthRecord>
{
    using RegistryType = Registrar::GetRegistrarEppCredentialsReply::Data::Credentials;
};

}//namespace Fred::Registry
}//namespace Fred

#endif//FRED_UNWRAP_HH_431CD4E1851EC67F979C982AA02DEC31
