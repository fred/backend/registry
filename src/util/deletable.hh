/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETABLE_HH_9947E96A5C20E7D10C33D5C92553930A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DELETABLE_HH_9947E96A5C20E7D10C33D5C92553930A

#include "src/util/strong_type.hh"

#include <boost/optional.hpp>

#include <string>
#include <utility>

namespace Fred {
namespace Registry {
namespace Util {

template <typename T>
using Deletable = StrongType<boost::optional<T>, struct DeletableTag_>;

template <typename T>
constexpr decltype(auto) to_set(T&& value)
{
    return Fred::Registry::Util::make_strong<Deletable<std::decay_t<T>>>(boost::optional<std::decay_t<T>>{std::forward<T>(value)});
}

template <typename T>
constexpr decltype(auto) to_delete()
{
    return Fred::Registry::Util::make_strong<Deletable<std::decay_t<T>>>(boost::optional<std::decay_t<T>>{});
}

template <typename T, typename OperationDelete, typename OperationSet>
void select_operation(
        const boost::optional<Deletable<T>>& operation,
        OperationDelete&& on_delete,
        OperationSet&& on_set)
{
    if (operation != boost::none)
    {
        const boost::optional<T>& value_to_set = get_raw_value_from(*operation);
        if (value_to_set == boost::none)
        {
            std::forward<OperationDelete>(on_delete)();
        }
        else
        {
            std::forward<OperationSet>(on_set)(*value_to_set);
        }
    }
}

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//DELETABLE_HH_9947E96A5C20E7D10C33D5C92553930A
