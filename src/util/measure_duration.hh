/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MEASURE_DURATION_HH_35FDAD36AD510F3AF02DCC4AD81525EC//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define MEASURE_DURATION_HH_35FDAD36AD510F3AF02DCC4AD81525EC

#include <chrono>
#include <type_traits>

namespace Fred {
namespace Registry {
namespace Util {

template <typename T, typename P = std::chrono::milliseconds>
class MeasureDuration
{
public:
    using Time = typename P::rep;
    ~MeasureDuration()
    {
        try
        {
            const auto t1 = std::chrono::steady_clock().now();
            static_assert(std::is_same<Time, decltype(std::declval<P>().count())>::value, "unexpected type");
            T::measure_completed(std::chrono::duration_cast<P>(t1 - t0_).count());
        }
        catch (...) { }
    }
private:
    const decltype(std::chrono::steady_clock().now()) t0_ = std::chrono::steady_clock().now();
};

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//MEASURE_DURATION_HH_35FDAD36AD510F3AF02DCC4AD81525EC
