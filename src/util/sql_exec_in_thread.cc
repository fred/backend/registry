/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/sql_exec_in_thread.hh"

#include "src/util/measure_duration.hh"

#include "util/log/log.hh"

#include <boost/variant.hpp>

#include <atomic>
#include <chrono>
#include <exception>
#include <thread>
#include <utility>

namespace Fred {
namespace Registry {
namespace Util {

namespace {

class AutocommitedTransaction
{
public:
    explicit AutocommitedTransaction(
            const std::unique_ptr<Database::StandaloneConnection>& conn,
            const Transaction& tx)
        : conn_{conn},
          tx_{tx}
    {
        if (tx_.has_start_cmd())
        {
            conn_->exec(tx_.get_start_cmd());
        }
    }
    ~AutocommitedTransaction()
    {
        if (tx_.has_start_cmd() && !std::uncaught_exception())
        {
            try
            {
                conn_->exec("COMMIT");
            }
            catch (...) { }
        }
    }
private:
    const std::unique_ptr<Database::StandaloneConnection>& conn_;
    const Transaction& tx_;
};

}//namespace Fred::Registry::Util::{anonymous}

Transaction::Transaction(std::string start_cmd)
    : start_cmd_{std::move(start_cmd)}
{ }

bool Transaction::has_start_cmd() const noexcept
{
    return !start_cmd_.empty();
}

const std::string& Transaction::get_start_cmd() const
{
    if (has_start_cmd())
    {
        return start_cmd_;
    }
    struct MissingStartCmd : std::exception
    {
        const char* what() const noexcept override { return "missing command for transaction start"; }
    };
    throw MissingStartCmd{};
}

class SqlInThreadSolver::Solver
{
public:
    Solver(const std::string& sql, Transaction tx)
        : duration_ms_{},
          tx_{std::move(tx)},
          thread_{[this, sql]() { this->exec(sql); }}
    { }
    Solver(const std::string& sql, const Database::QueryParams& params, Transaction tx)
        : duration_ms_{},
          tx_{std::move(tx)},
          thread_{[this, sql, params]() { this->exec(sql, params); }}
    { }
    const Database::Result& join()
    {
        thread_.join();
        GetDatabaseResult visitor;
        return result_.apply_visitor(visitor);
    }
    void wait() noexcept
    {
        if (thread_.joinable())
        {
            thread_.join();
        }
    }
    int get_duration_in_ms() const
    {
        return duration_ms_;
    }
private:
    template <typename T>
    void solve(T callable)
    {
        auto t0 = std::chrono::steady_clock().now();
        try
        {
            const auto conn = Database::get_default_manager<Database::StandaloneConnectionFactory>().acquire();
            const AutocommitedTransaction commit_on_exit{conn, tx_};
            t0 = std::chrono::steady_clock().now();
            result_ = callable(*conn);
        }
        catch (const Database::ResultFailed& e)
        {
            result_ = e;
        }
        catch (const Database::Exception& e)
        {
            result_ = e;
        }
        catch (const std::runtime_error& e)
        {
            result_ = e;
        }
        catch (const std::exception& e)
        {
            result_ = std::runtime_error(e.what());
        }
        catch (...)
        {
            result_ = std::runtime_error("unexpected exception caught");
        }
        const auto t1 = std::chrono::steady_clock().now();
        duration_ms_ = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count();
        FREDLOG_DEBUG("exec done in " + std::to_string(duration_ms_) + "ms");
    }
    void exec(const std::string& sql)
    {
        this->solve([&](Database::StandaloneConnection& conn) { return conn.exec(sql); });
    }
    void exec(const std::string& sql, const Database::QueryParams& params)
    {
        this->solve([&](Database::StandaloneConnection& conn) { return conn.exec_params(sql, params); });
    }
    using ExecResult = boost::variant<Database::Result,
                                      Database::ResultFailed,
                                      Database::Exception,
                                      std::runtime_error>;
    struct GetDatabaseResult : boost::static_visitor<const Database::Result&>
    {
        const Database::Result& operator()(const Database::Result& result) const
        {
            return result;
        }
        template <typename E>
        const Database::Result& operator()(const E& e) const
        {
            throw e;
        }
    };
    ExecResult result_;
    int duration_ms_;
    Transaction tx_;
    std::thread thread_;
};

SqlInThreadSolver::SqlInThreadSolver()
    : running_task_()
{
}

SqlInThreadSolver::SqlInThreadSolver(SqlInThreadSolver&& src)
    : running_task_(std::move(src.running_task_))
{
}

SqlInThreadSolver::SqlInThreadSolver(const std::string& sql, Transaction tx)
    : running_task_(std::make_unique<Solver>(sql, std::move(tx)))
{
}

SqlInThreadSolver::SqlInThreadSolver(const std::string& sql, const Database::QueryParams& params, Transaction tx)
    : running_task_(std::make_unique<Solver>(sql, params, std::move(tx)))
{
}

SqlInThreadSolver::~SqlInThreadSolver()
{
    if (running_task_ != nullptr)
    {
        running_task_->wait();
    }
}

SqlInThreadSolver& SqlInThreadSolver::operator=(SqlInThreadSolver&& src)
{
    std::swap(running_task_, src.running_task_);
    return *this;
}

bool SqlInThreadSolver::is_joinable() const noexcept
{
    return running_task_ != nullptr;
}

SqlInThreadSolver::Result::Result()
    : result_(),
      duration_ms_()
{
}

SqlInThreadSolver::Result::Result(const Result& src)
    : result_(src.result_),
      duration_ms_(src.duration_ms_)
{
}

SqlInThreadSolver::Result::Result(SqlInThreadSolver&& solver)
    : result_(solver.running_task_->join()),
      duration_ms_(solver.running_task_->get_duration_in_ms())
{
    solver.running_task_.reset(nullptr);
}

SqlInThreadSolver::Result::~Result()
{
}

SqlInThreadSolver::Result& SqlInThreadSolver::Result::operator=(const Result& src)
{
    result_ = src.result_;
    duration_ms_ = src.duration_ms_;
    return *this;
}

SqlInThreadSolver::Result& SqlInThreadSolver::Result::operator=(SqlInThreadSolver&& solver)
{
    result_ = solver.running_task_->join();
    duration_ms_ = solver.running_task_->get_duration_in_ms();
    solver.running_task_.reset(nullptr);
    return *this;
}

const Database::Result& SqlInThreadSolver::Result::get_result() const
{
    return result_;
}

int SqlInThreadSolver::Result::get_duration_in_ms() const
{
    return duration_ms_;
}

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Util;

Transaction Fred::Registry::Util::no_transaction()
{
    return Transaction{};
}

Transaction Fred::Registry::Util::repeatable_read_read_only_transaction()
{
    return Transaction{"START TRANSACTION ISOLATION LEVEL REPEATABLE READ READ ONLY"};
}

Transaction Fred::Registry::Util::serializable_read_only_deferrable_transaction()
{
    return Transaction{"START TRANSACTION ISOLATION LEVEL SERIALIZABLE READ ONLY DEFERRABLE"};
}

SqlInThreadSolver Fred::Registry::Util::sql_exec_in_thread(const std::string& sql, Transaction tx)
{
    return SqlInThreadSolver(sql, std::move(tx));
}

SqlInThreadSolver Fred::Registry::Util::sql_exec_in_thread(
        const std::string& sql,
        const Database::QueryParams& params,
        Transaction tx)
{
    return SqlInThreadSolver(sql, params, std::move(tx));
}
