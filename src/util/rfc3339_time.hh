/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RFC3339_HH_26AC5953D159FDE76125CD62AD8DA7C2//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define RFC3339_HH_26AC5953D159FDE76125CD62AD8DA7C2

#include <chrono>
#include <exception>
#include <string>

namespace Fred {
namespace Registry {
namespace Util {

using TimePointSeconds = std::chrono::time_point<std::chrono::system_clock, std::chrono::seconds>;

//YYYY-MM-DDThh:mm:ss[.f*][Z|+oo:oo|-oo:oo]
TimePointSeconds seconds_from_rfc3339_formated_string(const std::string& rfc3339_time);

struct NotRfc3339Compliant : std::exception
{
    const char* what() const noexcept override = 0;
};

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//RFC3339_HH_26AC5953D159FDE76125CD62AD8DA7C2
