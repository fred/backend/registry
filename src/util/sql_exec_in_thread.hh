/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SQL_EXEC_IN_THREAD_HH_B16115CD0A9DEF575E13BC69436A8CFA//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define SQL_EXEC_IN_THREAD_HH_B16115CD0A9DEF575E13BC69436A8CFA

#include "libfred/db_settings.hh"

#include "util/db/query_param.hh"

#include <memory>
#include <string>

namespace Fred {
namespace Registry {
namespace Util {

class Transaction
{
public:
    bool has_start_cmd() const noexcept;
    const std::string& get_start_cmd() const;
private:
    Transaction(std::string start_cmd = {});
    std::string start_cmd_;
    friend Transaction no_transaction();
    friend Transaction repeatable_read_read_only_transaction();
    friend Transaction serializable_read_only_deferrable_transaction();
};

class SqlInThreadSolver
{
public:
    class Result
    {
    public:
        Result();
        Result(const Result&);
        explicit Result(SqlInThreadSolver&& solver);
        ~Result();
        Result& operator=(const Result&);
        Result& operator=(SqlInThreadSolver&& solver);
        const Database::Result& get_result() const;
        int get_duration_in_ms() const;
    private:
        Database::Result result_;
        int duration_ms_;
    };
    SqlInThreadSolver();
    SqlInThreadSolver(const SqlInThreadSolver&) = delete;
    SqlInThreadSolver(SqlInThreadSolver&&);
    ~SqlInThreadSolver();
    SqlInThreadSolver& operator=(const SqlInThreadSolver&) = delete;
    SqlInThreadSolver& operator=(SqlInThreadSolver&&);
    bool is_joinable() const noexcept;
private:
    SqlInThreadSolver(const std::string& sql, Transaction);
    SqlInThreadSolver(const std::string& sql, const Database::QueryParams& params, Transaction);
    class Solver;
    std::unique_ptr<Solver> running_task_;
    friend SqlInThreadSolver sql_exec_in_thread(const std::string&, Transaction);
    friend SqlInThreadSolver sql_exec_in_thread(const std::string&, const Database::QueryParams&, Transaction);
};

Transaction no_transaction();
Transaction repeatable_read_read_only_transaction();
Transaction serializable_read_only_deferrable_transaction();

SqlInThreadSolver sql_exec_in_thread(const std::string& sql, Transaction tx);
SqlInThreadSolver sql_exec_in_thread(
        const std::string& sql,
        const Database::QueryParams& params,
        Transaction tx);

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//SQL_EXEC_IN_THREAD_HH_B16115CD0A9DEF575E13BC69436A8CFA
