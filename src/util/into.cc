/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/into.hh"

#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <cstring>
#include <ctime>
#include <iomanip>
#include <sstream>

namespace Fred {
namespace Registry {
namespace Util {

std::string Into<std::string>::from(const std::string& src)
{
    return "'" + src + "'";
}

std::string Into<std::string>::from(std::int32_t src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(std::int64_t src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(std::uint32_t src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(std::uint64_t src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(unsigned long long src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(float src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(bool src)
{
    return std::to_string(src);
}

std::string Into<std::string>::from(const boost::uuids::uuid& src)
{
    return boost::lexical_cast<std::string>(src);
}

std::string Into<std::string>::from(const Contact::ContactAdditionalIdentifier& src)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const Contact::NationalIdentityNumber& src) const
        {
            return "national identity number: " + Into<std::string>::from(src);
        }
        std::string operator()(const Contact::NationalIdentityCard& src) const
        {
            return "national identity card: " + Into<std::string>::from(src);
        }
        std::string operator()(const Contact::PassportNumber& src) const
        {
            return "passport number: " + Into<std::string>::from(src);
        }
        std::string operator()(const CompanyRegistrationNumber& src) const
        {
            return "company registration number: " + Into<std::string>::from(src);
        }
        std::string operator()(const Contact::SocialSecurityNumber& src) const
        {
            return "social security number: " + Into<std::string>::from(src);
        }
        std::string operator()(const Contact::Birthdate& src) const
        {
            return "birthdate: " + Into<std::string>::from(src);
        }
    };
    return boost::apply_visitor(Visitor(), src);
}

std::string Into<std::string>::from(const Contact::ContactHistoryInterval::Limit& src)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const Contact::ContactHistoryInterval::TimePoint& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Contact::ContactHistoryInterval::HistoryId& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Contact::ContactHistoryInterval::NoLimit&) const
        {
            return "no limit";
        }
    };
    return boost::apply_visitor(Visitor(), src);
}

std::string Into<std::string>::from(const ContactRepresentative::ContactRepresentativeRef& src)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const ContactRepresentative::ContactRepresentativeId& src) const
        {
            return "contact representative id: " + Into<std::string>::from(src);
        }
        std::string operator()(const ContactRepresentative::ContactRepresentativeHistoryId& src) const
        {
            return "contact representative history id: " + Into<std::string>::from(src);
        }
        std::string operator()(const ContactRepresentative::ContactId& src) const
        {
            return "contact id: " + Into<std::string>::from(src);
        }
    };
    return boost::apply_visitor(Visitor(), src);
}

std::string Into<std::string>::from(const Domain::DomainHistoryInterval::Limit& src)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const Domain::DomainHistoryInterval::TimePoint& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Domain::DomainHistoryInterval::HistoryId& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Domain::DomainHistoryInterval::NoLimit&) const
        {
            return "no limit";
        }
    };
    return boost::apply_visitor(Visitor(), src);
}

std::string Into<std::string>::from(const Keyset::KeysetHistoryInterval::Limit& src)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const Keyset::KeysetHistoryInterval::TimePoint& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Keyset::KeysetHistoryInterval::HistoryId& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Keyset::KeysetHistoryInterval::NoLimit&) const
        {
            return "no limit";
        }
    };
    return boost::apply_visitor(Visitor(), src);
}

std::string Into<std::string>::from(const Nsset::NssetHistoryInterval::Limit& src)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const Nsset::NssetHistoryInterval::TimePoint& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Nsset::NssetHistoryInterval::HistoryId& src) const
        {
            return Into<std::string>::from(src);
        }
        std::string operator()(const Nsset::NssetHistoryInterval::NoLimit&) const
        {
            return "no limit";
        }
    };
    return boost::apply_visitor(Visitor(), src);
}

namespace {

template <typename> struct BoostTimeResolutionTraits;

template <>
struct BoostTimeResolutionTraits<boost::date_time::milli_res>
{
    using StdChronoResolution = std::chrono::milliseconds;
};

template <>
struct BoostTimeResolutionTraits<boost::date_time::micro_res>
{
    using StdChronoResolution = std::chrono::microseconds;
};

template <>
struct BoostTimeResolutionTraits<boost::date_time::nano_res>
{
    using StdChronoResolution = std::chrono::nanoseconds;
};

template <typename> struct Log10;

template <std::intmax_t Denom>
struct Log10<std::ratio<1, Denom>>
{
    static constexpr auto value = Log10<std::ratio<1, Denom / 10>>::value - 1;
    static_assert((Denom % 10) == 0, "Denom must be 10^x");
};

template <>
struct Log10<std::ratio<1, 1>>
{
    static constexpr auto value = 0;
};

}//namespace Fred::Registry::Util::{anonymous}

template <typename Clock, typename Resolution>
std::string Into<std::string>::from(const std::chrono::time_point<Clock, Resolution>& src)
{
    using Period = typename Resolution::period;
    const auto counts_from_epoch = src.time_since_epoch().count();
    const auto seconds_part = (counts_from_epoch * Period::num) / Period::den;
    std::tm parsed_date;
    if (::gmtime_r(&seconds_part, &parsed_date) == nullptr)//std::gmtime may not be thread-safe
    {
        const auto c_errno = errno;
        return std::string("gmtime_r() failed: ") + std::strerror(c_errno);
    }
    std::ostringstream out;
    out << (1900 + parsed_date.tm_year) << "-"
        << std::setw(2) << std::setfill('0') << (parsed_date.tm_mon + 1) << "-"
        << std::setw(2) << std::setfill('0') << parsed_date.tm_mday << " "
        << std::setw(2) << std::setfill('0') << parsed_date.tm_hour << ":"
        << std::setw(2) << std::setfill('0') << parsed_date.tm_min << ":"
        << std::setw(2) << std::setfill('0') << parsed_date.tm_sec;
    static constexpr bool has_frac_part = 1 < Period::den;
    if (has_frac_part)
    {
        const auto frac_part = (counts_from_epoch * Period::num) % Period::den;
        out << "." << std::setw(-Log10<Period>::value) << std::setfill('0') << frac_part;
    }
    return out.str();
}

template std::string Into<std::string>::from(const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>&);
template std::string Into<std::string>::from(const std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>&);
template std::string Into<std::string>::from(const std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>&);

template <typename Clock, typename Resolution>
std::chrono::time_point<Clock, Resolution> Into<std::chrono::time_point<Clock, Resolution>>::from(const std::string& yyyy_mm_dd_hh_mm_ss_usec)
{
    const auto time_since_epoch = boost::posix_time::time_from_string(yyyy_mm_dd_hh_mm_ss_usec) - boost::posix_time::from_time_t(0);
    using FractionalSeconds = typename BoostTimeResolutionTraits<typename boost::posix_time::time_duration::traits_type>::StdChronoResolution;
    return Result{std::chrono::seconds(time_since_epoch.total_seconds())} +
           std::chrono::duration_cast<Resolution>(FractionalSeconds(time_since_epoch.fractional_seconds()));
}

template <typename Clock, typename Resolution>
std::chrono::time_point<Clock, Resolution> Into<std::chrono::time_point<Clock, Resolution>>::from(const boost::gregorian::date& date)
{
    const auto time_since_epoch = boost::posix_time::ptime{date} - boost::posix_time::from_time_t(0);
    using FractionalSeconds = typename BoostTimeResolutionTraits<typename boost::posix_time::time_duration::traits_type>::StdChronoResolution;
    return Result{std::chrono::seconds(time_since_epoch.total_seconds())} +
           std::chrono::duration_cast<Resolution>(FractionalSeconds(time_since_epoch.fractional_seconds()));
}

template <typename Clock, typename Resolution>
boost::gregorian::date Into<boost::gregorian::date>::from(const std::chrono::time_point<Clock, Resolution>& src)
{
    const auto seconds_from_epoch = std::chrono::duration_cast<std::chrono::seconds>(src.time_since_epoch());
    const auto time = boost::posix_time::from_time_t(seconds_from_epoch.count());
    return time.date();
}

template std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>
Into<std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>>::from(const std::string&);
template std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>
Into<std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>>::from(const std::string&);
template std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>
Into<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>>::from(const std::string&);

template std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>
Into<std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>>::from(const boost::gregorian::date&);
template std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>
Into<std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>>::from(const boost::gregorian::date&);
template std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>
Into<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>>::from(const boost::gregorian::date&);

template boost::gregorian::date
Into<boost::gregorian::date>::from(const std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>&);
template boost::gregorian::date
Into<boost::gregorian::date>::from(const std::chrono::time_point<std::chrono::system_clock, std::chrono::microseconds>&);
template boost::gregorian::date
Into<boost::gregorian::date>::from(const std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>&);

std::string Into<std::string>::from(Contact::ContactItem src)
{
    switch (src)
    {
        case Contact::ContactItem::contact_handle:
            return "contact_handle";
        case Contact::ContactItem::name:
            return "name";
        case Contact::ContactItem::organization:
            return "organization";
        case Contact::ContactItem::place_street:
            return "place.street";
        case Contact::ContactItem::place_city:
            return "place.city";
        case Contact::ContactItem::place_state_or_province:
            return "place.state_or_province";
        case Contact::ContactItem::place_postal_code:
            return "place.postal_code";
        case Contact::ContactItem::telephone:
            return "telephone";
        case Contact::ContactItem::fax:
            return "fax";
        case Contact::ContactItem::email:
            return "email";
        case Contact::ContactItem::notify_email:
            return "notify_email";
        case Contact::ContactItem::vat_identification_number:
            return "vat_identification_number";
        case Contact::ContactItem::additional_identifier:
            return "additional_identifier";
        case Contact::ContactItem::address_company:
            return "address.company";
        case Contact::ContactItem::address_street:
            return "address.street";
        case Contact::ContactItem::address_city:
            return "address.city";
        case Contact::ContactItem::address_state_or_province:
            return "address.state_or_province";
        case Contact::ContactItem::address_postal_code:
            return "address.postal_code";
    }
    throw std::runtime_error("unknown contact item");
}

std::string Into<std::string>::from(const Contact::Privacy& src)
{
    return src.is_publicly_available ? "public"
                                     : "private";
}

std::string Into<std::string>::from(Domain::DomainItem src)
{
    switch (src)
    {
        case Domain::DomainItem::fqdn:
            return "fqdn";
    }
    throw std::runtime_error("unknown domain item");
}

std::string Into<std::string>::from(Keyset::KeysetItem src)
{
    switch (src)
    {
        case Keyset::KeysetItem::keyset_handle:
            return "keyset_handle";
        case Keyset::KeysetItem::public_key:
            return "dns_keys.key";
    }
    throw std::runtime_error("unknown keyset item");
}

std::string Into<std::string>::from(Nsset::NssetItem src)
{
    switch (src)
    {
        case Nsset::NssetItem::nsset_handle:
            return "nsset_handle";
        case Nsset::NssetItem::dns_hosts_fqdn:
            return "dns_hosts.fqdn";
        case Nsset::NssetItem::dns_hosts_ip_addresses:
            return "dns_hosts.ip_addresses";
    }
    throw std::runtime_error("unknown nsset item");
}

std::string Into<std::string>::from(const Domain::TimeInterval& src)
{
    return "{" + Into<std::string>::from(src.lower_bound) + ", " + Into<std::string>::from(src.upper_bound) + "}";
}

std::string Into<std::string>::from(Domain::LifecycleEvent src)
{
    switch (src)
    {
        case Domain::LifecycleEvent::domain_delete :
            return "domain_delete";
        case Domain::LifecycleEvent::domain_outzone:
            return "domain_outzone";
    }
    throw std::runtime_error("unknown LifecycleEvent");
}

std::string Into<std::string>::from(const Domain::DomainContactRole& src)
{
    switch (src)
    {
        case Domain::DomainContactRole::unspecified:
            return "unspecified";
        case Domain::DomainContactRole::registrant: // holder
            return "registrant";
        case Domain::DomainContactRole::administrative:
            return "administrative";
        case Domain::DomainContactRole::keyset_technical:
            return "keyset_technical";
        case Domain::DomainContactRole::nsset_technical:
            return "nsset_technical";
    }
    throw std::runtime_error{"unknown DomainContactRole item"};
}

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred
