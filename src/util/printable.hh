/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef PRINTABLE_HH_52E08B1E9C2B67523346D062BC36BE12//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PRINTABLE_HH_52E08B1E9C2B67523346D062BC36BE12

#include "util/printable.hh"

namespace Fred {
namespace Registry {
namespace Util {

template <typename T>
using Printable = ::Util::Printable<T>;

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//PRINTABLE_HH_52E08B1E9C2B67523346D062BC36BE12
