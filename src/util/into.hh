/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INTO_HH_641B328DC7FFA550662C9E152729B983//date "+%s.%N"|md5sum|tr "[a-F]" "[A-F]"
#define INTO_HH_641B328DC7FFA550662C9E152729B983

#include "src/uuid.hh"
#include "src/contact/contact_state_history.hh"
#include "src/contact/privacy_controlled.hh"
#include "src/contact/search_contact.hh"
#include "src/contact/update_contact.hh"
#include "src/contact_representative/contact_representative_common_types.hh"
#include "src/domain/domain_common_types.hh"
#include "src/domain/domain_state_history.hh"
#include "src/domain/get_domains_notify_info_request.hh"
#include "src/domain/search_domain.hh"
#include "src/keyset/keyset_state_history.hh"
#include "src/keyset/search_keyset.hh"
#include "src/nsset/nsset_state_history.hh"
#include "src/nsset/search_nsset.hh"
#include "src/util/printable.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/optional.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <map>
#include <set>
#include <string>
#include <utility>
#include <vector>

namespace Fred {
namespace Registry {
namespace Util {

template <typename> struct Into;

template <>
struct Into<std::string>
{
    template <typename T>
    static std::string from(const Printable<T>& src)
    {
        return static_cast<const T&>(src).to_string();
    }
    static std::string from(const std::string& src);
    static std::string from(std::int32_t src);
    static std::string from(std::int64_t src);
    static std::string from(std::uint32_t src);
    static std::string from(std::uint64_t src);
    static std::string from(unsigned long long src);
    static std::string from(float src);
    static std::string from(bool src);
    static std::string from(const boost::uuids::uuid& src);
    template <typename T, typename N, template <typename> class ...Skills>
    static std::string from(const Util::StrongType<T, N, Skills...>& src)
    {
        return from(get_raw_value_from(src));
    }
    static std::string from(const Contact::ContactAdditionalIdentifier& src);
    static std::string from(const Contact::ContactHistoryInterval::Limit& src);
    static std::string from(const Contact::Privacy& src);
    static std::string from(const ContactRepresentative::ContactRepresentativeRef& src);
    static std::string from(const Domain::DomainHistoryInterval::Limit& src);
    static std::string from(const Keyset::KeysetHistoryInterval::Limit& src);
    static std::string from(const Nsset::NssetHistoryInterval::Limit& src);
    template <typename Clock, typename Resolution>
    static std::string from(const std::chrono::time_point<Clock, Resolution>& src);
    static std::string from(Contact::ContactItem src);
    static std::string from(Domain::DomainItem src);
    static std::string from(Keyset::KeysetItem src);
    static std::string from(Nsset::NssetItem src);
    static std::string from(const Domain::TimeInterval& src);
    static std::string from(Domain::LifecycleEvent src);
    static std::string from(const Domain::DomainContactRole& src);
    template <typename T>
    static std::string from(const boost::optional<T>& src)
    {
        if (src == boost::none)
        {
            return "none";
        }
        return Into<std::string>::from(*src);
    }
    struct BoostVariantToString : boost::static_visitor<std::string>
    {
        template <typename T>
        std::string operator()(T&& src) const
        {
            return Into<std::string>::from(std::forward<T>(src));
        }
    };
    template <typename ...Ts>
    static std::string from(const boost::variant<Ts...>& src)
    {
        return boost::apply_visitor(BoostVariantToString{}, src);
    }
    template <typename T, typename C, typename A>
    static std::string from(const std::set<T, C, A>& src)
    {
        if (src.empty())
        {
            return "{ }";
        }
        std::string result;
        for (const auto& item : src)
        {
            if (!result.empty())
            {
                result += ", ";
            }
            result += Into<std::string>::from(item);
        }
        return "{ " + result + " }";
    }
    template <typename K, typename V, typename C, typename A>
    static std::string from(const std::map<K, V, C, A>& src)
    {
        if (src.empty())
        {
            return "[ ]";
        }
        std::string result;
        for (const auto& key_value : src)
        {
            if (!result.empty())
            {
                result += ", ";
            }
            result += Into<std::string>::from(key_value.first) + ": " + Into<std::string>::from(key_value.second);
        }
        return "[ " + result + " ]";
    }
    template <typename T>
    static std::string from(const std::vector<T>& src)
    {
        if (src.empty())
        {
            return "{ }";
        }
        std::string result;
        for (const auto& item : src)
        {
            if (!result.empty())
            {
                result += ", ";
            }
            result += Into<std::string>::from(item);
        }
        return "{ " + result + " }";
    }
    template <typename T>
    static std::string from(const Contact::PrivacyControlled<T>& src)
    {
        const auto str_data = Into<std::string>::from(get_privacy_controlled_data(src));
        if (is_publicly_available(src))
        {
            return "public(" + str_data + ")";
        }
        return "non-public(" + str_data + ")";
    }
    static std::string from(const boost::asio::ip::address& ip_address)
    {
        return ip_address.to_string(); // FIXME deprecated
    }
};

template <typename Clock, typename Resolution>
struct Into<std::chrono::time_point<Clock, Resolution>>
{
    using Result = std::chrono::time_point<Clock, Resolution>;
    static Result from(const std::string& yyyy_mm_dd_hh_mm_ss_usec);
    static Result from(const boost::gregorian::date& date);
    static Result from(const boost::posix_time::ptime& time);
};

template <>
struct Into<boost::gregorian::date>
{
    using Result = boost::gregorian::date;
    template <typename Clock, typename Resolution>
    static Result from(const std::chrono::time_point<Clock, Resolution>& src);
};

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//INTO_HH_641B328DC7FFA550662C9E152729B983
