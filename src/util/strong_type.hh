/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRONG_TYPE_HH_803A93278FD56048C9930CDCC773833C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define STRONG_TYPE_HH_803A93278FD56048C9930CDCC773833C

#include "src/util/printable.hh"

#include <boost/lexical_cast.hpp>

#include <string>
#include <type_traits>

namespace Fred {
namespace Registry {
namespace Util {

template <typename Type, typename Name, template <typename> class ...Skills> class StrongType;

template <typename S, typename T>
constexpr S make_strong(T&&);

template <typename T, typename N, template <typename> class ...Skills>
constexpr const T& get_raw_value_from(const StrongType<T, N, Skills...>&);

template <typename N, template <typename> class ...Skills>
using StrongString = StrongType<std::string, N, Skills...>;

template <typename T, typename N, template <typename> class ...Skills>
class StrongType : public Skills<StrongType<T, N, Skills...>>...
{
public:
    using UnderlyingType = T;
    using Tag = N;
    constexpr StrongType() = default;
    ~StrongType() = default;
    constexpr StrongType(const StrongType&) = default;
    constexpr StrongType(StrongType&&) = default;
    constexpr StrongType& operator=(const StrongType&) = default;
    constexpr StrongType& operator=(StrongType&&) = default;
private:
    explicit constexpr StrongType(const T& src) : value_(src) { }
    explicit constexpr StrongType(T&& src) : value_(std::move(src)) { }
    constexpr StrongType& operator=(const T& src) { value_ = src; return *this; }
    constexpr StrongType& operator=(T&& src) { std::swap(value_, src); return *this; }
    T value_;
    friend StrongType make_strong<>(const T&);
    friend StrongType make_strong<>(T&&);
    friend const T& get_raw_value_from<>(const StrongType&);
};

namespace Skill {

template <typename T>
struct Printable : Util::Printable<T>
{
    template <typename S>
    static std::string to_string_cast(const S& src)
    {
        return boost::lexical_cast<std::string>(src);
    }
    static std::string to_string_cast(const std::string& src)
    {
        return "'" + src + "'";
    }
    const T& get_derived() const
    {
        return static_cast<const T&>(*this);
    }
    std::string to_string() const
    {
        return to_string_cast(get_raw_value_from(this->get_derived()));
    }
};

template <typename T>
struct HasOperatorLess
{
    friend bool operator<(const T& lhs, const T& rhs)
    {
        return get_raw_value_from(lhs) < get_raw_value_from(rhs);
    }
};

}//namespace Fred::Util::Skill

template <typename S, typename T>
constexpr S make_strong(T&& src)
{
    return S(std::forward<T>(src));
};

template <typename T, typename N, template <typename> class ...Skills>
constexpr const T& get_raw_value_from(const StrongType<T, N, Skills...>& src)
{
    return src.value_;
}

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//STRONG_TYPE_HH_803A93278FD56048C9930CDCC773833C
