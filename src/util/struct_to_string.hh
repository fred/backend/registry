/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef STRUCT_TO_STRING_HH_D7EB4DDD9A09912C1FFF5FE3F0B0972A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define STRUCT_TO_STRING_HH_D7EB4DDD9A09912C1FFF5FE3F0B0972A

#include "src/util/into.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Util {

class StructToString
{
public:
    StructToString();
    template <typename T>
    StructToString& add(const std::string& name, const T& value)
    {
        if (!buffer_.empty())
        {
            buffer_ += ", ";
        }
        buffer_ += name + ": " + Into<std::string>::from(value);
        return *this;
    }
    std::string finish();
private:
    std::string buffer_;
};

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//STRUCT_TO_STRING_HH_D7EB4DDD9A09912C1FFF5FE3F0B0972A
