/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef ENUM_SEQUENCE_HH_EE2C1726FD1512F13C9AE8DAEFA6C76E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define ENUM_SEQUENCE_HH_EE2C1726FD1512F13C9AE8DAEFA6C76E

#include <array>
#include <utility>

namespace Fred {
namespace Registry {
namespace Util {

template <typename I, typename T> class EnumSequence;

template <typename E, typename T, typename V, E ...items>
constexpr void visit(EnumSequence<std::integer_sequence<E, items...>, T>& sequence, V&& visiter)
{
    sequence.visit(std::forward<V>(visiter));
}

template <typename E, E head, E ...tail, typename T>
class EnumSequence<std::integer_sequence<E, head, tail...>, T>
{
public:
    using Index = std::integer_sequence<E, head, tail...>;
    template <typename V>
    void visit(V&& visiter)
    {
        auto data_begin = data_.begin();
        visit(std::forward<V>(visiter), data_begin);
    }
private:
    using Tail = EnumSequence<std::integer_sequence<E, tail...>, T>;
    static constexpr std::size_t length = 1 + Tail::length;
    template <typename V, typename I>
    static void visit(V&& visiter, I& data_iterator)
    {
        visiter.template operator()<head>(*data_iterator);
        ++data_iterator;
        Tail::template visit(std::forward<V>(visiter), data_iterator);
    }
    std::array<T, length> data_;
    template <typename, typename>
    friend class EnumSequence;
};

template <typename E, typename T>
class EnumSequence<std::integer_sequence<E>, T>
{
public:
    using Index = std::integer_sequence<E>;
    template <typename V>
    constexpr void visit(V&&) const noexcept { }
private:
    static constexpr std::size_t length = 0;
    template <typename V, typename I>
    static constexpr void visit(V&&, I&) noexcept { }
    template <typename, typename>
    friend class EnumSequence;
};

}//namespace Fred::Registry::Util
}//namespace Fred::Registry
}//namespace Fred

#endif//ENUM_SEQUENCE_HH_EE2C1726FD1512F13C9AE8DAEFA6C76E
