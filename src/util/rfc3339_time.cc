/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/rfc3339_time.hh"

#include <boost/date_time/posix_time/posix_time.hpp>

#include <cerrno>
#include <cstring>
#include <ctime>
#include <utility>

namespace {
namespace Rfc3339 {

std::chrono::seconds get_time_zone_offset(const std::string&);
void check_local_time_part(const std::string&);
void check_mday(const std::tm&);
int get_value(const char* str, int length);
class NotCompliant : public Fred::Registry::Util::NotRfc3339Compliant
{
public:
    NotCompliant(std::string msg) : msg_{std::move(msg)} { }
    const char* what() const noexcept override { return msg_.c_str(); }
private:
    std::string msg_;
};

}//namespace {anonymous}::Rfc3339
}//namespace {anonymous}

using namespace Fred::Registry::Util;

TimePointSeconds Fred::Registry::Util::seconds_from_rfc3339_formated_string(const std::string& src)
{
    static constexpr auto length_of_the_shortest_value = std::strlen("YYYY-MM-DDThh:mm:ssZ");
    if (src.length() < length_of_the_shortest_value)
    {
        throw Rfc3339::NotCompliant{"string too short to represent datetime in RFC 3339 compliant format"};
    }
    static constexpr auto date_time_delimiter_position = std::strlen("YYYY-MM-DD");
    switch (src[date_time_delimiter_position])
    {
        case 'T'://preferred by RFC 3339
        case 't'://allowed by RFC 3339
        case ' '://allowed by RFC 3339
            break;
        default:
            throw Rfc3339::NotCompliant{"unexpected date-time delimiter"};
    }
    try
    {
        Rfc3339::check_local_time_part(src);
        const auto t = [str = src.c_str()]()
        {
            std::tm tm{};
            tm.tm_year = Rfc3339::get_value(str, std::strlen("YYYY")) - 1900; // YYYY - 1900
            tm.tm_mon = Rfc3339::get_value(str + std::strlen("YYYY-"), std::strlen("MM")) - 1; // MM - 1
            tm.tm_mday = Rfc3339::get_value(str + std::strlen("YYYY-MM-"), std::strlen("DD"));
            tm.tm_hour = Rfc3339::get_value(str + std::strlen("YYYY-MM-DDT"), std::strlen("hh"));
            tm.tm_min = Rfc3339::get_value(str + std::strlen("YYYY-MM-DDThh:"), std::strlen("mm"));
            tm.tm_sec = Rfc3339::get_value(str + std::strlen("YYYY-MM-DDThh:mm:"), std::strlen("ss"));
            tm.tm_isdst = 0;
            Rfc3339::check_mday(tm);
            const auto t = ::timegm(&tm);
            if (t == ::time_t{-1})
            {
                throw Rfc3339::NotCompliant{std::strerror(errno)};
            }
            return std::chrono::seconds{t};
        }();
        const auto timezone_offset = Rfc3339::get_time_zone_offset(src);
        return TimePointSeconds{t - timezone_offset};
    }
    catch (const NotRfc3339Compliant&)
    {
        throw;
    }
    catch (const std::exception& e)
    {
        throw Rfc3339::NotCompliant{e.what()};
    }
    catch (...)
    {
        throw Rfc3339::NotCompliant{"unexpected exception"};
    }
}

namespace {
namespace Rfc3339 {

constexpr bool is_leap_year(int);
void check_february_mday(const std::tm&);
constexpr bool is_zulu_zone_indicator(char);
constexpr bool is_start_of_local_offset(char);
constexpr int get_value(char);

}//namespace {anonymous}::Rfc3339

using namespace Rfc3339;

std::chrono::seconds Rfc3339::get_time_zone_offset(const std::string& src)
{
    const auto zulu_zone_position = src.length() - std::strlen("Z");
    if (is_zulu_zone_indicator(src[zulu_zone_position]))
    {
        static constexpr auto zulu_zone_offset = std::chrono::seconds{0};
        return zulu_zone_offset;
    }
    const int local_zone_position = src.length() - std::strlen("+00:00");
    if (!is_start_of_local_offset(src[local_zone_position]))
    {
        throw NotCompliant{"string does not contain any time zone information"};
    }
    if (src[local_zone_position + std::strlen("+00")] != ':')
    {
        throw NotCompliant{"invalid time zone hours-minutes delimiter"};
    }
    const auto sign = src[local_zone_position] == '+' ? 1 : -1;
    const auto offset_hours = get_value(src.c_str() + local_zone_position + std::strlen("+"), std::strlen("00"));
    const auto offset_minutes = get_value(src.c_str() + local_zone_position + std::strlen("+00:"), std::strlen("00"));
    if ((12 < offset_hours) && ((sign != 1) || (14 < offset_hours)))
    {
        throw NotCompliant{"too many hours in offset"};
    }
    if (60 <= offset_minutes)
    {
        throw NotCompliant{"too many minutes in offset"};
    }
    const auto offset = 60 * offset_hours + offset_minutes;
    const auto the_offset_is_unknown = (sign == -1) && (offset == 0);//offset `-00:00` means "the offset to local time is unknown"
    if (the_offset_is_unknown)
    {
        throw NotCompliant{"time zone offset is unknown"};
    }
    return std::chrono::minutes{sign * offset};
}

void Rfc3339::check_local_time_part(const std::string& src)
{
    //hh:mm:ss[.f*]
    const char* const local_time_part = src.c_str() + std::strlen("YYYY-MM-DDT");
    if ((local_time_part[std::strlen("hh")] != ':') ||
        (local_time_part[std::strlen("hh:mm")] != ':'))
    {
        throw NotCompliant{"invalid hours-minutes-seconds delimiter"};
    }
    const auto hours = get_value(local_time_part + std::strlen(""), std::strlen("hh"));
    const auto minutes = get_value(local_time_part + std::strlen("hh:"), std::strlen("mm"));
    const auto seconds = get_value(local_time_part + std::strlen("hh:mm:"), std::strlen("ss"));
    if (24 <= hours)
    {
        throw NotCompliant{"too many hours"};
    }
    if (60 <= minutes)
    {
        throw NotCompliant{"too many minutes"};
    }
    if (60 <= seconds)
    {
        throw NotCompliant{"too many seconds"};
    }
    const char* const fraction_begin = local_time_part + std::strlen("hh:mm:ss");
    const char* fraction_end = fraction_begin;
    if (*fraction_begin == '.')
    {
        ++fraction_end;
        while (('0' <= *fraction_end) && (*fraction_end <= '9'))
        {
            ++fraction_end;
        }
        if (fraction_end <= (fraction_begin + std::strlen(".")))
        {
            throw NotCompliant{"fraction of seconds has to contain at least one cipher"};
        }
    }
    const auto zone_length = src.c_str() + src.length() - fraction_end;
    if (zone_length == std::strlen("Z"))
    {
        return;
    }
    if ((zone_length == std::strlen("+00:00")) && !is_zulu_zone_indicator(src.back()))
    {
        return;
    }
    throw NotCompliant{"invalid time zone offset"};
}

template <int divisor>
constexpr bool is_divisible_by(int divident)
{
    return (divident % divisor) == 0;
}

constexpr bool Rfc3339::is_leap_year(int year)
{
    return (is_divisible_by<4>(year) && !is_divisible_by<100>(year)) || is_divisible_by<400>(year);
}

static_assert(!Rfc3339::is_leap_year(1900), "1900 is not a leap year");
static_assert(!Rfc3339::is_leap_year(1901), "1901 is not a leap year");
static_assert(!Rfc3339::is_leap_year(1902), "1902 is not a leap year");
static_assert(!Rfc3339::is_leap_year(1903), "1903 is not a leap year");
static_assert(Rfc3339::is_leap_year(1904), "1904 is a leap year");
static_assert(Rfc3339::is_leap_year(2000), "2000 is a leap year");

void Rfc3339::check_february_mday(const std::tm& tm)
{
    const auto number_of_days = !is_leap_year(tm.tm_year + 1900) ? 28
                                                                 : 29;
    if (number_of_days < tm.tm_mday)
    {
        throw NotCompliant{"not a month day number"};
    }
}

void Rfc3339::check_mday(const std::tm& tm)
{
    if (tm.tm_mday < 1)
    {
        throw NotCompliant{"not a month day number"};
    }
    switch (tm.tm_mon + 1)
    {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            if (tm.tm_mday <= 31)
            {
                return;
            }
            throw NotCompliant{"not a month day number"};
        case 4:
        case 6:
        case 9:
        case 11:
            if (tm.tm_mday <= 30)
            {
                return;
            }
            throw NotCompliant{"not a month day number"};
        case 2:
            check_february_mday(tm);
            return;
    }
    throw NotCompliant{"not a month number"};
}

constexpr bool Rfc3339::is_zulu_zone_indicator(char c)
{
    switch (c)
    {
        case 'Z':
        case 'z':
            return true;
    }
    return false;
}

constexpr bool Rfc3339::is_start_of_local_offset(char c)
{
    switch (c)
    {
        case '+':
        case '-':
            return true;
    }
    return false;
}

constexpr int Rfc3339::get_value(char c)
{
    if (('0' <= c) && (c <= '9'))
    {
        return c - '0';
    }
    throw NotCompliant{"not a digit character"};
}

int Rfc3339::get_value(const char* str, int length)
{
    int value = 0;
    for (const char* const end = str + length; str != end; ++str)
    {
        value = 10 * value + get_value(*str);
    }
    return value;
}

}//namespace {anonymous}
