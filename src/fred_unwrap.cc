/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/fred_unwrap.hh"
#include "src/domain/domain_common_types.hh"
#include "src/contact/privacy_controlled.hh"

#include "libfred/registrable_object/contact/contact_reference.hh"

#include <boost/asio/ip/address.hpp>
#include <boost/date_time/posix_time/ptime.hpp>

#include <regex>
#include <stdexcept>

namespace Fred {
namespace Registry {

namespace {

template <typename D, typename S>
D into(const S&);

template <typename D, typename S>
D into(const Nullable<S>&);

template <typename D, typename S>
boost::optional<D> into_optional(const Nullable<S>& src)
{
    if (src.isnull())
    {
        return boost::none;
    }
    return into<D>(src.get_value());
}

template <typename D, typename S>
boost::optional<D> into_optional(const Optional<S>& src)
{
    if (!src.is_set())
    {
        return boost::none;
    }
    return into<D>(src.get_value());
}

template <typename S, typename T>
boost::optional<S> into_optional_strong(const Nullable<T>& src)
{
    if (src.isnull())
    {
        return boost::none;
    }
    return Util::make_strong<S>(src.get_value());
}

template <typename S>
std::vector<S> into_vector_of_strong(const Nullable<std::string>& src, const std::string& delimiter)
{
    if (src.isnull() || src.get_value().empty())
    {
        return std::vector<S>{};
    }

    std::vector<S> result;
    const std::regex delimiter_regex(delimiter);
    const std::string data = src.get_value();
    static constexpr int stuff_between_matches = -1;
    std::sregex_token_iterator iter{data.cbegin(), data.cend(), delimiter_regex, stuff_between_matches};

    for (std::sregex_token_iterator end; iter != end; ++iter)
    {
        result.push_back(Util::make_strong<S>(iter->str()));
    }

    return result;
}

template <typename S>
decltype(auto) emails_into_vector_of_strong(const Nullable<std::string>& src)
{
    return into_vector_of_strong<S>(src, "\\s*,\\s*");
}

template <>
std::string into(const Nullable<std::string>& src)
{
    if (src.isnull())
    {
        return std::string();
    }
    return src.get_value();
}

template <>
std::string into(const Optional<std::string>& src)
{
    if (!src.is_set())
    {
        return std::string();
    }
    return src.get_value();
}

template <>
Contact::ContactId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Contact::ContactId>(src);
}

template <>
Contact::ContactHistoryId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Contact::ContactHistoryId>(src);
}

template <>
Contact::ContactLightInfo into(const LibFred::RegistrableObject::Contact::ContactReference& src)
{
    Contact::ContactLightInfo dst;
    dst.id = into<Contact::ContactId>(get_raw_value_from(src.uuid));
    dst.handle = Util::make_strong<Contact::ContactHandle>(src.handle);
    dst.name = src.name;
    dst.organization = src.organization;
    return dst;
}

template <>
PlaceAddress into(const LibFred::Contact::PlaceAddress& src)
{
    PlaceAddress dst;
    if (!src.street3.get_value_or_default().empty())
    {
        dst.street.reserve(3);
        dst.street.push_back(src.street1);
        dst.street.push_back(src.street2.get_value_or_default());
        dst.street.push_back(src.street3.get_value());
    }
    else if (!src.street2.get_value_or_default().empty())
    {
        dst.street.reserve(2);
        dst.street.push_back(src.street1);
        dst.street.push_back(src.street2.get_value());
    }
    else if (!src.street1.empty())
    {
        dst.street.reserve(1);
        dst.street.push_back(src.street1);
    }
    dst.city = src.city;
    dst.state_or_province = into<std::string>(src.stateorprovince);
    dst.postal_code = Util::make_strong<PostalCode>(src.postalcode);
    dst.country_code = Util::make_strong<CountryCode>(src.country);
    return dst;
}

Contact::ContactAdditionalIdentifier into_contact_additional_identifier(
        const std::string& ssn_type,
        const std::string& ssn_value)
{
    if (ssn_type == LibFred::PersonalIdUnion::get_RC("").get_type())
    {
        return Util::make_strong<Contact::NationalIdentityNumber>(ssn_value);
    }
    if (ssn_type == LibFred::PersonalIdUnion::get_OP("").get_type())
    {
        return Util::make_strong<Contact::NationalIdentityCard>(ssn_value);
    }
    if (ssn_type == LibFred::PersonalIdUnion::get_PASS("").get_type())
    {
        return Util::make_strong<Contact::PassportNumber>(ssn_value);
    }
    if (ssn_type == LibFred::PersonalIdUnion::get_ICO("").get_type())
    {
        return Util::make_strong<CompanyRegistrationNumber>(ssn_value);
    }
    if (ssn_type == LibFred::PersonalIdUnion::get_MPSV("").get_type())
    {
        return Util::make_strong<Contact::SocialSecurityNumber>(ssn_value);
    }
    if (ssn_type == LibFred::PersonalIdUnion::get_BIRTHDAY("").get_type())
    {
        return Util::make_strong<Contact::Birthdate>(ssn_value);
    }
    throw std::runtime_error("unknown 'ssn type'");
}

boost::optional<Contact::ContactAdditionalIdentifier> into_optional_contact_additional_identifier(
        const Nullable<std::string>& ssn_type,
        const Nullable<std::string>& ssn_value)
{
    if (ssn_type.isnull() || ssn_value.isnull())
    {
        return boost::none;
    }
    return into_contact_additional_identifier(ssn_type.get_value(), ssn_value.get_value());
}

template <>
Contact::ContactAddress into(const LibFred::ContactAddress& src)
{
    Contact::ContactAddress dst;
    dst.company = into<std::string>(src.company_name);
    if (!src.street3.get_value_or_default().empty())
    {
        dst.street.reserve(3);
        dst.street.push_back(src.street1);
        dst.street.push_back(src.street2.get_value_or_default());
        dst.street.push_back(src.street3.get_value());
    }
    else if (!src.street2.get_value_or_default().empty())
    {
        dst.street.reserve(2);
        dst.street.push_back(src.street1);
        dst.street.push_back(src.street2.get_value());
    }
    else if (!src.street1.empty())
    {
        dst.street.reserve(1);
        dst.street.push_back(src.street1);
    }
    dst.city = src.city;
    dst.state_or_province = into<std::string>(src.stateorprovince);
    dst.postal_code = Util::make_strong<PostalCode>(src.postalcode);
    dst.country_code = Util::make_strong<CountryCode>(src.country);
    return dst;
}

template <>
Contact::WarningLetter into(const Nullable<bool>& src)
{
    Contact::WarningLetter dst;
    if (src.isnull())
    {
        dst.preference = Contact::WarningLetter::SendingPreference::not_specified;
    }
    else
    {
        dst.preference = src.get_value() ? Contact::WarningLetter::SendingPreference::to_send
                                         : Contact::WarningLetter::SendingPreference::not_to_send;
    }
    return dst;
}

template <typename T>
Contact::PrivacyControlled<T> into_privacy_controlled(const T& src, bool is_public)
{
    if (is_public)
    {
        return Contact::make_public_data(src);
    }
    return Contact::make_non_public_data(src);
}

template <LibFred::Object_Type::Enum o>
struct CollectStateFlagsPresence
{
    template <typename ...Flags>
    struct Visitor
    {
        using State = LibFred::RegistrableObject::State;
        template <typename F, int>
        ::Util::FlagSetVisiting visit(const typename State::Of<o, Flags...>::Type& state)
        {
            result[F::name] = state.template is_set<F>();
            return ::Util::FlagSetVisiting::can_continue;
        }
        std::map<std::string, bool> result;
    };
};

template <LibFred::Object_Type::Enum o, typename ...Flags>
std::map<std::string, bool> into_flags_presence(
        const ::Util::FlagSet<LibFred::RegistrableObject::State::Of<o, Flags...>, Flags...>& src)
{
    return src.template visit<CollectStateFlagsPresence<o>::template Visitor>().result;
}

template <>
Contact::ContactState into(const LibFred::RegistrableObject::Contact::ContactState& src)
{
    Contact::ContactState dst;
    dst.flag_presents = into_flags_presence(src);
    return dst;
}

template <>
Domain::DomainState into(const LibFred::RegistrableObject::Domain::DomainState& src)
{
    Domain::DomainState dst;
    dst.flag_presents = into_flags_presence(src);
    return dst;
}

template <>
Keyset::KeysetState into(const LibFred::RegistrableObject::Keyset::KeysetState& src)
{
    Keyset::KeysetState dst;
    dst.flag_presents = into_flags_presence(src);
    return dst;
}

template <>
Nsset::NssetState into(const LibFred::RegistrableObject::Nsset::NssetState& src)
{
    Nsset::NssetState dst;
    dst.flag_presents = into_flags_presence(src);
    return dst;
}

struct CollectStateFlagNames
{
    template <typename ...Flags>
    struct Visitor
    {
        using State = LibFred::RegistrableObject::State;
        template <typename F, int idx>
        ::Util::FlagSetVisiting visit()
        {
            if (names.size() != unsigned(idx))
            {
                throw std::runtime_error("unexpected order of filling names");
            }
            names.emplace_back(F::name);
            return ::Util::FlagSetVisiting::can_continue;
        }
        std::vector<std::string> names;
    };
};

template <LibFred::Object_Type::Enum o>
struct CollectPresentStateFlags
{
    template <typename ...Flags>
    struct Visitor
    {
        Visitor() { result.reserve(sizeof...(Flags)); }
        using State = LibFred::RegistrableObject::State;
        template <typename F, int>
        ::Util::FlagSetVisiting visit(const typename State::Of<o, Flags...>::Type& state)
        {
            result.push_back(state.template is_set<F>());
            return ::Util::FlagSetVisiting::can_continue;
        }
        std::vector<bool> result;
    };
};

template <LibFred::Object_Type::Enum o, typename ...Flags>
std::vector<bool> into_present_flags(
        const ::Util::FlagSet<LibFred::RegistrableObject::State::Of<o, Flags...>, Flags...>& src)
{
    return src.template visit<CollectPresentStateFlags<o>::template Visitor>().result;
}

template <>
Contact::ContactStateHistory into(const LibFred::RegistrableObject::Contact::ContactStateHistory& src)
{
    Contact::ContactStateHistory dst;
    dst.flags_names =
            LibFred::RegistrableObject::Contact::ContactState::static_visit<CollectStateFlagNames::Visitor>().names;
    dst.timeline.reserve(src.timeline.size());
    for (const auto& item : src.timeline)
    {
        Contact::ContactStateHistory::Record record;
        record.valid_from = item.valid_from;
        record.presents = into_present_flags(item.state);
        dst.timeline.push_back(record);
    }
    const bool valid_to_is_unlimited = src.valid_to == src.valid_to.max();
    if (!valid_to_is_unlimited)
    {
        dst.valid_to = src.valid_to;
    }
    return dst;
}

template <>
Contact::ContactDataHistory::Record into(const LibFred::RegistrableObject::Contact::ContactDataHistory::Record& src)
{
    Contact::ContactDataHistory::Record dst;
    dst.contact_history_id = into<Contact::ContactHistoryId>(get_raw_value_from(src.history_uuid));
    dst.valid_from = src.valid_from;
    return dst;
}

template <>
Contact::ContactDataHistory into(const LibFred::RegistrableObject::Contact::ContactDataHistory& src)
{
    Contact::ContactDataHistory dst;
    dst.contact_id = into<Contact::ContactId>(get_raw_value_from(src.object_uuid));
    dst.timeline.reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        dst.timeline.push_back(into<Contact::ContactDataHistory::Record>(record));
    }
    dst.valid_to = src.valid_to;
    return dst;
}

template <>
Contact::ContactLifetime into(const LibFred::RegistrableObject::Contact::ContactHandleHistory::Record& src)
{
    Contact::ContactLifetime dst;
    dst.contact_id = into<Contact::ContactId>(get_raw_value_from(src.uuid));
    dst.begin.contact_history_id = into<Contact::ContactHistoryId>(get_raw_value_from(src.begin.history_uuid));
    dst.begin.timestamp = src.begin.timestamp;
    if (src.end.timestamp != boost::none)
    {
        Contact::ContactLifetime::TimeSpec end;
        end.contact_history_id = into<Contact::ContactHistoryId>(get_raw_value_from(src.end.history_uuid));
        end.timestamp = *src.end.timestamp;
        dst.end = end;
    }
    return dst;
}

template <typename Clock, typename Dur>
std::chrono::time_point<Clock, Dur> into_time_point(const boost::posix_time::ptime& src)
{
    const auto time_since_epoch = src - boost::posix_time::from_time_t(0);
    static_assert(Dur::period::num == 1);
    return Clock::from_time_t(time_since_epoch.total_seconds()) +
           Dur{time_since_epoch.fractional_seconds() *
               (Dur::period::den / (time_since_epoch.ticks_per_second() * Dur::period::num))};
}

template <>
std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> into(const boost::posix_time::ptime& src)
{
    return into_time_point<std::chrono::system_clock, std::chrono::nanoseconds>(src);
}

template <>
Registrar::RegistrarHandle into(const std::string& src)
{
    return Util::make_strong<Registrar::RegistrarHandle>(src);
}

template <>
Keyset::DnsKey into(const LibFred::DnsKey& src)
{
    Keyset::DnsKey dst;
    dst.flags = src.get_flags();
    dst.protocol = src.get_protocol();
    dst.alg = src.get_alg();
    dst.key = src.get_key();
    return dst;
}

template <>
std::vector<Keyset::DnsKey> into(const std::vector<LibFred::DnsKey>& src)
{
    std::vector<Keyset::DnsKey> dst;
    dst.reserve(src.size());
    std::transform(
            src.cbegin(),
            src.cend(),
            std::back_inserter(dst),
            [] (const auto& c) {
                return into<Keyset::DnsKey>(c);
            });
    return dst;
}

template <>
Nsset::DnsHost into(const LibFred::DnsHost& src)
{
    Nsset::DnsHost dst;
    dst.fqdn = src.get_fqdn();
    for (const auto& inet_addr : src.get_inet_addr())
    {
        dst.ip_addresses.push_back(inet_addr);
    }
    return dst;
}

template <>
std::vector<Nsset::DnsHost> into(const std::vector<LibFred::DnsHost>& src)
{
    std::vector<Nsset::DnsHost> dst;
    dst.reserve(src.size());
    std::transform(
            src.cbegin(),
            src.cend(),
            std::back_inserter(dst),
            [] (const auto& c) {
                return into<Nsset::DnsHost>(c);
            });
    return dst;
}

template <>
Nsset::TechnicalCheckLevel into(const short& src)
{
    Nsset::TechnicalCheckLevel dst;
    dst.technical_check_level = src;
    return dst;
}

template <typename Info>
Object::ObjectEvents make_object_events(const Info& src)
{
    using TimePoint = Object::ObjectEvents::EventData::TimePoint;
    Object::ObjectEvents dst;

    dst.registered = Object::ObjectEvents::EventData{
            into<TimePoint>(src.creation_time),
            into<Registrar::RegistrarHandle>(src.create_registrar_handle)};

    if (src.transfer_time.isnull())
    {
        dst.transferred = Object::ObjectEvents::EventData{
                boost::none,
                into<Registrar::RegistrarHandle>(src.sponsoring_registrar_handle)};
    }
    else
    {
        dst.transferred = Object::ObjectEvents::EventData{
                into<TimePoint>(src.transfer_time.get_value()),
                into<Registrar::RegistrarHandle>(src.sponsoring_registrar_handle)};
    }

    if (src.update_time.isnull())
    {
        dst.updated = boost::none;
    }
    else
    {
        if (!src.update_registrar_handle.isnull())
        {
            dst.updated = Object::ObjectEvents::EventData{
                    into<TimePoint>(src.update_time.get_value()),
                    into<Registrar::RegistrarHandle>(src.update_registrar_handle.get_value())};
        }
        else
        {
            dst.updated = Object::ObjectEvents::EventData{into<TimePoint>(src.update_time.get_value())};
        }
    }

    if (src.delete_time.isnull())
    {
        dst.unregistered = boost::none;
    }
    else
    {
        dst.unregistered = Object::ObjectEvents::EventData{into<TimePoint>(src.delete_time.get_value())};
    }
    return dst;
}

template <>
Object::ObjectEvents into(const LibFred::InfoContactData& src)
{
    return make_object_events(src);
}

template <>
Object::ObjectEvents into(const LibFred::InfoDomainData& src)
{
    return make_object_events(src);
}

template <>
Object::ObjectEvents into(const LibFred::InfoKeysetData& src)
{
    return make_object_events(src);
}

template <>
Object::ObjectEvents into(const LibFred::InfoNssetData& src)
{
    return make_object_events(src);
}

template <>
Keyset::KeysetId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Keyset::KeysetId>(src);
}

template <>
Keyset::KeysetHistoryId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Keyset::KeysetHistoryId>(src);
}

template <>
Keyset::KeysetLightInfo into(const LibFred::RegistrableObject::Keyset::KeysetReference& src)
{
    Keyset::KeysetLightInfo dst;
    dst.id = into<Keyset::KeysetId>(get_raw_value_from(src.uuid));
    dst.handle = Util::make_strong<Keyset::KeysetHandle>(src.handle);
    return dst;
}

template <>
Nsset::NssetId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Nsset::NssetId>(src);
}

template <>
Nsset::NssetHistoryId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Nsset::NssetHistoryId>(src);
}

template <>
Nsset::NssetLightInfo into(const LibFred::RegistrableObject::Nsset::NssetReference& src)
{
    Nsset::NssetLightInfo dst;
    dst.id = into<Nsset::NssetId>(get_raw_value_from(src.uuid));
    dst.handle = Util::make_strong<Nsset::NssetHandle>(src.handle);
    return dst;
}

template <>
Domain::DomainId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Domain::DomainId>(src);
}

template <>
Domain::DomainHistoryId into(const boost::uuids::uuid& src)
{
    return Util::make_strong<Domain::DomainHistoryId>(src);
}

template <>
std::vector<Contact::ContactLightInfo> into(const std::vector<LibFred::RegistrableObject::Contact::ContactReference>& src)
{
    std::vector<Contact::ContactLightInfo> dst;
    dst.reserve(src.size());
    std::transform(
            src.cbegin(),
            src.cend(),
            std::back_inserter(dst),
            [] (const auto& c) {
                return into<Contact::ContactLightInfo>(c);
            });
    return dst;
}

template <>
Domain::DomainStateHistory into(const LibFred::RegistrableObject::Domain::DomainStateHistory& src)
{
    Domain::DomainStateHistory dst;
    dst.flags_names =
            LibFred::RegistrableObject::Domain::DomainState::static_visit<CollectStateFlagNames::Visitor>().names;
    dst.timeline.reserve(src.timeline.size());
    for (const auto& item : src.timeline)
    {
        Domain::DomainStateHistory::Record record;
        record.valid_from = item.valid_from;
        record.presents = into_present_flags(item.state);
        dst.timeline.push_back(record);
    }
    const bool valid_to_is_unlimited = src.valid_to == src.valid_to.max();
    if (!valid_to_is_unlimited)
    {
        dst.valid_to = src.valid_to;
    }
    return dst;
}

template <>
Domain::DomainDataHistory::Record into(const LibFred::RegistrableObject::Domain::DomainDataHistory::Record& src)
{
    Domain::DomainDataHistory::Record dst;
    dst.domain_history_id = into<Domain::DomainHistoryId>(get_raw_value_from(src.history_uuid));
    dst.valid_from = src.valid_from;
    return dst;
}

template <>
Domain::DomainDataHistory into(const LibFred::RegistrableObject::Domain::DomainDataHistory& src)
{
    Domain::DomainDataHistory dst;
    dst.domain_id = into<Domain::DomainId>(get_raw_value_from(src.object_uuid));
    dst.timeline.reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        dst.timeline.push_back(into<Domain::DomainDataHistory::Record>(record));
    }
    dst.valid_to = src.valid_to;
    return dst;
}

template <>
Domain::DomainLifetime into(const LibFred::RegistrableObject::Domain::FqdnHistory::Record& src)
{
    Domain::DomainLifetime dst;
    dst.domain_id = into<Domain::DomainId>(get_raw_value_from(src.uuid));
    dst.begin.domain_history_id = into<Domain::DomainHistoryId>(get_raw_value_from(src.begin.history_uuid));
    dst.begin.timestamp = src.begin.timestamp;
    if (src.end.timestamp != boost::none)
    {
        Domain::DomainLifetime::TimeSpec end;
        end.domain_history_id = into<Domain::DomainHistoryId>(get_raw_value_from(src.end.history_uuid));
        end.timestamp = *src.end.timestamp;
        dst.end = end;
    }
    return dst;
}

template <>
Keyset::KeysetStateHistory into(const LibFred::RegistrableObject::Keyset::KeysetStateHistory& src)
{
    Keyset::KeysetStateHistory dst;
    dst.flags_names =
            LibFred::RegistrableObject::Keyset::KeysetState::static_visit<CollectStateFlagNames::Visitor>().names;
    dst.timeline.reserve(src.timeline.size());
    for (const auto& item : src.timeline)
    {
        Keyset::KeysetStateHistory::Record record;
        record.valid_from = item.valid_from;
        record.presents = into_present_flags(item.state);
        dst.timeline.push_back(record);
    }
    const bool valid_to_is_unlimited = src.valid_to == src.valid_to.max();
    if (!valid_to_is_unlimited)
    {
        dst.valid_to = src.valid_to;
    }
    return dst;
}

template <>
Keyset::KeysetDataHistory::Record into(const LibFred::RegistrableObject::Keyset::KeysetDataHistory::Record& src)
{
    Keyset::KeysetDataHistory::Record dst;
    dst.keyset_history_id = into<Keyset::KeysetHistoryId>(get_raw_value_from(src.history_uuid));
    dst.valid_from = src.valid_from;
    return dst;
}

template <>
Keyset::KeysetDataHistory into(const LibFred::RegistrableObject::Keyset::KeysetDataHistory& src)
{
    Keyset::KeysetDataHistory dst;
    dst.keyset_id = into<Keyset::KeysetId>(get_raw_value_from(src.object_uuid));
    dst.timeline.reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        dst.timeline.push_back(into<Keyset::KeysetDataHistory::Record>(record));
    }
    dst.valid_to = src.valid_to;
    return dst;
}

template <>
Keyset::KeysetLifetime into(const LibFred::RegistrableObject::Keyset::KeysetHandleHistory::Record& src)
{
    Keyset::KeysetLifetime dst;
    dst.keyset_id = into<Keyset::KeysetId>(get_raw_value_from(src.uuid));
    dst.begin.keyset_history_id = into<Keyset::KeysetHistoryId>(get_raw_value_from(src.begin.history_uuid));
    dst.begin.timestamp = src.begin.timestamp;
    if (src.end.timestamp != boost::none)
    {
        Keyset::KeysetLifetime::TimeSpec end;
        end.keyset_history_id = into<Keyset::KeysetHistoryId>(get_raw_value_from(src.end.history_uuid));
        end.timestamp = *src.end.timestamp;
        dst.end = end;
    }
    return dst;
}

template <>
Nsset::NssetStateHistory into(const LibFred::RegistrableObject::Nsset::NssetStateHistory& src)
{
    Nsset::NssetStateHistory dst;
    dst.flags_names =
            LibFred::RegistrableObject::Nsset::NssetState::static_visit<CollectStateFlagNames::Visitor>().names;
    dst.timeline.reserve(src.timeline.size());
    for (const auto& item : src.timeline)
    {
        Nsset::NssetStateHistory::Record record;
        record.valid_from = item.valid_from;
        record.presents = into_present_flags(item.state);
        dst.timeline.push_back(record);
    }
    const bool valid_to_is_unlimited = src.valid_to == src.valid_to.max();
    if (!valid_to_is_unlimited)
    {
        dst.valid_to = src.valid_to;
    }
    return dst;
}

template <>
Nsset::NssetDataHistory::Record into(const LibFred::RegistrableObject::Nsset::NssetDataHistory::Record& src)
{
    Nsset::NssetDataHistory::Record dst;
    dst.nsset_history_id = into<Nsset::NssetHistoryId>(get_raw_value_from(src.history_uuid));
    dst.valid_from = src.valid_from;
    return dst;
}

template <>
Nsset::NssetDataHistory into(const LibFred::RegistrableObject::Nsset::NssetDataHistory& src)
{
    Nsset::NssetDataHistory dst;
    dst.nsset_id = into<Nsset::NssetId>(get_raw_value_from(src.object_uuid));
    dst.timeline.reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        dst.timeline.push_back(into<Nsset::NssetDataHistory::Record>(record));
    }
    dst.valid_to = src.valid_to;
    return dst;
}

template <>
Nsset::NssetLifetime into(const LibFred::RegistrableObject::Nsset::NssetHandleHistory::Record& src)
{
    Nsset::NssetLifetime dst;
    dst.nsset_id = into<Nsset::NssetId>(get_raw_value_from(src.uuid));
    dst.begin.nsset_history_id = into<Nsset::NssetHistoryId>(get_raw_value_from(src.begin.history_uuid));
    dst.begin.timestamp = src.begin.timestamp;
    if (src.end.timestamp != boost::none)
    {
        Nsset::NssetLifetime::TimeSpec end;
        end.nsset_history_id = into<Nsset::NssetHistoryId>(get_raw_value_from(src.end.history_uuid));
        end.timestamp = *src.end.timestamp;
        dst.end = end;
    }
    return dst;
}

boost::optional<PlaceAddress> into_optional_place_address(const LibFred::InfoRegistrarData& src)
{
    if (src.street1.isnull() &&
        src.street2.isnull() &&
        src.street3.isnull() &&
        src.city.isnull() &&
        src.stateorprovince.isnull() &&
        src.postalcode.isnull() &&
        src.country.isnull())
    {
        return boost::none;
    }
    PlaceAddress dst;
    if (!src.street3.get_value_or_default().empty())
    {
        dst.street.reserve(3);
        dst.street.push_back(src.street1.get_value_or_default());
        dst.street.push_back(src.street2.get_value_or_default());
        dst.street.push_back(src.street3.get_value());
    }
    else if (!src.street2.get_value_or_default().empty())
    {
        dst.street.reserve(2);
        dst.street.push_back(src.street1.get_value_or_default());
        dst.street.push_back(src.street2.get_value());
    }
    else if (!src.street1.get_value_or_default().empty())
    {
        dst.street.reserve(1);
        dst.street.push_back(src.street1.get_value());
    }
    dst.city = src.city.get_value_or_default();
    dst.state_or_province = into<std::string>(src.stateorprovince);
    dst.postal_code = Util::make_strong<PostalCode>(src.postalcode.get_value_or_default());
    dst.country_code = Util::make_strong<CountryCode>(src.country.get_value_or_default());
    return dst;
}

}//namespace Fred::Registry::{anonymous}

template <>
Contact::ContactInfoReply::Data fred_unwrap(const LibFred::InfoContactOutput& src)
{
    Contact::ContactInfoReply::Data dst;
    dst.contact_handle = Util::make_strong<Contact::ContactHandle>(src.info_contact_data.handle);
    dst.contact_id = into<Contact::ContactId>(get_raw_value_from(src.info_contact_data.uuid));
    dst.contact_history_id = into<Contact::ContactHistoryId>(
            get_raw_value_from(src.info_contact_data.history_uuid));
    dst.name = into_privacy_controlled(
            into<std::string>(src.info_contact_data.name),
            src.info_contact_data.disclosename);
    dst.organization = into_privacy_controlled(
            into<std::string>(src.info_contact_data.organization),
            src.info_contact_data.discloseorganization);
    if (!src.info_contact_data.place.isnull())
    {
        dst.place = into_privacy_controlled(
                into<PlaceAddress>(src.info_contact_data.place.get_value()),
                src.info_contact_data.discloseaddress);
    }
    dst.telephone = into_privacy_controlled(
            into_optional_strong<PhoneNumber>(src.info_contact_data.telephone),
            src.info_contact_data.disclosetelephone);
    dst.fax = into_privacy_controlled(
            into_optional_strong<PhoneNumber>(src.info_contact_data.fax),
            src.info_contact_data.disclosefax);
    dst.emails = into_privacy_controlled(
            emails_into_vector_of_strong<EmailAddress>(src.info_contact_data.email),
            src.info_contact_data.discloseemail);
    dst.notify_emails = into_privacy_controlled(
            emails_into_vector_of_strong<EmailAddress>(src.info_contact_data.notifyemail),
            src.info_contact_data.disclosenotifyemail);
    dst.vat_identification_number = into_privacy_controlled(
            into_optional_strong<VatIdentificationNumber>(src.info_contact_data.vat),
            src.info_contact_data.disclosevat);
    dst.additional_identifier = into_privacy_controlled(
            into_optional_contact_additional_identifier(
                    src.info_contact_data.ssntype,
                    src.info_contact_data.ssn),
            src.info_contact_data.discloseident);
    if (0 < src.info_contact_data.addresses.count(LibFred::ContactAddressType::MAILING))
    {
        dst.mailing_address = into<Contact::ContactAddress>(static_cast<const LibFred::ContactAddress&>(
                src.info_contact_data.get_address<LibFred::ContactAddressType::MAILING>()));
    }
    if (0 < src.info_contact_data.addresses.count(LibFred::ContactAddressType::BILLING))
    {
        dst.billing_address = into<Contact::ContactAddress>(static_cast<const LibFred::ContactAddress&>(
                src.info_contact_data.get_address<LibFred::ContactAddressType::BILLING>()));
    }
    if (0 < src.info_contact_data.addresses.count(LibFred::ContactAddressType::SHIPPING))
    {
        dst.shipping_address[0] = into<Contact::ContactAddress>(static_cast<const LibFred::ContactAddress&>(
                src.info_contact_data.get_address<LibFred::ContactAddressType::SHIPPING>()));
    }
    if (0 < src.info_contact_data.addresses.count(LibFred::ContactAddressType::SHIPPING_2))
    {
        dst.shipping_address[1] = into<Contact::ContactAddress>(static_cast<const LibFred::ContactAddress&>(
                src.info_contact_data.get_address<LibFred::ContactAddressType::SHIPPING_2>()));
    }
    if (0 < src.info_contact_data.addresses.count(LibFred::ContactAddressType::SHIPPING_3))
    {
        dst.shipping_address[2] = into<Contact::ContactAddress>(static_cast<const LibFred::ContactAddress&>(
                src.info_contact_data.get_address<LibFred::ContactAddressType::SHIPPING_3>()));
    }
    dst.warning_letter = into<Contact::WarningLetter>(src.info_contact_data.warning_letter);
    dst.representative_events = into<Object::ObjectEvents>(src.info_contact_data);
    dst.sponsoring_registrar = into<Registrar::RegistrarHandle>(
            src.info_contact_data.sponsoring_registrar_handle);
    return dst;
}

template <>
Object::ObjectEvents fred_unwrap(const LibFred::InfoContactData& src)
{
    return into<Object::ObjectEvents>(src);
}

template <>
Contact::ContactState fred_unwrap(const LibFred::RegistrableObject::Contact::ContactState& src)
{
    return into<Contact::ContactState>(src);
}

template <>
Contact::ContactStateHistory fred_unwrap(const LibFred::RegistrableObject::Contact::ContactStateHistory& src)
{
    return into<Contact::ContactStateHistory>(src);
}

template <>
Contact::ContactDataHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Contact::ContactDataHistory& src)
{
    Contact::ContactDataHistoryReply::Data dst;
    dst.history = into<Contact::ContactDataHistory>(src);
    return dst;
}

template <>
Contact::ContactHandleHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Contact::ContactHandleHistory& src)
{
    Contact::ContactHandleHistoryReply::Data dst;
    dst.contact_handle = Util::make_strong<Contact::ContactHandle>(src.handle);
    for (const auto& lifetime : src.timeline)
    {
        dst.timeline.push_back(into<Contact::ContactLifetime>(lifetime));
    }
    return dst;
}

template <>
Domain::DomainState fred_unwrap(const LibFred::RegistrableObject::Domain::DomainState& src)
{
    return into<Domain::DomainState>(src);
}

template <>
Domain::DomainStateHistory fred_unwrap(const LibFred::RegistrableObject::Domain::DomainStateHistory& src)
{
    return into<Domain::DomainStateHistory>(src);
}

template <>
Domain::DomainDataHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Domain::DomainDataHistory& src)
{
    Domain::DomainDataHistoryReply::Data dst;
    dst.history = into<Domain::DomainDataHistory>(src);
    return dst;
}

template <>
Domain::FqdnHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Domain::FqdnHistory& src)
{
    Domain::FqdnHistoryReply::Data dst;
    dst.fqdn = Util::make_strong<Domain::Fqdn>(src.handle);
    for (const auto& lifetime : src.timeline)
    {
        dst.timeline.push_back(into<Domain::DomainLifetime>(lifetime));
    }
    return dst;
}

template <>
Keyset::KeysetInfoReply::Data fred_unwrap(const LibFred::InfoKeysetOutput& src)
{
    Keyset::KeysetInfoReply::Data dst;
    dst.keyset_handle = Util::make_strong<Keyset::KeysetHandle>(src.info_keyset_data.handle);
    dst.keyset_id = into<Keyset::KeysetId>(get_raw_value_from(src.info_keyset_data.uuid));
    dst.keyset_history_id = into<Keyset::KeysetHistoryId>(
            get_raw_value_from(src.info_keyset_data.history_uuid));
    dst.dns_keys = into<std::vector<Keyset::DnsKey>>(src.info_keyset_data.dns_keys);
    dst.technical_contacts = into<std::vector<Contact::ContactLightInfo>>(src.info_keyset_data.tech_contacts);
    dst.representative_events = into<Object::ObjectEvents>(src.info_keyset_data);
    dst.sponsoring_registrar = into<Registrar::RegistrarHandle>(
            src.info_keyset_data.sponsoring_registrar_handle);
    return dst;
}

template <>
Keyset::KeysetState fred_unwrap(const LibFred::RegistrableObject::Keyset::KeysetState& src)
{
    return into<Keyset::KeysetState>(src);
}

template <>
Keyset::KeysetStateHistory fred_unwrap(const LibFred::RegistrableObject::Keyset::KeysetStateHistory& src)
{
    return into<Keyset::KeysetStateHistory>(src);
}

template <>
Keyset::KeysetDataHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Keyset::KeysetDataHistory& src)
{
    Keyset::KeysetDataHistoryReply::Data dst;
    dst.history = into<Keyset::KeysetDataHistory>(src);
    return dst;
}

template <>
Keyset::KeysetHandleHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Keyset::KeysetHandleHistory& src)
{
    Keyset::KeysetHandleHistoryReply::Data dst;
    dst.keyset_handle = Util::make_strong<Keyset::KeysetHandle>(src.handle);
    for (const auto& lifetime : src.timeline)
    {
        dst.timeline.push_back(into<Keyset::KeysetLifetime>(lifetime));
    }
    return dst;
}

template <>
Nsset::NssetInfoReply::Data fred_unwrap(const LibFred::InfoNssetOutput& src)
{
    Nsset::NssetInfoReply::Data dst;
    dst.nsset_handle = Util::make_strong<Nsset::NssetHandle>(src.info_nsset_data.handle);
    dst.nsset_id = into<Nsset::NssetId>(get_raw_value_from(src.info_nsset_data.uuid));
    dst.nsset_history_id = into<Nsset::NssetHistoryId>(
            get_raw_value_from(src.info_nsset_data.history_uuid));
    dst.technical_check_level = into_optional<Nsset::TechnicalCheckLevel>(src.info_nsset_data.tech_check_level);
    dst.dns_hosts = into<std::vector<Nsset::DnsHost>>(src.info_nsset_data.dns_hosts);
    dst.technical_contacts = into<std::vector<Contact::ContactLightInfo>>(src.info_nsset_data.tech_contacts);
    dst.representative_events = into<Object::ObjectEvents>(src.info_nsset_data);
    dst.sponsoring_registrar = into<Registrar::RegistrarHandle>(
            src.info_nsset_data.sponsoring_registrar_handle);
    return dst;
}

template <>
Nsset::NssetState fred_unwrap(const LibFred::RegistrableObject::Nsset::NssetState& src)
{
    return into<Nsset::NssetState>(src);
}

template <>
Nsset::NssetStateHistory fred_unwrap(const LibFred::RegistrableObject::Nsset::NssetStateHistory& src)
{
    return into<Nsset::NssetStateHistory>(src);
}

template <>
Nsset::NssetDataHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Nsset::NssetDataHistory& src)
{
    Nsset::NssetDataHistoryReply::Data dst;
    dst.history = into<Nsset::NssetDataHistory>(src);
    return dst;
}

template <>
Nsset::NssetHandleHistoryReply::Data fred_unwrap(const LibFred::RegistrableObject::Nsset::NssetHandleHistory& src)
{
    Nsset::NssetHandleHistoryReply::Data dst;
    dst.nsset_handle = Util::make_strong<Nsset::NssetHandle>(src.handle);
    for (const auto& lifetime : src.timeline)
    {
        dst.timeline.push_back(into<Nsset::NssetLifetime>(lifetime));
    }
    return dst;
}

template <>
Registrar::RegistrarInfoReply::Data fred_unwrap(const LibFred::InfoRegistrarOutput& src)
{
    Registrar::RegistrarInfoReply::Data dst;
    dst.registrar_handle = Util::make_strong<Registrar::RegistrarHandle>(src.info_registrar_data.handle);
    dst.name = into<std::string>(src.info_registrar_data.name);
    dst.organization = into<std::string>(src.info_registrar_data.organization);
    dst.place = into_optional_place_address(src.info_registrar_data);
    dst.telephone = into_optional_strong<PhoneNumber>(src.info_registrar_data.telephone);
    dst.fax = into_optional_strong<PhoneNumber>(src.info_registrar_data.fax);
    dst.emails = emails_into_vector_of_strong<EmailAddress>(src.info_registrar_data.email);
    dst.url = into_optional_strong<Url>(src.info_registrar_data.url);
    dst.is_system_registrar = !src.info_registrar_data.system.isnull() && src.info_registrar_data.system.get_value();
    dst.company_registration_number = into_optional_strong<CompanyRegistrationNumber>(src.info_registrar_data.ico);
    dst.vat_identification_number = into_optional_strong<VatIdentificationNumber>(src.info_registrar_data.dic);
    dst.variable_symbol = into<std::string>(src.info_registrar_data.variable_symbol);
    dst.payment_memo_regex = into<std::string>(src.info_registrar_data.payment_memo_regex);
    dst.is_vat_payer = src.info_registrar_data.vat_payer;
    dst.is_internal = src.info_registrar_data.is_internal;
    return dst;
}

template <>
std::map<std::string, boost::optional<Registrar::Credit>> fred_unwrap(const LibFred::RegistrarZoneCredit& src)
{
    std::map<std::string, boost::optional<Registrar::Credit>> dst;
    for (const auto& zone : src)
    {
        if (zone.has_credit())
        {
            dst.insert({zone.get_zone_fqdn(), Util::make_strong<Registrar::Credit>(zone.get_credit().get_string())});
        }
        else
        {
            dst.insert({zone.get_zone_fqdn(), boost::none});
        }
    }
    return dst;
}

namespace {

constexpr bool is_digit(char c) { return '0' <= c && c <= '9'; }

constexpr bool is_upper_hex(char c) { return 'A' <= c && c <= 'F'; }

constexpr bool is_lower_hex(char c) { return 'a' <= c && c <= 'f'; }

constexpr unsigned char decode_hex(char c)
{
    if (is_digit(c))
    {
        return static_cast<unsigned char>(c - '0');
    }
    if (is_upper_hex(c))
    {
        return static_cast<unsigned char>(10 + c - 'A');
    }
    if (is_lower_hex(c))
    {
        return static_cast<unsigned char>(10 + c - 'a');
    }
    struct InvalidCharacter : std::exception
    {
        const char* what() const noexcept override { return "Not a hex character"; }
    };
    throw InvalidCharacter{};
}

static_assert(decode_hex('0') == 0x0);
static_assert(decode_hex('9') == 0x9);
static_assert(decode_hex('A') == 0xA);
static_assert(decode_hex('F') == 0xF);
static_assert(decode_hex('a') == 0xA);
static_assert(decode_hex('f') == 0xF);

void delimiter_requested(char c)
{
    if (c == ':')
    {
        return;
    }
    struct InvalidCharacter : std::exception
    {
        const char* what() const noexcept override { return "Not a delimiter"; }
    };
    throw InvalidCharacter{};
}

std::vector<unsigned char> hex_to_binary(const std::string& hex)
{
    std::vector<unsigned char> result;
    result.reserve((hex.size() + 1) / 3);
    enum class Decode
    {
        most_significant_half,
        low_significant_half,
        delimiter
    };
    Decode state = Decode::most_significant_half;
    std::for_each(begin(hex), end(hex), [&](char c)
    {
        switch (state)
        {
            case Decode::most_significant_half:
                result.push_back(static_cast<unsigned char>(decode_hex(c) << 4));
                state = Decode::low_significant_half;
                return;
            case Decode::low_significant_half:
                result.back() |= decode_hex(c);
                state = Decode::delimiter;
                return;
            case Decode::delimiter:
                delimiter_requested(c);
                state = Decode::most_significant_half;
                return;
        }
        struct OutOfState : std::logic_error
        {
            OutOfState() : std::logic_error{"unknown state"} { }
        };
        throw OutOfState{};
    });
    if (state != Decode::delimiter)
    {
        struct InvalidNumberOfCharacters : std::exception
        {
            const char* what() const noexcept override { return "Unexpected end of hex formatted string value"; }
        };
        throw InvalidNumberOfCharacters{};
    }
    return result;
}

Registrar::SslCertificate make_certificate(const LibFred::Registrar::EppAuth::EppAuthRecord& record)
{
    Registrar::SslCertificate certificate;
    certificate.fingerprint = hex_to_binary(record.certificate_fingerprint);
    certificate.cert_data_pem = record.cert_data_pem;
    return certificate;
}

}//namespace Fred::Registry::{anonymous}

template <>
Registrar::RegistrarEppCredentialsId fred_unwrap(const LibFred::Registrar::EppAuth::EppAuthRecordUuid& uuid)
{
    return Util::make_strong<Registrar::RegistrarEppCredentialsId>(uuid.value);
}

template <>
Registrar::GetRegistrarEppCredentialsReply::Data::Credentials fred_unwrap(const LibFred::Registrar::EppAuth::EppAuthRecord& record)
{
    Registrar::GetRegistrarEppCredentialsReply::Data::Credentials result;
    result.credentials_id = fred_unwrap(record.uuid);
    result.create_time = record.create_time;
    result.certificate = make_certificate(record);
    return result;
}

}//namespace Fred::Registry
}//namespace Fred

namespace {

template <typename Clock, typename Dur>
std::chrono::time_point<Clock, Dur> local_date_to_utc_timestamp(
        const LibFred::OperationContext& ctx,
        const boost::gregorian::date& src)
{
    using TimePoint = std::chrono::time_point<Clock, Dur>;
    const auto dbres = ctx.get_conn().exec_params(
            "SELECT $1::DATE::TIMESTAMP AT TIME ZONE tz.val AT TIME ZONE 'UTC' "
              "FROM enum_parameters tz "
             "WHERE name = 'regular_day_procedure_zone'",
            Database::QueryParams{src});
    const bool no_time_zone_configured = dbres.size() == 0;
    if (no_time_zone_configured) // use default time zone
    {
        // default time zone is UTC
        return Fred::Registry::into<TimePoint>(boost::posix_time::ptime{src});
    }
    if (dbres.size() == 1)
    {
        return static_cast<TimePoint>(dbres[0][0]);
    }
    struct UnexpectedNumberOfRows : std::exception
    {
        const char* what() const noexcept override { return "at most one row expected"; }
    };
    throw UnexpectedNumberOfRows{};
}

decltype(auto) to_expiration_timestamp(
        const LibFred::OperationContext& ctx,
        const boost::gregorian::date& src)
{
    Fred::Registry::Domain::ExpirationTimestamp result;
    result.timestamp = local_date_to_utc_timestamp<std::chrono::system_clock, std::chrono::nanoseconds>(ctx, src);
    return result;
}

}//namespace {anonymous}

using namespace Fred::Registry;

Domain::DomainInfoReply::Data Fred::Registry::fred_unwrap(
        const LibFred::OperationContext& ctx,
        const LibFred::InfoDomainOutput& src)
{
    Domain::DomainInfoReply::Data dst;
    dst.fqdn = Util::make_strong<Domain::Fqdn>(src.info_domain_data.fqdn);
    dst.domain_id = into<Domain::DomainId>(get_raw_value_from(src.info_domain_data.uuid));
    dst.domain_history_id = into<Domain::DomainHistoryId>(
            get_raw_value_from(src.info_domain_data.history_uuid));
    if (!src.info_domain_data.keyset.isnull())
    {
        dst.keyset = into<Keyset::KeysetLightInfo>(src.info_domain_data.keyset.get_value());
    }
    if (!src.info_domain_data.nsset.isnull())
    {
        dst.nsset = into<Nsset::NssetLightInfo>(src.info_domain_data.nsset.get_value());
    }
    dst.registrant = into<Contact::ContactLightInfo>(src.info_domain_data.registrant);
    dst.sponsoring_registrar = into<Registrar::RegistrarHandle>(
            src.info_domain_data.sponsoring_registrar_handle);
    dst.administrative_contacts = into<std::vector<Contact::ContactLightInfo>>(src.info_domain_data.admin_contacts);
    dst.expires_at = to_expiration_timestamp(ctx, src.info_domain_data.expiration_date);
    if (!src.info_domain_data.enum_domain_validation.isnull())
    {
        dst.validation_expires_at = to_expiration_timestamp(
                ctx,
                src.info_domain_data.enum_domain_validation.get_value().validation_expiration);
    }
    dst.representative_events = into<Object::ObjectEvents>(src.info_domain_data);
    return dst;
}
