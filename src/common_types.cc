/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/common_types.hh"
#include "src/util/struct_to_string.hh"

#include <stdexcept>
#include <utility>

namespace Fred {
namespace Registry {

std::string PlaceAddress::to_string() const
{
    return Util::StructToString().add("street", street)
                                 .add("city", city)
                                 .add("state_or_province", state_or_province)
                                 .add("postal_code", postal_code)
                                 .add("country_code", country_code)
                                 .finish();
}

}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry;

std::string Fred::Registry::to_string(OrderByDirection direction)
{
    switch (direction)
    {
        case OrderByDirection::ascending:
            return "ascending";
        case OrderByDirection::descending:
            return "descending";
    }
    throw std::runtime_error{"unknown OrderByDirection item"};
}
