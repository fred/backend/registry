/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EXCEPTIONS_HH_F299DFDF9DE2F4BDBABB2F838F4C10A3//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define EXCEPTIONS_HH_F299DFDF9DE2F4BDBABB2F838F4C10A3

#include <stdexcept>
#include <string>

namespace Fred {
namespace Registry {

struct Exception : std::runtime_error
{
    explicit Exception(const std::string& msg);
};

struct InternalServerError : Exception
{
    explicit InternalServerError(const std::string& msg);
};

struct DatabaseError : InternalServerError
{
    explicit DatabaseError(const std::string& msg);
};

}//namespace Fred::Registry
}//namespace Fred

#endif
