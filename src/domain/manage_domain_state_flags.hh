/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANAGE_DOMAIN_STATE_FLAGS_HH_840DC83E452D2D34C4FCEDD4795B6930//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define MANAGE_DOMAIN_STATE_FLAGS_HH_840DC83E452D2D34C4FCEDD4795B6930

#include "src/domain/manage_domain_state_flags_reply.hh"
#include "src/domain/manage_domain_state_flags_request.hh"

#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>


namespace Fred {
namespace Registry {
namespace Domain {

ManageDomainStateFlagsReply::Data manage_domain_state_flags(
        const ManageDomainStateFlagsRequest& request,
        const boost::optional<std::string>& by_registrar);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//MANAGE_DOMAIN_STATE_FLAGS_HH_840DC83E452D2D34C4FCEDD4795B6930
