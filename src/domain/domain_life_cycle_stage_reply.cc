/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/domain_life_cycle_stage_reply.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Domain {

std::string DomainLifeCycleStageReply::Data::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("is_delete_candidate", is_delete_candidate)
                                 .add("is_auctioned", is_auctioned)
                                 .add("blacklisted", blacklisted)
                                 .finish();
}

bool DomainLifeCycleStageReply::LessBlockId::operator()(
        const Registry::DomainBlacklist::BlockId& lhs,
        const Registry::DomainBlacklist::BlockId& rhs) const
{
    return get_raw_value_from(lhs) < get_raw_value_from(rhs);
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
