/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/update_domains_additional_notify_info.hh"
#include "src/domain/update_domains_additional_notify_info_reply.hh"
#include "src/util/into.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/db_settings.hh"

#include "util/log/log.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>
#include <map>
#include <stdexcept>
#include <tuple>
#include <utility>

namespace Fred {
namespace Registry {
namespace Domain {

bool operator<(const TimeInterval& lhs, const TimeInterval& rhs) noexcept
{
    return std::make_tuple(lhs.lower_bound, lhs.upper_bound) < std::make_tuple(rhs.lower_bound, rhs.upper_bound);
}

bool operator<(const DomainId& lhs, const DomainId& rhs) noexcept
{
    return get_raw_value_from(lhs) < get_raw_value_from(rhs);
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

namespace {

#if 0

WITH time_zone AS (
    SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'),
     dlp AS (
    SELECT id,
           valid_for_exdate_after,
           COALESCE((SELECT i.valid_for_exdate_after
                     FROM domain_lifecycle_parameters i
                     WHERE o.id < i.id
                     ORDER BY i.id
                     LIMIT 1),
                    'infinity'::TIMESTAMP) AS valid_for_exdate_before,
           '2022-09-18 22:00:00'::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val -
           expiration_dns_protection_period AS domain_exdate_from,
           '2022-09-19 22:00:00'::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val -
           expiration_dns_protection_period AS domain_exdate_to
    FROM domain_lifecycle_parameters o,
         time_zone),
     lifecycle AS (
    SELECT id, domain_exdate_from, domain_exdate_to
    FROM dlp
    WHERE valid_for_exdate_after <= domain_exdate_to AND domain_exdate_from < valid_for_exdate_before),
     state_flag AS (
    SELECT id
    FROM enum_object_states
    WHERE name = 'outzoneUnguardedWarning' AND
          get_object_type_id('domain') = ANY(types))
DELETE FROM notification.object_state_additional_contact ac
USING lifecycle lc
JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to
JOIN object_registry obr ON obr.id = d.id,
state_flag
WHERE ac.type = 'email_address'::notification.CONTACT_TYPE AND
      ac.state_flag_id = state_flag.id AND
      ac.object_state_id IS NOT NULL AND
      d.id = ac.object_id AND
      d.exdate <= ac.valid_from AND
      obr.uuid IN ();

WITH time_zone AS (
    SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'),
     dlp AS (
    SELECT id,
           valid_for_exdate_after,
           COALESCE((SELECT i.valid_for_exdate_after
                     FROM domain_lifecycle_parameters i
                     WHERE o.id < i.id
                     ORDER BY i.id
                     LIMIT 1),
                    'infinity'::TIMESTAMP) AS valid_for_exdate_before,
           '2022-10-26 22:00:00'::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val -
           expiration_dns_protection_period AS domain_exdate_from,
           '2022-10-27 22:00:00'::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val -
           expiration_dns_protection_period AS domain_exdate_to
    FROM domain_lifecycle_parameters o,
         time_zone),
     lifecycle AS (
    SELECT id, domain_exdate_from, domain_exdate_to
    FROM dlp
    WHERE valid_for_exdate_after <= domain_exdate_to AND domain_exdate_from < valid_for_exdate_before),
     state_flag AS (
    SELECT id
    FROM enum_object_states
    WHERE name = 'outzoneUnguardedWarning' AND
          get_object_type_id('domain') = ANY(types))
INSERT INTO notification.object_state_additional_contact (object_id, state_flag_id, valid_from, type, contacts)
    SELECT d.id, state_flag.id, d.exdate, 'email_address'::notification.CONTACT_TYPE,
           '{a@mail.cz,b@mail.cz,c@mail.cz}'::VARCHAR[]
    FROM lifecycle lc
    JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to
    JOIN object_registry obr ON obr.id = d.id,
         state_flag
    WHERE obr.uuid = '40b3ed2b-1911-4f29-8424-dc90c9a0df5c'::UUID
ON CONFLICT (object_id, state_flag_id, valid_from, type)
DO UPDATE SET contacts = '{a@mail.cz,b@mail.cz,c@mail.cz,d@mail.cz}'::VARCHAR[]
   WHERE notification.object_state_additional_contact.id =
         (SELECT ac.id
          FROM notification.object_state_additional_contact ac,
               lifecycle lc
          JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to
          JOIN object_registry obr ON obr.id = d.id,
               state_flag
          WHERE obr.uuid = '40b3ed2b-1911-4f29-8424-dc90c9a0df5c'::UUID AND
                ac.object_id = obr.id AND
                ac.state_flag_id = state_flag.id AND
                ac.valid_from = d.exdate AND
                ac.type = 'email_address'::notification.CONTACT_TYPE)
RETURNING id;
#endif

std::string to_sql_string(const std::chrono::system_clock::time_point& time_point)
{
    const auto time_point_usec = std::chrono::time_point_cast<std::chrono::microseconds>(time_point);
    return Fred::Registry::Util::Into<std::string>::from(time_point_usec);
}

struct DomainEmailAddresses
{
    Fred::Registry::Domain::DomainId domain_id;
    std::vector<Fred::Registry::Domain::EmailAddress> email_addresses;
};

struct DomainPhoneNumbers
{
    Fred::Registry::Domain::DomainId domain_id;
    std::vector<Fred::Registry::Domain::PhoneNumber> phone_numbers;
};

using DomainsByTime = std::map<Fred::Registry::Domain::TimeInterval, std::vector<Fred::Registry::Domain::DomainId>>;
using DomainsEmailAddressesByTime = std::map<Fred::Registry::Domain::TimeInterval, std::vector<DomainEmailAddresses>>;
using DomainsPhoneNumbersByTime = std::map<Fred::Registry::Domain::TimeInterval, std::vector<DomainPhoneNumbers>>;

auto get_domain_emails_to_delete(
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    DomainsByTime result;
    std::for_each(begin(request.domains_additional_info), end(request.domains_additional_info), [&](auto&& info)
    {
        if (info.additional_emails.empty())
        {
            result[info.event_time].emplace_back(info.domain_id);
        }
    });
    return result;
}

auto get_domain_phones_to_delete(
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    DomainsByTime result;
    std::for_each(begin(request.domains_additional_info), end(request.domains_additional_info), [&](auto&& info)
    {
        if (info.additional_phones.empty())
        {
            result[info.event_time].emplace_back(info.domain_id);
        }
    });
    return result;
}

auto get_domain_emails_to_set(
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    DomainsEmailAddressesByTime result;
    std::for_each(begin(request.domains_additional_info), end(request.domains_additional_info), [&](auto&& info)
    {
        if (!info.additional_emails.empty())
        {
            result[info.event_time].push_back({info.domain_id, info.additional_emails});
        }
    });
    return result;
}

auto get_domain_phones_to_set(
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    DomainsPhoneNumbersByTime result;
    std::for_each(begin(request.domains_additional_info), end(request.domains_additional_info), [&](auto&& info)
    {
        if (!info.additional_phones.empty())
        {
            result[info.event_time].push_back({info.domain_id, info.additional_phones});
        }
    });
    return result;
}

std::string get_lifecycle_parameter_name(Fred::Registry::Domain::LifecycleEvent event)
{
    switch (event)
    {
        case Fred::Registry::Domain::LifecycleEvent::domain_delete:
            return "expiration_registration_protection_period";
        case Fred::Registry::Domain::LifecycleEvent::domain_outzone:
            return "expiration_dns_protection_period";
    }
    throw std::runtime_error{"unknown lifecycle event"};
}

std::string get_state_flag_name(Fred::Registry::Domain::LifecycleEvent event)
{
    switch (event)
    {
        case Fred::Registry::Domain::LifecycleEvent::domain_delete:
            return "deleteWarning";
        case Fred::Registry::Domain::LifecycleEvent::domain_outzone:
            return "outzoneUnguardedWarning";
    }
    throw std::runtime_error{"unknown lifecycle event"};
}

template <typename> std::string make_contact_type();

template <>
std::string make_contact_type<DomainEmailAddresses>()
{
    return "email_address";
}

template <>
std::string make_contact_type<DomainPhoneNumbers>()
{
    return "phone_number";
}

void delete_domain_contacts(
        const LibFred::OperationContext& ctx,
        const DomainsByTime::value_type& time_domains_pair,
        const std::string& contact_type,
        Fred::Registry::Domain::LifecycleEvent event)
{
    auto params = Database::QueryParams{
            to_sql_string(time_domains_pair.first.lower_bound),
            to_sql_string(time_domains_pair.first.upper_bound),
            contact_type};
    const auto domains_set = [&](const auto& domains)
    {
        std::string sql;
        std::for_each(begin(domains), end(domains), [&](auto&& domain_id)
        {
            if (!sql.empty())
            {
                sql += ", ";
            }
            params.push_back(get_raw_value_from(domain_id));
            sql += "$" + std::to_string(params.size()) + "::UUID";
        });
        return sql;
    };
    const auto lifecycle_parameter_name = get_lifecycle_parameter_name(event);
    const auto state_flag_name = get_state_flag_name(event);
    std::string sql =
            "WITH time_zone AS ("
                "SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'), "
                 "dlp AS ("
                "SELECT id, "
                       "valid_for_exdate_after, "
                       "COALESCE((SELECT i.valid_for_exdate_after "
                                 "FROM domain_lifecycle_parameters i "
                                 "WHERE o.id < i.id "
                                 "ORDER BY i.id "
                                 "LIMIT 1), "
                                "'infinity'::TIMESTAMP) AS valid_for_exdate_before, "
                       "$1::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val - " +
                       lifecycle_parameter_name + " AS domain_exdate_from, "
                       "$2::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val - " +
                       lifecycle_parameter_name + " AS domain_exdate_to "
                "FROM domain_lifecycle_parameters o, "
                     "time_zone), "
                 "lifecycle AS ( "
                "SELECT id, domain_exdate_from, domain_exdate_to "
                "FROM dlp "
                "WHERE valid_for_exdate_after <= domain_exdate_to AND domain_exdate_from < valid_for_exdate_before), "
                 "state_flag AS ( "
                "SELECT id "
                "FROM enum_object_states "
                "WHERE name = '" + state_flag_name + "' AND "
                      "get_object_type_id('domain') = ANY(types)) "
            "DELETE FROM notification.object_state_additional_contact ac "
            "USING lifecycle lc "
            "JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to "
            "JOIN object_registry obr ON obr.id = d.id, "
                 "state_flag "
            "WHERE ac.type = $3::notification.CONTACT_TYPE AND "
                  "ac.state_flag_id = state_flag.id AND "
                  "ac.object_state_id IS NULL AND "
                  "d.id = ac.object_id AND "
                  "d.exdate <= ac.valid_from AND "
                  "obr.uuid IN (" + domains_set(time_domains_pair.second) + ")";
    ctx.get_conn().exec_params(sql, params);
}

void delete_domain_emails(
        const LibFred::OperationContext& ctx,
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    const auto to_delete = get_domain_emails_to_delete(request);
    std::for_each(begin(to_delete), end(to_delete), [&](auto&& time_domains_pair)
    {
        delete_domain_contacts(ctx, time_domains_pair, make_contact_type<DomainEmailAddresses>(), request.event);
    });
}

void delete_domain_phones(
        const LibFred::OperationContext& ctx,
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    const auto to_delete = get_domain_phones_to_delete(request);
    std::for_each(begin(to_delete), end(to_delete), [&](auto&& time_domains_pair)
    {
        delete_domain_contacts(ctx, time_domains_pair, make_contact_type<DomainPhoneNumbers>(), request.event);
    });
}

std::string make_contacts(const DomainEmailAddresses& domain, Database::QueryParams& params)
{
    std::string array;
    std::for_each(begin(domain.email_addresses), end(domain.email_addresses), [&](auto&& contact_data)
    {
        if (!array.empty())
        {
            array += ", ";
        }
        params.push_back(get_raw_value_from(contact_data));
        array += "$" + std::to_string(params.size()) + "::VARCHAR";
    });
    return "ARRAY[" + array + "]";
}

std::string make_contacts(const DomainPhoneNumbers& domain, Database::QueryParams& params)
{
    std::string array;
    std::for_each(begin(domain.phone_numbers), end(domain.phone_numbers), [&](auto&& contact_data)
    {
        if (!array.empty())
        {
            array += ", ";
        }
        params.push_back(get_raw_value_from(contact_data));
        array += "$" + std::to_string(params.size()) + "::VARCHAR";
    });
    return "ARRAY[" + array + "]";
}

template <typename ContactType>
Fred::Registry::Domain::LifecycleEvent get_event();

template <>
Fred::Registry::Domain::LifecycleEvent get_event<DomainEmailAddresses>()
{
    return Fred::Registry::Domain::LifecycleEvent::domain_outzone;
}

template <>
Fred::Registry::Domain::LifecycleEvent get_event<DomainPhoneNumbers>()
{
    return Fred::Registry::Domain::LifecycleEvent::domain_delete;
}

template <typename ContactType>
void set_domain_contacts(
        const LibFred::OperationContext& ctx,
        const std::pair<const Fred::Registry::Domain::TimeInterval, std::vector<ContactType>>& time_domains_pair)
{
    const auto common_params = Database::QueryParams{
            to_sql_string(time_domains_pair.first.lower_bound),
            to_sql_string(time_domains_pair.first.upper_bound),
            make_contact_type<ContactType>()};
    const auto lifecycle_parameter_name = get_lifecycle_parameter_name(get_event<ContactType>());
    const auto state_flag_name = get_state_flag_name(get_event<ContactType>());
    std::for_each(begin(time_domains_pair.second), end(time_domains_pair.second), [&](auto&& domain)
    {
        auto params = common_params;
        params.push_back(domain.domain_id);
        const auto contacts = make_contacts(domain, params);
        std::string sql =
                "WITH time_zone AS ("
                    "SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'), "
                     "dlp AS ("
                    "SELECT id, "
                           "valid_for_exdate_after, "
                           "COALESCE((SELECT i.valid_for_exdate_after "
                                     "FROM domain_lifecycle_parameters i "
                                     "WHERE o.id < i.id "
                                     "ORDER BY i.id "
                                     "LIMIT 1), "
                                    "'infinity'::TIMESTAMP) AS valid_for_exdate_before, "
                           "$1::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val - " +
                           lifecycle_parameter_name + " AS domain_exdate_from, "
                           "$2::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val - " +
                           lifecycle_parameter_name + " AS domain_exdate_to "
                    "FROM domain_lifecycle_parameters o, "
                         "time_zone), "
                     "lifecycle AS ( "
                    "SELECT id, domain_exdate_from, domain_exdate_to "
                    "FROM dlp "
                    "WHERE valid_for_exdate_after <= domain_exdate_to AND domain_exdate_from < valid_for_exdate_before), "
                     "state_flag AS ( "
                    "SELECT id "
                    "FROM enum_object_states "
                    "WHERE name = '" + state_flag_name + "' AND "
                          "get_object_type_id('domain') = ANY(types)) "
                "INSERT INTO notification.object_state_additional_contact "
                    "(object_id, state_flag_id, valid_from, type, contacts) "
                    "SELECT d.id, state_flag.id, d.exdate, $3::notification.CONTACT_TYPE, " + contacts + " "
                    "FROM lifecycle lc "
                    "JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to "
                    "JOIN object_registry obr ON obr.id = d.id, "
                         "state_flag "
                    "WHERE obr.uuid = $4::UUID "
                "ON CONFLICT (object_id, state_flag_id, valid_from, type) "
                "DO UPDATE SET contacts = " + contacts + " "
                   "WHERE notification.object_state_additional_contact.id = "
                         "(SELECT ac.id "
                         "FROM notification.object_state_additional_contact ac, "
                              "lifecycle lc "
                         "JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to "
                         "JOIN object_registry obr ON obr.id = d.id, "
                              "state_flag "
                         "WHERE obr.uuid = $4::UUID AND "
                               "ac.object_id = obr.id AND "
                               "ac.state_flag_id = state_flag.id AND "
                               "ac.valid_from = d.exdate AND "
                               "ac.type = $3::notification.CONTACT_TYPE) "
                "RETURNING id";
        ctx.get_conn().exec_params(sql, params);
    });
}

struct DomainDoesNotExist : std::exception
{
    const char* what() const noexcept override { return "domain does not exist"; }
};

void domains_lock_for_update(
        const LibFred::OperationContext& ctx,
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    std::set<Fred::Registry::Domain::DomainId> domains;
    Database::QueryParams params;
    std::string sql_domains;
    std::for_each(begin(request.domains_additional_info), end(request.domains_additional_info), [&](auto&& info)
    {
        if (domains.insert(info.domain_id).second)
        {
            if (!sql_domains.empty())
            {
                sql_domains += ", ";
            }
            params.push_back(info.domain_id);
            sql_domains += "$" + std::to_string(params.size()) + "::UUID";
        }
    });
    const auto sql =
            "SELECT 0 "
            "FROM object_registry "
            "WHERE uuid IN (" + sql_domains + ") AND "
                  "type = get_object_type_id('domain') AND "
                  "erdate IS NULL "
            "FOR UPDATE";
    const auto all_domains_exist = ctx.get_conn().exec_params(sql, params).size() == domains.size();
    if (!all_domains_exist)
    {
        throw DomainDoesNotExist{};
    }
}

void set_domains_emails(
        const LibFred::OperationContext& ctx,
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    const auto emails_to_set = get_domain_emails_to_set(request);
    std::for_each(begin(emails_to_set), end(emails_to_set), [&](auto&& time_domains_pair)
    {
        set_domain_contacts(ctx, time_domains_pair);
    });
}

void set_domains_phones(
        const LibFred::OperationContext& ctx,
        const Fred::Registry::Domain::UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    const auto phones_to_set = get_domain_phones_to_set(request);
    std::for_each(begin(phones_to_set), end(phones_to_set), [&](auto&& time_domains_pair)
    {
        set_domain_contacts(ctx, time_domains_pair);
    });
}

}//namespace {anonymous}

using namespace Fred::Registry::Domain;

void Fred::Registry::Domain::update_domains_additional_notify_info(
        const UpdateDomainsAdditionalNotifyInfoRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        domains_lock_for_update(ctx, request);
        delete_domain_emails(ctx, request);
        delete_domain_phones(ctx, request);
        set_domains_emails(ctx, request);
        set_domains_phones(ctx, request);
        ctx.commit_transaction();
    }
    catch (const DomainDoesNotExist&)
    {
        throw UpdateDomainsAdditionalNotifyInfoReply::Exception::DomainDoesNotExist{request};
    }
    catch (const UpdateDomainsAdditionalNotifyInfoReply::Exception::InvalidDomainContactInfo& e)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
