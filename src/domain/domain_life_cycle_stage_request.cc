/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/domain_life_cycle_stage_request.hh"
#include "src/util/struct_to_string.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

std::string DomainLifeCycleStageRequest::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
