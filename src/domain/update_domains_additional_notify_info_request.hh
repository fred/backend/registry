/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_REQUEST_HH_D730834638F86F2AABB4C36B48796AEC//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_REQUEST_HH_D730834638F86F2AABB4C36B48796AEC

#include "src/domain/get_domains_notify_info.hh"
#include "src/util/printable.hh"

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct UpdateDomainsAdditionalNotifyInfoRequest : Util::Printable<UpdateDomainsAdditionalNotifyInfoRequest>
{
    struct DomainAdditionalInfo : Util::Printable<DomainAdditionalInfo>
    {
        DomainId domain_id;
        TimeInterval event_time;
        std::vector<EmailAddress> additional_emails;
        std::vector<PhoneNumber> additional_phones;
        std::string to_string() const;
    };
    LifecycleEvent event;
    std::vector<DomainAdditionalInfo> domains_additional_info;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_REQUEST_HH_D730834638F86F2AABB4C36B48796AEC
