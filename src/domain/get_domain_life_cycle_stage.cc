/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/get_domain_life_cycle_stage.hh"

#include "src/domain/domain_common_types.hh"
#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/util/into.hh"

#include "libfred/opcontext.hh"
#include "libfred/registrable_object/domain/check_domain.hh"
#include "util/db/db_exceptions.hh"
#include "util/log/log.hh"

#include <boost/uuid/uuid.hpp>

#if 0
WITH t AS
(
    SELECT data.time AT TIME ZONE 'UTC' AS time,
           DATE_TRUNC('DAY', data.time AT TIME ZONE data.tz) AT TIME ZONE data.tz AT TIME ZONE 'UTC' AS day_begin,
           (DATE_TRUNC('DAY', data.time AT TIME ZONE data.tz) + '1DAY'::INTERVAL) AT TIME ZONE data.tz AT TIME ZONE 'UTC' AS day_end
      FROM (SELECT '2019-06-15 18:20:05.180832'::TIMESTAMP AT TIME ZONE 'UTC' AS time,
                   'Europe/Prague' AS tz) data
)
SELECT CASE WHEN os.id IS NULL AND (d.erdate IS NULL OR t.time < d.erdate) OR -- still registered or deleted in the future
                 os.id IS NOT NULL AND t.day_end <= d.erdate -- deleteCandidate but deleted tomorrow or later
            THEN d.uuid
            ELSE NULL
       END,
       os.id IS NOT NULL, -- is deleteCandidate
       EXISTS (SELECT FROM domain_blacklist
                WHERE ((NOT is_regex AND LOWER('dsgv93xvf6gs1s9a'::TEXT) = fqdn)
                       OR (is_regex AND 'dsgv93xvf6gs1s9a'::TEXT ~* fqdn))
                  AND valid_from <= t.time
                  AND (valid_to IS NULL OR t.time < valid_to))
  FROM t
 CROSS JOIN object_registry d
  LEFT JOIN object_state os ON os.object_id = d.id AND
                               os.state_id = (SELECT id FROM enum_object_states eos
                                               WHERE eos.name = 'deleteCandidate') AND
                               ((d.erdate IS NOT NULL AND                            -- deleted domain
                                os.valid_from < d.erdate AND                         -- deleteCandidate set at the
                                (os.valid_to IS NULL OR d.erdate <= os.valid_to)) OR -- time of deletion
                               (d.erdate IS NULL AND                                 -- registered domain
                                os.valid_from <= t.time AND                          -- deleteCandidate is set right
                                (os.valid_to IS NULL OR t.time < os.valid_to)))      -- "now" (t.time)
 WHERE d.type = get_object_type_id('domain') AND
       d.name = LOWER('dsgv93xvf6gs1s9a.cz') AND
       d.crdate <= t.time AND -- domain create before or at the 'time'
       (d.erdate IS NULL OR t.day_begin <= d.erdate) -- exclude domains deleted yesterday and earlier
 ORDER BY d.crdate DESC
 LIMIT 1;
#endif

namespace {

std::string make_time_part(
        const boost::optional<Fred::Registry::Domain::DomainLifeCycleStageRequest::TimePoint>& time,
        Database::QueryParams& params)
{
    if (time == boost::none)
    {
        return "NOW()";
    }
    params.push_back(Fred::Registry::Util::Into<std::string>::from(*time));
    return "$" + std::to_string(params.size()) + "::TIMESTAMP AT TIME ZONE 'UTC'";
}

std::string get_registry_time_zone_sql(const Database::QueryParams&)
{
    return "(SELECT COALESCE((SELECT val FROM enum_parameters "
                                       "WHERE name = 'regular_day_procedure_zone'), "
                            "'UTC'))";
}

std::string make_time_zone_part(Database::QueryParams& params)
{
    return get_registry_time_zone_sql(params);
}

std::string make_domain_name_part(
        const Fred::Registry::Domain::Fqdn& fqdn,
        Database::QueryParams& params)
{
    params.emplace_back(get_raw_value_from(fqdn));
    return "LOWER($" + std::to_string(params.size()) + "::TEXT)";
}

auto make_query(
        const Fred::Registry::Domain::DomainLifeCycleStageRequest& request,
        Database::QueryParams& params)
{
    params.clear();
    return "WITH t AS "
           "("
               "SELECT data.time AT TIME ZONE 'UTC' AS time, "
                      "DATE_TRUNC('DAY', data.time AT TIME ZONE data.tz) AT TIME ZONE data.tz "
                                                                        "AT TIME ZONE 'UTC' AS day_begin, "
                      "(DATE_TRUNC('DAY', data.time AT TIME ZONE data.tz) + '1DAY'::INTERVAL) AT TIME ZONE data.tz "
                                                                        "AT TIME ZONE 'UTC' AS day_end "
                 "FROM (SELECT " + make_time_part(request.time, params) + " AS time, "
                                 + make_time_zone_part(params) + " AS tz) data"
           ") "
           "SELECT CASE WHEN os.id IS NULL AND (d.erdate IS NULL OR t.time < d.erdate) OR "
                            "os.id IS NOT NULL AND t.day_end <= d.erdate "
                       "THEN d.uuid "
                       "ELSE NULL "
                  "END, "
                  "os.id IS NOT NULL, "
                  "EXISTS (SELECT FROM domain_blacklist "
                           "WHERE ((NOT is_regex AND LOWER(d.name) = fqdn) "
                                  "OR (is_regex AND d.name ~* fqdn)) "
                             "AND valid_from <= t.time "
                             "AND (valid_to IS NULL OR t.time < valid_to)) "
             "FROM t "
       "CROSS JOIN object_registry d "
        "LEFT JOIN object_state os ON os.object_id = d.id AND "
                                     "os.state_id = (SELECT id FROM enum_object_states eos "
                                                     "WHERE eos.name = 'deleteCandidate') AND "
                                     "((d.erdate IS NOT NULL AND "
                                       "os.valid_from < d.erdate AND "
                                       "(os.valid_to IS NULL OR d.erdate <= os.valid_to)) OR "
                                      "(d.erdate IS NULL AND "
                                       "os.valid_from <= t.time AND "
                                       "(os.valid_to IS NULL OR t.time < os.valid_to))) "
            "WHERE d.type = get_object_type_id('domain') AND "
                  "d.name = " + make_domain_name_part(request.fqdn, params) + " AND "
                  "d.crdate <= t.time AND "
                  "(d.erdate IS NULL OR t.day_begin <= d.erdate) "
         "ORDER BY d.crdate DESC "
            "LIMIT 1";
}

auto get_domain_life_cycle_stage_from_db(
        const LibFred::OperationContext& ctx,
        const Fred::Registry::Domain::DomainLifeCycleStageRequest& request)
{
    Database::QueryParams params;
    const auto sql = make_query(request, params);
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    Fred::Registry::Domain::DomainLifeCycleStageReply::Data result;
    const auto get_is_auctioned = [&ctx, &request]()
    {
        return (request.time == boost::none) &&
               LibFred::CheckDomain{get_raw_value_from(request.fqdn)}.is_auctioned(ctx);
    };
    const auto get_blacklisted = [&ctx, &request]()
    {
        const auto blacklisted_by = LibFred::CheckDomain{get_raw_value_from(request.fqdn)}
                .blacklisted_by(ctx, request.time);
        Fred::Registry::Domain::DomainLifeCycleStageReply::Blacklist result;
        std::transform(
                begin(blacklisted_by),
                end(blacklisted_by),
                std::inserter(result, end(result)),
                [](auto&& block)
                {
                    return Fred::Registry::Util::make_strong<Fred::Registry::DomainBlacklist::BlockId>(block);
                });
        return result;
    };
    if (dbres.size() <= 0)
    {
        result.domain_id = boost::none;
        result.is_delete_candidate = false;
        result.is_auctioned = get_is_auctioned();
        result.blacklisted = get_blacklisted();

    }
    else if (dbres.size() == 1)
    {
        if (dbres[0][0].isnull())
        {
            result.domain_id = boost::none;
            result.is_delete_candidate = static_cast<bool>(dbres[0][1]);
            result.is_auctioned = get_is_auctioned();
        }
        else
        {
            result.domain_id = Fred::Registry::Util::make_strong<Fred::Registry::Domain::DomainId>(
                    boost::uuids::string_generator{}(static_cast<std::string>(dbres[0][0])));
            result.is_delete_candidate = false;
            result.is_auctioned = false;
        }
        const auto is_blacklisted = static_cast<bool>(dbres[0][2]);
        if (is_blacklisted)
        {
            result.blacklisted = get_blacklisted();
        }
    }
    else
    {
        throw std::runtime_error{"unexpected number of rows"};
    }
    return result;
}

}//namespace {anonymous}

using namespace Fred::Registry::Domain;

DomainLifeCycleStageReply::Data Fred::Registry::Domain::get_domain_life_cycle_stage(
        const DomainLifeCycleStageRequest& request)
{
    auto result = DomainLifeCycleStageReply::Data{};
    try
    {
        LibFred::OperationContextCreator ctx;
        result = get_domain_life_cycle_stage_from_db(ctx, request);
        ctx.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"exception caught: "} + e.what());
        throw InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        FREDLOG_WARNING("unknown exception caught");
        throw InternalServerError("unexpected unknown exception");
    }
    return result;
}
