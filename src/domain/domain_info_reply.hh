/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DOMAIN_INFO_REPLY_HH_0E8302018D5E41A1A4A60FD3F4D73AA8
#define DOMAIN_INFO_REPLY_HH_0E8302018D5E41A1A4A60FD3F4D73AA8

#include "src/contact/contact_common_types.hh"
#include "src/domain/domain_common_types.hh"
#include "src/domain/domain_info_request.hh"
#include "src/keyset/keyset_common_types.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/object/object_common_types.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"
#include "src/exceptions.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>
  
#include <cstdint>
#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct DomainInfoReply
{
    struct Data : Util::Printable<Data>
    {
        Fqdn fqdn;
        DomainId domain_id;
        DomainHistoryId domain_history_id;
        boost::optional<Nsset::NssetLightInfo> nsset;
        boost::optional<Keyset::KeysetLightInfo> keyset;
        Contact::ContactLightInfo registrant;
        Registrar::RegistrarHandle sponsoring_registrar;
        std::vector<Contact::ContactLightInfo> administrative_contacts;
        ExpirationTimestamp expires_at;
        Object::ObjectEvents representative_events;
        DomainLifeCycleMilestone expiration_warning_scheduled_at;
        DomainLifeCycleMilestone outzone_unguarded_warning_scheduled_at;
        DomainLifeCycleMilestone outzone_scheduled_at;
        DomainLifeCycleMilestone delete_warning_scheduled_at;
        DomainLifeCycleMilestone delete_candidate_scheduled_at;
        boost::optional<ExpirationTimestamp> validation_expires_at;
        boost::optional<DomainLifeCycleTimestamp> outzone_at;
        boost::optional<DomainLifeCycleTimestamp> delete_candidate_at;
        std::string to_string() const;
    };
    struct Exception : Util::Printable<Exception>
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const DomainInfoRequest& src);
        };
        struct InvalidData : Registry::Exception, Util::Printable<InvalidData>
        {
            explicit InvalidData(const DomainInfoRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
            std::string to_string() const;
        };
        explicit Exception(InvalidData);
        explicit Exception(DomainDoesNotExist);
        using Reasons = boost::variant<InvalidData, DomainDoesNotExist>;
        Reasons reasons;
        std::string to_string() const;
    };
    using Result = boost::variant<Data, Exception::InvalidData, Exception::DomainDoesNotExist>;
};

struct BatchDomainInfoReply
{
    struct Error : Util::Printable<Error>
    {
        explicit Error(BatchDomainInfoRequest::Request, DomainInfoReply::Exception);
        BatchDomainInfoRequest::Request request;
        DomainInfoReply::Exception exception;
        std::string to_string() const;
    };
    using DataOrError = boost::variant<DomainInfoReply::Data, Error>;
    struct Data : Util::Printable<Data>
    {
        struct BatchReply : Util::Printable<BatchReply>
        {
            DataOrError data_or_error;
            std::string to_string() const;
        };
        std::vector<BatchReply> replies;
        std::string to_string() const;
    };
};

struct DomainIdReply
{
    struct Data : Util::Printable<Data>
    {
        DomainId domain_id;
        DomainHistoryId domain_history_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const DomainIdRequest& src);
        };
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
