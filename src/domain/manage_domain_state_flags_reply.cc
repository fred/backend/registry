/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/manage_domain_state_flags_reply.hh"
#include "src/util/struct_to_string.hh"

#include <boost/uuid/uuid_io.hpp>

namespace Fred {
namespace Registry {
namespace Domain {

std::string ManageDomainStateFlagsReply::Data::DomainStateFlagRequestInfo::to_string() const
{
    return Util::StructToString().add("request_id", request_id)
                                 .add("domain_id", domain_id)
                                 .add("state_flag", state_flag)
                                 .finish();
}

std::string ManageDomainStateFlagsReply::Data::to_string() const
{
    return Util::StructToString().add("state_flag_requests_created", state_flag_requests_created)
                                 .finish();
}

ManageDomainStateFlagsReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const ManageDomainStateFlagsRequest::Domain& src)
    : Registry::Exception{"domain " + src.to_string() + " does not exist"}
{ }

ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist::DomainStateFlagRequestDoesNotExist(const DomainStateFlagRequest& src)
    : Registry::Exception{"domain state flag request " + src.to_string() + " does not exist"}
{ }

ManageDomainStateFlagsReply::Exception::DomainStateFlagDoesNotExist::DomainStateFlagDoesNotExist(const std::string& src)
    : Registry::Exception{"domain state flag " + src + " does not exist"}
{ }

ManageDomainStateFlagsReply::Exception::DomainHistoryIdMismatch::DomainHistoryIdMismatch(const ManageDomainStateFlagsRequest::Domain& src)
    : Registry::Exception{"domain history id " + src.to_string() + " mismatches"}
{ }

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
