/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/domain_state_history.hh"

#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/domain/get_domain_state_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Domain {

DomainStateHistoryRequest::DomainStateHistoryRequest(
        const DomainId& _domain_id,
        const DomainHistoryInterval& _history)
    : domain_id(_domain_id),
      history(_history)
{
}

std::string DomainStateHistoryRequest::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("history", history)
                                 .finish();
}

std::string DomainStateHistory::to_string() const
{
    return Util::StructToString().add("flags_names", flags_names)
                                 .add("timeline", timeline)
                                 .finish();
}

std::string DomainStateHistory::Record::to_string() const
{
    return Util::StructToString().add("valid_from", valid_from)
                                 .add("presents", presents)
                                 .finish();
}

std::string DomainStateHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("history", history)
                                 .finish();
}

DomainStateHistoryReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const DomainStateHistoryRequest& src)
    : Registry::Exception("domain " + src.to_string() + " does not exist")
{
}

DomainStateHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const DomainStateHistoryRequest& src)
    : Registry::Exception("invalid domain history interval " + src.to_string())
{
}

DomainStateHistoryReply::Data domain_state_history(const DomainStateHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetDomainStateHistoryByUuid = LibFred::RegistrableObject::Domain::GetDomainStateHistoryByUuid;
        DomainStateHistoryReply::Data result;
        result.history = fred_unwrap(
                GetDomainStateHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::domain>(
                                get_raw_value_from(request.domain_id)))
                    .exec(ctx, fred_wrap(request.history)));
        result.domain_id = request.domain_id;
        return result;
    }
    catch (const LibFred::RegistrableObject::Domain::GetDomainStateHistoryByUuid::DoesNotExist&)
    {
        throw DomainStateHistoryReply::Exception::DomainDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Domain::GetDomainStateHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw DomainStateHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
