/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/list_domains_by_nsset_request.hh"

#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Domain {

ListDomainsByNssetRequest::OrderBy::OrderBy(OrderByField field, OrderByDirection direction) noexcept
    : field{field},
      direction{direction}
{ }

std::string ListDomainsByNssetRequest::OrderBy::to_string() const
{
    return Util::StructToString().add("field", Domain::to_string(field))
                                 .add("direction", Registry::to_string(direction))
                                 .finish();
}

std::string ListDomainsByNssetRequest::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("pagination", pagination)
                                 .add("order_by", order_by)
                                 .add("aggregate_entire_history", aggregate_entire_history)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::Domain;

std::string Fred::Registry::Domain::to_string(ListDomainsByNssetRequest::OrderByField field)
{
    switch (field)
    {
        case ListDomainsByNssetRequest::OrderByField::unspecified:
            return "unspecified";
        case ListDomainsByNssetRequest::OrderByField::exdate:
            return "exdate";
        case ListDomainsByNssetRequest::OrderByField::crdate:
            return "crdate";
        case ListDomainsByNssetRequest::OrderByField::fqdn:
            return "fqdn";
        case ListDomainsByNssetRequest::OrderByField::sponsoring_registrar_handle:
            return "sponsoring_registrar_handle";
    }
    throw std::runtime_error{"unknown ListDomainsByNssetRequest::OrderByField item"};
}
