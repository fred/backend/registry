/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/manage_domain_state_flags_request.hh"
#include "src/util/struct_to_string.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

namespace {

std::string valid_from_to_string(const boost::optional<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>>& t)
{
    if (t == boost::none)
    {
        return "now";
    }
    return "";
}

std::string valid_to_to_string(const boost::optional<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>>& t)
{
    if (t == boost::none)
    {
        return "unlimited";
    }
    return "";
}

} // namespace Fred::Registry::Domain::{anonymous}

std::string ManageDomainStateFlagsRequest::Domain::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("domain_history_id", domain_history_id)
                                 .finish();
}

std::string ManageDomainStateFlagsRequest::Validity::to_string() const
{
    return Util::StructToString().add("valid_from", valid_from_to_string(valid_from))
                                 .add("valid_to", valid_to_to_string(valid_to))
                                 .finish();
}

std::string ManageDomainStateFlagsRequest::ToCreate::to_string() const
{
    return Util::StructToString().add("state_flags", state_flags)
                                 .add("domains", domains)
                                 .finish();
}

std::string ManageDomainStateFlagsRequest::to_string() const
{
    return Util::StructToString().add("state_flag_requests_to_create", state_flag_requests_to_create)
                                 .add("state_flag_requests_to_close", state_flag_requests_to_close)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
