/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/fqdn_history.hh"

#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/domain/get_fqdn_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Domain {

FqdnHistoryRequest::FqdnHistoryRequest(const Fqdn& _fqdn)
    : fqdn(_fqdn)
{ }

std::string FqdnHistoryRequest::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .finish();
}

std::string DomainLifetime::TimeSpec::to_string() const
{
    return Util::StructToString().add("domain_history_id", domain_history_id)
                                 .add("timestamp", timestamp)
                                 .finish();
}

std::string DomainLifetime::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("begin", begin)
                                 .add("end", end)
                                 .finish();
}

std::string FqdnHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .add("timeline", timeline)
                                 .finish();
}

FqdnHistoryReply::Data fqdn_history(const FqdnHistoryRequest& request)
{
    try
    {
        using GetFqdnHistory = LibFred::RegistrableObject::Domain::GetFqdnHistory;
        LibFred::OperationContextCreator ctx;
        return fred_unwrap(GetFqdnHistory(get_raw_value_from(request.fqdn)).exec(ctx));
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
