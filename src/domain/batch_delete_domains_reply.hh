/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BATCH_DELETE_DOMAINS_REPLY_HH_5C31C8A395EF0662AD355FA1020EFCA9//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define BATCH_DELETE_DOMAINS_REPLY_HH_5C31C8A395EF0662AD355FA1020EFCA9

#include "src/contact/contact_common_types.hh"
#include "src/domain/domain_common_types.hh"
#include "src/util/printable.hh"

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct BatchDeleteDomainsReply
{
    struct NotDeleted : Util::Printable<NotDeleted>
    {
        enum class Reason
        {
            undefined, // first item in enum should not be from the set of useful values
            domain_blocked,
            domain_does_not_exist,
            domain_history_id_mismatch,
            insufficient_permissions // registrar (configured in backend) does not have permissions to perform requested operation
        };
        DomainId domain_id;
        Reason reason;
        std::string to_string() const;
    };
    struct Data : Util::Printable<Data>
    {
        std::vector<NotDeleted> not_deleted_domains;
        std::string to_string() const;
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//BATCH_DELETE_DOMAINS_REPLY_HH_5C31C8A395EF0662AD355FA1020EFCA9
