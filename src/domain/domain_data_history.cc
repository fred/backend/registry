/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/domain_data_history.hh"

#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"

#include "libfred/opcontext.hh"
#include "libfred/registrable_object/domain/get_domain_data_history.hh"
#include "libfred/registrable_object/uuid.hh"
#include "util/db/db_exceptions.hh"


namespace Fred {
namespace Registry {
namespace Domain {

DomainDataHistoryRequest::DomainDataHistoryRequest(
        const DomainId& _domain_id,
        const DomainHistoryInterval& _history)
    : domain_id(_domain_id),
      history(_history)
{
}

std::string DomainDataHistoryRequest::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("history", history)
                                 .finish();
}

std::string DomainDataHistory::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("timeline", timeline)
                                 .add("valid_to", valid_to)
                                 .finish();
}

std::string DomainDataHistory::Record::to_string() const
{
    return Util::StructToString().add("domain_history_id", domain_history_id)
                                 .add("valid_from", valid_from)
                                 .add("log_entry_id", log_entry_id)
                                 .finish();
}

std::string DomainDataHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("history", history).finish();
}

DomainDataHistoryReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const DomainDataHistoryRequest& src)
    : Registry::Exception("domain " + src.to_string() + " does not exist")
{
}

DomainDataHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const DomainDataHistoryRequest& src)
    : Registry::Exception("invalid domain history interval " + src.to_string())
{
}

DomainDataHistoryReply::Data domain_data_history(const DomainDataHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetDomainDataHistoryByUuid = LibFred::RegistrableObject::Domain::GetDomainDataHistoryByUuid;
        return fred_unwrap(
                GetDomainDataHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::domain>(
                                get_raw_value_from(request.domain_id)))
                    .exec(ctx, fred_wrap(request.history)));
    }
    catch (const LibFred::RegistrableObject::Domain::GetDomainDataHistoryByUuid::DoesNotExist&)
    {
        throw DomainDataHistoryReply::Exception::DomainDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Domain::GetDomainDataHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw DomainDataHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
    throw DomainDataHistoryReply::Exception::DomainDoesNotExist(request);
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
