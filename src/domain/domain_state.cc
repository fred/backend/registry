/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/domain_state.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/domain/get_domain_state.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Domain {

std::string DomainStateRequest::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .finish();
}

std::string DomainState::to_string() const
{
    if (flag_presents.empty())
    {
        return "{ }";
    }
    std::string buffer;
    for (const auto& presents : flag_presents)
    {
        if (!buffer.empty())
        {
            buffer += ", ";
        }
        buffer += "'" + presents.first + "': " + (presents.second ? "presents" : "absents");
    }
    return "{ " + buffer + " }";
}

std::string DomainStateReply::Data::to_string() const
{
    return Util::StructToString().add("state", state).finish();
}

DomainStateReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const DomainStateRequest& src)
    : Registry::Exception("domain " + src.to_string() + " does not exist")
{
}

DomainStateReply::Data domain_state(const DomainStateRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        DomainStateReply::Data result;
        const auto domain_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::domain>(
                Util::get_raw_value_from(request.domain_id));
        result.state = fred_unwrap(LibFred::RegistrableObject::Domain::GetDomainStateByUuid(domain_uuid).exec(ctx));
        result.domain_id = request.domain_id;
        return result;
    }
    catch (const LibFred::RegistrableObject::Domain::GetDomainStateByFqdn::DoesNotExist& e)
    {
        throw DomainStateReply::Exception::DomainDoesNotExist(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
