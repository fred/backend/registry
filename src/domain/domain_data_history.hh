/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAIN_DATA_HISTORY_HH_FC537CC04CD846AC9DEABCC812D5186B
#define DOMAIN_DATA_HISTORY_HH_FC537CC04CD846AC9DEABCC812D5186B

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/domain/domain_common_types.hh"
#include "src/domain/domain_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <map>
#include <string>
#include <vector>


namespace Fred {
namespace Registry {
namespace Domain {

struct DomainDataHistoryRequest : Util::Printable<DomainDataHistoryRequest>
{
    DomainDataHistoryRequest(
            const DomainId& domain_id,
            const DomainHistoryInterval& history);
    DomainId domain_id;
    DomainHistoryInterval history;
    std::string to_string() const;
};

struct DomainDataHistory : Util::Printable<DomainDataHistory>
{
    using TimePoint = DomainHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        DomainHistoryId domain_history_id;
        TimePoint valid_from;
        LogEntryId log_entry_id;
        std::string to_string() const;
    };
    DomainId domain_id;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct DomainDataHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        DomainDataHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const DomainDataHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const DomainDataHistoryRequest& request);
        };
    };
};

DomainDataHistoryReply::Data domain_data_history(const DomainDataHistoryRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
