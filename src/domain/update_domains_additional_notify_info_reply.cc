/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/update_domains_additional_notify_info_reply.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Domain {

UpdateDomainsAdditionalNotifyInfoReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const UpdateDomainsAdditionalNotifyInfoRequest&)
    : Registry::Exception{"domain does not exist"}
{}

UpdateDomainsAdditionalNotifyInfoReply::Exception::InvalidDomainContactInfo::InvalidDomainContactInfo(std::string invalid_value)
    : Registry::Exception{"invalid contact"},
      invalid_value{std::move(invalid_value)}
{}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
