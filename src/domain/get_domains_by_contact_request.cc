/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/get_domains_by_contact_request.hh"
#include "src/util/struct_to_string.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

std::string GetDomainsByContactRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("pagination", pagination)
                                 .add("order_by", order_by)
                                 .add("fqdn_filter", fqdn_filter)
                                 .add("include_holder", include_holder)
                                 .add("include_admin_contact", include_admin_contact)
                                 .add("include_keyset_tech_contact", include_keyset_tech_contact)
                                 .add("include_nsset_tech_contact", include_nsset_tech_contact)
                                 .add("include_deleted_domains", include_deleted_domains)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
