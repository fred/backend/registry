/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/list_domains_by_keyset_request.hh"

#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Domain {

ListDomainsByKeysetRequest::OrderBy::OrderBy(OrderByField field, OrderByDirection direction) noexcept
    : field{field},
      direction{direction}
{ }

std::string ListDomainsByKeysetRequest::OrderBy::to_string() const
{
    return Util::StructToString().add("field", Domain::to_string(field))
                                 .add("direction", Registry::to_string(direction))
                                 .finish();
}

std::string ListDomainsByKeysetRequest::to_string() const
{
    return Util::StructToString().add("keyset_id", keyset_id)
                                 .add("pagination", pagination)
                                 .add("order_by", order_by)
                                 .add("aggregate_entire_history", aggregate_entire_history)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::Domain;

std::string Fred::Registry::Domain::to_string(ListDomainsByKeysetRequest::OrderByField field)
{
    switch (field)
    {
        case ListDomainsByKeysetRequest::OrderByField::unspecified:
            return "unspecified";
        case ListDomainsByKeysetRequest::OrderByField::exdate:
            return "exdate";
        case ListDomainsByKeysetRequest::OrderByField::crdate:
            return "crdate";
        case ListDomainsByKeysetRequest::OrderByField::fqdn:
            return "fqdn";
        case ListDomainsByKeysetRequest::OrderByField::sponsoring_registrar_handle:
            return "sponsoring_registrar_handle";
    }
    throw std::runtime_error{"unknown ListDomainsByKeysetRequest::OrderByField item"};
}
