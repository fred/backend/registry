/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef FQDN_HISTORY_HH_22542493BCB843FD9DBD62F81990B33D
#define FQDN_HISTORY_HH_22542493BCB843FD9DBD62F81990B33D

#include "src/domain/domain_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <string>
#include <vector>


namespace Fred {
namespace Registry {
namespace Domain {

struct FqdnHistoryRequest : Util::Printable<FqdnHistoryRequest>
{
    explicit FqdnHistoryRequest(const Fqdn& fqdn);
    Fqdn fqdn;
    std::string to_string() const;
};

struct DomainLifetime : Util::Printable<DomainLifetime>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    struct TimeSpec : Util::Printable<TimeSpec>
    {
        DomainHistoryId domain_history_id;
        TimePoint timestamp;
        std::string to_string() const;
    };
    DomainId domain_id;
    TimeSpec begin;
    boost::optional<TimeSpec> end;
    std::string to_string() const;
};

struct FqdnHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        Fqdn fqdn;
        std::vector<DomainLifetime> timeline;
        std::string to_string() const;
    };
};

FqdnHistoryReply::Data fqdn_history(const FqdnHistoryRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
