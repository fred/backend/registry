/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DOMAINS_BY_CONTACT_REPLY_HH_CBC16CB493FE15A64CAE80745F90154D//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_DOMAINS_BY_CONTACT_REPLY_HH_CBC16CB493FE15A64CAE80745F90154D

#include "src/domain/domain_common_types.hh"
#include "src/domain/get_domains_by_contact_request.hh"
#include "src/pagination_reply.hh"
#include "src/util/printable.hh"
#include "src/exceptions.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct GetDomainsByContactReply
{
    struct Data : Util::Printable<Data>
    {
        struct Domain : Util::Printable<Domain>
        {
            DomainLightInfo domain;
            DomainHistoryId domain_history_id;
            bool is_holder;
            bool is_admin_contact;
            bool is_keyset_tech_contact;
            bool is_nsset_tech_contact;
            bool is_deleted;
            std::string to_string() const;
        };
        std::vector<Domain> domains;
        boost::optional<PaginationReply> pagination;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const GetDomainsByContactRequest& src);
        };
        struct InvalidOrderBy : Registry::Exception
        {
            explicit InvalidOrderBy(const GetDomainsByContactRequest& src);
        };
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//GET_DOMAINS_BY_CONTACT_REPLY_HH_CBC16CB493FE15A64CAE80745F90154D
