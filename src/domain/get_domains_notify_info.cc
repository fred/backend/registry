/*
 * Copyright (C) 2022-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/common_types.hh"
#include "src/domain/get_domains_notify_info.hh"
#include "src/util/into.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "util/db/db_exceptions.hh"

#include "util/log/log.hh"

#include <cstdint>
#include <limits>
#include <stdexcept>
#include <utility>

namespace {

#if 0

WITH time_zone AS (
    SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'),
     dlp AS (
    SELECT o.id,
           o.valid_for_exdate_after,
           COALESCE((SELECT i.valid_for_exdate_after
                     FROM domain_lifecycle_parameters i
                     WHERE o.id < i.id
                     ORDER BY i.id
                     LIMIT 1),
                    'infinity'::TIMESTAMP) AS valid_for_exdate_before,
           '2022-09-18 22:00:00'::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val -
           o.expiration_dns_protection_period AS domain_exdate_from,
           '2022-09-19 22:00:00'::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val -
           o.expiration_dns_protection_period AS domain_exdate_to
    FROM domain_lifecycle_parameters o,
         time_zone),
     lifecycle AS (
    SELECT id, domain_exdate_from, domain_exdate_to
    FROM dlp
    WHERE valid_for_exdate_after <= domain_exdate_to AND domain_exdate_from < valid_for_exdate_before),
     state_flag AS (
    SELECT id
    FROM enum_object_states
    WHERE name = 'outzoneUnguardedWarning' AND
          get_object_type_id('domain') = ANY(types)),
     page_break AS (
    SELECT COALESCE(LOWER(ch.email), '') AS registrant_key,
           ch.id AS registrant_id,
           LOWER(dobr.name) AS fqdn
    FROM contact_history ch,
         object_registry dobr
    WHERE ch.historyid = 25648451 AND
          dobr.uuid = 'b94eba42-5055-4c96-bb77-a6e1890abacc'::UUID),
     domain_data AS (
    SELECT d.id AS domain_id,
           dobr.uuid AS domain_uuid,
           LOWER(dobr.name) AS fqdn,
           d.exdate,
           d.registrant,
           robr.uuid AS registrant_uuid,
           robr.historyid,
           UPPER(robr.name) AS registrant_handle,
           registrant.name AS registrant_name,
           registrant.organization AS registrant_organization,
           COALESCE(LOWER(registrant.email), '') AS registrant_email,
           COALESCE(registrant.telephone, '') AS registrant_telephone,
           COUNT(*) OVER() AS domains_left
    FROM lifecycle lc
    JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to
    JOIN object_registry dobr ON dobr.id = d.id
    JOIN contact registrant ON registrant.id = d.registrant
    JOIN object_registry robr ON robr.id = registrant.id,
         state_flag,
         page_break
    WHERE NOT EXISTS (SELECT 0 FROM notification.object_state_additional_contact ac
                      WHERE ac.object_id = d.id AND
                            d.exdate <= ac.valid_from AND
                            ac.type = 'email_address'::notification.CONTACT_TYPE AND
                            ac.state_flag_id = state_flag.id AND
                            ac.object_state_id IS NOT NULL) AND
          ROW(page_break.registrant_email, page_break.registrant_id, page_break.fqdn) <
          ROW(COALESCE(LOWER(registrant.email), ''), registrant.id, LOWER(dobr.name))
    ORDER BY 11, d.registrant, 3
    LIMIT 10)
SELECT dd.domain_uuid,
       dd.fqdn,
       dd.registrant_uuid,
       dd.historyid,
       dd.registrant_handle,
       dd.registrant_name,
       dd.registrant_organization,
       dd.registrant_email,
       dd.registrant_telephone,
       dd.domains_left,
       UNNEST(CASE WHEN ac.contacts <> '{}' THEN ac.contacts ELSE '{NULL}' END) AS contact
FROM lifecycle lc
JOIN domain_data dd ON lc.domain_exdate_from <= dd.exdate AND dd.exdate < lc.domain_exdate_to
LEFT JOIN notification.object_state_additional_contact ac ON ac.object_id = dd.domain_id AND
                                                             dd.exdate <= ac.valid_from AND
                                                             ac.type = 'email_address'::notification.CONTACT_TYPE
LEFT JOIN state_flag ON state_flag.id = ac.state_flag_id
WHERE ac.object_id IS NULL OR ac.object_state_id IS NULL
ORDER BY 8, dd.registrant, 2;

#endif

void set_last_page(
        const boost::optional<Fred::Registry::PaginationRequest>& request,
        Fred::Registry::Domain::GetDomainsNotifyInfoReply::Data& reply)
{
    if (request != boost::none)
    {
        reply.pagination = Fred::Registry::last_page();
    }
}

struct PageToken
{
    Fred::Registry::Domain::DomainId domain_id;
    unsigned long long history_id;
};

auto make_domain_id(std::string::const_iterator begin, std::string::const_iterator end)
{
    using DomainId = Fred::Registry::Domain::DomainId;
    return Fred::Registry::Util::make_strong<DomainId>(boost::uuids::string_generator{}(begin, end));
}

auto make_history_id(const std::string& str)
{
    std::size_t end = 0;
    auto history_id = std::stoull(str, &end);
    if (end < str.length())
    {
        throw std::runtime_error{"invalid history id"};
    }
    return history_id;
}

PageToken make_page_token(const std::string& str)
{
    static constexpr auto uuid_str_length = std::string::size_type{36};
    if (str.length() < uuid_str_length)
    {
        throw std::runtime_error{"page token too short"};
    }
    auto page_token = PageToken{
            make_domain_id(str.begin(), str.begin() + uuid_str_length),
            make_history_id(str.substr(uuid_str_length))};
    return page_token;
}

enum class Move
{
    forward,
    backward
};

struct PageBreak
{
    PageBreak()
        : with_part{},
          where_part{}
    { }
    std::string with_part;
    std::string where_part;
};

PageBreak make_page_break(
        const std::string& page_token_str,
        Database::QueryParams& params,
        Fred::Registry::Domain::LifecycleEvent event,
        Move page_direction)
{
    auto result = PageBreak{};
    if (page_token_str.empty())
    {
        return result;
    }
    const auto page_token = make_page_token(page_token_str);
    params.emplace_back(get_raw_value_from(page_token.domain_id));
    const auto domain_id_idx = params.size();
    params.emplace_back(page_token.history_id);
    const auto history_id_idx = params.size();
    {
        const auto key = event == Fred::Registry::Domain::LifecycleEvent::domain_outzone ? "LOWER(ch.email)"
                                                                                         : "ch.telephone";
        result.with_part =
                "SELECT COALESCE(" + std::string{key} + ", '') AS registrant_key, "
                       "ch.id AS registrant_id, "
                       "LOWER(dobr.name) AS fqdn "
                  "FROM contact_history ch, "
                       "object_registry dobr "
                 "WHERE ch.historyid = $" + std::to_string(history_id_idx) + "::INT AND "
                       "dobr.uuid = $" + std::to_string(domain_id_idx) + "::UUID";
    }
    {
        const auto key = event == Fred::Registry::Domain::LifecycleEvent::domain_outzone ? "LOWER(registrant.email)"
                                                                                         : "registrant.telephone";
        result.where_part = [&]()
        {
            std::string compare = page_direction == Move::forward ? "<" : ">=";
            return "ROW(page_break.registrant_key, page_break.registrant_id, page_break.fqdn) " + compare + " "
                   "ROW(COALESCE(" + key + ", ''), registrant.id, LOWER(dobr.name))";
        }();
    }
    return result;
}

std::string to_sql_string(const std::chrono::system_clock::time_point& time_point)
{
    const auto time_point_usec = std::chrono::time_point_cast<std::chrono::microseconds>(time_point);
    return Fred::Registry::Util::Into<std::string>::from(time_point_usec);
}

template <typename Fnc>
void call_if_nonempty_string(const Database::Value& value, Fnc&& fnc)
{
    if (!value.isnull())
    {
        auto str = static_cast<std::string>(value);
        if (!str.empty())
        {
            std::forward<Fnc>(fnc)(std::move(str));
        }
    }
}

std::string as_empty_string_if_null(const Database::Value& value)
{
    return value.isnull() ? std::string{}
                          : static_cast<std::string>(value);
}

auto make_info(Database::Row row, Fred::Registry::Domain::DomainId domain_id)
{
    Fred::Registry::Domain::GetDomainsNotifyInfoReply::Data::Info info;
    info.domain_id = std::move(domain_id);
    info.fqdn = static_cast<std::string>(row[1]);
    info.registrant_id = Fred::Registry::Util::make_strong<Fred::Registry::Contact::ContactId>(
            boost::uuids::string_generator{}(static_cast<std::string>(row[2])));
    info.registrant_handle = Fred::Registry::Util::make_strong<Fred::Registry::Contact::ContactHandle>(
            static_cast<std::string>(row[4]));
    info.registrant_name = as_empty_string_if_null(row[5]);
    info.registrant_organization = as_empty_string_if_null(row[6]);
    call_if_nonempty_string(row[7], [&](std::string value)
    {
        auto email_address = Fred::Registry::Util::make_strong<Fred::Registry::Domain::EmailAddress>(
                std::move(value));
        info.emails.emplace_back(std::move(email_address));
    });
    call_if_nonempty_string(row[8], [&](std::string value)
    {
        auto phone_number = Fred::Registry::Util::make_strong<Fred::Registry::Domain::PhoneNumber>(
                std::move(value));
        info.phones.emplace_back(std::move(phone_number));
    });
    return info;
}

std::string get_lifecycle_parameter_name(Fred::Registry::Domain::LifecycleEvent event)
{
    switch (event)
    {
        case Fred::Registry::Domain::LifecycleEvent::domain_delete:
            return "expiration_registration_protection_period";
        case Fred::Registry::Domain::LifecycleEvent::domain_outzone:
            return "expiration_dns_protection_period";
    }
    throw std::runtime_error{"unknown lifecycle event"};
}

std::string get_state_flag_name(Fred::Registry::Domain::LifecycleEvent event)
{
    switch (event)
    {
        case Fred::Registry::Domain::LifecycleEvent::domain_delete:
            return "deleteCandidate";
        case Fred::Registry::Domain::LifecycleEvent::domain_outzone:
            return "outzoneUnguardedWarning";
    }
    throw std::runtime_error{"unknown lifecycle event"};
}

std::string get_contact_type_name(Fred::Registry::Domain::LifecycleEvent event)
{
    switch (event)
    {
        case Fred::Registry::Domain::LifecycleEvent::domain_delete:
            return "phone_number";
        case Fred::Registry::Domain::LifecycleEvent::domain_outzone:
            return "email_address";
    }
    throw std::runtime_error{"unknown lifecycle event"};
}

template <typename T>
std::string make_sql_order_by_columns(Fred::Registry::OrderByDirection direction, T&& column)
{
    if (direction == Fred::Registry::OrderByDirection::ascending)
    {
        return std::string{std::forward<T>(column)};
    }
    return std::string{std::forward<T>(column)} + " DESC";
}

template <typename T, typename ...Ts>
std::string make_sql_order_by_columns(Fred::Registry::OrderByDirection direction, T&& first_column, Ts&& ...columns)
{
    std::string result = std::string{std::forward<T>(first_column)};
    if (direction == Fred::Registry::OrderByDirection::descending)
    {
        result.append(" DESC");
    }
    result += ", " + make_sql_order_by_columns(direction, std::forward<Ts>(columns)...);
    return result;
}

template <typename ...Ts>
std::string make_sql_order_by(Fred::Registry::OrderByDirection direction, Ts&& ...columns)
{
    return "ORDER BY " + make_sql_order_by_columns(direction, std::forward<Ts>(columns)...);
}

std::string get_domain_data_order_part(Fred::Registry::Domain::LifecycleEvent event, Move page_direction)
{
    const auto direction = page_direction == Move::forward ? Fred::Registry::OrderByDirection::ascending
                                                           : Fred::Registry::OrderByDirection::descending;
    if (event == Fred::Registry::Domain::LifecycleEvent::domain_outzone)
    {
        return make_sql_order_by(direction, "11", "d.registrant", "3");
    }
    return make_sql_order_by(direction, "12", "d.registrant", "3");
}

std::string get_primary_column_number(Fred::Registry::Domain::LifecycleEvent event)
{
    switch (event)
    {
        case Fred::Registry::Domain::LifecycleEvent::domain_delete:
            return "9"; // registrant_telephone
        case Fred::Registry::Domain::LifecycleEvent::domain_outzone:
            return "8"; // registrant_email
    }
    throw std::runtime_error{"unknown lifecycle event"};
}

}//namespace {anonymous}

using namespace Fred::Registry::Domain;

GetDomainsNotifyInfoReply::Data Fred::Registry::Domain::get_domains_notify_info(
        const GetDomainsNotifyInfoRequest& request)
{
    try
    {
        auto params = Database::QueryParams{
                to_sql_string(request.event_time.lower_bound),
                to_sql_string(request.event_time.upper_bound)};
        auto page_break = PageBreak{};
        static constexpr auto no_limit = std::numeric_limits<std::int32_t>::max();
        auto limit = std::uint32_t{no_limit};
        auto page_direction = Move::forward;
        if (request.pagination != boost::none)
        {
            if (request.pagination->page_size == 0)
            {
                static constexpr auto default_page_size = std::int32_t{1000};
                limit = default_page_size;
            }
            else if (0 < request.pagination->page_size)
            {
                limit = request.pagination->page_size;
            }
            else
            {
                page_direction = Move::backward;
                limit = -(request.pagination->page_size - 1);
            }
            page_break = make_page_break(request.pagination->page_token, params, request.event, page_direction);
        }
        const auto lifecycle_parameter_name = get_lifecycle_parameter_name(request.event);
        const auto state_flag_name = get_state_flag_name(request.event);
        const auto contact_type_name = get_contact_type_name(request.event);
        std::string sql =
                "WITH time_zone AS "
                "("
                    "SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone'"
                "), "
                "dlp AS "
                "("
                    "SELECT o.id, "
                           "o.valid_for_exdate_after, "
                           "COALESCE((SELECT i.valid_for_exdate_after "
                                       "FROM domain_lifecycle_parameters i "
                                      "WHERE o.id < i.id "
                                   "ORDER BY i.id "
                                      "LIMIT 1), "
                                    "'infinity'::TIMESTAMP) AS valid_for_exdate_before, "
                           "$1::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val - "
                           "o." + lifecycle_parameter_name + " AS domain_exdate_from, "
                           "$2::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val - "
                           "o." + lifecycle_parameter_name + " AS domain_exdate_to "
                      "FROM domain_lifecycle_parameters o, "
                           "time_zone"
                "), "
                "lifecycle AS "
                "("
                    "SELECT id, domain_exdate_from, domain_exdate_to "
                      "FROM dlp "
                     "WHERE valid_for_exdate_after <= domain_exdate_to AND domain_exdate_from < valid_for_exdate_before"
                "), "
                "state_flag AS "
                "("
                    "SELECT id "
                      "FROM enum_object_states "
                     "WHERE name = '" + state_flag_name + "' AND "
                           "get_object_type_id('domain') = ANY(types)"
                "), ";
        if (!page_break.with_part.empty())
        {
            sql += "page_break AS (" + page_break.with_part + "), ";
        }
        sql += "domain_data AS "
                "("
                    "SELECT d.id AS domain_id, "
                           "dobr.uuid AS domain_uuid, "
                           "LOWER(dobr.name) AS fqdn, "
                           "d.exdate, "
                           "d.registrant, "
                           "robr.uuid AS registrant_uuid, "
                           "robr.historyid, "
                           "UPPER(robr.name) AS registrant_handle, "
                           "registrant.name AS registrant_name, "
                           "registrant.organization AS registrant_organization, "
                           "COALESCE(LOWER(registrant.email), '') AS registrant_email, "
                           "COALESCE(registrant.telephone, '') AS registrant_telephone, "
                           "COUNT(*) OVER() AS domains_left "
                      "FROM lifecycle lc "
                      "JOIN domain d ON lc.domain_exdate_from <= d.exdate AND d.exdate < lc.domain_exdate_to "
                      "JOIN object_registry dobr ON dobr.id = d.id "
                      "JOIN contact registrant ON registrant.id = d.registrant "
                      "JOIN object_registry robr ON robr.id = registrant.id, "
                           "state_flag";
        if (!page_break.with_part.empty())
        {
            sql += ", page_break";
        }
        sql += " "
            "WHERE NOT EXISTS (SELECT 0 FROM notification.object_state_additional_contact ac "
                               "WHERE ac.object_id = d.id AND "
                                     "d.exdate <= ac.valid_from AND "
                                     "ac.type = '" + contact_type_name + "'::notification.CONTACT_TYPE AND "
                                     "ac.state_flag_id = state_flag.id AND "
                                     "ac.object_state_id IS NOT NULL)";
        if (!page_break.where_part.empty())
        {
            sql += " AND " + page_break.where_part;
        }
        sql += " " + get_domain_data_order_part(request.event, page_direction);
        if (request.pagination != boost::none)
        {
            sql += " LIMIT " + std::to_string(limit);
        }
        sql += ") "
            "SELECT dd.domain_uuid, "
                   "dd.fqdn, "
                   "dd.registrant_uuid, "
                   "dd.historyid, "
                   "dd.registrant_handle, "
                   "dd.registrant_name, "
                   "dd.registrant_organization, "
                   "dd.registrant_email, "
                   "dd.registrant_telephone, "
                   "dd.domains_left, "
                   "UNNEST(CASE WHEN ac.contacts <> '{}' THEN ac.contacts ELSE '{NULL}' END) AS contact "
              "FROM lifecycle lc "
              "JOIN domain_data dd ON lc.domain_exdate_from <= dd.exdate AND dd.exdate < lc.domain_exdate_to "
         "LEFT JOIN notification.object_state_additional_contact ac ON ac.object_id = dd.domain_id AND "
                                                                      "dd.exdate <= ac.valid_from AND "
                                                                      "ac.type = '" + contact_type_name + "'::notification.CONTACT_TYPE "
         "LEFT JOIN state_flag ON state_flag.id = ac.state_flag_id "
             "WHERE ac.object_id IS NULL OR ac.object_state_id IS NULL " + [&]()
            {
                const auto direction = page_direction == Move::forward ? Fred::Registry::OrderByDirection::ascending
                                                                       : Fred::Registry::OrderByDirection::descending;
                auto primary_column = get_primary_column_number(request.event);
                return make_sql_order_by(direction, primary_column, "dd.registrant", "2", "11");
            }();
        LibFred::OperationContextCreator ctx;
        auto dbres = ctx.get_conn().exec_params(sql, params);
        GetDomainsNotifyInfoReply::Data reply;
        if (dbres.size() <= 0)
        {
            set_last_page(request.pagination, reply);
            ctx.commit_transaction();
            return reply;
        }
        if (request.pagination != boost::none)
        {
            reply.pagination = PaginationReply{};
        }
        if (page_direction == Move::forward)
        {
            if (reply.pagination != boost::none)
            {
                const auto domains_left = static_cast<std::int64_t>(dbres[0][9]);
                if (limit < domains_left)
                {
                    reply.pagination->items_left = domains_left - limit;
                }
                else
                {
                    reply.pagination->items_left = 0;
                }
            }
            GetDomainsNotifyInfoReply::Data::Info* last_info = nullptr;
            for (std::size_t idx = 0; idx < dbres.size(); ++idx)
            {
                auto domain_id = Util::make_strong<DomainId>(
                        boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
                if ((last_info == nullptr) ||
                    (get_raw_value_from(last_info->domain_id) != get_raw_value_from(domain_id)))
                {
                    auto info = make_info(dbres[idx], std::move(domain_id));
                    reply.domains.emplace_back(std::move(info));
                    last_info = reply.domains.data() + reply.domains.size() - 1;
                }
                if (reply.pagination != boost::none)
                {
                    const auto is_last_row = (idx + 1) == dbres.size();
                    if (is_last_row)
                    {
                        if (0 < *(reply.pagination->items_left))
                        {
                            reply.pagination->next_page_token = to_string(get_raw_value_from(last_info->domain_id)).append(
                                    std::to_string(static_cast<unsigned long long>(dbres[idx][3])));
                        }
                        else
                        {
                            reply.pagination->next_page_token.clear();
                        }
                    }
                }
                if (!dbres[idx][10].isnull())
                {
                    switch (request.event)
                    {
                        case LifecycleEvent::domain_delete:
                        {
                            auto phone_number = Util::make_strong<PhoneNumber>(static_cast<std::string>(dbres[idx][10]));
                            last_info->additional_phones.emplace_back(std::move(phone_number));
                            break;
                        }
                        case LifecycleEvent::domain_outzone:
                        {
                            auto email_address = Util::make_strong<EmailAddress>(static_cast<std::string>(dbres[idx][10]));
                            last_info->additional_emails.emplace_back(std::move(email_address));
                            break;
                        }
                    }
                }
            }
        }
        else if (page_direction == Move::backward)
        {
            const auto domains_left = static_cast<std::int64_t>(dbres[0][9]);
            const auto is_last_page = domains_left <= (limit - 1);
            if (reply.pagination != boost::none)
            {
                if (!is_last_page)
                {
                    reply.pagination->items_left = domains_left - limit + 1;
                }
            }
            auto data_rows = is_last_page ? dbres.size()
                                          : dbres.size() - 1;
            while (!is_last_page &&
                   (0 < data_rows) &&
                   (static_cast<std::string>(dbres[data_rows - 1][0]) == static_cast<std::string>(dbres[data_rows][0])))
            {
                --data_rows;
            }
            GetDomainsNotifyInfoReply::Data::Info* last_info = nullptr;
            for (std::size_t cnt = 0; cnt < data_rows; ++cnt)
            {
                const auto idx = data_rows - 1 - cnt;
                auto domain_id = Util::make_strong<DomainId>(
                        boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
                if ((last_info == nullptr) ||
                    (get_raw_value_from(last_info->domain_id) != get_raw_value_from(domain_id)))
                {
                    auto info = make_info(dbres[idx], std::move(domain_id));
                    reply.domains.emplace_back(std::move(info));
                    last_info = reply.domains.data() + reply.domains.size() - 1;
                }
                if (!dbres[idx][10].isnull())
                {
                    switch (request.event)
                    {
                        case LifecycleEvent::domain_delete:
                        {
                            auto phone_number = Util::make_strong<PhoneNumber>(static_cast<std::string>(dbres[idx][10]));
                            last_info->additional_phones.emplace_back(std::move(phone_number));
                            break;
                        }
                        case LifecycleEvent::domain_outzone:
                        {
                            auto email_address = Util::make_strong<EmailAddress>(static_cast<std::string>(dbres[idx][10]));
                            last_info->additional_emails.emplace_back(std::move(email_address));
                            break;
                        }
                    }
                }
            }
            if ((reply.pagination != boost::none) && !is_last_page)
            {
                const auto idx = dbres.size() - 1;
                const auto domain_id = boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0]));
                reply.pagination->next_page_token = to_string(domain_id).append(
                        std::to_string(static_cast<unsigned long long>(dbres[idx][3])));
            }
        }
        else
        {
            throw std::runtime_error{"unknown page direction value"};
        }
        ctx.commit_transaction();
        return reply;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
