/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAIN_LIFE_CYCLE_STAGE_REPLY_HH_7FA16739761DACA8F06DC99E6A4ED047//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DOMAIN_LIFE_CYCLE_STAGE_REPLY_HH_7FA16739761DACA8F06DC99E6A4ED047

#include "src/domain/domain_common_types.hh"
#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>

namespace Fred {
namespace Registry {
namespace Domain {

struct DomainLifeCycleStageReply
{
    struct LessBlockId
    {
        bool operator()(const Registry::DomainBlacklist::BlockId&, const Registry::DomainBlacklist::BlockId&) const;
    };
    using Blacklist = std::set<Registry::DomainBlacklist::BlockId, LessBlockId>;
    struct Data : Util::Printable<Data>
    {
        boost::optional<DomainId> domain_id;
        bool is_delete_candidate;
        bool is_auctioned;
        Blacklist blacklisted;
        std::string to_string() const;
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//DOMAIN_LIFE_CYCLE_STAGE_REPLY_HH_7FA16739761DACA8F06DC99E6A4ED047
