/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/get_domains_by_contact_reply.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Domain {

std::string GetDomainsByContactReply::Data::Domain::to_string() const
{
    return Util::StructToString().add("domain", domain)
                                 .add("domain_history_id", domain_history_id)
                                 .add("is_holder", is_holder)
                                 .add("is_admin_contact", is_admin_contact)
                                 .add("is_keyset_tech_contact", is_keyset_tech_contact)
                                 .add("is_nsset_tech_contact", is_nsset_tech_contact)
                                 .add("is_deleted", is_deleted)
                                 .finish();
}

std::string GetDomainsByContactReply::Data::to_string() const
{
    return Util::StructToString().add("domains", domains)
                                 .add("pagination", pagination)
                                 .finish();
}

GetDomainsByContactReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const GetDomainsByContactRequest& src)
    : Registry::Exception{"contact " + src.to_string() + " does not exist"}
{ }

GetDomainsByContactReply::Exception::InvalidOrderBy::InvalidOrderBy(const GetDomainsByContactRequest& src)
    : Registry::Exception{"invalid 'order by' specification: " + src.to_string()}
{ }

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
