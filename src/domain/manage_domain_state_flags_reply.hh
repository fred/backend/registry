/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MANAGE_DOMAIN_STATE_FLAGS_REPLY_HH_34F9A99112B46F0B708D11A0BC0CD9C5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define MANAGE_DOMAIN_STATE_FLAGS_REPLY_HH_34F9A99112B46F0B708D11A0BC0CD9C5

#include "src/domain/domain_common_types.hh"
#include "src/domain/manage_domain_state_flags_request.hh"
#include "src/util/printable.hh"
#include "src/exceptions.hh"

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct ManageDomainStateFlagsReply
{
    struct Data : Util::Printable<Data>
    {
        struct DomainStateFlagRequestInfo : Util::Printable<DomainStateFlagRequestInfo>
        {
            DomainStateFlagRequest request_id;
            DomainId domain_id;
            std::string state_flag;
            std::string to_string() const;
        };
        std::vector<DomainStateFlagRequestInfo> state_flag_requests_created;
        std::string to_string() const;
    };
    struct Exception
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const ManageDomainStateFlagsRequest::Domain& src);
        };
        struct DomainStateFlagRequestDoesNotExist : Registry::Exception
        {
            explicit DomainStateFlagRequestDoesNotExist(const DomainStateFlagRequest& src);
        };
        struct DomainStateFlagDoesNotExist : Registry::Exception
        {
            explicit DomainStateFlagDoesNotExist(const std::string& src);
        };
        struct DomainHistoryIdMismatch : Registry::Exception
        {
            explicit DomainHistoryIdMismatch(const ManageDomainStateFlagsRequest::Domain& src);
        };
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//MANAGE_DOMAIN_STATE_FLAGS_REPLY_HH_34F9A99112B46F0B708D11A0BC0CD9C5
