/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_DOMAINS_REPLY_HH_25785AB375D14049A9158A368EDA9531
#define LIST_DOMAINS_REPLY_HH_25785AB375D14049A9158A368EDA9531

#include "src/domain/domain_common_types.hh"
#include "src/domain/list_domains_request.hh"
#include "src/util/printable.hh"

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct ListDomainsReply
{
    struct Data : Util::Printable<Data>
    {
        std::vector<DomainLightInfo> domains;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ZoneDoesNotExist : Registry::Exception
        {
            explicit ZoneDoesNotExist(const ListDomainsRequest& request);
        };
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
