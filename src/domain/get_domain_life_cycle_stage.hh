/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DOMAIN_LIFE_CYCLE_STAGE_HH_59EFAF59329E6E70A440B4D786BBCEC5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_DOMAIN_LIFE_CYCLE_STAGE_HH_59EFAF59329E6E70A440B4D786BBCEC5

#include "src/domain/domain_life_cycle_stage_reply.hh"
#include "src/domain/domain_life_cycle_stage_request.hh"

namespace Fred {
namespace Registry {
namespace Domain {

DomainLifeCycleStageReply::Data get_domain_life_cycle_stage(const DomainLifeCycleStageRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//GET_DOMAIN_LIFE_CYCLE_STAGE_HH_59EFAF59329E6E70A440B4D786BBCEC5
