/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/manage_domain_state_flags.hh"
#include "src/exceptions.hh"
#include "src/util/into.hh"

#include "libfred/db_settings.hh"
#include "libfred/object_state/perform_object_state_request.hh"
#include "libfred/registrar/info_registrar.hh"
#include "util/db/db_exceptions.hh"

#include <boost/optional.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <algorithm>
#include <functional>
#include <iterator>
#include <map>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>


namespace Fred {
namespace Registry {
namespace Domain {

namespace {

struct OperationNotAllowed : Exception
{
    OperationNotAllowed(const std::string& msg)
        : Exception{msg}
    { }
};

auto make_uuid(const std::string& src)
{
    std::istringstream in{src};
    boost::uuids::uuid uuid;
    in >> uuid;
    return uuid;
}

using DomainStateFlagName = Util::StrongType<std::string, struct DomainStateFlagNameTag>;

template <typename T> struct Less;

template <typename T, typename N, template <typename> class ...Skills>
struct Less<Util::StrongType<T, N, Skills...>>
{
    using Type = Util::StrongType<T, N, Skills...>;
    bool operator()(const Type& lhs, const Type& rhs) const noexcept
    {
        return get_raw_value_from(lhs) < get_raw_value_from(rhs);
    }
};

using SetOfDomainStateFlagNames = std::set<DomainStateFlagName, Less<DomainStateFlagName>>;

SetOfDomainStateFlagNames get_manual_domain_state_flag_names(
        const LibFred::OperationContext& ctx,
        const std::set<std::string>& domain_state_flags)
{
    SetOfDomainStateFlagNames result;
    if (domain_state_flags.empty())
    {
        return result;
    }
    const auto db_res = ctx.get_conn().exec(
            "SELECT name "
            "FROM enum_object_states "
            "WHERE get_object_type_id('domain') = ANY(types) AND "
                  "manual");
    for (std::size_t idx = 0; idx < db_res.size(); ++idx)
    {
        auto state_flag_name = Util::make_strong<DomainStateFlagName>(static_cast<std::string>(db_res[idx][0]));
        if (0 < domain_state_flags.count(get_raw_value_from(state_flag_name)))
        {
            result.insert(std::move(state_flag_name));
            if (result.size() == domain_state_flags.size())
            {
                return result;
            }
        }
    }
    struct InvalidDomainStateFlags : std::runtime_error
    {
        InvalidDomainStateFlags()
            : std::runtime_error{"invalid domain state flags"}
        { }
        std::set<std::string> state_flags;
    } exception;
    std::for_each(begin(domain_state_flags), end(domain_state_flags), [&](auto&& domain_state_flag)
            {
                if (result.count(Util::make_strong<DomainStateFlagName>(domain_state_flag)) == 0)
                {
                    exception.state_flags.insert(domain_state_flag);
                }
            });
    throw exception;
}

struct Validity
{
    using Time = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    boost::optional<Time> valid_from; // missing value means "now"
    boost::optional<Time> valid_to; // missing value means "unlimited"
};

using ValidityByDomainStateFlagName = std::map<DomainStateFlagName, Validity, Less<DomainStateFlagName>>;

ValidityByDomainStateFlagName make_validity_by_name(
        const std::map<std::string, ManageDomainStateFlagsRequest::Validity>& validity_by_name,
        const SetOfDomainStateFlagNames& available_names)
{
    ValidityByDomainStateFlagName result;
    std::transform(
            begin(validity_by_name),
            end(validity_by_name),
            std::inserter(result, end(result)),
            [&](auto&& name_validity_pair)
            {
                auto name = Util::make_strong<DomainStateFlagName>(name_validity_pair.first);
                const bool name_is_available = 0 < available_names.count(name);
                if (!name_is_available)
                {
                    throw ManageDomainStateFlagsReply::Exception::DomainStateFlagDoesNotExist{get_raw_value_from(name)};
                }
                return std::make_pair(name, Validity{name_validity_pair.second.valid_from,
                                                     name_validity_pair.second.valid_to});
            });
    return result;
}

struct InvalidBoundaries : std::runtime_error
{
    InvalidBoundaries() : std::runtime_error{"valid_from is greater than valid_to"} { }
};

class MultipleDomainsLocked;

// locks multiple objects at once in a defined order to prevent deadlock
MultipleDomainsLocked lock_multiple_domain_state_flag_requests(
        LibFred::OperationContext& ctx,
        const std::vector<ManageDomainStateFlagsRequest::ToCreate>& to_create,
        const std::vector<DomainStateFlagRequest>& to_close);

using DomainNumericId = Util::StrongType<unsigned long long, struct DomainNumericIdTag>;
using RegistrarId = Util::StrongType<unsigned long long, struct RegistrarIdTag>;
struct DomainInfo
{
    DomainHistoryId domain_history_uuid;
    DomainNumericId domain_id;
    RegistrarId registrar_id;
};
struct RequestInfo
{
    DomainNumericId domain_id;
    RegistrarId registrar_id;
};

class LockedRequestsToCreateNewOne
{
public:
    const LibFred::OperationContext& get_ctx() const noexcept { return ctx_; }
    DomainNumericId get_domain_id() const noexcept { return domain_id_; }
    bool is_sponsoring_registrar(RegistrarId registrar_id) const noexcept
    {
        return get_raw_value_from(registrar_id_) == get_raw_value_from(registrar_id);
    }
private:
    LockedRequestsToCreateNewOne(
            const LibFred::OperationContext& ctx,
            const DomainInfo& domain_info)
        : ctx_{ctx},
          domain_id_{domain_info.domain_id},
          registrar_id_{domain_info.registrar_id}
    { }
    const LibFred::OperationContext& ctx_;
    DomainNumericId domain_id_;
    RegistrarId registrar_id_;
    friend class MultipleDomainsLocked;
};

class LockedRequestsToCloseOne
{
public:
    const LibFred::OperationContext& get_ctx() const noexcept { return ctx_; }
    DomainStateFlagRequest get_request_id() const noexcept { return request_id_; }
    bool is_sponsoring_registrar(RegistrarId registrar_id) const noexcept
    {
        return get_raw_value_from(registrar_id_) == get_raw_value_from(registrar_id);
    }
private:
    LockedRequestsToCloseOne(
            const LibFred::OperationContext& ctx,
            DomainStateFlagRequest request_id,
            const RequestInfo request_info)
        : ctx_{ctx},
          request_id_{request_id},
          registrar_id_{request_info.registrar_id}
    { }
    const LibFred::OperationContext& ctx_;
    DomainStateFlagRequest request_id_;
    RegistrarId registrar_id_;
    friend class MultipleDomainsLocked;
};

class MultipleDomainsLocked
{
public:
    LockedRequestsToCreateNewOne get_locked_requests_by_domain(
            const ManageDomainStateFlagsRequest::Domain& domain) const
    {
        return LockedRequestsToCreateNewOne{ locked_.get_ctx(), locked_.get(domain) };
    }
    LockedRequestsToCloseOne get_locked_requests_by_request(
            const DomainStateFlagRequest& request) const
    {
        return LockedRequestsToCloseOne{ locked_.get_ctx(), request, locked_.get(request) };
    }
    void perform_object_state_requests() const
    {
        locked_.perform_object_state_requests();
    }
private:
    MultipleDomainsLocked(
            LibFred::OperationContext& ctx,
            const std::vector<ManageDomainStateFlagsRequest::ToCreate>& to_create,
            const std::vector<DomainStateFlagRequest>& to_close)
        : locked_{ctx, lock_in_db(ctx, to_create, to_close)}
    {
        this->has_all_of(to_create);
        this->has_all_of(to_close);
    }
    void has_all_of(const std::vector<ManageDomainStateFlagsRequest::Domain>& domains) const
    {
        std::for_each(begin(domains), end(domains), [&](auto&& domain) { locked_.get(domain); });
    }
    void has_all_of(const std::vector<ManageDomainStateFlagsRequest::ToCreate>& to_create) const
    {
        std::for_each(begin(to_create), end(to_create), [&](auto&& request) { has_all_of(request.domains); });
    }
    void has_all_of(const std::vector<DomainStateFlagRequest>& requests) const
    {
        std::for_each(begin(requests), end(requests), [&](auto&& request) { locked_.get(request); });
    }
    Database::Result lock_in_db(
            const LibFred::OperationContext& ctx,
            const std::vector<ManageDomainStateFlagsRequest::ToCreate>& to_create,
            const std::vector<DomainStateFlagRequest>& to_close)
    {
        auto arguments = Database::query_param_list{};
        const auto to_create_uuids = [&]()
        {
            if (to_create.empty())
            {
                return std::string{"NULL::UUID"};
            }
            std::string list = "";
            std::for_each(begin(to_create), end(to_create), [&](auto&& request)
            {
                std::for_each(begin(request.domains), end(request.domains), [&](auto&& domain)
                {
                    arguments(get_raw_value_from(domain.domain_id));
                    if (list.empty())
                    {
                        list = "$" + std::to_string(arguments.size()) + "::UUID";
                    }
                    else
                    {
                        list += ", $" + std::to_string(arguments.size()) + "::UUID";
                    }
                });
            });
            return list;
        };
        const auto to_close_uuids = [&]()
        {
            if (to_close.empty())
            {
                return std::string{"NULL::UUID"};
            }
            std::string list = "";
            std::for_each(begin(to_close), end(to_close), [&](auto&& request)
            {
                arguments(get_raw_value_from(request));
                if (list.empty())
                {
                    list = "$" + std::to_string(arguments.size()) + "::UUID";
                }
                else
                {
                    list += ", $" + std::to_string(arguments.size()) + "::UUID";
                }
            });
            return list;
        };
        const auto sql =
                "WITH d AS ("
                    "SELECT id, NULL::INT AS request_id "
                    "FROM object_registry "
                    "WHERE uuid IN (" + to_create_uuids() + ") AND "
                          "erdate IS NULL AND "
                          "type = get_object_type_id('domain') "
                    "UNION "
                    "SELECT obr.id, osr.id AS request_id "
                    "FROM object_state_request osr "
                    "JOIN object_registry obr ON obr.id = osr.object_id "
                    "WHERE osr.uuid IN (" + to_close_uuids() + ") AND "
                          "obr.erdate IS NULL AND "
                          "obr.type = get_object_type_id('domain')), "
                     "to_lock AS (SELECT id FROM d GROUP BY 1 ORDER BY 1), "
                     "l AS ("
                    "SELECT o.id, o.clid, lock_object_state_request_lock(o.id) "
                    "FROM to_lock "
                    "JOIN object o ON o.id = to_lock.id) "
                "SELECT NULL::UUID AS request_uuid, "
                       "obr.uuid AS domain_uuid, "
                       "h.uuid AS domain_history_uuid, "
                       "obr.id AS domain_id, "
                       "l.clid AS registrar_id "
                "FROM l "
                "JOIN d ON d.id = l.id AND d.request_id IS NULL "
                "JOIN object_registry obr ON obr.id = l.id "
                "JOIN history h ON h.id = obr.historyid "
                "UNION "
                "SELECT osr.uuid AS request_uuid, "
                       "obr.uuid AS domain_uuid, "
                       "NULL::UUID AS domain_history_uuid, "
                       "obr.id AS domain_id, "
                       "l.clid AS registrar_id "
                "FROM l "
                "JOIN d ON d.id = l.id AND d.request_id IS NOT NULL "
                "JOIN object_registry obr ON obr.id = l.id "
                "JOIN object_state_request osr ON osr.id = d.request_id";
        return ctx.get_conn().exec_params(sql, arguments);
    }
    class Locked
    {
    public:
        Locked(LibFred::OperationContext& ctx, const Database::Result& db_res)
            : ctx_{ctx},
              domains_{locked_domains(db_res)},
              requests_{locked_requests(db_res)}
        { }
        const LibFred::OperationContext& get_ctx() const noexcept
        {
            return ctx_;
        }
        const DomainInfo& get(const ManageDomainStateFlagsRequest::Domain& domain) const
        {
            const auto locked_itr = domains_.find(domain.domain_id);
            if (locked_itr == end(domains_))
            {
                throw ManageDomainStateFlagsReply::Exception::DomainDoesNotExist{domain};
            }
            if (domain.domain_history_id != boost::none &&
                (get_raw_value_from(locked_itr->second.domain_history_uuid) !=
                 get_raw_value_from(*domain.domain_history_id)))
            {
                throw ManageDomainStateFlagsReply::Exception::DomainHistoryIdMismatch{domain};
            }
            return locked_itr->second;
        }
        const RequestInfo& get(const DomainStateFlagRequest& request) const
        {
            const auto locked_itr = requests_.find(request);
            if (locked_itr == end(requests_))
            {
                throw ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist{request};
            }
            return locked_itr->second;
        }
        void perform_object_state_requests() const
        {
            std::for_each(begin(domains_), end(domains_), [&](auto&& domain)
            {
                LibFred::PerformObjectStateRequest{get_raw_value_from(domain.second.domain_id)}.exec(ctx_);
            });
            std::for_each(begin(requests_), end(requests_), [&](auto&& request)
            {
                LibFred::PerformObjectStateRequest{get_raw_value_from(request.second.domain_id)}.exec(ctx_);
            });
        }
    private:
        using LockedDomains = std::map<DomainId, DomainInfo, Less<DomainId>>;
        using LockedRequests = std::map<DomainStateFlagRequest, RequestInfo, Less<DomainStateFlagRequest>>;
        static LockedDomains locked_domains(const Database::Result& db_res)
        {
            LockedDomains domains;
            for (std::size_t idx = 0; idx < db_res.size(); ++idx)
            {
                const bool is_domain = db_res[idx][0].isnull();
                if (is_domain)
                {
                    domains.insert(std::make_pair(
                            Util::make_strong<DomainId>(make_uuid(static_cast<std::string>(db_res[idx][1]))),
                            DomainInfo{
                                    Util::make_strong<DomainHistoryId>(make_uuid(static_cast<std::string>(db_res[idx][2]))),
                                    Util::make_strong<DomainNumericId>(static_cast<unsigned long long>(db_res[idx][3])),
                                    Util::make_strong<RegistrarId>(static_cast<unsigned long long>(db_res[idx][4]))}));
                }
            }
            return domains;
        }
        static LockedRequests locked_requests(const Database::Result& db_res)
        {
            LockedRequests requests;
            for (std::size_t idx = 0; idx < db_res.size(); ++idx)
            {
                const bool is_request = !db_res[idx][0].isnull();
                if (is_request)
                {
                    requests.insert(std::make_pair(
                            Util::make_strong<DomainStateFlagRequest>(make_uuid(static_cast<std::string>(db_res[idx][0]))),
                            RequestInfo{
                                    Util::make_strong<DomainNumericId>(static_cast<unsigned long long>(db_res[idx][3])),
                                    Util::make_strong<RegistrarId>(static_cast<unsigned long long>(db_res[idx][4]))}));
                }
            }
            return requests;
        }
        LibFred::OperationContext& ctx_;
        LockedDomains domains_;
        LockedRequests requests_;
    };
    Locked locked_;
    friend MultipleDomainsLocked lock_multiple_domain_state_flag_requests(
            LibFred::OperationContext& ctx,
            const std::vector<ManageDomainStateFlagsRequest::ToCreate>& to_create,
            const std::vector<DomainStateFlagRequest>& to_close)
    {
        return MultipleDomainsLocked{ctx, to_create, to_close};
    }
};

void check_permissions(
        const std::vector<LockedRequestsToCloseOne>& locked_requests,
        const LibFred::InfoRegistrarData& by_registrar)
{
    const bool is_system_registrar = !by_registrar.system.isnull() && by_registrar.system.get_value();
    std::for_each(begin(locked_requests), end(locked_requests), [&](auto&& request_to_close)
            {
                const bool insufficient_permissions =
                        !is_system_registrar &&
                        !request_to_close.is_sponsoring_registrar(Util::make_strong<RegistrarId>(by_registrar.id));
                if (insufficient_permissions)
                {
                    throw OperationNotAllowed{"insufficient permissions for registrar"};
                }
            });
}

void close_domain_state_flag_requests(
        const std::vector<LockedRequestsToCloseOne>& locked_requests,
        const LibFred::InfoRegistrarData& by_registrar)
{
    if (locked_requests.empty())
    {
        return;
    }
    check_permissions(locked_requests, by_registrar);
    auto arguments = Database::query_param_list{};
    const auto requests_uuids = [&]()
    {
        if (locked_requests.empty())
        {
            return std::string{"NULL::UUID"};
        }
        std::string list = "";
        std::for_each(begin(locked_requests), end(locked_requests), [&](auto&& request_to_close)
                {
                    arguments(get_raw_value_from(request_to_close.get_request_id()));
                    if (list.empty())
                    {
                        list = "$" + std::to_string(arguments.size()) + "::UUID";
                    }
                    else
                    {
                        list += ", $" + std::to_string(arguments.size()) + "::UUID";
                    }
                });
        return list;
    };
    const auto sql =
            "UPDATE object_state_request "
            "SET canceled = CURRENT_TIMESTAMP "
            "WHERE canceled is NULL AND "
                  "valid_from <= CURRENT_TIMESTAMP AND "
                  "(valid_to IS NULL OR CURRENT_TIMESTAMP < valid_to) AND "
                  "uuid IN (" + requests_uuids() + ") "
            "RETURNING uuid";
    const auto db_res = locked_requests.front().get_ctx().get_conn().exec_params(sql, arguments);
    if (db_res.size() == locked_requests.size())
    {
        return;
    }
    std::set<boost::uuids::uuid> to_cancel;
    std::transform(begin(locked_requests), end(locked_requests), std::inserter(to_cancel, end(to_cancel)), [&](auto&& request_to_close)
            {
                return get_raw_value_from(request_to_close.get_request_id());
            });
    for (std::size_t idx = 0; idx < db_res.size(); ++idx)
    {
        const auto request_uuid = make_uuid(static_cast<std::string>(db_res[idx][0]));
        if (to_cancel.count(request_uuid) <= 0)
        {
            throw ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist{
                    Util::make_strong<DomainStateFlagRequest>(request_uuid)};
        }
    }
}

void create_domain_state_flag_requests(
        const LockedRequestsToCreateNewOne& locked_domain,
        const ValidityByDomainStateFlagName& requested_state_flags,
        const LibFred::InfoRegistrarData& by_registrar,
        std::function<void(const DomainStateFlagRequest&, const std::string&)> on_create_callback)
{
    const bool insufficient_permissions =
            !locked_domain.is_sponsoring_registrar(Util::make_strong<RegistrarId>(by_registrar.id)) &&
            (by_registrar.system.isnull() || !by_registrar.system.get_value());
    if (insufficient_permissions)
    {
        throw OperationNotAllowed{"insufficient permissions for registrar"};
    }
    auto arguments = Database::query_param_list{};
    const auto sql =
            "INSERT INTO object_state_request (object_id, state_id, crdate, valid_from, valid_to) "
            "VALUES " + [&]()
            {
                std::string list;
                std::for_each(begin(requested_state_flags), end(requested_state_flags), [&](auto&& flag_validity)
                        {
                            if (!list.empty())
                            {
                                list += ", ";
                            }
                            arguments(get_raw_value_from(flag_validity.first));
                            list += "(" + std::to_string(get_raw_value_from(locked_domain.get_domain_id())) + ", " // object_id
                                     "(SELECT id " // state_id
                                      "FROM enum_object_states "
                                      "WHERE name = $" + std::to_string(arguments.size()) + "::TEXT), "
                                     "NOW(), "; // crdate
                            if (flag_validity.second.valid_from == boost::none) // valid_from
                            {
                                list += "NOW(), ";
                            }
                            else
                            {
                                arguments(Util::Into<std::string>::from(*flag_validity.second.valid_from));
                                list += "$" + std::to_string(arguments.size()) + "::TIMESTAMP, ";
                            }
                            if (flag_validity.second.valid_to == boost::none) // valid_to
                            {
                                list += "NULL)";
                            }
                            else
                            {
                                arguments(Util::Into<std::string>::from(*flag_validity.second.valid_to));
                                list += "$" + std::to_string(arguments.size()) + "::TIMESTAMP)";
                            }
                        });
                return list;
            }() + " "
            "RETURNING (SELECT name FROM enum_object_states WHERE id = state_id),"
                      "uuid,"
                      "valid_to IS NULL OR valid_from <= valid_to AS is_correct";
    const auto db_res = locked_domain.get_ctx().get_conn().exec_params(sql, arguments);
    if (db_res.size() != requested_state_flags.size())
    {
        throw std::runtime_error{"unexpected number of rows"};
    }
    for (std::size_t idx = 0; idx < db_res.size(); ++idx)
    {
        const bool validity_is_correct = static_cast<bool>(db_res[idx][2]);
        if (!validity_is_correct)
        {
            throw InvalidBoundaries{};
        }
        on_create_callback(
                Util::make_strong<DomainStateFlagRequest>(make_uuid(static_cast<std::string>(db_res[idx][1]))),
                static_cast<std::string>(db_res[idx][0]));
    }
}

auto get_requested_state_flags(
        const LibFred::OperationContext& ctx,
        const std::map<std::string, ManageDomainStateFlagsRequest::Validity>& state_flags)
{
    const auto requested_flags = [&]()
            {
                std::set<std::string> flags;
                std::for_each(
                        begin(state_flags),
                        end(state_flags),
                        [&](auto&& flag_validity_pair)
                        {
                            flags.insert(flag_validity_pair.first);
                        });
                return flags;
            }();
    const auto available_flags = get_manual_domain_state_flag_names(ctx, requested_flags);
    return make_validity_by_name(state_flags, available_flags);
}

} // namespace Fred::Registry::Domain::{anonymous}

ManageDomainStateFlagsReply::Data manage_domain_state_flags(
        const ManageDomainStateFlagsRequest& request,
        const boost::optional<std::string>& by_registrar)
{
    try
    {
        ManageDomainStateFlagsReply::Data reply;
        if (request.state_flag_requests_to_create.empty() &&
            request.state_flag_requests_to_close.empty())
        {
            return reply;
        }
        if (by_registrar == boost::none)
        {
            throw Exception{"no registrar configured"};
        }
        LibFred::OperationContextCreator ctx;
        const auto registrar_info = LibFred::InfoRegistrarByHandle{*by_registrar}.exec(ctx).info_registrar_data;

        const auto locked_domains = lock_multiple_domain_state_flag_requests(
                ctx,
                request.state_flag_requests_to_create,
                request.state_flag_requests_to_close);
        std::for_each(begin(request.state_flag_requests_to_create), end(request.state_flag_requests_to_create), [&](auto&& to_create)
        {
            const auto state_flags = get_requested_state_flags(ctx, to_create.state_flags);
            std::for_each(begin(to_create.domains), end(to_create.domains), [&](auto&& domain)
            {
                create_domain_state_flag_requests(
                        locked_domains.get_locked_requests_by_domain(domain),
                        state_flags,
                        registrar_info,
                        [&](const DomainStateFlagRequest& request_id, const std::string& state_flag)
                        {
                            ManageDomainStateFlagsReply::Data::DomainStateFlagRequestInfo request{};
                            request.request_id = request_id;
                            request.domain_id = domain.domain_id;
                            request.state_flag = state_flag;
                            reply.state_flag_requests_created.push_back(std::move(request));
                        });
            });
        });
        std::vector<LockedRequestsToCloseOne> locked_requests;
        locked_requests.reserve(request.state_flag_requests_to_close.size());
        std::transform(
                begin(request.state_flag_requests_to_close),
                end(request.state_flag_requests_to_close),
                std::back_inserter(locked_requests),
                [&](auto&& request)
                {
                    return locked_domains.get_locked_requests_by_request(request);
                });
        close_domain_state_flag_requests(locked_requests, registrar_info);
        locked_domains.perform_object_state_requests();
        ctx.commit_transaction();
        return reply;
    }
    catch (const ManageDomainStateFlagsReply::Exception::DomainDoesNotExist&)
    {
        throw;
    }
    catch (const ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist&)
    {
        throw;
    }
    catch (const ManageDomainStateFlagsReply::Exception::DomainStateFlagDoesNotExist&)
    {
        throw;
    }
    catch (const ManageDomainStateFlagsReply::Exception::DomainHistoryIdMismatch&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }

}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
