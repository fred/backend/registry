/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEARCH_DOMAIN_HH_EA253F98448C4C1982BDF801E83980F8
#define SEARCH_DOMAIN_HH_EA253F98448C4C1982BDF801E83980F8

#include "src/domain/domain_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <set>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

enum class DomainItem
{
    fqdn
};

struct SearchDomainRequest : Util::Printable<SearchDomainRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> limit;
    std::set<DomainItem> searched_items;
    std::string to_string() const;
};

struct SearchDomainReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            DomainId domain_id;
            std::set<DomainItem> matched_items;
            std::string to_string() const;
        };
        std::vector<Result> most_similar_domains;
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::set<DomainItem> searched_items;
        std::string to_string() const;
    };
    Data data;
};

SearchDomainReply::Data search_domain(const SearchDomainRequest& request);

struct SearchDomainHistoryRequest : Util::Printable<SearchDomainHistoryRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> max_number_of_domains;
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    boost::optional<TimePoint> data_valid_from;
    boost::optional<TimePoint> data_valid_to;
    std::set<DomainItem> searched_items;
    std::string to_string() const;
};

struct SearchDomainHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            struct HistoryPeriod : Util::Printable<HistoryPeriod>
            {
                using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
                std::set<DomainItem> matched_items;
                TimePoint valid_from;
                boost::optional<TimePoint> valid_to;
                std::vector<DomainHistoryId> domain_history_ids;
                std::string to_string() const;
            };
            DomainId object_id;
            std::vector<HistoryPeriod> histories;
            std::string to_string() const;
        };
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::vector<Result> results;
        std::set<DomainItem> searched_items;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ChronologyViolation : Registry::Exception
        {
            explicit ChronologyViolation(const SearchDomainHistoryRequest& request);
        };
    };
};

SearchDomainHistoryReply::Data search_domain(const SearchDomainHistoryRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
