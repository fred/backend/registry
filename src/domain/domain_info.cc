/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/domain_info.hh"

#include "src/util/strong_type.hh"
#include "src/fred_unwrap.hh"
#include "src/util/strong_type.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrable_object/is_registered.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

#include <algorithm>
#include <iterator>

namespace Fred {
namespace Registry {
namespace Domain {

namespace {

template <typename Clock, typename Dur>
std::chrono::time_point<Clock, Dur> into_time_point(const boost::posix_time::ptime& src)
{
    const auto time_since_epoch = src - boost::posix_time::from_time_t(0);
    static_assert(Dur::period::num == 1);
    return Clock::from_time_t(time_since_epoch.total_seconds()) +
           Dur{time_since_epoch.fractional_seconds() *
               (Dur::period::den / (time_since_epoch.ticks_per_second() * Dur::period::num))};
}

DomainLifeCycleMilestone make_domain_life_cycle_milestone(const Database::Value& value)
{
    DomainLifeCycleMilestone result;
    result.timestamp = into_time_point<std::chrono::system_clock, std::chrono::nanoseconds>(
            boost::posix_time::time_from_string(
                    static_cast<std::string>(value)));
    return result;
}

boost::optional<DomainLifeCycleTimestamp> make_domain_life_cycle_timestamp(
        const Database::Value& value)
{
    if (value.isnull())
    {
        return boost::none;
    }
    DomainLifeCycleTimestamp result;
    result.timestamp = into_time_point<std::chrono::system_clock, std::chrono::nanoseconds>(
            boost::posix_time::time_from_string(
                    static_cast<std::string>(value)));
    return boost::make_optional(result);
}

struct Milestones
{
    DomainLifeCycleMilestone expiration_warning_scheduled_at;
    DomainLifeCycleMilestone outzone_unguarded_warning_scheduled_at;
    DomainLifeCycleMilestone outzone_scheduled_at;
    DomainLifeCycleMilestone delete_warning_scheduled_at;
    DomainLifeCycleMilestone delete_candidate_scheduled_at;
    boost::optional<DomainLifeCycleTimestamp> outzone_at;
    boost::optional<DomainLifeCycleTimestamp> delete_candidate_at;
};

Milestones get_domain_life_cycle_milestones(
        const LibFred::OperationContext& ctx,
        const DomainId& domain_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "WITH tz AS "
            "("
                "SELECT COALESCE((SELECT val "
                                   "FROM enum_parameters "
                                  "WHERE name = 'regular_day_procedure_zone'), "
                                "'UTC') AS value"
            ") "
            "SELECT (d.exdate + dlp.expiration_notify_period) AT TIME ZONE tz.value "
                                                             "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.outzone_unguarded_email_warning_period) AT TIME ZONE tz.value "
                                                                           "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.expiration_dns_protection_period) AT TIME ZONE tz.value "
                                                                     "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.expiration_letter_warning_period) AT TIME ZONE tz.value "
                                                                     "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.expiration_registration_protection_period) AT TIME ZONE tz.value "
                                                                              "AT TIME ZONE 'UTC', "
                   "(SELECT os.valid_from "
                      "FROM object_state os "
                      "JOIN enum_object_states eos ON eos.id = os.state_id "
                     "WHERE os.object_id = obr.id AND "
                           "os.valid_to IS NULL AND "
                           "eos.name = 'outzone'), "
                   "(SELECT os.valid_from "
                      "FROM object_state os "
                      "JOIN enum_object_states eos ON eos.id = os.state_id "
                     "WHERE os.object_id = obr.id AND "
                           "os.valid_to IS NULL AND "
                           "eos.name = 'deleteCandidate') "
              "FROM tz, "
                   "domain_lifecycle_parameters dlp, "
                   "object_registry obr "
              "JOIN domain_history d ON d.id = obr.id AND d.historyid = obr.historyid "
             "WHERE obr.uuid = $1::UUID AND "
                   "dlp.valid_for_exdate_after::DATE <= d.exdate "
          "ORDER BY dlp.valid_for_exdate_after DESC "
             "LIMIT 1",
            Database::QueryParams{get_raw_value_from(domain_id)});
    if (dbres.size() != 1)
    {
        struct UnexpectedResult : std::exception
        {
            const char* what() const noexcept override { return "just one row expected as a result"; }
        };
        throw UnexpectedResult{};
    }
    return Milestones{
        make_domain_life_cycle_milestone(dbres[0][0]),
        make_domain_life_cycle_milestone(dbres[0][1]),
        make_domain_life_cycle_milestone(dbres[0][2]),
        make_domain_life_cycle_milestone(dbres[0][3]),
        make_domain_life_cycle_milestone(dbres[0][4]),
        make_domain_life_cycle_timestamp(dbres[0][5]),
        make_domain_life_cycle_timestamp(dbres[0][6])
    };
}

Milestones get_domain_life_cycle_milestones(
        const LibFred::OperationContext& ctx,
        const DomainHistoryId& domain_history_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "WITH tz AS "
            "("
                "SELECT COALESCE((SELECT val "
                                   "FROM enum_parameters "
                                  "WHERE name = 'regular_day_procedure_zone'), "
                                "'UTC') AS value"
            ") "
            "SELECT (d.exdate + dlp.expiration_notify_period) AT TIME ZONE tz.value "
                                                             "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.outzone_unguarded_email_warning_period) AT TIME ZONE tz.value "
                                                                           "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.expiration_dns_protection_period) AT TIME ZONE tz.value "
                                                                     "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.expiration_letter_warning_period) AT TIME ZONE tz.value "
                                                                     "AT TIME ZONE 'UTC', "
                   "(d.exdate + dlp.expiration_registration_protection_period) AT TIME ZONE tz.value "
                                                                              "AT TIME ZONE 'UTC', "
                   "(SELECT os.valid_from "
                      "FROM object_state os "
                      "JOIN enum_object_states eos ON eos.id = os.state_id "
                     "WHERE os.object_id = d.id AND "
                           "os.valid_to IS NULL AND "
                           "eos.name = 'outzone'), "
                   "(SELECT os.valid_from "
                      "FROM object_state os "
                      "JOIN enum_object_states eos ON eos.id = os.state_id "
                     "WHERE os.object_id = d.id AND "
                           "os.valid_to IS NULL AND "
                           "eos.name = 'deleteCandidate') "
              "FROM tz, "
                   "domain_lifecycle_parameters dlp, "
                   "domain_history d "
              "JOIN history h ON h.id = d.historyid "
             "WHERE h.uuid = $1::UUID AND "
                   "dlp.valid_for_exdate_after::DATE <= d.exdate "
          "ORDER BY dlp.valid_for_exdate_after DESC "
             "LIMIT 1",
            Database::QueryParams{get_raw_value_from(domain_history_id)});
    if (dbres.size() != 1)
    {
        struct UnexpectedResult : std::exception
        {
            const char* what() const noexcept override { return "just one row expected as a result"; }
        };
        throw UnexpectedResult{};
    }
    return Milestones{
        make_domain_life_cycle_milestone(dbres[0][0]),
        make_domain_life_cycle_milestone(dbres[0][1]),
        make_domain_life_cycle_milestone(dbres[0][2]),
        make_domain_life_cycle_milestone(dbres[0][3]),
        make_domain_life_cycle_milestone(dbres[0][4]),
        make_domain_life_cycle_timestamp(dbres[0][5]),
        make_domain_life_cycle_timestamp(dbres[0][6])
    };
}

DomainInfoRequest get_checked_request_data(const BatchDomainInfoRequest::Request& data)
{
    DomainInfoRequest dst;
    try
    {
        dst.domain_id = Util::make_strong<DomainId>(boost::uuids::string_generator{}(data.domain_id));
    }
    catch (const std::exception&)
    {
        throw DomainInfoReply::Exception::InvalidData{dst, {"domain_id"}};
    }
    if (!data.domain_history_id.empty())
    {
        try
        {
            dst.domain_history_id = Util::make_strong<DomainHistoryId>(boost::uuids::string_generator{}(data.domain_history_id));
        }
        catch (const std::exception&)
        {
            throw DomainInfoReply::Exception::InvalidData{dst, {"domain_history_id"}};
        }
    }
    return dst;
}

struct DomainDoesNotExist {};

template <LibFred::DbLock locking>
DomainInfoReply::Data domain_info(
        const LibFred::OperationContextUsing<locking>& ctx,
        const DomainInfoRequest& request)
{
    try
    {
        try
        {
            auto result = [&]()
            {
                struct Result
                {
                    DomainInfoReply::Data data;
                    Milestones milestones;
                };
                if (request.domain_history_id != boost::none)
                {
                    const auto domain_history_uuid = LibFred::RegistrableObject::make_history_uuid_of<LibFred::Object_Type::domain>(
                            get_raw_value_from(*request.domain_history_id));
                    return Result{
                            fred_unwrap(ctx, LibFred::InfoDomainByHistoryUuid(domain_history_uuid).exec(ctx)),
                            get_domain_life_cycle_milestones(ctx, *request.domain_history_id)};
                }
                const auto domain_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::domain>(
                        get_raw_value_from(request.domain_id));
                return Result{
                        fred_unwrap(ctx, LibFred::InfoDomainByUuid(domain_uuid).exec(ctx)),
                        get_domain_life_cycle_milestones(ctx, request.domain_id)};
            }();
            result.data.expiration_warning_scheduled_at = result.milestones.expiration_warning_scheduled_at;
            result.data.outzone_unguarded_warning_scheduled_at = result.milestones.outzone_unguarded_warning_scheduled_at;
            result.data.outzone_scheduled_at = result.milestones.outzone_scheduled_at;
            result.data.delete_warning_scheduled_at = result.milestones.delete_warning_scheduled_at;
            result.data.delete_candidate_scheduled_at = result.milestones.delete_candidate_scheduled_at;
            result.data.outzone_at = result.milestones.outzone_at;
            result.data.delete_candidate_at = result.milestones.delete_candidate_at;
            return result.data;
        }
        catch (const LibFred::InfoDomainByUuid::Exception& e)
        {
            if (e.is_set_unknown_domain_uuid())
            {
                if (LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::domain, e.get_unknown_domain_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered domain");
                }
                throw DomainInfoReply::Exception::DomainDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::DomainInfoByUuid::Exception");
        }
        catch (const LibFred::InfoDomainByHistoryUuid::Exception& e)
        {
            if (e.is_set_unknown_domain_history_uuid())
            {
                if (LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::domain, e.get_unknown_domain_history_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered domain");
                }
                throw DomainInfoReply::Exception::DomainDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoDomainByHistoryUuid::Exception");
        }
        catch (const DomainDoesNotExist&)
        {
            throw DomainInfoReply::Exception::DomainDoesNotExist(request);
        }
    }
    catch (const DomainInfoReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const DomainInfoReply::Exception::DomainDoesNotExist&)
    {
        throw;
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError{"unknown exception"};
    }
}

template <LibFred::DbLock locking>
BatchDomainInfoReply::Data::BatchReply get_batch_reply(
        const LibFred::OperationContextUsing<locking>& ctx,
        const BatchDomainInfoRequest::Request& request)
{
    BatchDomainInfoReply::Data::BatchReply reply;
    try
    {
        reply.data_or_error = domain_info(ctx, get_checked_request_data(request));
    }
    catch (DomainInfoReply::Exception::InvalidData& exception)
    {
        reply.data_or_error = BatchDomainInfoReply::Error{
                request,
                DomainInfoReply::Exception{std::move(exception)}};
    }
    catch (DomainInfoReply::Exception::DomainDoesNotExist& exception)
    {
        reply.data_or_error = BatchDomainInfoReply::Error{
                request,
                DomainInfoReply::Exception{std::move(exception)}};
    }
    return reply;
}

} // namespace Fred::Registry::Domain::{anonymous}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::Domain;

DomainInfoReply::Data Fred::Registry::Domain::domain_info(const DomainInfoRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx{ctx_provider};
        auto result = domain_info(ctx, request);
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
}

BatchDomainInfoReply::Data Fred::Registry::Domain::batch_domain_info(const BatchDomainInfoRequest& batch_request)
{
    try
    {
        const auto& requests = batch_request.requests;
        BatchDomainInfoReply::Data result;
        if (requests.empty())
        {
            return result;
        }
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx{ctx_provider};
        std::transform(cbegin(requests), cend(requests), std::back_inserter(result.replies), [&ctx](auto&& request)
        {
            return get_batch_reply(ctx, request);
        });
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
}

DomainIdReply::Data Fred::Registry::Domain::get_domain_id(const DomainIdRequest& request)
{
    try
    {
        const auto db_res = [&request]()
        {
            LibFred::OperationContextCreator ctx;
            const auto db_res = ctx.get_conn().exec_params(
                    "SELECT obr.uuid, h.uuid "
                    "FROM object_registry obr "
                    "JOIN history h ON h.id = obr.historyid "
                    "WHERE obr.erdate IS NULL AND "
                          "obr.type = get_object_type_id('domain') AND "
                          "obr.name = LOWER($1::TEXT) "
                    "FOR SHARE OF obr",
                    Database::QueryParams{get_raw_value_from(request.fqdn)});
            ctx.commit_transaction();
            return db_res;
        }();

        if (db_res.size() == 0)
        {
            throw DomainIdReply::Exception::DomainDoesNotExist(request);
        }
        if (1 < db_res.size())
        {
            throw Registry::InternalServerError("too many domains selected by fqdn");
        }
        auto result = DomainIdReply::Data{};
        result.domain_id = Util::make_strong<DomainId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][0])));
        result.domain_history_id = Util::make_strong<DomainHistoryId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][1])));
        return result;
    }
    catch (const DomainIdReply::Exception::DomainDoesNotExist&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
