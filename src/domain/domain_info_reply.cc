/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/domain_info_reply.hh"

#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Domain {

std::string DomainInfoReply::Data::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .add("domain_id", domain_id)
                                 .add("domain_history_id", domain_history_id)
                                 .add("nsset", nsset)
                                 .add("keyset", keyset)
                                 .add("registrant", registrant)
                                 .add("sponsoring_registrar", sponsoring_registrar)
                                 .add("administrative_contacts", administrative_contacts)
                                 .add("expires_at", expires_at)
                                 .add("representative_events", representative_events)
                                 .add("expiration_warning_scheduled_at", expiration_warning_scheduled_at)
                                 .add("outzone_unguarded_warning_scheduled_at", outzone_unguarded_warning_scheduled_at)
                                 .add("outzone_scheduled_at", outzone_scheduled_at)
                                 .add("delete_warning_scheduled_at", delete_warning_scheduled_at)
                                 .add("delete_candidate_scheduled_at", delete_candidate_scheduled_at)
                                 .add("validation_expires_at", validation_expires_at)
                                 .add("outzone_at", outzone_at)
                                 .add("delete_candidate_at", delete_candidate_at)
                                 .finish();
}

DomainInfoReply::Exception::InvalidData::InvalidData(
        const DomainInfoRequest&,
        std::set<std::string> fields)
    : Registry::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

DomainInfoReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const DomainInfoRequest& src)
    : Registry::Exception("domain " + src.to_string() + " does not exist")
{
}

std::string DomainInfoReply::Exception::InvalidData::to_string() const
{
    return Util::StructToString().add("fields", fields)
                                 .finish();
}

DomainInfoReply::Exception::Exception(InvalidData src)
    : reasons{std::move(src)}
{ }

DomainInfoReply::Exception::Exception(DomainDoesNotExist src)
    : reasons{std::move(src)}
{ }

std::string DomainInfoReply::Exception::to_string() const
{
    struct Printer : boost::static_visitor<std::string>
    {
        std::string operator()(const DomainDoesNotExist&) const
        {
            return "domain_does_not_exist";
        }
        std::string operator()(const InvalidData& invalid_data) const
        {
            return Util::StructToString{}.add("invalid_data", invalid_data)
                                         .finish();
        }
    };
    return boost::apply_visitor(Printer{}, reasons);
}

std::string BatchDomainInfoReply::Data::BatchReply::to_string() const
{
    struct Printer : boost::static_visitor<std::string>
    {
        std::string operator()(const DomainInfoReply::Data& data) const
        {
            return Util::StructToString{}.add("data", data)
                                         .finish();
        }
        std::string operator()(const Error& error) const
        {
            return Util::StructToString{}.add("error", error)
                                         .finish();
        }
    };
    return boost::apply_visitor(Printer{}, data_or_error);
}

BatchDomainInfoReply::Error::Error(BatchDomainInfoRequest::Request request, DomainInfoReply::Exception exception)
    : request{std::move(request)},
      exception{std::move(exception)}
{ }

std::string BatchDomainInfoReply::Error::to_string() const
{
    return Util::StructToString{}.add("request", request)
                                 .add("exception", exception)
                                 .finish();
}

std::string BatchDomainInfoReply::Data::to_string() const
{
    return Util::StructToString{}.add("replies", replies)
                                 .finish();
}

std::string DomainIdReply::Data::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("domain_history_id", domain_history_id)
                                 .finish();
}

DomainIdReply::Exception::DomainDoesNotExist::DomainDoesNotExist(const DomainIdRequest& src)
    : Registry::Exception("domain " + src.to_string() + " does not exist")
{
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
