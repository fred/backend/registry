/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DOMAINS_NOTIFY_INFO_REPLY_HH_D241A990256CD2905BE3A9290B0C478A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_DOMAINS_NOTIFY_INFO_REPLY_HH_D241A990256CD2905BE3A9290B0C478A

#include "src/contact/contact_common_types.hh"
#include "src/domain/domain_common_types.hh"
#include "src/pagination_reply.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

struct GetDomainsNotifyInfoReply
{
    struct Data : Util::Printable<Data>
    {
        struct Info : Util::Printable<Info>
        {
            DomainId domain_id;
            std::string fqdn;
            Contact::ContactId registrant_id;
            Contact::ContactHandle registrant_handle;
            std::string registrant_name;
            std::string registrant_organization;
            std::vector<EmailAddress> emails;
            std::vector<EmailAddress> additional_emails;
            std::vector<PhoneNumber> phones;
            std::vector<PhoneNumber> additional_phones;
            std::string to_string() const;
        };
        std::vector<Info> domains;
        boost::optional<PaginationReply> pagination;
        std::string to_string() const;
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//GET_DOMAINS_NOTIFY_INFO_REPLY_HH_D241A990256CD2905BE3A9290B0C478A
