/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/domain/list_domains_by_keyset.hh"

#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "libfred/db_settings.hh"

#include "util/db/db_exceptions.hh"
#include "util/log/log.hh"

#include <cstdint>
#include <limits>
#include <stdexcept>
#include <utility>

namespace {

void set_last_page(
        const boost::optional<Fred::Registry::PaginationRequest>& pagination_request,
        boost::optional<Fred::Registry::PaginationReply>& pagination_reply)
{
    if (pagination_request != boost::none)
    {
        pagination_reply = Fred::Registry::last_page();
    }
}

bool does_keyset_exist(const LibFred::OperationContext& ctx, const Fred::Registry::Keyset::KeysetId& keyset_id)
{
    return 0 < ctx.get_conn().exec_params(
            // clang-format off
            "SELECT "
              "FROM object_registry "
             "WHERE uuid = $1::UUID AND "
                   "type = get_object_type_id('keyset') AND "
                   "erdate IS NULL",
            // clang-format on
            Database::QueryParams{keyset_id}).size();
}

enum class PaginationDirection
{
    forward,
    backward
};

struct InvalidPageSize {};
struct InvalidOrderBy {};

PaginationDirection get_pagination_direction(const Fred::Registry::PaginationRequest& pagination)
{
    if (0 < pagination.page_size)
    {
        return PaginationDirection::forward;
    }
    if (pagination.page_size < 0)
    {
        return PaginationDirection::backward;
    }
    throw InvalidPageSize{};
}

std::string make_page_break(const Fred::Registry::PaginationRequest& pagination, Database::QueryParams& params)
{
    params.push_back(pagination.page_token);
    // clang-format off
    return "page_break AS MATERIALIZED "
           "("
               "SELECT obr.uuid, "
                      "obr.name AS fqdn, "
                      "obr.crdate, "
                      "(SELECT exdate FROM domain WHERE domain.id = obr.id) AS exdate, "
                      "r.handle AS registrar_handle "
                 "FROM object_registry obr "
                 "JOIN object o ON o.id = obr.id "
                 "JOIN registrar r ON r.id = o.clid "
                "WHERE obr.uuid = $" + std::to_string(params.size()) + "::UUID"
           ")";
    // clang-format on
}

std::pair<std::string, std::string> to_column_ascending(Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField field)
{
    switch (field)
    {
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::crdate:
            return std::make_pair("pb.crdate", "obr.crdate");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::exdate:
            return std::make_pair("pb.exdate", "d.exdate");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::fqdn:
            return std::make_pair("pb.fqdn", "obr.name");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::sponsoring_registrar_handle:
            return std::make_pair("pb.registrar_handle", "r.handle");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::unspecified:
            throw InvalidOrderBy{};
    }
    throw std::runtime_error{"unknown order by field"};
}

std::string make_page_break_cmp_expr(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by,
        PaginationDirection pagination_direction)
{
    std::string smaller_value;
    std::string greater_value;
    std::for_each(cbegin(order_by), cend(order_by), [&smaller_value, &greater_value](auto&& column)
    {
        const auto column_expr = to_column_ascending(column.field);
        if (column.direction != Fred::Registry::OrderByDirection::descending)
        {
            smaller_value.append(column_expr.first);
            greater_value.append(column_expr.second);
        }
        else
        {
            smaller_value.append(column_expr.second);
            greater_value.append(column_expr.first);
        }
        smaller_value.append(", ");
        greater_value.append(", ");
    });
    smaller_value.append("pb.uuid");
    greater_value.append("obr.uuid");
    auto expresion = "ROW(" + smaller_value + ") < ROW(" + greater_value + ")";
    switch (pagination_direction)
    {
        case PaginationDirection::forward:
            return expresion;
        case PaginationDirection::backward:
            expresion = "NOT(" + expresion + ")";
            return expresion;
    }
    throw std::runtime_error{"unknown PaginationDirection value"};
}

std::string to_order_by_column(Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField field)
{
    switch (field)
    {
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::crdate:
            return "obr.crdate";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::exdate:
            return "d.exdate";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::fqdn:
            return "1";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::sponsoring_registrar_handle:
            return "r.handle";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::unspecified:
            throw InvalidOrderBy{};
    }
    throw std::runtime_error{"unknown order by field"};
}

std::string make_order_by_part(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by,
        std::string (*get_sql_direction)(Fred::Registry::OrderByDirection))
{
    std::string sql = {};
    std::for_each(cbegin(order_by), cend(order_by), [&sql, &get_sql_direction](auto&& column)
    {
        const auto column_expr = to_order_by_column(column.field);
        sql.append(column_expr + get_sql_direction(column.direction) + ", ");
    });
    sql.append("2" + get_sql_direction(Fred::Registry::OrderByDirection::ascending));
    return sql;
}

std::string normal_order_comparison(Fred::Registry::OrderByDirection direction)
{
    switch (direction)
    {
        case Fred::Registry::OrderByDirection::ascending:
            return {};
        case Fred::Registry::OrderByDirection::descending:
            return " DESC";
    }
    throw std::runtime_error{"invalid OrderByDirection value"};
}

std::string make_order_by_part(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by)
{
    return make_order_by_part(order_by, normal_order_comparison);
}

std::string reverse_order_comparison(Fred::Registry::OrderByDirection direction)
{
    static const auto make_oposite = [](Fred::Registry::OrderByDirection direction)
    {
        switch (direction)
        {
            case Fred::Registry::OrderByDirection::ascending:
                return Fred::Registry::OrderByDirection::descending;
            case Fred::Registry::OrderByDirection::descending:
                return Fred::Registry::OrderByDirection::ascending;
        }
        throw std::runtime_error{"invalid OrderByDirection value"};
    };
    return normal_order_comparison(make_oposite(direction));
}

std::string make_reverse_order_by_part(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by)
{
    return make_order_by_part(order_by, reverse_order_comparison);
}

struct KeysetDoesNotExist {};

template <typename P, typename F>
void fill_in_forward(
        Fred::Registry::Domain::ListDomainsByKeysetReply::Data& reply,
        const Database::Result& dbres,
        P push_back_domain,
        F get_full_count)
{
    if (dbres.size() <= 0)
    {
        return;
    }
    reply.domains.reserve(dbres.size());
    for (std::size_t idx = 0; idx < dbres.size(); ++idx)
    {
        push_back_domain(dbres[idx]);
    }
    if (reply.pagination != boost::none)
    {
        const auto items_left = get_full_count(dbres[0]) - dbres.size();
        if (0 < items_left)
        {
            reply.pagination->items_left = items_left;
            reply.pagination->next_page_token = to_string(get_raw_value_from(reply.domains.back().domain.id));
        }
    }
    FREDLOG_DEBUG("moving forward: success");
}

template <typename P, typename F>
void fill_in_backward(
        Fred::Registry::Domain::ListDomainsByKeysetReply::Data& reply,
        const Database::Result& dbres,
        P push_back_domain,
        F get_full_count,
        std::size_t page_limit)
{
    if (dbres.size() <= 0)
    {
        return;
    }
    const bool last_page = dbres.size() < page_limit;
    const auto page_size = last_page ? dbres.size()
                                     : page_limit - 1;
    reply.domains.reserve(page_size);
    for (std::size_t cnt = 0; cnt < page_size; ++cnt)
    {
        push_back_domain(dbres[page_size - 1 - cnt]); // page_size - 1, page_size - 2, ..., 0
    }
    if (reply.pagination != boost::none)
    {
        const auto items_left = get_full_count(dbres[0]) - page_size;
        if (0 < items_left)
        {
            reply.pagination->items_left = items_left;
            if (page_size != (dbres.size() - 1))
            {
                LIBLOG_WARNING("page_size {} should be {}", page_size, dbres.size() - 1);
            }
            else
            {
                reply.pagination->next_page_token = static_cast<std::string>(dbres[page_size][0]);
            }
        }
    }
    FREDLOG_DEBUG("moving backward: success");
}

#if 0
SELECT domain_id,
       fqdn,
       history_id,
       is_deleted,
       full_count
#endif
Fred::Registry::Domain::ListDomainsByKeysetReply::Data exec_query(
        const std::string& sql,
        const Database::QueryParams& params,
        const Fred::Registry::Keyset::KeysetId& keyset_id,
        const boost::optional<Fred::Registry::PaginationRequest>& pagination,
        std::size_t page_limit,
        bool reverse_order)
{
    LibFred::OperationContextCreator ctx;
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    const bool result_empty = dbres.size() <= 0;
    const bool contact_exists = !result_empty || does_keyset_exist(ctx, keyset_id);
    ctx.commit_transaction();
    auto reply = Fred::Registry::Domain::ListDomainsByKeysetReply::Data{};
    set_last_page(pagination, reply.pagination);
    if (result_empty)
    {
        if (!contact_exists)
        {
            throw KeysetDoesNotExist{};
        }
        return reply;
    }
    const auto push_back_domain = [&](const Database::Row& columns)
    {
        Fred::Registry::Domain::ListDomainsByKeysetReply::Data::Domain domain;
        domain.domain.id = Fred::Registry::Util::make_strong<Fred::Registry::Domain::DomainId>(
                boost::uuids::string_generator{}(static_cast<std::string>(columns[0])));
        domain.domain.fqdn = Fred::Registry::Util::make_strong<Fred::Registry::Domain::Fqdn>(
                static_cast<std::string>(columns[1]));
        domain.domain_history_id = Fred::Registry::Util::make_strong<Fred::Registry::Domain::DomainHistoryId>(
                boost::uuids::string_generator{}(static_cast<std::string>(columns[2])));
        domain.is_deleted = static_cast<bool>(columns[3]);
        reply.domains.push_back(std::move(domain));
    };
    static const auto get_full_count = [](const Database::Row& columns)
    {
        return static_cast<std::uint64_t>(columns[4]);
    };
    if (!reverse_order)
    {
        fill_in_forward(reply, dbres, push_back_domain, get_full_count);
    }
    else
    {
        fill_in_backward(reply, dbres, push_back_domain, get_full_count, page_limit);
    }
    return reply;
}

#if 0
WITH k AS NOT MATERIALIZED
(
    SELECT id
      FROM object_registry
     WHERE uuid = '465dabf1-877a-4187-b6ba-4b92a6549608'::UUID
),
page_break AS MATERIALIZED
(
    SELECT obr.uuid,
           obr.name AS fqdn,
           obr.crdate,
           (SELECT exdate FROM domain WHERE domain.id = obr.id) AS exdate,
           r.handle AS registrar_handle
      FROM object_registry obr
      JOIN object o ON o.id = obr.id
      JOIN registrar r ON r.id = o.clid
     WHERE obr.uuid = '14b0ef91-8a62-4c82-bc93-db75b671d915'::UUID
)
SELECT obr.uuid AS domain_id,
       obr.name AS fqdn,
       h.uuid AS history_id,
       obr.erdate IS NOT NULL AS is_deleted,
       COUNT(*) OVER() AS full_count
  FROM (SELECT DISTINCT d.id, d.exdate
          FROM k
          JOIN domain d ON d.keyset = k.id) AS d(id, exdate)
  JOIN object_registry obr ON obr.id = d.id
  JOIN object o ON o.id = obr.id
  JOIN history h ON h.id = obr.historyid
  JOIN registrar r ON r.id = o.clid,
       page_break pb
 WHERE ROW(pb.crdate, pb.uuid) < ROW(obr.crdate, obr.uuid)
 ORDER BY obr.crdate, 2
 LIMIT 10;
#endif

Fred::Registry::Domain::ListDomainsByKeysetReply::Data get_domains_by_keyset(
        const Fred::Registry::Keyset::KeysetId& keyset_id,
        const boost::optional<Fred::Registry::PaginationRequest>& pagination,
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by)
{
    auto params = Database::QueryParams{keyset_id};
    auto sql = std::string{
            // clang-format off
            "WITH k AS NOT MATERIALIZED "
            "("
                "SELECT id "
                  "FROM object_registry "
                 "WHERE uuid = $1::UUID"
            ")"};
            // clang-format on
    const bool pagination_in_progress = (pagination != boost::none) &&
                                        !pagination->page_token.empty();
    if (pagination_in_progress)
    {
        sql += ", " + make_page_break(*pagination, params);
    }
    sql += " "
           // clang-format off
           "SELECT obr.uuid AS domain_id, "
                  "obr.name AS fqdn, "
                  "h.uuid AS history_id, "
                  "obr.erdate IS NOT NULL AS is_deleted, "
                  "COUNT(*) OVER() AS full_count "
             "FROM (SELECT DISTINCT d.id, d.exdate "
                     "FROM k "
                     "JOIN domain d ON d.keyset = k.id) AS d(id, exdate) "
             "JOIN object_registry obr ON obr.id = d.id "
             "JOIN object o ON o.id = obr.id "
             "JOIN history h ON h.id = obr.historyid "
             "JOIN registrar r ON r.id = o.clid";
            // clang-format on
    if (pagination_in_progress)
    {
        // clang-format off
        sql += ", "
                  "page_break pb "
            "WHERE " + make_page_break_cmp_expr(order_by, get_pagination_direction(*pagination));
        // clang-format on
    }
    const bool reverse_order = (pagination != boost::none) &&
                                (get_pagination_direction(*pagination) == PaginationDirection::backward);
    if (!order_by.empty())
    {
        sql += " ORDER BY " + (!reverse_order ? make_order_by_part(order_by)
                                              : make_reverse_order_by_part(order_by));
    }
    const auto page_limit = [&]() -> std::size_t
    {
        if (pagination == boost::none)
        {
            return 0;
        }
        if (!reverse_order)
        {
            return pagination->page_size;
        }
        return 1 - pagination->page_size; // |page_size| + 1
    }();
    if (pagination != boost::none)
    {
        sql += " LIMIT " + std::to_string(page_limit);
    }
    return exec_query(sql, params, keyset_id, pagination, page_limit, reverse_order);
}

std::string make_page_break_with_history(
        const Fred::Registry::PaginationRequest& pagination,
        Database::QueryParams& params)
{
    params.push_back(pagination.page_token);
    // clang-format off
    return "page_break AS MATERIALIZED "
           "("
               "SELECT obr.uuid, "
                      "obr.name AS fqdn, "
                      "obr.crdate, "
                      "r.handle AS registrar_handle "
                 "FROM object_registry obr "
                 "JOIN object_history o ON o.id = obr.id AND "
                                          "o.historyid = obr.historyid "
                 "JOIN registrar r ON r.id = o.clid "
                "WHERE obr.uuid = $" + std::to_string(params.size()) + "::UUID"
           ")";
    // clang-format on
}

std::pair<std::string, std::string> to_column_ascending_with_history(
        Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField field)
{
    switch (field)
    {
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::crdate:
            return std::make_pair("pb.crdate", "obr.crdate");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::exdate:
            throw InvalidOrderBy{};
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::fqdn:
            return std::make_pair("pb.fqdn", "obr.name");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::sponsoring_registrar_handle:
            return std::make_pair("pb.registrar_handle", "r.handle");
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::unspecified:
            throw InvalidOrderBy{};
    }
    throw std::runtime_error{"unknown order by field"};
}

std::string make_page_break_cmp_expr_with_history(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by,
        PaginationDirection pagination_direction)
{
    std::string smaller_value;
    std::string greater_value;
    std::for_each(cbegin(order_by), cend(order_by), [&smaller_value, &greater_value](auto&& column)
    {
        const auto column_expr = to_column_ascending_with_history(column.field);
        if (column.direction != Fred::Registry::OrderByDirection::descending)
        {
            smaller_value.append(column_expr.first);
            greater_value.append(column_expr.second);
        }
        else
        {
            smaller_value.append(column_expr.second);
            greater_value.append(column_expr.first);
        }
        smaller_value.append(", ");
        greater_value.append(", ");
    });
    smaller_value.append("pb.uuid");
    greater_value.append("obr.uuid");
    auto expresion = "ROW(" + smaller_value + ") < ROW(" + greater_value + ")";
    switch (pagination_direction)
    {
        case PaginationDirection::forward:
            return expresion;
        case PaginationDirection::backward:
            expresion = "NOT(" + expresion + ")";
            return expresion;
    }
    throw std::runtime_error{"unknown PaginationDirection value"};
}

std::string to_order_by_column_with_history(Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField field)
{
    switch (field)
    {
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::crdate:
            return "obr.crdate";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::exdate:
            throw InvalidOrderBy{};
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::fqdn:
            return "1";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::sponsoring_registrar_handle:
            return "r.handle";
        case Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderByField::unspecified:
            throw InvalidOrderBy{};
    }
    throw std::runtime_error{"unknown order by field"};
}

std::string make_order_by_part_with_history(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by,
        std::string (*get_sql_direction)(Fred::Registry::OrderByDirection))
{
    std::string sql = {};
    std::for_each(cbegin(order_by), cend(order_by), [&sql, &get_sql_direction](auto&& column)
    {
        const auto column_expr = to_order_by_column_with_history(column.field);
        sql.append(column_expr + get_sql_direction(column.direction) + ", ");
    });
    sql.append("2" + get_sql_direction(Fred::Registry::OrderByDirection::ascending));
    return sql;
}

std::string make_order_by_part_with_history(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by)
{
    return make_order_by_part_with_history(order_by, normal_order_comparison);
}

std::string make_reverse_order_by_part_with_history(
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by)
{
    return make_order_by_part_with_history(order_by, reverse_order_comparison);
}

#if 0
WITH k AS NOT MATERIALIZED
(
    SELECT id
      FROM object_registry
     WHERE uuid = '465dabf1-877a-4187-b6ba-4b92a6549608'::UUID
),
page_break AS MATERIALIZED
(
    SELECT obr.uuid,
           obr.name AS fqdn,
           obr.crdate,
           r.handle AS registrar_handle
      FROM object_registry obr
      JOIN object_history o ON o.id = obr.id AND
                               o.historyid = obr.historyid
      JOIN registrar r ON r.id = o.clid
     WHERE obr.uuid = '14b0ef91-8a62-4c82-bc93-db75b671d915'::UUID
)
SELECT obr.uuid AS domain_id,
       obr.name AS fqdn,
       h.uuid AS history_id,
       obr.erdate IS NOT NULL AS is_deleted,
       COUNT(*) OVER() AS full_count
  FROM (SELECT DISTINCT dh.id
          FROM k
          JOIN domain_history dh ON dh.keyset = k.id) AS dh(id)
  JOIN object_registry obr ON obr.id = dh.id
  JOIN object_history o ON o.id = obr.id AND
                           o.historyid = obr.historyid
  JOIN history h ON h.id = obr.historyid
  JOIN registrar r ON r.id = o.clid,
       page_break pb
 WHERE ROW(pb.crdate, pb.uuid) < ROW(obr.crdate, obr.uuid)
 ORDER BY obr.crdate, 2
 LIMIT 10;
#endif

Fred::Registry::Domain::ListDomainsByKeysetReply::Data get_domains_by_keyset_with_history(
        const Fred::Registry::Keyset::KeysetId& keyset_id,
        const boost::optional<Fred::Registry::PaginationRequest>& pagination,
        const std::vector<Fred::Registry::Domain::ListDomainsByKeysetRequest::OrderBy>& order_by)
{
    auto params = Database::QueryParams{keyset_id};
    auto sql = std::string{
            // clang-format off
            "WITH k AS NOT MATERIALIZED "
            "("
                "SELECT id "
                  "FROM object_registry "
                 "WHERE uuid = $1::UUID"
            ")"};
            // clang-format on
    const bool pagination_in_progress = (pagination != boost::none) &&
                                        !pagination->page_token.empty();
    if (pagination_in_progress)
    {
        sql += ", " + make_page_break_with_history(*pagination, params);
    }
    // clang-format off
    sql += " "
           "SELECT obr.uuid AS keyset_id, "
                  "obr.name AS keyset_handle, "
                  "h.uuid AS history_id, "
                  "obr.erdate IS NOT NULL AS is_deleted, "
                  "COUNT(*) OVER() AS full_count "
             "FROM (SELECT DISTINCT dh.id "
                     "FROM k "
                     "JOIN domain_history dh ON dh.keyset = k.id) AS dh(id) "
             "JOIN object_registry obr ON obr.id = dh.id "
             "JOIN object_history o ON o.id = obr.id AND "
                                      "o.historyid = obr.historyid "
             "JOIN history h ON h.id = obr.historyid "
             "JOIN registrar r ON r.id = o.clid";
    // clang-format on
    if (pagination_in_progress)
    {
        sql += ", "
                  "page_break pb "
            "WHERE " + make_page_break_cmp_expr_with_history(order_by, get_pagination_direction(*pagination));
    }
    const bool reverse_order = (pagination != boost::none) &&
                                (get_pagination_direction(*pagination) == PaginationDirection::backward);
    if (!order_by.empty())
    {
        sql += " ORDER BY " + (!reverse_order ? make_order_by_part_with_history(order_by)
                                              : make_reverse_order_by_part_with_history(order_by));
    }
    const auto page_limit = [&]() -> std::size_t
    {
        if (pagination == boost::none)
        {
            return 0;
        }
        if (!reverse_order)
        {
            return pagination->page_size;
        }
        return 1 - pagination->page_size; // |page_size| + 1
    }();
    if (pagination != boost::none)
    {
        sql += " LIMIT " + std::to_string(page_limit);
    }
    return exec_query(sql, params, keyset_id, pagination, page_limit, reverse_order);
}

}//namespace {anonymous}

using namespace Fred::Registry::Domain;

ListDomainsByKeysetReply::Data Fred::Registry::Domain::list_domains_by_keyset(const ListDomainsByKeysetRequest& request)
{
    try
    {
        if (!request.aggregate_entire_history)
        {
            return get_domains_by_keyset(request.keyset_id, request.pagination, request.order_by);
        }
        return get_domains_by_keyset_with_history(request.keyset_id, request.pagination, request.order_by);
    }
    catch (const KeysetDoesNotExist&)
    {
        throw ListDomainsByKeysetReply::Exception::KeysetDoesNotExist{request};
    }
    catch (const InvalidPageSize&)
    {
        throw ListDomainsByKeysetReply::Exception::InvalidData{request, {"pagination.page_size"}};
    }
    catch (const InvalidOrderBy&)
    {
        throw ListDomainsByKeysetReply::Exception::InvalidData{request, {"order_by"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception caught: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError{"unknown exception caught"};
    }
    return {};
}
