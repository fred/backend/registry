/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BATCH_DELETE_DOMAINS_HH_488937F85EAE324FFEE6CAD62A84A4DF//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define BATCH_DELETE_DOMAINS_HH_488937F85EAE324FFEE6CAD62A84A4DF

#include "src/domain/batch_delete_domains_reply.hh"
#include "src/domain/batch_delete_domains_request.hh"

#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>


namespace Fred {
namespace Registry {
namespace Domain {

BatchDeleteDomainsReply::Data batch_delete_domains(
        const BatchDeleteDomainsRequest& request,
        const boost::optional<std::string>& by_registrar);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//BATCH_DELETE_DOMAINS_HH_488937F85EAE324FFEE6CAD62A84A4DF
