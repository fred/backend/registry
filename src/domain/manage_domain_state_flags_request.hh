/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANAGE_DOMAIN_STATE_FLAGS_REQUEST_HH_01175F8977C0C4D857D0247F11A7D169//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define MANAGE_DOMAIN_STATE_FLAGS_REQUEST_HH_01175F8977C0C4D857D0247F11A7D169

#include "src/domain/domain_common_types.hh"

#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <map>
#include <vector>


namespace Fred {
namespace Registry {
namespace Domain {

struct ManageDomainStateFlagsRequest : Util::Printable<ManageDomainStateFlagsRequest>
{
    struct Domain : Util::Printable<Domain>
    {
        DomainId domain_id;
        boost::optional<DomainHistoryId> domain_history_id;
        std::string to_string() const;
    };
    struct Validity : Util::Printable<Validity>
    {
        boost::optional<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>> valid_from; // missing value means "now"
        boost::optional<std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>> valid_to; // missing value means "unlimited"
        std::string to_string() const;
    };
    struct ToCreate : Util::Printable<ToCreate>
    {
        std::map<std::string, Validity> state_flags;
        std::vector<Domain> domains;
        std::string to_string() const;
    };
    std::vector<ToCreate> state_flag_requests_to_create;
    std::vector<DomainStateFlagRequest> state_flag_requests_to_close;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//MANAGE_DOMAIN_STATE_FLAGS_REQUEST_HH_01175F8977C0C4D857D0247F11A7D169
