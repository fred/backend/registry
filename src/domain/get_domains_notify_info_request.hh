/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DOMAINS_NOTIFY_INFO_REQUEST_HH_0393F0739775CD27E75FA0BFC1E8725E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_DOMAINS_NOTIFY_INFO_REQUEST_HH_0393F0739775CD27E75FA0BFC1E8725E

#include "src/pagination_request.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

struct TimeInterval
{
    std::chrono::system_clock::time_point lower_bound;
    std::chrono::system_clock::time_point upper_bound;
};

enum class LifecycleEvent
{
    domain_outzone,
    domain_delete
};

struct GetDomainsNotifyInfoRequest : Util::Printable<GetDomainsNotifyInfoRequest>
{
    TimeInterval event_time;
    LifecycleEvent event;
    boost::optional<PaginationRequest> pagination;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//GET_DOMAINS_NOTIFY_INFO_REQUEST_HH_0393F0739775CD27E75FA0BFC1E8725E
