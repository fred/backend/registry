/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_DOMAINS_BY_NSSET_REQUEST_HH_F894E698C5514E17934786CCEF02BAEC
#define LIST_DOMAINS_BY_NSSET_REQUEST_HH_F894E698C5514E17934786CCEF02BAEC

#include "src/common_types.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/pagination_request.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct ListDomainsByNssetRequest : Util::Printable<ListDomainsByNssetRequest>
{
    enum class OrderByField
    {
        unspecified,
        crdate,
        exdate,
        fqdn,
        sponsoring_registrar_handle
    };
    struct OrderBy : Util::Printable<OrderBy>
    {
        explicit OrderBy(OrderByField, OrderByDirection) noexcept;
        OrderByField field;
        OrderByDirection direction;
        std::string to_string() const;
    };
    Nsset::NssetId nsset_id;
    boost::optional<PaginationRequest> pagination;
    std::vector<OrderBy> order_by;
    bool aggregate_entire_history;
    std::string to_string() const;
};

std::string to_string(ListDomainsByNssetRequest::OrderByField);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
