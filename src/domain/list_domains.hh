/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_DOMAINS_HH_81FB62D864AC4A25B4040B6D80DAD3FA
#define LIST_DOMAINS_HH_81FB62D864AC4A25B4040B6D80DAD3FA

#include "src/domain/list_domains_reply.hh"
#include "src/domain/list_domains_request.hh"

#include "src/util/printable.hh"

namespace Fred {
namespace Registry {
namespace Domain {

ListDomainsReply::Data list_domains(const ListDomainsRequest& _request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
