/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAIN_LIFE_CYCLE_STAGE_REQUEST_HH_902C7953E439683C8FACA1002AAA74EB//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define DOMAIN_LIFE_CYCLE_STAGE_REQUEST_HH_902C7953E439683C8FACA1002AAA74EB

#include "src/domain/domain_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

struct DomainLifeCycleStageRequest : Util::Printable<DomainLifeCycleStageRequest>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    Fqdn fqdn;
    boost::optional<TimePoint> time;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//DOMAIN_LIFE_CYCLE_STAGE_REQUEST_HH_902C7953E439683C8FACA1002AAA74EB
