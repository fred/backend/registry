/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DOMAINS_BY_CONTACT_HH_F723DE4700236974E74A7D149AC3C0DE//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_DOMAINS_BY_CONTACT_HH_F723DE4700236974E74A7D149AC3C0DE

#include "src/domain/get_domains_by_contact_reply.hh"
#include "src/domain/get_domains_by_contact_request.hh"


namespace Fred {
namespace Registry {
namespace Domain {

GetDomainsByContactReply::Data get_domains_by_contact(const GetDomainsByContactRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//GET_DOMAINS_BY_CONTACT_HH_F723DE4700236974E74A7D149AC3C0DE
