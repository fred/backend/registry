/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/search_domain.hh"

#include "src/util/enum_sequence.hh"
#include "src/util/into.hh"
#include "src/util/measure_duration.hh"
#include "src/util/sql_exec_in_thread.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "util/log/log.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <cmath>

#include <algorithm>
#include <iterator>
#include <memory>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

std::string SearchDomainRequest::to_string() const
{
    return Util::StructToString().add("query_values", query_values)
                                 .add("limit", limit)
                                 .add("searched_items", searched_items)
                                 .finish();
}

std::string SearchDomainReply::Data::to_string() const
{
    return Util::StructToString().add("most_similar_domains", most_similar_domains)
                                 .add("result_count", result_count)
                                 .add("searched_items", searched_items)
                                 .finish();
}

std::string SearchDomainReply::Data::Result::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("matched_items", matched_items)
                                 .finish();
}

std::string SearchDomainReply::Data::FuzzyValue::to_string() const
{
    return Util::StructToString().add("lower_estimate", lower_estimate)
                                 .add("upper_estimate", upper_estimate)
                                 .finish();
}

std::string SearchDomainHistoryRequest::to_string() const
{
    return Util::StructToString().add("query_values", query_values)
        .add("max_number_of_domains", max_number_of_domains)
        .add("data_valid_from", data_valid_from)
        .add("data_valid_to", data_valid_to)
        .add("searched_items", searched_items)
        .finish();
}

std::string SearchDomainHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("result_count", result_count)
        .add("results", results)
        .add("searched_items", searched_items)
        .finish();
}

std::string SearchDomainHistoryReply::Data::FuzzyValue::to_string() const
{
    return Util::StructToString().add("lower_estimate", lower_estimate)
        .add("upper_estimate", upper_estimate)
        .finish();
}

std::string SearchDomainHistoryReply::Data::Result::to_string() const
{
    return Util::StructToString().add("object_id", object_id)
        .add("histories", histories)
        .finish();
}

std::string SearchDomainHistoryReply::Data::Result::HistoryPeriod::to_string() const
{
    return Util::StructToString().add("matched_items", matched_items)
        .add("valid_from", valid_from)
        .add("valid_to", valid_to)
        .add("domain_history_ids", domain_history_ids)
        .finish();
}

namespace {

std::vector<std::string> split(const std::string& src, const std::regex& delimiter)
{
    constexpr auto stuff_between_matches = -1;
    std::sregex_token_iterator begin(src.cbegin(), src.cend(), delimiter, stuff_between_matches);
    static const std::sregex_token_iterator end;
    std::vector<std::string> result;
    std::copy_if(begin, end, std::back_inserter(result), [](const std::string& item) { return !item.empty(); });
    return result;
}

std::vector<std::string> to_words(const std::string& by_spaces_delimited_words)
{
    return split(by_spaces_delimited_words, std::regex("\\s+"));
}

struct Delimiter
{
    template <typename T>
    explicit Delimiter(T&& src)
        : value{std::forward<T>(src)} { }
    std::string value;
};

class Collector
{
public:
    explicit Collector(Delimiter&& delimiter)
        : delimiter_{std::move(delimiter.value)}
    { }
    template <typename T>
    Collector& append(T&& item)
    {
        if (!sum_.empty())
        {
            sum_.append(delimiter_);
        }
        sum_.append(std::forward<T>(item));
        return *this;
    }
    const std::string& get_sum() const noexcept
    {
        return sum_;
    }
private:
    const std::string delimiter_;
    std::string sum_;
};

class CollectorWithParentheses
{
public:
    explicit CollectorWithParentheses(Delimiter&& delimiter)
        : delimiter_{std::move(delimiter.value)},
        delimiter_in_use_{false}
    { }
    template <typename T>
    CollectorWithParentheses& append(T&& item)
    {
        if (!sum_.empty())
        {
            if (!delimiter_in_use_)
            {
                sum_ = "(" + sum_ + ")";
                delimiter_in_use_ = true;
            }
            sum_.append(delimiter_ + "(" + std::forward<T>(item) + ")");
        }
        else
        {
            sum_ = std::forward<T>(item);
        }
        return *this;
    }
    const std::string& get_sum() const noexcept
    {
        return sum_;
    }
private:
    const std::string delimiter_;
    bool delimiter_in_use_;
    std::string sum_;
};

using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;

struct HistoryLimit
{
    boost::optional<TimePoint> data_valid_from;
    boost::optional<TimePoint> data_valid_to;
};

struct ChronologyViolationError : std::exception
{
    const char* what() const noexcept override
    {
        return "chronology violation";
    }
};

HistoryLimit make_history_limit(const boost::optional<TimePoint>& from, const boost::optional<TimePoint>& to)
{
    if ((from != boost::none) && (to != boost::none))
    {
        if (*to < *from)
        {
            throw ChronologyViolationError();
        }
    }
    return HistoryLimit{from, to};
}

struct Record
{
    struct ObjectId
    {
        unsigned long long numeric_id;
        DomainId domain_id;
    };
    struct Period
    {
        DomainHistoryId history_id;
        boost::optional<DomainHistoryId> next_history_id;
        TimePoint valid_from;
        boost::optional<TimePoint> valid_to;
    };
    struct Data
    {
        std::size_t value_length;
        struct WordSimilarity
        {
            double partial; //result of word_similarity(word, value)
            double complete;//result of similarity(word, value)
        };
        struct ExpressionSimilarity//one expression consists of one or more words
        {
            std::vector<WordSimilarity> words_similarities;
        };
        std::vector<ExpressionSimilarity> similarities;
    };
    ObjectId object;
    Period period;
    std::map<DomainItem, Data> item_data;
};

struct ExpressionMatch
{
    DomainItem item_type;
    std::size_t item_value_length;
    std::vector<Record::Data::WordSimilarity> words_similarities;
    static bool is_similar(double similarity)
    {
        static constexpr double similarity_threshold{0.299999};
        return similarity_threshold < similarity;
    }
    bool is_match() const
    {
        for (const auto& word_similarity : words_similarities)
        {
            if (!is_similar(word_similarity.partial))
            {
                return false;
            }
        }
        return true;
    }
    auto sum_similarity() const
    {
        Record::Data::WordSimilarity sum{ 0.0, 0.0 };
        for (const auto& similarity : words_similarities)
        {
            sum.partial += similarity.partial * similarity.partial;
            sum.complete += similarity.complete * similarity.complete;
        }
        sum.partial = std::sqrt(sum.partial / words_similarities.size());
        sum.complete = std::sqrt(sum.complete / words_similarities.size());
        return sum;
    }
    friend bool operator<(const ExpressionMatch& lhs, const ExpressionMatch& rhs)
    {
        if (!lhs.is_match())
        {
            return rhs.is_match();
        }
        if (!rhs.is_match())
        {
            return false;
        }
        const auto lhs_sum_similarity = lhs.sum_similarity();
        const auto rhs_sum_similarity = rhs.sum_similarity();
        if (lhs_sum_similarity.partial < rhs_sum_similarity.partial)
        {
            return true;
        }
        if (rhs_sum_similarity.partial < lhs_sum_similarity.partial)
        {
            return false;
        }
        if (rhs.item_value_length < lhs.item_value_length)
        {
            return true;
        }
        if (lhs.item_value_length < rhs.item_value_length)
        {
            return false;
        }
        return lhs_sum_similarity.complete < rhs_sum_similarity.complete;
    }
    friend bool operator==(const ExpressionMatch& lhs, const ExpressionMatch& rhs)
    {
        return !(lhs < rhs) &&
               !(rhs < lhs);
    }
    friend bool operator!=(const ExpressionMatch& lhs, const ExpressionMatch& rhs)
    {
        return !(lhs == rhs);
    }
};

auto get_best_matches(const std::map<DomainItem, Record::Data>& item_data)
{
    std::vector<ExpressionMatch> best_matches;
    for (const auto& item_name_data : item_data)
    {
        ExpressionMatch expression_match;
        expression_match.item_type = item_name_data.first;
        expression_match.item_value_length = item_name_data.second.value_length;
        const bool first_item = best_matches.empty();
        if (first_item)
        {
            best_matches.reserve(item_name_data.second.similarities.size());
        }
        std::size_t expression_idx = 0;
        for (const auto& similarity : item_name_data.second.similarities)
        {
            expression_match.words_similarities = similarity.words_similarities;
            if (first_item)
            {
                best_matches.push_back(expression_match);
            }
            else if (best_matches[expression_idx] < expression_match)
            {
                best_matches[expression_idx] = expression_match;
            }
            ++expression_idx;
        }
    }
    return best_matches;
}

bool all_expressions_found(const std::vector<ExpressionMatch>& matches)
{
    for (const auto& expression_match : matches)
    {
        if (!expression_match.is_match())
        {
            return false;
        }
    }
    return true;
}

bool all_expressions_equal(const std::vector<ExpressionMatch>& lhs, const std::vector<ExpressionMatch>& rhs)
{
    if (lhs.size() != rhs.size())
    {
        return false;
    }
    for (std::size_t idx = 0; idx < lhs.size(); ++idx)
    {
        if (lhs[idx] != rhs[idx])
        {
            return false;
        }
    }
    return true;
}

std::unique_ptr<std::vector<Record>> combine(const std::vector<Record>& a, const std::vector<Record>& b)
{
    auto sum = std::make_unique<std::vector<Record>>();
    sum->reserve(a.size() + b.size());
    auto a_top = a.begin();
    auto b_top = b.begin();
    while ((a_top != a.end()) && (b_top != b.end()))
    {
        if (a_top->object.numeric_id < b_top->object.numeric_id)
        {
            sum->push_back(*a_top);
            ++a_top;
        }
        else if (b_top->object.numeric_id < a_top->object.numeric_id)
        {
            sum->push_back(*b_top);
            ++b_top;
        }
        else
        {
            if (a_top->period.valid_from < b_top->period.valid_from)
            {
                sum->push_back(*a_top);
                ++a_top;
            }
            else if (b_top->period.valid_from < a_top->period.valid_from)
            {
                sum->push_back(*b_top);
                ++b_top;
            }
            else
            {
                Record c = *a_top;
                c.item_data.insert(b_top->item_data.begin(), b_top->item_data.end());
                sum->push_back(c);
                ++a_top;
                ++b_top;
            }
        }
    }
    while (a_top != a.end())
    {
        sum->push_back(*a_top);
        ++a_top;
    }
    while (b_top != b.end())
    {
        sum->push_back(*b_top);
        ++b_top;
    }
    return sum;
}

template <DomainItem item>
Record make_record_for_item(
    const Database::Result::Row& row,
    const std::vector<std::size_t>& numbers_of_words)
{
    Record record;
    record.object.numeric_id = static_cast<unsigned long long>(row[0]);
    record.object.domain_id = Util::make_strong<Registry::Domain::DomainId>(
        boost::uuids::string_generator()(static_cast<std::string>(row[1])));
    record.period.history_id = Util::make_strong<Registry::Domain::DomainHistoryId>(
        boost::uuids::string_generator()(static_cast<std::string>(row[2])));
    if (!row[3].isnull())
    {
        record.period.next_history_id = Util::make_strong<Registry::Domain::DomainHistoryId>(
            boost::uuids::string_generator()(static_cast<std::string>(row[3])));
    }
    record.period.valid_from = Util::Into<TimePoint>::from(static_cast<std::string>(row[4]));
    if (!row[5].isnull())
    {
        record.period.valid_to = Util::Into<TimePoint>::from(static_cast<std::string>(row[5]));
    }
    Record::Data data;
    data.value_length = static_cast<std::size_t>(row[6]);
    data.similarities.reserve(numbers_of_words.size());
    auto column_idx = 7;
    for (const auto words : numbers_of_words)
    {
        Record::Data::ExpressionSimilarity expression_similarity;
        expression_similarity.words_similarities.reserve(words);
        for (std::size_t word_idx = 0; word_idx < words; ++word_idx, column_idx += 2)
        {
            Record::Data::WordSimilarity word_similarity;
            word_similarity.partial = static_cast<double>(row[column_idx]);
            word_similarity.complete = static_cast<double>(row[column_idx + 1]);
            expression_similarity.words_similarities.push_back(word_similarity);
        }
        data.similarities.push_back(expression_similarity);
    }
    record.item_data[item] = std::move(data);
    return record;
}

template <DomainItem> class SearchIn;

#if 0
SELECT c.id,
       obr.uuid,
       h.uuid,
       hn.uuid,
       h.valid_from,
       h.valid_to,
       length(obr.name),
       word_similarity(unaccented.v1w1,obr.name),similarity(unaccented.v1w1,obr.name),
       word_similarity(unaccented.v1w2,obr.name),similarity(unaccented.v1w2,obr.name),
       word_similarity(unaccented.v2w1,obr.name),similarity(unaccented.v2w1,obr.name)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     object_registry obr
JOIN domain_history c ON c.id=obr.id
JOIN history h ON h.id=c.historyid
LEFT JOIN history hn ON hn.id=h.next
WHERE (((unaccented.v1w1 <% obr.name) AND
        (unaccented.v1w2 <% obr.name)) OR
       (unaccented.v2w1 <% obr.name)) AND
      obr.type=get_object_type_id('domain') AND
      ('2012-01-01 0:00:00'::TIMESTAMP<=obr.erdate OR obr.erdate IS NULL) AND
      ('2012-01-01 0:00:00'::TIMESTAMP<=h.valid_to OR h.valid_to IS NULL) AND
      (h.valid_from<'2016-01-01 0:00:00'::TIMESTAMP)
ORDER BY 1,5;

SELECT c.id,
       obr.uuid,
       h.uuid,
       NULL,
       h.valid_from,
       NULL,
       length(obr.name),
       word_similarity(unaccented.v1w1,obr.name),similarity(unaccented.v1w1,obr.name),
       word_similarity(unaccented.v1w2,obr.name),similarity(unaccented.v1w2,obr.name),
       word_similarity(unaccented.v2w1,obr.name),similarity(unaccented.v2w1,obr.name)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     object_registry obr
JOIN domain_history c ON c.id=obr.id
JOIN history h ON h.id=c.historyid
WHERE (((unaccented.v1w1 <% obr.name) AND
       (unaccented.v1w2 <% obr.name)) OR
       (unaccented.v2w1 <% obr.name)) AND
      h.valid_to IS NULL AND
      obr.erdate IS NULL AND
      obr.type=get_object_type_id('domain')
ORDER BY 1,5;
#endif
template <>
class SearchIn<DomainItem::fqdn>
{
public:
    explicit SearchIn(const std::vector<std::size_t>& numbers_of_words, const boost::optional<HistoryLimit>& history_limit)
    {
        Collector what(Delimiter(","));
        Collector unaccented_what(Delimiter(","));
        Collector unaccented_as(Delimiter(","));
        CollectorWithParentheses where(Delimiter(" OR "));
        int param_idx = 0;
        int value_idx = 0;
        for (const auto words : numbers_of_words)
        {
            CollectorWithParentheses sub_where(Delimiter(" AND "));
            const std::string value_name = "v" + std::to_string(value_idx + 1) + "w";
            for (unsigned word_idx = 0; word_idx < words; ++word_idx, ++param_idx)
            {
                const std::string value_word_name = value_name + std::to_string(word_idx + 1);
                what.append("word_similarity(unaccented." + value_word_name + ",obr.name),"
                            "similarity(unaccented." + value_word_name + ",obr.name)");
                unaccented_what.append("f_unaccent($" + std::to_string(param_idx + 1) + "::TEXT)");
                unaccented_as.append(value_word_name);
                sub_where.append("unaccented." + value_word_name + " <% obr.name");
            }
            where.append(sub_where.get_sum());
            ++value_idx;
        }
        const bool search_in_history = history_limit != boost::none;
        if (search_in_history)
        {
            sql_ = "SELECT c.id,"
                          "obr.uuid,"
                          "h.uuid,"
                          "hn.uuid,"
                          "h.valid_from,"
                          "h.valid_to,"
                          "length(obr.name)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "object_registry obr "
                   "JOIN domain_history c ON c.id=obr.id "
                   "JOIN history h ON h.id=c.historyid "
                   "LEFT JOIN history hn ON hn.id=h.next "
                   "WHERE (" + where.get_sum() + ") AND "
                          "obr.type=get_object_type_id('domain')";
            const bool search_limited_by_date =
                (history_limit->data_valid_from != boost::none) ||
                (history_limit->data_valid_to != boost::none);
            if (search_limited_by_date)
            {
                const bool has_lower_limit = (history_limit->data_valid_from != boost::none);
                if (has_lower_limit)
                {
                    ++param_idx;
                    const std::string lower_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (" + lower_limit + "<obr.erdate OR obr.erdate IS NULL)"
                                                     " AND (" + lower_limit + "<h.valid_to OR h.valid_to IS NULL)";
                }
                const bool has_upper_limit = (history_limit->data_valid_to != boost::none);
                if (has_upper_limit)
                {
                    ++param_idx;
                    const std::string upper_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (h.valid_from<=" + upper_limit + ")";
                }
            }
            sql_ += " ORDER BY 1,5";
        }
        else
        {
            sql_ = "SELECT c.id,"
                          "obr.uuid,"
                          "h.uuid,"
                          "NULL,"
                          "h.valid_from,"
                          "NULL,"
                          "length(obr.name)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "object_registry obr "
                   "JOIN domain_history c ON c.id=obr.id "
                   "JOIN history h ON h.id=c.historyid "
                   "WHERE (" + where.get_sum() + ") AND "
                          "h.valid_to IS NULL AND "
                          "obr.erdate IS NULL AND "
                          "obr.type=get_object_type_id('domain') "
                   "ORDER BY 1,5";
        }
    }
    const std::string& get_sql() const noexcept
    {
        return sql_;
    }
    static Record make_record(
        const Database::Result::Row& row,
        const std::vector<std::size_t>& numbers_of_words)
    {
        return make_record_for_item<DomainItem::fqdn>(row, numbers_of_words);
    }
private:
    std::string sql_;
};

template <DomainItem ...items>
using DomainColumns = std::integer_sequence<DomainItem, items...>;

template <typename T>
using SearchableItems = Util::EnumSequence<DomainColumns<DomainItem::fqdn>, T>;

class RunQuery
{
public:
    RunQuery(const std::set<DomainItem>& searched_items,
             const std::vector<std::size_t>& numbers_of_words,
             const boost::optional<HistoryLimit>& history_limit,
             const Database::QueryParams& params)
        : searched_items_(searched_items),
          numbers_of_words_(numbers_of_words),
          history_limit_(history_limit),
          params_(params)
    { }
    template <DomainItem column>
    void operator()(Util::SqlInThreadSolver& running_sql) const
    {
        const bool to_search = 0 < searched_items_.count(column);
        if (to_search)
        {
            running_sql = Util::sql_exec_in_thread(
                    SearchIn<column>{
                            numbers_of_words_,
                            history_limit_}.get_sql(),
                    params_,
                    Util::repeatable_read_read_only_transaction());
        }
    }
private:
    const std::set<DomainItem> searched_items_;
    const std::vector<std::size_t> numbers_of_words_;
    const boost::optional<HistoryLimit> history_limit_;
    const Database::QueryParams params_;
};

class GetResult
{
public:
    explicit GetResult(const std::vector<std::size_t>& numbers_of_words)
        : numbers_of_words_{numbers_of_words},
          sum_matches_{std::make_unique<std::vector<Record>>()}
    { }
    template <DomainItem column>
    void operator()(Util::SqlInThreadSolver& running_sql)
    {
        if (running_sql.is_joinable())
        {
            const Database::Result task_result = Util::SqlInThreadSolver::Result(std::move(running_sql)).get_result();
            if (0 < task_result.size())
            {
                auto matches = std::make_unique<std::vector<Record>>();
                matches->reserve(task_result.size());
                for (std::size_t row_idx = 0; row_idx < task_result.size(); ++row_idx)
                {
                    matches->push_back(SearchIn<column>::make_record(task_result[row_idx], numbers_of_words_));
                }
                if (sum_matches_->empty())
                {
                    sum_matches_ = std::move(matches);
                }
                else
                {
                    auto combined_matches = combine(*sum_matches_, *matches);
                    sum_matches_ = std::move(combined_matches);
                }
            }
        }
    }
    const std::vector<Record>& get_sum_matches() const noexcept
    {
        return *sum_matches_;
    }
    void release_sum_matches() noexcept
    {
        sum_matches_.release();
    }
private:
    const std::vector<std::size_t>& numbers_of_words_;
    std::unique_ptr<std::vector<Record>> sum_matches_;
};

struct SearchDataRequest
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> max_number_of_domains;
    boost::optional<HistoryLimit> history_limit;
    std::set<DomainItem> searched_items;
};

auto make_request(const SearchDomainRequest& src)
{
    SearchDataRequest request;
    request.query_values = src.query_values;
    request.max_number_of_domains = src.limit;
    request.history_limit = boost::none;
    request.searched_items = src.searched_items;
    return request;
}

auto make_request(const SearchDomainHistoryRequest& src)
{
    SearchDataRequest request;
    request.query_values = src.query_values;
    request.max_number_of_domains = src.max_number_of_domains;
    request.history_limit = make_history_limit(src.data_valid_from, src.data_valid_to);
    request.searched_items = src.searched_items;
    return request;
}

auto search_domain(const SearchDataRequest& request)
{
    Database::QueryParams params;
    const std::vector<std::size_t> numbers_of_words = [&]()
    {
        std::vector<std::size_t> numbers_of_words;
        numbers_of_words.reserve(request.query_values.size());
        for (const auto& by_spaces_delimited_words : request.query_values)
        {
            const auto words = to_words(by_spaces_delimited_words);
            if (!words.empty())
            {
                params.insert(params.end(), words.cbegin(), words.cend());
            }
            numbers_of_words.push_back(words.size());
        }
        return numbers_of_words;
    }();
    if (request.history_limit != boost::none)
    {
        if (request.history_limit->data_valid_from != boost::none)
        {
            params.push_back(Util::Into<std::string>::from(*(request.history_limit->data_valid_from)));
        }
        if (request.history_limit->data_valid_to != boost::none)
        {
            params.push_back(Util::Into<std::string>::from(*(request.history_limit->data_valid_to)));
        }
    }
    SearchableItems<Util::SqlInThreadSolver> running_sql{};

    visit(running_sql, RunQuery(request.searched_items, numbers_of_words, request.history_limit, params));
    GetResult data_collector{numbers_of_words};
    {
        const struct LogDuration : Util::MeasureDuration<LogDuration>
        {
            static void measure_completed(Time duration_ms)
            {
                FREDLOG_DEBUG("data collected in " + std::to_string(duration_ms) + "ms");
            }
        } log_duration{};
        visit(running_sql, data_collector);
    }
    const struct LogDuration : Util::MeasureDuration<LogDuration>
    {
        static void measure_completed(Time duration_ms)
        {
            FREDLOG_DEBUG("matches completed in " + std::to_string(duration_ms) + "ms");
        }
    } log_duration{};
    struct Match
    {
        explicit Match(const Record::ObjectId& _object)
            : object{_object},
            histories{},
            similarity{0.0},
            value_length{0}
        { }
        Record::ObjectId object;
        struct History
        {
            std::vector<ExpressionMatch> expressions;
            std::vector<Record::Period> periods;
        };
        std::vector<History> histories;
        double similarity;
        std::size_t value_length;
        void append_period(const Record::Period& period)
        {
            histories.back().periods.push_back(period);
        }
        void append_history(std::vector<ExpressionMatch>&& expressions_best_matches, const Record::Period& period)
        {
            History history;
            history.expressions = std::move(expressions_best_matches);
            history.periods.push_back(period);
            histories.push_back(std::move(history));
            this->update_similarity();
        }
        void update_similarity()
        {
            double new_similarity{0.0};
            double best_similarity{0.0};
            std::size_t value_length_of_most_similar{0};
            for (const auto& expression : histories.back().expressions)
            {
                const auto partial_similarity = expression.sum_similarity().partial;
                new_similarity += partial_similarity * partial_similarity;
                if (best_similarity < partial_similarity)
                {
                    best_similarity = partial_similarity;
                    value_length_of_most_similar = expression.item_value_length;
                }
                else if (best_similarity == partial_similarity)
                {
                    if ((value_length_of_most_similar == 0) ||
                        (expression.item_value_length < value_length_of_most_similar))
                    {
                        value_length_of_most_similar = expression.item_value_length;
                    }
                }
            }
            if (similarity <= new_similarity)
            {
                similarity = new_similarity;
                value_length = value_length_of_most_similar;
            }
        }
    };
    std::vector<Match> matches_by_similarity{};
    matches_by_similarity.reserve(data_collector.get_sum_matches().size());
    for (const auto& record : data_collector.get_sum_matches())
    {
        std::vector<ExpressionMatch> expressions_best_matches = get_best_matches(record.item_data);
        if (all_expressions_found(expressions_best_matches))
        {
            if (matches_by_similarity.empty() || (matches_by_similarity.back().object.numeric_id != record.object.numeric_id))
            {
                Match match{record.object};
                match.append_history(std::move(expressions_best_matches), record.period);
                matches_by_similarity.push_back(std::move(match));
            }
            else
            {
                const bool period_extends_history =
                    (matches_by_similarity.back().histories.back().periods.back().next_history_id != boost::none) &&
                    (get_raw_value_from(*matches_by_similarity.back().histories.back().periods.back().next_history_id) ==
                     get_raw_value_from(record.period.history_id));
                if (period_extends_history &&
                    all_expressions_equal(expressions_best_matches, matches_by_similarity.back().histories.back().expressions))
                {
                    matches_by_similarity.back().append_period(record.period);
                }
                else
                {
                    matches_by_similarity.back().append_history(std::move(expressions_best_matches), record.period);
                }
            }
        }
    }
    data_collector.release_sum_matches();
    struct BySimilarityDescByLengthAsc
    {
        bool operator()(const Match& lhs, const Match& rhs) const
        {
            return std::make_tuple(rhs.similarity, lhs.value_length, rhs.object.numeric_id) <
                   std::make_tuple(lhs.similarity, rhs.value_length, lhs.object.numeric_id);
        }
    };
    std::sort(matches_by_similarity.begin(), matches_by_similarity.end(), BySimilarityDescByLengthAsc());
    const auto result_count = matches_by_similarity.size();
    if ((request.max_number_of_domains != boost::none) &&
        (*request.max_number_of_domains < result_count))
    {
        matches_by_similarity.erase(matches_by_similarity.begin() + *request.max_number_of_domains, matches_by_similarity.end());
    }
    struct Result
    {
        std::vector<Match> matches;
        std::size_t result_count;
    } result{ std::move(matches_by_similarity), result_count };
    return result;
}

}//namespace Fred::Registry::Domain::{anonymous}

SearchDomainReply::Data search_domain(const SearchDomainRequest& request)
{
    const auto found = search_domain(make_request(request));
    SearchDomainReply::Data result;
    for (const auto& match : found.matches)
    {
        SearchDomainReply::Data::Result domain{};
        domain.domain_id = match.object.domain_id;
        for (const auto& history : match.histories)
        {
            for (const auto& item : history.expressions)
            {
                domain.matched_items.insert(item.item_type);
            }
        }
        result.most_similar_domains.push_back(std::move(domain));
    }
    result.result_count.lower_estimate = found.result_count;
    result.result_count.upper_estimate = found.result_count;
    result.searched_items = request.searched_items;
    return result;
}

SearchDomainHistoryReply::Data search_domain(const SearchDomainHistoryRequest& request)
{
    try
    {
        const auto found = search_domain(make_request(request));
        SearchDomainHistoryReply::Data result;
        for (const auto& match : found.matches)
        {
            SearchDomainHistoryReply::Data::Result domain{};
            domain.object_id = match.object.domain_id;
            for (const auto& history : match.histories)
            {
                SearchDomainHistoryReply::Data::Result::HistoryPeriod history_period;
                history_period.valid_from = history.periods.front().valid_from;
                history_period.valid_to = history.periods.back().valid_to;
                for (const auto& item : history.expressions)
                {
                    history_period.matched_items.insert(item.item_type);
                }
                for (const auto& period : history.periods)
                {
                    history_period.domain_history_ids.push_back(period.history_id);
                }
                domain.histories.push_back(std::move(history_period));
            }
            result.results.push_back(std::move(domain));
        }
        result.result_count.lower_estimate = found.result_count;
        result.result_count.upper_estimate = found.result_count;
        result.searched_items = request.searched_items;
        return result;
    }
    catch (const ChronologyViolationError&)
    {
        throw SearchDomainHistoryReply::Exception::ChronologyViolation(request);
    }
    catch (const Database::ResultFailed&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

SearchDomainHistoryReply::Exception::ChronologyViolation::ChronologyViolation(const SearchDomainHistoryRequest&)
    : Registry::Exception("chronology violation")
{ }

}//namespace Fred::Registry::Domain
}//namespace Fred::Registry
}//namespace Fred
