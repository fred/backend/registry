/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DOMAIN_INFO_REQUEST_HH_29BEAD02A01B4ABCB19FA16D0EC65150
#define DOMAIN_INFO_REQUEST_HH_29BEAD02A01B4ABCB19FA16D0EC65150

#include "src/domain/domain_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct DomainInfoRequest
{
    DomainId domain_id;
    boost::optional<DomainHistoryId> domain_history_id;
    std::string to_string() const;
};

struct BatchDomainInfoRequest : Util::Printable<BatchDomainInfoRequest>
{
    struct Request : Util::Printable<Request>
    {
        std::string domain_id;
        std::string domain_history_id;
        std::string to_string() const;
    };
    std::vector<Request> requests;
    std::string to_string() const;
};

struct DomainIdRequest : Util::Printable<DomainIdRequest>
{
    Fqdn fqdn;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
