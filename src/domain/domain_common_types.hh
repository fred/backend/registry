/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAIN_COMMON_TYPES_HH_4EF409C26304470EBAD33B8C4C64206D
#define DOMAIN_COMMON_TYPES_HH_4EF409C26304470EBAD33B8C4C64206D

#include "src/common_types.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"
#include "src/exceptions.hh"
#include "src/uuid.hh"

#include <chrono>
#include <stdexcept>
#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

enum class DomainContactRole
{
    unspecified,
    registrant, // role_holder
    administrative,
    keyset_technical,
    nsset_technical
};

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

using Fqdn = Util::StrongString<struct Fqdn_Tag, Util::Skill::Printable>;

using DomainId = Uuid<struct DomainIdTag>;
using DomainHistoryId = Uuid<struct DomainHistoryIdTag>;
using DomainStateFlagRequest = Uuid<struct DomainStateFlagRequestTag>;

struct DomainLightInfo : Util::Printable<DomainLightInfo>
{
    DomainId id;
    Fqdn fqdn;
    std::string to_string() const;
};

struct ExpirationTimestamp : Util::Printable<ExpirationTimestamp>
{
    std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> timestamp;
    std::string to_string() const;
};

struct DomainLifeCycleTimestamp : Util::Printable<DomainLifeCycleTimestamp>
{
    std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> timestamp;
    std::string to_string() const;
};

struct DomainLifeCycleMilestone : Util::Printable<DomainLifeCycleMilestone>
{
    std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds> timestamp;
    std::string to_string() const;
};

using EmailAddress = Util::StrongString<struct EmailAddressTag_>;
using PhoneNumber = Util::StrongString<struct PhoneNumberTag_>;

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
