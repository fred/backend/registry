/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/batch_delete_domains.hh"

#include "libfred/db_settings.hh"
#include "libfred/poll/create_poll_message.hh"
#include "libfred/poll/message_type.hh"
#include "libfred/registrable_object/domain/auction/create_auction.hh"
#include "libfred/registrable_object/domain/delete_domain.hh"
#include "libfred/registrable_object/domain/get_domain_state.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrar/info_registrar.hh"
#include "libfred/zone/info_zone.hh"
#include "util/db/db_exceptions.hh"

#include <boost/optional.hpp>

#include <algorithm>
#include <exception>
#include <map>

namespace Fred {
namespace Registry {
namespace Domain {

namespace {

class ZoneInfoCache
{
public:
    explicit ZoneInfoCache(const LibFred::OperationContext& ctx)
        : ctx_{ctx}
    { }
    bool auction_enabled(const std::string& zone_fqdn)
    {
        const auto iter = auction_enabled_.find(zone_fqdn);
        if (iter != end(auction_enabled_))
        {
            return iter->second;
        }
        const bool result = boost::apply_visitor(AuctionEnabled{}, LibFred::Zone::InfoZone{zone_fqdn}.exec(ctx_));
        auction_enabled_[zone_fqdn] = result;
        return result;
    }
private:
    struct AuctionEnabled : boost::static_visitor<bool>
    {
        bool operator()(boost::blank) const noexcept
        {
            return false;
        }
        template <typename T>
        bool operator()(T&& data) const noexcept
        {
            return data.auction_enabled;
        }
    };
    const LibFred::OperationContext& ctx_;
    std::map<std::string, bool> auction_enabled_;
};

template <typename F>
auto protect_by_savepoint(const LibFred::OperationContext& ctx, F&& fnc)
{
    class Savepoint
    {
    public:
        Savepoint(const LibFred::OperationContext& ctx)
            : ctx_{ctx}
        {
            ctx_.get_conn().exec("SAVEPOINT _batch_delete_domains_savepoint_");
        }
        ~Savepoint()
        {
            try
            {
                ctx_.get_conn().exec("RELEASE SAVEPOINT _batch_delete_domains_savepoint_");
            }
            catch (...) { }
        }
        void rollback() const
        {
            try
            {
                ctx_.get_conn().exec("ROLLBACK TO SAVEPOINT _batch_delete_domains_savepoint_");
            }
            catch (...) { }
        }
    private:
        const LibFred::OperationContext& ctx_;
    } savepoint{ctx};
    try
    {
        return std::forward<F>(fnc)(ctx);
    }
    catch (...)
    {
        savepoint.rollback();
        throw;
    }
}

constexpr auto no_registrar_id = 0;

bool not_a_registrar(const LibFred::InfoRegistrarData& registrar_info)
{
    return registrar_info.id == no_registrar_id;
}

boost::optional<BatchDeleteDomainsReply::NotDeleted> try_to_delete(
        const LibFred::OperationContext& ctx,
        const BatchDeleteDomainsRequest::Domain& domain,
        const LibFred::InfoRegistrarData& by_registrar,
        ZoneInfoCache& zone_info)
{
    const auto not_deleted = [&](BatchDeleteDomainsReply::NotDeleted::Reason reason)
    {
        BatchDeleteDomainsReply::NotDeleted result{};
        result.domain_id = domain.domain_id;
        result.reason = reason;
        return result;
    };
    if (not_a_registrar(by_registrar))
    {
        return not_deleted(BatchDeleteDomainsReply::NotDeleted::Reason::insufficient_permissions);
    }
    try
    {
        const auto libfred_domain_uuid =
                ::Util::make_strong<LibFred::RegistrableObject::Domain::DomainUuid>(
                        get_raw_value_from(domain.domain_id));
        const auto domain_info = protect_by_savepoint(
                ctx,
                [&](const LibFred::OperationContext& ctx)
                {
                    return LibFred::InfoDomainByUuid{libfred_domain_uuid}.exec(
                            LibFred::OperationContextLockingForUpdate{ctx}).info_domain_data;
                });
        const bool is_sponsoring_registrar = (domain_info.sponsoring_registrar_handle == by_registrar.handle);
        const bool insufficient_permissions =
                !is_sponsoring_registrar &&
                (by_registrar.system.isnull() || !by_registrar.system.get_value());
        if (insufficient_permissions)
        {
            return not_deleted(BatchDeleteDomainsReply::NotDeleted::Reason::insufficient_permissions);
        }
        if (get_raw_value_from(domain.domain_history_id) != get_raw_value_from(domain_info.history_uuid))
        {
            return not_deleted(BatchDeleteDomainsReply::NotDeleted::Reason::domain_history_id_mismatch);
        }
        const auto domain_state = protect_by_savepoint(
                ctx,
                [&](const LibFred::OperationContext& ctx)
                {
                    return LibFred::RegistrableObject::Domain::GetDomainStateByUuid{libfred_domain_uuid}
                            .exec(ctx);
                });
        if (domain_state.are_set_any_of<LibFred::RegistrableObject::Domain::ServerDeleteProhibited>())
        {
            return not_deleted(BatchDeleteDomainsReply::NotDeleted::Reason::domain_blocked);
        }

        protect_by_savepoint(
                ctx,
                [&](const LibFred::OperationContext& ctx)
                {
                    LibFred::DeleteDomainById{domain_info.id}.exec(ctx);
                    if (zone_info.auction_enabled(domain_info.zone.handle))
                    {
                        LibFred::Domain::Auction::CreateAuction{domain_info.fqdn}.exec(ctx);
                    }
                    if (!is_sponsoring_registrar)
                    {
                        LibFred::Poll::CreatePollMessage<::LibFred::Poll::MessageType::delete_domain>()
                            .exec(ctx, domain_info.historyid);
                    }
                });
        return boost::none;
    }
    catch (const LibFred::InfoDomainByUuid::Exception& e)
    {
        if (e.is_set_unknown_domain_uuid())
        {
            return not_deleted(BatchDeleteDomainsReply::NotDeleted::Reason::domain_does_not_exist);
        }
        throw;
    }
}

} // namespace Fred::Registry::Domain::{anonymous}

BatchDeleteDomainsReply::Data batch_delete_domains(
        const BatchDeleteDomainsRequest& request,
        const boost::optional<std::string>& by_registrar)
{
    try
    {
        BatchDeleteDomainsReply::Data reply;
        LibFred::OperationContextCreator ctx;
        const auto registrar_info = [&]()
        {
            if (by_registrar != boost::none)
            {
                try
                {
                    return protect_by_savepoint(
                            ctx,
                            [&](const LibFred::OperationContext& ctx)
                            {
                                return LibFred::InfoRegistrarByHandle{*by_registrar}.exec(ctx).info_registrar_data;
                            });
                }
                catch (...) { }
            }
            LibFred::InfoRegistrarData no_registrar{};
            no_registrar.id = no_registrar_id;
            return no_registrar;
        }();
        ZoneInfoCache zone_info{ctx};
        std::for_each(begin(request.domains), end(request.domains), [&](auto&& domain)
                {
                    const auto result = try_to_delete(ctx, domain, registrar_info, zone_info);
                    if (result != boost::none)
                    {
                        reply.not_deleted_domains.push_back(*result);
                    }
                });
        ctx.commit_transaction();
        return reply;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }

}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
