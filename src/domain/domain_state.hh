/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DOMAIN_STATE_HH_9B528419466E403290036CE3364F1910
#define DOMAIN_STATE_HH_9B528419466E403290036CE3364F1910

#include "src/domain/domain_common_types.hh"
#include "src/exceptions.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

struct DomainStateRequest : Util::Printable<DomainStateRequest>
{
    DomainId domain_id;
    std::string to_string() const;
};

struct DomainState : Util::Printable<DomainState>
{
    std::map<std::string, bool> flag_presents;
    std::string to_string() const;
};

struct DomainStateReply
{
    struct Data : Util::Printable<Data>
    {
        DomainId domain_id;
        boost::optional<DomainHistoryId> history_id;
        DomainState state;
        std::string to_string() const;
    };
    struct Exception
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const DomainStateRequest& request);
        };
    };
};

DomainStateReply::Data domain_state(const DomainStateRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
