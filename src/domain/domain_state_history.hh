/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DOMAIN_STATE_HISTORY_HH_A317DD5AF02B4C6897FCBA07F6A4FCA3
#define DOMAIN_STATE_HISTORY_HH_A317DD5AF02B4C6897FCBA07F6A4FCA3

#include "src/domain/domain_common_types.hh"
#include "src/domain/domain_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <map>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct DomainStateHistoryRequest : Util::Printable<DomainStateHistoryRequest>
{
    DomainStateHistoryRequest(
            const DomainId& _domain_id,
            const DomainHistoryInterval& _history);
    DomainId domain_id;
    DomainHistoryInterval history;
    std::string to_string() const;
};

struct DomainStateHistory : Util::Printable<DomainStateHistory>
{
    using TimePoint = DomainHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        TimePoint valid_from;
        std::vector<bool> presents;
        std::string to_string() const;
    };
    std::vector<std::string> flags_names;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct DomainStateHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        DomainId domain_id;
        DomainStateHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const DomainStateHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const DomainStateHistoryRequest& request);
        };
    };
};

DomainStateHistoryReply::Data domain_state_history(const DomainStateHistoryRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
