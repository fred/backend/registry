/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/get_domains_notify_info_reply.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Domain {

std::string GetDomainsNotifyInfoReply::Data::Info::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("fqdn", fqdn)
                                 .add("registrant_id", registrant_id)
                                 .add("registrant_handle", registrant_handle)
                                 .add("registrant_name", registrant_name)
                                 .add("registrant_organization", registrant_organization)
                                 .add("emails", emails)
                                 .add("additional_emails", additional_emails)
                                 .add("phones", phones)
                                 .add("additional_phones", additional_phones)
                                 .finish();
}

std::string GetDomainsNotifyInfoReply::Data::to_string() const
{
    return Util::StructToString().add("domains", domains)
                                 .add("pagination", pagination)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
