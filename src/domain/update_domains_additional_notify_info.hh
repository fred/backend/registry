/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_HH_A09B4D325706643C9C17AB134B295553//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_HH_A09B4D325706643C9C17AB134B295553

#include "src/domain/update_domains_additional_notify_info_request.hh"

namespace Fred {
namespace Registry {
namespace Domain {

void update_domains_additional_notify_info(const UpdateDomainsAdditionalNotifyInfoRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_HH_A09B4D325706643C9C17AB134B295553
