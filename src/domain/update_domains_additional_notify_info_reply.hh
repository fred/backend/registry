/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_REPLY_HH_6982A3986E76B59F7516A8536BC31C93//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_REPLY_HH_6982A3986E76B59F7516A8536BC31C93

#include "src/domain/domain_common_types.hh"
#include "src/domain/update_domains_additional_notify_info_request.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

struct UpdateDomainsAdditionalNotifyInfoReply
{
    struct Exception
    {
        struct DomainDoesNotExist : Registry::Exception
        {
            explicit DomainDoesNotExist(const UpdateDomainsAdditionalNotifyInfoRequest& src);
        };
        struct InvalidDomainContactInfo : Registry::Exception
        {
            explicit InvalidDomainContactInfo(std::string invalid_value);
            std::string invalid_value;
        };
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//UPDATE_DOMAINS_ADDITIONAL_NOTIFY_INFO_REPLY_HH_6982A3986E76B59F7516A8536BC31C93
