/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_DOMAINS_BY_CONTACT_REQUEST_HH_57EBC81BF91FFCE5718CC48DD668B73E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define GET_DOMAINS_BY_CONTACT_REQUEST_HH_57EBC81BF91FFCE5718CC48DD668B73E

#include "src/contact/contact_common_types.hh"
#include "src/pagination_request.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace Domain {

struct GetDomainsByContactRequest : Util::Printable<GetDomainsByContactRequest>
{
    Contact::ContactId contact_id;
    boost::optional<PaginationRequest> pagination;
    std::string order_by;
    std::string fqdn_filter;
    bool include_holder;
    bool include_admin_contact;
    bool include_keyset_tech_contact;
    bool include_nsset_tech_contact;
    bool include_deleted_domains;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//GET_DOMAINS_BY_CONTACT_REQUEST_HH_57EBC81BF91FFCE5718CC48DD668B73E
