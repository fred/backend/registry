/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BATCH_DELETE_DOMAINS_REQUEST_HH_EEBD729DB1705BFDE12F9627062F4E6E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define BATCH_DELETE_DOMAINS_REQUEST_HH_EEBD729DB1705BFDE12F9627062F4E6E

#include "src/domain/domain_common_types.hh"

#include "src/util/printable.hh"


namespace Fred {
namespace Registry {
namespace Domain {

struct BatchDeleteDomainsRequest : Util::Printable<BatchDeleteDomainsRequest>
{
    struct Domain : Util::Printable<Domain>
    {
        DomainId domain_id;
        DomainHistoryId domain_history_id;
        std::string to_string() const;
    };
    std::vector<Domain> domains;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif//BATCH_DELETE_DOMAINS_REQUEST_HH_EEBD729DB1705BFDE12F9627062F4E6E
