/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_DOMAINS_BY_NSSET_REPLY_HH_4B0BF96B86E147E0AA44EBB0D8133D28
#define LIST_DOMAINS_BY_NSSET_REPLY_HH_4B0BF96B86E147E0AA44EBB0D8133D28

#include "src/domain/list_domains_by_nsset_request.hh"

#include "src/domain/domain_common_types.hh"
#include "src/exceptions.hh"
#include "src/pagination_reply.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Domain {

struct ListDomainsByNssetReply
{
    struct Data : Util::Printable<Data>
    {
        struct Domain : Util::Printable<Domain>
        {
            DomainLightInfo domain;
            DomainHistoryId domain_history_id;
            bool is_deleted;
            std::string to_string() const;
        };
        std::vector<Domain> domains;
        boost::optional<PaginationReply> pagination;
        std::string to_string() const;
    };
    struct Exception
    {
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const ListDomainsByNssetRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
        struct NssetDoesNotExist : Registry::Exception
        {
            explicit NssetDoesNotExist(const ListDomainsByNssetRequest& request);
        };
    };
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
