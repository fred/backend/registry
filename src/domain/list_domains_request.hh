/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_DOMAINS_REQUEST_HH_999F1D917E5641BA8AE4C9298AC2349C
#define LIST_DOMAINS_REQUEST_HH_999F1D917E5641BA8AE4C9298AC2349C

#include "src/util/printable.hh"

namespace Fred {
namespace Registry {
namespace Domain {

struct ListDomainsRequest : Util::Printable<ListDomainsRequest>
{
    std::string zone;
    std::string to_string() const;
};

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
