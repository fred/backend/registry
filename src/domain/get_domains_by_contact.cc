/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/get_domains_by_contact.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/db_settings.hh"

#include "util/db/db_exceptions.hh"
#include "util/log/log.hh"

#include <cstdint>
#include <limits>
#include <stdexcept>
#include <utility>

namespace {

#if 0

WITH c AS NOT MATERIALIZED (
    SELECT id
    FROM object_registry
    WHERE uuid = '9918b4bf-b718-4360-aa25-2e660b7697a9'),
     page_break AS MATERIALIZED (
    SELECT obr.uuid, obr.name AS fqdn, obr.crdate::DATE, dh.exdate, r.handle AS registrar_handle
    FROM object_registry obr
    JOIN domain_history dh ON dh.id = obr.id
    JOIN object_history oh ON oh.id = obr.id AND
                              oh.historyid = dh.historyid
    JOIN registrar r ON r.id = oh.clid
    WHERE obr.uuid = 'ad0ea2c4-a055-4493-ab93-d0e2bcb0cf12'::UUID AND
          dh.historyid = 30940229),
     cd AS NOT MATERIALIZED (
    SELECT d.id AS domain_id, 't'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n
    FROM c
    JOIN domain d ON d.registrant = c.id
    UNION ALL
    SELECT dcm.domainid AS domain_id, 'f'::BOOL AS h, 't'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n
    FROM c
    JOIN domain_contact_map dcm ON dcm.contactid = c.id
    UNION ALL
    SELECT d.id AS domain_id, 'f'::BOOL AS h, 'f'::BOOL AS a, 't'::BOOL AS k, 'f'::BOOL AS n
    FROM c
    JOIN keyset_contact_map kcm ON kcm.contactid = c.id
    JOIN domain d ON d.keyset = kcm.keysetid
    UNION ALL
    SELECT d.id AS domain_id, 'f'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 't'::BOOL AS n
    FROM c
    JOIN nsset_contact_map ncm ON ncm.contactid = c.id
    JOIN domain d ON d.nsset = ncm.nssetid),
     d AS NOT MATERIALIZED (
    SELECT obr.id, obr.historyid AS history_id, obr.uuid AS uuid, obr.name AS fqdn, BOOL_OR(h) AS holder, BOOL_OR(a) AS admin_c, BOOL_OR(k) AS keyset_tech_c, BOOL_OR(n) AS nsset_tech_c
    FROM cd
    JOIN object_registry obr ON obr.id = cd.domain_id
    GROUP BY 1, 2)
SELECT d.uuid, d.history_id, d.fqdn, h.uuid AS history_uuid, d.holder, d.admin_c, d.keyset_tech_c, d.nsset_tech_c, COUNT(*) OVER() AS full_count
FROM d
JOIN object_registry obr ON obr.id = d.id
JOIN domain ON domain.id = obr.id
JOIN object o ON o.id = obr.id
JOIN registrar r ON r.id = o.clid
JOIN history h ON h.id = d.history_id,
     page_break pb
WHERE ROW(pb.crdate, pb.uuid) < ROW(obr.crdate::DATE, obr.uuid) AND
      LOWER(obr.name) LIKE REGEXP_REPLACE('*', '\*', '%', 'g')
ORDER BY obr.crdate::DATE, 1
LIMIT 10;

SELECT obr.uuid, obr.name AS fqdn, obr.crdate::DATE, dh.exdate::DATE, r.handle AS registrar_handle
FROM object_registry obr
JOIN domain_history dh ON dh.id = obr.id
JOIN object_history oh ON oh.id = obr.id AND
                          oh.historyid = dh.historyid
JOIN registrar r ON r.id = oh.clid
WHERE obr.uuid = 'ad0ea2c4-a055-4493-ab93-d0e2bcb0cf12'::UUID AND
      dh.historyid = 30940229;

WITH c AS NOT MATERIALIZED
(
    SELECT id
      FROM object_registry
     WHERE uuid = '9918b4bf-b718-4360-aa25-2e660b7697a9'
),
page_break AS MATERIALIZED
(
    SELECT obr.uuid, obr.name AS fqdn, obr.crdate::DATE, dh.exdate, r.handle AS registrar_handle
      FROM object_registry obr
      JOIN domain_history dh ON dh.id = obr.id
      JOIN object_history oh ON oh.id = obr.id AND
                                oh.historyid = dh.historyid
      JOIN registrar r ON r.id = oh.clid
     WHERE obr.uuid = 'ad0ea2c4-a055-4493-ab93-d0e2bcb0cf12'::UUID AND
           dh.historyid = 30940229
),
cd AS NOT MATERIALIZED
(
    SELECT d.id AS domain_id, 't'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n
      FROM c
      JOIN domain_history d ON d.registrant = c.id
      JOIN history h ON h.id = d.historyid
     WHERE h.next IS NULL
 UNION ALL
    SELECT dcm.domainid AS domain_id, 'f'::BOOL AS h, 't'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n
      FROM c
      JOIN domain_contact_map_history dcm ON dcm.contactid = c.id
      JOIN history h ON h.id = dcm.historyid
     WHERE h.next IS NULL
 UNION ALL
    SELECT d.id AS domain_id, 'f'::BOOL AS h, 'f'::BOOL AS a, 't'::BOOL AS k, 'f'::BOOL AS n
      FROM c
      JOIN keyset_contact_map kcm ON kcm.contactid = c.id
      JOIN domain_history d ON d.keyset = kcm.keysetid
      JOIN history h ON h.id = d.historyid
     WHERE h.next IS NULL
 UNION ALL
    SELECT d.id AS domain_id, 'f'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 't'::BOOL AS n
      FROM c
      JOIN nsset_contact_map ncm ON ncm.contactid = c.id
      JOIN domain_history d ON d.nsset = ncm.nssetid
      JOIN history h ON h.id = d.historyid
     WHERE h.next IS NULL
),
d AS NOT MATERIALIZED
(
    SELECT domain_id AS id, BOOL_OR(h) AS holder, BOOL_OR(a) AS admin_c, BOOL_OR(k) AS keyset_tech_c, BOOL_OR(n) AS nsset_tech_c
      FROM cd
  GROUP BY 1
)
SELECT obr.uuid, obr.historyid, h.uuid AS history_uuid, d.holder, d.admin_c, d.keyset_tech_c, d.nsset_tech_c, obr.erdate IS NOT NULL AS is_deleted, COUNT(*) OVER() AS full_count
  FROM d
  JOIN object_registry obr ON obr.id = d.id
  JOIN domain_history dh ON dh.id = obr.id AND dh.historyid = obr.historyid
  JOIN object_history o ON o.id = obr.id AND o.historyid = obr.historyid
  JOIN registrar r ON r.id = o.clid
  JOIN history h ON h.id = obr.historyid,
       page_break pb
 WHERE ROW(pb.crdate, pb.uuid) < ROW(obr.crdate::DATE, obr.uuid) AND
       LOWER(obr.name) LIKE REGEXP_REPLACE('*', '\*', '%', 'g')
 ORDER BY obr.crdate::DATE, 1
 LIMIT 10;

#endif

std::string make_sql_cd_part(const Fred::Registry::Domain::GetDomainsByContactRequest& request)
{
    std::string sql;
    const auto append = [&](const auto& query)
    {
        if (!sql.empty())
        {
            sql += " UNION ALL ";
        }
        sql += query;
    };
    if (request.include_holder)
    {
        if (request.include_deleted_domains)
        {
            append("SELECT d.id AS domain_id, h.id AS history_id, "
                          "'t'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n "
                     "FROM c "
                     "JOIN domain_history d ON d.registrant = c.id "
                     "JOIN history h ON h.id = d.historyid "
                    "WHERE h.next IS NULL");
        }
        else
        {
            append("SELECT d.id AS domain_id, "
                          "'t'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n "
                     "FROM c "
                     "JOIN domain d ON d.registrant = c.id");
        }
    }
    if (request.include_admin_contact)
    {
        if (request.include_deleted_domains)
        {
            append("SELECT dcm.domainid AS domain_id, h.id AS history_id, "
                          "'f'::BOOL AS h, 't'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n "
                     "FROM c "
                     "JOIN domain_contact_map_history dcm ON dcm.contactid = c.id "
                     "JOIN history h ON h.id = dcm.historyid "
                    "WHERE h.next IS NULL");
        }
        else
        {
            append("SELECT dcm.domainid AS domain_id, "
                          "'f'::BOOL AS h, 't'::BOOL AS a, 'f'::BOOL AS k, 'f'::BOOL AS n "
                     "FROM c "
                     "JOIN domain_contact_map dcm ON dcm.contactid = c.id");
        }
    }
    if (request.include_keyset_tech_contact)
    {
        if (request.include_deleted_domains)
        {
            append("SELECT d.id AS domain_id, h.id AS history_id, "
                          "'f'::BOOL AS h, 'f'::BOOL AS a, 't'::BOOL AS k, 'f'::BOOL AS n "
                     "FROM c "
                     "JOIN keyset_contact_map kcm ON kcm.contactid = c.id "
                     "JOIN domain_history d ON d.keyset = kcm.keysetid "
                     "JOIN history h ON h.id = d.historyid "
                    "WHERE h.next IS NULL");
        }
        else
        {
            append("SELECT d.id AS domain_id, "
                          "'f'::BOOL AS h, 'f'::BOOL AS a, 't'::BOOL AS k, 'f'::BOOL AS n "
                     "FROM c "
                     "JOIN keyset_contact_map kcm ON kcm.contactid = c.id "
                     "JOIN domain d ON d.keyset = kcm.keysetid");
        }
    }
    if (request.include_nsset_tech_contact)
    {
        if (request.include_deleted_domains)
        {
            append("SELECT d.id AS domain_id, h.id AS history_id, "
                          "'f'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 't'::BOOL AS n "
                     "FROM c "
                     "JOIN nsset_contact_map ncm ON ncm.contactid = c.id "
                     "JOIN domain_history d ON d.nsset = ncm.nssetid "
                     "JOIN history h ON h.id = d.historyid "
                    "WHERE h.next IS NULL");
        }
        else
        {
            append("SELECT d.id AS domain_id, "
                          "'f'::BOOL AS h, 'f'::BOOL AS a, 'f'::BOOL AS k, 't'::BOOL AS n "
                     "FROM c "
                     "JOIN nsset_contact_map ncm ON ncm.contactid = c.id "
                     "JOIN domain d ON d.nsset = ncm.nssetid");
        }
    }
    return sql;
}

enum class DataField
{
    create_date,
    expiration_date,
    fqdn,
    sponsoring_registrar_handle
};

enum class Direction
{
    ascending,
    descending
};

struct OrderBy
{
    DataField data_field;
    Direction direction;
};

struct InvalidOrderBy : std::exception { };

DataField make_data_field(const std::string& str, bool skip_flag)
{
    const std::string::size_type begin = skip_flag ? 1 : 0;
    const std::string::size_type size = str.size() - begin;
    const auto is_field = [&](const auto& field)
    {
        return str.compare(begin, size, field) == 0;
    };
    if (is_field("crdate"))
    {
        return DataField::create_date;
    }
    if (is_field("exdate"))
    {
        return DataField::expiration_date;
    }
    if (is_field("fqdn"))
    {
        return DataField::fqdn;
    }
    if (is_field("registrar"))
    {
        return DataField::sponsoring_registrar_handle;
    }
    struct UnsupportedDataField : InvalidOrderBy
    {
        const char* what() const noexcept override { return "unsupported data field"; }
    };
    throw UnsupportedDataField{};
}

OrderBy make_order_by(const std::string& str)
{
    if (str.empty())
    {
        return OrderBy{DataField::fqdn, Direction::ascending};
    }
    static constexpr char ascending_flag = '+';
    static constexpr char descending_flag = '-';
    const auto get_data_field = [](const std::string& str)
    {
        const bool skip = str.front() == ascending_flag || str.front() == descending_flag;
        return make_data_field(str, skip);
    };
    const auto get_direction = [](const std::string& str)
    {
        if (str.front() == descending_flag)
        {
            return Direction::descending;
        }
        return Direction::ascending;
    };
    return OrderBy{get_data_field(str), get_direction(str)};
}

struct PageToken
{
    Fred::Registry::Domain::DomainId domain_id;
    unsigned long long history_id;
};

auto make_domain_id(std::string::const_iterator begin, std::string::const_iterator end)
{
    using DomainId = Fred::Registry::Domain::DomainId;
    return Fred::Registry::Util::make_strong<DomainId>(boost::uuids::string_generator{}(begin, end));
}

auto make_history_id(const std::string& str)
{
    std::size_t end = 0;
    auto history_id = std::stoull(str, &end);
    if (end < str.length())
    {
        throw std::runtime_error{"invalid history id"};
    }
    return history_id;
}

PageToken make_page_token(const std::string& str)
{
    static constexpr auto uuid_str_length = std::string::size_type{36};
    if (str.length() < uuid_str_length)
    {
        throw std::runtime_error{"page token too short"};
    }
    auto page_token = PageToken{
            make_domain_id(str.begin(), str.begin() + uuid_str_length),
            make_history_id(str.substr(uuid_str_length))};
    return page_token;
}

enum class Move
{
    forward,
    backward
};

struct PageBreak
{
    std::string with_part;
    std::string where_part;
};

PageBreak make_page_break(
        const std::string& page_token_str,
        OrderBy order_by,
        Database::QueryParams& params,
        Move page_direction)
{
    auto result = PageBreak{ "", "" };
    if (page_token_str.empty())
    {
        return result;
    }
    const auto page_token = make_page_token(page_token_str);
    params.emplace_back(get_raw_value_from(page_token.domain_id));
    const auto domain_id_idx = params.size();
    params.emplace_back(page_token.history_id);
    const auto history_id_idx = params.size();
    result.with_part =
            "SELECT obr.uuid, obr.name AS fqdn, obr.crdate::DATE, dh.exdate, r.handle AS registrar_handle "
            "FROM object_registry obr "
            "JOIN domain_history dh ON dh.id = obr.id "
            "JOIN object_history oh ON oh.id = obr.id AND "
                                      "oh.historyid = dh.historyid "
            "JOIN registrar r ON r.id = oh.clid "
            "WHERE obr.uuid = $" + std::to_string(domain_id_idx) + "::UUID AND "
                  "dh.historyid = $" + std::to_string(history_id_idx) + "::INT";
    const auto fields = [](DataField data_field)
    {
        switch (data_field)
        {
            case DataField::create_date:
                return std::make_tuple("pb.crdate", "obr.crdate::DATE");
            case DataField::expiration_date:
                return std::make_tuple("pb.exdate", "domain.exdate");
            case DataField::fqdn:
                return std::make_tuple("pb.fqdn", "obr.name");
            case DataField::sponsoring_registrar_handle:
                return std::make_tuple("pb.registrar_handle", "r.handle");
        }
        throw std::runtime_error{"unknown data field"};
    }(order_by.data_field);
    result.where_part = [&]()
    {
        auto compare = page_direction == Move::forward ? "<" : ">=";
        switch (order_by.direction)
        {
            case Direction::ascending:
                return std::string() + "ROW(" + std::get<0>(fields) + ", pb.uuid) " + compare + " "
                                       "ROW(" + std::get<1>(fields) + ", obr.uuid)";
            case Direction::descending:
                return std::string() + "ROW(" + std::get<1>(fields) + ", pb.uuid) " + compare + " "
                                       "ROW(" + std::get<0>(fields) + ", obr.uuid)";
        }
        throw std::runtime_error{"unknown direction"};
    }();
    return result;
}

std::string make_filter_condition(const std::string& fqdn_filter, Database::QueryParams& params)
{
    auto sql = std::string{""};
    if (!fqdn_filter.empty())
    {
        params.emplace_back(fqdn_filter);
        const auto filter_idx = params.size();
        sql = "LOWER(obr.name) LIKE REGEXP_REPLACE($" + std::to_string(filter_idx) + "::TEXT, '\\*', '%', 'g')";
    }
    return sql;
}

std::string make_order_by_part(OrderBy order_by, Move page_direction)
{
    const auto sql_field = [](DataField data_field) -> std::string
    {
        switch (data_field)
        {
            case DataField::create_date:
                return "obr.crdate::DATE";
            case DataField::expiration_date:
                return "domain.exdate";
            case DataField::fqdn:
                return "obr.name";
            case DataField::sponsoring_registrar_handle:
                return "r.handle";
        }
        throw std::runtime_error{"unknown data field"};
    }(order_by.data_field);
    const auto direction =
            (order_by.direction == Direction::descending && page_direction == Move::forward) ||
            (order_by.direction == Direction::ascending && page_direction == Move::backward)
                ? " DESC"
                : "";
    return "ORDER BY " + sql_field + direction + ", 1";
}

void set_last_page(
        const Fred::Registry::Domain::GetDomainsByContactRequest& request,
        Fred::Registry::Domain::GetDomainsByContactReply::Data& reply)
{
    if (request.pagination != boost::none)
    {
        reply.pagination = Fred::Registry::last_page();
    }
}

bool does_contact_exist(const LibFred::OperationContext& ctx, const Fred::Registry::Contact::ContactId& contact_id)
{
    return 0 < ctx.get_conn().exec_params(
            "SELECT 0 "
            "FROM object_registry "
            "WHERE uuid = $1::UUID AND "
                  "type = get_object_type_id('contact') AND "
                  "erdate IS NULL",
            Database::QueryParams{contact_id}).size();
}

}//namespace {anonymous}

using namespace Fred::Registry::Domain;

GetDomainsByContactReply::Data Fred::Registry::Domain::get_domains_by_contact(const GetDomainsByContactRequest& request)
{
    try
    {
        auto reply = GetDomainsByContactReply::Data{};
        set_last_page(request, reply);
        const auto sql_cd_part = make_sql_cd_part(request);
        if (sql_cd_part.empty())
        {
            return reply;
        }
        const auto order_by = [&]()
        {
            try
            {
                return make_order_by(request.order_by);
            }
            catch (const InvalidOrderBy& e)
            {
                FREDLOG_DEBUG("make_order_by(" + request.order_by + ") failure: " + e.what());
                throw GetDomainsByContactReply::Exception::InvalidOrderBy{request};
            }
        }();
        auto params = Database::QueryParams{request.contact_id};
        auto page_break = PageBreak{ "", "" };
        static constexpr auto no_limit = std::numeric_limits<std::int32_t>::max();
        auto limit = std::uint32_t{no_limit};
        auto page_direction = Move::forward;
        if (request.pagination != boost::none)
        {
            if (request.pagination->page_size == 0)
            {
                static constexpr auto default_page_size = std::int32_t{1000};
                limit = default_page_size;
            }
            else if (0 < request.pagination->page_size)
            {
                limit = request.pagination->page_size;
            }
            else
            {
                page_direction = Move::backward;
                limit = -(request.pagination->page_size - 1);
            }
            page_break = make_page_break(request.pagination->page_token, order_by, params, page_direction);
        }
        const auto filter_condition = make_filter_condition(request.fqdn_filter, params);
        const auto sql_order_by_part = make_order_by_part(order_by, page_direction);
        std::string sql =
                "WITH c AS NOT MATERIALIZED ("
                    "SELECT id "
                    "FROM object_registry "
                    "WHERE uuid = $1::UUID AND "
                          "type = get_object_type_id('contact') AND "
                          "erdate IS NULL), ";
        if (!page_break.with_part.empty())
        {
            sql += "page_break AS MATERIALIZED (" + page_break.with_part + "), ";
        }
        sql +=  "cd AS NOT MATERIALIZED (" + sql_cd_part + "), "
                "d AS NOT MATERIALIZED ("
                    "SELECT domain_id AS id, BOOL_OR(h) AS holder, BOOL_OR(a) AS admin_c, BOOL_OR(k) AS keyset_tech_c, BOOL_OR(n) AS nsset_tech_c "
                    "FROM cd "
                    "GROUP BY 1) "
                "SELECT obr.uuid, obr.historyid, obr.name, h.uuid, d.holder, d.admin_c, d.keyset_tech_c, d.nsset_tech_c, obr.erdate IS NOT NULL AS is_deleted, COUNT(*) OVER() AS full_count "
                  "FROM d "
                  "JOIN object_registry obr ON obr.id = d.id ";
        if (request.include_deleted_domains)
        {
            sql += "JOIN domain_history domain ON domain.id = obr.id AND domain.historyid = obr.historyid "
                   "JOIN object_history o ON o.id = obr.id AND o.historyid = obr.historyid ";
        }
        else
        {
            sql += "JOIN domain ON domain.id = obr.id "
                   "JOIN object o ON o.id = obr.id ";
        }
        sql += "JOIN registrar r ON r.id = o.clid "
               "JOIN history h ON h.id = obr.historyid";
        if (!page_break.with_part.empty())
        {
            sql += ", page_break pb";
        }
        if (!page_break.with_part.empty() || !filter_condition.empty())
        {
            auto condition = page_break.where_part;
            if (!filter_condition.empty())
            {
                if (!condition.empty())
                {
                    condition += " AND ";
                }
                condition += filter_condition;
            }
            sql += " WHERE " + condition;
        }
        sql += " " + sql_order_by_part + " LIMIT " + std::to_string(limit);
        LibFred::OperationContextCreator ctx;
        const auto dbres = ctx.get_conn().exec_params(sql, params);
        const bool contact_exists = (0 < dbres.size()) || does_contact_exist(ctx, request.contact_id);
        ctx.commit_transaction();
        if (dbres.size() == 0)
        {
            if (!contact_exists)
            {
                throw GetDomainsByContactReply::Exception::ContactDoesNotExist{request};
            }
            set_last_page(request, reply);
            return reply;
        }
        if (request.pagination != boost::none)
        {
            reply.pagination = PaginationReply{};
        }
        if (page_direction == Move::forward)
        {
            if (reply.pagination != boost::none)
            {
                const auto domains_left = static_cast<std::int64_t>(dbres[0][9]);
                if (limit < domains_left)
                {
                    reply.pagination->items_left = domains_left - limit;
                }
                else
                {
                    reply.pagination->items_left = 0;
                }
            }
            for (std::size_t idx = 0; idx < dbres.size(); ++idx)
            {
                GetDomainsByContactReply::Data::Domain domain;
                domain.domain.id = Fred::Registry::Util::make_strong<DomainId>(
                        boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
                domain.domain.fqdn = Fred::Registry::Util::make_strong<Fqdn>(
                        static_cast<std::string>(dbres[idx][2]));
                if (reply.pagination != boost::none)
                {
                    const auto is_last_row = (idx + 1) == dbres.size();
                    if (is_last_row)
                    {
                        if (0 < *(reply.pagination->items_left))
                        {
                            reply.pagination->next_page_token = to_string(get_raw_value_from(domain.domain.id)).append(
                                    std::to_string(static_cast<unsigned long long>(dbres[idx][1])));
                        }
                        else
                        {
                            reply.pagination->next_page_token.clear();
                        }
                    }
                }
                domain.domain_history_id = Fred::Registry::Util::make_strong<DomainHistoryId>(
                        boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][3])));
                domain.is_holder = static_cast<bool>(dbres[idx][4]);
                domain.is_admin_contact = static_cast<bool>(dbres[idx][5]);
                domain.is_keyset_tech_contact = static_cast<bool>(dbres[idx][6]);
                domain.is_nsset_tech_contact = static_cast<bool>(dbres[idx][7]);
                domain.is_deleted = static_cast<bool>(dbres[idx][8]);
                reply.domains.emplace_back(std::move(domain));
            }
            FREDLOG_DEBUG("get_domains_by_contact(forward): success");
            return reply;
        }
        if (page_direction == Move::backward)
        {
            if (reply.pagination != boost::none)
            {
                const auto domains_left = static_cast<std::int64_t>(dbres[0][9]);
                if ((limit - 1) < domains_left)
                {
                    reply.pagination->items_left = domains_left - limit + 1;
                }
            }
            const auto data_rows = limit <= dbres.size() ? limit - 1
                                                         : dbres.size();
            for (std::size_t cnt = 0; cnt < data_rows; ++cnt)
            {
                const auto idx = data_rows - 1 - cnt;
                GetDomainsByContactReply::Data::Domain domain;
                domain.domain.id = Fred::Registry::Util::make_strong<DomainId>(
                        boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
                domain.domain.fqdn = Fred::Registry::Util::make_strong<Fqdn>(
                        static_cast<std::string>(dbres[idx][2]));
                domain.domain_history_id = Fred::Registry::Util::make_strong<DomainHistoryId>(
                        boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][3])));
                domain.is_holder = static_cast<bool>(dbres[idx][4]);
                domain.is_admin_contact = static_cast<bool>(dbres[idx][5]);
                domain.is_keyset_tech_contact = static_cast<bool>(dbres[idx][6]);
                domain.is_nsset_tech_contact = static_cast<bool>(dbres[idx][7]);
                domain.is_deleted = static_cast<bool>(dbres[idx][8]);
                reply.domains.emplace_back(std::move(domain));
            }
            if ((reply.pagination != boost::none) && (limit <= dbres.size()))
            {
                const auto idx = dbres.size() - 1;
                const auto domain_id = boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0]));
                reply.pagination->next_page_token = to_string(domain_id).append(
                        std::to_string(static_cast<unsigned long long>(dbres[idx][1])));
            }
            FREDLOG_DEBUG("get_domains_by_contact(backward): success");
            return reply;
        }
        throw std::runtime_error{"unknown page direction value"};
    }
    catch (const GetDomainsByContactReply::Exception::ContactDoesNotExist& e)
    {
        throw;
    }
    catch (const InvalidOrderBy& e)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }

}
