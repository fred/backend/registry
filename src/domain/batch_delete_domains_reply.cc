/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/batch_delete_domains_reply.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Domain {

namespace {

std::string reason_to_string(BatchDeleteDomainsReply::NotDeleted::Reason reason)
{
    switch (reason)
    {
        case BatchDeleteDomainsReply::NotDeleted::Reason::domain_blocked:
            return "domain_blocked";
        case BatchDeleteDomainsReply::NotDeleted::Reason::domain_does_not_exist:
            return "domain_does_not_exist";
        case BatchDeleteDomainsReply::NotDeleted::Reason::domain_history_id_mismatch:
            return "domain_history_id_mismatch";
        case BatchDeleteDomainsReply::NotDeleted::Reason::insufficient_permissions:
            return "insufficient_permissions";
        case BatchDeleteDomainsReply::NotDeleted::Reason::undefined:
            return "undefined";
    }
    return "unexpected value " + std::to_string(static_cast<int>(reason));
}

} // namespace Fred::Registry::Domain::{anonymous}

std::string BatchDeleteDomainsReply::NotDeleted::to_string() const
{
    return Util::StructToString().add("domain_id", domain_id)
                                 .add("reason", reason_to_string(reason))
                                 .finish();
}

std::string BatchDeleteDomainsReply::Data::to_string() const
{
    return Util::StructToString().add("not_deleted_domains", not_deleted_domains)
                                 .finish();
}

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred
