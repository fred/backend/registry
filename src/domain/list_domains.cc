/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain/list_domains.hh"

#include "src/domain/domain_common_types.hh"
#include "src/util/strong_type.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

using namespace Fred::Registry::Domain;

ListDomainsReply::Data Fred::Registry::Domain::list_domains(const ListDomainsRequest& _request)
{
    try
    {
        ListDomainsReply::Data reply;
        const auto dbres = [&_request]()
        {
            LibFred::OperationContextCreator ctx;
            auto dbres = ctx.get_conn().exec_params(
                // clang-format off
                "SELECT oreg.uuid, "
                       "oreg.name "
                  "FROM zone z "
                  "LEFT JOIN (domain d "
                              "JOIN object_registry oreg "
                                "ON oreg.id = d.id) ON d.zone = z.id "
                 "WHERE z.fqdn = LOWER($1::TEXT) "
                 "ORDER BY oreg.name",
                 Database::QueryParams{_request.zone});
                // clang-format on
            ctx.commit_transaction();
            return dbres;
        }();

        if (dbres.size() == 0)
        {
            throw ListDomainsReply::Exception::ZoneDoesNotExist{_request};
        }
        if (dbres.size() == 1 && dbres[0][0].isnull())
        {
            return reply;
        }
        reply.domains.reserve(dbres.size());
        for (std::size_t idx = 0; idx < dbres.size(); ++idx)
        {
            Domain::DomainLightInfo domain_light_info;
            domain_light_info.id = Util::make_strong<DomainId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
            domain_light_info.fqdn = Util::make_strong<Fqdn>(static_cast<std::string>(dbres[idx][1]));
            reply.domains.push_back(std::move(domain_light_info));
        }
        return reply;
    }
    catch (const ListDomainsReply::Exception::ZoneDoesNotExist&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
