/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef DOMAIN_INFO_HH_5F7A67159D4843CBB62C1F8C8A0FCB78
#define DOMAIN_INFO_HH_5F7A67159D4843CBB62C1F8C8A0FCB78

#include "src/domain/domain_info_request.hh"
#include "src/domain/domain_info_reply.hh"

namespace Fred {
namespace Registry {
namespace Domain {

DomainInfoReply::Data domain_info(const DomainInfoRequest& request);
BatchDomainInfoReply::Data batch_domain_info(const BatchDomainInfoRequest& request);

DomainIdReply::Data get_domain_id(const DomainIdRequest& request);

} // namespace Fred::Registry::Domain
} // namespace Fred::Registry
} // namespace Fred

#endif
