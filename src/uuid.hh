/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef UUID_HH_033152F0206E46259300EE707661C053
#define UUID_HH_033152F0206E46259300EE707661C053

#include "src/util/strong_type.hh"

#include <boost/uuid/uuid.hpp>

namespace Fred {
namespace Registry {

template <typename N>
using Uuid = Util::StrongType<boost::uuids::uuid, N, Util::Skill::Printable>;

} // namespace Fred::Registry
} // namespace Fred

#endif
