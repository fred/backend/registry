/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cfg.hh"

#include "src/contact/contact_common_types.hh"
#include "src/contact/contact_data_history.hh"
#include "src/contact/contact_handle_history.hh"
#include "src/contact/contact_info.hh"
#include "src/contact/contact_state.hh"
#include "src/contact/contact_state_flags.hh"
#include "src/contact/contact_state_history.hh"
#include "src/contact/create_contact.hh"
#include "src/contact/list_contacts.hh"
#include "src/contact/list_merge_candidates.hh"
#include "src/contact/merge_contacts.hh"
#include "src/contact/merge_contacts_reply.hh"
#include "src/contact/update_contact.hh"
#include "src/contact/update_contact_state.hh"

#include "src/contact_representative/contact_representative_common_types.hh"
#include "src/contact_representative/contact_representative_info.hh"
#include "src/contact_representative/create_contact_representative.hh"
#include "src/contact_representative/delete_contact_representative.hh"
#include "src/contact_representative/update_contact_representative.hh"

#include "src/diagnostics/about.hh"
#include "src/diagnostics/status.hh"

#include "src/domain/batch_delete_domains.hh"
#include "src/domain/domain_common_types.hh"
#include "src/domain/domain_data_history.hh"
#include "src/domain/domain_info.hh"
#include "src/domain/domain_info_reply.hh"
#include "src/domain/domain_info_request.hh"
#include "src/domain/domain_state.hh"
#include "src/domain/domain_state_flags.hh"
#include "src/domain/domain_state_history.hh"
#include "src/domain/fqdn_history.hh"
#include "src/domain/get_domain_life_cycle_stage.hh"
#include "src/domain/get_domains_by_contact.hh"
#include "src/domain/get_domains_notify_info.hh"
#include "src/domain/list_domains.hh"
#include "src/domain/list_domains_by_contact.hh"
#include "src/domain/list_domains_by_keyset.hh"
#include "src/domain/list_domains_by_nsset.hh"
#include "src/domain/manage_domain_state_flags.hh"
#include "src/domain/update_domains_additional_notify_info.hh"
#include "src/domain/update_domains_additional_notify_info_reply.hh"

#include "src/domain_blacklist/block_info.hh"
#include "src/domain_blacklist/create_block.hh"
#include "src/domain_blacklist/delete_block.hh"
#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/domain_blacklist/list_blocks.hh"

#include "src/keyset/keyset_common_types.hh"
#include "src/keyset/keyset_data_history.hh"
#include "src/keyset/keyset_handle_history.hh"
#include "src/keyset/keyset_info.hh"
#include "src/keyset/keyset_info_reply.hh"
#include "src/keyset/keyset_info_request.hh"
#include "src/keyset/keyset_state.hh"
#include "src/keyset/keyset_state_flags.hh"
#include "src/keyset/keyset_state_history.hh"
#include "src/keyset/list_keysets_by_contact.hh"

#include "src/nsset/check_dns_host.hh"
#include "src/nsset/list_nssets.hh"
#include "src/nsset/list_nssets_by_contact.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/nsset/nsset_data_history.hh"
#include "src/nsset/nsset_handle_history.hh"
#include "src/nsset/nsset_info.hh"
#include "src/nsset/nsset_info_reply.hh"
#include "src/nsset/nsset_info_request.hh"
#include "src/nsset/nsset_state.hh"
#include "src/nsset/nsset_state_flags.hh"
#include "src/nsset/nsset_state_history.hh"

#include "src/contact/search_contact.hh"
#include "src/domain/search_domain.hh"
#include "src/keyset/search_keyset.hh"
#include "src/nsset/search_nsset.hh"

#include "src/registrar/add_registrar_certification.hh"
#include "src/registrar/add_registrar_epp_credentials.hh"
#include "src/registrar/add_registrar_group_membership.hh"
#include "src/registrar/add_registrar_zone_access.hh"
#include "src/registrar/create_registrar.hh"
#include "src/registrar/delete_registrar_certification.hh"
#include "src/registrar/delete_registrar_epp_credentials.hh"
#include "src/registrar/delete_registrar_group_membership.hh"
#include "src/registrar/registrar_certifications.hh"
#include "src/registrar/registrar_groups.hh"
#include "src/registrar/registrar_groups_membership.hh"
#include "src/registrar/registrar_credit.hh"
#include "src/registrar/registrar_epp_credentials.hh"
#include "src/registrar/registrar_info.hh"
#include "src/registrar/registrar_list.hh"
#include "src/registrar/update_registrar.hh"
#include "src/registrar/update_registrar_certification.hh"
#include "src/registrar/update_registrar_epp_credentials.hh"

#include "src/proto_unwrap.hh"
#include "src/proto_wrap.hh"

#include "util/log/log.hh"
#include "util/random/random.hh"

#include "libfred/db_settings.hh"

#include "libfred/registrable_object/contact/contact_state.hh"
#include "libfred/registrable_object/domain/domain_state.hh"
#include "libfred/registrable_object/keyset/keyset_state.hh"
#include "libfred/registrable_object/nsset/nsset_state.hh"

#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include "fred_api/registry/contact/service_contact_grpc.pb.h"
#include "fred_api/registry/contact/service_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact_representative/service_contact_representative_grpc.pb.h"
#include "fred_api/registry/contact_representative/service_contact_representative_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain_blacklist/service_domain_blacklist_grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/registrar/service_admin_grpc.pb.h"
#include "fred_api/registry/registrar/service_admin_grpc.grpc.pb.h"
#include "fred_api/registry/registrar/service_registrar_grpc.pb.h"
#include "fred_api/registry/registrar/service_registrar_grpc.pb.h"

#include <grpc++/grpc++.h>

#include <boost/algorithm/string.hpp>
#include <boost/date_time.hpp>
#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include <pthread.h>

#include <csignal>
#include <cstring>
#include <cstdlib>

#include <chrono>
#include <iostream>
#include <memory>
#include <string>

namespace Fred {
namespace Registry {

namespace {

std::string create_ctx_function_name(const char* fnc)
{
    std::string name(fnc);
    std::replace(name.begin(), name.end(), '_', '-');
    return name;
}

grpc::Status on_database_error(const DatabaseError& e)
{
    FREDLOG_ERROR(std::string{"InternalServerError.DatabaseError caught: "} + e.what());
    return grpc::Status(grpc::StatusCode::INTERNAL, "InternalServerError.DatabaseError");
}

grpc::Status on_internal_server_error(const std::exception& e)
{
    FREDLOG_ERROR(std::string{"InternalServerError caught: "} + e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "InternalServerError");
}

grpc::Status on_invalid_argument(const std::exception& e)
{
    FREDLOG_WARNING(std::string{"InvalidArgument caught: "} + e.what());
    return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT, "InvalidArgument");
}

grpc::Status on_std_exception(const std::exception& e)
{
    FREDLOG_ERROR(std::string{"std::exception caught: "} + e.what());
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception based on std::exception");
}

grpc::Status on_database_exception(const Database::ResultFailed& e)
{
    if (std::strstr(e.what(), "canceling statement due to statement timeout") != nullptr)
    {
        FREDLOG_INFO("SQL timed out");
        return grpc::Status{grpc::StatusCode::DEADLINE_EXCEEDED, "SQL timed out"};
    }
    return on_std_exception(e);
}

grpc::Status on_unknown_exception()
{
    FREDLOG_ERROR("unknown exception caught");
    return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception");
}

}//namespace Fred::Registry::{anonymous}

namespace Contact {

namespace {

class ContactService final : public Api::Contact::Contact::Service
{
private:
    grpc::Status create_contact(
            grpc::ServerContext*,
            const Api::Contact::CreateContactRequest* _api_request,
            Api::Contact::CreateContactReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: create_contact requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: create_contact(" + request.to_string() + ")");
            const auto result = Fred::Registry::Contact::create_contact(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const CreateContactReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateContactReply::Exception::ContactAlreadyExists& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateContactReply::Exception::ContactHandleInProtectionPeriod& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateContactReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_handle_history(
            grpc::ServerContext*,
            const Api::Contact::ContactHandleHistoryRequest* _api_request,
            Api::Contact::ContactHandleHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_handle_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: contact_handle_history(" + request.to_string() + ")");
            const auto handle_history = contact_handle_history(request);
            proto_wrap(handle_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + handle_history.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_history(
            grpc::ServerContext*,
            const Api::Contact::ContactHistoryRequest* _api_request,
            Api::Contact::ContactHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: contact_history(" + request.to_string() + ")");
            const auto data_history = contact_data_history(request);
            proto_wrap(data_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + data_history.to_string());
            return grpc::Status::OK;
        }
        catch (const ContactDataHistoryReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const ContactDataHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_info(
            grpc::ServerContext*,
            const Api::Contact::ContactInfoRequest* _api_request,
            Api::Contact::ContactInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: contact_info(" + request.to_string() + ")");
            const auto info = contact_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const ContactInfoReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_id(
            grpc::ServerContext*,
            const Api::Contact::ContactIdRequest* _api_request,
            Api::Contact::ContactIdReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_id requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: get_contact_id(" + request.to_string() + ")");
            const auto id = Fred::Registry::Contact::get_contact_id(request);
            proto_wrap(id, _api_response);
            FREDLOG_DEBUG("server: answer = " + id.to_string());
            return grpc::Status::OK;
        }
        catch (const ContactIdReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_state(
            grpc::ServerContext*,
            const Api::Contact::ContactStateRequest* _api_request,
            Api::Contact::ContactStateReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_state requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: contact_state(" + request.to_string() + ")");
            const auto state = contact_state(request);
            proto_wrap(state, _api_response);
            FREDLOG_DEBUG("server: answer = " + state.to_string());
            return grpc::Status::OK;
        }
        catch (const ContactStateReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_state_history(
            grpc::ServerContext*,
            const Api::Contact::ContactStateHistoryRequest* _api_request,
            Api::Contact::ContactStateHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_state_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: contact_state_history(" + request.to_string() + ")");
            const auto state_history = contact_state_history(request);
            proto_wrap(state_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + state_history.to_string());
            return grpc::Status::OK;
        }
        catch (const ContactStateHistoryReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const ContactStateHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_contact_state_flags(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Api::Contact::ContactStateFlagsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_state_flags requested");
            const auto info = get_contact_state_flags_info();
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + to_string(info));
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_contact(
            grpc::ServerContext*,
            const Api::Contact::UpdateContactRequest* _api_request,
            Api::Contact::UpdateContactReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_contact requested");
            return grpc::Status{grpc::StatusCode::UNIMPLEMENTED, "not yet implemented"};
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_contact(" + request.to_string() + ")");
            const auto result = Contact::update_contact(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const UpdateContactReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateContactReply::Exception::NotCurrentVersion& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateContactReply::Exception::InvalidArgument& e)
        {
            return on_invalid_argument(static_cast<const std::exception&>(e));
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_contact_state(
            grpc::ServerContext*,
            const Api::Contact::UpdateContactStateRequest* _api_request,
            Api::Contact::UpdateContactStateReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_contact_state requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_contact_state(" + request.to_string() + ")");
            const auto result = Contact::update_contact_state(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_contacts(
            grpc::ServerContext* context,
            const google::protobuf::Empty*,
            grpc::ServerWriter<Api::Contact::ListContactsReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_contacts requested");
            FREDLOG_DEBUG("server: list_contacts()");
            const auto reply = Contact::list_contacts();
            std::size_t offset = 0;
            while (true)
            {
                Api::Contact::ListContactsReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_merge_candidates(
            grpc::ServerContext* context,
            const Api::Contact::ListMergeCandidatesRequest* api_request,
            grpc::ServerWriter<Api::Contact::ListMergeCandidatesReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_merge_candidates requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_merge_candidates(" + request.to_string() + ")");
            const auto reply = Contact::list_merge_candidates(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Contact::ListMergeCandidatesReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Contact::ListMergeCandidatesReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Contact::ListMergeCandidatesReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Contact::ListMergeCandidatesReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Contact::ListMergeCandidatesReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status merge_contacts(
            grpc::ServerContext*,
            const Api::Contact::MergeContactsRequest* api_request,
            Api::Contact::MergeContactsReply* api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: merge_contacts requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: merge_contacts(" + request.to_string() + ")");
            Contact::merge_contacts(request);
            FREDLOG_DEBUG("server: answer = (method does not return data)");
            return grpc::Status::OK;
        }
        catch (const Contact::MergeContactsReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, api_response);
            return grpc::Status::OK;
        }
        catch (const Contact::MergeContactsReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

class SearchContactService final : public Api::Contact::SearchContact::SearchContact::Service
{
private:
    grpc::Status search_contact(
            grpc::ServerContext*,
            const Api::Contact::SearchContactRequest* _api_request,
            Api::Contact::SearchContactReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_contact requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_contact(" + request.to_string() + ")");
            const auto response = Fred::Registry::Contact::search_contact(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status search_contact_history(
        grpc::ServerContext*,
        const Api::Contact::SearchContactHistoryRequest* _api_request,
        Api::Contact::SearchContactHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_contact_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_contact_history(" + request.to_string() + ")");
            const auto response = Fred::Registry::Contact::search_contact(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const SearchContactHistoryReply::Exception::ChronologyViolation& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

}//namespace Fred::Registry::Contact::{anonymous}

}//namespace Fred::Registry::Contact

namespace Domain {

namespace {

class AdminService final : public Api::Domain::Admin::Service
{
private:
    grpc::Status batch_delete_domains(
            grpc::ServerContext* context,
            grpc::ServerReaderWriter<Api::Domain::BatchDeleteDomainsReply, ::Fred::Registry::Api::Domain::BatchDeleteDomainsRequest>* api_stream) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: batch_delete_domains requested");
            ::Fred::Registry::Api::Domain::BatchDeleteDomainsRequest request_chunk;
            Domain::BatchDeleteDomainsRequest request{};
            while (api_stream->Read(&request_chunk))
            {
                ::Fred::Registry::proto_unwrap_back(request_chunk, request);
            }
            if (context->IsCancelled())
            {
                FREDLOG_WARNING("server: api stream cancelled");
                return grpc::Status::CANCELLED;
            }
            const auto reply = ::Fred::Registry::Domain::batch_delete_domains(
                    request,
                    Cfg::Options::get().fred.registrar_originator);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::BatchDeleteDomainsReply response{};
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_stream->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const auto stream_is_ok = api_stream->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status manage_domain_state_flags(
            grpc::ServerContext* context,
            grpc::ServerReaderWriter<Api::Domain::ManageDomainStateFlagsReply, Api::Domain::ManageDomainStateFlagsRequest>* api_stream) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: manage_domain_state_flags requested");
            ::Fred::Registry::Api::Domain::ManageDomainStateFlagsRequest request_chunk;
            Domain::ManageDomainStateFlagsRequest request{};
            while (api_stream->Read(&request_chunk))
            {
                ::Fred::Registry::proto_unwrap_back(request_chunk, request);
            }
            if (context->IsCancelled())
            {
                FREDLOG_WARNING("server: api stream cancelled");
                return grpc::Status::CANCELLED;
            }
            const auto reply = ::Fred::Registry::Domain::manage_domain_state_flags(
                    request,
                    Cfg::Options::get().fred.registrar_originator);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::ManageDomainStateFlagsReply response{};
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_stream->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_stream->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Domain::ManageDomainStateFlagsReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ManageDomainStateFlagsReply api_response;
            proto_wrap(e, &api_response);
            api_stream->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Domain::ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ManageDomainStateFlagsReply api_response;
            proto_wrap(e, &api_response);
            api_stream->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Domain::ManageDomainStateFlagsReply::Exception::DomainStateFlagDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ManageDomainStateFlagsReply api_response;
            proto_wrap(e, &api_response);
            api_stream->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Domain::ManageDomainStateFlagsReply::Exception::DomainHistoryIdMismatch& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ManageDomainStateFlagsReply api_response;
            proto_wrap(e, &api_response);
            api_stream->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domains_outzone_notify_info(
            grpc::ServerContext* context,
            const Api::Domain::GetDomainsOutzoneNotifyInfoRequest* api_request,
            grpc::ServerWriter<Api::Domain::GetDomainsNotifyInfoReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domains_outzone_notify_info requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: get_domains_outzone_notify_info(" + request.to_string() + ")");
            const auto reply = Domain::get_domains_notify_info(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::GetDomainsNotifyInfoReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_domains_outzone_additional_notify_info(
            ::grpc::ServerContext*,
            ::grpc::ServerReader<::Fred::Registry::Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest>* api_stream_reader,
            ::Fred::Registry::Api::Domain::UpdateDomainsAdditionalNotifyInfoReply* api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_domains_outzone_additional_notify_info requested");
            ::Fred::Registry::Api::Domain::UpdateDomainsOutzoneAdditionalNotifyInfoRequest request_chunk;
            Domain::UpdateDomainsAdditionalNotifyInfoRequest request{};
            while (api_stream_reader->Read(&request_chunk))
            {
                ::Fred::Registry::proto_unwrap_back(request_chunk, request);
            }
            ::Fred::Registry::Domain::update_domains_additional_notify_info(request);
            api_response->clear_exception();
            return grpc::Status::OK;
        }
        catch (const Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, api_response);
            return grpc::Status::OK;
        }
        catch (const Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::InvalidDomainContactInfo& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domains_delete_notify_info(
            grpc::ServerContext* context,
            const Api::Domain::GetDomainsDeleteNotifyInfoRequest* api_request,
            grpc::ServerWriter<Api::Domain::GetDomainsNotifyInfoReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domains_delete_notify_info requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: get_domains_delete_notify_info(" + request.to_string() + ")");
            const auto reply = Domain::get_domains_notify_info(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::GetDomainsNotifyInfoReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_domains_delete_additional_notify_info(
            ::grpc::ServerContext*,
            ::grpc::ServerReader<::Fred::Registry::Api::Domain::UpdateDomainsDeleteAdditionalNotifyInfoRequest>* api_stream_reader,
            ::Fred::Registry::Api::Domain::UpdateDomainsAdditionalNotifyInfoReply* api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_domains_delete_additional_notify_info requested");
            ::Fred::Registry::Api::Domain::UpdateDomainsDeleteAdditionalNotifyInfoRequest request_chunk;
            Domain::UpdateDomainsAdditionalNotifyInfoRequest request{};
            while (api_stream_reader->Read(&request_chunk))
            {
                ::Fred::Registry::proto_unwrap_back(request_chunk, request);
            }
            ::Fred::Registry::Domain::update_domains_additional_notify_info(request);
            api_response->clear_exception();
            return grpc::Status::OK;
        }
        catch (const Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, api_response);
            return grpc::Status::OK;
        }
        catch (const Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::InvalidDomainContactInfo& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            proto_wrap(e, api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

class DomainService final : public Api::Domain::Domain::Service
{
private:
    grpc::Status get_fqdn_history(
            grpc::ServerContext*,
            const Api::Domain::FqdnHistoryRequest* _api_request,
            Api::Domain::FqdnHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_fqdn_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: fqdn_history(" + request.to_string() + ")");
            const auto handle_history = fqdn_history(request);
            proto_wrap(handle_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + handle_history.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_history(
            grpc::ServerContext*,
            const Api::Domain::DomainHistoryRequest* _api_request,
            Api::Domain::DomainHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: domain_history(" + request.to_string() + ")");
            const auto data_history = domain_data_history(request);
            proto_wrap(data_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + data_history.to_string());
            return grpc::Status::OK;
        }
        catch (const DomainDataHistoryReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DomainDataHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_info(
            grpc::ServerContext*,
            const Api::Domain::DomainInfoRequest* _api_request,
            Api::Domain::DomainInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: domain_info(" + request.to_string() + ")");
            const auto info = domain_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const DomainInfoReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status batch_domain_info(
            grpc::ServerContext* context,
            grpc::ServerReaderWriter<Api::Domain::BatchDomainInfoReply,
                                     Api::Domain::BatchDomainInfoRequest>* api_stream) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: batch_domain_info requested");
            Api::Domain::BatchDomainInfoRequest request_chunk;
            Domain::BatchDomainInfoRequest request{};
            while (api_stream->Read(&request_chunk))
            {
                proto_unwrap_back(request_chunk, request);
            }
            if (context->IsCancelled())
            {
                FREDLOG_WARNING("server: api stream cancelled");
                return grpc::Status::CANCELLED;
            }
            const auto reply = Domain::batch_domain_info(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::BatchDomainInfoReply response{};
                offset = proto_wrap(reply, offset, &response);
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_stream->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const auto stream_is_ok = api_stream->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_id(
            grpc::ServerContext*,
            const Api::Domain::DomainIdRequest* _api_request,
            Api::Domain::DomainIdReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_id requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: domain_id(" + request.to_string() + ")");
            const auto id = Fred::Registry::Domain::get_domain_id(request);
            proto_wrap(id, _api_response);
            FREDLOG_DEBUG("server: answer = " + id.to_string());
            return grpc::Status::OK;
        }
        catch (const DomainIdReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_state(
            grpc::ServerContext*,
            const Api::Domain::DomainStateRequest* _api_request,
            Api::Domain::DomainStateReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_state requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: domain_state(" + request.to_string() + ")");
            const auto state = domain_state(request);
            proto_wrap(state, _api_response);
            FREDLOG_DEBUG("server: answer = " + state.to_string());
            return grpc::Status::OK;
        }
        catch (const DomainStateReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_state_history(
            grpc::ServerContext*,
            const Api::Domain::DomainStateHistoryRequest* _api_request,
            Api::Domain::DomainStateHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_state_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: domain_state_history(" + request.to_string() + ")");
            const auto state_history = domain_state_history(request);
            proto_wrap(state_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + state_history.to_string());
            return grpc::Status::OK;
        }
        catch (const DomainStateHistoryReply::Exception::DomainDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DomainStateHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_state_flags(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Api::Domain::DomainStateFlagsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_state_flags requested");
            const auto info = get_domain_state_flags_info();
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + to_string(info));
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domains_by_contact(
            grpc::ServerContext* context,
            const Api::Domain::DomainsByContactRequest* api_request,
            grpc::ServerWriter<Api::Domain::DomainsByContactReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domains_by_contact requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: get_domains_by_contact(" + request.to_string() + ")");
            const auto reply = Domain::get_domains_by_contact(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::DomainsByContactReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Fred::Registry::Domain::GetDomainsByContactReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Fred::Registry::Api::Domain::DomainsByContactReply reply;
            proto_wrap(e, &reply);
            api_response_writer->WriteLast(reply, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Fred::Registry::Domain::GetDomainsByContactReply::Exception::InvalidOrderBy& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Fred::Registry::Api::Domain::DomainsByContactReply reply;
            proto_wrap(e, &reply);
            api_response_writer->WriteLast(reply, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_domain_life_cycle_stage(
            grpc::ServerContext*,
            const Api::Domain::DomainLifeCycleStageRequest* api_request,
            Api::Domain::DomainLifeCycleStageReply* api_response)
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_domain_life_cycle_stage requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: get_domain_life_cycle_stage(" + request.to_string() + ")");
            const auto response = Fred::Registry::Domain::get_domain_life_cycle_stage(request);
            proto_wrap(response, api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_domains(
            grpc::ServerContext* context,
            const Api::Domain::ListDomainsRequest* api_request,
            grpc::ServerWriter<Api::Domain::ListDomainsReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_domains requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_domains(" + request.to_string() + ")");
            const auto reply = Domain::list_domains(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::ListDomainsReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsReply::Exception::ZoneDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_domains_by_nsset(
            grpc::ServerContext* context,
            const Api::Domain::ListDomainsByNssetRequest* api_request,
            grpc::ServerWriter<Api::Domain::ListDomainsByNssetReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_domains_by_nsset requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_domains_by_nsset(" + request.to_string() + ")");
            const auto reply = Domain::list_domains_by_nsset(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::ListDomainsByNssetReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsByNssetReply::Exception::NssetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsByNssetReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsByNssetReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsByNssetReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_domains_by_keyset(
            grpc::ServerContext* context,
            const Api::Domain::ListDomainsByKeysetRequest* api_request,
            grpc::ServerWriter<Api::Domain::ListDomainsByKeysetReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_domains_by_keyset requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_domains_by_keyset(" + request.to_string() + ")");
            const auto reply = Domain::list_domains_by_keyset(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::ListDomainsByKeysetReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsByKeysetReply::Exception::KeysetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsByKeysetReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsByKeysetReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsByKeysetReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_domains_by_contact(
            grpc::ServerContext* context,
            const Api::Domain::ListDomainsByContactRequest* api_request,
            grpc::ServerWriter<Api::Domain::ListDomainsByContactReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_domains_by_contact requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_domains_by_contact(" + request.to_string() + ")");
            const auto reply = Domain::list_domains_by_contact(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Domain::ListDomainsByContactReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsByContactReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsByContactReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Domain::ListDomainsByContactReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string{"server: "} + e.what());
            Api::Domain::ListDomainsByContactReply api_response;
            proto_wrap(e, &api_response);
            api_response_writer->WriteLast(api_response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

class SearchDomainService final : public Api::Domain::SearchDomain::SearchDomain::Service
{
private:
    grpc::Status search_domain(
            grpc::ServerContext*,
            const Api::Domain::SearchDomainRequest* _api_request,
            Api::Domain::SearchDomainReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_domain requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_domain(" + request.to_string() + ")");
            const auto response = Fred::Registry::Domain::search_domain(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status search_domain_history(
            grpc::ServerContext*,
            const Api::Domain::SearchDomainHistoryRequest* _api_request,
            Api::Domain::SearchDomainHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_domain_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_domain_history(" + request.to_string() + ")");
            const auto response = Fred::Registry::Domain::search_domain(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const SearchDomainHistoryReply::Exception::ChronologyViolation& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

}//namespace Fred::Registry::Domain::{anonymous}

}//namespace Fred::Registry::Domain

namespace DomainBlacklist {

namespace {

class DomainBlacklistService final : public Api::DomainBlacklist::Block::Service
{
private:
    grpc::Status get_block_info(
            grpc::ServerContext*,
            const Api::DomainBlacklist::BlockInfoRequest* _api_request,
            Api::DomainBlacklist::BlockInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_block_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: block_info(" + request.to_string() + ")");
            const auto info = DomainBlacklist::block_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const BlockInfoReply::Exception::BlockDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const BlockInfoReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status create_block(
            grpc::ServerContext*,
            const Api::DomainBlacklist::CreateBlockRequest* _api_request,
            Api::DomainBlacklist::CreateBlockReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: create_block requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: create_block(" + request.to_string() + ")");
            const auto result = DomainBlacklist::create_block(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const CreateBlockReply::Exception::BlockIdAlreadyExists& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateBlockReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status delete_block(
            grpc::ServerContext*,
            const Api::DomainBlacklist::DeleteBlockRequest* _api_request,
            Api::DomainBlacklist::DeleteBlockReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: delete_block requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: delete_block(" + request.to_string() + ")");
            const auto result = DomainBlacklist::delete_block(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = void");
            return grpc::Status::OK;
        }
        catch (const DeleteBlockReply::Exception::BlockDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DeleteBlockReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_blocks(
            grpc::ServerContext* context,
            const google::protobuf::Empty*,
            grpc::ServerWriter<Api::DomainBlacklist::ListBlocksReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_blocks requested");
            const auto reply = DomainBlacklist::list_blocks();
            std::size_t offset = 0;
            while (true)
            {
                Api::DomainBlacklist::ListBlocksReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

} // namespace Fred::Registry::DomainBlacklist::{anonymous}

} // namespace Fred::Registry::DomainBlacklist

namespace Keyset {

namespace {

class KeysetService final : public Api::Keyset::Keyset::Service
{
private:
    grpc::Status get_keyset_handle_history(
            grpc::ServerContext*,
            const Api::Keyset::KeysetHandleHistoryRequest* _api_request,
            Api::Keyset::KeysetHandleHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_handle_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: keyset_handle_history(" + request.to_string() + ")");
            const auto handle_history = keyset_handle_history(request);
            proto_wrap(handle_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + handle_history.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_keyset_history(
            grpc::ServerContext*,
            const Api::Keyset::KeysetHistoryRequest* _api_request,
            Api::Keyset::KeysetHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: keyset_history(" + request.to_string() + ")");
            const auto data_history = keyset_data_history(request);
            proto_wrap(data_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + data_history.to_string());
            return grpc::Status::OK;
        }
        catch (const KeysetDataHistoryReply::Exception::KeysetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const KeysetDataHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_keyset_info(
            grpc::ServerContext*,
            const Api::Keyset::KeysetInfoRequest* _api_request,
            Api::Keyset::KeysetInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: keyset_info(" + request.to_string() + ")");
            const auto info = keyset_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const KeysetInfoReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const KeysetInfoReply::Exception::KeysetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status batch_keyset_info(
            grpc::ServerContext* context,
            grpc::ServerReaderWriter<Api::Keyset::BatchKeysetInfoReply,
                                     Api::Keyset::BatchKeysetInfoRequest>* api_stream) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: batch_keyset_info requested");
            Api::Keyset::BatchKeysetInfoRequest request_chunk;
            Keyset::BatchKeysetInfoRequest request{};
            while (api_stream->Read(&request_chunk))
            {
                proto_unwrap_back(request_chunk, request);
            }
            if (context->IsCancelled())
            {
                FREDLOG_WARNING("server: api stream cancelled");
                return grpc::Status::CANCELLED;
            }
            const auto reply = Keyset::batch_keyset_info(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Keyset::BatchKeysetInfoReply response{};
                offset = proto_wrap(reply, offset, &response);
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_stream->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const auto stream_is_ok = api_stream->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_keyset_id(
            grpc::ServerContext*,
            const Api::Keyset::KeysetIdRequest* _api_request,
            Api::Keyset::KeysetIdReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_id requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: keyset_id(" + request.to_string() + ")");
            const auto id = Fred::Registry::Keyset::get_keyset_id(request);
            proto_wrap(id, _api_response);
            FREDLOG_DEBUG("server: answer = " + id.to_string());
            return grpc::Status::OK;
        }
        catch (const KeysetIdReply::Exception::KeysetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_keyset_state(
            grpc::ServerContext*,
            const Api::Keyset::KeysetStateRequest* _api_request,
            Api::Keyset::KeysetStateReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_state requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: keyset_state(" + request.to_string() + ")");
            const auto state = keyset_state(request);
            proto_wrap(state, _api_response);
            FREDLOG_DEBUG("server: answer = " + state.to_string());
            return grpc::Status::OK;
        }
        catch (const KeysetStateReply::Exception::KeysetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_keyset_state_history(
            grpc::ServerContext*,
            const Api::Keyset::KeysetStateHistoryRequest* _api_request,
            Api::Keyset::KeysetStateHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_state_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: keyset_state_history(" + request.to_string() + ")");
            const auto state_history = keyset_state_history(request);
            proto_wrap(state_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + state_history.to_string());
            return grpc::Status::OK;
        }
        catch (const KeysetStateHistoryReply::Exception::KeysetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const KeysetStateHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_keyset_state_flags(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Api::Keyset::KeysetStateFlagsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_keyset_state_flags requested");
            const auto info = get_keyset_state_flags_info();
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + to_string(info));
            return grpc::Status::OK;
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status list_keysets_by_contact(
            grpc::ServerContext* context,
            const ::Fred::Registry::Api::Keyset::ListKeysetsByContactRequest* api_request,
            grpc::ServerWriter<Api::Keyset::ListKeysetsByContactReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_keysets_by_contact requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_keysets_by_contact(" + request.to_string() + ")");
            const auto reply = Keyset::list_keysets_by_contact(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Keyset::ListKeysetsByContactReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const ListKeysetsByContactReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            Api::Keyset::ListKeysetsByContactReply response;
            proto_wrap(e, &response);
            api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const ListKeysetsByContactReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            Api::Keyset::ListKeysetsByContactReply response;
            proto_wrap(e, &response);
            api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

class SearchKeysetService final : public Api::Keyset::SearchKeyset::SearchKeyset::Service
{
private:
    grpc::Status search_keyset(
            grpc::ServerContext*,
            const Api::Keyset::SearchKeysetRequest* _api_request,
            Api::Keyset::SearchKeysetReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_keyset requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("_api_request = " + request.to_string());
            const auto response = Fred::Registry::Keyset::search_keyset(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status search_keyset_history(
            grpc::ServerContext*,
            const Api::Keyset::SearchKeysetHistoryRequest* _api_request,
            Api::Keyset::SearchKeysetHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_keyset_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_keyset_history(" + request.to_string() + ")");
            const auto response = Fred::Registry::Keyset::search_keyset(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const SearchKeysetHistoryReply::Exception::ChronologyViolation& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

}//namespace Fred::Registry::Keyset::{anonymous}

}//namespace Fred::Registry::Keyset

namespace Nsset {

namespace {

class NssetService final : public Api::Nsset::Nsset::Service
{
private:
    grpc::Status get_nsset_handle_history(
            grpc::ServerContext*,
            const Api::Nsset::NssetHandleHistoryRequest* _api_request,
            Api::Nsset::NssetHandleHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_handle_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: nsset_handle_history(" + request.to_string() + ")");
            const auto handle_history = nsset_handle_history(request);
            proto_wrap(handle_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + handle_history.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_nsset_history(
            grpc::ServerContext*,
            const Api::Nsset::NssetHistoryRequest* _api_request,
            Api::Nsset::NssetHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: nsset_history(" + request.to_string() + ")");
            const auto data_history = nsset_data_history(request);
            proto_wrap(data_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + data_history.to_string());
            return grpc::Status::OK;
        }
        catch (const NssetDataHistoryReply::Exception::NssetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const NssetDataHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_nsset_info(
            grpc::ServerContext*,
            const Api::Nsset::NssetInfoRequest* _api_request,
            Api::Nsset::NssetInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: nsset_info(" + request.to_string() + ")");
            const auto info = nsset_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const NssetInfoReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const NssetInfoReply::Exception::NssetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status batch_nsset_info(
            grpc::ServerContext* context,
            grpc::ServerReaderWriter<Api::Nsset::BatchNssetInfoReply,
                                     Api::Nsset::BatchNssetInfoRequest>* api_stream) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: batch_nsset_info requested");
            Api::Nsset::BatchNssetInfoRequest request_chunk;
            Nsset::BatchNssetInfoRequest request{};
            while (api_stream->Read(&request_chunk))
            {
                proto_unwrap_back(request_chunk, request);
            }
            if (context->IsCancelled())
            {
                FREDLOG_WARNING("server: api stream cancelled");
                return grpc::Status::CANCELLED;
            }
            const auto reply = Nsset::batch_nsset_info(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Nsset::BatchNssetInfoReply response{};
                offset = proto_wrap(reply, offset, &response);
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_stream->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const auto stream_is_ok = api_stream->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_nsset_id(
            grpc::ServerContext*,
            const Api::Nsset::NssetIdRequest* _api_request,
            Api::Nsset::NssetIdReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_id requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: get_nsset_id(" + request.to_string() + ")");
            const auto id = Fred::Registry::Nsset::get_nsset_id(request);
            proto_wrap(id, _api_response);
            FREDLOG_DEBUG("server: answer = " + id.to_string());
            return grpc::Status::OK;
        }
        catch (const NssetIdReply::Exception::NssetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_nsset_state(
            grpc::ServerContext*,
            const Api::Nsset::NssetStateRequest* _api_request,
            Api::Nsset::NssetStateReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_state requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: nsset_state(" + request.to_string() + ")");
            const auto state = nsset_state(request);
            proto_wrap(state, _api_response);
            FREDLOG_DEBUG("server: answer = " + state.to_string());
            return grpc::Status::OK;
        }
        catch (const NssetStateReply::Exception::NssetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_nsset_state_history(
            grpc::ServerContext*,
            const Api::Nsset::NssetStateHistoryRequest* _api_request,
            Api::Nsset::NssetStateHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_state_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: nsset_state_history(" + request.to_string() + ")");
            const auto state_history = nsset_state_history(request);
            proto_wrap(state_history, _api_response);
            FREDLOG_DEBUG("server: answer = " + state_history.to_string());
            return grpc::Status::OK;
        }
        catch (const NssetStateHistoryReply::Exception::NssetDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const NssetStateHistoryReply::Exception::InvalidHistoryInterval& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_nsset_state_flags(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Api::Nsset::NssetStateFlagsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_nsset_state_flags requested");
            const auto info = get_nsset_state_flags_info();
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + to_string(info));
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status check_dns_host(
            grpc::ServerContext*,
            const Api::Nsset::CheckDnsHostRequest* _api_request,
            Api::Nsset::CheckDnsHostReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: check_dns_host requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: check_dns_host(" + request.to_string() + ")");
            const auto info = Fred::Registry::Nsset::check_dns_host(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }

    grpc::Status list_nssets(
            grpc::ServerContext* context,
            const ::Fred::Registry::Api::Nsset::ListNssetsRequest* api_request,
            grpc::ServerWriter<Api::Nsset::ListNssetsReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_nssets requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_nssets(" + request.to_string() + ")");
            const auto reply = Nsset::list_nssets(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Nsset::ListNssetsReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }

    grpc::Status list_nssets_by_contact(
            grpc::ServerContext* context,
            const ::Fred::Registry::Api::Nsset::ListNssetsByContactRequest* api_request,
            grpc::ServerWriter<Api::Nsset::ListNssetsByContactReply>* api_response_writer) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: list_nssets_by_contact requested");
            const auto request = proto_unwrap(*api_request);
            FREDLOG_DEBUG("server: list_nssets_by_contact(" + request.to_string() + ")");
            const auto reply = Nsset::list_nssets_by_contact(request);
            std::size_t offset = 0;
            while (true)
            {
                Api::Nsset::ListNssetsByContactReply response;
                offset = proto_wrap(reply, offset, response.mutable_data());
                const bool last_chunk = offset == 0;
                if (last_chunk)
                {
                    api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
                    break;
                }
                const bool stream_is_ok = api_response_writer->Write(response);
                if (!stream_is_ok || context->IsCancelled())
                {
                    FREDLOG_WARNING("server: api stream cancelled");
                    return grpc::Status::CANCELLED;
                }
            }
            return grpc::Status::OK;
        }
        catch (const ListNssetsByContactReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            Api::Nsset::ListNssetsByContactReply response;
            proto_wrap(e, &response);
            api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const ListNssetsByContactReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            Api::Nsset::ListNssetsByContactReply response;
            proto_wrap(e, &response);
            api_response_writer->WriteLast(response, ::grpc::WriteOptions{});
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

class SearchNssetService final : public Api::Nsset::SearchNsset::SearchNsset::Service
{
private:
    grpc::Status search_nsset(
            grpc::ServerContext*,
            const Api::Nsset::SearchNssetRequest* _api_request,
            Api::Nsset::SearchNssetReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_nsset requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_nsset(" + request.to_string() + ")");
            const auto response = Fred::Registry::Nsset::search_nsset(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status search_nsset_history(
            grpc::ServerContext*,
            const Api::Nsset::SearchNssetHistoryRequest* _api_request,
            Api::Nsset::SearchNssetHistoryReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: search_nsset_history requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: search_nsset_history(" + request.to_string() + ")");
            const auto response = Fred::Registry::Nsset::search_nsset(request);
            proto_wrap(response, _api_response);
            FREDLOG_DEBUG("server: answer = " + response.to_string());
            return grpc::Status::OK;
        }
        catch (const SearchNssetHistoryReply::Exception::ChronologyViolation& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const Database::ResultFailed& e)
        {
            return on_database_exception(e);
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

}//namespace Fred::Registry::Nsset::{anonymous}

}//namespace Fred::Registry::Nsset

namespace Registrar {

namespace {

class RegistrarService final : public Api::Registrar::Registrar::Service
{
private:
    grpc::Status get_registrar_credit(
            grpc::ServerContext*,
            const Api::Registrar::RegistrarCreditRequest* _api_request,
            Api::Registrar::RegistrarCreditReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_credit requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: registrar_credit(" + request.to_string() + ")");
            const auto info = registrar_credit(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const RegistrarCreditReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_info(
            grpc::ServerContext*,
            const Api::Registrar::RegistrarInfoRequest* _api_request,
            Api::Registrar::RegistrarInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: registrar_info(" + request.to_string() + ")");
            const auto info = registrar_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const RegistrarInfoReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_handles(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Api::Registrar::RegistrarListReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_handles requested");
            FREDLOG_DEBUG("server: registrar_list()");
            const auto info = registrar_list();
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_zone_access(
            grpc::ServerContext*,
            const Api::Registrar::RegistrarZoneAccessRequest* _api_request,
            Api::Registrar::RegistrarZoneAccessReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_zone_access requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: registrar_zone_access(" + request.to_string() + ")");
            const auto info = registrar_zone_access(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const RegistrarZoneAccessReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_epp_credentials(
            grpc::ServerContext*,
            const Api::Registrar::GetRegistrarEppCredentialsRequest* _api_request,
            Api::Registrar::GetRegistrarEppCredentialsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_epp_credentials requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: get_registrar_epp_credentials(" + request.to_string() + ")");
            const auto info = registrar_epp_credentials(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const GetRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_groups(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Fred::Registry::Api::Registrar::GetRegistrarGroupsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_groups requested");
            const auto info = registrar_groups();
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_groups_membership(
            grpc::ServerContext*,
            const Fred::Registry::Api::Registrar::GetRegistrarGroupsMembershipRequest* _api_request,
            Fred::Registry::Api::Registrar::GetRegistrarGroupsMembershipReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_groups_membership requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: get_registrar_groups_membership(" + request.to_string() + ")");
            const auto info = Registrar::get_registrar_groups_membership(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const GetRegistrarGroupsMembershipReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status get_registrar_certifications(
            grpc::ServerContext*,
            const Fred::Registry::Api::Registrar::GetRegistrarCertificationsRequest* _api_request,
            Fred::Registry::Api::Registrar::GetRegistrarCertificationsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_registrar_certifications requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: get_registrar_certifications(" + request.to_string() + ")");
            const auto info = Registrar::get_registrar_certifications(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const GetRegistrarCertificationsReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

class AdminService final : public Api::Registrar::Admin::Service
{
private:
    grpc::Status create_registrar(
            grpc::ServerContext*,
            const Api::Registrar::CreateRegistrarRequest* _api_request,
            Api::Registrar::CreateRegistrarReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: create_registrar requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: create_registrar(" + request.to_string() + ")");
            const auto result = Registrar::create_registrar(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const CreateRegistrarReply::Exception::RegistrarAlreadyExists& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateRegistrarReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_registrar(
            grpc::ServerContext*,
            const Api::Registrar::UpdateRegistrarRequest* _api_request,
            Api::Registrar::UpdateRegistrarReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_registrar requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_registrar(" + request.to_string() + ")");
            const auto result = Registrar::update_registrar(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status add_registrar_zone_access(
            grpc::ServerContext*,
            const Api::Registrar::AddRegistrarZoneAccessRequest* _api_request,
            Api::Registrar::AddRegistrarZoneAccessReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: add_registrar_zone_access requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: add_registrar_zone_access(" + request.to_string() + ")");
            const auto result = Registrar::add_registrar_zone_access(request);
            proto_wrap(result, _api_response->mutable_data());
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const AddRegistrarZoneAccessReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const AddRegistrarZoneAccessReply::Exception::ZoneDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const AddRegistrarZoneAccessReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status delete_registrar_zone_access(
            grpc::ServerContext*,
            const Api::Registrar::DeleteRegistrarZoneAccessRequest* _api_request,
            Api::Registrar::DeleteRegistrarZoneAccessReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: delete_registrar_zone_access requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: delete_registrar_zone_access(" + request.to_string() + ")");
            Registrar::delete_registrar_zone_access(request);
            FREDLOG_DEBUG("server: answer = void");
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarZoneAccessReply::Exception::HistoryChangeProhibited& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarZoneAccessReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_registrar_zone_access(
            grpc::ServerContext*,
            const Api::Registrar::UpdateRegistrarZoneAccessRequest* _api_request,
            Api::Registrar::UpdateRegistrarZoneAccessReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_registrar_zone_access requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_registrar_zone_access(" + request.to_string() + ")");
            const auto result = Registrar::update_registrar_zone_access(request);
            proto_wrap(result, _api_response->mutable_data());
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarZoneAccessReply::Exception::HistoryChangeProhibited& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarZoneAccessReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status add_registrar_epp_credentials(
            grpc::ServerContext*,
            const Api::Registrar::AddRegistrarEppCredentialsRequest* _api_request,
            Api::Registrar::AddRegistrarEppCredentialsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: add_registrar_epp_credentials requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: add_registrar_epp_credentials(" + request.to_string() + ")");
            const auto result = Registrar::add_registrar_epp_credentials(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const AddRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const AddRegistrarEppCredentialsReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status delete_registrar_epp_credentials(
            grpc::ServerContext*,
            const Api::Registrar::DeleteRegistrarEppCredentialsRequest* _api_request,
            Api::Registrar::DeleteRegistrarEppCredentialsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: delete_registrar_epp_credentials requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: delete_registrar_epp_credentials(" + request.to_string() + ")");
            Registrar::delete_registrar_epp_credentials(request);
            FREDLOG_DEBUG("server: answer = void");
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarEppCredentialsReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_registrar_epp_credentials(
            grpc::ServerContext*,
            const Api::Registrar::UpdateRegistrarEppCredentialsRequest* _api_request,
            Api::Registrar::UpdateRegistrarEppCredentialsReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_registrar_epp_credentials requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_registrar_epp_credentials(" + request.to_string() + ")");
            Registrar::update_registrar_epp_credentials(request);
            FREDLOG_DEBUG("server: answer = void");
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarEppCredentialsReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status add_registrar_group_membership(
            grpc::ServerContext*,
            const Fred::Registry::Api::Registrar::AddRegistrarGroupMembershipRequest* _api_request,
            Fred::Registry::Api::Registrar::AddRegistrarGroupMembershipReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: add_registrar_group_membership requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: add_registrar_group_membership(" + request.to_string() + ")");
            const auto result = Registrar::add_registrar_group_membership(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const AddRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const AddRegistrarGroupMembershipReply::Exception::GroupDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status delete_registrar_group_membership(
            grpc::ServerContext*,
            const Fred::Registry::Api::Registrar::DeleteRegistrarGroupMembershipRequest* _api_request,
            Fred::Registry::Api::Registrar::DeleteRegistrarGroupMembershipReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: delete_registrar_group_membership requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: delete_registrar_group_membership(" + request.to_string() + ")");
            const auto result = Registrar::delete_registrar_group_membership(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarGroupMembershipReply::Exception::GroupDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status add_registrar_certification(
            grpc::ServerContext*,
            const Api::Registrar::AddRegistrarCertificationRequest* _api_request,
            Api::Registrar::AddRegistrarCertificationReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: add_registrar_certification requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: add_registrar_certification(" + request.to_string() + ")");
            const auto result = Registrar::add_registrar_certification(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const AddRegistrarCertificationReply::Exception::RegistrarDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const AddRegistrarCertificationReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_registrar_certification(
            grpc::ServerContext*,
            const Api::Registrar::UpdateRegistrarCertificationRequest* _api_request,
            Api::Registrar::UpdateRegistrarCertificationReply* _api_response)
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_registrar_certification requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_registrar_certification(" + request.to_string() + ")");
            const auto result = Registrar::update_registrar_certification(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateRegistrarCertificationReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status delete_registrar_certification(
            grpc::ServerContext*,
            const Api::Registrar::DeleteRegistrarCertificationRequest* _api_request,
            Api::Registrar::DeleteRegistrarCertificationReply* _api_response)
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: delete_registrar_certification requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: delete_registrar_certification(" + request.to_string() + ")");
            Registrar::delete_registrar_certification(request);
            FREDLOG_DEBUG("server: answer received");
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const DeleteRegistrarCertificationReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

}//namespace Fred::Registry::Registrar::{anonymous}

}//namespace Fred::Registry::Registrar

namespace ContactRepresentative {

namespace {

class ContactRepresentativeService final : public Api::ContactRepresentative::ContactRepresentative::Service
{
private:
    grpc::Status get_contact_representative_info(
            grpc::ServerContext*,
            const Api::ContactRepresentative::ContactRepresentativeInfoRequest* _api_request,
            Api::ContactRepresentative::ContactRepresentativeInfoReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: get_contact_representative_info requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: contact_representative_info(" + request.to_string() + ")");
            const auto info = contact_representative_info(request);
            proto_wrap(info, _api_response);
            FREDLOG_DEBUG("server: answer = " + info.to_string());
            return grpc::Status::OK;
        }
        catch (const ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status create_contact_representative(
            grpc::ServerContext*,
            const Api::ContactRepresentative::CreateContactRepresentativeRequest* _api_request,
            Api::ContactRepresentative::CreateContactRepresentativeReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: create_contact_representative requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: create_contact_representative(" + request.to_string() + ")");
            const auto result = ContactRepresentative::create_contact_representative(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const CreateContactRepresentativeReply::Exception::ContactRepresentativeAlreadyExists& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateContactRepresentativeReply::Exception::ContactDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const CreateContactRepresentativeReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status update_contact_representative(
            grpc::ServerContext*,
            const Api::ContactRepresentative::UpdateContactRepresentativeRequest* _api_request,
            Api::ContactRepresentative::UpdateContactRepresentativeReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: update_contact_representative requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: update_contact_representative(" + request.to_string() + ")");
            const auto result = ContactRepresentative::update_contact_representative(request);
            proto_wrap(result, _api_response);
            FREDLOG_DEBUG("server: answer = " + result.to_string());
            return grpc::Status::OK;
        }
        catch (const UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const UpdateContactRepresentativeReply::Exception::InvalidData& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
    grpc::Status delete_contact_representative(
            grpc::ServerContext*,
            const Api::ContactRepresentative::DeleteContactRepresentativeRequest* _api_request,
            Api::ContactRepresentative::DeleteContactRepresentativeReply* _api_response) override
    {
        FREDLOG_SET_CONTEXT(LogContext, log_context, create_ctx_function_name(__func__));
        try
        {
            FREDLOG_INFO("server: delete_contact_representative requested");
            const auto request = proto_unwrap(*_api_request);
            FREDLOG_DEBUG("server: delete_contact_representative(" + request.to_string() + ")");
            ContactRepresentative::delete_contact_representative(request);
            FREDLOG_DEBUG("server: answer = void");
            return grpc::Status::OK;
        }
        catch (const DeleteContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist& e)
        {
            FREDLOG_INFO(std::string("server: ") + e.what());
            proto_wrap(e, _api_response);
            return grpc::Status::OK;
        }
        catch (const Registry::DatabaseError& e)
        {
            return on_database_error(e);
        }
        catch (const Registry::InternalServerError& e)
        {
            return on_internal_server_error(static_cast<const std::exception&>(e));
        }
        catch (const std::exception& e)
        {
            return on_std_exception(e);
        }
        catch (...)
        {
            return on_unknown_exception();
        }
    }
};

} // namespace Fred::Registry::ContactRepresentative::{anonymous}

} // namespace Fred::Registry::ContactRepresentative

namespace {

void run_server(const std::string& server_address)
{
    ::sigset_t blocked_signals;

    // Other threads created by grpc::Server will inherit a copy of the signal mask.
    ::sigemptyset(&blocked_signals);
    ::sigaddset(&blocked_signals, SIGHUP);
    ::sigaddset(&blocked_signals, SIGINT);
    ::sigaddset(&blocked_signals, SIGQUIT);
    ::sigaddset(&blocked_signals, SIGTERM);
    const auto pthread_sigmask_result = ::pthread_sigmask(SIG_BLOCK, &blocked_signals, nullptr);
    constexpr int operation_success = 0;
    if (pthread_sigmask_result != operation_success)
    {
        throw std::runtime_error(std::strerror(pthread_sigmask_result));
    }

    auto diagnostics_service =
            LibDiagnostics::make_grpc_service(
                    Fred::Registry::Diagnostics::about,
                    Fred::Registry::Diagnostics::status);

    Registry::Contact::ContactService contact_service;
    Registry::Domain::AdminService domain_admin_service;
    Registry::Domain::DomainService domain_service;
    Registry::DomainBlacklist::DomainBlacklistService domain_blacklist_service;
    Registry::Keyset::KeysetService keyset_service;
    Registry::Nsset::NssetService nsset_service;
    Registry::Registrar::AdminService registrar_admin_service;
    Registry::Registrar::RegistrarService registrar_service;

    Registry::Contact::SearchContactService search_contact_service;
    Registry::Domain::SearchDomainService search_domain_service;
    Registry::Keyset::SearchKeysetService search_keyset_service;
    Registry::Nsset::SearchNssetService search_nsset_service;

    Registry::ContactRepresentative::ContactRepresentativeService contact_representative_service;

    grpc::ServerBuilder builder;

    // Listen on the given address without any authentication mechanism.
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

    // Register "service" as the instance through which we'll communicate with
    // clients. In this case it corresponds to an *synchronous* service.
    builder.RegisterService(diagnostics_service.get());
    builder.RegisterService(&contact_service);
    builder.RegisterService(&domain_admin_service);
    builder.RegisterService(&domain_service);
    builder.RegisterService(&domain_blacklist_service);
    builder.RegisterService(&keyset_service);
    builder.RegisterService(&nsset_service);
    builder.RegisterService(&registrar_admin_service);
    builder.RegisterService(&registrar_service);

    builder.RegisterService(&search_contact_service);
    builder.RegisterService(&search_domain_service);
    builder.RegisterService(&search_keyset_service);
    builder.RegisterService(&search_nsset_service);

    builder.RegisterService(&contact_representative_service);

    // Finally assemble the server.
    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    FREDLOG_INFO("server: listen on " + server_address);

    int received_signal;
    const auto sigwait_result = ::sigwait(&blocked_signals, &received_signal);
    if (sigwait_result != operation_success)
    {
        throw std::runtime_error(std::strerror(sigwait_result));
    }
    switch (received_signal)
    {
        case SIGHUP:
            FREDLOG_DEBUG("SIGHUP received");
            break;
        case SIGINT:
            FREDLOG_DEBUG("SIGINT received");
            break;
        case SIGQUIT:
            FREDLOG_DEBUG("SIGQUIT received");
            break;
        case SIGTERM:
            FREDLOG_DEBUG("SIGTERM received");
            break;
        default:
            throw std::runtime_error("unexpected signal received");
    }
    const std::chrono::system_clock::time_point deadline = std::chrono::system_clock::now() + std::chrono::seconds(10);
    server->Shutdown(deadline);
}

}//namespace Fred::Registry::{anonymous}

}//namespace Fred::Registry
}//namespace Fred

namespace {

enum class Logger
{
    is_running,
    is_not_running
};

auto severity_to_level(Cfg::LogSeverity severity)
{
    switch (severity)
    {
        case Cfg::LogSeverity::emerg:
            return LibLog::Level::critical;
        case Cfg::LogSeverity::alert:
            return LibLog::Level::critical;
        case Cfg::LogSeverity::crit:
            return LibLog::Level::critical;
        case Cfg::LogSeverity::err:
            return LibLog::Level::error;
        case Cfg::LogSeverity::warning:
            return LibLog::Level::warning;
        case Cfg::LogSeverity::notice:
            return LibLog::Level::info;
        case Cfg::LogSeverity::info:
            return LibLog::Level::info;
        case Cfg::LogSeverity::debug:
            return LibLog::Level::debug;
        case Cfg::LogSeverity::trace:
            return LibLog::Level::trace;
    }
    return LibLog::Level::trace;
}

struct SetupLogging : boost::static_visitor<Logger>
{
    Logger operator()(const boost::blank&) const
    {
        return Logger::is_not_running;
    }
    Logger operator()(const Cfg::Options::Console& option) const
    {
        LibLog::Sink::ConsoleSinkConfig config;
        LibLog::LogConfig log_config;
        config.set_level(severity_to_level(option.min_severity));
        config.set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr);
        config.set_color_mode(LibLog::ColorMode::never);
        log_config.add_sink_config(config);
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        return Logger::is_running;
    }
    Logger operator()(const Cfg::Options::LogFile& option) const
    {
        LibLog::Sink::FileSinkConfig config{option.file_name};
        LibLog::LogConfig log_config;
        config.set_level(severity_to_level(option.min_severity));
        log_config.add_sink_config(config);
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        return Logger::is_running;
    }
    Logger operator()(const Cfg::Options::SysLog& option) const
    {
        LibLog::Sink::SyslogSinkConfig config;
        LibLog::LogConfig log_config;
        if (!option.ident.empty())
        {
            config.set_syslog_ident(option.ident);
        }
        config.set_level(severity_to_level(option.min_severity));
        config.set_syslog_facility(option.facility);
        log_config.add_sink_config(config);
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        return Logger::is_running;
    }
};

std::string make_connect_string(const Cfg::Options::Database& option)
{
    std::string result;
    if (option.host != boost::none)
    {
        result = "host=" + *option.host;
    }
    if (option.port != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "port=" + std::to_string(*option.port);
    }
    if (option.user != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "user=" + *option.user;
    }
    if (option.dbname != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "dbname=" + *option.dbname;
    }
    if (option.password != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "password=" + *option.password;
    }
    return result;
}

}//namespace::{anonymous}

int main(int argc, char** argv)
{
    try
    {
        Cfg::Options::init(argc, argv);
    }
    catch (const Cfg::AllDone& e)
    {
        std::cout << e.what() << std::endl;
        return EXIT_SUCCESS;
    }
    catch (const Cfg::UnknownOption& unknown_option)
    {
        std::cerr << unknown_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::MissingOption& missing_option)
    {
        std::cerr << missing_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::Exception& e)
    {
        std::cerr << "Configuration problem: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }

    switch (boost::apply_visitor(SetupLogging{}, Cfg::Options::get().log))
    {
        case Logger::is_running:
            FREDLOG_DEBUG("fred-registry-services configuration successfully loaded");
            break;
        case Logger::is_not_running:
            std::cout << "fred-registry-services configuration successfully loaded" << std::endl;
            break;
    }
    Database::emplace_default_manager<Database::StandaloneManager>(
            make_connect_string(Cfg::Options::get().database));

    try
    {
        Fred::Registry::run_server(Cfg::Options::get().registry.listen_on);
        FREDLOG_INFO("Server shutdown");
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }
}
