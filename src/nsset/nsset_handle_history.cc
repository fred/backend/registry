/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/nsset/nsset_handle_history.hh"

#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/nsset/get_nsset_handle_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

NssetHandleHistoryRequest::NssetHandleHistoryRequest(const NssetHandle& _nsset_handle)
    : nsset_handle(_nsset_handle)
{
}

std::string NssetHandleHistoryRequest::to_string() const
{
    return Util::StructToString().add("nsset_handle", nsset_handle)
                                 .finish();
}

std::string NssetLifetime::TimeSpec::to_string() const
{
    return Util::StructToString().add("nsset_history_id", nsset_history_id)
                                 .add("timestamp", timestamp)
                                 .finish();
}

std::string NssetLifetime::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("begin", begin)
                                 .add("end", end)
                                 .finish();
}

std::string NssetHandleHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("nsset_handle", nsset_handle)
                                 .add("timeline", timeline)
                                 .finish();
}

NssetHandleHistoryReply::Data nsset_handle_history(const NssetHandleHistoryRequest& request)
{
    try
    {
        using GetNssetHandleHistory = LibFred::RegistrableObject::Nsset::GetNssetHandleHistory;
        LibFred::OperationContextCreator ctx;
        return fred_unwrap(GetNssetHandleHistory(get_raw_value_from(request.nsset_handle)).exec(ctx));
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
