/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/nsset/nsset_state_flags.hh"

#include "libfred/db_settings.hh"

#include <type_traits>

namespace Fred {
namespace Registry {
namespace Nsset {

namespace {

template <typename ...Flags>
struct StateFlagsInfoCollector
{
    using Info = std::array<StateFlagInfo, sizeof...(Flags)>;
    template <typename F, int idx>
    ::Util::FlagSetVisiting visit()
    {
        static_assert((0 <= idx) && (idx < sizeof...(Flags)), "idx out of range");
        info[idx] = StateFlagInfo::make_from<F>();
        return ::Util::FlagSetVisiting::can_continue;
    }
    Info info;
};

template <LibFred::RegistrableObject::State::Flag::Manipulation>
constexpr StateFlagInfo::Manipulation into_manipulation();

template <>
constexpr StateFlagInfo::Manipulation into_manipulation<LibFred::RegistrableObject::State::Flag::Manipulation::automatic>()
{
    return StateFlagInfo::Manipulation::automatic;
}

template <>
constexpr StateFlagInfo::Manipulation into_manipulation<LibFred::RegistrableObject::State::Flag::Manipulation::manual>()
{
    return StateFlagInfo::Manipulation::manual;
}

template <LibFred::RegistrableObject::State::Flag::Visibility>
constexpr StateFlagInfo::Visibility into_visibility();

template <>
constexpr StateFlagInfo::Visibility into_visibility<LibFred::RegistrableObject::State::Flag::Visibility::external>()
{
    return StateFlagInfo::Visibility::external;
}

template <>
constexpr StateFlagInfo::Visibility into_visibility<LibFred::RegistrableObject::State::Flag::Visibility::internal>()
{
    return StateFlagInfo::Visibility::internal;
}

} // namespace Fred::Registry::Nsset::{anonymous}

template <typename F>
StateFlagInfo StateFlagInfo::make_from()
{
    StateFlagInfo result;
    result.name = F::name;
    result.how_to_set = into_manipulation<F::how_to_set>();
    result.visibility = into_visibility<F::visibility>();
    return result;
}

std::string StateFlagInfo::to_string() const
{
    std::string result = name;
    switch (how_to_set)
    {
        case Manipulation::automatic:
            result += " automatic";
            break;
        case Manipulation::manual:
            result += " manual";
            break;
    }
    switch (visibility)
    {
        case Visibility::external:
            result += " external";
            break;
        case Visibility::internal:
            result += " internal";
            break;
    }
    return result;
}

NssetStateFlagsInfo get_nsset_state_flags_info()
{
    static_assert(std::is_same<decltype(LibFred::RegistrableObject::Nsset::NssetState::static_visit<StateFlagsInfoCollector>().info),
                               NssetStateFlagsInfo>::value,
                  "incompatible types");
    return LibFred::RegistrableObject::Nsset::NssetState::static_visit<StateFlagsInfoCollector>().info;
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
