/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NSSET_INFO_REPLY_HH_2303A757249D4C799EB411F3BD2A5A51
#define NSSET_INFO_REPLY_HH_2303A757249D4C799EB411F3BD2A5A51

#include "src/contact/contact_common_types.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/nsset/nsset_info_request.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/object/object_common_types.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"
#include "src/exceptions.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>
  
#include <cstdint>
#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetInfoReply
{
    struct Data : Util::Printable<Data>
    {
        NssetHandle nsset_handle;
        NssetId nsset_id;
        NssetHistoryId nsset_history_id;
        std::vector<DnsHost> dns_hosts;
        std::vector<Contact::ContactLightInfo> technical_contacts;
        boost::optional<TechnicalCheckLevel> technical_check_level;
        Object::ObjectEvents representative_events;
        Registrar::RegistrarHandle sponsoring_registrar;
        boost::optional<std::int64_t> domains_count;
        std::string to_string() const;
    };
    struct Exception : Util::Printable<Exception>
    {
        struct NssetDoesNotExist : Registry::Exception
        {
            explicit NssetDoesNotExist(const NssetInfoRequest& src);
        };
        struct InvalidData : Registry::Exception, Util::Printable<InvalidData>
        {
            explicit InvalidData(const NssetInfoRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
            std::string to_string() const;
        };
        explicit Exception(InvalidData);
        explicit Exception(NssetDoesNotExist);
        using Reasons = boost::variant<InvalidData, NssetDoesNotExist>;
        Reasons reasons;
        std::string to_string() const;
    };
    using Result = boost::variant<Data, Exception::InvalidData, Exception::NssetDoesNotExist>;
};

struct BatchNssetInfoReply
{
    struct Error : Util::Printable<Error>
    {
        explicit Error(BatchNssetInfoRequest::Request, NssetInfoReply::Exception);
        BatchNssetInfoRequest::Request request;
        NssetInfoReply::Exception exception;
        std::string to_string() const;
    };
    using DataOrError = boost::variant<NssetInfoReply::Data, Error>;
    struct Data : Util::Printable<Data>
    {
        struct BatchReply : Util::Printable<BatchReply>
        {
            DataOrError data_or_error;
            std::string to_string() const;
        };
        std::vector<BatchReply> replies;
        std::string to_string() const;
    };
};

struct NssetIdReply
{
    struct Data : Util::Printable<Data>
    {
        NssetId nsset_id;
        NssetHistoryId nsset_history_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct NssetDoesNotExist : Registry::Exception
        {
            explicit NssetDoesNotExist(const NssetIdRequest& src);
        };
    };
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
