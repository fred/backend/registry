/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/nsset/nsset_info_reply.hh"

#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Nsset {

std::string NssetInfoReply::Data::to_string() const
{
    return Util::StructToString().add("nsset_handle", nsset_handle)
                                 .add("nsset_id", nsset_id)
                                 .add("nsset_history_id", nsset_history_id)
                                 .add("dns_hosts", dns_hosts)
                                 .add("technical_contacts", technical_contacts)
                                 .add("technical_check_level", technical_check_level)
                                 .add("representative_events", representative_events)
                                 .add("sponsoring_registrar", sponsoring_registrar)
                                 .add("domains_count", domains_count)
                                 .finish();
}

NssetInfoReply::Exception::InvalidData::InvalidData(
        const NssetInfoRequest&,
        std::set<std::string> fields)
    : Registry::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

NssetInfoReply::Exception::NssetDoesNotExist::NssetDoesNotExist(const NssetInfoRequest& src)
    : Registry::Exception("nsset " + src.to_string() + " does not exist")
{
}

std::string NssetInfoReply::Exception::InvalidData::to_string() const
{
    return Util::StructToString().add("fields", fields)
                                 .finish();
}

NssetInfoReply::Exception::Exception(InvalidData src)
    : reasons{std::move(src)}
{ }

NssetInfoReply::Exception::Exception(NssetDoesNotExist src)
    : reasons{std::move(src)}
{ }

std::string NssetInfoReply::Exception::to_string() const
{
    struct Printer : boost::static_visitor<std::string>
    {
        std::string operator()(const NssetDoesNotExist&) const
        {
            return "nsset_does_not_exist";
        }
        std::string operator()(const InvalidData& invalid_data) const
        {
            return Util::StructToString{}.add("invalid_data", invalid_data)
                                         .finish();
        }
    };
    return boost::apply_visitor(Printer{}, reasons);
}

std::string BatchNssetInfoReply::Data::BatchReply::to_string() const
{
    struct Printer : boost::static_visitor<std::string>
    {
        std::string operator()(const NssetInfoReply::Data& data) const
        {
            return Util::StructToString{}.add("data", data)
                                         .finish();
        }
        std::string operator()(const Error& error) const
        {
            return Util::StructToString{}.add("error", error)
                                         .finish();
        }
    };
    return boost::apply_visitor(Printer{}, data_or_error);
}

BatchNssetInfoReply::Error::Error(BatchNssetInfoRequest::Request request, NssetInfoReply::Exception exception)
    : request{std::move(request)},
      exception{std::move(exception)}
{ }

std::string BatchNssetInfoReply::Error::to_string() const
{
    return Util::StructToString{}.add("request", request)
                                 .add("exception", exception)
                                 .finish();
}

std::string BatchNssetInfoReply::Data::to_string() const
{
    return Util::StructToString{}.add("replies", replies)
                                 .finish();
}

std::string NssetIdReply::Data::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("nsset_history_id", nsset_history_id)
                                 .finish();
}

NssetIdReply::Exception::NssetDoesNotExist::NssetDoesNotExist(const NssetIdRequest& src)
    : Registry::Exception("nsset " + src.to_string() + " does not exist")
{
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
