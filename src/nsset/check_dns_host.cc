/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/nsset/check_dns_host.hh"

#include "src/exceptions.hh"

#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <string>
#include <exception>

using namespace Fred::Registry::Nsset;

CheckDnsHostReply::Data Fred::Registry::Nsset::check_dns_host(const CheckDnsHostRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        CheckDnsHostReply::Data result;
        result.dns_host_exists = 0 < ctx.get_conn().exec_params(
                "SELECT "
                  "FROM host "
                 "WHERE fqdn = LOWER($1::TEXT) "
                 "LIMIT 1",
                Database::QueryParams{request.fqdn}).size();
        ctx.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }

}
