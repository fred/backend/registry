/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_NSSETS_BY_CONTACT_REPLY_HH_F56B41C151705789F4DB5167FC8B8291//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_NSSETS_BY_CONTACT_REPLY_HH_F56B41C151705789F4DB5167FC8B8291

#include "src/contact/contact_common_types.hh"
#include "src/exceptions.hh"
#include "src/nsset/list_nssets_by_contact_request.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/object/object_common_types.hh"
#include "src/pagination_reply.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct ListNssetsByContactReply
{
    struct Data : Util::Printable<Data>
    {
        struct Nsset : Util::Printable<Nsset>
        {
            NssetLightInfo nsset;
            NssetHistoryId nsset_history_id;
            bool is_deleted;
            std::string to_string() const;
        };
        std::vector<Nsset> nssets;
        boost::optional<PaginationReply> pagination;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ListNssetsByContactRequest& src);
        };
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const ListNssetsByContactRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif//LIST_NSSETS_BY_CONTACT_REPLY_HH_F56B41C151705789F4DB5167FC8B8291
