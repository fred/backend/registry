/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NSSET_INFO_REQUEST_HH_D796CB12695542BCBE7D4131C9BF8051
#define NSSET_INFO_REQUEST_HH_D796CB12695542BCBE7D4131C9BF8051

#include "src/nsset/nsset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetInfoRequest : Util::Printable<NssetInfoRequest>
{
    NssetId nsset_id;
    boost::optional<NssetHistoryId> nsset_history_id;
    bool include_domains_count;
    std::string to_string() const;
};

struct BatchNssetInfoRequest : Util::Printable<BatchNssetInfoRequest>
{
    struct Request : Util::Printable<Request>
    {
        std::string nsset_id;
        std::string nsset_history_id;
        bool include_domains_count;
        std::string to_string() const;
    };
    std::vector<Request> requests;
    std::string to_string() const;
};

struct NssetIdRequest : Util::Printable<NssetIdRequest>
{
    NssetHandle nsset_handle;
    std::string to_string() const;
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
