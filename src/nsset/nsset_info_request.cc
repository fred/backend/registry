/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/nsset/nsset_info_request.hh"

#include "src/util/struct_to_string.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Nsset {

std::string NssetInfoRequest::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("nsset_history_id", nsset_history_id)
                                 .add("include_domains_count", include_domains_count)
                                 .finish();
}

std::string BatchNssetInfoRequest::Request::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("nsset_history_id", nsset_history_id)
                                 .add("include_domains_count", include_domains_count)
                                 .finish();
}

std::string BatchNssetInfoRequest::to_string() const
{
    return Util::StructToString().add("requests", requests)
                                 .finish();
}

std::string NssetIdRequest::to_string() const
{
    return Util::StructToString().add("nsset_handle", nsset_handle)
                                 .finish();
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
