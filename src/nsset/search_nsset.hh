/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEARCH_NSSET_HH_B40B9CF8A3A146BE89BB7918311E908E
#define SEARCH_NSSET_HH_B40B9CF8A3A146BE89BB7918311E908E

#include "src/nsset/nsset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <set>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

enum class NssetItem
{
    nsset_handle,
    dns_hosts_fqdn,
    dns_hosts_ip_addresses
};

struct SearchNssetRequest : Util::Printable<SearchNssetRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> limit;
    std::set<NssetItem> searched_items;
    std::string to_string() const;
};

struct SearchNssetReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            NssetId nsset_id;
            std::set<NssetItem> matched_items;
            std::string to_string() const;
        };
        std::vector<Result> most_similar_nssets;
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::set<NssetItem> searched_items;
        std::string to_string() const;
    };
    Data data;
};

SearchNssetReply::Data search_nsset(const SearchNssetRequest& request);

struct SearchNssetHistoryRequest : Util::Printable<SearchNssetHistoryRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> max_number_of_nssets;
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    boost::optional<TimePoint> data_valid_from;
    boost::optional<TimePoint> data_valid_to;
    std::set<NssetItem> searched_items;
    std::string to_string() const;
};

struct SearchNssetHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            struct HistoryPeriod : Util::Printable<HistoryPeriod>
            {
                using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
                std::set<NssetItem> matched_items;
                TimePoint valid_from;
                boost::optional<TimePoint> valid_to;
                std::vector<NssetHistoryId> nsset_history_ids;
                std::string to_string() const;
            };
            NssetId object_id;
            std::vector<HistoryPeriod> histories;
            std::string to_string() const;
        };
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::vector<Result> results;
        std::set<NssetItem> searched_items;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ChronologyViolation : Registry::Exception
        {
            explicit ChronologyViolation(const SearchNssetHistoryRequest& request);
        };
    };
};

SearchNssetHistoryReply::Data search_nsset(const SearchNssetHistoryRequest& request);

}//namespace Fred::Registry::Nsset
}//namespace Fred::Registry
}//namespace Fred

#endif//SEARCH_NSSET_HH_B40B9CF8A3A146BE89BB7918311E908E
