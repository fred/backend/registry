/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NSSET_DATA_HISTORY_HH_22F7AC8AA7294E76A4E0C8B8A626AFB7
#define NSSET_DATA_HISTORY_HH_22F7AC8AA7294E76A4E0C8B8A626AFB7

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/nsset/nsset_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <map>
#include <string>
#include <vector>


namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetDataHistoryRequest : Util::Printable<NssetDataHistoryRequest>
{
    NssetDataHistoryRequest(
            const NssetId& nsset_id,
            const NssetHistoryInterval& history);
    NssetId nsset_id;
    NssetHistoryInterval history;
    std::string to_string() const;
};

struct NssetDataHistory : Util::Printable<NssetDataHistory>
{
    using TimePoint = NssetHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        NssetHistoryId nsset_history_id;
        TimePoint valid_from;
        LogEntryId log_entry_id;
        std::string to_string() const;
    };
    NssetId nsset_id;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct NssetDataHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        NssetDataHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct NssetDoesNotExist : Registry::Exception
        {
            explicit NssetDoesNotExist(const NssetDataHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const NssetDataHistoryRequest& request);
        };
    };
};

NssetDataHistoryReply::Data nsset_data_history(const NssetDataHistoryRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
