/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/nsset/nsset_info.hh"

#include "src/fred_unwrap.hh"
#include "src/util/strong_type.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/is_registered.hh"
#include "libfred/registrable_object/nsset/info_nsset.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

#include <algorithm>
#include <iterator>

namespace Fred {
namespace Registry {
namespace Nsset {

namespace {

bool does_history_continue(const LibFred::InfoNssetOutput& info)
{
    return !info.next_historyid.isnull();
}

bool does_exist(const LibFred::InfoNssetData& data)
{
    return data.delete_time.isnull();
}

NssetInfoRequest get_checked_request_data(const BatchNssetInfoRequest::Request& data)
{
    NssetInfoRequest dst;
    try
    {
        dst.nsset_id = Util::make_strong<NssetId>(boost::uuids::string_generator{}(data.nsset_id));
    }
    catch (const std::exception&)
    {
        throw NssetInfoReply::Exception::InvalidData{dst, {"nsset_id"}};
    }
    if (!data.nsset_history_id.empty())
    {
        try
        {
            dst.nsset_history_id = Util::make_strong<NssetHistoryId>(boost::uuids::string_generator{}(data.nsset_history_id));
        }
        catch (const std::exception&)
        {
            throw NssetInfoReply::Exception::InvalidData{dst, {"nsset_history_id"}};
        }
    }
    dst.include_domains_count = data.include_domains_count;
    return dst;
}

struct NssetDoesNotExist {};

std::int64_t get_number_of_connected_domains(const LibFred::OperationContext& ctx, const NssetId& nsset_id)
{
    const auto dbres = ctx.get_conn().exec_params(
            "SELECT (SELECT COUNT(DISTINCT d.id) "
                      "FROM domain d "
                     "WHERE d.nsset = obr.id) "
              "FROM object_registry obr "
             "WHERE obr.type = get_object_type_id('nsset') AND "
                   "obr.erdate IS NULL AND "
                   "obr.uuid = $1::UUID",
            Database::QueryParams{get_raw_value_from(nsset_id)}
    );
    if (dbres.size() == 1)
    {
        return static_cast<std::int64_t>(dbres[0][0]);
    }
    if (dbres.size() == 0)
    {
        throw NssetDoesNotExist{};
    }
    struct DatabaseMustBeCrazy : std::exception
    {
        const char* what() const noexcept override { return "unexpected number of nssets"; }
    };
    throw DatabaseMustBeCrazy{};
}

template <LibFred::DbLock locking>
NssetInfoReply::Data nsset_info(
        const LibFred::OperationContextUsing<locking>& ctx,
        const NssetInfoRequest& request)
{
    try
    {
        try
        {
            auto result = [&]()
            {
                if (request.nsset_history_id != boost::none)
                {
                    const auto nsset_history_uuid = LibFred::RegistrableObject::make_history_uuid_of<LibFred::Object_Type::nsset>(
                            get_raw_value_from(*request.nsset_history_id));
                    auto info = LibFred::InfoNssetByHistoryUuid{nsset_history_uuid}.exec(ctx);
                    if (request.include_domains_count)
                    {
                        if (does_history_continue(info) ||
                            !does_exist(info.info_nsset_data))
                        {
                            throw NssetInfoReply::Exception::InvalidData{request, {"include_domains_count"}};
                        }
                    }
                    return fred_unwrap(info);
                }
                const auto nsset_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::nsset>(
                        get_raw_value_from(request.nsset_id));
                return fred_unwrap(LibFred::InfoNssetByUuid(nsset_uuid).exec(ctx));
            }();
            if (request.include_domains_count)
            {
                result.domains_count = get_number_of_connected_domains(ctx, result.nsset_id);
            }
            return result;
        }
        catch (const LibFred::InfoNssetByUuid::Exception& e)
        {
            if (e.is_set_unknown_nsset_uuid())
            {
                if (LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::nsset, e.get_unknown_nsset_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered nsset");
                }
                throw NssetInfoReply::Exception::NssetDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::NssetInfoByUuid::Exception");
        }
        catch (const LibFred::InfoNssetByHistoryUuid::Exception& e)
        {
            if (e.is_set_unknown_nsset_history_uuid())
            {
                if (LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::nsset, e.get_unknown_nsset_history_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered nsset");
                }
                throw NssetInfoReply::Exception::NssetDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoNssetByHistoryUuid::Exception");
        }
        catch (const NssetDoesNotExist&)
        {
            throw NssetInfoReply::Exception::NssetDoesNotExist(request);
        }
    }
    catch (const NssetInfoReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const NssetInfoReply::Exception::NssetDoesNotExist&)
    {
        throw;
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError{"unknown exception"};
    }
}

template <LibFred::DbLock locking>
BatchNssetInfoReply::Data::BatchReply get_batch_reply(
        const LibFred::OperationContextUsing<locking>& ctx,
        const BatchNssetInfoRequest::Request& request)
{
    BatchNssetInfoReply::Data::BatchReply reply;
    try
    {
        reply.data_or_error = nsset_info(ctx, get_checked_request_data(request));
    }
    catch (NssetInfoReply::Exception::InvalidData& exception)
    {
        reply.data_or_error = BatchNssetInfoReply::Error{
                request,
                NssetInfoReply::Exception{std::move(exception)}};
    }
    catch (NssetInfoReply::Exception::NssetDoesNotExist& exception)
    {
        reply.data_or_error = BatchNssetInfoReply::Error{
                request,
                NssetInfoReply::Exception{std::move(exception)}};
    }
    return reply;
}

}//namespace Fred::Registry::Nsset::{anonymous}

}//namespace Fred::Registry::Nsset
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Nsset;

NssetInfoReply::Data Fred::Registry::Nsset::nsset_info(const NssetInfoRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx{ctx_provider};
        auto result = nsset_info(ctx, request);
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
}

BatchNssetInfoReply::Data Fred::Registry::Nsset::batch_nsset_info(const BatchNssetInfoRequest& batch_request)
{
    try
    {
        const auto& requests = batch_request.requests;
        BatchNssetInfoReply::Data result;
        if (requests.empty())
        {
            return result;
        }
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx{ctx_provider};
        std::transform(cbegin(requests), cend(requests), std::back_inserter(result.replies), [&ctx](auto&& request)
        {
            return get_batch_reply(ctx, request);
        });
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
}
  
NssetIdReply::Data Fred::Registry::Nsset::get_nsset_id(const NssetIdRequest& request)
{
    try
    {
        const auto db_res = [&request]()
        {
            LibFred::OperationContextCreator ctx;
            const auto db_res = ctx.get_conn().exec_params(
                    "SELECT obr.uuid, h.uuid "
                    "FROM object_registry obr "
                    "JOIN history h ON h.id = obr.historyid "
                    "WHERE obr.erdate IS NULL AND "
                          "obr.type = get_object_type_id('nsset') AND "
                          "UPPER(obr.name) = UPPER($1::TEXT) "
                    "FOR SHARE OF obr",
                    Database::QueryParams{get_raw_value_from(request.nsset_handle)});
            ctx.commit_transaction();
            return db_res;
        }();

        if (db_res.size() == 0)
        {
            throw NssetIdReply::Exception::NssetDoesNotExist(request);
        }
        if (1 < db_res.size())
        {
            throw Registry::InternalServerError("too many nssets selected by handle");
        }
        auto result = NssetIdReply::Data{};
        result.nsset_id = Util::make_strong<NssetId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][0])));
        result.nsset_history_id = Util::make_strong<NssetHistoryId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][1])));
        return result;
    }
    catch (const NssetIdReply::Exception::NssetDoesNotExist&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
