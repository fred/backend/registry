/*
 * Copyright (C) 2019  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NSSET_STATE_HH_AA535F231DE14CADB0CF41E5CAC051F9
#define NSSET_STATE_HH_AA535F231DE14CADB0CF41E5CAC051F9

#include "src/exceptions.hh"
#include "src/nsset/nsset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetStateRequest : Util::Printable<NssetStateRequest>
{
    NssetId nsset_id;
    std::string to_string() const;
};

struct NssetState : Util::Printable<NssetState>
{
    std::map<std::string, bool> flag_presents;
    std::string to_string() const;
};

struct NssetStateReply
{
    struct Data : Util::Printable<Data>
    {
        NssetId nsset_id;
        boost::optional<NssetHistoryId> history_id;
        NssetState state;
        std::string to_string() const;
    };
    struct Exception
    {
        struct NssetDoesNotExist : Registry::Exception
        {
            explicit NssetDoesNotExist(const NssetStateRequest& request);
        };
    };
};

NssetStateReply::Data nsset_state(const NssetStateRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
