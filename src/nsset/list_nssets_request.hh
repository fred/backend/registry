/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_NSSETS_REQUEST_HH_4423C41438E34970B47EDF9F5FADD843
#define LIST_NSSETS_REQUEST_HH_4423C41438E34970B47EDF9F5FADD843

#include "src/util/printable.hh"

#include <string>

namespace Fred {
namespace Registry {
namespace Nsset {

struct ListNssetsRequest : Util::Printable<ListNssetsRequest>
{
    bool exclude_nssets_without_domain;
    std::string to_string() const;
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
