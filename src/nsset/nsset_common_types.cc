/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/nsset/nsset_common_types.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

Exception::Exception(const std::string& msg)
    : Registry::Exception(msg)
{
}

std::string NssetLightInfo::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .add("handle", handle)
                                 .finish();
}

std::string TechnicalCheckLevel::to_string() const
{
    return Util::StructToString().add("technical_check_level", technical_check_level)
                                 .finish();
}

std::string DnsHost::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .add("dns_hosts", ip_addresses)
                                 .finish();
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
