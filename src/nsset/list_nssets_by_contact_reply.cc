/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/nsset/list_nssets_by_contact_reply.hh"
#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace Nsset {

std::string ListNssetsByContactReply::Data::Nsset::to_string() const
{
    return Util::StructToString().add("nsset", nsset)
                                 .add("nsset_history_id", nsset_history_id)
                                 .add("is_deleted", is_deleted)
                                 .finish();
}

std::string ListNssetsByContactReply::Data::to_string() const
{
    return Util::StructToString().add("nssets", nssets)
                                 .add("pagination", pagination)
                                 .finish();
}

ListNssetsByContactReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ListNssetsByContactRequest& src)
    : Registry::Exception{"contact " + src.to_string() + " does not exist"}
{ }

ListNssetsByContactReply::Exception::InvalidData::InvalidData(const ListNssetsByContactRequest& src, std::set<std::string> fields)
    : Registry::Exception{"invalid data: " + src.to_string()},
      fields{std::move(fields)}
{ }

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
