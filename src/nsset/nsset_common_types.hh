/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NSSET_COMMON_TYPES_HH_30CD8DDC9DE64649B56DCA73B1BA35C9
#define NSSET_COMMON_TYPES_HH_30CD8DDC9DE64649B56DCA73B1BA35C9

#include "src/common_types.hh"
#include "src/util/printable.hh"
#include "src/exceptions.hh"
#include "src/uuid.hh"

#include <boost/asio/ip/address.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

using NssetHandle = Util::StrongString<struct NssetHandle_Tag, Util::Skill::Printable>;

using NssetId = Uuid<struct NssetIdTag>;
using NssetHistoryId = Uuid<struct NssetHistoryIdTag>;

struct NssetLightInfo : Util::Printable<NssetLightInfo>
{
    NssetId id;
    NssetHandle handle;
    std::string to_string() const;
};

struct TechnicalCheckLevel : Util::Printable<TechnicalCheckLevel>
{
    uint32_t technical_check_level;
    std::string to_string() const;
};

struct DnsHost : Util::Printable<DnsHost>
{
    std::string fqdn;
    std::vector<boost::asio::ip::address> ip_addresses;
    std::string to_string() const;
};

}//namespace Fred::Registry::Nsset
}//namespace Fred::Registry
}//namespace Fred

#endif//NSSET_COMMON_TYPES_HH_30CD8DDC9DE64649B56DCA73B1BA35C9
