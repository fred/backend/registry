/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_NSSETS_BY_CONTACT_HH_7DF315A496F45CF7301A3F6355EFC29E//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define LIST_NSSETS_BY_CONTACT_HH_7DF315A496F45CF7301A3F6355EFC29E

#include "src/nsset/list_nssets_by_contact_reply.hh"
#include "src/nsset/list_nssets_by_contact_request.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

ListNssetsByContactReply::Data list_nssets_by_contact(const ListNssetsByContactRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif//LIST_NSSETS_BY_CONTACT_HH_7DF315A496F45CF7301A3F6355EFC29E
