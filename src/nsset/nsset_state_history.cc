/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/nsset/nsset_state_history.hh"

#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/nsset/get_nsset_state_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

NssetStateHistoryRequest::NssetStateHistoryRequest(
        const NssetId& _nsset_id,
        const NssetHistoryInterval& _history)
    : nsset_id(_nsset_id),
      history(_history)
{
}

std::string NssetStateHistoryRequest::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("history", history)
                                 .finish();
}

std::string NssetStateHistory::to_string() const
{
    return Util::StructToString().add("flags_names", flags_names)
                                 .add("timeline", timeline)
                                 .finish();
}

std::string NssetStateHistory::Record::to_string() const
{
    return Util::StructToString().add("valid_from", valid_from)
                                 .add("presents", presents)
                                 .finish();
}

std::string NssetStateHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("history", history)
                                 .finish();
}

NssetStateHistoryReply::Exception::NssetDoesNotExist::NssetDoesNotExist(const NssetStateHistoryRequest& src)
    : Registry::Exception("nsset " + src.to_string() + " does not exist")
{
}

NssetStateHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const NssetStateHistoryRequest& src)
    : Registry::Exception("invalid nsset history interval " + src.to_string())
{
}

NssetStateHistoryReply::Data nsset_state_history(const NssetStateHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetNssetStateHistoryByUuid = LibFred::RegistrableObject::Nsset::GetNssetStateHistoryByUuid;
        NssetStateHistoryReply::Data result;
        result.history = fred_unwrap(
                GetNssetStateHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::nsset>(
                                get_raw_value_from(request.nsset_id)))
                    .exec(ctx, fred_wrap(request.history)));
        result.nsset_id = request.nsset_id;
        return result;
    }
    catch (const LibFred::RegistrableObject::Nsset::GetNssetStateHistoryByUuid::DoesNotExist&)
    {
        throw NssetStateHistoryReply::Exception::NssetDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Nsset::GetNssetStateHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw NssetStateHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
