/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_NSSETS_HH_1772C4D797294CD38071972FEFE54568
#define LIST_NSSETS_HH_1772C4D797294CD38071972FEFE54568

#include "src/nsset/list_nssets_reply.hh"
#include "src/nsset/list_nssets_request.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

ListNssetsReply::Data list_nssets(const ListNssetsRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
