/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NSSET_STATE_HISTORY_HH_3D96F27D4B5C47F4BC2456DE2A2E28E3
#define NSSET_STATE_HISTORY_HH_3D96F27D4B5C47F4BC2456DE2A2E28E3

#include "src/nsset/nsset_common_types.hh"
#include "src/nsset/nsset_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <map>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetStateHistoryRequest : Util::Printable<NssetStateHistoryRequest>
{
    NssetStateHistoryRequest(
            const NssetId& _nsset_id,
            const NssetHistoryInterval& _history);
    NssetId nsset_id;
    NssetHistoryInterval history;
    std::string to_string() const;
};

struct NssetStateHistory : Util::Printable<NssetStateHistory>
{
    using TimePoint = NssetHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        TimePoint valid_from;
        std::vector<bool> presents;
        std::string to_string() const;
    };
    std::vector<std::string> flags_names;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct NssetStateHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        NssetId nsset_id;
        NssetStateHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct NssetDoesNotExist : Registry::Exception
        {
            explicit NssetDoesNotExist(const NssetStateHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const NssetStateHistoryRequest& request);
        };
    };
};

NssetStateHistoryReply::Data nsset_state_history(const NssetStateHistoryRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
