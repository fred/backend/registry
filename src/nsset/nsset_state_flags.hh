/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NSSET_STATE_FLAGS_HH_14DF688FFFFB4A79912BAF76CE74ECA7
#define NSSET_STATE_FLAGS_HH_14DF688FFFFB4A79912BAF76CE74ECA7

#include "src/util/printable.hh"

#include "libfred/registrable_object/nsset/nsset_state.hh"

#include <array>
#include <sstream>
#include <string>

namespace Fred {
namespace Registry {
namespace Nsset {

struct StateFlagInfo : Util::Printable<StateFlagInfo>
{
    enum class Manipulation
    {
        manual,
        automatic
    };
    enum class Visibility
    {
        external,
        internal
    };
    template <typename>
    static StateFlagInfo make_from();
    std::string to_string() const;
    std::string name;
    Manipulation how_to_set;
    Visibility visibility;
};

using NssetStateFlagsInfo = std::array<StateFlagInfo, LibFred::RegistrableObject::Nsset::NssetState::number_of_flags>;

template <typename T, std::size_t number_of_items>
std::string to_string(const std::array<T, number_of_items>& value)
{
    bool the_first = true;
    std::ostringstream out;
    for (const auto& item : value)
    {
        if (the_first)
        {
            the_first = false;
            out << "{ ";
        }
        else
        {
            out << ", ";
        }
        out << item;
    }
    out << " }";
    return out.str();
}

template <typename T>
std::string to_string(const std::array<T, 0>& value)
{
    return "{ }";
}

NssetStateFlagsInfo get_nsset_state_flags_info();

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
