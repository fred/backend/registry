/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NSSET_HISTORY_INTERVAL_HH_008945C45E064B06BD3133A2C23B52B7
#define NSSET_HISTORY_INTERVAL_HH_008945C45E064B06BD3133A2C23B52B7

#include "src/util/printable.hh"
#include "src/util/strong_type.hh"

#include <boost/variant.hpp>
#include <boost/uuid/uuid.hpp>

#include <chrono>
#include <string>
#include <type_traits>

namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetHistoryInterval : Util::Printable<NssetHistoryInterval>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    using HistoryId = Util::StrongType<boost::uuids::uuid, struct HistoryIdTag, Util::Skill::Printable>;
    struct NoLimit { };
    using Limit = boost::variant<TimePoint, HistoryId, NoLimit>;
    template <typename>
    struct NamedLimit
    {
        template <typename T>
        explicit NamedLimit(const T& src) : value(src)
        {
            static_assert(std::is_same<T, TimePoint>::value ||
                          std::is_same<T, HistoryId>::value ||
                          std::is_same<T, NoLimit>::value,
                          "unsupported conversion requested");
        }
        Limit value;
    };
    using LowerLimit = NamedLimit<struct Lower>;
    using UpperLimit = NamedLimit<struct Upper>;
    NssetHistoryInterval(
            const LowerLimit& _lower_limit,
            const UpperLimit& _upper_limit);
    Limit lower_limit;
    Limit upper_limit;
    std::string to_string() const;
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
