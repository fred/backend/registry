/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CHECK_DNS_HOST_HH_D54908C20099EFB99A3EB3558A70BB5F//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CHECK_DNS_HOST_HH_D54908C20099EFB99A3EB3558A70BB5F

#include "src/nsset/check_dns_host_request.hh"
#include "src/nsset/check_dns_host_reply.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

CheckDnsHostReply::Data check_dns_host(const CheckDnsHostRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif//CHECK_DNS_HOST_HH_D54908C20099EFB99A3EB3558A70BB5F
