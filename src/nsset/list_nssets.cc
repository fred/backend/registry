/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/nsset/list_nssets.hh"

#include "src/util/strong_type.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

using namespace Fred::Registry::Nsset;

#if 0
SELECT oreg.uuid,
       oreg.name
  FROM object_registry oreg
 WHERE oreg.type = get_object_type_id('nsset')
   AND oreg.erdate IS NULL
   AND EXISTS (SELECT FROM domain WHERE nsset = oreg.id)
 ORDER BY oreg.name
#endif

ListNssetsReply::Data Fred::Registry::Nsset::list_nssets(const ListNssetsRequest& request)
{
    try
    {
        ListNssetsReply::Data reply;
        const auto dbres = [&request]()
        {
            LibFred::OperationContextCreator ctx;
            std::string linked{};
            if (request.exclude_nssets_without_domain)
            {
                linked = "AND EXISTS (SELECT FROM domain WHERE nsset = oreg.id) ";
            }
            auto dbres = ctx.get_conn().exec(
                // clang-format off
                "SELECT oreg.uuid, "
                       "oreg.name "
                  "FROM object_registry oreg "
                 "WHERE oreg.type = get_object_type_id('nsset') "
                   "AND oreg.erdate IS NULL " +
                   linked +
                 "ORDER BY oreg.name");
                // clang-format on
            ctx.commit_transaction();
            return dbres;
        }();

        reply.nssets.reserve(dbres.size());
        for (std::size_t idx = 0; idx < dbres.size(); ++idx)
        {
            Nsset::NssetLightInfo nsset_light_info;
            nsset_light_info.id = Util::make_strong<NssetId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
            nsset_light_info.handle = Util::make_strong<NssetHandle>(static_cast<std::string>(dbres[idx][1]));
            reply.nssets.push_back(std::move(nsset_light_info));
        }
        return reply;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
