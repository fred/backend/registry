/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CHECK_DNS_HOST_REPLY_HH_C0B4F5B636CF9E60A384E3B8A39BE2CD//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CHECK_DNS_HOST_REPLY_HH_C0B4F5B636CF9E60A384E3B8A39BE2CD

#include "src/util/printable.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

struct CheckDnsHostReply
{
    struct Data : Util::Printable<Data>
    {
        bool dns_host_exists;
        std::string to_string() const;
    };
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif//CHECK_DNS_HOST_REPLY_HH_C0B4F5B636CF9E60A384E3B8A39BE2CD
