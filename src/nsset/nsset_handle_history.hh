/*
 * Copyright (C) 2019-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NSSET_HANDLE_HISTORY_HH_13DD24E8A2CB4525A06C3698EF24E560
#define NSSET_HANDLE_HISTORY_HH_13DD24E8A2CB4525A06C3698EF24E560

#include "src/nsset/nsset_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct NssetHandleHistoryRequest : Util::Printable<NssetHandleHistoryRequest>
{
    explicit NssetHandleHistoryRequest(const NssetHandle& nsset_handle);
    NssetHandle nsset_handle;
    std::string to_string() const;
};

struct NssetLifetime : Util::Printable<NssetLifetime>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    struct TimeSpec : Util::Printable<TimeSpec>
    {
        NssetHistoryId nsset_history_id;
        TimePoint timestamp;
        std::string to_string() const;
    };
    NssetId nsset_id;
    TimeSpec begin;
    boost::optional<TimeSpec> end;
    std::string to_string() const;
};

struct NssetHandleHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        NssetHandle nsset_handle;
        std::vector<NssetLifetime> timeline;
        std::string to_string() const;
    };
};

NssetHandleHistoryReply::Data nsset_handle_history(const NssetHandleHistoryRequest& request);


} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
