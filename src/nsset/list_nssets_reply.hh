/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_NSSETS_REPLY_HH_262A54580AEA407AAEF45D0269A7A4F3
#define LIST_NSSETS_REPLY_HH_262A54580AEA407AAEF45D0269A7A4F3

#include "src/nsset/nsset_common_types.hh"

#include "src/util/printable.hh"

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Nsset {

struct ListNssetsReply
{
    struct Data : Util::Printable<Data>
    {
        std::vector<NssetLightInfo> nssets;
        std::string to_string() const;
    };
};

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
