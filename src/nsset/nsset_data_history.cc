/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/nsset/nsset_data_history.hh"

#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"

#include "libfred/opcontext.hh"
#include "libfred/registrable_object/nsset/get_nsset_data_history.hh"
#include "libfred/registrable_object/uuid.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

NssetDataHistoryRequest::NssetDataHistoryRequest(
        const NssetId& _nsset_id,
        const NssetHistoryInterval& _history)
    : nsset_id(_nsset_id),
      history(_history)
{
}

std::string NssetDataHistoryRequest::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("history", history)
                                 .finish();
}

std::string NssetDataHistory::to_string() const
{
    return Util::StructToString().add("nsset_id", nsset_id)
                                 .add("timeline", timeline)
                                 .add("valid_to", valid_to)
                                 .finish();
}

std::string NssetDataHistory::Record::to_string() const
{
    return Util::StructToString().add("nsset_history_id", nsset_history_id)
                                 .add("valid_from", valid_from)
                                 .add("log_entry_id", log_entry_id)
                                 .finish();
}

std::string NssetDataHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("history", history).finish();
}

NssetDataHistoryReply::Exception::NssetDoesNotExist::NssetDoesNotExist(const NssetDataHistoryRequest& src)
    : Registry::Exception("nsset " + src.to_string() + " does not exist")
{
}

NssetDataHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const NssetDataHistoryRequest& src)
    : Registry::Exception("invalid nsset history interval " + src.to_string())
{
}

NssetDataHistoryReply::Data nsset_data_history(const NssetDataHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetNssetDataHistoryByUuid = LibFred::RegistrableObject::Nsset::GetNssetDataHistoryByUuid;
        return fred_unwrap(
                GetNssetDataHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::nsset>(
                                get_raw_value_from(request.nsset_id)))
                        .exec(ctx, fred_wrap(request.history)));
    }
    catch (const LibFred::RegistrableObject::Nsset::GetNssetDataHistoryByUuid::DoesNotExist&)
    {
        throw NssetDataHistoryReply::Exception::NssetDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Nsset::GetNssetDataHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw NssetDataHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
    throw NssetDataHistoryReply::Exception::NssetDoesNotExist(request);
}

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred
