/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NSSET_INFO_HH_6F1499F036DC4A56B4F2A8FB8EF2A27F
#define NSSET_INFO_HH_6F1499F036DC4A56B4F2A8FB8EF2A27F

#include "src/nsset/nsset_info_request.hh"
#include "src/nsset/nsset_info_reply.hh"

namespace Fred {
namespace Registry {
namespace Nsset {

NssetInfoReply::Data nsset_info(const NssetInfoRequest& request);
BatchNssetInfoReply::Data batch_nsset_info(const BatchNssetInfoRequest& request);

NssetIdReply::Data get_nsset_id(const NssetIdRequest& request);

} // namespace Fred::Registry::Nsset
} // namespace Fred::Registry
} // namespace Fred

#endif
