/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/pagination_reply.hh"

#include "src/util/struct_to_string.hh"


namespace Fred {
namespace Registry {

std::string PaginationReply::to_string() const
{
    return Util::StructToString().add("next_page_token", next_page_token)
                                 .add("items_left", items_left)
                                 .finish();
}

}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry;

PaginationReply Fred::Registry::last_page()
{
    PaginationReply pagination;
    pagination.next_page_token = "";
    pagination.items_left = 0;
    return pagination;
}
