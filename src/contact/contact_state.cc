/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/contact_state.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/get_contact_state.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Contact {

std::string ContactStateRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .finish();
}

std::string ContactState::to_string() const
{
    if (flag_presents.empty())
    {
        return "{ }";
    }
    std::string buffer;
    for (const auto& presents : flag_presents)
    {
        if (!buffer.empty())
        {
            buffer += ", ";
        }
        buffer += "'" + presents.first + "': " + (presents.second ? "presents" : "absents");
    }
    return "{ " + buffer + " }";
}

std::string ContactStateReply::Data::to_string() const
{
    return Util::StructToString().add("state", state).finish();
}

ContactStateReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ContactStateRequest& src)
    : Registry::Exception("contact " + src.to_string() + " does not exist")
{
}

ContactStateReply::Data contact_state(const ContactStateRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        ContactStateReply::Data result;
        const auto contact_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::contact>(
                Util::get_raw_value_from(request.contact_id));
        result.state = fred_unwrap(LibFred::RegistrableObject::Contact::GetContactStateByUuid(contact_uuid).exec(ctx));
        result.contact_id = request.contact_id;
        return result;
    }
    catch (const LibFred::RegistrableObject::Contact::GetContactStateByUuid::DoesNotExist& e)
    {
        throw ContactStateReply::Exception::ContactDoesNotExist(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
