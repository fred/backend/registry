/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PRIVACY_CONTROLLED_HH_509D53266446DEF3F9305677CCD9BF3A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PRIVACY_CONTROLLED_HH_509D53266446DEF3F9305677CCD9BF3A

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <stdexcept>
#include <type_traits>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

template <typename> class PublicData;
template <typename> class NonPublicData;
template <typename> class DefaultPublicityData;
template <typename> struct PrivacyControlledData;

template <typename T> PublicData<std::decay_t<T>> make_public_data(T&&);
template <typename T> const T& get_data_from(const PublicData<T>&);
template <typename T> NonPublicData<std::decay_t<T>> make_non_public_data(T&&);
template <typename T> const T& get_data_from(const NonPublicData<T>&);
template <typename T> DefaultPublicityData<std::decay_t<T>> make_default_publicity_data(T&&);
template <typename T> const T& get_data_from(const DefaultPublicityData<T>&);

template <typename T>
class PublicData
{
public:
    PublicData() = delete;
    PublicData(const PublicData&) = default;
    PublicData(PublicData&&) = default;
    PublicData& operator=(const PublicData&) = default;
    PublicData& operator=(PublicData&&) = default;
private:
    template <typename Src, std::enable_if_t<std::is_same<std::decay_t<Src>, T>::value>* = nullptr>
    explicit PublicData(Src&& src) : data_{std::forward<Src>(src)} { }
    T data_;
    template <typename Src>
    friend PublicData<std::decay_t<Src>> make_public_data(Src&&);
    friend const T& get_data_from<>(const PublicData&);
};

template <typename T>
PublicData<std::decay_t<T>> make_public_data(T&& src)
{
    return PublicData<std::decay_t<T>>{std::forward<T>(src)};
}

template <typename T>
class NonPublicData
{
public:
    NonPublicData() = delete;
    NonPublicData(const NonPublicData&) = default;
    NonPublicData(NonPublicData&&) = default;
    NonPublicData& operator=(const NonPublicData&) = default;
    NonPublicData& operator=(NonPublicData&&) = default;
private:
    template <typename Src, std::enable_if_t<std::is_same<std::decay_t<Src>, T>::value>* = nullptr>
    explicit NonPublicData(Src&& src) : data_{std::forward<Src>(src)} { }
    T data_;
    template <typename Src>
    friend NonPublicData<std::decay_t<Src>> make_non_public_data(Src&&);
    friend const T& get_data_from<>(const NonPublicData&);
};

template <typename T>
NonPublicData<std::decay_t<T>> make_non_public_data(T&& src)
{
    return NonPublicData<std::decay_t<T>>{std::forward<T>(src)};
}

template <typename T>
class DefaultPublicityData
{
public:
    DefaultPublicityData() = delete;
    DefaultPublicityData(const DefaultPublicityData&) = default;
    DefaultPublicityData(DefaultPublicityData&&) = default;
    DefaultPublicityData& operator=(const DefaultPublicityData&) = default;
    DefaultPublicityData& operator=(DefaultPublicityData&&) = default;
private:
    template <typename Src, std::enable_if_t<std::is_same<std::decay_t<Src>, T>::value>* = nullptr>
    explicit DefaultPublicityData(Src&& src) : data_{std::forward<Src>(src)} { }
    T data_;
    template <typename Src>
    friend DefaultPublicityData<std::decay_t<Src>> make_default_publicity_data(Src&&);
    friend const T& get_data_from<>(const DefaultPublicityData&);
};

template <typename T>
DefaultPublicityData<std::decay_t<T>> make_default_publicity_data(T&& src)
{
    return DefaultPublicityData<std::decay_t<T>>{std::forward<T>(src)};
}

template <typename T>
using PrivacyControlled = boost::variant<boost::blank, PublicData<T>, NonPublicData<T>, DefaultPublicityData<T>>;

template <typename T>
using PrivacyControlledOptional = PrivacyControlled<boost::optional<T>>;

template <typename T>
const T& get_data_from(const PublicData<T>& src)
{
    return src.data_;
}

template <typename T>
const T& get_data_from(const NonPublicData<T>& src)
{
    return src.data_;
}

template <typename T>
const T& get_data_from(const DefaultPublicityData<T>& src)
{
    return src.data_;
}

template <typename T>
T get_privacy_controlled_data(const PrivacyControlled<T>& variant)
{
    using Result = T;
    struct Visitor : boost::static_visitor<Result>
    {
        Result operator()(const PublicData<T>& data) const
        {
            return get_data_from(data);
        }
        Result operator()(const NonPublicData<T>& data) const
        {
            return get_data_from(data);
        }
        Result operator()(const DefaultPublicityData<T>& data) const
        {
            return get_data_from(data);
        }
        Result operator()(const boost::blank&) const
        {
            throw std::runtime_error("no data available");
        }
    };
    return boost::apply_visitor(Visitor(), variant);
}

template <typename T>
bool is_publicly_available(const PrivacyControlled<T>& variant, bool default_is_publish = true)
{
    using Result = bool;
    class Visitor : public boost::static_visitor<Result>
    {
    public:
        constexpr explicit Visitor(bool default_is_publish)
            : default_is_publish_{default_is_publish}
        { }
        constexpr Result operator()(const PublicData<T>&) const noexcept
        {
            return true;
        }
        constexpr Result operator()(const NonPublicData<T>&) const noexcept
        {
            return false;
        }
        constexpr Result operator()(const DefaultPublicityData<T>&) const noexcept
        {
            return default_is_publish_;
        }
        constexpr Result operator()(const boost::blank&) const
        {
            throw std::runtime_error("no data available");
        }
    private:
        bool default_is_publish_;
    };
    return boost::apply_visitor(Visitor{default_is_publish}, variant);
}

template <typename T>
bool does_present_privacy_controlled_data(const PrivacyControlledOptional<T>& variant)
{
    using Result = bool;
    struct Visitor : boost::static_visitor<Result>
    {
        Result operator()(const PublicData<boost::optional<T>>& data) const
        {
            return get_data_from(data) != boost::none;
        }
        Result operator()(const NonPublicData<boost::optional<T>>& data) const
        {
            return get_data_from(data) != boost::none;
        }
        Result operator()(const DefaultPublicityData<boost::optional<T>>& data) const
        {
            return get_data_from(data) != boost::none;
        }
        Result operator()(const boost::blank&) const
        {
            throw std::runtime_error("no data available");
        }
    };
    return boost::apply_visitor(Visitor{}, variant);
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//PRIVACY_CONTROLLED_HH_509D53266446DEF3F9305677CCD9BF3A
