/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MERGE_CONTACTS_REPLY_HH_979395AD3DA040588B36BD7D1C1D4889
#define MERGE_CONTACTS_REPLY_HH_979395AD3DA040588B36BD7D1C1D4889

#include "src/contact/merge_contacts_request.hh"

#include "src/exceptions.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace Contact {

struct MergeContactsReply
{
    struct Exception
    {
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const MergeContactsRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const MergeContactsRequest& request);
        };
    };
};

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

#endif
