/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_CONTACTS_HH_EB993D16C8944BBB8C9CC7A25A555362
#define LIST_CONTACTS_HH_EB993D16C8944BBB8C9CC7A25A555362

#include "src/contact/list_contacts_reply.hh"

namespace Fred {
namespace Registry {
namespace Contact {

ListContactsReply::Data list_contacts();

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

#endif
