/*
 * Copyright (C) 2024-2025  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/list_merge_candidates.hh"

#include "src/contact/merge_contacts_diff.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/db_settings.hh"

#include "util/db/db_exceptions.hh"
#include "util/log/log.hh"

#include <cstdint>
#include <limits>
#include <stdexcept>
#include <utility>

namespace {

void set_last_page(
        const boost::optional<Fred::Registry::PaginationRequest>& pagination_request,
        boost::optional<Fred::Registry::PaginationReply>& pagination_reply)
{
    if (pagination_request != boost::none)
    {
        pagination_reply = Fred::Registry::last_page();
    }
}

bool does_contact_exist(const LibFred::OperationContext& ctx, const Fred::Registry::Contact::ContactId& contact_id)
{
    return 0 < ctx.get_conn().exec_params(
            // clang-format off
            "SELECT "
              "FROM object_registry "
             "WHERE uuid = $1::UUID AND "
                   "type = get_object_type_id('contact') AND "
                   "erdate IS NULL",
            // clang-format on
            Database::QueryParams{contact_id}).size();
}

enum class PaginationDirection
{
    forward,
    backward
};

struct InvalidPageSize {};
struct InvalidOrderBy {};

PaginationDirection get_pagination_direction(const Fred::Registry::PaginationRequest& pagination)
{
    if (0 < pagination.page_size)
    {
        return PaginationDirection::forward;
    }
    if (pagination.page_size < 0)
    {
        return PaginationDirection::backward;
    }
    throw InvalidPageSize{};
}

std::string make_where()
{
    return "NOT(" + std::string(Fred::Registry::Contact::merge_contacts_diff()) + ") "
                "AND oreg_src.name != oreg_dst.name "
                "AND os_src.id IS NULL "
                "AND NOT EXISTS (SELECT "
                                  "FROM contact_identity ci "
                                 "WHERE ci.contact_id = oreg_src.id "
                                   "AND ci.valid_to IS NULL)";
}

std::string make_page_break(const Fred::Registry::PaginationRequest& pagination, Database::QueryParams& params)
{
    params.push_back(pagination.page_token);
    // clang-format off
    return "page_break AS MATERIALIZED "
           "(SELECT oreg_src.uuid, "
                   "oreg_src.name AS handle, "
                   "(SELECT count(foo.c) "
                      "FROM (SELECT id AS c "
                              "FROM domain "
                             "WHERE registrant = oreg_src.id "
                             "UNION "
                            "SELECT domainid AS c "
                              "FROM domain_contact_map "
                             "WHERE contactid = oreg_src.id) AS foo) AS domain_count, "
                   "(SELECT count(ncm.nssetid) "
                      "FROM nsset_contact_map ncm "
                     "WHERE ncm.contactid = oreg_src.id) AS nsset_count, "
                   "(SELECT count(kcm.keysetid) "
                      "FROM keyset_contact_map kcm "
                     "WHERE kcm.contactid = oreg_src.id) AS keyset_count, "
                   "oreg_src.crdate, "
                   "r.handle AS registrar_handle, "
                   "c_src.name, "
                   "c_src.organization "
              "FROM object_registry oreg_src "
              "JOIN contact c_src "
                "ON c_src.id = oreg_src.id "
               "AND oreg_src.erdate IS NULL "
              "JOIN object o "
                "ON o.id = oreg_src.id "
             "CROSS JOIN (object_registry oreg_dst "
                         "JOIN contact c_dst "
                           "ON c_dst.id = oreg_dst.id "
                          "AND oreg_dst.erdate IS NULL "
                          "AND oreg_dst.uuid = $" + std::to_string(params.size()) + "::UUID) "
              "LEFT JOIN object_state os_src "
                "ON os_src.object_id = c_src.id "
               "AND os_src.state_id IN (SELECT eos.id "
                                         "FROM enum_object_states eos "
                                        "WHERE eos.name IN ('mojeidContact'::TEXT, "
                                                           "'serverDeleteProhibited'::TEXT, "
                                                           "'serverBlocked'::TEXT)) " // forbidden states of src contact
               "AND os_src.valid_from <= CURRENT_TIMESTAMP "
               "AND (os_src.valid_to IS NULL OR os_src.valid_to > CURRENT_TIMESTAMP) "
            "WHERE " + make_where() + ")";
    // clang-format on
}

std::pair<std::string, std::string> to_column_ascending(Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField field)
{
    switch (field)
    {
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::crdate:
            return std::make_pair("pb.crdate", "obr.crdate");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::handle:
            return std::make_pair("pb.handle", "obr.name");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::name:
            return std::make_pair("pb.name", "tmp.name");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::organization:
            return std::make_pair("pb.organization", "tmp.organization");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::domain_count:
            return std::make_pair("pb.domain_count", "tmp.domain_count");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::nsset_count:
            return std::make_pair("pb.nsset_count", "tmp.nsset_count");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::keyset_count:
            return std::make_pair("pb.keyset_count", "tmp.keyset_count");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::sponsoring_registrar_handle:
            return std::make_pair("pb.registrar_handle", "r.handle");
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::unspecified:
            throw InvalidOrderBy{};
    }
    throw std::runtime_error{"unknown order by field"};
}

std::string make_page_break_cmp_expr(
        const std::vector<Fred::Registry::Contact::ListMergeCandidatesRequest::OrderBy>& order_by,
        PaginationDirection pagination_direction)
{
    std::string smaller_value;
    std::string greater_value;
    std::for_each(cbegin(order_by), cend(order_by), [&smaller_value, &greater_value](auto&& column)
    {
        const auto column_expr = to_column_ascending(column.field);
        if (column.direction != Fred::Registry::OrderByDirection::descending)
        {
            smaller_value.append(column_expr.first);
            greater_value.append(column_expr.second);
        }
        else
        {
            smaller_value.append(column_expr.second);
            greater_value.append(column_expr.first);
        }
        smaller_value.append(", ");
        greater_value.append(", ");
    });
    smaller_value.append("pb.uuid");
    greater_value.append("obr.uuid");
    auto expresion = "ROW(" + smaller_value + ") < ROW(" + greater_value + ")";
    switch (pagination_direction)
    {
        case PaginationDirection::forward:
            return expresion;
        case PaginationDirection::backward:
            expresion = "NOT(" + expresion + ")";
            return expresion;
    }
    throw std::runtime_error{"unknown PaginationDirection value"};
}

std::string to_order_by_column(Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField field)
{
    switch (field)
    {
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::crdate:
            return "obr.crdate";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::handle:
            return "1";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::name:
            return "tmp.name";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::organization:
            return "tmp.organization";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::domain_count:
            return "tmp.domain_count";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::nsset_count:
            return "tmp.nsset_count";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::keyset_count:
            return "tmp.keyset_count";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::sponsoring_registrar_handle:
            return "r.handle";
        case Fred::Registry::Contact::ListMergeCandidatesRequest::OrderByField::unspecified:
            throw InvalidOrderBy{};
    }
    throw std::runtime_error{"unknown order by field"};
}

std::string make_order_by_part(
        const std::vector<Fred::Registry::Contact::ListMergeCandidatesRequest::OrderBy>& order_by,
        std::string (*get_sql_direction)(Fred::Registry::OrderByDirection))
{
    std::string sql = {};
    std::for_each(cbegin(order_by), cend(order_by), [&sql, &get_sql_direction](auto&& column)
    {
        const auto column_expr = to_order_by_column(column.field);
        sql.append(column_expr + get_sql_direction(column.direction) + ", ");
    });
    sql.append("2" + get_sql_direction(Fred::Registry::OrderByDirection::ascending));
    return sql;
}

std::string normal_order_comparison(Fred::Registry::OrderByDirection direction)
{
    switch (direction)
    {
        case Fred::Registry::OrderByDirection::ascending:
            return {};
        case Fred::Registry::OrderByDirection::descending:
            return " DESC";
    }
    throw std::runtime_error{"invalid OrderByDirection value"};
}

std::string make_order_by_part(
        const std::vector<Fred::Registry::Contact::ListMergeCandidatesRequest::OrderBy>& order_by)
{
    return make_order_by_part(order_by, normal_order_comparison);
}

std::string reverse_order_comparison(Fred::Registry::OrderByDirection direction)
{
    static const auto make_oposite = [](Fred::Registry::OrderByDirection direction)
    {
        switch (direction)
        {
            case Fred::Registry::OrderByDirection::ascending:
                return Fred::Registry::OrderByDirection::descending;
            case Fred::Registry::OrderByDirection::descending:
                return Fred::Registry::OrderByDirection::ascending;
        }
        throw std::runtime_error{"invalid OrderByDirection value"};
    };
    return normal_order_comparison(make_oposite(direction));
}

std::string make_reverse_order_by_part(
        const std::vector<Fred::Registry::Contact::ListMergeCandidatesRequest::OrderBy>& order_by)
{
    return make_order_by_part(order_by, reverse_order_comparison);
}

struct ContactDoesNotExist {};

template <typename P, typename F>
void fill_in_forward(
        Fred::Registry::Contact::ListMergeCandidatesReply::Data& reply,
        const Database::Result& dbres,
        P push_back_merge_candidate,
        F get_full_count)
{
    if (dbres.size() <= 0)
    {
        return;
    }
    reply.merge_candidates.reserve(dbres.size());
    for (std::size_t idx = 0; idx < dbres.size(); ++idx)
    {
        push_back_merge_candidate(dbres[idx]);
    }
    if (reply.pagination != boost::none)
    {
        const auto items_left = get_full_count(dbres[0]) - dbres.size();
        if (0 < items_left)
        {
            reply.pagination->items_left = items_left;
            reply.pagination->next_page_token = to_string(get_raw_value_from(reply.merge_candidates.back().contact.id));
        }
    }
    FREDLOG_DEBUG("moving forward: success");
}

template <typename P, typename F>
void fill_in_backward(
        Fred::Registry::Contact::ListMergeCandidatesReply::Data& reply,
        const Database::Result& dbres,
        P push_back_merge_candidate,
        F get_full_count,
        std::size_t page_limit)
{
    if (dbres.size() <= 0)
    {
        return;
    }
    const bool last_page = dbres.size() < page_limit;
    const auto page_size = last_page ? dbres.size()
                                     : page_limit - 1;
    reply.merge_candidates.reserve(page_size);
    for (std::size_t cnt = 0; cnt < page_size; ++cnt)
    {
        push_back_merge_candidate(dbres[page_size - 1 - cnt]); // page_size - 1, page_size - 2, ..., 0
    }
    if (reply.pagination != boost::none)
    {
        const auto items_left = get_full_count(dbres[0]) - page_size;
        if (0 < items_left)
        {
            reply.pagination->items_left = items_left;
            if (page_size != (dbres.size() - 1))
            {
                LIBLOG_WARNING("page_size {} should be {}", page_size, dbres.size() - 1);
            }
            else
            {
                reply.pagination->next_page_token = static_cast<std::string>(dbres[page_size][0]);
            }
        }
    }
    FREDLOG_DEBUG("moving backward: success");
}

#if 0
SELECT contact_id,
       handle,
       domain_count,
       nsset_count,
       keyset_count,
       registrar_id,
       registrar_handle,
       registrar_name,
       registrar_organization,
       full_count
#endif
Fred::Registry::Contact::ListMergeCandidatesReply::Data exec_query(
        const std::string& sql,
        const Database::QueryParams& params,
        const Fred::Registry::Contact::ContactId& contact_id,
        const boost::optional<Fred::Registry::PaginationRequest>& pagination,
        std::size_t page_limit,
        bool reverse_order)
{
    LibFred::OperationContextCreator ctx;
    const auto dbres = ctx.get_conn().exec_params(sql, params);
    const bool result_empty = dbres.size() <= 0;
    const bool contact_exists = !result_empty || does_contact_exist(ctx, contact_id);
    ctx.commit_transaction();
    auto reply = Fred::Registry::Contact::ListMergeCandidatesReply::Data{};
    set_last_page(pagination, reply.pagination);
    if (result_empty)
    {
        if (!contact_exists)
        {
            throw ContactDoesNotExist{};
        }
        return reply;
    }
    const auto push_back_merge_candidate = [&](const Database::Row& columns)
    {
        Fred::Registry::Contact::ListMergeCandidatesReply::Data::MergeCandidate merge_candidate;
        merge_candidate.contact.id = Fred::Registry::Util::make_strong<Fred::Registry::Contact::ContactId>(
                boost::uuids::string_generator{}(static_cast<std::string>(columns[0])));
        merge_candidate.contact.handle = Fred::Registry::Util::make_strong<Fred::Registry::Contact::ContactHandle>(
                static_cast<std::string>(columns[1]));
        merge_candidate.domain_count = static_cast<std::uint64_t>(columns[2]);
        merge_candidate.nsset_count = static_cast<std::uint64_t>(columns[3]);
        merge_candidate.keyset_count = static_cast<std::uint64_t>(columns[4]);

        merge_candidate.sponsoring_registrar.id = Fred::Registry::Util::make_strong<Fred::Registry::Registrar::RegistrarId>(
                boost::uuids::string_generator{}(static_cast<std::string>(columns[5])));
        merge_candidate.sponsoring_registrar.handle = Fred::Registry::Util::make_strong<Fred::Registry::Registrar::RegistrarHandle>(
                static_cast<std::string>(columns[6]));
        merge_candidate.sponsoring_registrar.name = static_cast<std::string>(columns[7]);
        merge_candidate.sponsoring_registrar.organization = static_cast<std::string>(columns[8]);

        reply.merge_candidates.push_back(std::move(merge_candidate));
    };
    static const auto get_full_count = [](const Database::Row& columns)
    {
        return static_cast<std::uint64_t>(columns[9]);
    };
    if (!reverse_order)
    {
        fill_in_forward(reply, dbres, push_back_merge_candidate, get_full_count);
    }
    else
    {
        fill_in_backward(reply, dbres, push_back_merge_candidate, get_full_count, page_limit);
    }
    return reply;
}

#if 0
WITH dc AS NOT MATERIALIZED
(
    SELECT id
      FROM object_registry
     WHERE uuid = '465dabf1-877a-4187-b6ba-4b92a6549608'::UUID
),
page_break AS MATERIALIZED
(
SELECT oreg_src.uuid,
       oreg_src.name AS handle,
       (SELECT count(foo.c)
          FROM (SELECT id AS c
                  FROM domain
                 WHERE registrant = oreg_src.id
                 UNION
                SELECT domainid AS c
                  FROM domain_contact_map
                 WHERE contactid = oreg_src.id) AS foo) AS domain_count,
       (SELECT count(ncm.nssetid)
          FROM nsset_contact_map ncm
         WHERE ncm.contactid = oreg_src.id) AS nsset_count,
       (SELECT count(kcm.keysetid)
          FROM keyset_contact_map kcm
         WHERE kcm.contactid = oreg_src.id) AS keyset_count,
       oreg_src.crdate,
  FROM object_registry oreg_src
  JOIN contact c_src
    ON c_src.id = oreg_src.id
   AND oreg_src.erdate IS NULL
  JOIN object o
    ON o.id = oreg_src.id
 CROSS JOIN (object_registry oreg_dst
             JOIN contact c_dst
               ON c_dst.id = oreg_dst.id
              AND oreg_dst.erdate IS NULL
              AND oreg_dst.uuid = '14b0ef91-8a62-4c82-bc93-db75b671d915'::UUID)
  LEFT JOIN object_state os_src
    ON os_src.object_id = c_src.id
   AND os_src.state_id IN (SELECT eos.id
                             FROM enum_object_states eos
                            WHERE eos.name IN ('mojeidContact'::TEXT,
                                               'serverDeleteProhibited'::TEXT,
                                               'serverBlocked'::TEXT)) -- forbidden states of src contact
   AND os_src.valid_from <= CURRENT_TIMESTAMP
   AND (os_src.valid_to IS NULL OR os_src.valid_to > CURRENT_TIMESTAMP)
WHERE
        NOT (
        -- the same
           (COALESCE(LOWER(REGEXP_REPLACE(c_src.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
        -- if dst filled then src the same or empt
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.vat,'')) != ''::text)
        OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.ssn,'')) != ''::text)
        OR (COALESCE(c_src.ssntype, 0) != COALESCE(c_dst.ssntype, 0) AND COALESCE(c_src.ssntype, 0) != 0)
        )
        AND oreg_src.name != oreg_dst.name
        AND os_src.id IS NULL
        AND NOT EXISTS (SELECT
                          FROM contact_identity ci
                         WHERE ci.contact_id = oreg_src.id
                           AND ci.valid_to IS NULL)
)
SELECT obr.uuid AS contact_id,
       obr.name AS handle,
       tmp.domain_count,
       tmp.nsset_count,
       tmp.keyset_count,
       r.uuid AS registrar_id,
       r.handle AS registrar_handle,
       r.name AS registrar_name,
       r.organization AS registrar_organization
       COUNT(*) OVER() AS full_count
  FROM (SELECT oreg_src.id,
               oreg_src.name AS handle,
               c_src.name,
               c_src.organization,
               (SELECT count(foo.c)
                  FROM (SELECT id AS c
                          FROM domain
                         WHERE registrant = oreg_src.id
                         UNION
                        SELECT domainid AS c
                          FROM domain_contact_map
                         WHERE contactid = oreg_src.id) AS foo) AS domain_count,
               (SELECT count(ncm.nssetid)
                  FROM nsset_contact_map ncm
                 WHERE ncm.contactid = oreg_src.id) AS nsset_count,
               (SELECT count(kcm.keysetid)
                  FROM keyset_contact_map kcm
                 WHERE kcm.contactid = oreg_src.id) AS keyset_count,
               oreg_src.crdate
          FROM object_registry oreg_src
          JOIN contact c_src
            ON c_src.id = oreg_src.id
           AND oreg_src.erdate IS NULL
          JOIN object o
            ON o.id = oreg_src.id
    CROSS JOIN (object_registry oreg_dst
                JOIN dc
                  ON oreg_dst.id = dc.id -- here
                JOIN contact c_dst
                  ON c_dst.id = oreg_dst.id
                 AND oreg_dst.erdate IS NULL)
     LEFT JOIN object_state os_src
       ON os_src.object_id = c_src.id
      AND os_src.state_id IN (SELECT eos.id
                                FROM enum_object_states eos
                               WHERE eos.name IN ('mojeidContact'::TEXT
                                                  'serverDeleteProhibited'::TEXT
                                                  'serverBlocked'::TEXT)) -- forbidden states of src contact
      AND os_src.valid_from <= CURRENT_TIMESTAMP
      AND (os_src.valid_to IS NULL OR os_src.valid_to > CURRENT_TIMESTAMP)
    WHERE NOT (
           -- the same
              (COALESCE(LOWER(REGEXP_REPLACE(c_src.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
           -- if dst filled then src the same or empt
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.vat,'')) != ''::text)
           OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.ssn,'')) != ''::text)
           OR (COALESCE(c_src.ssntype, 0) != COALESCE(c_dst.ssntype, 0) AND COALESCE(c_src.ssntype, 0) != 0)
           )
           AND oreg_src.name != oreg_dst.name
           AND os_src.id IS NULL
           AND NOT EXISTS (SELECT
                             FROM contact_identity ci
                            WHERE ci.contact_id = oreg_src.id
                              AND ci.valid_to IS NULL)
     ) AS tmp(id, handle, name, organization, domain_count, nsset_count, keyset_count, crdate)
     JOIN object_registry obr ON obr.id = tmp.id
     JOIN object o ON o.id = obr.id
     JOIN history h ON h.id = obr.historyid
     JOIN registrar r ON r.id = o.clid,
          page_break pb
    WHERE ROW(pb.crdate, pb.uuid) < ROW(obr.crdate, obr.uuid)
    ORDER BY obr.crdate, 2
    LIMIT 10;
#endif

Fred::Registry::Contact::ListMergeCandidatesReply::Data get_merge_candidates(
        const Fred::Registry::Contact::ContactId& contact_id,
        const boost::optional<Fred::Registry::PaginationRequest>& pagination,
        const std::vector<Fred::Registry::Contact::ListMergeCandidatesRequest::OrderBy>& order_by)
{
    auto params = Database::QueryParams{contact_id};
    auto sql = std::string{
            // clang-format off
            "WITH dc AS NOT MATERIALIZED "
            "("
                "SELECT id "
                  "FROM object_registry "
                 "WHERE uuid = $1::UUID"
            ")"};
            // clang-format on
    const bool pagination_in_progress = (pagination != boost::none) &&
                                        !pagination->page_token.empty();
    if (pagination_in_progress)
    {
        sql += ", " + make_page_break(*pagination, params);
    }
    sql += " "
           // clang-format off
"SELECT obr.uuid AS contact_id, "
       "obr.name AS handle, "
       "tmp.domain_count, "
       "tmp.nsset_count, "
       "tmp.keyset_count, "
       "r.uuid AS registrar_id, "
       "r.handle AS registrar_handle, "
       "r.name AS registrar_name, "
       "r.organization AS registrar_organization, "
       "COUNT(*) OVER() AS full_count "
  "FROM (SELECT oreg_src.id, "
               "oreg_src.name AS handle, "
               "c_src.name, "
               "c_src.organization, "
               "(SELECT count(foo.c) "
                  "FROM (SELECT id AS c "
                          "FROM domain "
                         "WHERE registrant = oreg_src.id "
                         "UNION "
                        "SELECT domainid AS c "
                          "FROM domain_contact_map "
                         "WHERE contactid = oreg_src.id) AS foo) AS domain_count, "
               "(SELECT count(ncm.nssetid) "
                  "FROM nsset_contact_map ncm "
                 "WHERE ncm.contactid = oreg_src.id) AS nsset_count, "
               "(SELECT count(kcm.keysetid) "
                  "FROM keyset_contact_map kcm "
                 "WHERE kcm.contactid = oreg_src.id) AS keyset_count, "
               "oreg_src.crdate "
          "FROM object_registry oreg_src "
          "JOIN contact c_src "
            "ON c_src.id = oreg_src.id "
           "AND oreg_src.erdate IS NULL "
          "JOIN object o "
            "ON o.id = oreg_src.id "
         "CROSS JOIN (object_registry oreg_dst "
                     "JOIN dc "
                       "ON oreg_dst.id = dc.id " // here
                     "JOIN contact c_dst "
                       "ON c_dst.id = oreg_dst.id "
                      "AND oreg_dst.erdate IS NULL) "
          "LEFT JOIN object_state os_src "
            "ON os_src.object_id = c_src.id "
           "AND os_src.state_id IN (SELECT eos.id "
                                     "FROM enum_object_states eos "
                                    "WHERE eos.name IN ('mojeidContact'::TEXT, "
                                                       "'serverDeleteProhibited'::TEXT, "
                                                       "'serverBlocked'::TEXT)) " // forbidden states of src contact
           "AND os_src.valid_from <= CURRENT_TIMESTAMP "
           "AND (os_src.valid_to IS NULL OR os_src.valid_to > CURRENT_TIMESTAMP) "
         "WHERE " + make_where() + ") AS tmp(id, handle, name, organization, domain_count, nsset_count, keyset_count, crdate) "
          "JOIN object_registry obr ON obr.id = tmp.id "
          "JOIN object o ON o.id = obr.id "
          "JOIN history h ON h.id = obr.historyid "
          "JOIN registrar r ON r.id = o.clid";
            // clang-format on
    if (pagination_in_progress)
    {
        // clang-format off
        sql += ", "
                  "page_break pb "
            "WHERE " + make_page_break_cmp_expr(order_by, get_pagination_direction(*pagination));
        // clang-format on
    }
    const bool reverse_order = (pagination != boost::none) &&
                                (get_pagination_direction(*pagination) == PaginationDirection::backward);
    if (!order_by.empty())
    {
        sql += " ORDER BY " + (!reverse_order ? make_order_by_part(order_by)
                                              : make_reverse_order_by_part(order_by));
    }
    const auto page_limit = [&]() -> std::size_t
    {
        if (pagination == boost::none)
        {
            return 0;
        }
        if (!reverse_order)
        {
            return pagination->page_size;
        }
        return 1 - pagination->page_size; // |page_size| + 1
    }();
    if (pagination != boost::none)
    {
        sql += " LIMIT " + std::to_string(page_limit);
    }
    return exec_query(sql, params, contact_id, pagination, page_limit, reverse_order);
}

}//namespace {anonymous}

using namespace Fred::Registry::Contact;

ListMergeCandidatesReply::Data Fred::Registry::Contact::list_merge_candidates(const ListMergeCandidatesRequest& request)
{
    try
    {
        return get_merge_candidates(request.contact_id, request.pagination, request.order_by);
    }
    catch (const ContactDoesNotExist&)
    {
        throw ListMergeCandidatesReply::Exception::ContactDoesNotExist{request};
    }
    catch (const InvalidPageSize&)
    {
        throw ListMergeCandidatesReply::Exception::InvalidData{request, {"pagination.page_size"}};
    }
    catch (const InvalidOrderBy&)
    {
        throw ListMergeCandidatesReply::Exception::InvalidData{request, {"order_by"}};
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception caught: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError{"unknown exception caught"};
    }
    return {};
}
