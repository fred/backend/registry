/*
 * Copyright (C) 2018-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_STATE_HISTORY_HH_401231D535202981B49ABA2CCC01333A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_STATE_HISTORY_HH_401231D535202981B49ABA2CCC01333A

#include "src/contact/contact_common_types.hh"
#include "src/contact/contact_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct ContactStateHistoryRequest : Util::Printable<ContactStateHistoryRequest>
{
    ContactStateHistoryRequest(
            const ContactId& _contact_id,
            const ContactHistoryInterval& _history);
    ContactId contact_id;
    ContactHistoryInterval history;
    std::string to_string() const;
};

struct ContactStateHistory : Util::Printable<ContactStateHistory>
{
    using TimePoint = ContactHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        TimePoint valid_from;
        std::vector<bool> presents;
        std::string to_string() const;
    };
    std::vector<std::string> flags_names;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct ContactStateHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        explicit Data(ContactId contact_id, ContactStateHistory history);
        ContactId contact_id;
        ContactStateHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ContactStateHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const ContactStateHistoryRequest& request);
        };
    };
};

ContactStateHistoryReply::Data contact_state_history(const ContactStateHistoryRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_STATE_HISTORY_HH_401231D535202981B49ABA2CCC01333A
