/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/merge_contacts_reply.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Contact {

MergeContactsReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const MergeContactsRequest& src)
    : Registry::Exception{"contact " + src.to_string() + " does not exist"}
{ }

MergeContactsReply::Exception::InvalidData::InvalidData(const MergeContactsRequest& src, std::set<std::string> fields)
    : Registry::Exception{"invalid data: " + src.to_string()},
      fields{std::move(fields)}
{ }

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred
