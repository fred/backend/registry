/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef LIST_MERGE_CANDIDATES_REPLY_HH_046AD19020B34DC6A70E0956FBEBB4D8
#define LIST_MERGE_CANDIDATES_REPLY_HH_046AD19020B34DC6A70E0956FBEBB4D8

#include "src/contact/list_merge_candidates_request.hh"

#include "src/contact/contact_common_types.hh"
#include "src/exceptions.hh"
#include "src/pagination_reply.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <cstdint>
#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct ListMergeCandidatesReply
{
    struct Data : Util::Printable<Data>
    {
        struct MergeCandidate : Util::Printable<MergeCandidate>
        {
            ContactLightInfo contact;
            std::uint64_t domain_count;
            std::uint64_t nsset_count;
            std::uint64_t keyset_count;
            Registrar::RegistrarLightInfo sponsoring_registrar;
            std::string to_string() const;
        };
        std::vector<MergeCandidate> merge_candidates;
        boost::optional<PaginationReply> pagination;
        std::string to_string() const;
    };
    struct Exception
    {
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const ListMergeCandidatesRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ListMergeCandidatesRequest& request);
        };
    };
};

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

#endif
