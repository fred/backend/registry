/*
 * Copyright (C) 2018-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEARCH_CONTACT_HH_BAC06D8158562066C83CAA2C6EED3A11//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define SEARCH_CONTACT_HH_BAC06D8158562066C83CAA2C6EED3A11

#include "src/contact/contact_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <set>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

enum class ContactItem
{
    contact_handle,
    name,
    organization,
    place_street,
    place_city,
    place_state_or_province,
    place_postal_code,
    telephone,
    fax,
    email,
    notify_email,
    vat_identification_number,
    additional_identifier,
    address_company,
    address_street,
    address_city,
    address_state_or_province,
    address_postal_code,
};

struct SearchContactRequest : Util::Printable<SearchContactRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> limit;
    std::set<ContactItem> searched_items;
    std::string to_string() const;
};

struct SearchContactReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            ContactId contact_id;
            std::set<std::string> matched_items;
            std::string to_string() const;
        };
        std::vector<Result> most_similar_contacts;
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::set<ContactItem> searched_items;
        std::string to_string() const;
    };
    Data data;
};

SearchContactReply::Data search_contact(const SearchContactRequest& request);

struct SearchContactHistoryRequest : Util::Printable<SearchContactHistoryRequest>
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> max_number_of_contacts;
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    boost::optional<TimePoint> data_valid_from;
    boost::optional<TimePoint> data_valid_to;
    std::set<ContactItem> searched_items;
    std::string to_string() const;
};

struct SearchContactHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        struct Result : Util::Printable<Result>
        {
            struct HistoryPeriod : Util::Printable<HistoryPeriod>
            {
                using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
                std::set<std::string> matched_items;
                TimePoint valid_from;
                boost::optional<TimePoint> valid_to;
                std::vector<ContactHistoryId> contact_history_ids;
                std::string to_string() const;
            };
            ContactId object_id;
            std::vector<HistoryPeriod> histories;
            std::string to_string() const;
        };
        struct FuzzyValue : Util::Printable<FuzzyValue>
        {
            std::size_t lower_estimate;
            std::size_t upper_estimate;
            std::string to_string() const;
        } result_count;
        std::vector<Result> results;
        std::set<ContactItem> searched_items;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ChronologyViolation : Registry::Exception
        {
            explicit ChronologyViolation(const SearchContactHistoryRequest& request);
        };
    };
};

SearchContactHistoryReply::Data search_contact(const SearchContactHistoryRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//SEARCH_CONTACT_HH_BAC06D8158562066C83CAA2C6EED3A11
