/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/list_merge_candidates_reply.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Contact {

std::string ListMergeCandidatesReply::Data::MergeCandidate::to_string() const
{
    return Util::StructToString().add("contact", contact)
                                 .add("domain_count", domain_count)
                                 .add("nsset_count", nsset_count)
                                 .add("keyset_count", keyset_count)
                                 .add("sponsoring_registrar", sponsoring_registrar)
                                 .finish();
}

std::string ListMergeCandidatesReply::Data::to_string() const
{
    return Util::StructToString().add("merge_candidates", merge_candidates)
                                 .add("pagination", pagination)
                                 .finish();
}

ListMergeCandidatesReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ListMergeCandidatesRequest& src)
    : Registry::Exception{"contact " + src.to_string() + " does not exist"}
{ }

ListMergeCandidatesReply::Exception::InvalidData::InvalidData(const ListMergeCandidatesRequest& src, std::set<std::string> fields)
    : Registry::Exception{"invalid data: " + src.to_string()},
      fields{std::move(fields)}
{ }

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred
