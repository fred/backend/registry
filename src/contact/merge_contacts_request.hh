/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MERGE_CONTACTS_REQUEST_HH_71E019A7F2C145208AA79F5E5FA64F3A
#define MERGE_CONTACTS_REQUEST_HH_71E019A7F2C145208AA79F5E5FA64F3A

#include "src/common_types.hh"
#include "src/contact/contact_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct MergeContactsRequest : Util::Printable<MergeContactsRequest>
{
    ContactId target_contact_id;
    std::vector<ContactId> source_contact_ids;
    boost::optional<LogEntryId> log_entry_id;
    std::string to_string() const;
};

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

#endif
