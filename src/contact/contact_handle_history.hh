/*
 * Copyright (C) 2018-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONTACT_HANDLE_HISTORY_HH_9F110629D2C48F6A36379B36751DB2EF//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_HANDLE_HISTORY_HH_9F110629D2C48F6A36379B36751DB2EF

#include "src/contact/contact_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct ContactHandleHistoryRequest : Util::Printable<ContactHandleHistoryRequest>
{
    explicit ContactHandleHistoryRequest(const ContactHandle& contact_handle);
    ContactHandle contact_handle;
    std::string to_string() const;
};

struct ContactLifetime : Util::Printable<ContactLifetime>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;
    struct TimeSpec : Util::Printable<TimeSpec>
    {
        ContactHistoryId contact_history_id;
        TimePoint timestamp;
        std::string to_string() const;
    };
    ContactId contact_id;
    TimeSpec begin;
    boost::optional<TimeSpec> end;
    std::string to_string() const;
};

struct ContactHandleHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        ContactHandle contact_handle;
        std::vector<ContactLifetime> timeline;
        std::string to_string() const;
    };
};

ContactHandleHistoryReply::Data contact_handle_history(const ContactHandleHistoryRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_HANDLE_HISTORY_HH_9F110629D2C48F6A36379B36751DB2EF
