/*
 * Copyright (C) 2018-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONTACT_STATE_HH_62B479CDC5283D7418B50D4DCC6B9DCE//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_STATE_HH_62B479CDC5283D7418B50D4DCC6B9DCE

#include "src/exceptions.hh"
#include "src/contact/contact_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Contact {

struct ContactStateRequest : Util::Printable<ContactStateRequest>
{
    ContactId contact_id;
    std::string to_string() const;
};

struct ContactState : Util::Printable<ContactState>
{
    std::map<std::string, bool> flag_presents;
    std::string to_string() const;
};

struct ContactStateReply
{
    struct Data : Util::Printable<Data>
    {
        ContactId contact_id;
        boost::optional<ContactHistoryId> history_id;
        ContactState state;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ContactStateRequest& request);
        };
    };
};

ContactStateReply::Data contact_state(const ContactStateRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_STATE_HH_62B479CDC5283D7418B50D4DCC6B9DCE
