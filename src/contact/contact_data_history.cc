/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/contact_data_history.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"

#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/get_contact_data_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Contact {

ContactDataHistoryRequest::ContactDataHistoryRequest(
        const ContactId& _contact_id,
        const ContactHistoryInterval& _history)
    : contact_id(_contact_id),
      history(_history)
{ }

std::string ContactDataHistoryRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("history", history)
                                 .finish();
}

std::string ContactDataHistory::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("timeline", timeline)
                                 .add("valid_to", valid_to)
                                 .finish();
}

std::string ContactDataHistory::Record::to_string() const
{
    return Util::StructToString().add("contact_history_id", contact_history_id)
                                 .add("valid_from", valid_from)
                                 .add("log_entry_id", log_entry_id)
                                 .finish();
}

std::string ContactDataHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("history", history).finish();
}

ContactDataHistoryReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ContactDataHistoryRequest& src)
    : Registry::Exception("contact " + src.to_string() + " does not exist")
{
}

ContactDataHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const ContactDataHistoryRequest& src)
    : Registry::Exception("invalid contact history interval " + src.to_string())
{
}

ContactDataHistoryReply::Data contact_data_history(const ContactDataHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetContactDataHistoryByUuid = LibFred::RegistrableObject::Contact::GetContactDataHistoryByUuid;
        return fred_unwrap(
                GetContactDataHistoryByUuid(
                        LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::contact>(
                                get_raw_value_from(request.contact_id)))
                    .exec(ctx, fred_wrap(request.history)));
    }
    catch (const LibFred::RegistrableObject::Contact::GetContactDataHistoryByUuid::DoesNotExist&)
    {
        throw ContactDataHistoryReply::Exception::ContactDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Contact::GetContactDataHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw ContactDataHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
    throw ContactDataHistoryReply::Exception::ContactDoesNotExist(request);
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
