/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/merge_contacts.hh"

#include "src/contact/merge_contacts_reply.hh"

#include "src/cfg.hh"
#include "src/contact/merge_contacts_diff.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/strong_type.hh"
#include "src/registrar/registrar_common_types.hh"
#include "util/log/log.hh"

#include "libfred/db_settings.hh"
#include "libfred/object/object_states_info.hh"
#include "libfred/object_state/cancel_object_state_request_id.hh"
#include "libfred/object_state/create_object_state_request_id.hh"
#include "libfred/object_state/get_object_state_descriptions.hh"
#include "libfred/object_state/get_object_states.hh"
#include "libfred/object_state/object_has_state.hh"
#include "libfred/object_state/perform_object_state_request.hh"
#include "libfred/opcontext.hh"
#include "libfred/poll/create_poll_message.hh"
#include "libfred/registrable_object/contact/check_contact.hh"
#include "libfred/registrable_object/contact/merge_contact.hh"

#include <boost/algorithm/string.hpp>
#include <boost/exception/exception.hpp>

#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

namespace {

using ContactNumericId = Util::StrongType<unsigned long long, struct ContactNumericIdTag>;

struct ContactInfo
{
    ContactHandle handle;
    ContactNumericId numeric_id;
};

std::vector<ContactInfo> get_existing_contacts_by_uuid(const LibFred::OperationContext& _ctx, const std::vector<ContactId>& _contact_ids)
{
        std::vector<ContactInfo> result;
        Database::query_param_list params;
        std::string csv;
        for (const auto contact : _contact_ids)
        {
            csv += csv.empty() ? "$" : ", $";
            csv += params.add(Util::get_raw_value_from(contact));
            csv += "::UUID";
        }
        std::string sql{
            // clang-format off
                "SELECT name, "
                       "id "
                  "FROM object_registry "
                 "WHERE uuid IN (" + csv + ") "
                   "AND type = get_object_type_id('contact') "
                   "AND erdate IS NULL"};
            // clang-format on

        const auto dbres = _ctx.get_conn().exec_params(sql, params);

        result.reserve(dbres.size());
        for (Database::Result::size_type idx = 0; idx < dbres.size(); ++idx)
        {
            result.push_back(ContactInfo{
                Util::make_strong<ContactHandle>(static_cast<std::string>(dbres[idx]["name"])),
                Util::make_strong<ContactNumericId>(static_cast<unsigned long long>(dbres[idx]["id"]))});
        }
        return result;
}

boost::optional<ContactInfo> get_existing_contact_by_uuid(const LibFred::OperationContext& _ctx, const ContactId& _contact_id)
{
        const auto contacts = get_existing_contacts_by_uuid(_ctx, {_contact_id});
        if (contacts.size() > 1)
        {
            throw std::runtime_error{"unexpected number of contacts"};
        }
        if (contacts.empty())
        {
            return boost::none;
        }
        return contacts.at(0);
}

boost::optional<unsigned long long> log_entry_id_to_log_request_id(const LogEntryId& _log_entry_id)
{
    return std::stoull(Util::get_raw_value_from(_log_entry_id));
}

bool is_attached_to_identity(
        const LibFred::OperationContext& ctx,
        unsigned long long contact_id)
{
    return 0 < ctx.get_conn().exec_params(
            // clang-format off
            "SELECT "
              "FROM contact_identity "
             "WHERE contact_id = $1::BIGINT "
               "AND valid_to IS NULL "
             "LIMIT 1",
            // clang-format on
            Database::query_param_list{contact_id}).size();
}

struct MergeContactDiffContacts
{
    bool operator()(
            const LibFred::OperationContext& ctx,
            const std::string& _src_contact_handle,
            const std::string& _dst_contact_handle) const
    {
        if (boost::algorithm::to_upper_copy(_src_contact_handle).compare(
                    boost::algorithm::to_upper_copy(_dst_contact_handle)) == 0)
        {
            BOOST_THROW_EXCEPTION(
                    LibFred::MergeContact::Exception().set_identical_contacts_handle(_dst_contact_handle));
        }

        Database::Result diff_result = ctx.get_conn().exec_params(
                // clang-format off
                "SELECT " + std::string(merge_contacts_diff()) + "AS differ, "
                        "c_src.id AS src_contact_id, "
                        "c_dst.id AS dst_contact_id "
                "FROM (object_registry oreg_src "
                      "JOIN contact c_src "
                        "ON c_src.id = oreg_src.id "
                       "AND oreg_src.name = UPPER($1::text) "
                       "AND oreg_src.erdate IS NULL), "
                     "(object_registry oreg_dst "
                      "JOIN contact c_dst "
                        "ON c_dst.id = oreg_dst.id "
                       "AND oreg_dst.name = UPPER($2::text) "
                       "AND oreg_dst.erdate IS NULL)",
                // clang-format on
                Database::query_param_list(
                        _src_contact_handle)(_dst_contact_handle));
        if (diff_result.size() != 1)
        {
            BOOST_THROW_EXCEPTION(
                    LibFred::MergeContact::Exception().set_unable_to_get_difference_of_contacts(
                            LibFred::MergeContact::InvalidContacts(
                                    _src_contact_handle,
                                    _dst_contact_handle)));
        }

        const auto dst_contact_id = static_cast<unsigned long long>(diff_result[0]["dst_contact_id"]);

        if (LibFred::ObjectHasState(
                    dst_contact_id,
                    LibFred::Object_State::server_blocked).exec(ctx))
        {
            BOOST_THROW_EXCEPTION(
                    LibFred::MergeContact::Exception().set_dst_contact_invalid(_src_contact_handle));
        }

        const auto src_contact_id = static_cast<unsigned long long>(diff_result[0]["src_contact_id"]);
        const auto state_flags = LibFred::ObjectStatesInfo{LibFred::GetObjectStates{src_contact_id}.exec(ctx)};

        if (state_flags.presents(LibFred::Object_State::mojeid_contact) ||
            state_flags.presents(LibFred::Object_State::server_blocked) ||
            state_flags.presents(LibFred::Object_State::server_delete_prohibited) ||
            is_attached_to_identity(ctx, src_contact_id))
        {
            BOOST_THROW_EXCEPTION(
                    LibFred::MergeContact::Exception().set_src_contact_invalid(_src_contact_handle));
        }

        return static_cast<bool>(diff_result[0]["differ"]);
    }
};

static void log_and_rethrow_exception_handler()
{
    try
    {
        throw;
    }
    catch (const LibFred::OperationException& ex)
    {
        FREDLOG_WARNING(ex.what());
        FREDLOG_WARNING(
                boost::algorithm::replace_all_copy(
                        boost::diagnostic_information(ex),
                        "\n",
                        " "));
        throw;
    }
    catch (const LibFred::InternalError& ex)
    {
        FREDLOG_ERROR(
                boost::algorithm::replace_all_copy(
                        ex.get_exception_stack_info(),
                        "\n",
                        " "));
        FREDLOG_ERROR(
                boost::algorithm::replace_all_copy(
                        boost::diagnostic_information(ex),
                        "\n",
                        " "));
        FREDLOG_ERROR(ex.what());
        throw;
    }
    catch (const std::exception& ex)
    {
        FREDLOG_ERROR(ex.what());
        throw;
    }
    catch (...)
    {
        FREDLOG_ERROR("unknown exception");
        throw;
    }
}

} // namespace {anonymous}

struct TargetContactDoesNotExist {};
struct InvalidSourceContacts {};

void merge_contacts(
        const LibFred::OperationContext& _ctx,
        const ContactId& _target_contact_id,
        const std::vector<ContactId>& _source_contact_ids,
        const boost::optional<LogEntryId>& _log_entry_id)
{
    const auto target_contact = get_existing_contact_by_uuid(_ctx, _target_contact_id);
    if (target_contact == boost::none)
    {
        throw TargetContactDoesNotExist{};
    }

    const auto source_contacts = get_existing_contacts_by_uuid(_ctx, _source_contact_ids);
    if (_source_contact_ids.empty())
    {
        throw InvalidSourceContacts{};
    }

    try
    {
        const bool registrar_configured = Cfg::Options::get().fred.registrar_originator != boost::none;
        if (!registrar_configured)
        {
            struct NoRegistrarConfigured : std::exception
            {
                const char* what() const noexcept override { return "No registrar_originator configured"; }
            };
            throw NoRegistrarConfigured{};
        }
        const auto registrar_originator_handle = Util::make_strong<Registry::Registrar::RegistrarHandle>(*Cfg::Options::get().fred.registrar_originator);

        for (Database::Result::size_type i = 0; i < source_contacts.size(); ++i)
        {
            LibFred::MergeContactOutput merge_data;
            try
            {
                auto merge_contact_op = LibFred::MergeContact(
                        Util::get_raw_value_from(source_contacts[i].handle),
                        Util::get_raw_value_from(target_contact->handle),
                        get_raw_value_from(registrar_originator_handle),
                        MergeContactDiffContacts());
                if (_log_entry_id != boost::none)
                {
                    const auto log_request_id = log_entry_id_to_log_request_id(*_log_entry_id);
                    if (log_request_id != boost::none)
                    {
                        merge_contact_op.set_logd_request_id(*log_request_id);
                    }
                }
                merge_data = merge_contact_op.exec(_ctx);
            }
            catch (const LibFred::MergeContact::Exception& ex)
            {
                FREDLOG_WARNING(
                        boost::algorithm::replace_all_copy(
                                boost::diagnostic_information(ex),
                                "\n",
                                " "));
                throw InvalidSourceContacts{};
            }

            if ((merge_data.contactid.src_contact_id != Util::get_raw_value_from(source_contacts[i].numeric_id))
                || (merge_data.contactid.dst_contact_id != Util::get_raw_value_from(target_contact->numeric_id)))
            {
                FREDLOG_ERROR(
                        boost::format(
                                "id mismatch merge_data.contactid.src_contact_id: %1% != src_handle_result[i][\"id\"]: %2% "
                                "or merge_data.contactid.dst_contact_id: %3% != dst_contact_id: %4%")
                        % merge_data.contactid.src_contact_id
                        % Util::get_raw_value_from(source_contacts[i].numeric_id)
                        % merge_data.contactid.dst_contact_id
                        % Util::get_raw_value_from(target_contact->numeric_id));
                throw Registry::InternalServerError{"merge contacts id mismatch"};
            }

            LibFred::create_poll_messages(
                    merge_data,
                    _ctx);
        }
    }
    catch (const InvalidSourceContacts&)
    {
        FREDLOG_WARNING("invalid source contacts");
        throw;
    }
    catch (...)
    {
        log_and_rethrow_exception_handler();
    }
}

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::Contact;

void Fred::Registry::Contact::merge_contacts(const MergeContactsRequest& _request)
{
    try
    {
       LibFred::OperationContextCreator ctx;
       merge_contacts(ctx, _request.target_contact_id, _request.source_contact_ids, _request.log_entry_id);
       ctx.commit_transaction();
    }
    catch (const TargetContactDoesNotExist&)
    {
        throw MergeContactsReply::Exception::ContactDoesNotExist{_request};
    }
    catch (const InvalidSourceContacts&)
    {
        throw MergeContactsReply::Exception::InvalidData(_request, {"source_contact_ids"});
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception caught: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError{"unknown exception caught"};
    }
}
