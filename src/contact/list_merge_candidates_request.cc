/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/list_merge_candidates_request.hh"

#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Contact {

ListMergeCandidatesRequest::OrderBy::OrderBy(OrderByField field, OrderByDirection direction) noexcept
    : field{field},
      direction{direction}
{ }

std::string ListMergeCandidatesRequest::OrderBy::to_string() const
{
    return Util::StructToString().add("field", Contact::to_string(field))
                                 .add("direction", Registry::to_string(direction))
                                 .finish();
}

std::string ListMergeCandidatesRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("pagination", pagination)
                                 .add("order_by", order_by)
                                 .finish();
}

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::Contact;

std::string Fred::Registry::Contact::to_string(ListMergeCandidatesRequest::OrderByField field)
{
    switch (field)
    {
        case ListMergeCandidatesRequest::OrderByField::unspecified:
            return "unspecified";
        case ListMergeCandidatesRequest::OrderByField::crdate:
            return "crdate";
        case ListMergeCandidatesRequest::OrderByField::handle:
            return "handle";
        case ListMergeCandidatesRequest::OrderByField::name:
            return "name";
        case ListMergeCandidatesRequest::OrderByField::organization:
            return "organization";
        case ListMergeCandidatesRequest::OrderByField::domain_count:
            return "domain_count";
        case ListMergeCandidatesRequest::OrderByField::nsset_count:
            return "nsset_count";
        case ListMergeCandidatesRequest::OrderByField::keyset_count:
            return "keyset_count";
        case ListMergeCandidatesRequest::OrderByField::sponsoring_registrar_handle:
            return "sponsoring_registrar_handle";
    }
    throw std::runtime_error{"unknown ListMergeCandidatesRequest::OrderByField item"};
}
