/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/contact_info.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/contact_state.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/is_registered.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Contact {

std::string ContactInfoRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .finish();
}

std::string ContactInfoReply::Data::to_string() const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .add("place", place)
                                 .add("telephone", telephone)
                                 .add("fax", fax)
                                 .add("emails", emails)
                                 .add("notify_emails", notify_emails)
                                 .add("vat_identification_number", vat_identification_number)
                                 .add("additional_identifier", additional_identifier)
                                 .add("mailing_address", mailing_address)
                                 .add("billing_address", billing_address)
                                 .add("shipping_address1", shipping_address[0])
                                 .add("shipping_address2", shipping_address[1])
                                 .add("shipping_address3", shipping_address[2])
                                 .add("warning_letter", warning_letter)
                                 .add("representative_events", representative_events)
                                 .add("sponsoring_registrar", sponsoring_registrar)
                                 .finish();
}

ContactInfoReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ContactInfoRequest& src)
    : Registry::Exception("contact " + src.to_string() + " does not exist")
{
}

ContactInfoReply::Data contact_info(const ContactInfoRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx(ctx_provider);
        try
        {
            if (request.contact_history_id != boost::none)
            {
                const auto contact_history_uuid = LibFred::RegistrableObject::make_history_uuid_of<LibFred::Object_Type::contact>(
                        Util::get_raw_value_from(*request.contact_history_id));
                return fred_unwrap(LibFred::InfoContactByHistoryUuid(contact_history_uuid).exec(ctx));
            }
            const auto contact_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::contact>(
                    Util::get_raw_value_from(request.contact_id));
            return fred_unwrap(LibFred::InfoContactByUuid(contact_uuid).exec(ctx));
        }
        catch (const LibFred::InfoContactByUuid::Exception& e)
        {
            if (e.is_set_unknown_contact_uuid())
            {
                if (LibFred::is_registered_by_uuid(ctx, LibFred::Object_Type::contact, e.get_unknown_contact_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered contact");
                }
                throw ContactInfoReply::Exception::ContactDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoContactByUuid::Exception");
        }
        catch (const LibFred::InfoContactByHistoryUuid::Exception& e)
        {
            if (e.is_set_unknown_contact_history_uuid())
            {
                if (LibFred::is_registered_by_history_uuid(ctx, LibFred::Object_Type::contact, e.get_unknown_contact_history_uuid()))
                {
                    // Issue fred/libfred#30 workaround
                    throw Registry::DatabaseError("Database::Exception: failed to get info about registered contact");
                }
                throw ContactInfoReply::Exception::ContactDoesNotExist(request);
            }
            throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoContactByHistoryUuid::Exception");
        }
    }
    catch (const ContactInfoReply::Exception::ContactDoesNotExist&)
    {
        throw;
    }
    catch (const Registry::DatabaseError&)
    {
        throw;
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

std::string ContactIdRequest::to_string() const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .finish();
}

std::string ContactIdReply::Data::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .finish();
}

ContactIdReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ContactIdRequest& src)
    : Registry::Exception{"contact " + src.to_string() + " does not exist"}
{
}

ContactIdReply::Data get_contact_id(const ContactIdRequest& request)
{
    try
    {
        const auto db_res = [&request]()
        {
            LibFred::OperationContextCreator ctx;
            const auto db_res = ctx.get_conn().exec_params(
                    "SELECT obr.uuid, h.uuid "
                    "FROM object_registry obr "
                    "JOIN history h ON h.id = obr.historyid "
                    "WHERE obr.erdate IS NULL AND "
                          "obr.type = get_object_type_id('contact') AND "
                          "UPPER(obr.name) = UPPER($1::TEXT) "
                    "FOR SHARE OF obr",
                    Database::QueryParams{get_raw_value_from(request.contact_handle)});
            ctx.commit_transaction();
            return db_res;
        }();

        if (db_res.size() == 0)
        {
            throw ContactIdReply::Exception::ContactDoesNotExist(request);
        }
        if (1 < db_res.size())
        {
            throw Registry::InternalServerError("too many contacts selected by handle");
        }
        ContactIdReply::Data result;
        result.contact_id = Util::make_strong<ContactId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][0])));
        result.contact_history_id = Util::make_strong<ContactHistoryId>(
                boost::uuids::string_generator{}(static_cast<std::string>(db_res[0][1])));
        return result;
    }
    catch (const ContactIdReply::Exception::ContactDoesNotExist&)
    {
        throw;
    }
    catch (const Registry::InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
