/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MERGE_CONTACTS_DIFF_HH_C82B7C0A42E542568187966AE6B31E7F
#define MERGE_CONTACTS_DIFF_HH_C82B7C0A42E542568187966AE6B31E7F

namespace Fred {
namespace Registry {
namespace Contact {

constexpr const char* merge_contacts_diff()
{
    return "(COALESCE(LOWER(REGEXP_REPLACE(c_src.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '')) "
           // if dst filled then src the same or empty
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND BTRIM(COALESCE(c_src.vat,'')) != ''::text) "
           "OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND BTRIM(COALESCE(c_src.ssn,'')) != ''::text) "
           "OR (COALESCE(c_src.ssntype, 0) != COALESCE(c_dst.ssntype, 0) AND COALESCE(c_src.ssntype, 0) != 0) ";
};

} // namespace Fred::Registry::Contact
} // namespace Fred::Registry
} // namespace Fred

#endif
