/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact/create_contact.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/check_contact.hh"
#include "libfred/registrable_object/contact/create_contact.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "util/db/db_exceptions.hh"

#include <boost/lexical_cast.hpp>

#include <algorithm>
#include <iterator>
#include <regex>
#include <stdexcept>
#include <utility>

namespace Fred {
namespace Registry {
namespace Contact {

CreateContactRequest::CreateContactRequest(
        ContactHandle contact_handle,
        PrivacyControlled<std::string> name,
        PrivacyControlled<std::string> organization,
        PrivacyControlled<PlaceAddress> place,
        PrivacyControlledOptional<PhoneNumber> telephone,
        PrivacyControlledOptional<PhoneNumber> fax,
        PrivacyControlled<std::vector<EmailAddress>> emails,
        PrivacyControlled<std::vector<EmailAddress>> notify_emails,
        PrivacyControlledOptional<VatIdentificationNumber> vat_identification_number,
        PrivacyControlledOptional<ContactAdditionalIdentifier> additional_identifier,
        boost::optional<ContactAddress> mailing_address,
        boost::optional<ContactAddress> billing_address,
        std::array<boost::optional<ContactAddress>, number_of_shipping_addresses> shipping_address,
        WarningLetter warning_letter,
        boost::optional<LogEntryId> log_entry_id,
        Registrar::RegistrarHandle by_registrar)
    : contact_handle{std::move(contact_handle)},
      name{std::move(name)},
      organization{std::move(organization)},
      place{std::move(place)},
      telephone{std::move(telephone)},
      fax{std::move(fax)},
      emails{std::move(emails)},
      notify_emails{std::move(notify_emails)},
      vat_identification_number{std::move(vat_identification_number)},
      additional_identifier{std::move(additional_identifier)},
      mailing_address{std::move(mailing_address)},
      billing_address{std::move(billing_address)},
      shipping_address{std::move(shipping_address)},
      warning_letter{std::move(warning_letter)},
      log_entry_id{std::move(log_entry_id)},
      by_registrar{std::move(by_registrar)},
      unused_discloseflags{}
{ }

std::string CreateContactRequest::to_string() const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .add("place", place)
                                 .add("telephone", telephone)
                                 .add("fax", fax)
                                 .add("emails", emails)
                                 .add("notify_emails", notify_emails)
                                 .add("vat_identification_number", vat_identification_number)
                                 .add("additional_identifier", additional_identifier)
                                 .add("mailing_address", mailing_address)
                                 .add("billing_address", billing_address)
                                 .add("shipping_address1", shipping_address[0])
                                 .add("shipping_address2", shipping_address[1])
                                 .add("shipping_address3", shipping_address[2])
                                 .add("warning_letter", warning_letter)
                                 .add("log_entry_id", log_entry_id)
                                 .add("by_registrar", by_registrar)
                                 .finish();
}

std::string CreateContactReply::Data::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .finish();
}

CreateContactReply::Exception::InvalidData::InvalidData(const CreateContactRequest&, std::set<std::string> fields)
    : Registry::Exception{"invalid data"},
      fields{std::move(fields)}
{ }

CreateContactReply::Exception::RegistrarDoesNotExist::RegistrarDoesNotExist(const CreateContactRequest& src)
    : Registry::Exception{"registrar " + get_raw_value_from(src.by_registrar) + " does not exist"}
{ }

CreateContactReply::Exception::ContactAlreadyExists::ContactAlreadyExists(const CreateContactRequest&)
    : Registry::Exception{"contact already exists"}
{ }

CreateContactReply::Exception::ContactHandleInProtectionPeriod::ContactHandleInProtectionPeriod(const CreateContactRequest&)
    : Registry::Exception{"contact handle in protection period"}
{ }

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Contact;

namespace {

bool is_country_code_valid(const LibFred::OperationContext& ctx, const Fred::Registry::CountryCode& cc)
{
    return 0 < ctx.get_conn().exec_params(
                "SELECT FROM enum_country WHERE id = $1::TEXT FOR SHARE",
                Database::query_param_list(get_raw_value_from(cc))).size();
}

Optional<unsigned long long> get_log_entry_id(const CreateContactRequest& request)
{
    if (request.log_entry_id != boost::none)
    {
        return boost::lexical_cast<unsigned long long>(get_raw_value_from(*request.log_entry_id).c_str());
    }
    return {};
}

constexpr bool is_utf8_leading_byte(char byte)
{
    return (byte & 0xc0) != 0x80;
}

static_assert(is_utf8_leading_byte(" "[0]), "must be leading byte");
static_assert(is_utf8_leading_byte("a"[0]), "must be leading byte");
static_assert(is_utf8_leading_byte("á"[0]), "must be leading byte");
static_assert(!is_utf8_leading_byte("á"[1]), "cannot be leading byte");

//normalizedString: A string that does not contain line feeds, carriage returns, or tabs
template <typename S, typename L>
bool is_normalized_string(const std::string& value, S is_too_short, L is_too_long)
{
    std::size_t length = 0;
    if (is_too_long(length))
    {
        return true;
    }
    const bool is_ok = std::none_of(begin(value), end(value), [&length, &is_too_long](char c)
    {
        if (is_utf8_leading_byte(c))
        {
            ++length;
            if (is_too_long(length))
            {
                return true;
            }
            switch (c)
            {
                case '\r': // carriage return
                case '\n': // line feed
                case '\t': // horizontal tab
                case '\v': // vertical tab
                    return true;
            }
        }
        return false;
    });
    return is_ok && !is_too_short(length);
}

//<simpleType name="postalLineType">
//  <restriction base="normalizedString">
//    <minLength value="1"/>
//    <maxLength value="255"/>
//  </restriction>
//</simpleType>
bool is_postal_line_type(const std::string& value)
{
    return is_normalized_string(
            value,
            [](auto&& length) { return length < 1; },
            [](auto&& length) { return 255 < length; });
}

//<simpleType name="optPostalLineType">
//  <restriction base="normalizedString">
//    <maxLength value="255"/>
//  </restriction>
//</simpleType>
bool is_opt_postal_line_type(const std::string& value)
{
    return is_normalized_string(
            value,
            [](auto&&) { return false; },
            [](auto&& length) { return 255 < length; });
}

//token: A string that does not contain line feeds, carriage returns, tabs, leading or trailing spaces, or multiple spaces
template <typename S, typename L>
bool is_token(const std::string& value, S is_too_short, L is_too_long)
{
    if (value.empty())
    {
        return !is_too_short(0);
    }
    if ((value.front() == ' ') ||
        (value.back() == ' '))
    {
        return false;
    }
    std::size_t length = 0;
    if (is_too_long(length))
    {
        return true;
    }
    bool previous_is_space = false;
    const bool is_ok = std::none_of(
            begin(value),
            end(value),
            [&length, &is_too_long, &previous_is_space](char c)
    {
        if (is_utf8_leading_byte(c))
        {
            ++length;
            if (is_too_long(length))
            {
                return true;
            }
            switch (c)
            {
                case '\r': // carriage return
                case '\n': // line feed
                case '\t': // horizontal tab
                case '\v': // vertical tab
                    return true;
                case ' ':
                {
                    const bool two_spaces_in_row = previous_is_space;
                    previous_is_space = true;
                    return two_spaces_in_row;
                }
                default:
                    previous_is_space = false;
            }
        }
        return false;
    });
    return is_ok && !is_too_short(length);
}

//<simpleType name="pcType">
//  <restriction base="token">
//    <maxLength value="16"/>
//  </restriction>
//</simpleType>
bool is_pc_type(const Fred::Registry::PostalCode& value)
{
    return is_token(
            get_raw_value_from(value),
            [](auto&&) { return false; },
            [](auto&& length) { return 16 < length; });
}

//<simpleType name="e164StringType">
//  <restriction base="token">
//    <pattern value="(\+[0-9]{1,3}\.[0-9]{1,14})?"/>
//    <maxLength value="17"/>
//  </restriction>
//</simpleType>
bool is_e164_string_type(const Fred::Registry::PhoneNumber& phone_number)
{
    static const auto regex = std::regex{"\\+[0-9]{1,3}\\.[0-9]{1,14}"};
    const auto& value = get_raw_value_from(phone_number);
    std::smatch m;
    return is_token(
            value,
            [](auto&&) { return false; },
            [](auto&& length) { return 17 < length; }) &&
           std::regex_match(value, m, regex);
}

//<simpleType name="emailCommaListType">
//  <restriction base="token">
//    <pattern value="[^@, ]{1,64}@[^@, ]+(,[^@, ]{1,64}@[^@, ]+)*"/>
//    <maxLength value="320"/>
//  </restriction>
//</simpleType>
bool is_email_comma_list_type(const std::vector<Fred::Registry::EmailAddress>& values)
{
    static const auto regex = std::regex{"[^@, ]{1,64}@[^@, ]+"};
    std::size_t total_length = 0;
    return std::all_of(begin(values), end(values), [&total_length](auto&& value)
    {
        const std::string& email = get_raw_value_from(value);
        std::size_t email_length = 0;
        if (!is_token(
                email,
                [&email_length](auto&& length)
                {
                    email_length = length;
                    return length < 3;
                },
                [&total_length](auto&& length) { return 320 < (total_length + length); }))
        {
            return false;
        }
        if (total_length == 0)
        {
            total_length = email_length;
        }
        else
        {
            total_length += 1 + email_length;
            if (320 < total_length)
            {
                return false;
            }
        }
        std::smatch m;
        return std::regex_match(email, m, regex);
    });
}

//<simpleType name="vatT">
//  <restriction base="token">
//    <maxLength value="20"/>
//  </restriction>
//</simpleType>
bool is_vat_t(const Fred::Registry::VatIdentificationNumber& value)
{
    return is_token(
            get_raw_value_from(value),
            [](auto&&) { return false; },
            [](auto&& length) { return 20 < length; });
}

//<simpleType name="identValueT">
//  <restriction base="token">
//    <maxLength value="32"/>
//  </restriction>
//</simpleType>
struct IsIdentValueT : boost::static_visitor<bool>
{
    template <typename T>
    bool operator()(const T& value) const noexcept
    {
        return is_token(
                get_raw_value_from(value),
                [](auto&&) { return false; },
                [](auto&& length) { return 32 < length; });
    }
};
bool is_ident_value_t(const ContactAdditionalIdentifier& value) noexcept
{
    return boost::apply_visitor(IsIdentValueT{}, value);
}

std::string get_type(const ContactAdditionalIdentifier& value)
{
    struct Visitor : boost::static_visitor<std::string>
    {
        std::string operator()(const NationalIdentityNumber&) const
        {
            return "national_identity_number";
        }
        std::string operator()(const NationalIdentityCard&) const
        {
            return "national_identity_card";
        }
        std::string operator()(const PassportNumber&) const
        {
            return "passport_number";
        }
        std::string operator()(const Fred::Registry::CompanyRegistrationNumber&) const
        {
            return "company_registration_number";
        }
        std::string operator()(const SocialSecurityNumber&) const
        {
            return "social_security_number";
        }
        std::string operator()(const Birthdate&) const
        {
            return "birthdate";
        }
    };
    return boost::apply_visitor(Visitor{}, value);
}

void check_create_contact_request(const LibFred::OperationContext& ctx, const CreateContactRequest& request)
{
    const bool handle_is_valid = LibFred::Contact::get_handle_syntax_validity(
            ctx,
            get_raw_value_from(request.contact_handle)) == LibFred::ContactHandleState::SyntaxValidity::valid;
    if (!handle_is_valid)
    {
        throw CreateContactReply::Exception::InvalidData{request, {"contact_handle"}};
    }

    switch (LibFred::Contact::get_handle_registrability(ctx, get_raw_value_from(request.contact_handle)))
    {
        case LibFred::ContactHandleState::Registrability::registered:
            throw CreateContactReply::Exception::ContactAlreadyExists{request};
        case LibFred::ContactHandleState::Registrability::in_protection_period:
            throw CreateContactReply::Exception::ContactHandleInProtectionPeriod{request};
        case LibFred::ContactHandleState::Registrability::available:
            break;
    }

    auto invalid_data = CreateContactReply::Exception::InvalidData{request, {}};
    if (!is_postal_line_type(get_privacy_controlled_data(request.name)))
    {
        invalid_data.fields.insert("name");
    }
    if (!is_opt_postal_line_type(get_privacy_controlled_data(request.organization)))
    {
        invalid_data.fields.insert("organization");
    }
    const auto check_address = [&invalid_data, &ctx](const auto& address, auto make_address_name)
    {
        if (!is_country_code_valid(ctx, address.country_code))
        {
            invalid_data.fields.insert(make_address_name() + std::string{".country_code"});
        }
        if (address.street.empty() ||
            !is_postal_line_type(address.street[0]))
        {
            invalid_data.fields.insert(make_address_name() + std::string{".street[0]"});
        }
        static constexpr auto max_number_of_streets = 3;
        if (max_number_of_streets < address.street.size())
        {
            invalid_data.fields.insert(make_address_name() + std::string{".street.size()"});
        }
        for (std::size_t idx = 1; (idx < address.street.size()) && (idx < max_number_of_streets); ++idx)
        {
            if (!is_opt_postal_line_type(address.street[idx]))
            {
                invalid_data.fields.insert(make_address_name() + (".street[" + std::to_string(idx) + "]"));
            }
        }
        if (!is_postal_line_type(address.city))
        {
            invalid_data.fields.insert(make_address_name() + std::string{".city"});
        }
        if (!is_opt_postal_line_type(address.state_or_province))
        {
            invalid_data.fields.insert(make_address_name() + std::string{".state_or_province"});
        }
        if (!is_pc_type(address.postal_code))
        {
            invalid_data.fields.insert(make_address_name() + std::string{".postal_code"});
        }
    };
    check_address(get_privacy_controlled_data(request.place), []() { return "place"; });

    if (request.mailing_address != boost::none)
    {
        check_address(*request.mailing_address, []() { return "mailing_address"; });
    }
    if (request.billing_address != boost::none)
    {
        check_address(*request.billing_address, []() { return "billing_address"; });
    }
    int address_idx = 0;
    std::for_each(begin(request.shipping_address), end(request.shipping_address), [&](auto&& address)
    {
        ++address_idx;
        if (address != boost::none)
        {
            check_address(*address, [address_idx]() { return "shipping_address" + std::to_string(address_idx); });
        }
    });

    if (does_present_privacy_controlled_data(request.telephone) &&
        !is_e164_string_type(*get_privacy_controlled_data(request.telephone)))
    {
        invalid_data.fields.insert("telephone");
    }
    if (does_present_privacy_controlled_data(request.fax) &&
        !is_e164_string_type(*get_privacy_controlled_data(request.fax)))
    {
        invalid_data.fields.insert("fax");
    }
    if (get_privacy_controlled_data(request.emails).empty() ||
        !is_email_comma_list_type(get_privacy_controlled_data(request.emails)))
    {
        invalid_data.fields.insert("emails");
    }
    if (!is_email_comma_list_type(get_privacy_controlled_data(request.notify_emails)))
    {
        invalid_data.fields.insert("notify_emails");
    }
    if (does_present_privacy_controlled_data(request.vat_identification_number) &&
        !is_vat_t(*get_privacy_controlled_data(request.vat_identification_number)))
    {
        invalid_data.fields.insert("vat_identification_number");
    }
    if (does_present_privacy_controlled_data(request.additional_identifier) &&
        !is_ident_value_t(*get_privacy_controlled_data(request.additional_identifier)))
    {
        invalid_data.fields.insert("additional_identifier." + get_type(*get_privacy_controlled_data(request.additional_identifier)));
    }
    std::transform(
            begin(request.unused_discloseflags),
            end(request.unused_discloseflags),
            std::inserter(invalid_data.fields, end(invalid_data.fields)),
            [](auto&& discloseflag) { return "disclose[" + discloseflag + "]"; });

    try
    {
        get_log_entry_id(request);
    }
    catch (const boost::bad_lexical_cast&)
    {
        invalid_data.fields.insert("log_entry_id");
    }

    if (!invalid_data.fields.empty())
    {
        throw invalid_data;
    }
}

template <typename T>
Optional<T> to_optional(const T&);

template <>
Optional<std::string> to_optional<std::string>(const std::string& src)
{
    return src.empty() ? Optional<std::string>{}
                       : Optional<std::string>{src};
}

template <typename T>
Optional<T> to_optional(const boost::optional<T>& src)
{
    return src != boost::none ? Optional<T>{*src}
                              : Optional<T>{};
}

template <typename T, typename N, template <typename> class ...Skills>
Optional<T> to_optional_raw_value(const boost::optional<Fred::Registry::Util::StrongType<T, N, Skills...>>& src)
{
    return src != boost::none ? Optional<T>{get_raw_value_from(*src)}
                              : Optional<T>{};
}

Optional<std::string> to_optional(const std::vector<std::string>& src, std::size_t idx)
{
    return idx < src.size() ? to_optional(src[idx])
                            : Optional<std::string>{};
}

template <typename N, template <typename> class ...Skills>
Optional<std::string> to_optional_emails(const std::vector<Fred::Registry::Util::StrongString<N, Skills...>>& src)
{
    if (src.empty())
    {
        return {};
    }
    std::string result;
    std::for_each(begin(src), end(src), [&](auto&& email)
    {
        const auto& email_str = get_raw_value_from(email);
        if (email_str.empty())
        {
            return;
        }
        if (result.empty())
        {
            result = email_str;
        }
        else
        {
            result.append("," + email_str);
        }
    });
    if (result.empty())
    {
        return {};
    }
    return result;
}

struct GetPersonalIdUnion : boost::static_visitor<LibFred::PersonalIdUnion>
{
    LibFred::PersonalIdUnion operator()(const NationalIdentityNumber& value) const
    {
        return LibFred::PersonalIdUnion::get_RC(get_raw_value_from(value));
    }
    LibFred::PersonalIdUnion operator()(const NationalIdentityCard& value) const
    {
        return LibFred::PersonalIdUnion::get_OP(get_raw_value_from(value));
    }
    LibFred::PersonalIdUnion operator()(const PassportNumber& value) const
    {
        return LibFred::PersonalIdUnion::get_PASS(get_raw_value_from(value));
    }
    LibFred::PersonalIdUnion operator()(const Fred::Registry::CompanyRegistrationNumber& value) const
    {
        return LibFred::PersonalIdUnion::get_ICO(get_raw_value_from(value));
    }
    LibFred::PersonalIdUnion operator()(const SocialSecurityNumber& value) const
    {
        return LibFred::PersonalIdUnion::get_MPSV(get_raw_value_from(value));
    }
    LibFred::PersonalIdUnion operator()(const Birthdate& value) const
    {
        return LibFred::PersonalIdUnion::get_BIRTHDAY(get_raw_value_from(value));
    }
};

boost::optional<LibFred::PersonalIdUnion> get_optional_personal_id_union(
        const boost::optional<ContactAdditionalIdentifier>& identifier)
{
    if (identifier == boost::none)
    {
        return boost::none;
    }
    return boost::apply_visitor(GetPersonalIdUnion{}, *identifier);
}

constexpr auto contact_address_types = std::array<LibFred::ContactAddressType::Value, 3>{
        LibFred::ContactAddressType::SHIPPING,
        LibFred::ContactAddressType::SHIPPING_2,
        LibFred::ContactAddressType::SHIPPING_3};

constexpr LibFred::ContactAddressType::Value get_shipping_address_type(unsigned idx)
{
    return contact_address_types[idx];
}

static_assert(get_shipping_address_type(0) == LibFred::ContactAddressType::SHIPPING, "must be SHIPPING");
static_assert(get_shipping_address_type(1) == LibFred::ContactAddressType::SHIPPING_2, "must be SHIPPING_2");
static_assert(get_shipping_address_type(2) == LibFred::ContactAddressType::SHIPPING_3, "must be SHIPPING_3");

LibFred::ContactAddress make_contact_address(const ContactAddress& src)
{
    LibFred::ContactAddress dst;
    dst.street1 = src.street[0];
    dst.street2 = to_optional(src.street, 1);
    dst.street3 = to_optional(src.street, 2);
    dst.city = src.city;
    dst.stateorprovince = to_optional(src.state_or_province);
    dst.postalcode = get_raw_value_from(src.postal_code);
    dst.country = get_raw_value_from(src.country_code);
    return dst;
}

Optional<bool> to_optional(WarningLetter::SendingPreference preference)
{
    switch (preference)
    {
        case WarningLetter::SendingPreference::not_to_send:
            return false;
        case WarningLetter::SendingPreference::to_send:
            return true;
        case WarningLetter::SendingPreference::not_specified:
            return {};
    }
    throw std::runtime_error{"invalid warning letter preference value"};
}

}//namespace {anonymous}

CreateContactReply::Data Fred::Registry::Contact::create_contact(const CreateContactRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        check_create_contact_request(ctx, request);
        LibFred::Contact::PlaceAddress place;
        place.street1 = get_privacy_controlled_data(request.place).street[0];
        place.street2 = to_optional(get_privacy_controlled_data(request.place).street, 1);
        place.street3 = to_optional(get_privacy_controlled_data(request.place).street, 2);
        place.city = get_privacy_controlled_data(request.place).city;
        place.stateorprovince = get_privacy_controlled_data(request.place).state_or_province;
        place.postalcode = get_raw_value_from(get_privacy_controlled_data(request.place).postal_code);
        place.country = get_raw_value_from(get_privacy_controlled_data(request.place).country_code);

        const auto ident = get_optional_personal_id_union(get_privacy_controlled_data(request.additional_identifier));
        LibFred::ContactAddressList addresses;
        if (request.mailing_address != boost::none)
        {
            addresses.insert(std::make_pair(LibFred::ContactAddressType::MAILING, make_contact_address(*request.mailing_address)));
        }
        if (request.billing_address != boost::none)
        {
            addresses.insert(std::make_pair(LibFred::ContactAddressType::BILLING, make_contact_address(*request.billing_address)));
        }
        unsigned address_idx = 0;
        std::for_each(begin(request.shipping_address), end(request.shipping_address), [&](auto&& src_address)
        {
            if (src_address != boost::none)
            {
                addresses.insert(std::make_pair(get_shipping_address_type(address_idx), make_contact_address(*src_address)));
            }
            ++address_idx;
        });
        static constexpr bool default_is_to_disclose = true;
        static constexpr bool default_is_to_hide = !default_is_to_disclose;
        const auto create_contact_op = LibFred::CreateContact{
            get_raw_value_from(request.contact_handle),
            get_raw_value_from(request.by_registrar),
            {},//authinfopw
            to_optional(get_privacy_controlled_data(request.name)),
            to_optional(get_privacy_controlled_data(request.organization)),
            place,
            to_optional_raw_value(get_privacy_controlled_data(request.telephone)),
            to_optional_raw_value(get_privacy_controlled_data(request.fax)),
            to_optional_emails(get_privacy_controlled_data(request.emails)),
            to_optional_emails(get_privacy_controlled_data(request.notify_emails)),
            to_optional_raw_value(get_privacy_controlled_data(request.vat_identification_number)),
            ident != boost::none ? ident->get_type() : Optional<std::string>(),
            ident != boost::none ? ident->get() : Optional<std::string>(),
            addresses.empty() ? Optional<LibFred::ContactAddressList>{}
                              : Optional<LibFred::ContactAddressList>{addresses},
            is_publicly_available(request.name, default_is_to_disclose),
            is_publicly_available(request.organization, default_is_to_disclose),
            is_publicly_available(request.place, default_is_to_disclose),
            is_publicly_available(request.telephone, default_is_to_hide),
            is_publicly_available(request.fax, default_is_to_hide),
            is_publicly_available(request.emails, default_is_to_hide),
            is_publicly_available(request.vat_identification_number, default_is_to_hide),
            is_publicly_available(request.additional_identifier, default_is_to_hide),
            is_publicly_available(request.notify_emails, default_is_to_hide),
            to_optional(request.warning_letter.preference),
            get_log_entry_id(request)};
        const auto create_data = create_contact_op.exec(ctx, "UTC");
        const auto contact_info = LibFred::InfoContactById{create_data.create_object_result.object_id}
                .exec(ctx).info_contact_data;

        CreateContactReply::Data result;
        result.contact_id = Fred::Registry::Util::make_strong<ContactId>(get_raw_value_from(contact_info.uuid));
        ctx.commit_transaction();
        return result;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError{std::string{"Database::Exception: "} + e.what()};
    }
    catch (const LibFred::CreateContact::Exception& e)
    {
        if (e.is_set_forbidden_company_name_setting())
        {
            throw CreateContactReply::Exception::InvalidData{request, {"some_address.company"}};
        }
        if (e.is_set_invalid_contact_handle())
        {
            throw CreateContactReply::Exception::InvalidData{request, {"contact_handle"}};
        }
        if (e.is_set_unknown_country())
        {
            throw CreateContactReply::Exception::InvalidData{request, {"some_address.country"}};
        }
        if (e.is_set_unknown_registrar_handle())
        {
            throw CreateContactReply::Exception::RegistrarDoesNotExist{request};
        }
        if (e.is_set_unknown_ssntype())
        {
            throw CreateContactReply::Exception::InvalidData{request, {"additional_identifier"}};
        }
        throw Registry::InternalServerError{std::string{"LibFred::CreateContact::Exception: "} + e.what()};
    }
    catch (const CreateContactReply::Exception::ContactAlreadyExists&)
    {
        throw;
    }
    catch (const CreateContactReply::Exception::ContactHandleInProtectionPeriod&)
    {
        throw;
    }
    catch (const CreateContactReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const CreateContactReply::Exception::RegistrarDoesNotExist&)
    {
        throw;
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError{std::string{"std::exception: "} + e.what()};
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
