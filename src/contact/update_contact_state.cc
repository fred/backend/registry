/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact/update_contact_state.hh"
#include "src/contact/contact_state_flags.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/object_state/cancel_object_state_request_id.hh"
#include "libfred/object_state/create_object_state_request_id.hh"
#include "libfred/object_state/perform_object_state_request.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "util/db/db_exceptions.hh"

#include <algorithm>

namespace Fred {
namespace Registry {
namespace Contact {

std::string UpdateContactStateRequest::to_string()const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .add("set_flags", set_flags)
                                 .add("log_entry_id", log_entry_id)
                                 .add("originator", originator)
                                 .finish();
}

std::string UpdateContactStateReply::Data::to_string()const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .finish();
}

UpdateContactStateReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const UpdateContactStateRequest& src)
    : Registry::Exception("contact " + Util::Into<std::string>::from(src.contact_id) + " does not exist")
{
}

UpdateContactStateReply::Exception::NotCurrentVersion::NotCurrentVersion(const UpdateContactStateRequest& src)
    : Registry::Exception(Util::Into<std::string>::from(src.contact_history_id) + " of contact " + Util::Into<std::string>::from(src.contact_id) + " is not current version")
{
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry::Contact;

namespace {

void existing_manual_state_flags_required(const std::map<std::string, bool>& set_flags)
{
    const auto flags_manipulation = []()
    {
        const auto flags_info = get_contact_state_flags_info();
        std::map<std::string, StateFlagInfo::Manipulation> result;
        for (const auto& info : flags_info)
        {
            result[info.name] = info.how_to_set;
        }
        return result;
    }();
    std::for_each(begin(set_flags), end(set_flags), [&flags_manipulation](auto&& name_action_pair)
    {
        const auto& flag_name = name_action_pair.first;
        const auto flag_info_iter = flags_manipulation.find(flag_name);
        const bool flag_exists = flag_info_iter != flags_manipulation.end();
        if (!flag_exists)
        {
            throw Fred::Registry::InternalServerError{"an unknown flag \"" + flag_name + "\""};
        }
        if (flag_info_iter->second != StateFlagInfo::Manipulation::manual)
        {
            throw Fred::Registry::InternalServerError{"flag \"" + flag_name + "\" cannot be manually manipulated"};
        }
    });
}

}//namespace {anonymous}

UpdateContactStateReply::Data Fred::Registry::Contact::update_contact_state(
        const UpdateContactStateRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForUpdate ctx(ctx_provider);

        const auto contact_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::contact>(
                get_raw_value_from(request.contact_id));
        const auto info_contact_output = LibFred::InfoContactByUuid{contact_uuid}.exec(ctx);
        const bool contact_is_registered = info_contact_output.info_contact_data.delete_time.isnull();
        if (!contact_is_registered)
        {
            throw UpdateContactStateReply::Exception::ContactDoesNotExist{request};
        }
        const auto contact_history_uuid = LibFred::RegistrableObject::make_history_uuid_of<LibFred::Object_Type::contact>(
                get_raw_value_from(request.contact_history_id));
        const bool is_current_contact_data =
                info_contact_output.next_historyid.isnull() &&
                (get_raw_value_from(info_contact_output.info_contact_data.history_uuid) == get_raw_value_from(contact_history_uuid));
        if (!is_current_contact_data)
        {
            throw UpdateContactStateReply::Exception::NotCurrentVersion{request};
        }
        const auto originator = request.originator == boost::none ? std::string{}
                                                                  : get_raw_value_from(*request.originator);

        existing_manual_state_flags_required(request.set_flags);
        bool request_changed = false;
        std::for_each(begin(request.set_flags), end(request.set_flags), [&](auto&& name_action_pair)
        {
            const auto flags = LibFred::StatusList{name_action_pair.first};
            const auto to_set = name_action_pair.second;
            try
            {
                LibFred::CancelObjectStateRequestId{info_contact_output.info_contact_data.id, flags}.exec(ctx);
                request_changed = true;
            }
            catch (const LibFred::CancelObjectStateRequestId::Exception& e)
            {
                if (e.is_set_object_id_not_found())
                {
                    throw UpdateContactStateReply::Exception::ContactDoesNotExist{request};
                }
                const bool allowed_problem_occured = e.is_set_state_not_found();
                if (!allowed_problem_occured)
                {
                    throw Fred::Registry::InternalServerError{"LibFred::CancelObjectStateRequestId::Exception caught"};
                }
            }
            if (to_set)
            {
                LibFred::CreateObjectStateRequestId{info_contact_output.info_contact_data.id, flags}.exec(ctx);
                request_changed = true;
            }
        });
        if (request_changed)
        {
            LibFred::PerformObjectStateRequest{info_contact_output.info_contact_data.id}.exec(ctx);
        }
        UpdateContactStateReply::Data result;
        result.contact_id = request.contact_id;
        result.contact_handle = Util::make_strong<ContactHandle>(info_contact_output.info_contact_data.handle);
        result.contact_history_id = request.contact_history_id;
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const UpdateContactStateReply::Exception::ContactDoesNotExist&)
    {
        throw;
    }
    catch (const UpdateContactStateReply::Exception::NotCurrentVersion&)
    {
        throw;
    }
    catch (const UpdateContactStateReply::Exception::UpdateProhibited&)
    {
        throw;
    }
    catch (const UpdateContactStateReply::Exception::UnauthorizedRegistrar&)
    {
        throw;
    }
    catch (const Fred::Registry::InternalServerError&)
    {
        throw;
    }
    catch (const LibFred::InfoContactByUuid::Exception& e)
    {
        if (e.is_set_unknown_contact_uuid())
        {
            throw UpdateContactStateReply::Exception::ContactDoesNotExist(request);
        }
        throw Fred::Registry::InternalServerError("unexpected problem signalized by LibFred::InfoContactByUuid::Exception");
    }
    catch (const LibFred::CreateObjectStateRequestId::Exception& e)
    {
        throw Fred::Registry::InternalServerError("unexpected problem signalized by LibFred::CreateObjectStateRequestId::Exception");
    }
    catch (const Database::Exception& e)
    {
        throw Fred::Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Fred::Registry::InternalServerError(e.what());
    }
    catch (...)
    {
        throw Fred::Registry::InternalServerError("unexpected unknown exception");
    }
}
