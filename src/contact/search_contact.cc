/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact/search_contact.hh"

#include "src/util/enum_sequence.hh"
#include "src/util/into.hh"
#include "src/util/measure_duration.hh"
#include "src/util/sql_exec_in_thread.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "util/db/db_exceptions.hh"
#include "util/log/log.hh"

#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <cmath>

#include <algorithm>
#include <iterator>
#include <memory>
#include <regex>
#include <set>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

std::string SearchContactRequest::to_string() const
{
    return Util::StructToString().add("query_values", query_values)
                                 .add("limit", limit)
                                 .add("searched_items", searched_items)
                                 .finish();
}

std::string SearchContactReply::Data::to_string() const
{
    return Util::StructToString().add("most_similar_contacts", most_similar_contacts)
                                 .add("result_count", result_count)
                                 .add("searched_items", searched_items)
                                 .finish();
}

std::string SearchContactReply::Data::Result::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("matched_items", matched_items)
                                 .finish();
}

std::string SearchContactReply::Data::FuzzyValue::to_string() const
{
    return Util::StructToString().add("lower_estimate", lower_estimate)
                                 .add("upper_estimate", upper_estimate)
                                 .finish();
}

std::string SearchContactHistoryRequest::to_string() const
{
    return Util::StructToString().add("query_values", query_values)
                                 .add("max_number_of_contacts", max_number_of_contacts)
                                 .add("data_valid_from", data_valid_from)
                                 .add("data_valid_to", data_valid_to)
                                 .add("searched_items", searched_items)
                                 .finish();
}

std::string SearchContactHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("result_count", result_count)
                                 .add("results", results)
                                 .add("searched_items", searched_items)
                                 .finish();
}

std::string SearchContactHistoryReply::Data::FuzzyValue::to_string() const
{
    return Util::StructToString().add("lower_estimate", lower_estimate)
                                 .add("upper_estimate", upper_estimate)
                                 .finish();
}

std::string SearchContactHistoryReply::Data::Result::to_string() const
{
    return Util::StructToString().add("object_id", object_id)
                                 .add("histories", histories)
                                 .finish();
}

std::string SearchContactHistoryReply::Data::Result::HistoryPeriod::to_string() const
{
    return Util::StructToString().add("matched_items", matched_items)
                                 .add("valid_from", valid_from)
                                 .add("valid_to", valid_to)
                                 .add("contact_history_ids", contact_history_ids)
                                 .finish();
}

namespace {

std::vector<std::string> split(const std::string& src, const std::regex& delimiter)
{
    constexpr auto stuff_between_matches = -1;
    std::sregex_token_iterator begin(src.cbegin(), src.cend(), delimiter, stuff_between_matches);
    static const std::sregex_token_iterator end;
    std::vector<std::string> result;
    std::copy_if(begin, end, std::back_inserter(result), [](const std::string& item) { return !item.empty(); });
    return result;
}

std::vector<std::string> to_words(const std::string& by_spaces_delimited_words)
{
    return split(by_spaces_delimited_words, std::regex("\\s+"));
}

struct Delimiter
{
    template <typename T>
    explicit Delimiter(T&& src)
        : value{std::forward<T>(src)} { }
    std::string value;
};

class Collector
{
public:
    explicit Collector(Delimiter&& delimiter)
        : delimiter_{std::move(delimiter.value)}
    { }
    template <typename T>
    Collector& append(T&& item)
    {
        if (!sum_.empty())
        {
            sum_.append(delimiter_);
        }
        sum_.append(std::forward<T>(item));
        return *this;
    }
    const std::string& get_sum() const noexcept
    {
        return sum_;
    }
private:
    const std::string delimiter_;
    std::string sum_;
};

class CollectorWithParentheses
{
public:
    explicit CollectorWithParentheses(Delimiter&& delimiter)
        : delimiter_{std::move(delimiter.value)},
          delimiter_in_use_{false}
    { }
    template <typename T>
    CollectorWithParentheses& append(T&& item)
    {
        if (!sum_.empty())
        {
            if (!delimiter_in_use_)
            {
                sum_ = "(" + sum_ + ")";
                delimiter_in_use_ = true;
            }
            sum_.append(delimiter_ + "(" + std::forward<T>(item) + ")");
        }
        else
        {
            sum_ = std::forward<T>(item);
        }
        return *this;
    }
    const std::string& get_sum() const noexcept
    {
        return sum_;
    }
private:
    const std::string delimiter_;
    bool delimiter_in_use_;
    std::string sum_;
};

template <ContactItem value>
struct ContactItemTraits;

template <>
struct ContactItemTraits<ContactItem::contact_handle>
{
    static constexpr const char* item_name()
    {
        return "contact_handle";
    }
};

template <>
struct ContactItemTraits<ContactItem::name>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(name)";
    }
    static constexpr const char* item_name()
    {
        return "name";
    }
};

template <>
struct ContactItemTraits<ContactItem::organization>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(organization)";
    }
    static constexpr const char* item_name()
    {
        return "organization";
    }
};

template <>
struct ContactItemTraits<ContactItem::place_street>
{
    static constexpr const char* transformed_column_value()
    {
        return "unaccent_streets(street1,street2,street3)";
    }
    static constexpr const char* item_name()
    {
        return "place.street";
    }
};

template <>
struct ContactItemTraits<ContactItem::place_city>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(city)";
    }
    static constexpr const char* item_name()
    {
        return "place.city";
    }
};

template <>
struct ContactItemTraits<ContactItem::place_state_or_province>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(stateorprovince)";
    }
    static constexpr const char* item_name()
    {
        return "place.state_or_province";
    }
};

template <>
struct ContactItemTraits<ContactItem::place_postal_code>
{
    static constexpr const char* transformed_column_value()
    {
        return "postalcode";
    }
    static constexpr const char* item_name()
    {
        return "place.postal_code";
    }
};

template <>
struct ContactItemTraits<ContactItem::telephone>
{
    static constexpr const char* transformed_column_value()
    {
        return "telephone";
    }
    static constexpr const char* item_name()
    {
        return "telephone";
    }
};

template <>
struct ContactItemTraits<ContactItem::fax>
{
    static constexpr const char* transformed_column_value()
    {
        return "fax";
    }
    static constexpr const char* item_name()
    {
        return "fax";
    }
};

template <>
struct ContactItemTraits<ContactItem::email>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(email)";
    }
    static constexpr const char* item_name()
    {
        return "email";
    }
};

template <>
struct ContactItemTraits<ContactItem::notify_email>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(notifyemail)";
    }
    static constexpr const char* item_name()
    {
        return "notify_email";
    }
};

template <>
struct ContactItemTraits<ContactItem::vat_identification_number>
{
    static constexpr const char* transformed_column_value()
    {
        return "vat";
    }
    static constexpr const char* item_name()
    {
        return "vat_identification_number";
    }
};

using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;

struct HistoryLimit
{
    boost::optional<TimePoint> data_valid_from;
    boost::optional<TimePoint> data_valid_to;
};

struct ChronologyViolationError : std::exception
{
    const char* what() const noexcept override
    {
        return "chronology violation";
    }
};

HistoryLimit make_history_limit(const boost::optional<TimePoint>& from, const boost::optional<TimePoint>& to)
{
    if ((from != boost::none) && (to != boost::none))
    {
        if (*to < *from)
        {
            throw ChronologyViolationError();
        }
    }
    return HistoryLimit{from, to};
}

struct Record
{
    struct ObjectId
    {
        unsigned long long numeric_id;
        ContactId contact_id;
    };
    struct Period
    {
        ContactHistoryId history_id;
        boost::optional<ContactHistoryId> next_history_id;
        TimePoint valid_from;
        boost::optional<TimePoint> valid_to;
    };
    struct Data
    {
        std::size_t value_length;
        struct WordSimilarity
        {
            double partial; //result of word_similarity(word, value)
            double complete;//result of similarity(word, value)
        };
        struct ExpressionSimilarity//one expression consists of one or more words
        {
            std::vector<WordSimilarity> words_similarities;
        };
        std::vector<ExpressionSimilarity> similarities;
    };
    ObjectId object;
    Period period;
    std::map<std::string, Data> item_data;
};

constexpr bool is_partially_similar(const Record::Data::WordSimilarity& word_similarity)
{
    constexpr double similarity_threshold = 0.299999;
    return similarity_threshold < word_similarity.partial;
}

bool is_match(const std::vector<Record::Data::WordSimilarity>& words_similarities)
{
    return std::all_of(begin(words_similarities), end(words_similarities), is_partially_similar);
}

struct ExpressionMatch
{
    std::string item_name;
    std::size_t item_value_length;
    std::vector<Record::Data::WordSimilarity> words_similarities;
    bool is_match() const
    {
        return Contact::is_match(words_similarities);
    }
    auto sum_similarity() const
    {
        Record::Data::WordSimilarity sum{ 0.0, 0.0 };
        for (const auto& similarity : words_similarities)
        {
            sum.partial += similarity.partial * similarity.partial;
            sum.complete += similarity.complete * similarity.complete;
        }
        sum.partial = std::sqrt(sum.partial / static_cast<double>(words_similarities.size()));
        sum.complete = std::sqrt(sum.complete / static_cast<double>(words_similarities.size()));
        return sum;
    }
    friend bool operator<(const ExpressionMatch& lhs, const ExpressionMatch& rhs)
    {
        if (!lhs.is_match())
        {
            return rhs.is_match();
        }
        if (!rhs.is_match())
        {
            return false;
        }
        const auto lhs_sum_similarity = lhs.sum_similarity();
        const auto rhs_sum_similarity = rhs.sum_similarity();
        if (lhs_sum_similarity.partial < rhs_sum_similarity.partial)
        {
            return true;
        }
        if (rhs_sum_similarity.partial < lhs_sum_similarity.partial)
        {
            return false;
        }
        if (rhs.item_value_length < lhs.item_value_length)
        {
            return true;
        }
        if (lhs.item_value_length < rhs.item_value_length)
        {
            return false;
        }
        return lhs_sum_similarity.complete < rhs_sum_similarity.complete;
    }
    friend bool operator==(const ExpressionMatch& lhs, const ExpressionMatch& rhs)
    {
        return !(lhs < rhs) &&
               !(rhs < lhs);
    }
    friend bool operator!=(const ExpressionMatch& lhs, const ExpressionMatch& rhs)
    {
        return !(lhs == rhs);
    }
};

auto get_best_matches(const std::map<std::string, Record::Data>& item_data)
{
    std::vector<ExpressionMatch> best_matches;
    std::for_each(begin(item_data), end(item_data), [&](const auto& item_name_data)
    {
        ExpressionMatch expression_match;
        expression_match.item_name = item_name_data.first;
        expression_match.item_value_length = item_name_data.second.value_length;
        const auto& similarities = item_name_data.second.similarities;
        const bool first_item = best_matches.empty();
        if (first_item)
        {
            best_matches.reserve(item_name_data.second.similarities.size());
            std::transform(begin(similarities), end(similarities), back_inserter(best_matches),
                    [&](const auto& similarity)
                    {
                        expression_match.words_similarities = similarity.words_similarities;
                        return expression_match;
                    });
        }
        else
        {
            std::size_t expression_idx = 0;
            std::for_each(begin(similarities), end(similarities), [&](const auto& similarity)
            {
                expression_match.words_similarities = similarity.words_similarities;
                if (best_matches[expression_idx] < expression_match)
                {
                    best_matches[expression_idx] = expression_match;
                }
                ++expression_idx;
            });
        }
    });
    return best_matches;
}

auto get_all_matched_items(const std::map<std::string, Record::Data>& item_data)
{
    std::set<std::string> matched_items;
    std::for_each(begin(item_data), end(item_data), [&](const auto& item_name_data)
    {
        const auto& similarities = item_name_data.second.similarities;
        const auto is_match = std::any_of(begin(similarities), end(similarities), [](const auto& similarity)
        {
            return Contact::is_match(similarity.words_similarities);
        });
        if (is_match)
        {
            matched_items.insert(item_name_data.first);
        }
    });
    return matched_items;
}

bool all_expressions_found(const std::vector<ExpressionMatch>& matches)
{
    for (const auto& expression_match : matches)
    {
        if (!expression_match.is_match())
        {
            return false;
        }
    }
    return true;
}

bool all_expressions_equal(const std::vector<ExpressionMatch>& lhs, const std::vector<ExpressionMatch>& rhs)
{
    if (lhs.size() != rhs.size())
    {
        return false;
    }
    for (std::size_t idx = 0; idx < lhs.size(); ++idx)
    {
        if (lhs[idx] != rhs[idx])
        {
            return false;
        }
    }
    return true;
}

std::unique_ptr<std::vector<Record>> combine(const std::vector<Record>& a, const std::vector<Record>& b)
{
    auto sum = std::make_unique<std::vector<Record>>();
    sum->reserve(a.size() + b.size());
    auto a_top = a.begin();
    auto b_top = b.begin();
    while ((a_top != a.end()) && (b_top != b.end()))
    {
        if (a_top->object.numeric_id < b_top->object.numeric_id)
        {
            sum->push_back(*a_top);
            ++a_top;
        }
        else if (b_top->object.numeric_id < a_top->object.numeric_id)
        {
            sum->push_back(*b_top);
            ++b_top;
        }
        else
        {
            if (a_top->period.valid_from < b_top->period.valid_from)
            {
                sum->push_back(*a_top);
                ++a_top;
            }
            else if (b_top->period.valid_from < a_top->period.valid_from)
            {
                sum->push_back(*b_top);
                ++b_top;
            }
            else
            {
                Record c = *a_top;
                c.item_data.insert(b_top->item_data.begin(), b_top->item_data.end());
                sum->push_back(c);
                ++a_top;
                ++b_top;
            }
        }
    }
    while (a_top != a.end())
    {
        sum->push_back(*a_top);
        ++a_top;
    }
    while (b_top != b.end())
    {
        sum->push_back(*b_top);
        ++b_top;
    }
    return sum;
}

template <ContactItem item>
Record make_record_for_item(
    const Database::Result::Row& row,
    const std::vector<std::size_t>& numbers_of_words)
{
    Record record;
    record.object.numeric_id = static_cast<unsigned long long>(row[0]);
    record.object.contact_id = Util::make_strong<Registry::Contact::ContactId>(
        boost::uuids::string_generator()(static_cast<std::string>(row[1])));
    record.period.history_id = Util::make_strong<Registry::Contact::ContactHistoryId>(
        boost::uuids::string_generator()(static_cast<std::string>(row[2])));
    if (!row[3].isnull())
    {
        record.period.next_history_id = Util::make_strong<Registry::Contact::ContactHistoryId>(
            boost::uuids::string_generator()(static_cast<std::string>(row[3])));
    }
    record.period.valid_from = Util::Into<TimePoint>::from(static_cast<std::string>(row[4]));
    if (!row[5].isnull())
    {
        record.period.valid_to = Util::Into<TimePoint>::from(static_cast<std::string>(row[5]));
    }
    Record::Data data;
    data.value_length = static_cast<std::size_t>(row[6]);
    data.similarities.reserve(numbers_of_words.size());
    auto column_idx = 7;
    for (const auto words : numbers_of_words)
    {
        Record::Data::ExpressionSimilarity expression_similarity;
        expression_similarity.words_similarities.reserve(words);
        for (std::size_t word_idx = 0; word_idx < words; ++word_idx, column_idx += 2)
        {
            Record::Data::WordSimilarity word_similarity;
            word_similarity.partial = static_cast<double>(row[column_idx]);
            word_similarity.complete = static_cast<double>(row[column_idx + 1]);
            expression_similarity.words_similarities.push_back(word_similarity);
        }
        data.similarities.push_back(expression_similarity);
    }
    record.item_data[ContactItemTraits<item>::item_name()] = std::move(data);
    return record;
}

auto collect_order_by(int& idx, Collector& order_by_1, Collector& order_by_2)
{
    ++idx;
    const auto str_idx = std::to_string(idx);
    struct SqlColumns
    {
        std::string w;
        std::string s;
    };
    auto col = SqlColumns{
            "w" + str_idx,
            "s" + str_idx};
    order_by_1.append(col.w).append(col.s);
    order_by_2.append(col.w + "*" + col.w).append(col.s + "*" + col.s);
    return col;
}

void apply_limit(
        std::string& sql,
        int number_of_parameters,
        const std::string& order_by_1,
        const std::string& order_by_2)
{
    sql = "WITH all_c AS (" + sql + "),"
          "best_c AS "
          "("
              "SELECT * "
                "FROM all_c "
            "ORDER BY GREATEST(" + order_by_1 + ") DESC, " +
                      order_by_2 + " DESC "
               "LIMIT 10 * $" + std::to_string(number_of_parameters) + "::INT"
          ") "
          "SELECT * "
            "FROM best_c";
}

#if 0
WITH all_c AS
(
    SELECT c.id AS contact_id,
           obr.uuid,
           h.uuid,
           hn.uuid,
           h.valid_from AS history_valid_from,
           h.valid_to,
           length(c.v),
           word_similarity(unaccented.v1w1,c.v) AS w1, similarity(unaccented.v1w1,c.v) AS s1,
           word_similarity(unaccented.v1w2,c.v) AS w2, similarity(unaccented.v1w2,c.v) AS s2,
           word_similarity(unaccented.v2w1,c.v) AS w3, similarity(unaccented.v2w1,c.v) AS s3
      FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
           (SELECT id,historyid,f_unaccent(city) FROM contact_history) AS c(id,history_id,v)
      JOIN object_registry obr ON obr.id=c.id
      JOIN history h ON h.id=c.history_id
 LEFT JOIN history hn ON hn.id=h.next
     WHERE (((unaccented.v1w1 <% c.v) AND
             (unaccented.v1w2 <% c.v)) OR
            (unaccented.v2w1 <% c.v)) AND
           ('2010-01-01 0:00:00'::TIMESTAMP<obr.erdate OR obr.erdate IS NULL) AND
           ('2010-01-01 0:00:00'::TIMESTAMP<h.valid_to OR h.valid_to IS NULL) AND
           (h.valid_from<='2011-01-01 0:00:00'::TIMESTAMP)
),
best_c AS (
    SELECT *
      FROM all_c
  ORDER BY GREATEST(w1, s1, w2, s2, w3, s3) DESC,
           w1*w1+s1*s1+w2*w2+s2*s2+w3*w3+s3*s3 DESC
     LIMIT 10 * 100)
SELECT *
  FROM best_c
 ORDER BY contact_id, history_valid_from;

SELECT c.id AS contact_id,
       obr.uuid,
       h.uuid,
       NULL,
       h.valid_from AS history_valid_from,
       NULL,
       length(c.v),
       word_similarity(unaccented.v1w1,c.v),similarity(unaccented.v1w1,c.v),
       word_similarity(unaccented.v1w2,c.v),similarity(unaccented.v1w2,c.v),
       word_similarity(unaccented.v2w1,c.v),similarity(unaccented.v2w1,c.v)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     (SELECT id,historyid,f_unaccent(city) FROM contact_history) AS c(id,history_id,v)
JOIN object_registry obr ON obr.id=c.id
JOIN history h ON h.id=c.history_id
WHERE (((unaccented.v1w1 <% c.v) AND
        (unaccented.v1w2 <% c.v)) OR
       (unaccented.v2w1 <% c.v)) AND
      h.valid_to IS NULL AND
      obr.erdate IS NULL
ORDER BY contact_id, history_valid_from;
#endif
template <ContactItem item>
class SearchIn
{
public:
    explicit SearchIn(
            const std::vector<std::size_t>& numbers_of_words,
            const boost::optional<HistoryLimit>& history_limit,
            bool limited_number_of_rows)
    {
        Collector what(Delimiter(","));
        Collector order_by_1(Delimiter(","));
        Collector order_by_2(Delimiter("+"));
        Collector unaccented_what(Delimiter(","));
        Collector unaccented_as(Delimiter(","));
        CollectorWithParentheses where(Delimiter(" OR "));
        int param_idx = 0;
        int value_idx = 0;
        int idx = 0;
        for (const auto words : numbers_of_words)
        {
            CollectorWithParentheses sub_where(Delimiter(" AND "));
            const std::string value_name = "v" + std::to_string(value_idx + 1) + "w";
            for (unsigned word_idx = 0; word_idx < words; ++word_idx, ++param_idx)
            {
                const std::string value_word_name = value_name + std::to_string(word_idx + 1);
                const auto col = collect_order_by(idx, order_by_1, order_by_2);
                what.append("word_similarity(unaccented." + value_word_name + ",c.v) AS " + col.w + ","
                            "similarity(unaccented." + value_word_name + ",c.v) AS " + col.s + "");
                unaccented_what.append("f_unaccent($" + std::to_string(param_idx + 1) + "::TEXT)");
                unaccented_as.append(value_word_name);
                sub_where.append("unaccented." + value_word_name + " <% c.v");
            }
            where.append(sub_where.get_sum());
            ++value_idx;
        }
        const bool search_in_history = history_limit != boost::none;
        if (search_in_history)
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "obr.uuid,"
                          "h.uuid,"
                          "hn.uuid,"
                          "h.valid_from AS history_valid_from,"
                          "h.valid_to,"
                          "length(c.v)," +
                           what.get_sum() + " "
                     "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                          "(SELECT id,historyid," + ContactItemTraits<item>::transformed_column_value() + " "
                             "FROM contact_history) c(id,history_id,v) "
                     "JOIN object_registry obr ON obr.id=c.id "
                     "JOIN history h ON h.id=c.history_id "
                "LEFT JOIN history hn ON hn.id=h.next "
                    "WHERE ";
            const bool search_limited_by_date =
                (history_limit->data_valid_from != boost::none) ||
                (history_limit->data_valid_to != boost::none);
            if (search_limited_by_date)
            {
                sql_ += "(" + where.get_sum() + ")";
                const bool has_lower_limit = (history_limit->data_valid_from != boost::none);
                if (has_lower_limit)
                {
                    ++param_idx;
                    const std::string lower_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (" + lower_limit + "<obr.erdate OR obr.erdate IS NULL)"
                            " AND (" + lower_limit + "<h.valid_to OR h.valid_to IS NULL)";
                }
                const bool has_upper_limit = (history_limit->data_valid_to != boost::none);
                if (has_upper_limit)
                {
                    ++param_idx;
                    const std::string upper_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (h.valid_from<=" + upper_limit + ")";
                }
            }
            else
            {
                sql_ += where.get_sum();
            }
        }
        else
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "obr.uuid,"
                          "h.uuid,"
                          "NULL,"
                          "h.valid_from AS history_valid_from,"
                          "NULL,"
                          "length(c.v)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "(SELECT id,historyid," + ContactItemTraits<item>::transformed_column_value() + " "
                         "FROM contact_history) c(id,history_id,v) "
                   "JOIN object_registry obr ON obr.id=c.id "
                   "JOIN history h ON h.id=c.history_id "
                   "WHERE (" + where.get_sum() + ") AND "
                         "obr.erdate IS NULL AND "
                         "h.valid_to IS NULL";
        }
        if (limited_number_of_rows)
        {
            apply_limit(sql_, param_idx + 1, order_by_1.get_sum(), order_by_2.get_sum());
        }
        sql_ += " ORDER BY contact_id, history_valid_from";
    }
    const std::string& get_sql() const noexcept
    {
        return sql_;
    }
    static Record make_record(
        const Database::Result::Row& row,
        const std::vector<std::size_t>& numbers_of_words)
    {
        return make_record_for_item<item>(row, numbers_of_words);
    }
private:
    std::string sql_;
};

#if 0
SELECT c.id AS contact_id,
       obr.uuid,
       h.uuid,
       hn.uuid,
       h.valid_from AS history_valid_from,
       h.valid_to,
       length(obr.name),
       word_similarity(unaccented.v1w1,obr.name),similarity(unaccented.v1w1,obr.name),
       word_similarity(unaccented.v1w2,obr.name),similarity(unaccented.v1w2,obr.name),
       word_similarity(unaccented.v2w1,obr.name),similarity(unaccented.v2w1,obr.name)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     object_registry obr
JOIN contact_history c ON c.id=obr.id
JOIN history h ON h.id=c.historyid
LEFT JOIN history hn ON hn.id=h.next
WHERE (((unaccented.v1w1 <% obr.name) AND
        (unaccented.v1w2 <% obr.name)) OR
       (unaccented.v2w1 <% obr.name)) AND
      obr.type=get_object_type_id('contact') AND
      ('2012-01-01 0:00:00'::TIMESTAMP<=obr.erdate OR obr.erdate IS NULL) AND
      ('2012-01-01 0:00:00'::TIMESTAMP<=h.valid_to OR h.valid_to IS NULL) AND
      (h.valid_from<'2016-01-01 0:00:00'::TIMESTAMP)
ORDER BY contact_id, history_valid_from;

SELECT c.id AS contact_id,
       obr.uuid,
       h.uuid,
       NULL,
       h.valid_from AS history_valid_from,
       NULL,
       length(obr.name),
       word_similarity(unaccented.v1w1,obr.name),similarity(unaccented.v1w1,obr.name),
       word_similarity(unaccented.v1w2,obr.name),similarity(unaccented.v1w2,obr.name),
       word_similarity(unaccented.v2w1,obr.name),similarity(unaccented.v2w1,obr.name)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     object_registry obr
JOIN contact_history c ON c.id=obr.id
JOIN history h ON h.id=c.historyid
WHERE (((unaccented.v1w1 <% obr.name) AND
       (unaccented.v1w2 <% obr.name)) OR
       (unaccented.v2w1 <% obr.name)) AND
      h.valid_to IS NULL AND
      obr.erdate IS NULL AND
      obr.type=get_object_type_id('contact')
ORDER BY contact_id, history_valid_from;
#endif
template <>
class SearchIn<ContactItem::contact_handle>
{
public:
    explicit SearchIn(
            const std::vector<std::size_t>& numbers_of_words,
            const boost::optional<HistoryLimit>& history_limit,
            bool limited_number_of_rows)
    {
        Collector what(Delimiter(","));
        Collector order_by_1(Delimiter(","));
        Collector order_by_2(Delimiter("+"));
        Collector unaccented_what(Delimiter(","));
        Collector unaccented_as(Delimiter(","));
        CollectorWithParentheses where(Delimiter(" OR "));
        int param_idx = 0;
        int value_idx = 0;
        int idx = 0;
        for (const auto words : numbers_of_words)
        {
            CollectorWithParentheses sub_where(Delimiter(" AND "));
            const std::string value_name = "v" + std::to_string(value_idx + 1) + "w";
            for (unsigned word_idx = 0; word_idx < words; ++word_idx, ++param_idx)
            {
                const std::string value_word_name = value_name + std::to_string(word_idx + 1);
                const auto col = collect_order_by(idx, order_by_1, order_by_2);
                what.append("word_similarity(unaccented." + value_word_name + ",obr.name) AS " + col.w + ","
                            "similarity(unaccented." + value_word_name + ",obr.name) AS " + col.s);
                unaccented_what.append("f_unaccent($" + std::to_string(param_idx + 1) + "::TEXT)");
                unaccented_as.append(value_word_name);
                sub_where.append("unaccented." + value_word_name + " <% obr.name");
            }
            where.append(sub_where.get_sum());
            ++value_idx;
        }
        const bool search_in_history = history_limit != boost::none;
        if (search_in_history)
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "obr.uuid,"
                          "h.uuid,"
                          "hn.uuid,"
                          "h.valid_from AS history_valid_from,"
                          "h.valid_to,"
                          "length(obr.name)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "object_registry obr "
                   "JOIN contact_history c ON c.id=obr.id "
                   "JOIN history h ON h.id=c.historyid "
                   "LEFT JOIN history hn ON hn.id=h.next "
                   "WHERE (" + where.get_sum() + ") AND "
                         "obr.type=get_object_type_id('contact')";
            const bool search_limited_by_date =
                (history_limit->data_valid_from != boost::none) ||
                (history_limit->data_valid_to != boost::none);
            if (search_limited_by_date)
            {
                const bool has_lower_limit = (history_limit->data_valid_from != boost::none);
                if (has_lower_limit)
                {
                    ++param_idx;
                    const std::string lower_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (" + lower_limit + "<obr.erdate OR obr.erdate IS NULL)"
                                                     " AND (" + lower_limit + "<h.valid_to OR h.valid_to IS NULL)";
                }
                const bool has_upper_limit = (history_limit->data_valid_to != boost::none);
                if (has_upper_limit)
                {
                    ++param_idx;
                    const std::string upper_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (h.valid_from<=" + upper_limit + ")";
                }
            }
        }
        else
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "obr.uuid,"
                          "h.uuid,"
                          "NULL,"
                          "h.valid_from AS history_valid_from,"
                          "NULL,"
                          "length(obr.name)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "object_registry obr "
                   "JOIN contact_history c ON c.id=obr.id "
                   "JOIN history h ON h.id=c.historyid "
                   "WHERE (" + where.get_sum() + ") AND "
                         "h.valid_to IS NULL AND "
                         "obr.erdate IS NULL AND "
                         "obr.type=get_object_type_id('contact')";
        }
        if (limited_number_of_rows)
        {
            apply_limit(sql_, param_idx + 1, order_by_1.get_sum(), order_by_2.get_sum());
        }
        sql_ += " ORDER BY contact_id, history_valid_from";
    }
    const std::string& get_sql() const noexcept
    {
        return sql_;
    }
    static Record make_record(
        const Database::Result::Row& row,
        const std::vector<std::size_t>& numbers_of_words)
    {
        return make_record_for_item<ContactItem::contact_handle>(row, numbers_of_words);
    }
private:
    std::string sql_;
};

#if 0
SELECT c.id AS contact_id,
       c.ssn_type,
       obr.uuid,
       h.uuid,
       hn.uuid,
       h.valid_from AS history_valid_from,
       h.valid_to,
       length(c.v),
       word_similarity(unaccented.v1w1,c.v),similarity(unaccented.v1w1,c.v),
       word_similarity(unaccented.v1w2,c.v),similarity(unaccented.v1w2,c.v),
       word_similarity(unaccented.v2w1,c.v),similarity(unaccented.v2w1,c.v)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     (SELECT c.id,c.historyid,c.ssn,es.type FROM contact_history c
      JOIN enum_ssntype es ON es.id=c.ssntype) c(id,history_id,v,ssn_type)
JOIN object_registry obr ON obr.id=c.id
JOIN history h ON h.id=c.history_id
LEFT JOIN history hn ON hn.id=h.next
WHERE (((unaccented.v1w1 <% c.v) AND
        (unaccented.v1w2 <% c.v)) OR
       (unaccented.v2w1 <% c.v)) AND
      ('2010-01-01 0:00:00'::TIMESTAMP<obr.erdate OR obr.erdate IS NULL) AND
      ('2010-01-01 0:00:00'::TIMESTAMP<h.valid_to OR h.valid_to IS NULL) AND
      (h.valid_from<='2011-01-01 0:00:00'::TIMESTAMP)
ORDER BY contact_id, history_valid_from;

SELECT c.id AS contact_id,
       c.ssn_type,
       obr.uuid,
       h.uuid,
       NULL,
       h.valid_from AS history_valid_from,
       NULL,
       length(c.v),
       word_similarity(unaccented.v1w1,c.v),similarity(unaccented.v1w1,c.v),
       word_similarity(unaccented.v1w2,c.v),similarity(unaccented.v1w2,c.v),
       word_similarity(unaccented.v2w1,c.v),similarity(unaccented.v2w1,c.v)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     (SELECT c.id,c.historyid,c.ssn,es.type FROM contact_history c
      JOIN enum_ssntype es ON es.id=c.ssntype) c(id,history_id,v,ssn_type)
JOIN object_registry obr ON obr.id=c.id
JOIN history h ON h.id=c.history_id
WHERE (((unaccented.v1w1 <% c.v) AND
        (unaccented.v1w2 <% c.v)) OR
       (unaccented.v2w1 <% c.v)) AND
      h.valid_to IS NULL AND
      obr.erdate IS NULL
ORDER BY contact_id, history_valid_from;
#endif
template <>
class SearchIn<ContactItem::additional_identifier>
{
public:
    explicit SearchIn(
            const std::vector<std::size_t>& numbers_of_words,
            const boost::optional<HistoryLimit>& history_limit,
            bool limited_number_of_rows)
    {
        Collector what(Delimiter(","));
        Collector order_by_1(Delimiter(","));
        Collector order_by_2(Delimiter("+"));
        Collector unaccented_what(Delimiter(","));
        Collector unaccented_as(Delimiter(","));
        CollectorWithParentheses where(Delimiter(" OR "));
        int param_idx = 0;
        int value_idx = 0;
        int idx = 0;
        for (const auto words : numbers_of_words)
        {
            CollectorWithParentheses sub_where(Delimiter(" AND "));
            const std::string value_name = "v" + std::to_string(value_idx + 1) + "w";
            for (unsigned word_idx = 0; word_idx < words; ++word_idx, ++param_idx)
            {
                const std::string value_word_name = value_name + std::to_string(word_idx + 1);
                const auto col = collect_order_by(idx, order_by_1, order_by_2);
                what.append("word_similarity(unaccented." + value_word_name + ",c.v) AS " + col.w + ","
                            "similarity(unaccented." + value_word_name + ",c.v) AS " + col.s);
                unaccented_what.append("f_unaccent($" + std::to_string(param_idx + 1) + "::TEXT)");
                unaccented_as.append(value_word_name);
                sub_where.append("unaccented." + value_word_name + " <% c.v");
            }
            where.append(sub_where.get_sum());
            ++value_idx;
        }
        const bool search_in_history = history_limit != boost::none;
        if (search_in_history)
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "c.ssn_type,"
                          "obr.uuid,"
                          "h.uuid,"
                          "hn.uuid,"
                          "h.valid_from AS history_valid_from,"
                          "h.valid_to,"
                          "length(c.v)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "(SELECT c.id,c.historyid,c.ssn,es.type "
                         "FROM contact_history c "
                         "JOIN enum_ssntype es ON es.id=c.ssntype) c(id,history_id,v,ssn_type) "
                   "JOIN object_registry obr ON obr.id=c.id "
                   "JOIN history h ON h.id=c.history_id "
                   "LEFT JOIN history hn ON hn.id=h.next "
                   "WHERE ";
            const bool search_limited_by_date =
                (history_limit->data_valid_from != boost::none) ||
                (history_limit->data_valid_to != boost::none);
            if (search_limited_by_date)
            {
                sql_ += "(" + where.get_sum() + ")";
                const bool has_lower_limit = (history_limit->data_valid_from != boost::none);
                if (has_lower_limit)
                {
                    ++param_idx;
                    const std::string lower_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (" + lower_limit + "<obr.erdate OR obr.erdate IS NULL)"
                            " AND (" + lower_limit + "<h.valid_to OR h.valid_to IS NULL)";
                }
                const bool has_upper_limit = (history_limit->data_valid_to != boost::none);
                if (has_upper_limit)
                {
                    ++param_idx;
                    const std::string upper_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (h.valid_from<=" + upper_limit + ")";
                }
            }
            else
            {
                sql_ += where.get_sum();
            }
        }
        else
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "c.ssn_type,"
                          "obr.uuid,"
                          "h.uuid,"
                          "NULL,"
                          "h.valid_from AS history_valid_from,"
                          "NULL,"
                          "length(c.v)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "(SELECT c.id,c.historyid,c.ssn,es.type "
                         "FROM contact_history c "
                         "JOIN enum_ssntype es ON es.id=c.ssntype) c(id,history_id,v,ssn_type) "
                   "JOIN object_registry obr ON obr.id=c.id "
                   "JOIN history h ON h.id=c.history_id "
                   "WHERE (" + where.get_sum() + ") AND "
                   "h.valid_to IS NULL AND "
                   "obr.erdate IS NULL";
        }
        if (limited_number_of_rows)
        {
            apply_limit(sql_, param_idx + 1, order_by_1.get_sum(), order_by_2.get_sum());
        }
        sql_ += " ORDER BY contact_id, history_valid_from";
    }
    const std::string& get_sql() const noexcept
    {
        return sql_;
    }
    static Record make_record(
        const Database::Result::Row& row,
        const std::vector<std::size_t>& numbers_of_words)
    {
        Record record;
        record.object.numeric_id = static_cast<unsigned long long>(row[0]);
        const auto ssn_type = row[1].isnull() ? std::string()
                                              : static_cast<std::string>(row[1]);
        const std::string ssn_type_name = [&]()
                {
                    if (ssn_type == "RC")
                    {
                        return "national_identity_number";
                    }
                    if (ssn_type == "OP")
                    {
                        return "national_identity_card";
                    }
                    if (ssn_type == "PASS")
                    {
                        return "passport_number";
                    }
                    if (ssn_type == "ICO")
                    {
                        return "company_registration_number";
                    }
                    if (ssn_type == "MPSV")
                    {
                        return "social_security_number";
                    }
                    if (ssn_type == "BIRTHDAY")
                    {
                        return "birthdate";
                    }
                    return "";
                }();
        record.object.contact_id = Util::make_strong<Registry::Contact::ContactId>(
                boost::uuids::string_generator()(static_cast<std::string>(row[2])));
        record.period.history_id = Util::make_strong<Registry::Contact::ContactHistoryId>(
                boost::uuids::string_generator()(static_cast<std::string>(row[3])));
        if (!row[4].isnull())
        {
            record.period.next_history_id = Util::make_strong<Registry::Contact::ContactHistoryId>(
                    boost::uuids::string_generator()(static_cast<std::string>(row[4])));
        }
        record.period.valid_from = Util::Into<TimePoint>::from(static_cast<std::string>(row[5]));
        if (!row[6].isnull())
        {
            record.period.valid_to = Util::Into<TimePoint>::from(static_cast<std::string>(row[6]));
        }
        Record::Data data;
        data.value_length = static_cast<std::size_t>(row[7]);
        data.similarities.reserve(numbers_of_words.size());
        auto column_idx = 8;
        for (const auto words : numbers_of_words)
        {
            Record::Data::ExpressionSimilarity expression_similarity;
            expression_similarity.words_similarities.reserve(words);
            for (std::size_t word_idx = 0; word_idx < words; ++word_idx, column_idx += 2)
            {
                Record::Data::WordSimilarity word_similarity;
                word_similarity.partial = static_cast<double>(row[column_idx]);
                word_similarity.complete = static_cast<double>(row[column_idx + 1]);
                expression_similarity.words_similarities.push_back(word_similarity);
            }
            data.similarities.push_back(expression_similarity);
        }
        record.item_data["additional_identifier." + ssn_type_name] = data;
        return record;
    }
private:
    std::string sql_;
};

template <>
struct ContactItemTraits<ContactItem::address_company>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(company_name)";
    }
    static constexpr const char* item_name()
    {
        return "company";
    }
};

template <>
struct ContactItemTraits<ContactItem::address_street>
{
    static constexpr const char* transformed_column_value()
    {
        return "unaccent_streets(street1,street2,street3)";
    }
    static constexpr const char* item_name()
    {
        return "street";
    }
};

template <>
struct ContactItemTraits<ContactItem::address_city>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(city)";
    }
    static constexpr const char* item_name()
    {
        return "city";
    }
};

template <>
struct ContactItemTraits<ContactItem::address_state_or_province>
{
    static constexpr const char* transformed_column_value()
    {
        return "f_unaccent(stateorprovince)";
    }
    static constexpr const char* item_name()
    {
        return "state_or_province";
    }
};

template <>
struct ContactItemTraits<ContactItem::address_postal_code>
{
    static constexpr const char* transformed_column_value()
    {
        return "postalcode";
    }
    static constexpr const char* item_name()
    {
        return "postal_code";
    }
};

#if 0
SELECT c.id AS contact_id,
       c.type,
       obr.uuid,
       h.uuid,
       hn.uuid,
       h.valid_from AS history_valid_from,
       h.valid_to,
       length(c.v),
       word_similarity(unaccented.v1w1,c.v),similarity(unaccented.v1w1,c.v),
       word_similarity(unaccented.v1w2,c.v),similarity(unaccented.v1w2,c.v),
       word_similarity(unaccented.v2w1,c.v),similarity(unaccented.v2w1,c.v)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     (SELECT contactid,historyid,f_unaccent(city),type
      FROM contact_address_history) AS c(id,history_id,v,type)
JOIN object_registry obr ON obr.id=c.id
JOIN history h ON h.id=c.history_id
LEFT JOIN history hn ON hn.id=h.next
WHERE (((unaccented.v1w1 <% c.v) AND
        (unaccented.v1w2 <% c.v)) OR
       (unaccented.v2w1 <% c.v)) AND
      ('2010-01-01 0:00:00'::TIMESTAMP<obr.erdate OR obr.erdate IS NULL) AND
      ('2010-01-01 0:00:00'::TIMESTAMP<h.valid_to OR h.valid_to IS NULL) AND
      (h.valid_from<='2011-01-01 0:00:00'::TIMESTAMP)
ORDER BY contact_id, history_valid_from;

SELECT c.id AS contact_id,
       c.type,
       obr.uuid,
       h.uuid,
       NULL,
       h.valid_from AS history_valid_from,
       NULL,
       length(c.v),
       word_similarity(unaccented.v1w1,c.v),similarity(unaccented.v1w1,c.v),
       word_similarity(unaccented.v1w2,c.v),similarity(unaccented.v1w2,c.v),
       word_similarity(unaccented.v2w1,c.v),similarity(unaccented.v2w1,c.v)
FROM (SELECT f_unaccent('Moravské'),f_unaccent('Budějovice'),f_unaccent('Novák')) AS unaccented(v1w1,v1w2,v2w1),
     (SELECT contactid,historyid,f_unaccent(city),type
      FROM contact_address_history) AS c(id,history_id,v,type)
JOIN object_registry obr ON obr.id=c.id
JOIN history h ON h.id=c.history_id
WHERE (((unaccented.v1w1 <% c.v) AND
        (unaccented.v1w2 <% c.v)) OR
       (unaccented.v2w1 <% c.v)) AND
      h.valid_to IS NULL AND
      obr.erdate IS NULL
ORDER BY contact_id, history_valid_from;
#endif
template <ContactItem item>
class SearchInContactAddress
{
public:
    explicit SearchInContactAddress(
            const std::vector<std::size_t>& numbers_of_words,
            const boost::optional<HistoryLimit>& history_limit,
            bool limited_number_of_rows)
    {
        Collector what(Delimiter(","));
        Collector order_by_1(Delimiter(","));
        Collector order_by_2(Delimiter("+"));
        Collector unaccented_what(Delimiter(","));
        Collector unaccented_as(Delimiter(","));
        CollectorWithParentheses where(Delimiter(" OR "));
        int param_idx = 0;
        int value_idx = 0;
        int idx = 0;
        for (const auto words : numbers_of_words)
        {
            CollectorWithParentheses sub_where(Delimiter(" AND "));
            const std::string value_name = "v" + std::to_string(value_idx + 1) + "w";
            for (unsigned word_idx = 0; word_idx < words; ++word_idx, ++param_idx)
            {
                const std::string value_word_name = value_name + std::to_string(word_idx + 1);
                const auto col = collect_order_by(idx, order_by_1, order_by_2);
                what.append("word_similarity(unaccented." + value_word_name + ",c.v) AS " + col.w + ","
                            "similarity(unaccented." + value_word_name + ",c.v) AS " + col.s);
                unaccented_what.append("f_unaccent($" + std::to_string(param_idx + 1) + "::TEXT)");
                unaccented_as.append(value_word_name);
                sub_where.append("unaccented." + value_word_name + " <% c.v");
            }
            where.append(sub_where.get_sum());
            ++value_idx;
        }
        const bool search_in_history = history_limit != boost::none;
        if (search_in_history)
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "c.type,"
                          "obr.uuid,"
                          "h.uuid,"
                          "hn.uuid,"
                          "h.valid_from AS history_valid_from,"
                          "h.valid_to,"
                          "length(c.v)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "(SELECT contactid,historyid," + ContactItemTraits<item>::transformed_column_value() + ",type "
                         "FROM contact_address_history) c(id,history_id,v,type) "
                   "JOIN object_registry obr ON obr.id=c.id "
                   "JOIN history h ON h.id=c.history_id "
                   "LEFT JOIN history hn ON hn.id=h.next "
                   "WHERE ";
            const bool search_limited_by_date =
                (history_limit->data_valid_from != boost::none) ||
                (history_limit->data_valid_to != boost::none);
            if (search_limited_by_date)
            {
                sql_ += "(" + where.get_sum() + ")";
                const bool has_lower_limit = (history_limit->data_valid_from != boost::none);
                if (has_lower_limit)
                {
                    ++param_idx;
                    const std::string lower_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (" + lower_limit + "<obr.erdate OR obr.erdate IS NULL)"
                                                     " AND (" + lower_limit + "<h.valid_to OR h.valid_to IS NULL)";
                }
                const bool has_upper_limit = (history_limit->data_valid_to != boost::none);
                if (has_upper_limit)
                {
                    ++param_idx;
                    const std::string upper_limit = "$" + std::to_string(param_idx) + "::TIMESTAMP";
                    sql_ += " AND (h.valid_from<=" + upper_limit + ")";
                }
            }
            else
            {
                sql_ += where.get_sum();
            }
        }
        else
        {
            sql_ = "SELECT c.id AS contact_id,"
                          "c.type,"
                          "obr.uuid,"
                          "h.uuid,"
                          "NULL,"
                          "h.valid_from AS history_valid_from,"
                          "NULL,"
                          "length(c.v)," +
                           what.get_sum() + " "
                   "FROM (SELECT " + unaccented_what.get_sum() + ") unaccented(" + unaccented_as.get_sum() + "),"
                        "(SELECT contactid,historyid," + ContactItemTraits<item>::transformed_column_value() + ",type "
                         "FROM contact_address_history) c(id,history_id,v,type) "
                   "JOIN object_registry obr ON obr.id=c.id "
                   "JOIN history h ON h.id=c.history_id "
                   "WHERE (" + where.get_sum() + ") AND "
                         "h.valid_to IS NULL AND "
                         "obr.erdate IS NULL";
        }
        if (limited_number_of_rows)
        {
            apply_limit(sql_, param_idx + 1, order_by_1.get_sum(), order_by_2.get_sum());
        }
        sql_ += " ORDER BY contact_id, history_valid_from";
    }
    const std::string& get_sql() const noexcept
    {
        return sql_;
    }
    static Record make_record(
        const Database::Result::Row& row,
        const std::vector<std::size_t>& numbers_of_words)
    {
        Record record;
        record.object.numeric_id = static_cast<unsigned long long>(row[0]);
        const auto address_type = static_cast<std::string>(row[1]);
        record.object.contact_id = Util::make_strong<Registry::Contact::ContactId>(
            boost::uuids::string_generator()(static_cast<std::string>(row[2])));
        record.period.history_id = Util::make_strong<Registry::Contact::ContactHistoryId>(
            boost::uuids::string_generator()(static_cast<std::string>(row[3])));
        if (!row[4].isnull())
        {
            record.period.next_history_id = Util::make_strong<Registry::Contact::ContactHistoryId>(
                boost::uuids::string_generator()(static_cast<std::string>(row[4])));
        }
        record.period.valid_from = Util::Into<TimePoint>::from(static_cast<std::string>(row[5]));
        if (!row[6].isnull())
        {
            record.period.valid_to = Util::Into<TimePoint>::from(static_cast<std::string>(row[6]));
        }
        Record::Data data;
        const std::string item_name = [&]()
                {
                    if (address_type == "MAILING")
                    {
                        return "mailing_address";
                    }
                    if (address_type == "BILLING")
                    {
                        return "billing_address";
                    }
                    if (address_type == "SHIPPING")
                    {
                        return "shipping_address1";
                    }
                    if (address_type == "SHIPPING_2")
                    {
                        return "shipping_address2";
                    }
                    if (address_type == "SHIPPING_3")
                    {
                        return "shipping_address3";
                    }
                    return "";
                }() + std::string(".") + ContactItemTraits<item>::item_name();
        data.value_length = static_cast<std::size_t>(row[7]);
        data.similarities.reserve(numbers_of_words.size());
        auto column_idx = 8;
        for (const auto words : numbers_of_words)
        {
            Record::Data::ExpressionSimilarity expression_similarity;
            expression_similarity.words_similarities.reserve(words);
            for (std::size_t word_idx = 0; word_idx < words; ++word_idx, column_idx += 2)
            {
                Record::Data::WordSimilarity word_similarity;
                word_similarity.partial = static_cast<double>(row[column_idx]);
                word_similarity.complete = static_cast<double>(row[column_idx + 1]);
                expression_similarity.words_similarities.push_back(word_similarity);
            }
            data.similarities.push_back(expression_similarity);
        }
        record.item_data[item_name] = data;
        return record;
    }
private:
    std::string sql_;
};

template <>
class SearchIn<ContactItem::address_company> : public SearchInContactAddress<ContactItem::address_company>
{
public:
    using SearchInContactAddress<ContactItem::address_company>::SearchInContactAddress;
};

template <>
class SearchIn<ContactItem::address_street> : public SearchInContactAddress<ContactItem::address_street>
{
public:
    using SearchInContactAddress<ContactItem::address_street>::SearchInContactAddress;
};

template <>
class SearchIn<ContactItem::address_city> : public SearchInContactAddress<ContactItem::address_city>
{
public:
    using SearchInContactAddress<ContactItem::address_city>::SearchInContactAddress;
};

template <>
class SearchIn<ContactItem::address_state_or_province> : public SearchInContactAddress<ContactItem::address_state_or_province>
{
public:
    using SearchInContactAddress<ContactItem::address_state_or_province>::SearchInContactAddress;
};

template <>
class SearchIn<ContactItem::address_postal_code> : public SearchInContactAddress<ContactItem::address_postal_code>
{
public:
    using SearchInContactAddress<ContactItem::address_postal_code>::SearchInContactAddress;
};

template <ContactItem ...items>
using ContactColumns = std::integer_sequence<ContactItem, items...>;

template <typename T>
using SearchableItems = Util::EnumSequence<ContactColumns<ContactItem::name,
                                                          ContactItem::organization,
                                                          ContactItem::place_street,
                                                          ContactItem::place_city,
                                                          ContactItem::place_state_or_province,
                                                          ContactItem::place_postal_code,
                                                          ContactItem::telephone,
                                                          ContactItem::fax,
                                                          ContactItem::email,
                                                          ContactItem::notify_email,
                                                          ContactItem::vat_identification_number,
                                                          ContactItem::additional_identifier,
                                                          ContactItem::contact_handle,
                                                          ContactItem::address_company,
                                                          ContactItem::address_street,
                                                          ContactItem::address_city,
                                                          ContactItem::address_state_or_province,
                                                          ContactItem::address_postal_code>,
                                           T>;

class RunQuery
{
public:
    explicit RunQuery(
            const std::set<ContactItem>& searched_items,
            const std::vector<std::size_t>& numbers_of_words,
            const boost::optional<HistoryLimit>& history_limit,
            bool limited_number_of_rows,
            const Database::QueryParams& params)
        : searched_items_(searched_items),
          numbers_of_words_(numbers_of_words),
          history_limit_(history_limit),
          limited_number_of_rows_(limited_number_of_rows),
          params_(params)
    { }
    template <ContactItem column>
    void operator()(Util::SqlInThreadSolver& running_sql) const
    {
        const bool to_search = 0 < searched_items_.count(column);
        if (to_search)
        {
            running_sql = Util::sql_exec_in_thread(
                    SearchIn<column>{
                            numbers_of_words_,
                            history_limit_,
                            limited_number_of_rows_}.get_sql(),
                    params_,
                    Util::repeatable_read_read_only_transaction());
        }
    }
private:
    std::set<ContactItem> searched_items_;
    std::vector<std::size_t> numbers_of_words_;
    boost::optional<HistoryLimit> history_limit_;
    bool limited_number_of_rows_;
    Database::QueryParams params_;
};

class GetResult
{
public:
    explicit GetResult(const std::vector<std::size_t>& numbers_of_words)
        : numbers_of_words_{numbers_of_words},
          sum_matches_{std::make_unique<std::vector<Record>>()}
    { }
    template <ContactItem column>
    void operator()(Util::SqlInThreadSolver& running_sql)
    {
        if (running_sql.is_joinable())
        {
            const Database::Result task_result = Util::SqlInThreadSolver::Result(std::move(running_sql)).get_result();
            if (0 < task_result.size())
            {
                auto matches = std::make_unique<std::vector<Record>>();
                matches->reserve(task_result.size());
                for (std::size_t row_idx = 0; row_idx < task_result.size(); ++row_idx)
                {
                    matches->push_back(SearchIn<column>::make_record(task_result[row_idx], numbers_of_words_));
                }
                if (sum_matches_->empty())
                {
                    sum_matches_ = std::move(matches);
                }
                else
                {
                    auto combined_matches = combine(*sum_matches_, *matches);
                    sum_matches_ = std::move(combined_matches);
                }
            }
        }
    }
    const std::vector<Record>& get_sum_matches() const noexcept
    {
        return *sum_matches_;
    }
    void release_sum_matches() noexcept
    {
        sum_matches_.release();
    }
private:
    const std::vector<std::size_t>& numbers_of_words_;
    std::unique_ptr<std::vector<Record>> sum_matches_;
};

struct SearchDataRequest
{
    std::vector<std::string> query_values;
    boost::optional<std::size_t> max_number_of_contacts;
    boost::optional<HistoryLimit> history_limit;
    std::set<ContactItem> searched_items;
};

auto make_request(const SearchContactRequest& src)
{
    SearchDataRequest request;
    request.query_values = src.query_values;
    request.max_number_of_contacts = src.limit;
    request.history_limit = boost::none;
    request.searched_items = src.searched_items;
    return request;
}

auto make_request(const SearchContactHistoryRequest& src)
{
    SearchDataRequest request;
    request.query_values = src.query_values;
    request.max_number_of_contacts = src.max_number_of_contacts;
    request.history_limit = make_history_limit(src.data_valid_from, src.data_valid_to);
    request.searched_items = src.searched_items;
    return request;
}

auto search_contact(const SearchDataRequest& request)
{
    Database::QueryParams params;
    const std::vector<std::size_t> numbers_of_words = [&]()
    {
        std::vector<std::size_t> numbers_of_words;
        numbers_of_words.reserve(request.query_values.size());
        for (const auto& by_spaces_delimited_words : request.query_values)
        {
            const auto words = to_words(by_spaces_delimited_words);
            if (!words.empty())
            {
                params.insert(params.end(), words.cbegin(), words.cend());
            }
            numbers_of_words.push_back(words.size());
        }
        return numbers_of_words;
    }();
    if (request.history_limit != boost::none)
    {
        if (request.history_limit->data_valid_from != boost::none)
        {
            params.push_back(Util::Into<std::string>::from(*(request.history_limit->data_valid_from)));
        }
        if (request.history_limit->data_valid_to != boost::none)
        {
            params.push_back(Util::Into<std::string>::from(*(request.history_limit->data_valid_to)));
        }
    }
    const bool has_limit = request.max_number_of_contacts != boost::none;
    if (has_limit)
    {
        params.push_back(std::to_string(*request.max_number_of_contacts));
    }
    SearchableItems<Util::SqlInThreadSolver> running_sql{};

    visit(running_sql, RunQuery{request.searched_items, numbers_of_words, request.history_limit, has_limit, params});
    GetResult data_collector{numbers_of_words};
    {
        const struct LogDuration : Util::MeasureDuration<LogDuration>
        {
            static void measure_completed(Time duration_ms)
            {
                FREDLOG_DEBUG("data collected in " + std::to_string(duration_ms) + "ms");
            }
        } log_duration{};
        visit(running_sql, data_collector);
    }
    const struct LogDuration : Util::MeasureDuration<LogDuration>
    {
        static void measure_completed(Time duration_ms)
        {
            FREDLOG_DEBUG("matches completed in " + std::to_string(duration_ms) + "ms");
        }
    } log_duration{};
    struct Match
    {
        explicit Match(const Record::ObjectId& _object)
            : object{_object},
              histories{},
              similarity{0.0},
              value_length{0}
        { }
        Record::ObjectId object;
        struct History
        {
            std::vector<ExpressionMatch> expressions;
            std::vector<Record::Period> periods;
            std::set<std::string> matched_items;
        };
        std::vector<History> histories;
        double similarity;
        std::size_t value_length;
        void append_period(const Record::Period& period)
        {
            histories.back().periods.push_back(period);
        }
        void append_history(
                std::vector<ExpressionMatch>&& expressions_best_matches,
                const Record::Period& period,
                std::set<std::string> matched_items)
        {
            histories.push_back({std::move(expressions_best_matches), {period}, std::move(matched_items)});
            this->update_similarity();
        }
        void update_similarity()
        {
            double new_similarity{0.0};
            double best_similarity{0.0};
            std::size_t value_length_of_most_similar{0};
            for (const auto& expression : histories.back().expressions)
            {
                const auto partial_similarity = expression.sum_similarity().partial;
                new_similarity += partial_similarity * partial_similarity;
                if (best_similarity < partial_similarity)
                {
                    best_similarity = partial_similarity;
                    value_length_of_most_similar = expression.item_value_length;
                }
                else if (best_similarity == partial_similarity)
                {
                    if ((value_length_of_most_similar == 0) ||
                        (expression.item_value_length < value_length_of_most_similar))
                    {
                        value_length_of_most_similar = expression.item_value_length;
                    }
                }
            }
            if (similarity <= new_similarity)
            {
                similarity = new_similarity;
                value_length = value_length_of_most_similar;
            }
        }
    };
    std::vector<Match> matches_by_similarity{};
    matches_by_similarity.reserve(data_collector.get_sum_matches().size());
    for (const auto& record : data_collector.get_sum_matches())
    {
        std::vector<ExpressionMatch> expressions_best_matches = get_best_matches(record.item_data);
        if (all_expressions_found(expressions_best_matches))
        {
            auto matched_items = get_all_matched_items(record.item_data);
            if (matches_by_similarity.empty() || (matches_by_similarity.back().object.numeric_id != record.object.numeric_id))
            {
                Match match{record.object};
                match.append_history(std::move(expressions_best_matches), record.period, std::move(matched_items));
                matches_by_similarity.push_back(std::move(match));
            }
            else
            {
                const bool period_extends_history =
                        (matches_by_similarity.back().histories.back().periods.back().next_history_id != boost::none) &&
                        (get_raw_value_from(*matches_by_similarity.back().histories.back().periods.back().next_history_id) ==
                         get_raw_value_from(record.period.history_id));
                if (period_extends_history &&
                    all_expressions_equal(expressions_best_matches, matches_by_similarity.back().histories.back().expressions))
                {
                    matches_by_similarity.back().append_period(record.period);
                }
                else
                {
                    matches_by_similarity.back().append_history(std::move(expressions_best_matches), record.period, std::move(matched_items));
                }
            }
        }
    }
    data_collector.release_sum_matches();
    struct BySimilarityDescByLengthAsc
    {
        bool operator()(const Match& lhs, const Match& rhs) const
        {
            return std::make_tuple(rhs.similarity, lhs.value_length, rhs.object.numeric_id) <
                   std::make_tuple(lhs.similarity, rhs.value_length, lhs.object.numeric_id);
        }
    };
    std::sort(matches_by_similarity.begin(), matches_by_similarity.end(), BySimilarityDescByLengthAsc());
    const auto result_count = matches_by_similarity.size();
    if ((request.max_number_of_contacts != boost::none) &&
        (*request.max_number_of_contacts < result_count))
    {
        matches_by_similarity.erase(matches_by_similarity.begin() + *request.max_number_of_contacts, matches_by_similarity.end());
    }
    struct Result
    {
        std::vector<Match> matches;
        std::size_t result_count;
    } result{ std::move(matches_by_similarity), result_count };
    return result;
}

}//namespace Fred::Registry::Contact::{anonymous}

SearchContactReply::Data search_contact(const SearchContactRequest& request)
{
    const auto found = search_contact(make_request(request));
    SearchContactReply::Data result;
    for (const auto& match : found.matches)
    {
        SearchContactReply::Data::Result contact{};
        contact.contact_id = match.object.contact_id;
        for (const auto& history : match.histories)
        {
            contact.matched_items = history.matched_items;
        }
        result.most_similar_contacts.push_back(std::move(contact));
    }
    result.result_count.lower_estimate = found.result_count;
    result.result_count.upper_estimate = found.result_count;
    result.searched_items = request.searched_items;
    return result;
}

SearchContactHistoryReply::Data search_contact(const SearchContactHistoryRequest& request)
{
    try
    {
        const auto found = search_contact(make_request(request));
        SearchContactHistoryReply::Data result;
        for (const auto& match : found.matches)
        {
            SearchContactHistoryReply::Data::Result contact{};
            contact.object_id = match.object.contact_id;
            for (const auto& history : match.histories)
            {
                SearchContactHistoryReply::Data::Result::HistoryPeriod history_period;
                history_period.valid_from = history.periods.front().valid_from;
                history_period.valid_to = history.periods.back().valid_to;
                history_period.matched_items = history.matched_items;
                for (const auto& period : history.periods)
                {
                    history_period.contact_history_ids.push_back(period.history_id);
                }
                contact.histories.push_back(std::move(history_period));
            }
            result.results.push_back(std::move(contact));
        }
        result.result_count.lower_estimate = found.result_count;
        result.result_count.upper_estimate = found.result_count;
        result.searched_items = request.searched_items;
        return result;
    }
    catch (const ChronologyViolationError&)
    {
        throw SearchContactHistoryReply::Exception::ChronologyViolation(request);
    }
    catch (const Database::ResultFailed&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

SearchContactHistoryReply::Exception::ChronologyViolation::ChronologyViolation(const SearchContactHistoryRequest&)
    : Registry::Exception("chronology violation")
{ }

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
