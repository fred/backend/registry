/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/list_contacts.hh"

#include "src/contact/contact_common_types.hh"
#include "src/util/strong_type.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

using namespace Fred::Registry::Contact;

ListContactsReply::Data Fred::Registry::Contact::list_contacts()
{
    try
    {
        ListContactsReply::Data reply;
        const auto dbres = []()
        {
            LibFred::OperationContextCreator ctx;
            auto dbres = ctx.get_conn().exec(
                // clang-format off
                "SELECT oreg.uuid, "
                       "oreg.name, "
                       "c.name, "
                       "c.organization "
                  "FROM contact c "
                  "JOIN object_registry oreg "
                    "ON c.id = oreg.id "
                 "ORDER BY oreg.name");
                // clang-format on
            ctx.commit_transaction();
            return dbres;
        }();

        reply.contacts.reserve(dbres.size());
        for (std::size_t idx = 0; idx < dbres.size(); ++idx)
        {
            Contact::ContactLightInfo contact_light_info;
            contact_light_info.id = Util::make_strong<ContactId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
            contact_light_info.handle = Util::make_strong<ContactHandle>(static_cast<std::string>(dbres[idx][1]));
            if (!dbres[idx][2].isnull())
            {
                contact_light_info.name = static_cast<std::string>(dbres[idx][2]);
            }
            if (!dbres[idx][3].isnull())
            {
                contact_light_info.organization = static_cast<std::string>(dbres[idx][3]);
            }
            reply.contacts.push_back(std::move(contact_light_info));
        }
        return reply;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
