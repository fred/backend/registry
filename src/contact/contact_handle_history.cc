/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/contact_handle_history.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/get_contact_handle_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Contact {

ContactHandleHistoryRequest::ContactHandleHistoryRequest(const ContactHandle& _contact_handle)
    : contact_handle(_contact_handle)
{ }

std::string ContactHandleHistoryRequest::to_string() const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .finish();
}

std::string ContactLifetime::TimeSpec::to_string() const
{
    return Util::StructToString().add("contact_history_id", contact_history_id)
                                 .add("timestamp", timestamp)
                                 .finish();
}

std::string ContactLifetime::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("begin", begin)
                                 .add("end", end)
                                 .finish();
}

std::string ContactHandleHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .add("timeline", timeline)
                                 .finish();
}

ContactHandleHistoryReply::Data contact_handle_history(const ContactHandleHistoryRequest& request)
{
    try
    {
        using GetContactHandleHistory = LibFred::RegistrableObject::Contact::GetContactHandleHistory;
        LibFred::OperationContextCreator ctx;
        return fred_unwrap(GetContactHandleHistory(get_raw_value_from(request.contact_handle)).exec(ctx));
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
