/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_CONTACT_HH_62B890BF4F2026B707413E908AE8BA17//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CREATE_CONTACT_HH_62B890BF4F2026B707413E908AE8BA17

#include "src/common_types.hh"
#include "src/contact/contact_common_types.hh"
#include "src/contact/privacy_controlled.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/exceptions.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <array>
#include <set>
#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct CreateContactRequest : Util::Printable<CreateContactRequest>
{
    static constexpr auto number_of_shipping_addresses = 3;
    explicit CreateContactRequest(
            ContactHandle contact_handle,
            PrivacyControlled<std::string> name,
            PrivacyControlled<std::string> organization,
            PrivacyControlled<PlaceAddress> place,
            PrivacyControlledOptional<PhoneNumber> telephone,
            PrivacyControlledOptional<PhoneNumber> fax,
            PrivacyControlled<std::vector<EmailAddress>> emails,
            PrivacyControlled<std::vector<EmailAddress>> notify_emails,
            PrivacyControlledOptional<VatIdentificationNumber> vat_identification_number,
            PrivacyControlledOptional<ContactAdditionalIdentifier> additional_identifier,
            boost::optional<ContactAddress> mailing_address,
            boost::optional<ContactAddress> billing_address,
            std::array<boost::optional<ContactAddress>, number_of_shipping_addresses> shipping_address,
            WarningLetter warning_letter,
            boost::optional<LogEntryId> log_entry_id,
            Registrar::RegistrarHandle by_registrar);
    ContactHandle contact_handle;
    PrivacyControlled<std::string> name;
    PrivacyControlled<std::string> organization;
    PrivacyControlled<PlaceAddress> place;
    PrivacyControlledOptional<PhoneNumber> telephone;
    PrivacyControlledOptional<PhoneNumber> fax;
    PrivacyControlled<std::vector<EmailAddress>> emails;
    PrivacyControlled<std::vector<EmailAddress>> notify_emails;
    PrivacyControlledOptional<VatIdentificationNumber> vat_identification_number;
    PrivacyControlledOptional<ContactAdditionalIdentifier> additional_identifier;
    boost::optional<ContactAddress> mailing_address;
    boost::optional<ContactAddress> billing_address;
    std::array<boost::optional<ContactAddress>, number_of_shipping_addresses> shipping_address;
    WarningLetter warning_letter;
    boost::optional<LogEntryId> log_entry_id;
    Registrar::RegistrarHandle by_registrar;
    std::set<std::string> unused_discloseflags;
    std::string to_string() const;
};

struct CreateContactReply
{
    struct Data : Util::Printable<Data>
    {
        ContactId contact_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct InvalidData : Registry::Exception
        {
            explicit InvalidData(const CreateContactRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
        struct ContactAlreadyExists : Registry::Exception
        {
            explicit ContactAlreadyExists(const CreateContactRequest& src);
        };
        struct ContactHandleInProtectionPeriod : Registry::Exception
        {
            explicit ContactHandleInProtectionPeriod(const CreateContactRequest& src);
        };
        struct RegistrarDoesNotExist : Registry::Exception
        {
            explicit RegistrarDoesNotExist(const CreateContactRequest& src);
        };
    };
};

CreateContactReply::Data create_contact(const CreateContactRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CREATE_CONTACT_HH_62B890BF4F2026B707413E908AE8BA17
