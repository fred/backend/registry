/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/contact_state_history.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"
#include "src/fred_wrap.hh"

#include "libfred/db_settings.hh"
#include "libfred/registrable_object/contact/get_contact_state_history.hh"
#include "util/db/db_exceptions.hh"

namespace Fred {
namespace Registry {
namespace Contact {

ContactStateHistoryRequest::ContactStateHistoryRequest(
        const ContactId& _contact_id,
        const ContactHistoryInterval& _history)
    : contact_id(_contact_id),
      history(_history)
{ }

std::string ContactStateHistoryRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("history", history)
                                 .finish();
}

std::string ContactStateHistory::to_string() const
{
    return Util::StructToString().add("flags_names", flags_names)
                                 .add("timeline", timeline)
                                 .add("valid_to", valid_to)
                                 .finish();
}

std::string ContactStateHistory::Record::to_string() const
{
    return Util::StructToString().add("valid_from", valid_from)
                                 .add("presents", presents)
                                 .finish();
}

ContactStateHistoryReply::Data::Data(ContactId contact_id, ContactStateHistory history)
    : contact_id{std::move(contact_id)},
      history{std::move(history)}
{ }

std::string ContactStateHistoryReply::Data::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("history", history)
                                 .finish();
}

ContactStateHistoryReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ContactStateHistoryRequest& src)
    : Registry::Exception("contact " + src.to_string() + " does not exist")
{
}

ContactStateHistoryReply::Exception::InvalidHistoryInterval::InvalidHistoryInterval(
        const ContactStateHistoryRequest& src)
    : Registry::Exception("invalid contact history interval " + src.to_string())
{
}

ContactStateHistoryReply::Data contact_state_history(const ContactStateHistoryRequest& request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        using GetContactStateHistoryByUuid = LibFred::RegistrableObject::Contact::GetContactStateHistoryByUuid;
        return ContactStateHistoryReply::Data{
                request.contact_id,
                fred_unwrap(
                        GetContactStateHistoryByUuid(
                                LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::contact>(
                                        get_raw_value_from(request.contact_id)))
                                .exec(ctx, fred_wrap(request.history)))};
    }
    catch (const LibFred::RegistrableObject::Contact::GetContactStateHistoryByUuid::DoesNotExist&)
    {
        throw ContactStateHistoryReply::Exception::ContactDoesNotExist(request);
    }
    catch (const LibFred::RegistrableObject::Contact::GetContactStateHistoryByUuid::InvalidHistoryIntervalSpecification&)
    {
        throw ContactStateHistoryReply::Exception::InvalidHistoryInterval(request);
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
