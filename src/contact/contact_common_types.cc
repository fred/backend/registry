/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact/contact_common_types.hh"
#include "src/util/struct_to_string.hh"

namespace Fred {
namespace Registry {
namespace Contact {

Exception::Exception(const std::string& msg)
    : Registry::Exception(msg)
{
}

std::string ContactLightInfo::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .add("handle", handle)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .finish();
}

std::string to_string(const ContactAdditionalIdentifier& variant)
{
    return Util::Into<std::string>::from(variant);
}

std::string ContactAddress::to_string() const
{
    return Util::StructToString().add("company", company)
                                 .add("street", street)
                                 .add("city", city)
                                 .add("state_or_province", state_or_province)
                                 .add("postal_code", postal_code)
                                 .add("country_code", country_code)
                                 .finish();
}

std::string WarningLetter::to_string() const
{
    switch (preference)
    {
        case SendingPreference::to_send: return "to send";
        case SendingPreference::not_to_send: return "not to send";
        case SendingPreference::not_specified: return "not specified";
    }
    throw std::runtime_error("unknown warning letter preference");
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
