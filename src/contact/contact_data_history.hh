/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_DATA_HISTORY_HH_8BC01622328BB549DDB602B314DDB235//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_DATA_HISTORY_HH_8BC01622328BB549DDB602B314DDB235

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/contact/contact_common_types.hh"
#include "src/contact/contact_history_interval.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <map>
#include <string>
#include <vector>


namespace Fred {
namespace Registry {
namespace Contact {

struct ContactDataHistoryRequest : Util::Printable<ContactDataHistoryRequest>
{
    ContactDataHistoryRequest(
            const ContactId& contact_id,
            const ContactHistoryInterval& history);
    ContactId contact_id;
    ContactHistoryInterval history;
    std::string to_string() const;
};

struct ContactDataHistory : Util::Printable<ContactDataHistory>
{
    using TimePoint = ContactHistoryInterval::TimePoint;
    struct Record : Util::Printable<Record>
    {
        ContactHistoryId contact_history_id;
        TimePoint valid_from;
        LogEntryId log_entry_id;
        std::string to_string() const;
    };
    ContactId contact_id;
    std::vector<Record> timeline;
    boost::optional<TimePoint> valid_to;
    std::string to_string() const;
};

struct ContactDataHistoryReply
{
    struct Data : Util::Printable<Data>
    {
        ContactDataHistory history;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ContactDataHistoryRequest& request);
        };
        struct InvalidHistoryInterval : Registry::Exception
        {
            explicit InvalidHistoryInterval(const ContactDataHistoryRequest& request);
        };
    };
};

ContactDataHistoryReply::Data contact_data_history(const ContactDataHistoryRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_DATA_HISTORY_HH_8BC01622328BB549DDB602B314DDB235
