/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_COMMON_TYPES_HH_949152BB5A2556E3934B166F801B7EB0//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_COMMON_TYPES_HH_949152BB5A2556E3934B166F801B7EB0

#include "src/common_types.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"
#include "src/exceptions.hh"
#include "src/uuid.hh"

#include <boost/optional.hpp>
#include <boost/variant.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace Contact {

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

using ContactHandle = Util::StrongString<struct ContactHandle_Tag, Util::Skill::Printable>;
using NationalIdentityNumber = Util::StrongString<struct NationalIdentityNumber_Tag, Util::Skill::Printable>;
using NationalIdentityCard = Util::StrongString<struct NationalIdentityCard_Tag, Util::Skill::Printable>;
using PassportNumber = Util::StrongString<struct PassportNumber_Tag, Util::Skill::Printable>;
using SocialSecurityNumber = Util::StrongString<struct SocialSecurityNumber_Tag, Util::Skill::Printable>;
using Birthdate = Util::StrongString<struct Birthdate_Tag, Util::Skill::Printable>;

using ContactId = Uuid<struct ContactIdTag>;

using ContactHistoryId = Uuid<struct ContactHistoryIdTag>;

struct ContactLightInfo : Util::Printable<ContactLightInfo>
{
    ContactId id;
    ContactHandle handle;
    std::string name;
    std::string organization;
    std::string to_string() const;
};

using ContactAdditionalIdentifier = boost::variant<
        NationalIdentityNumber,
        NationalIdentityCard,
        PassportNumber,
        CompanyRegistrationNumber,
        SocialSecurityNumber,
        Birthdate>;

std::string to_string(const ContactAdditionalIdentifier& variant);

struct ContactAddress : Util::Printable<ContactAddress>
{
    std::string company;
    std::vector<std::string> street;
    std::string city;
    std::string state_or_province;
    PostalCode postal_code;
    CountryCode country_code;
    std::string to_string() const;
};

struct WarningLetter : Util::Printable<WarningLetter>
{
    enum class SendingPreference
    {
        to_send,
        not_to_send,
        not_specified
    };
    SendingPreference preference;
    std::string to_string() const;
};

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_COMMON_TYPES_HH_949152BB5A2556E3934B166F801B7EB0
