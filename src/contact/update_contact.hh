/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_CONTACT_HH_F89D3D2F86147D93C92075B86DB3A0CF//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_CONTACT_HH_F89D3D2F86147D93C92075B86DB3A0CF

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/contact/contact_common_types.hh"
#include "src/object/object_common_types.hh"
#include "src/util/deletable.hh"
#include "src/util/printable.hh"
#include "src/registrar/registrar_common_types.hh"

#include <boost/optional.hpp>

#include <array>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct SetEmailAddresses : Util::Printable<SetEmailAddresses>
{
    std::vector<EmailAddress> values;
    std::string to_string() const;
};

struct Privacy
{
    bool is_publicly_available;
};

constexpr decltype(auto) publicly_available_data() noexcept
{
    return Privacy{true};
}

constexpr decltype(auto) operator!(const Privacy& privacy) noexcept
{
    return Privacy{!privacy.is_publicly_available};
}

struct UpdateContactRequest : Util::Printable<UpdateContactRequest>
{
    ContactId contact_id;
    ContactHistoryId contact_history_id;

    boost::optional<std::string> set_name;
    boost::optional<Privacy> set_name_privacy;
    boost::optional<std::string> set_organization;
    boost::optional<Privacy> set_organization_privacy;
    boost::optional<PlaceAddress> set_place;
    boost::optional<Privacy> set_place_privacy;
    boost::optional<PhoneNumber> set_telephone;
    boost::optional<Privacy> set_telephone_privacy;
    boost::optional<PhoneNumber> set_fax;
    boost::optional<Privacy> set_fax_privacy;
    boost::optional<SetEmailAddresses> set_emails;
    boost::optional<Privacy> set_emails_privacy;
    boost::optional<SetEmailAddresses> set_notify_emails;
    boost::optional<Privacy> set_notify_emails_privacy;
    boost::optional<VatIdentificationNumber> set_vat_identification_number;
    boost::optional<Privacy> set_vat_identification_number_privacy;
    boost::optional<Util::Deletable<ContactAdditionalIdentifier>> set_additional_identifier;
    boost::optional<Privacy> set_additional_identifier_privacy;
    boost::optional<Util::Deletable<ContactAddress>> set_mailing_address;
    boost::optional<Util::Deletable<ContactAddress>> set_billing_address;
    static constexpr auto number_of_shipping_addresses = 3;
    std::array<boost::optional<Util::Deletable<ContactAddress>>, number_of_shipping_addresses> set_shipping_address;
    boost::optional<WarningLetter> set_warning_letter;
    boost::optional<Util::Deletable<AuthInfo>> set_auth_info;

    boost::optional<LogEntryId> log_entry_id;
    Registrar::RegistrarHandle registrar_originator;

    std::string to_string() const;
};

struct UpdateContactReply
{
    struct Data : Util::Printable<Data>
    {
        ContactHandle contact_handle;
        ContactId contact_id;
        ContactHistoryId contact_history_id;
        Object::ObjectEvents events;

        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const UpdateContactRequest& src);
        };
        struct NotCurrentVersion : Registry::Exception
        {
            explicit NotCurrentVersion(const UpdateContactRequest& src);
        };
        struct UpdateProhibited : Registry::Exception
        {
            explicit UpdateProhibited(const UpdateContactRequest& src);
        };
        struct UnauthorizedRegistrar : Registry::Exception
        {
            explicit UnauthorizedRegistrar(const UpdateContactRequest& src);
        };
        struct InvalidArgument : Registry::Exception
        {
            explicit InvalidArgument(const UpdateContactRequest& src);
        };
    };
};

UpdateContactReply::Data update_contact(const UpdateContactRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//UPDATE_CONTACT_HH_F89D3D2F86147D93C92075B86DB3A0CF
