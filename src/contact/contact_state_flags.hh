/*
 * Copyright (C) 2018-2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef CONTACT_STATE_FLAGS_HH_1F72E7B22FE1A5A94418141B65AB6021//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_STATE_FLAGS_HH_1F72E7B22FE1A5A94418141B65AB6021

#include "src/util/printable.hh"

#include "libfred/registrable_object/contact/contact_state.hh"

#include <array>
#include <sstream>
#include <string>

namespace Fred {
namespace Registry {
namespace Contact {

struct StateFlagInfo : Util::Printable<StateFlagInfo>
{
    enum class Manipulation
    {
        manual,
        automatic
    };
    enum class Visibility
    {
        external,
        internal
    };
    template <typename>
    static StateFlagInfo make_from();
    std::string to_string() const;
    std::string name;
    Manipulation how_to_set;
    Visibility visibility;
};

using ContactStateFlagsInfo = std::array<StateFlagInfo, LibFred::RegistrableObject::Contact::ContactState::number_of_flags>;

template <typename T, std::size_t number_of_items>
std::string to_string(const std::array<T, number_of_items>& value)
{
    bool the_first = true;
    std::ostringstream out;
    for (const auto& item : value)
    {
        if (the_first)
        {
            the_first = false;
            out << "{ ";
        }
        else
        {
            out << ", ";
        }
        out << item;
    }
    out << " }";
    return out.str();
}

template <typename T>
std::string to_string(const std::array<T, 0>& value)
{
    return "{ }";
}

ContactStateFlagsInfo get_contact_state_flags_info();

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_STATE_FLAGS_HH_1F72E7B22FE1A5A94418141B65AB6021
