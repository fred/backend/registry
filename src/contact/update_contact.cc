/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact/update_contact.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"
#include "src/fred_unwrap.hh"

#include "libfred/contact_verification/django_email_format.hh"
#include "libfred/notifier/enqueue_notification.hh"
#include "libfred/opcontext.hh"
#include "libfred/poll/create_poll_message.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/contact/update_contact.hh"
#include "libfred/registrar/info_registrar.hh"
#include "util/db/db_exceptions.hh"
#include "util/log/log.hh"

namespace Fred {
namespace Registry {
namespace Contact {

std::string SetEmailAddresses::to_string() const
{
    return Util::StructToString().add("values", values)
        .finish();
}

std::string UpdateContactRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .add("set_name", set_name)
                                 .add("set_name_privacy", set_name_privacy)
                                 .add("set_organization", set_organization)
                                 .add("set_organization_privacy", set_organization_privacy)
                                 .add("set_place", set_place)
                                 .add("set_place_privacy", set_place_privacy)
                                 .add("set_telephone", set_telephone)
                                 .add("set_telephone_privacy", set_telephone_privacy)
                                 .add("set_fax", set_fax)
                                 .add("set_fax_privacy", set_fax_privacy)
                                 .add("set_emails", set_emails)
                                 .add("set_emails_privacy", set_emails_privacy)
                                 .add("set_notify_emails", set_notify_emails)
                                 .add("set_notify_emails_privacy", set_notify_emails_privacy)
                                 .add("set_vat_identification_number", set_vat_identification_number)
                                 .add("set_vat_identification_number_privacy", set_vat_identification_number_privacy)
                                 .add("set_additional_identifier", set_additional_identifier)
                                 .add("set_additional_identifier_privacy", set_additional_identifier_privacy)
                                 .add("set_mailing_address", set_mailing_address)
                                 .add("set_billing_address", set_billing_address)
                                 .add("set_shipping_address1", set_shipping_address[0])
                                 .add("set_shipping_address2", set_shipping_address[1])
                                 .add("set_shipping_address3", set_shipping_address[2])
                                 .add("set_warning_letter", set_warning_letter)
                                 .add("set_auth_info", set_auth_info)
                                 .add("log_entry_id", log_entry_id)
                                 .add("registrar_originator", registrar_originator)
                                 .finish();
}

std::string UpdateContactReply::Data::to_string() const
{
    return Util::StructToString().add("contact_handle", contact_handle)
                                 .add("contact_id", contact_id)
                                 .add("contact_history_id", contact_history_id)
                                 .add("events", events)
                                 .finish();
}

UpdateContactReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const UpdateContactRequest& src)
    : Registry::Exception("contact " + Util::Into<std::string>::from(src.contact_id) + " does not exist")
{
}

UpdateContactReply::Exception::NotCurrentVersion::NotCurrentVersion(const UpdateContactRequest& src)
    : Registry::Exception(Util::Into<std::string>::from(src.contact_history_id) + " "
                          "of contact " + Util::Into<std::string>::from(src.contact_id) + " "
                          "is not current version")
{
}

UpdateContactReply::Exception::UnauthorizedRegistrar::UnauthorizedRegistrar(const UpdateContactRequest&)
    : Registry::Exception("unauthorized registrar")
{
}

UpdateContactReply::Exception::UpdateProhibited::UpdateProhibited(const UpdateContactRequest&)
    : Registry::Exception("update prohibited")
{
}

UpdateContactReply::Exception::InvalidArgument::InvalidArgument(const UpdateContactRequest&)
    : Registry::Exception("invalid argument")
{
}

namespace {

template <typename T, typename OperationSet>
void optional_set(
        const boost::optional<T>& operation,
        OperationSet&& on_set)
{
    if (operation != boost::none)
    {
        std::forward<OperationSet>(on_set)(*operation);
    }
}

struct InvalidEmail : std::exception
{
    const char* what() const noexcept override { return "invalid email format"; }
};

struct NoEmail : std::exception
{
    const char* what() const noexcept override { return "no email"; }
};

decltype(auto) join_emails(const std::vector<EmailAddress>& addresses)
{
    std::string result;
    static const auto get_valid_email = [](const EmailAddress& email)
    {
        const auto& raw_email = get_raw_value_from(email);
        static DjangoEmailFormat django_email_format{};//constructor is not constexpr and method check is not const!
        const bool email_is_valid = django_email_format.check(raw_email);
        if (!email_is_valid)
        {
            FREDLOG_INFO("email \"" + raw_email + "\" is not valid");
            throw InvalidEmail{};
        }
        return raw_email;
    };
    for (const auto& email : addresses)
    {
        if (result.empty())
        {
            result = get_valid_email(email);
        }
        else
        {
            result += "," + get_valid_email(email);
        }
    }
    return result;
}

decltype(auto) to_libfred_contact_address(const ContactAddress& src)
{
    LibFred::ContactAddress dst;
    if (!src.company.empty())
    {
        dst.company_name = src.company;
    }
    if (0 < src.street.size())
    {
        dst.street1 = src.street[0];
        if (1 < src.street.size())
        {
            dst.street2 = src.street[1];
            if (2 < src.street.size())
            {
                dst.street3 = src.street[2];
            }
        }
    }
    dst.city = src.city;
    if (!src.state_or_province.empty())
    {
        dst.stateorprovince = src.state_or_province;
    }
    dst.postalcode = get_raw_value_from(src.postal_code);
    dst.country = get_raw_value_from(src.country_code);
    return dst;
}

}//namespace Fred::Registry::Contact::{anonymous}

UpdateContactReply::Data update_contact(const UpdateContactRequest& request)
{
    try
    {
        FREDLOG_DEBUG("update_contact call");
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForUpdate ctx(ctx_provider);
        const auto contact_uuid = LibFred::RegistrableObject::make_uuid_of<LibFred::Object_Type::contact>(
                get_raw_value_from(request.contact_id));
        const auto info_contact_output = LibFred::InfoContactByUuid{contact_uuid}.exec(ctx);
        const bool contact_is_registered = info_contact_output.info_contact_data.delete_time.isnull();
        if (!contact_is_registered)
        {
            throw UpdateContactReply::Exception::ContactDoesNotExist{request};
        }
        const auto contact_history_uuid = LibFred::RegistrableObject::make_history_uuid_of<LibFred::Object_Type::contact>(
                get_raw_value_from(request.contact_history_id));
        const bool is_current_contact_data =
                info_contact_output.next_historyid.isnull() &&
                (get_raw_value_from(info_contact_output.info_contact_data.history_uuid) == get_raw_value_from(contact_history_uuid));
        if (!is_current_contact_data)
        {
            throw UpdateContactReply::Exception::NotCurrentVersion{request};
        }
        const auto originator = get_raw_value_from(request.registrar_originator);
        const auto originator_data = LibFred::InfoRegistrarByHandle{originator}.exec(ctx).info_registrar_data;
        if (info_contact_output.info_contact_data.sponsoring_registrar_handle != originator)
        {
            const auto is_system_registrar = originator_data.system;
            const bool is_allowed_to_update = !is_system_registrar.isnull() && is_system_registrar.get_value();
            if (!is_allowed_to_update)
            {
                throw UpdateContactReply::Exception::UnauthorizedRegistrar{request};
            }
        }
        LibFred::UpdateContactById update_contact_op{info_contact_output.info_contact_data.id, originator};
        optional_set(request.set_name,
                     [&](const std::string& value) { update_contact_op.set_name(value); });
        optional_set(request.set_name_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_disclosename(privacy.is_publicly_available); });
        optional_set(request.set_organization,
                     [&](const std::string& value) { update_contact_op.set_organization(value); });
        optional_set(request.set_organization_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_discloseorganization(privacy.is_publicly_available); });
        optional_set(request.set_place,
                     [&](const PlaceAddress& address)
                     {
                         LibFred::Contact::PlaceAddress place;
                         if (0 < address.street.size())
                         {
                             place.street1 = address.street[0];
                             if (1 < address.street.size())
                             {
                                 place.street2 = address.street[1];
                                 if (2 < address.street.size())
                                 {
                                     place.street3 = address.street[2];
                                 }
                             }
                         }
                         place.city = address.city;
                         if (!address.state_or_province.empty())
                         {
                             place.stateorprovince = address.state_or_province;
                         }
                         place.postalcode = get_raw_value_from(address.postal_code);
                         place.country = get_raw_value_from(address.country_code);
                         update_contact_op.set_place(place);
                     });
        optional_set(request.set_place_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_discloseaddress(privacy.is_publicly_available); });
        optional_set(request.set_telephone,
                     [&](const PhoneNumber& value) { update_contact_op.set_telephone(get_raw_value_from(value)); });
        optional_set(request.set_telephone_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_disclosetelephone(privacy.is_publicly_available); });
        optional_set(request.set_fax,
                     [&](const PhoneNumber& value) { update_contact_op.set_fax(get_raw_value_from(value)); });
        optional_set(request.set_fax_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_disclosefax(privacy.is_publicly_available); });
        optional_set(request.set_emails,
                     [&](const SetEmailAddresses& to_set)
                     {
                         if (to_set.values.empty())
                         {
                             throw NoEmail{};
                         }
                         update_contact_op.set_email(join_emails(to_set.values));
                     });
        optional_set(request.set_emails_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_discloseemail(privacy.is_publicly_available); });
        optional_set(request.set_notify_emails,
                     [&](const SetEmailAddresses& to_set)
                     {
                         if (to_set.values.empty())
                         {
                             update_contact_op.set_notifyemail(Nullable<std::string>{});
                         }
                         else
                         {
                             update_contact_op.set_notifyemail(join_emails(to_set.values));
                         }
                     });
        optional_set(request.set_notify_emails_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_disclosenotifyemail(privacy.is_publicly_available); });
        optional_set(request.set_vat_identification_number,
                     [&](const VatIdentificationNumber& value) { update_contact_op.set_vat(get_raw_value_from(value)); });
        optional_set(request.set_vat_identification_number_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_disclosevat(privacy.is_publicly_available); });
        const auto delete_personal_id = [&]()
        {
            update_contact_op.set_personal_id(Nullable<LibFred::PersonalIdUnion>{});
        };
        const auto set_personal_id = [&](const ContactAdditionalIdentifier& value)
        {
            class Visitor : public boost::static_visitor<void>
            {
            public:
                explicit Visitor(LibFred::UpdateContactById& update_contact_op) : update_contact_op_(update_contact_op) { }
                void operator()(const Contact::NationalIdentityNumber& src) const
                {
                    update_contact_op_.set_personal_id(LibFred::PersonalIdUnion::get_RC(get_raw_value_from(src)));
                }
                void operator()(const Contact::NationalIdentityCard& src) const
                {
                    update_contact_op_.set_personal_id(LibFred::PersonalIdUnion::get_OP(get_raw_value_from(src)));
                }
                void operator()(const Contact::PassportNumber& src) const
                {
                    update_contact_op_.set_personal_id(LibFred::PersonalIdUnion::get_PASS(get_raw_value_from(src)));
                }
                void operator()(const CompanyRegistrationNumber& src) const
                {
                    update_contact_op_.set_personal_id(LibFred::PersonalIdUnion::get_ICO(get_raw_value_from(src)));
                }
                void operator()(const Contact::SocialSecurityNumber& src) const
                {
                    update_contact_op_.set_personal_id(LibFred::PersonalIdUnion::get_MPSV(get_raw_value_from(src)));
                }
                void operator()(const Contact::Birthdate& src) const
                {
                    update_contact_op_.set_personal_id(LibFred::PersonalIdUnion::get_BIRTHDAY(get_raw_value_from(src)));
                }
            private:
                LibFred::UpdateContactById& update_contact_op_;
            };
            boost::apply_visitor(Visitor(update_contact_op), value);
        };
        select_operation(request.set_additional_identifier, delete_personal_id, set_personal_id);
        optional_set(request.set_additional_identifier_privacy,
                     [&](const Privacy& privacy) { update_contact_op.set_discloseident(privacy.is_publicly_available); });
        select_operation(request.set_mailing_address,
                         [&]() { update_contact_op.reset_address<LibFred::ContactAddressType::MAILING>(); },
                         [&](const ContactAddress& address_to_set)
                         {
                             const auto libfred_address_to_set = to_libfred_contact_address(address_to_set);
                             update_contact_op.set_address<LibFred::ContactAddressType::MAILING>(libfred_address_to_set);
                         });
        select_operation(request.set_billing_address,
                         [&]() { update_contact_op.reset_address<LibFred::ContactAddressType::BILLING>(); },
                         [&](const ContactAddress& address_to_set)
                         {
                             const auto libfred_address_to_set = to_libfred_contact_address(address_to_set);
                             update_contact_op.set_address<LibFred::ContactAddressType::BILLING>(libfred_address_to_set);
                         });
        select_operation(request.set_shipping_address[0],
                         [&]() { update_contact_op.reset_address<LibFred::ContactAddressType::SHIPPING>(); },
                         [&](const ContactAddress& address_to_set)
                         {
                             const auto libfred_address_to_set = to_libfred_contact_address(address_to_set);
                             update_contact_op.set_address<LibFred::ContactAddressType::SHIPPING>(libfred_address_to_set);
                         });
        select_operation(request.set_shipping_address[1],
                         [&]() { update_contact_op.reset_address<LibFred::ContactAddressType::SHIPPING_2>(); },
                         [&](const ContactAddress& address_to_set)
                         {
                             const auto libfred_address_to_set = to_libfred_contact_address(address_to_set);
                             update_contact_op.set_address<LibFred::ContactAddressType::SHIPPING_2>(libfred_address_to_set);
                         });
        select_operation(request.set_shipping_address[2],
                         [&]() { update_contact_op.reset_address<LibFred::ContactAddressType::SHIPPING_3>(); },
                         [&](const ContactAddress& address_to_set)
                         {
                             const auto libfred_address_to_set = to_libfred_contact_address(address_to_set);
                             update_contact_op.set_address<LibFred::ContactAddressType::SHIPPING_3>(libfred_address_to_set);
                         });
        optional_set(request.set_warning_letter,
                     [&](const WarningLetter& warning_letter)
                     {
                         boost::optional<Nullable<bool>> to_set;
                         switch (warning_letter.preference)
                         {
                             case WarningLetter::SendingPreference::not_to_send:
                                 to_set = Nullable<bool>{false};
                                 break;
                             case WarningLetter::SendingPreference::to_send:
                                 to_set = Nullable<bool>{true};
                                 break;
                             case WarningLetter::SendingPreference::not_specified:
                                 to_set = Nullable<bool>{};
                                 break;
                         }
                         if (to_set == boost::none)
                         {
                             FREDLOG_WARNING("unknown value of set_warning_letter flag");
                         }
                         else
                         {
                             update_contact_op.set_domain_expiration_warning_letter_enabled(*to_set);
                         }
                     });
        select_operation(request.set_auth_info,
                         [&]() { update_contact_op.set_authinfo(std::string{}); },
                         [&](const AuthInfo& value) { update_contact_op.set_authinfo(get_raw_value_from(value)); });
        const auto log_request_id = [&]()->boost::optional<unsigned long long>
        {
            if (request.log_entry_id != boost::none)
            {
                return std::stoull(get_raw_value_from(*request.log_entry_id));
            }
            return boost::none;
        }();
        if (log_request_id != boost::none)
        {
            update_contact_op.set_logd_request_id(*log_request_id);
        }
        const auto numeric_history_id = update_contact_op.exec(ctx);
        UpdateContactReply::Data result;
        result.contact_id = request.contact_id;
        result.contact_handle = Util::make_strong<ContactHandle>(info_contact_output.info_contact_data.handle);
        const auto after_update_data = LibFred::InfoContactHistoryByHistoryid(numeric_history_id).exec(ctx);
        result.contact_history_id = Util::make_strong<ContactHistoryId>(get_raw_value_from(after_update_data.info_contact_data.history_uuid));
        result.events = fred_unwrap(after_update_data.info_contact_data);
        LibFred::Poll::CreateUpdateOperationPollMessage<LibFred::Object_Type::contact>().exec(ctx, numeric_history_id);
        static constexpr auto no_log_request_id = 0ull;
        Notification::enqueue_notification(
                    ctx,
                    Notification::updated,
                    originator_data.id,
                    numeric_history_id,
                    ::Util::make_svtrid(log_request_id == boost::none ? no_log_request_id : *log_request_id));

        ctx_provider.commit_transaction();
        return result;
    }
    catch (const InvalidEmail&)
    {
        struct InvalidEmail : public UpdateContactReply::Exception::InvalidArgument
        {
            explicit InvalidEmail(const UpdateContactRequest& request)
                : UpdateContactReply::Exception::InvalidArgument{request}
            { }
            const char* what() const noexcept override { return "invalid email format"; }
        };
        throw InvalidEmail{request};
    }
    catch (const NoEmail&)
    {
        struct EmailRequired : public UpdateContactReply::Exception::InvalidArgument
        {
            explicit EmailRequired(const UpdateContactRequest& request)
                : UpdateContactReply::Exception::InvalidArgument{request}
            { }
            const char* what() const noexcept override { return "email is required"; }
        };
        throw EmailRequired{request};
    }
    catch (const UpdateContactReply::Exception::ContactDoesNotExist&)
    {
        throw;
    }
    catch (const UpdateContactReply::Exception::NotCurrentVersion&)
    {
        throw;
    }
    catch (const UpdateContactReply::Exception::UpdateProhibited&)
    {
        throw;
    }
    catch (const UpdateContactReply::Exception::UnauthorizedRegistrar&)
    {
        throw;
    }
    catch (const LibFred::InfoContactByUuid::Exception& e)
    {
        if (e.is_set_unknown_contact_uuid())
        {
            throw UpdateContactReply::Exception::ContactDoesNotExist(request);
        }
        throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoContactByUuid::Exception");
    }
    catch (const LibFred::InfoRegistrarByHandle::Exception& e)
    {
        if (e.is_set_unknown_registrar_handle())
        {
            struct UnknownRegistrar : public UpdateContactReply::Exception::UnauthorizedRegistrar
            {
                explicit UnknownRegistrar(const UpdateContactRequest& request)
                    : UpdateContactReply::Exception::UnauthorizedRegistrar{request}
                { }
                const char* what() const noexcept override { return "unknown registrar"; }
            };
            throw UnknownRegistrar{request};
        }
        FREDLOG_INFO(std::string{"LibFred::InfoRegistrarByHandle::Exception: "} + e.what());
        throw Registry::InternalServerError("unexpected problem signalized by LibFred::InfoRegistrarByHandle::Exception");
    }
    catch (const LibFred::UpdateContactById::ExceptionType& e)
    {
        if (e.is_set_unknown_country())
        {
            struct InvalidCountry : public UpdateContactReply::Exception::InvalidArgument
            {
                explicit InvalidCountry(const UpdateContactRequest& request)
                    : UpdateContactReply::Exception::InvalidArgument{request}
                { }
                const char* what() const noexcept override { return "invalid country"; }
            };
            throw InvalidCountry{request};
        }
        if (e.is_set_forbidden_company_name_setting())
        {
            struct ForbiddenCompanyNameSetting : public UpdateContactReply::Exception::InvalidArgument
            {
                explicit ForbiddenCompanyNameSetting(const UpdateContactRequest& request)
                    : UpdateContactReply::Exception::InvalidArgument{request}
                { }
                const char* what() const noexcept override { return "forbidden company name setting"; }
            };
            throw ForbiddenCompanyNameSetting{request};
        }
        if (e.is_set_unknown_registrar_handle())
        {
            struct UnknownRegistrar : public UpdateContactReply::Exception::UnauthorizedRegistrar
            {
                explicit UnknownRegistrar(const UpdateContactRequest& request)
                    : UpdateContactReply::Exception::UnauthorizedRegistrar{request}
                { }
                const char* what() const noexcept override { return "unknown registrar"; }
            };
            throw UnknownRegistrar{request};
        }
        FREDLOG_INFO(std::string{"LibFred::UpdateContactById::ExceptionType: "} + e.what());
        throw Registry::InternalServerError("unexpected problem signalized by LibFred::UpdateContactById::ExceptionType");
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_INFO(std::string{"Database::Exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_INFO(std::string{"std::exception: "} + e.what());
        throw Registry::InternalServerError("unexpected unknown exception");
    }
    catch (...)
    {
        FREDLOG_INFO("unknown exception");
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred
