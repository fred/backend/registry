WITH dc AS NOT MATERIALIZED
(
    SELECT id
      FROM object_registry
     WHERE uuid = '465dabf1-877a-4187-b6ba-4b92a6549608'::UUID
),
page_break AS MATERIALIZED
(
SELECT oreg_src.uuid,
       oreg_src.name AS handle,
       (SELECT count(foo.c)
          FROM (SELECT id AS c
                  FROM domain
                 WHERE registrant = oreg_src.id
                 UNION
                SELECT domainid AS c
                  FROM domain_contact_map
                 WHERE contactid = oreg_src.id) AS foo) AS domain_count,
       (SELECT count(ncm.nssetid)
          FROM nsset_contact_map ncm
         WHERE ncm.contactid = oreg_src.id) AS nsset_count,
       (SELECT count(kcm.keysetid)
          FROM keyset_contact_map kcm
         WHERE kcm.contactid = oreg_src.id) AS keyset_count,
       oreg_src.crdate,
       r.handle AS registrar_handle
  FROM (object_registry oreg_src
          JOIN contact c_src
            ON c_src.id = oreg_src.id
           AND oreg_src.erdate IS NULL
          JOIN object o
            ON o.id = oreg_src.id
          JOIN registrar r
            ON r.id = o.clid)
  JOIN (object_registry oreg_dst
          JOIN contact c_dst
            ON c_dst.id = oreg_dst.id
           AND oreg_dst.erdate IS NULL
           AND oreg_dst.uuid = '14b0ef91-8a62-4c82-bc93-db75b671d915'::UUID
       ) ON TRUE
  LEFT JOIN object_state os_src
    ON os_src.object_id = c_src.id
   AND os_src.state_id IN (SELECT eos.id
                             FROM enum_object_states eos
                            WHERE eos.name = 'mojeidContact'::TEXT
                               OR eos.name = 'serverDeleteProhibited'::TEXT
                               OR eos.name = 'serverBlocked'::TEXT) -- forbidden states of src contact
   AND os_src.valid_from <= CURRENT_TIMESTAMP
   AND (os_src.valid_to IS NULL OR os_src.valid_to > CURRENT_TIMESTAMP)
WHERE
(
-- the same
   (COALESCE(LOWER(REGEXP_REPLACE(c_src.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
-- if dst filled then src the same or empt
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.vat,'')) != ''::text)
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.ssn,'')) != ''::text)
OR (COALESCE(c_src.ssntype, 0) != COALESCE(c_dst.ssntype, 0) AND COALESCE(c_src.ssntype, 0) != 0)
) = FALSE
AND oreg_src.name != oreg_dst.name
AND os_src.id IS NULL
AND NOT EXISTS (SELECT
                  FROM contact_identity ci
                 WHERE ci.contact_id = oreg_src.id
                   AND ci.valid_to IS NULL)
)
SELECT obr.uuid AS contact_id,
       obr.name AS handle,
       h.uuid AS history_id,
       obr.erdate IS NOT NULL AS is_deleted,
       COUNT(*) OVER() AS full_count
  FROM (

SELECT oreg_src.id,
       oreg_src.name AS handle,
       (SELECT count(foo.c)
          FROM (SELECT id AS c
                  FROM domain
                 WHERE registrant = oreg_src.id
                 UNION
                SELECT domainid AS c
                  FROM domain_contact_map
                 WHERE contactid = oreg_src.id) AS foo) AS domain_count,
       (SELECT count(ncm.nssetid)
          FROM nsset_contact_map ncm
         WHERE ncm.contactid = oreg_src.id) AS nsset_count,
       (SELECT count(kcm.keysetid)
          FROM keyset_contact_map kcm
         WHERE kcm.contactid = oreg_src.id) AS keyset_count,
       oreg_src.crdate,
       r.handle AS registrar_handle
  FROM (object_registry oreg_src
          JOIN contact c_src
            ON c_src.id = oreg_src.id
           AND oreg_src.erdate IS NULL
          JOIN object o
            ON o.id = oreg_src.id
          JOIN registrar r
            ON r.id = o.clid)
  JOIN (object_registry oreg_dst
          JOIN dc
            ON oreg_dst.id = dc.id -- here
          JOIN contact c_dst
            ON c_dst.id = oreg_dst.id
           AND oreg_dst.erdate IS NULL
       ) ON TRUE
  LEFT JOIN object_state os_src
    ON os_src.object_id = c_src.id
   AND os_src.state_id IN (SELECT eos.id
                             FROM enum_object_states eos
                            WHERE eos.name = 'mojeidContact'::TEXT
                               OR eos.name = 'serverDeleteProhibited'::TEXT
                               OR eos.name = 'serverBlocked'::TEXT) -- forbidden states of src contact
   AND os_src.valid_from <= CURRENT_TIMESTAMP
   AND (os_src.valid_to IS NULL OR os_src.valid_to > CURRENT_TIMESTAMP)
WHERE
(
-- the same
   (COALESCE(LOWER(REGEXP_REPLACE(c_src.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.name, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.organization, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street1, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street2, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.street3, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.city, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.postalcode, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.stateorprovince, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.country, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.email, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), ''))
-- if dst filled then src the same or empt
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.vat, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.vat,'')) != ''::text)
OR (COALESCE(LOWER(REGEXP_REPLACE(c_src.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') != COALESCE(LOWER(REGEXP_REPLACE(c_dst.ssn, '^\\s+|\\s+$|(\\s)\\s+', '\\1', 'g')), '') AND trim(BOTH ' ' FROM COALESCE(c_src.ssn,'')) != ''::text)
OR (COALESCE(c_src.ssntype, 0) != COALESCE(c_dst.ssntype, 0) AND COALESCE(c_src.ssntype, 0) != 0)
) = FALSE
AND oreg_src.name != oreg_dst.name
AND os_src.id IS NULL
AND NOT EXISTS (SELECT
                  FROM contact_identity ci
                 WHERE ci.contact_id = oreg_src.id
                   AND ci.valid_to IS NULL)
) AS tmp(id, handle, domain_count, nsset_count, keyset_count, crdate, registrar_handle)
  JOIN object_registry obr ON obr.id = tmp.id
  JOIN object o ON o.id = obr.id
  JOIN history h ON h.id = obr.historyid
  JOIN registrar r ON r.id = o.clid,
       page_break pb
 WHERE ROW(pb.crdate, pb.uuid) < ROW(obr.crdate, obr.uuid)
 ORDER BY obr.crdate, 2
 LIMIT 10;
