/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_INFO_HH_AD41F5C01734A5C441BB51BA5676559A//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CONTACT_INFO_HH_AD41F5C01734A5C441BB51BA5676559A

#include "src/exceptions.hh"
#include "src/contact/contact_common_types.hh"
#include "src/contact/privacy_controlled.hh"
#include "src/object/object_common_types.hh"
#include "src/registrar/registrar_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <array>
#include <vector>

namespace Fred {
namespace Registry {
namespace Contact {

struct ContactInfoRequest : Util::Printable<ContactInfoRequest>
{
    ContactId contact_id;
    boost::optional<ContactHistoryId> contact_history_id;
    std::string to_string() const;
};

struct ContactInfoReply
{
    struct Data : Util::Printable<Data>
    {
        ContactHandle contact_handle;
        ContactId contact_id;
        ContactHistoryId contact_history_id;
        PrivacyControlled<std::string> name;
        PrivacyControlled<std::string> organization;
        PrivacyControlled<PlaceAddress> place;
        PrivacyControlledOptional<PhoneNumber> telephone;
        PrivacyControlledOptional<PhoneNumber> fax;
        PrivacyControlled<std::vector<EmailAddress>> emails;
        PrivacyControlled<std::vector<EmailAddress>> notify_emails;
        PrivacyControlledOptional<VatIdentificationNumber> vat_identification_number;
        PrivacyControlledOptional<ContactAdditionalIdentifier> additional_identifier;
        boost::optional<ContactAddress> mailing_address;
        boost::optional<ContactAddress> billing_address;
        static constexpr auto number_of_shipping_addresses = 3;
        std::array<boost::optional<ContactAddress>, number_of_shipping_addresses> shipping_address;
        WarningLetter warning_letter;
        Object::ObjectEvents representative_events;
        Registrar::RegistrarHandle sponsoring_registrar;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ContactInfoRequest& src);
        };
    };
};

ContactInfoReply::Data contact_info(const ContactInfoRequest& request);

struct ContactIdRequest : Util::Printable<ContactIdRequest>
{
    ContactHandle contact_handle;
    std::string to_string() const;
};

struct ContactIdReply
{
    struct Data : Util::Printable<Data>
    {
        ContactId contact_id;
        ContactHistoryId contact_history_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const ContactIdRequest& src);
        };
    };
};

ContactIdReply::Data get_contact_id(const ContactIdRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//CONTACT_INFO_HH_AD41F5C01734A5C441BB51BA5676559A
