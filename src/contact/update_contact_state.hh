/*
 * Copyright (C) 2020-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_CONTACT_STATE_HH_8A33152C432B5A66525BF74A8E807FD8//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define UPDATE_CONTACT_STATE_HH_8A33152C432B5A66525BF74A8E807FD8

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/contact/contact_common_types.hh"
#include "src/util/printable.hh"
#include "src/registrar/registrar_common_types.hh"

#include <boost/optional.hpp>

#include <map>
#include <string>

namespace Fred {
namespace Registry {
namespace Contact {

struct UpdateContactStateRequest : Util::Printable<UpdateContactStateRequest>
{
    ContactId contact_id;
    ContactHistoryId contact_history_id;

    std::map<std::string, bool> set_flags;

    boost::optional<LogEntryId> log_entry_id;
    boost::optional<Registrar::RegistrarHandle> originator;

    std::string to_string()const;
};

struct UpdateContactStateReply
{
    struct Data : Util::Printable<Data>
    {
        ContactHandle contact_handle;
        ContactId contact_id;
        ContactHistoryId contact_history_id;

        std::string to_string()const;
    };
    struct Exception
    {
        struct ContactDoesNotExist : Registry::Exception
        {
            explicit ContactDoesNotExist(const UpdateContactStateRequest& src);
        };
        struct NotCurrentVersion : Registry::Exception
        {
            explicit NotCurrentVersion(const UpdateContactStateRequest& src);
        };
        struct UpdateProhibited : Registry::Exception
        {
            explicit UpdateProhibited(const UpdateContactStateRequest& src);
        };
        struct UnauthorizedRegistrar : Registry::Exception
        {
            explicit UnauthorizedRegistrar(const UpdateContactStateRequest& src);
        };
    };
};

UpdateContactStateReply::Data update_contact_state(const UpdateContactStateRequest& request);

}//namespace Fred::Registry::Contact
}//namespace Fred::Registry
}//namespace Fred

#endif//UPDATE_CONTACT_STATE_HH_8A33152C432B5A66525BF74A8E807FD8
