/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLIENT_HH_FAA13CAC91E31C128C45A72B1CCE8C5C//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CLIENT_HH_FAA13CAC91E31C128C45A72B1CCE8C5C

#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <cstdint>
#include <istream>
#include <memory>
#include <string>
#include <vector>

namespace LibHttp {

struct HttpContext
{
    std::unique_ptr<boost::asio::io_context> io;
};

struct HttpsContext
{
    std::unique_ptr<boost::asio::io_context> io;
    std::unique_ptr<boost::asio::ssl::context> ssl;
};

using SocketContext = boost::variant<HttpContext, HttpsContext>;

SocketContext make_context();

SocketContext make_context(boost::asio::ssl::context::method);

struct Host
{
    std::string hostname;
    std::uint16_t port;
};

using Socket = boost::variant<
        boost::asio::ip::tcp::socket,
        std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>>;

Socket connect(SocketContext& ctx, const Host& host);

void close(Socket socket);

void set_timeout(Socket& socket, const std::chrono::microseconds& timeout);

struct Response
{
    int status;
    std::string body;
};

struct HeaderItem
{
    std::string name;
    std::string value;
};

using Header = std::vector<HeaderItem>;

Response get(
        Socket& socket,
        const std::string& hostname,
        const std::string& target,
        const Header& header = {});

Response post(
        Socket& socket,
        const std::string& hostname,
        const std::string& target,
        std::istream& body,
        const Header& header = {});

Response put(
        Socket& socket,
        const std::string& hostname,
        const std::string& target,
        std::istream& body,
        const Header& header = {});

}//namespace LibHttp

#endif//CLIENT_HH_FAA13CAC91E31C128C45A72B1CCE8C5C
