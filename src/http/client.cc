/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/http/client.hh"

#include "liblog/liblog.hh"

#include "libstrong/type.hh"

#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/certify/extensions.hpp>
#include <boost/certify/https_verification.hpp>

#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

namespace LibHttp {

namespace beast = boost::beast; // from <boost/beast.hpp>
namespace http = beast::http;   // from <boost/beast/http.hpp>
namespace net = boost::asio;    // from <boost/asio.hpp>
namespace ssl = net::ssl;       // from <boost/asio/ssl.hpp>
using tcp = net::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

namespace {

using Hostname = LibStrong::Type<std::string, struct HostnameTag_>;
using Target = LibStrong::Type<std::string, struct TargetTag_>;
using Body = LibStrong::Type<std::string, struct BodyTag_>;

auto make_request(
        http::verb method,
        const Hostname& hostname,
        const Target& target,
        const Body& body,
        const Header& header)
{
    static constexpr auto http_version = 11; // 1.1
    static constexpr auto user_agent = "Fred.Auction.Warehouse/" PACKAGE_VERSION;
    http::request<http::string_body> request{method, *target, http_version, *body};
    request.set(http::field::connection, "keep-alive");
    request.set(http::field::keep_alive, "timeout=5, max=1000");
    request.set(http::field::host, *hostname);
    request.set(http::field::user_agent, user_agent);
    request.set(http::field::content_type, "application/json");
    request.set(http::field::accept, "application/json");
    request.set(http::field::accept_charset, "utf-8");
    std::for_each(begin(header), end(header), [&request](auto&& item)
    {
        request.set(item.name, item.value);
    });
    if (!body->empty())
    {
        request.prepare_payload();
    }
    return request;
}

auto make_buffer()
{
    beast::flat_buffer buffer;
    buffer.reserve(0x1000); // sufficient size to avoid the need to allocate additional memory
    return buffer;
}

tcp::resolver::results_type resolve(
        net::io_context& ctx,
        const std::string& hostname,
        std::uint16_t port)
{
    tcp::resolver resolver{ctx};
    return resolver.resolve(hostname, std::to_string(port));
}

tcp::socket connect(net::io_context& ctx, const Host& host)
{
    tcp::socket socket{ctx};
    net::connect(socket, resolve(ctx, host.hostname, host.port));
    return socket;
}

std::unique_ptr<ssl::stream<tcp::socket>> connect(
        net::io_context& ctx,
        ssl::context& ssl_ctx,
        const Host& host)
{
    auto stream = std::make_unique<ssl::stream<tcp::socket>>(
            connect(ctx, host),
            ssl_ctx);
    // tag::stream_setup_source[]
    boost::certify::set_server_hostname(*stream, host.hostname);
    boost::certify::sni_hostname(*stream, host.hostname);
    // end::stream_setup_source[]

    stream->handshake(ssl::stream_base::handshake_type::client);
    return stream;
}

class Connect : boost::static_visitor<Socket>
{
public:
    explicit Connect(Host host)
        : host_{std::move(host)}
    { }
    Socket operator()(HttpContext& ctx) const
    {
        return Socket{connect(*ctx.io, host_)};
    }
    Socket operator()(HttpsContext& ctx) const
    {
        return Socket{connect(*ctx.io, *ctx.ssl, host_)};
    }
private:
    Host host_;
};

class Read : public boost::static_visitor<std::size_t>
{
public:
    explicit Read(beast::flat_buffer& buffer, http::response<http::string_body>& response)
        : buffer_{buffer},
          response_{response}
    { }
    std::size_t operator()(boost::asio::ip::tcp::socket& socket) const
    {
        return http::read(socket, buffer_, response_);
    }
    std::size_t operator()(std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>& socket) const
    {
        return http::read(*socket, buffer_, response_);
    }
private:
    beast::flat_buffer& buffer_;
    http::response<http::string_body>& response_;
};

class Write : public boost::static_visitor<unsigned long>
{
public:
    explicit Write(const http::request<http::string_body>& request)
        : request_{request}
    { }
    unsigned long operator()(boost::asio::ip::tcp::socket& socket) const
    {
        return http::write(socket, request_);
    }
    unsigned long operator()(std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>& socket) const
    {
        return http::write(*socket, request_);
    }
private:
    const http::request<http::string_body>& request_;
};

struct Free : boost::static_visitor<>
{
    void operator()(std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>& socket_ptr) const
    {
        // Gracefully close the stream
        beast::error_code ec;
        socket_ptr->shutdown(ec);
        if (ec == net::error::eof)
        {
            // Rationale:
            // http://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
            ec = {};
        }
        if (ec)
        {
            throw beast::system_error{ec};
        }
        socket_ptr->next_layer().close(ec);
    }
    void operator()(const boost::asio::ip::tcp::socket&) const
    { }
};

class SetTimeout : boost::static_visitor<>
{
public:
    explicit SetTimeout(const std::chrono::microseconds& timeout)
        : timeout_{.tv_sec = timeout.count() / 1'000'000, .tv_usec = timeout.count() % 1'000'000}
    { }
    void operator()(std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>& socket_ptr) const
    {
        (*this)(socket_ptr->next_layer());
    }
    void operator()(boost::asio::ip::tcp::socket& socket) const
    {
        static constexpr auto success = 0;
        if (::setsockopt(socket.native_handle(), SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<const void*>(&timeout_), sizeof(timeout_)) == success)
        {
            LIBLOG_DEBUG("setsockopt({}, SOL_SOCKET, SO_RCVTIMEO, {}.{:06d}, {})",
                         socket.native_handle(), timeout_.tv_sec, timeout_.tv_usec, sizeof(timeout_));
        }
        else
        {
            const auto c_errno = errno;
            LIBLOG_INFO("setsockopt({}, SOL_SOCKET, SO_RCVTIMEO, {}.{:06d}, {}) failure: {}",
                        socket.native_handle(), timeout_.tv_sec, timeout_.tv_usec, sizeof(timeout_), std::strerror(c_errno));
        }
#if SO_RCVTIMEO != SO_SNDTIMEO
        if (::setsockopt(socket.native_handle(), SOL_SOCKET, SO_SNDTIMEO, reinterpret_cast<const void*>(&timeout_), sizeof(timeout_)) == success)
        {
            LIBLOG_DEBUG("setsockopt({}, SOL_SOCKET, SO_SNDTIMEO, {}.{:06d}, {})",
                         socket.native_handle(), timeout_.tv_sec, timeout_.tv_usec, sizeof(timeout_));
        }
        else
        {
            const auto c_errno = errno;
            LIBLOG_INFO("setsockopt({}, SOL_SOCKET, SO_SNDTIMEO, {}.{:06d}, {}) failure: {}",
                        socket.native_handle(), timeout_.tv_sec, timeout_.tv_usec, sizeof(timeout_), std::strerror(c_errno));
        }
#endif
    }
private:
    struct ::timeval timeout_;
};

}//namespace LibHttp::{anonymous}

}//namespace LibHttp

using namespace LibHttp;

SocketContext LibHttp::make_context()
{
    return HttpContext{std::make_unique<net::io_context>()};
}

SocketContext LibHttp::make_context(boost::asio::ssl::context::method method)
{
    HttpsContext ctx{std::make_unique<net::io_context>(), std::make_unique<ssl::context>(method)};
    ctx.ssl->set_verify_mode(
            ssl::context::verify_peer |
            ssl::context::verify_fail_if_no_peer_cert);
    ctx.ssl->set_default_verify_paths();
    // tag::ctx_setup_source[]
    boost::certify::enable_native_https_server_verification(*ctx.ssl);
    // end::ctx_setup_source[]
    return ctx;
}

Socket LibHttp::connect(SocketContext& ctx, const Host& host)
{
    return boost::apply_visitor(Connect{host}, ctx);
}

Response LibHttp::get(
        Socket& socket,
        const std::string& hostname,
        const std::string& target,
        const Header& header)
{
    try
    {
        // Set up an HTTP GET request message
        auto request = make_request(http::verb::get, Hostname{hostname}, Target{target}, Body{std::string{}}, header);

        // Send the HTTP request to the remote host
        boost::apply_visitor(Write{request}, socket);

        // This buffer is used for reading and must be persisted
        auto buffer = make_buffer();

        // Declare a container to hold the response
        http::response<http::string_body> response;

        // Receive the HTTP response
        boost::apply_visitor(Read{buffer, response}, socket);

        // If we get here then the connection is closed gracefully
        return { static_cast<int>(response.result()), response.body() };
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Error: {}", e.what());
        throw;
    }
}

Response LibHttp::post(
        Socket& socket,
        const std::string& hostname,
        const std::string& target,
        std::istream& body,
        const Header& header)
{
    try
    {
        // Set up an HTTP POST request message
        auto request = make_request(
                http::verb::post,
                Hostname{hostname},
                Target{target},
                Body{std::string{std::istreambuf_iterator<char>(body), {}}},
                header);

        // Send the HTTP request to the remote host
        boost::apply_visitor(Write{request}, socket);

        // This buffer is used for reading and must be persisted
        auto buffer = make_buffer();

        // Declare a container to hold the response
        http::response<http::string_body> response;

        // Receive the HTTP response
        boost::apply_visitor(Read{buffer, response}, socket);

        // If we get here then the connection is closed gracefully
        return { static_cast<int>(response.result()), response.body() };
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Error: {}", e.what());
        throw;
    }
}

Response LibHttp::put(
        Socket& socket,
        const std::string& hostname,
        const std::string& target,
        std::istream& body,
        const Header& header)
{
    try
    {
        // Set up an HTTP PUT request message
        auto request = make_request(
                http::verb::put,
                Hostname{hostname},
                Target{target},
                Body{std::string{std::istreambuf_iterator<char>(body), {}}},
                header);

        // Send the HTTP request to the remote host
        boost::apply_visitor(Write{request}, socket);

        // This buffer is used for reading and must be persisted
        auto buffer = make_buffer();

        // Declare a container to hold the response
        http::response<http::string_body> response;

        // Receive the HTTP response
        boost::apply_visitor(Read{buffer, response}, socket);

        // If we get here then the connection is closed gracefully
        return { static_cast<int>(response.result()), response.body() };
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("Error: {}", e.what());
        throw;
    }
}

void LibHttp::close(Socket socket)
{
    boost::apply_visitor(Free{}, socket);
}

void LibHttp::set_timeout(Socket& socket, const std::chrono::microseconds& timeout)
{
    boost::apply_visitor(SetTimeout{timeout}, socket);
}
