/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain_blacklist/domain_blacklist_common_types.hh"

#include "src/util/struct_to_string.hh"

#include <utility>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

Exception::Exception(const std::string& msg)
    : Registry::Exception(msg)
{
}

InvalidUuid::InvalidUuid(const std::string& msg)
    : Exception(msg)
{
}

CreateBlockData::CreateBlockData()
    : is_regex(false)
{
}

BlockRef::BlockRef(
        BlockId _block_id,
        Fqdn _fqdn,
        bool _is_regex)
    : block_id(std::move(_block_id)),
      fqdn(std::move(_fqdn)),
      is_regex(_is_regex)
{
}

BlockRef::BlockRef()
    : is_regex(false)
{
}

std::string BlockRef::to_string() const
{
    return Util::StructToString().add("block_id", block_id)
                                 .add("fqdn", fqdn)
                                 .add("is_regex", is_regex)
                                 .finish();
}

CreateBlockData::CreateBlockData(
            std::string _fqdn,
            bool _is_regex,
            RequestedBy _requested_by,
            boost::optional<BlockId> _block_id,
            boost::optional<TimePoint> _valid_from,
            boost::optional<TimePoint> _valid_to,
            std::string _reason)
    : fqdn(std::move(_fqdn)),
      is_regex(std::move(_is_regex)),
      requested_by(std::move(_requested_by)),
      block_id(std::move(_block_id)),
      valid_from(std::move(_valid_from)),
      valid_to(std::move(_valid_to)),
      reason(std::move(_reason))
{
}

std::string CreateBlockData::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .add("is_regex", is_regex)
                                 .add("requested_by", requested_by)
                                 .add("block_id", block_id)
                                 .add("valid_from", valid_from)
                                 .add("valid_to", valid_to)
                                 .add("reason", reason)
                                 .finish();
}

BlockInfo::BlockInfo()
    : is_regex(false)
{
}

BlockInfo::BlockInfo(
            std::string _fqdn,
            bool _is_regex,
            RequestedBy _requested_by,
            BlockId _block_id,
            TimePoint _valid_from,
            boost::optional<TimePoint> _valid_to,
            std::string _reason)
    : fqdn(std::move(_fqdn)),
      is_regex(std::move(_is_regex)),
      requested_by(std::move(_requested_by)),
      block_id(std::move(_block_id)),
      valid_from(std::move(_valid_from)),
      valid_to(std::move(_valid_to)),
      reason(std::move(_reason))
{
}

std::string BlockInfo::to_string() const
{
    return Util::StructToString().add("fqdn", fqdn)
                                 .add("is_regex", is_regex)
                                 .add("requested_by", requested_by)
                                 .add("block_id", block_id)
                                 .add("valid_from", valid_from)
                                 .add("valid_to", valid_to)
                                 .add("reason", reason)
                                 .finish();
}

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred
