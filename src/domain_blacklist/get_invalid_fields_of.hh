/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_INVALID_FIELDS_OF_HH_E06EC6872D7B4A308C7E4AD4E907EC2B
#define GET_INVALID_FIELDS_OF_HH_E06EC6872D7B4A308C7E4AD4E907EC2B

#include "src/domain_blacklist/block_info.hh"
#include "src/domain_blacklist/create_block.hh"
#include "src/domain_blacklist/delete_block.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

std::set<std::string> get_invalid_fields_of(const CreateBlockRequest& _request);
std::set<std::string> get_invalid_fields_of(const DeleteBlockRequest& _request);

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

#endif
