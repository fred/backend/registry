/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DOMAIN_BLACKLIST_COMMON_TYPES_HH_1B70E2BF3DB64094B7360D096413114E
#define DOMAIN_BLACKLIST_COMMON_TYPES_HH_1B70E2BF3DB64094B7360D096413114E

#include "src/exceptions.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"
#include "src/uuid.hh"

#include <boost/uuid/uuid.hpp>
#include <boost/optional.hpp>

#include <chrono>
#include <string>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

struct InvalidUuid : Exception
{
    explicit InvalidUuid(const std::string& msg);
};

using RequestedBy = Util::StrongString<struct RequestedByTag, Util::Skill::Printable>;
using BlockId = Uuid<struct BlockIdTag>;
using Fqdn = Util::StrongString<struct FqdnTag, Util::Skill::Printable>;

struct BlockRef : Util::Printable<BlockRef>
{
    BlockId block_id;
    Fqdn fqdn;
    bool is_regex;

    BlockRef();

    BlockRef(BlockId _block_id,
            Fqdn _fqdn,
            bool _is_regex);

    std::string to_string() const;
};

struct CreateBlockData : Util::Printable<CreateBlockData>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;

    CreateBlockData();

    CreateBlockData(
            std::string _fqdn,
            bool _is_regex,
            RequestedBy _requested_by,
            boost::optional<BlockId> _block_id,
            boost::optional<TimePoint> _valid_from,
            boost::optional<TimePoint> _valid_to,
            std::string _reason);

    std::string fqdn;
    bool is_regex;
    RequestedBy requested_by;
    boost::optional<BlockId> block_id;
    boost::optional<TimePoint> valid_from;
    boost::optional<TimePoint> valid_to;
    std::string reason;

    std::string to_string() const;
};

struct BlockInfo : Util::Printable<BlockInfo>
{
    using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::nanoseconds>;

    BlockInfo();

    BlockInfo(
            std::string _fqdn,
            bool _is_regex,
            RequestedBy _requested_by,
            BlockId _block_id,
            TimePoint _valid_from,
            boost::optional<TimePoint> _valid_to,
            std::string _reason);

    std::string fqdn;
    bool is_regex;
    RequestedBy requested_by;
    BlockId block_id;
    TimePoint valid_from;
    boost::optional<TimePoint> valid_to;
    std::string reason;

    std::string to_string() const;
};

bool is_uuid_valid(const std::string& _value);

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

#endif
