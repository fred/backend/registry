/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain_blacklist/get_invalid_fields_of.hh"

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

namespace {

bool is_empty(const std::string& _value)
{
    return _value.empty() || std::all_of(begin(_value), end(_value), [](unsigned char c) { return std::isspace(c); });
}

template <typename N, template <typename> class ...Skills>
bool is_empty(const Fred::Registry::Util::StrongType<std::string, N, Skills...>& _value)
{
    return is_empty(get_raw_value_from(_value));
}

std::set<std::string> get_invalid_fields(const CreateBlockRequest& _request)
{
    std::set<std::string> invalid_fields;

    if (is_empty(_request.block.fqdn))
    {
        invalid_fields.insert("fqdn");
    }
    if (is_empty(get_raw_value_from(_request.block.requested_by)))
    {
        invalid_fields.insert("requested_by");
    }

    return invalid_fields;
}

std::set<std::string> get_invalid_fields(const DeleteBlockRequest& _request)
{
    std::set<std::string> invalid_fields;

    if (is_empty(get_raw_value_from(_request.requested_by)))
    {
        invalid_fields.insert("requested_by");
    }

    return invalid_fields;
}

} // Fred::Registry::DomainBlacklist::{anonymous}

std::set<std::string> get_invalid_fields_of(const CreateBlockRequest& _request)
{
    return get_invalid_fields(_request);
}

std::set<std::string> get_invalid_fields_of(const DeleteBlockRequest& _request)
{
    return get_invalid_fields(_request);
}

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred
