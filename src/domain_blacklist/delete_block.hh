/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETE_BLOCK_HH_7796ACF1C46E46FC8D3FF0FBBFEAE773
#define DELETE_BLOCK_HH_7796ACF1C46E46FC8D3FF0FBBFEAE773

#include "src/common_types.hh"
#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

struct DeleteBlockRequest : Util::Printable<DeleteBlockRequest>
{
    BlockId block_id;
    RequestedBy requested_by;

    std::string to_string() const;
};

struct DeleteBlockReply
{
    struct Data : Util::Printable<Data>
    {
        BlockId block_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct BlockDoesNotExist : DomainBlacklist::Exception
        {
            explicit BlockDoesNotExist(const DeleteBlockRequest& src);
        };
        struct InvalidData : DomainBlacklist::Exception
        {
            InvalidData(const DeleteBlockRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

DeleteBlockReply::Data delete_block(const DeleteBlockRequest& request);

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

#endif
