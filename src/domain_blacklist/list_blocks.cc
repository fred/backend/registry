/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain_blacklist/list_blocks.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

std::string ListBlocksReply::Data::to_string() const
{
    return Util::StructToString().add("blocks", blocks)
                                 .finish();
}

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred
  //
using namespace Fred::Registry::DomainBlacklist;

ListBlocksReply::Data Fred::Registry::DomainBlacklist::list_blocks()
{
    try
    {
        ListBlocksReply::Data reply;
        const auto dbres = []()
        {
            LibFred::OperationContextCreator ctx;
            auto dbres = ctx.get_conn().exec(
                    // clang-format off
                    "SELECT uuid, "
                           "fqdn, "
                           "is_regex "
                      "FROM domain_blacklist "
                     "WHERE valid_from <= NOW() "
                       "AND (valid_to IS NULL OR NOW() < valid_to) "
                     "ORDER BY id");
                    // clang-format on
            ctx.commit_transaction();
            return dbres;
        }();

        reply.blocks.reserve(dbres.size());
        for (std::size_t idx = 0; idx < dbres.size(); ++idx)
        {
            const auto block_id = Util::make_strong<BlockId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[idx][0])));
            auto fqdn = Util::make_strong<Fqdn>(static_cast<std::string>(dbres[idx][1]));
            const auto is_regex = static_cast<bool>(dbres[idx][2]);
            reply.blocks.emplace_back(block_id, std::move(fqdn), is_regex);
        }
        return reply;
    }
    catch (const Database::Exception& e)
    {
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        throw Registry::InternalServerError(std::string("expected unknown exception: ") + e.what());
    }
    catch (...)
    {
        throw Registry::InternalServerError("unexpected unknown exception");
    }
}
