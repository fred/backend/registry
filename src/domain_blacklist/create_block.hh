/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREATE_BLOCK_HH_76F301379B0C45D7BEBD9396A6558754
#define CREATE_BLOCK_HH_76F301379B0C45D7BEBD9396A6558754

#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/exceptions.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

struct CreateBlockRequest : Util::Printable<CreateBlockRequest>
{
    CreateBlockData block;

    std::string to_string() const;
};

struct CreateBlockReply
{
    struct Data : Util::Printable<Data>
    {
        BlockId block_id;
        std::string to_string() const;
    };
    struct Exception
    {
        struct BlockIdAlreadyExists : DomainBlacklist::Exception
        {
            explicit BlockIdAlreadyExists(const CreateBlockRequest& src);
        };
        struct InvalidData : DomainBlacklist::Exception
        {
            InvalidData(const CreateBlockRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

CreateBlockReply::Data create_block(const CreateBlockRequest& request);

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

#endif
