/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain_blacklist/block_info.hh"

#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/domain_blacklist/get_invalid_fields_of.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/optional.hpp>
#include <boost/uuid/string_generator.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

std::string BlockInfoRequest::to_string() const
{
    return Util::StructToString().add("block_id", block_id)
                                 .finish();
}

std::string BlockInfoReply::Data::to_string() const
{
    return Util::StructToString().add("block_info", block_info)
                                 .finish();
}

BlockInfoReply::Exception::BlockDoesNotExist::BlockDoesNotExist(const BlockInfoRequest& src)
    : Exception("block " + src.to_string() + " does not exist")
{
}

BlockInfoReply::Exception::InvalidData::InvalidData(const BlockInfoRequest&, std::set<std::string> fields)
    : Exception{"invalid data"},
      fields{std::move(fields)}
{
}

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::DomainBlacklist;

BlockInfoReply::Data Fred::Registry::DomainBlacklist::block_info(const BlockInfoRequest& _request)
{
    try
    {
        const auto dbres = [&_request]()
        {
            LibFred::OperationContextCreator ctx;
            auto dbres = ctx.get_conn().exec_params(
                    // clang-format off
                    "SELECT fqdn, "
                           "is_regex, "
                           "requested_by, "
                           "uuid, "
                           "valid_from, "
                           "valid_to, "
                           "reason "
                      "FROM domain_blacklist "
                     "WHERE uuid = $1::UUID",
                    // clang-format on
                    Database::QueryParams{get_raw_value_from(_request.block_id)});
            ctx.commit_transaction();
            return dbres;
        }();

        if (dbres.size() > 1)
        {
            throw DatabaseError{"too many rows"};
        }
        if (dbres.size() < 1)
        {
            throw BlockInfoReply::Exception::BlockDoesNotExist(_request);
        }

        const auto fqdn = static_cast<std::string>(dbres[0][0]);
        const auto is_regex = static_cast<bool>(dbres[0][1]);
        const auto requested_by = Util::make_strong<RequestedBy>(static_cast<std::string>(dbres[0][2]));
        const auto block_id = Util::make_strong<BlockId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[0][3])));
        const auto valid_from = static_cast<BlockInfo::TimePoint>(dbres[0][4]);
        const auto valid_to = !dbres[0][5].isnull() ? boost::optional<BlockInfo::TimePoint>{static_cast<BlockInfo::TimePoint>(dbres[0][5])} : boost::none;
        const auto reason = static_cast<std::string>(dbres[0][6]);

        BlockInfoReply::Data result;
        result.block_info =
                BlockInfo(
                        fqdn,
                        is_regex,
                        requested_by,
                        block_id,
                        valid_from,
                        valid_to,
                        reason);
        return result;
    }
    catch (const BlockInfoReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const BlockInfoReply::Exception::BlockDoesNotExist&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("unexpected std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}
