/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain_blacklist/create_block.hh"

#include "src/domain_blacklist/get_invalid_fields_of.hh"

#include "src/util/into.hh"
#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/string_generator.hpp>

#include <utility>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

std::string CreateBlockRequest::to_string() const
{
    return Util::StructToString().add("block", block)
                                 .finish();
}

std::string CreateBlockReply::Data::to_string() const
{
    return Util::StructToString().add("block_id", block_id)
                                 .finish();
}

CreateBlockReply::Exception::BlockIdAlreadyExists::BlockIdAlreadyExists(const CreateBlockRequest& src)
    : Exception{"block id " + Util::Into<std::string>::from(get_raw_value_from(*src.block.block_id)) + " already exists"}
{
}

CreateBlockReply::Exception::InvalidData::InvalidData(const CreateBlockRequest&, std::set<std::string> fields)
    : Exception{"invalid data"},
      fields{std::move(fields)}
{
}

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::DomainBlacklist;

CreateBlockReply::Data Fred::Registry::DomainBlacklist::create_block(const CreateBlockRequest& _request)
{
    try
    {
        const auto invalid_fields = get_invalid_fields_of(_request);
        if (!invalid_fields.empty())
        {
            throw CreateBlockReply::Exception::InvalidData(_request, invalid_fields);
        }

        Database::QueryParams params{
                _request.block.fqdn,
                _request.block.is_regex,
                get_raw_value_from(_request.block.requested_by),
                _request.block.reason};

        std::string query_columns{
                "fqdn, "
                "is_regex, "
                "requested_by, "
                "reason"};

        std::string query_values{
                "$1::TEXT, "
                "$2::BOOL, "
                "$3::TEXT, "
                "$4::TEXT"};

        std::string query_where{};

        if (_request.block.valid_from != boost::none)
        {
            query_columns += ", valid_from";
            params.push_back(Util::Into<std::string>::from(*_request.block.valid_from));
            query_values += ", $" + std::to_string(params.size()) + "::TIMESTAMP";
        }
        if (_request.block.valid_to != boost::none)
        {
            query_columns += ", valid_to";
            params.push_back(Util::Into<std::string>::from(*_request.block.valid_to));
            query_values += ", $" + std::to_string(params.size()) + "::TIMESTAMP";
        }
        if (_request.block.block_id != boost::none)
        {
            query_columns += ", uuid";
            params.push_back(Util::Into<std::string>::from(get_raw_value_from(*_request.block.block_id)));
            query_values += ", $" + std::to_string(params.size()) + "::UUID";
            query_where = " WHERE NOT EXISTS(SELECT FROM domain_blacklist WHERE uuid = $" + std::to_string(params.size()) + "::UUID)";
        }
        const auto query = "INSERT INTO domain_blacklist (" + query_columns + ") "
                           "SELECT " + query_values + query_where + " RETURNING uuid";

        LibFred::OperationContextCreator ctx;
        const auto dbres = ctx.get_conn().exec_params(query, params);

        if (dbres.size() == 0)
        {
            throw CreateBlockReply::Exception::BlockIdAlreadyExists(_request);
        }

        const auto block_inserted = !dbres[0][0].isnull();
        if (!block_inserted)
        {
            throw Registry::DatabaseError(std::string("Database::Exception: insert into domain_blacklist failed."));
        }

        CreateBlockReply::Data result;
        result.block_id = Util::make_strong<BlockId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[0][0])));
        ctx.commit_transaction();
        return result;
    }
    catch (const CreateBlockReply::Exception::BlockIdAlreadyExists&)
    {
        throw;
    }
    catch (const CreateBlockReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("unexpected std::exception");
    }
    catch (...)
    {
        FREDLOG_WARNING("unexpected unknow exception");
        throw InternalServerError("unexpected unknown exception");
    }
}
