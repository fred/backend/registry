/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_BLOCKS_HH_0D8DD8C166FF4E169C4242A03E73B1CB
#define LIST_BLOCKS_HH_0D8DD8C166FF4E169C4242A03E73B1CB

#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/util/printable.hh"

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

struct ListBlocksReply
{
    struct Data : Util::Printable<Data>
    {
        std::vector<BlockRef> blocks;
        std::string to_string() const;
    };
};

ListBlocksReply::Data list_blocks();

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

#endif
