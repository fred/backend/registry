/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/domain_blacklist/delete_block.hh"

#include "src/domain_blacklist/get_invalid_fields_of.hh"

#include "src/util/struct_to_string.hh"

#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"

#include <boost/uuid/string_generator.hpp>

#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

std::string DeleteBlockRequest::to_string() const
{
    return Util::StructToString().add("block_id", block_id)
                                 .add("requestd_by", requested_by)
                                 .finish();
}

std::string DeleteBlockReply::Data::to_string() const
{
    return Util::StructToString().add("block_id", block_id)
                                 .finish();
}

DeleteBlockReply::Exception::BlockDoesNotExist::BlockDoesNotExist(const DeleteBlockRequest& src)
    : Exception("block " + Util::Into<std::string>::from(get_raw_value_from(src.block_id)) +
                "requested by " + get_raw_value_from(src.requested_by) + " does not exist")
{
}

DeleteBlockReply::Exception::InvalidData::InvalidData(const DeleteBlockRequest&, std::set<std::string> fields)
    : Exception{"invalid data"},
      fields{std::move(fields)}
{
}

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::DomainBlacklist;

DeleteBlockReply::Data Fred::Registry::DomainBlacklist::delete_block(
        const DeleteBlockRequest& _request)
{
    try
    {
        const auto invalid_fields = get_invalid_fields_of(_request);
        if (!invalid_fields.empty())
        {
            throw DeleteBlockReply::Exception::InvalidData(_request, invalid_fields);
        }

        const std::string query{
                // clang-format off
                "UPDATE domain_blacklist "
                   "SET valid_to = GREATEST(NOW(), valid_from) "
                 "WHERE uuid = $1::UUID "
                   "AND requested_by = $2::TEXT "
                   "AND (valid_to IS NULL OR GREATEST(NOW(), valid_from) < valid_to) "
             "RETURNING uuid"};
                // clang-format on

        Database::QueryParams params{
            Util::Into<std::string>::from(get_raw_value_from(_request.block_id)),
            get_raw_value_from(_request.requested_by)};

        LibFred::OperationContextCreator ctx;

        const auto dbres = ctx.get_conn().exec_params(query, params);

        if (dbres.size() != 1)
        {
            throw DeleteBlockReply::Exception::BlockDoesNotExist{_request};
        }

        DeleteBlockReply::Data result;
        result.block_id = Util::make_strong<BlockId>(boost::uuids::string_generator{}(static_cast<std::string>(dbres[0][0])));
        ctx.commit_transaction();
        return result;
    }
    catch (const DeleteBlockReply::Exception::BlockDoesNotExist&)
    {
        throw;
    }
    catch (const CreateBlockReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
