/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BLOCK_INFO_HH_C9A988145498417689D7C9378B6595C9
#define BLOCK_INFO_HH_C9A988145498417689D7C9378B6595C9

#include "src/domain_blacklist/domain_blacklist_common_types.hh"
#include "src/util/printable.hh"

namespace Fred {
namespace Registry {
namespace DomainBlacklist {

struct BlockInfoRequest : Util::Printable<BlockInfoRequest>
{
    BlockId block_id;
    std::string to_string() const;
};

struct BlockInfoReply
{
    struct Data : Util::Printable<Data>
    {
        BlockInfo block_info;
        std::string to_string() const;
    };
    struct Exception
    {
        struct BlockDoesNotExist : DomainBlacklist::Exception
        {
            explicit BlockDoesNotExist(const BlockInfoRequest& src);
        };
        struct InvalidData : DomainBlacklist::Exception
        {
            InvalidData(const BlockInfoRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

BlockInfoReply::Data block_info(const BlockInfoRequest& request);

} // namespace Fred::Registry::DomainBlacklist
} // namespace Fred::Registry
} // namespace Fred

#endif
