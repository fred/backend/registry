/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact_representative/contact_representative_common_types.hh"
#include "src/util/struct_to_string.hh"

#include <boost/uuid/uuid_generators.hpp>

#include <utility>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

Exception::Exception(const std::string& msg)
    : Registry::Exception(msg)
{
}

InvalidUuid::InvalidUuid(const std::string& msg)
    : Exception(msg)
{
}

std::string to_string(const ContactRepresentativeRef& variant)
{
    return Util::Into<std::string>::from(variant);
}

ContactRepresentativeInfo::ContactRepresentativeInfo(
        ContactRepresentativeId _id,
        ContactRepresentativeHistoryId _history_id,
        ContactId _contact_id,
        std::string _name,
        std::string _organization,
        EmailAddress _email,
        PhoneNumber _telephone,
        PlaceAddress _place)
    : id(std::move(_id)),
      history_id(std::move(_history_id)),
      contact_id(std::move(_contact_id)),
      name(std::move(_name)),
      organization(std::move(_organization)),
      email(std::move(_email)),
      telephone(std::move(_telephone)),
      place(std::move(_place))
{
}

std::string ContactRepresentativeInfo::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .add("history_id", history_id)
                                 .add("contact_id", contact_id)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .add("email", email)
                                 .add("telephone", telephone)
                                 .add("place", place)
                                 .finish();
}

void check_uuid_validity(const std::string& _value)
{
    try
    {
        const auto uuid = boost::uuids::string_generator()(_value); 
        if (uuid.version() == boost::uuids::uuid::version_unknown)
        {
            throw InvalidUuid{"invalid uuid " + _value};
        }
    }
    catch (...)
    {
        throw InvalidUuid{"invalid uuid " + _value};
    }
}

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred
