/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_REPRESENTATIVE_INFO_HH_3F4BC1283E554B258B8D97A69480AD34
#define CONTACT_REPRESENTATIVE_INFO_HH_3F4BC1283E554B258B8D97A69480AD34

#include "src/contact_representative/contact_representative_common_types.hh"
#include "src/util/printable.hh"

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

struct ContactRepresentativeInfoRequest : Util::Printable<ContactRepresentativeInfoRequest>
{
    ContactRepresentativeRef contact_representative_ref;
    std::string to_string() const;
};

struct ContactRepresentativeInfoReply
{
    struct Data : Util::Printable<Data>
    {
        ContactRepresentativeInfo contact_representative;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactRepresentativeDoesNotExist : ContactRepresentative::Exception
        {
            explicit ContactRepresentativeDoesNotExist(const ContactRepresentativeInfoRequest& src);
        };
        struct ContactDoesNotExist : ContactRepresentative::Exception
        {
            explicit ContactDoesNotExist(const ContactRepresentativeInfoRequest& src);
        };
    };
};

ContactRepresentativeInfoReply::Data contact_representative_info(const ContactRepresentativeInfoRequest& request);


} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

#endif
