/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_INVALID_FIELDS_OF_HH_51D3EFB7AB9042EFA67F8114E4282D92
#define GET_INVALID_FIELDS_OF_HH_51D3EFB7AB9042EFA67F8114E4282D92

#include "src/contact_representative/create_contact_representative.hh"
#include "src/contact_representative/update_contact_representative.hh"

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

std::set<std::string> get_invalid_fields_of(const CreateContactRepresentativeRequest& _request);
std::set<std::string> get_invalid_fields_of(const UpdateContactRepresentativeRequest& _request);

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

#endif
