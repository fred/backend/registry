/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_REPRESENTATIVE_COMMON_TYPES_HH_FF0AE763180342378AB74993FD6FE818
#define CONTACT_REPRESENTATIVE_COMMON_TYPES_HH_FF0AE763180342378AB74993FD6FE818

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/util/printable.hh"
#include "src/util/strong_type.hh"
#include "src/uuid.hh"

#include <boost/variant.hpp>

#include <string>
#include <vector>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

struct Exception : Registry::Exception
{
    explicit Exception(const std::string& msg);
};

struct InvalidUuid : Exception
{
    explicit InvalidUuid(const std::string& msg);
};

using ContactRepresentativeId = Util::StrongString<struct ContactRepresentativeIdTag, Util::Skill::Printable>;
using ContactRepresentativeHistoryId = Util::StrongString<struct ContactRepresentativeHistoryIdTag, Util::Skill::Printable>;
using ContactId = Util::StrongString<struct ContactIdTag, Util::Skill::Printable>;

using ContactRepresentativeRef = boost::variant<
        ContactRepresentativeId,
        ContactRepresentativeHistoryId,
        ContactId>;

std::string to_string(const ContactRepresentativeRef& variant);

struct ContactRepresentativeInfo : Util::Printable<ContactRepresentativeInfo>
{
    ContactRepresentativeInfo() = default;

    ContactRepresentativeInfo(
            ContactRepresentativeId _id,
            ContactRepresentativeHistoryId _history_id,
            ContactId _contact_id,
            std::string _name,
            std::string _organization,
            EmailAddress _email,
            PhoneNumber _telephone,
            PlaceAddress _place);

    ContactRepresentativeId id;
    ContactRepresentativeHistoryId history_id;
    ContactId contact_id;
    std::string name;
    std::string organization;
    EmailAddress email;
    PhoneNumber telephone;
    PlaceAddress place;

    std::string to_string() const;
};

void check_uuid_validity(const std::string& _value);

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

#endif
