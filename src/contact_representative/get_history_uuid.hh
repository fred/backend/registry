/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_HISTORY_UUID_HH_932F326C744A47AAA36837FDF44B1B21
#define GET_HISTORY_UUID_HH_932F326C744A47AAA36837FDF44B1B21

#include "src/contact_representative/contact_representative_common_types.hh"

#include "libfred/opcontext.hh"

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

ContactRepresentativeHistoryId get_history_uuid(
        const LibFred::OperationContext& _ctx,
        unsigned long long _history_id);

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

#endif
