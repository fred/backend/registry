/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact_representative/get_history_uuid.hh"

#include "src/util/strong_type.hh"

#include <boost/lexical_cast.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

ContactRepresentativeHistoryId get_history_uuid(
        const LibFred::OperationContext& _ctx,
        unsigned long long _history_id)
{
    const std::string query{
            // clang-format off
            "SELECT history_uuid "
              "FROM contact_representative_history "
             "WHERE history_id = $1::BIGINT"};
           // clang-format on
    Database::QueryParams params{_history_id};
    const auto dbres = _ctx.get_conn().exec_params(query, params);
    const auto history_uuid = Fred::Registry::Util::make_strong<ContactRepresentativeHistoryId>(static_cast<std::string>(dbres[0][0]));
    return history_uuid;
}

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred
