/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact_representative/delete_contact_representative.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/opcontext.hh"
#include "util/db/db_exceptions.hh"


#include <algorithm>
#include <utility>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

std::string DeleteContactRepresentativeRequest::to_string() const
{
    return Util::StructToString().add("id", id)
                                 .finish();
}

DeleteContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist::ContactRepresentativeDoesNotExist(const DeleteContactRepresentativeRequest& src)
    : Exception("contact representative " + Util::Into<std::string>::from(get_raw_value_from(src.id)) + " does not exist")
{
}

struct DeleteFailed : Exception
{
    explicit DeleteFailed(const std::string& msg)
        : Exception(msg)
    {
    }
};

// trigger deletes the record when only log_entry_delete_id is updated
void delete_contact_representative(
        const LibFred::OperationContext& _ctx,
        const ContactRepresentativeId& _contact_representative_id,
        const LogEntryId& _log_entry_id)
{
        const std::string query{
                // clang-format off
                "WITH c AS "
                "("
                    "SELECT id "
                      "FROM contact_representative "
                     "WHERE uuid = $2::UUID"
                "), "
                "d AS "
                "("
                    "UPDATE contact_representative "
                       "SET log_entry_delete_id = $1::TEXT "
                      "FROM c "
                     "WHERE contact_representative.id = c.id"
                ") "
                "SELECT "
                  "FROM c"};
                // clang-format on

        Database::QueryParams params{
            get_raw_value_from(_log_entry_id),
            get_raw_value_from(_contact_representative_id)};

        const auto dbres = _ctx.get_conn().exec_params(query, params);

        if (dbres.size() != 1)
        {
            throw DeleteFailed{"update of log_entry_delete id to delete contact representative failed"};
        }
}

void delete_contact_representative(
        const LibFred::OperationContext& _ctx,
        const ContactRepresentativeId& _contact_representative_id)
{
        const std::string query{
                // clang-format off
                "DELETE FROM contact_representative "
                 "WHERE uuid = $1::UUID"};
                // clang-format on

        Database::QueryParams params{get_raw_value_from(_contact_representative_id)};

        const auto dbres = _ctx.get_conn().exec_params(query, params);

        if (dbres.rows_affected() != 1)
        {
            throw DeleteFailed{"delete contact representative failed"};
        }
}

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::ContactRepresentative;

void Fred::Registry::ContactRepresentative::delete_contact_representative(
        const DeleteContactRepresentativeRequest& _request)
{
    try
    {
        check_uuid_validity(get_raw_value_from(_request.id));
    }
    catch (const InvalidUuid&)
    {
        throw DeleteContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist{_request};
    }

    try
    {
        LibFred::OperationContextCreator ctx;

        if (_request.log_entry_id != boost::none)
        {
            delete_contact_representative(ctx, _request.id, *_request.log_entry_id);
        }
        else
        {
            delete_contact_representative(ctx, _request.id);
        }
        ctx.commit_transaction();
    }
    catch (const DeleteFailed&)
    {
        throw DeleteContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist{_request};
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception std::exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unknown exception");
    }
}
