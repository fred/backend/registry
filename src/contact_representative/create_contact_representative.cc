/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact_representative/create_contact_representative.hh"
#include "src/contact_representative/get_history_uuid.hh"
#include "src/contact_representative/get_invalid_fields_of.hh"

#include "src/util/strong_type.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "util/db/db_exceptions.hh"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_generators.hpp>

#include <algorithm>
#include <cctype>
#include <numeric>
#include <utility>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

std::string CreateContactRepresentativeRequest::to_string() const
{
    return Util::StructToString().add("contact_id", contact_id)
                                 .add("name", name)
                                 .add("organization", organization)
                                 .add("place", place)
                                 .add("telephone", telephone)
                                 .add("email", email)
                                 .finish();
}

std::string CreateContactRepresentativeReply::Data::to_string() const
{
    return Util::StructToString().add("contact_representative", contact_representative)
                                 .finish();
}

CreateContactRepresentativeReply::Exception::ContactRepresentativeAlreadyExists::ContactRepresentativeAlreadyExists(const CreateContactRepresentativeRequest& src)
    : Exception{"contact representative of contact " + Util::Into<std::string>::from(src.contact_id) + " already exists"}
{
}

CreateContactRepresentativeReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const CreateContactRepresentativeRequest& src)
    : Exception("contact " + Util::Into<std::string>::from(src.contact_id) + " does not exist")
{
}

CreateContactRepresentativeReply::Exception::InvalidData::InvalidData(const CreateContactRepresentativeRequest&, std::set<std::string> fields)
    : Exception{"invalid data"},
      fields{std::move(fields)}
{
}

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::ContactRepresentative;

CreateContactRepresentativeReply::Data Fred::Registry::ContactRepresentative::create_contact_representative(const CreateContactRepresentativeRequest& _request)
{
    try
    {
        LibFred::OperationContextCreator ctx_provider;
        LibFred::OperationContextLockingForShare ctx(ctx_provider);

        try
        {
            check_uuid_validity(get_raw_value_from(_request.contact_id));
        }
        catch (const InvalidUuid&)
        {
            throw CreateContactRepresentativeReply::Exception::ContactDoesNotExist(_request);
        }

        const auto invalid_fields = get_invalid_fields_of(_request);
        if (!invalid_fields.empty())
        {
            throw CreateContactRepresentativeReply::Exception::InvalidData(_request, invalid_fields);
        }

        const auto query = std::string{
                // clang-format off
                "WITH existing_record AS "
                "("
                    "SELECT uuid, history_id "
                      "FROM contact_representative "
                     "WHERE contact_id = ("
                        "SELECT oreg.id "
                          "FROM object_registry oreg "
                         "WHERE oreg.uuid = $1::UUID "
                           "AND oreg.type = get_object_type_id('contact') "
                           "AND oreg.erdate IS NULL)"
                "), "
                "new_record AS "
                "("
                    "INSERT INTO contact_representative ("
                        "contact_id, "
                        "name, "
                        "organization, "
                        "street1, "
                        "street2, "
                        "street3, "
                        "city, "
                        "state_or_province, "
                        "postal_code, "
                        "country_code, "
                        "telephone, "
                        "email, "
                        "log_entry_id) "
                    "SELECT oreg.id, "
                           "$2::TEXT, "
                           "$3::TEXT, "
                           "$4::TEXT, "
                           "$5::TEXT, "
                           "$6::TEXT, "
                           "$7::TEXT, "
                           "$8::TEXT, "
                           "$9::TEXT, "
                           "$10::TEXT, "
                           "$11::TEXT, "
                           "$12::TEXT, "}
                           + (_request.log_entry_id != boost::none ? "$13::TEXT" : "NULL") + " "
                      "FROM object_registry oreg "
                     "WHERE oreg.uuid = $1::UUID "
                       "AND oreg.type = get_object_type_id('contact') "
                       "AND oreg.erdate IS NULL "
                       "AND NOT EXISTS(SELECT FROM existing_record) "
                 "RETURNING uuid, history_id"
                ") "
                "SELECT EXISTS(SELECT FROM existing_record), (SELECT uuid FROM new_record), (SELECT history_id FROM new_record)";
                // clang-format on
        Database::QueryParams params{
                get_raw_value_from(_request.contact_id),
                _request.name,
                _request.organization,
                _request.place.street[0],
                _request.place.street.size() > 1 ? _request.place.street[1] : "",
                _request.place.street.size() > 2 ? _request.place.street[2] : "",
                _request.place.city,
                _request.place.state_or_province,
                get_raw_value_from(_request.place.postal_code),
                get_raw_value_from(_request.place.country_code),
                get_raw_value_from(_request.telephone),
                get_raw_value_from(_request.email)};
        if (_request.log_entry_id != boost::none)
        {
            params.push_back(get_raw_value_from(*_request.log_entry_id));
        }

        const auto dbres = ctx_provider.get_conn().exec_params(query, params);

        const auto contact_representative_already_exists = static_cast<bool>(dbres[0][0]);
        if (contact_representative_already_exists)
        {
            throw CreateContactRepresentativeReply::Exception::ContactRepresentativeAlreadyExists(_request);
        }

        const auto contact_representative_inserted = !dbres[0][1].isnull();
        if (!contact_representative_inserted)
        {
            throw CreateContactRepresentativeReply::Exception::ContactDoesNotExist(_request);
        }

        const auto uuid = Fred::Registry::Util::make_strong<ContactRepresentativeId>(static_cast<std::string>(dbres[0][1]));
        const auto history_id = boost::lexical_cast<unsigned long long>(static_cast<std::string>(dbres[0][2]));
        CreateContactRepresentativeReply::Data result;
        result.contact_representative =
                ContactRepresentativeInfo(
                        uuid,
                        get_history_uuid(ctx_provider, history_id),
                        _request.contact_id,
                        _request.name,
                        _request.organization,
                        _request.email,
                        _request.telephone,
                        _request.place);
        ctx_provider.commit_transaction();
        return result;
    }
    catch (const CreateContactRepresentativeReply::Exception::ContactRepresentativeAlreadyExists&)
    {
        throw;
    }
    catch (const CreateContactRepresentativeReply::Exception::ContactDoesNotExist&)
    {
        throw;
    }
    catch (const CreateContactRepresentativeReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception std::exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("unexpected std::exception");
    }
    catch (...)
    {
        FREDLOG_WARNING("unexpected unknow exception");
        throw InternalServerError("unexpected unknown exception");
    }
}
