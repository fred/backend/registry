/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DELETE_CONTACT_REPRESENTATIVE_HH_9D80AB9F42984050B8D46F7ED16334B0
#define DELETE_CONTACT_REPRESENTATIVE_HH_9D80AB9F42984050B8D46F7ED16334B0

#include "src/common_types.hh"
#include "src/contact_representative/contact_representative_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <string>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

struct DeleteContactRepresentativeRequest : Util::Printable<DeleteContactRepresentativeRequest>
{
    ContactRepresentativeId id;

    boost::optional<LogEntryId> log_entry_id;

    std::string to_string() const;
};

struct DeleteContactRepresentativeReply
{
    struct Exception
    {
        struct ContactRepresentativeDoesNotExist : ContactRepresentative::Exception
        {
            explicit ContactRepresentativeDoesNotExist(const DeleteContactRepresentativeRequest& src);
        };
    };
};

void delete_contact_representative(const DeleteContactRepresentativeRequest& request);

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

#endif
