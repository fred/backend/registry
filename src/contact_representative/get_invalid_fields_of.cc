/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/contact_representative/get_invalid_fields_of.hh"

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

namespace {

bool is_empty(const std::string& _value)
{
    return _value.empty() || std::all_of(begin(_value), end(_value), [](unsigned char c) { return std::isspace(c); });
}

template <typename N, template <typename> class ...Skills>
bool is_empty(const Fred::Registry::Util::StrongType<std::string, N, Skills...>& _value)
{
    return is_empty(get_raw_value_from(_value));
}

template <typename ContactRepresentativeRequest>
std::set<std::string> get_invalid_fields(const ContactRepresentativeRequest& _request)
{
    std::set<std::string> invalid_fields;

    if (is_empty(_request.name) && is_empty(_request.organization))
    {
        invalid_fields.insert("name");
        invalid_fields.insert("organization");
    }
    if (is_empty(_request.email))
    {
        invalid_fields.insert("email");
    }
    static constexpr auto max_number_of_streets = 3u;
    if ((_request.place.street.size() < 1) || (max_number_of_streets < _request.place.street.size()))
    {
        invalid_fields.insert("place.street");
    }
    else
    {
        const auto some_street_empty =
                std::any_of(begin(_request.place.street), end(_request.place.street), [](auto&& item) {
                    return is_empty(item);
                });
        if (some_street_empty)
        {
            invalid_fields.insert("place.street");
        }
    }
    if (is_empty(_request.place.city))
    {
        invalid_fields.insert("place.city");
    }
    if (is_empty(_request.place.postal_code))
    {
        invalid_fields.insert("place.postal_code");
    }
    if (is_empty(_request.place.country_code))
    {
        invalid_fields.insert("place.country_code");
    }

    return invalid_fields;
}

} // Fred::Registry::ContactRepresentative::{anonymous}

std::set<std::string> get_invalid_fields_of(const CreateContactRepresentativeRequest& _request)
{
    return get_invalid_fields(_request);
}

std::set<std::string> get_invalid_fields_of(const UpdateContactRepresentativeRequest& _request)
{
    return get_invalid_fields(_request);
}

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred
