/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact_representative/contact_representative_info.hh"

#include "src/util/into.hh"
#include "src/util/struct_to_string.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "util/db/db_exceptions.hh"

#include <boost/variant/static_visitor.hpp>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

std::string ContactRepresentativeInfoRequest::to_string() const
{
    return Util::StructToString().add("contact_representative_ref", contact_representative_ref)
                                 .finish();
}

std::string ContactRepresentativeInfoReply::Data::to_string() const
{
    return Util::StructToString().add("contact_representative", contact_representative)
                                 .finish();
}

ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist::ContactRepresentativeDoesNotExist(const ContactRepresentativeInfoRequest& src)
    : Exception("contact_representative " + src.to_string() + " does not exist")
{
}

ContactRepresentativeInfoReply::Exception::ContactDoesNotExist::ContactDoesNotExist(const ContactRepresentativeInfoRequest& src)
    : Exception("contact in " + src.to_string() + " does not exist")
{
}

class GetContactRepresentativeInfoSql : boost::static_visitor<>
{
public:
    explicit GetContactRepresentativeInfoSql(std::string& _query, Database::QueryParams& _params)
        : query_{_query},
          params_{_params}
    { }

    void operator()(const ContactRepresentativeId& _contact_representative_id) const
    {
        const auto raw_value = get_raw_value_from(_contact_representative_id);
        check_uuid_validity(raw_value);
        query_ = query_begin_ + std::string{"AND crh.uuid = $1::UUID"};
        params_.push_back(raw_value);
    }

    void operator()(const ContactRepresentativeHistoryId& _contact_representative_history_id) const
    {
        const auto raw_value = get_raw_value_from(_contact_representative_history_id);
        check_uuid_validity(raw_value);
        query_ = query_begin_ + std::string{"AND crh.history_uuid = $1::UUID"};
        params_.push_back(raw_value);
    }

    void operator()(const ContactId& _contact_id) const
    {
        const auto raw_value = get_raw_value_from(_contact_id);
        check_uuid_validity(raw_value);
        query_ = query_begin_ + std::string{"AND oreg.uuid = $1::UUID"};
        params_.push_back(raw_value);
    }

private:
    static constexpr auto query_begin_{
            // clang-format off
            "SELECT crh.uuid, "
                   "crh.history_uuid,"
                   "oreg.uuid, "
                   "crh.name, "
                   "crh.organization, "
                   "crh.street1, "
                   "crh.street2, "
                   "crh.street3, "
                   "crh.city, "
                   "crh.state_or_province, "
                   "crh.postal_code, "
                   "crh.country_code, "
                   "crh.telephone, "
                   "crh.email "
              "FROM contact_representative_history crh "
              "JOIN object_registry oreg "
                "ON oreg.id = crh.contact_id "
               "AND oreg.type = get_object_type_id('contact'::TEXT) "
               "AND oreg.erdate IS NULL "
             "WHERE crh.valid_to IS NULL "};
            // clang-format on
    std::string& query_;
    Database::QueryParams& params_;
};

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::ContactRepresentative;

ContactRepresentativeInfoReply::Data Fred::Registry::ContactRepresentative::contact_representative_info(const ContactRepresentativeInfoRequest& _request)
{
    try
    {
        LibFred::OperationContextCreator ctx;
        std::string query;
        Database::QueryParams params;

        try
        {
            boost::apply_visitor(GetContactRepresentativeInfoSql{query, params}, _request.contact_representative_ref);
        }
        catch (const InvalidUuid&)
        {
            throw ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist(_request);
        }

        const auto dbres = ctx.get_conn().exec_params(query, params);
        if (dbres.size() < 1)
        {
            throw ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist(_request);
        }

        PlaceAddress place_address;
        const auto uuid = Fred::Registry::Util::make_strong<ContactRepresentativeId>(static_cast<std::string>(dbres[0][0]));
        const auto history_uuid = Fred::Registry::Util::make_strong<ContactRepresentativeHistoryId>(static_cast<std::string>(dbres[0][1]));
        const auto contact_uuid = Fred::Registry::Util::make_strong<ContactId>(static_cast<std::string>(dbres[0][2]));
        const auto name = static_cast<std::string>(dbres[0][3]);
        const auto organization = static_cast<std::string>(dbres[0][4]);
        place_address.street.push_back(static_cast<std::string>(dbres[0][5]));
        if (!dbres[0][6].isnull())
        {
            place_address.street.push_back(static_cast<std::string>(dbres[0][6]));
            if (!dbres[0][7].isnull())
            {
                place_address.street.push_back(static_cast<std::string>(dbres[0][7]));
            }
        }
        place_address.city = dbres[0][8].isnull() ? "" : static_cast<std::string>(dbres[0][8]);
        place_address.state_or_province = dbres[0][9].isnull() ? "" : static_cast<std::string>(dbres[0][9]);
        place_address.postal_code = Fred::Registry::Util::make_strong<PostalCode>(static_cast<std::string>(dbres[0][10]));
        place_address.country_code = Fred::Registry::Util::make_strong<CountryCode>(static_cast<std::string>(dbres[0][11]));
        const auto telephone = Fred::Registry::Util::make_strong<PhoneNumber>(dbres[0][12].isnull() ? "" : static_cast<std::string>(dbres[0][12]));
        const auto email = Fred::Registry::Util::make_strong<EmailAddress>(static_cast<std::string>(dbres[0][13]));

        ContactRepresentativeInfoReply::Data result;
        result.contact_representative =
                ContactRepresentativeInfo(
                        uuid,
                        history_uuid,
                        contact_uuid,
                        name,
                        organization,
                        email,
                        telephone,
                        place_address);
        ctx.commit_transaction();
        return result;
    }
    catch (const InvalidUuid&)
    {
        throw ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist(_request);
    }
    catch (const ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception std::exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("unexpected std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}
