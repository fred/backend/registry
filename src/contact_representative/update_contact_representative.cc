/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/contact_representative/update_contact_representative.hh"
#include "src/contact_representative/get_history_uuid.hh"
#include "src/contact_representative/get_invalid_fields_of.hh"

#include "libfred/db_settings.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "util/db/db_exceptions.hh"

#include "src/util/struct_to_string.hh"

#include <boost/uuid/uuid_generators.hpp>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

std::string UpdateContactRepresentativeRequest::to_string() const
{
    return Util::StructToString().add("name", name)
                                 .add("organization", organization)
                                 .add("place", place)
                                 .add("telephone", telephone)
                                 .add("email", email)
                                 .finish();
}

std::string UpdateContactRepresentativeReply::Data::to_string() const
{
    return Util::StructToString().add("contact_representative", contact_representative)
                                 .finish();
}

UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist::ContactRepresentativeDoesNotExist(const UpdateContactRepresentativeRequest& src)
    : Exception("contact representative " + Util::Into<std::string>::from(get_raw_value_from(src.id)) + " does not exist")
{
}

UpdateContactRepresentativeReply::Exception::InvalidData::InvalidData(const UpdateContactRepresentativeRequest&, std::set<std::string> fields)
    : Exception("invalid data"),
      fields{std::move(fields)}
{
}

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

using namespace Fred::Registry::ContactRepresentative;

UpdateContactRepresentativeReply::Data Fred::Registry::ContactRepresentative::update_contact_representative(const UpdateContactRepresentativeRequest& _request)
{
    try
    {
        try
        {
            check_uuid_validity(get_raw_value_from(_request.id));
        }
        catch (const InvalidUuid&)
        {
            throw UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist(_request);
        }

        LibFred::OperationContextCreator ctx;

        const auto invalid_fields = get_invalid_fields_of(_request);
        if (!invalid_fields.empty())
        {
            throw UpdateContactRepresentativeReply::Exception::InvalidData(_request, invalid_fields);
        }

        const auto query = std::string{
                // clang-format off
                "UPDATE contact_representative "
                   "SET name = $2::TEXT, "
                       "organization = $3::TEXT, "
                       "street1 = $4::TEXT, "
                       "street2 = $5::TEXT, "
                       "street3 = $6::TEXT, "
                       "city = $7::TEXT, "
                       "state_or_province = $8::TEXT, "
                       "postal_code = $9::TEXT, "
                       "country_code = $10::TEXT, "
                       "telephone = $11::TEXT, "
                       "email = $12::TEXT, "}
                       + (_request.log_entry_id != boost::none ? "log_entry_id = $13::TEXT " : "log_entry_id = NULL") + " "
                 "WHERE uuid = $1::UUID "
             "RETURNING uuid, history_id, (SELECT uuid FROM object_registry WHERE id = contact_id)";
                // clang-format on
        Database::QueryParams params{
                get_raw_value_from(_request.id),
                _request.name,
                _request.organization,
                _request.place.street[0],
                _request.place.street.size() > 1 ? _request.place.street[1] : "",
                _request.place.street.size() > 2 ? _request.place.street[2] : "",
                _request.place.city,
                _request.place.state_or_province,
                get_raw_value_from(_request.place.postal_code),
                get_raw_value_from(_request.place.country_code),
                get_raw_value_from(_request.telephone),
                get_raw_value_from(_request.email)};
        if (_request.log_entry_id != boost::none)
        {
            params.push_back(get_raw_value_from(*_request.log_entry_id));
        }

        const auto dbres = ctx.get_conn().exec_params(query, params);

        if (dbres.rows_affected() < 1)
        {
            throw UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist(_request);
        }

        if (dbres.rows_affected() > 1)
        {
            throw InternalServerError("update of contact_representative failed - too many rows affected");
        }

        const auto uuid = Fred::Registry::Util::make_strong<ContactRepresentativeId>(static_cast<std::string>(dbres[0][0]));
        const auto history_id = static_cast<unsigned long long>(dbres[0][1]);
        const auto contact_uuid = Fred::Registry::Util::make_strong<ContactId>(static_cast<std::string>(dbres[0][2]));

        UpdateContactRepresentativeReply::Data result;
        result.contact_representative =
                ContactRepresentativeInfo(
                        uuid,
                        get_history_uuid(ctx, history_id),
                        contact_uuid,
                        _request.name,
                        _request.organization,
                        _request.email,
                        _request.telephone,
                        _request.place);
        ctx.commit_transaction();
        return result;
    }
    catch (const UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist&)
    {
        throw;
    }
    catch (const UpdateContactRepresentativeReply::Exception::InvalidData&)
    {
        throw;
    }
    catch (const InternalServerError&)
    {
        throw;
    }
    catch (const Database::Exception& e)
    {
        FREDLOG_WARNING(std::string{"Database::Exception std::exception: "} + e.what());
        throw Registry::DatabaseError(std::string("Database::Exception: ") + e.what());
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(std::string{"unexpected std::exception: "} + e.what());
        throw InternalServerError("unexpected std::exception");
    }
    catch (...)
    {
        throw InternalServerError("unexpected unknown exception");
    }
}
