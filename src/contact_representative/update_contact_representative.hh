/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UPDATE_CONTACT_REPRESENTATIVE_HH_517C9B273CCB4F99A81772A0C1498A00
#define UPDATE_CONTACT_REPRESENTATIVE_HH_517C9B273CCB4F99A81772A0C1498A00

#include "src/common_types.hh"
#include "src/exceptions.hh"
#include "src/contact_representative/contact_representative_common_types.hh"
#include "src/util/printable.hh"

#include <boost/optional.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Registry {
namespace ContactRepresentative {

struct UpdateContactRepresentativeRequest : Util::Printable<UpdateContactRepresentativeRequest>
{
    ContactRepresentativeId id;
    std::string name;
    std::string organization;
    PlaceAddress place;
    PhoneNumber telephone;
    EmailAddress email;

    boost::optional<LogEntryId> log_entry_id;

    std::string to_string() const;
};

struct UpdateContactRepresentativeReply
{
    struct Data : Util::Printable<Data>
    {
        ContactRepresentativeInfo contact_representative;
        std::string to_string() const;
    };
    struct Exception
    {
        struct ContactRepresentativeDoesNotExist : ContactRepresentative::Exception
        {
            explicit ContactRepresentativeDoesNotExist(const UpdateContactRepresentativeRequest& src);
        };
        struct InvalidData : ContactRepresentative::Exception
        {
            explicit InvalidData(const UpdateContactRepresentativeRequest& src, std::set<std::string> fields);
            std::set<std::string> fields;
        };
    };
};

UpdateContactRepresentativeReply::Data update_contact_representative(const UpdateContactRepresentativeRequest& request);

} // namespace Fred::Registry::ContactRepresentative
} // namespace Fred::Registry
} // namespace Fred

#endif
