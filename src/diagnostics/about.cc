/*
 * Copyright (C) 2019-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "config.h"

#include "src/diagnostics/about.hh"

#include "fred_api/registry/contact/service_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact_representative/service_contact_representative_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/registrar/service_registrar_grpc.grpc.pb.h"

namespace Fred {
namespace Registry {
namespace Diagnostics {

LibDiagnostics::AboutReply about()
{
    return {
            Fred::LibDiagnostics::AboutReply::ServerVersion{PACKAGE_VERSION},
            {
                    {Api::Contact::Contact::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Domain::Domain::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Keyset::Keyset::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Nsset::Nsset::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Registrar::Registrar::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Contact::SearchContact::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Domain::SearchDomain::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Keyset::SearchKeyset::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::Nsset::SearchNsset::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
                    {Api::ContactRepresentative::ContactRepresentative::service_full_name(), Fred::LibDiagnostics::AboutReply::ApiVersion{API_REGISTRY_VERSION}},
            }};
}

} // namespace Fred::Registry::Diagnostics
} // namespace Fred::Registry
} // namespace Fred
