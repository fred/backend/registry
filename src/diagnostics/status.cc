/*
 * Copyright (C) 2020-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/diagnostics/status.hh"

#include "fred_api/registry/contact/service_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact_representative/service_contact_representative_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/registrar/service_registrar_grpc.grpc.pb.h"

#include "libfred/opcontext.hh"

namespace Fred {
namespace Registry {
namespace Diagnostics {

LibDiagnostics::StatusReply status()
{
    bool db_accessible = false;
    try
    {
        LibFred::OperationContextCreator ctx;
        const auto query = "SELECT 'status'::TEXT WHERE false";
        ctx.get_conn().exec(query);
        db_accessible = true;
    }
    catch (...)
    {
    }

    const auto services = {
        Api::Contact::Contact::service_full_name(),
        Api::Domain::Domain::service_full_name(),
        Api::Keyset::Keyset::service_full_name(),
        Api::Nsset::Nsset::service_full_name(),
        Api::Registrar::Registrar::service_full_name(),
        Api::Contact::SearchContact::service_full_name(),
        Api::Domain::SearchDomain::service_full_name(),
        Api::Keyset::SearchKeyset::service_full_name(),
        Api::Nsset::SearchNsset::service_full_name(),
        Api::ContactRepresentative::ContactRepresentative::service_full_name()};

    LibDiagnostics::StatusReply reply;

    if (db_accessible)
    {
        const auto status_ok =
                LibDiagnostics::StatusReply::Service{
                        LibDiagnostics::StatusReply::Status::ok,
                        LibDiagnostics::StatusReply::Note{"OK"},
                        std::map<LibDiagnostics::StatusReply::ExtraKey, LibDiagnostics::StatusReply::ExtraValue>{}};
        for (const auto& service : services)
        {
            reply.services[service] = status_ok;
        }
    }
    else
    {
        const auto status_error =
                LibDiagnostics::StatusReply::Service{
                        LibDiagnostics::StatusReply::Status::error,
                        LibDiagnostics::StatusReply::Note{"Database not accessible."},
                        std::map<LibDiagnostics::StatusReply::ExtraKey, LibDiagnostics::StatusReply::ExtraValue>{}};
        for (const auto& service : services)
        {
            reply.services[service] = status_error;
        }
    }
    return reply;
}

} // namespace Fred::Registry::Diagnostics
} // namespace Fred::Registry
} // namespace Fred
