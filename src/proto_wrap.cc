/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/proto_wrap.hh"
#include "src/util/into.hh"

#include <boost/lexical_cast.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <algorithm>
#include <iterator>
#include <tuple>
#include <utility>

namespace Fred {
namespace Registry {

namespace {

template <typename N>
void proto_wrap_uuid(const Uuid<N>& src, Api::Uuid* dst)
{
    dst->set_value(boost::lexical_cast<std::string>(get_raw_value_from(src)));
}

template <typename N, typename A>
void proto_wrap_uuid(const Uuid<N>& src, A* dst)
{
    dst->set_value(boost::lexical_cast<std::string>(get_raw_value_from(src)));
}

}//namespace Fred::Registry::{anonymous}

template <>
void proto_wrap(const LogEntryId& src, Api::LogEntryId* dst)
{
    dst->set_value(get_raw_value_from(src));
}

template <>
struct RegistryTypeTraits<Registrar::RegistrarHandle>
{
    using ProtoType = Api::Registrar::RegistrarHandle;
};

template <>
void proto_wrap(const Registrar::RegistrarHandle& src, Api::Registrar::RegistrarHandle* dst)
{
    dst->set_value(get_raw_value_from(src));
}

template <>
struct RegistryTypeTraits<Contact::ContactLightInfo>
{
    using ProtoType = std::tuple<Api::Contact::ContactRef*, Api::Contact::ContactId*>;
};

template <>
void proto_wrap(const Contact::ContactLightInfo&, std::tuple<Api::Contact::ContactRef*, Api::Contact::ContactId*>*);

template <>
struct RegistryTypeTraits<Domain::DomainLightInfo>
{
    using ProtoType = std::tuple<Api::Domain::DomainRef*, Api::Domain::DomainId*>;
};

template <>
void proto_wrap(const Domain::DomainLightInfo&, std::tuple<Api::Domain::DomainRef*, Api::Domain::DomainId*>*);

template <>
struct RegistryTypeTraits<Keyset::KeysetId>
{
    using ProtoType = Api::Keyset::KeysetId;
};

template <>
void proto_wrap(const Keyset::KeysetId& src, Api::Keyset::KeysetId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

template <>
struct RegistryTypeTraits<Keyset::KeysetLightInfo>
{
    using ProtoType = std::tuple<Api::Keyset::KeysetRef*, Api::Keyset::KeysetId*>;
};

template <>
void proto_wrap(const Keyset::KeysetLightInfo&, std::tuple<Api::Keyset::KeysetRef*, Api::Keyset::KeysetId*>*);

template <>
struct RegistryTypeTraits<Nsset::NssetId>
{
    using ProtoType = Api::Nsset::NssetId;
};

template <>
void proto_wrap(const Nsset::NssetId& src, Api::Nsset::NssetId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

template <>
struct RegistryTypeTraits<Nsset::NssetLightInfo>
{
    using ProtoType = std::tuple<Api::Nsset::NssetRef*, Api::Nsset::NssetId*>;
};

template <>
void proto_wrap(const Nsset::NssetLightInfo&, std::tuple<Api::Nsset::NssetRef*, Api::Nsset::NssetId*>*);

namespace {

void proto_wrap(const Contact::ContactHandle& src, Api::Contact::ContactHandle* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::ContactId& src, Api::Contact::ContactId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Contact::ContactHistoryId& src, Api::Contact::ContactHistoryId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const PostalCode& src, Api::PostalCode* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const CountryCode& src, Api::CountryCode* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const PlaceAddress& src, Api::PlaceAddress* dst)
{
    for (const auto& street : src.street)
    {
        dst->add_street(street);
    }
    dst->set_city(src.city);
    dst->set_state_or_province(src.state_or_province);
    proto_wrap(src.postal_code, dst->mutable_postal_code());
    proto_wrap(src.country_code, dst->mutable_country_code());
}

void proto_wrap(const Contact::PrivacyControlled<PlaceAddress>& src, Api::PlaceAddress* dst)
{
    proto_wrap(get_privacy_controlled_data(src), dst);
}

void proto_wrap(const PhoneNumber& src, Api::PhoneNumber* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const EmailAddress& src, Api::EmailAddress* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const VatIdentificationNumber& src, Api::VatIdentificationNumber* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::NationalIdentityNumber& src, Api::Contact::NationalIdentityNumber* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::NationalIdentityCard& src, Api::Contact::NationalIdentityCard* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::PassportNumber& src, Api::Contact::PassportNumber* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const CompanyRegistrationNumber& src, Api::CompanyRegistrationNumber* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::SocialSecurityNumber& src, Api::Contact::SocialSecurityNumber* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::Birthdate& src, Api::Contact::Birthdate* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Contact::ContactAdditionalIdentifier& src, Api::Contact::ContactAdditionalIdentifier* dst)
{
    class Visitor : public boost::static_visitor<void>
    {
    public:
        explicit Visitor(Api::Contact::ContactAdditionalIdentifier* dst) : dst_(dst) { }
        void operator()(const Contact::NationalIdentityNumber& src) const
        {
            proto_wrap(src, dst_->mutable_national_identity_number());
        }
        void operator()(const Contact::NationalIdentityCard& src) const
        {
            proto_wrap(src, dst_->mutable_national_identity_card());
        }
        void operator()(const Contact::PassportNumber& src) const
        {
            proto_wrap(src, dst_->mutable_passport_number());
        }
        void operator()(const CompanyRegistrationNumber& src) const
        {
            proto_wrap(src, dst_->mutable_company_registration_number());
        }
        void operator()(const Contact::SocialSecurityNumber& src) const
        {
            proto_wrap(src, dst_->mutable_social_security_number());
        }
        void operator()(const Contact::Birthdate& src) const
        {
            proto_wrap(src, dst_->mutable_birthdate());
        }
    private:
        Api::Contact::ContactAdditionalIdentifier* dst_;
    };
    boost::apply_visitor(Visitor(dst), src);
}

void proto_wrap(const Contact::ContactAddress& src, Api::Contact::ContactAddress* dst)
{
    dst->set_company(src.company);
    for (const auto& street : src.street)
    {
        dst->add_street(street);
    }
    dst->set_city(src.city);
    dst->set_state_or_province(src.state_or_province);
    proto_wrap(src.postal_code, dst->mutable_postal_code());
    proto_wrap(src.country_code, dst->mutable_country_code());
}

void proto_wrap(const Contact::WarningLetter& src, Api::Contact::WarningLetter* dst)
{
    switch (src.preference)
    {
        case Contact::WarningLetter::SendingPreference::to_send:
            dst->set_preference(Api::Contact::WarningLetter::SendingPreference::to_send);
            return;
        case Contact::WarningLetter::SendingPreference::not_to_send:
            dst->set_preference(Api::Contact::WarningLetter::SendingPreference::not_to_send);
            return;
        case Contact::WarningLetter::SendingPreference::not_specified:
            dst->set_preference(Api::Contact::WarningLetter::SendingPreference::not_specified);
            return;
    }
    throw std::runtime_error("unknown warning letter preference");
}

template <typename R>
void proto_wrap(const std::chrono::time_point<std::chrono::system_clock, R>& src, ::google::protobuf::Timestamp* dst)
{
    const auto time_in_nanos =
            std::chrono::duration_cast<std::chrono::nanoseconds>(src.time_since_epoch()).count();
    dst->set_seconds(time_in_nanos / 1'000'000'000);
    dst->set_nanos(time_in_nanos % 1'000'000'000);
}

void proto_wrap(
        const Object::ObjectEvents::EventData& src,
        Registry::Api::ObjectEvents::EventData* dst)
{
    if (src.at != boost::none)
    {
        proto_wrap(*src.at, dst->mutable_timestamp());
    }
    else
    {
        dst->clear_timestamp();
    }
    if (src.by_registrar != boost::none)
    {
        Registry::proto_wrap(*src.by_registrar, dst->mutable_registrar_handle());
    }
    else
    {
        dst->clear_registrar_handle();
    }
}

void proto_wrap(const Object::ObjectEvents& src, Registry::Api::ObjectEvents* dst)
{
    proto_wrap(src.registered, dst->mutable_registered());
    if (src.transferred != boost::none)
    {
        proto_wrap(*src.transferred, dst->mutable_transferred());
    }
    else
    {
        dst->clear_transferred();
    }
    if (src.updated != boost::none)
    {
        proto_wrap(*src.updated, dst->mutable_updated());
    }
    else
    {
        dst->clear_updated();
    }
    if (src.unregistered != boost::none)
    {
        proto_wrap(*src.unregistered, dst->mutable_unregistered());
    }
    else
    {
        dst->clear_unregistered();
    }
}

template <typename T>
void proto_wrap_contact_discloseflags(const std::string& name, bool is_available_publicly, T& dst)
{
    dst[name] = is_available_publicly;
}

template <typename T>
void proto_wrap_publishability(const Contact::ContactInfoReply::Data& src, T* dst)
{
    proto_wrap_contact_discloseflags("name", is_publicly_available(src.name), *dst);
    proto_wrap_contact_discloseflags("organization", is_publicly_available(src.organization), *dst);
    proto_wrap_contact_discloseflags("place", is_publicly_available(src.place), *dst);
    proto_wrap_contact_discloseflags("telephone", is_publicly_available(src.telephone), *dst);
    proto_wrap_contact_discloseflags("fax", is_publicly_available(src.fax), *dst);
    proto_wrap_contact_discloseflags("emails", is_publicly_available(src.emails), *dst);
    proto_wrap_contact_discloseflags("notify_emails", is_publicly_available(src.notify_emails), *dst);
    proto_wrap_contact_discloseflags("vat_identification_number", is_publicly_available(src.vat_identification_number), *dst);
    proto_wrap_contact_discloseflags("additional_identifier", is_publicly_available(src.additional_identifier), *dst);
}

template <typename T>
void proto_wrap_emails(const std::vector<EmailAddress>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& email : src)
    {
        proto_wrap(email, dst->Add());
    }
}

void proto_wrap(const Contact::ContactInfoReply::Data& src, Api::Contact::ContactInfoReply::Data* dst)
{
    proto_wrap(src.contact_handle, dst->mutable_contact_handle());
    proto_wrap(src.contact_id, dst->mutable_contact_id());
    proto_wrap(src.contact_history_id, dst->mutable_contact_history_id());
    dst->set_name(get_privacy_controlled_data(src.name));
    dst->set_organization(get_privacy_controlled_data(src.organization));
    proto_wrap(src.place, dst->mutable_place());
    if (does_present_privacy_controlled_data(src.telephone))
    {
        proto_wrap(*get_privacy_controlled_data(src.telephone), dst->mutable_telephone());
    }
    if (does_present_privacy_controlled_data(src.fax))
    {
        proto_wrap(*get_privacy_controlled_data(src.fax), dst->mutable_fax());
    }
    proto_wrap_emails(get_privacy_controlled_data(src.emails), dst->mutable_emails());
    proto_wrap_emails(get_privacy_controlled_data(src.notify_emails), dst->mutable_notify_emails());
    if (does_present_privacy_controlled_data(src.vat_identification_number))
    {
        proto_wrap(*get_privacy_controlled_data(src.vat_identification_number), dst->mutable_vat_identification_number());
    }
    if (does_present_privacy_controlled_data(src.additional_identifier))
    {
        proto_wrap(*get_privacy_controlled_data(src.additional_identifier), dst->mutable_additional_identifier());
    }
    proto_wrap_publishability(src, dst->mutable_publish());
    if (src.mailing_address != boost::none)
    {
        proto_wrap(*src.mailing_address, dst->mutable_mailing_address());
    }
    if (src.billing_address != boost::none)
    {
        proto_wrap(*src.billing_address, dst->mutable_billing_address());
    }
    if (src.shipping_address[0] != boost::none)
    {
        proto_wrap(*src.shipping_address[0], dst->mutable_shipping_address1());
    }
    if (src.shipping_address[1] != boost::none)
    {
        proto_wrap(*src.shipping_address[1], dst->mutable_shipping_address2());
    }
    if (src.shipping_address[2] != boost::none)
    {
        proto_wrap(*src.shipping_address[2], dst->mutable_shipping_address3());
    }
    proto_wrap(src.warning_letter, dst->mutable_warning_letter());
    proto_wrap(src.representative_events, dst->mutable_events());
    Registry::proto_wrap(src.sponsoring_registrar, dst->mutable_sponsoring_registrar());
}

void proto_wrap(const Contact::ContactIdReply::Data& src, Api::Contact::ContactIdReply::Data* dst)
{
    proto_wrap(src.contact_id, dst->mutable_contact_id());
    proto_wrap(src.contact_history_id, dst->mutable_contact_history_id());
}

template <typename T>
void proto_wrap_contact_state_flags(const Contact::ContactStateFlagsInfo& src, T* dst)
{
    for (const auto& flag : src)
    {
        Registry::Api::StateFlag::Traits traits;
        switch (flag.how_to_set)
        {
            case Contact::StateFlagInfo::Manipulation::automatic:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::automatic);
                break;
            case Contact::StateFlagInfo::Manipulation::manual:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::manual);
                break;
        }
        switch (flag.visibility)
        {
            case Contact::StateFlagInfo::Visibility::external:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::external);
                break;
            case Contact::StateFlagInfo::Visibility::internal:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::internal);
                break;
        }
        (*dst)[flag.name] = traits;
    }
}

template <typename T>
void proto_wrap_contact_state(const Contact::ContactState& src, T* dst)
{
    for (const auto& name_presence : src.flag_presents)
    {
        (*dst)[name_presence.first] = name_presence.second;
    }
}

void proto_wrap(
        const Contact::ContactStateHistory::Record& record,
        const std::vector<std::string>& flags_names,
        Api::Contact::ContactStateHistory::Record* dst)
{
    if (record.presents.size() != flags_names.size())
    {
        throw std::runtime_error("number of flags must be equal to the number of flags names");
    }
    proto_wrap(record.valid_from, dst->mutable_valid_from());
    for (std::size_t idx = 0; idx < record.presents.size(); ++idx)
    {
        (*(dst->mutable_flags()))[flags_names[idx]] = record.presents[idx];
    }
}

void proto_wrap(const Contact::ContactStateHistory& src, Api::Contact::ContactStateHistory* dst)
{
    dst->mutable_timeline()->Reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        proto_wrap(record, src.flags_names, dst->mutable_timeline()->Add());
    }
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
}

void proto_wrap(const Contact::ContactDataHistory::Record& src, Api::Contact::ContactHistoryRecord* dst)
{
    proto_wrap(src.contact_history_id, dst->mutable_contact_history_id());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    proto_wrap(src.log_entry_id, dst->mutable_log_entry_id());
}

template <typename T>
void proto_wrap_vector_of_contact_data_history_record(const std::vector<Contact::ContactDataHistory::Record>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Contact::ContactDataHistory& src, Api::Contact::ContactHistoryReply::Data* dst)
{
    proto_wrap(src.contact_id, dst->mutable_contact_id());
    proto_wrap_vector_of_contact_data_history_record(src.timeline, dst->mutable_timeline());
    if (src.valid_to != boost::none)
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    else
    {
        dst->clear_valid_to();
    }
}

void proto_wrap(const Contact::ContactDataHistoryReply::Data& src, Api::Contact::ContactHistoryReply::Data* dst)
{
    proto_wrap(src.history, dst);
}

void proto_wrap(const Contact::CreateContactReply::Data& src, Api::Contact::CreateContactReply::Data* dst)
{
    proto_wrap(src.contact_id, dst->mutable_contact_id());
}

void proto_wrap(const Contact::ContactLifetime::TimeSpec& src, Api::Contact::ContactLifetime::TimeSpec* dst)
{
    proto_wrap(src.contact_history_id, dst->mutable_contact_history_id());
    proto_wrap(src.timestamp, dst->mutable_timestamp());
}

void proto_wrap(const Contact::ContactLifetime& src, Api::Contact::ContactLifetime* dst)
{
    proto_wrap(src.contact_id, dst->mutable_contact_id());
    proto_wrap(src.begin, dst->mutable_registered_from());
    if (src.end != boost::none)
    {
        proto_wrap(*src.end, dst->mutable_registered_to());
    }
    else
    {
        dst->clear_registered_to();
    }
}

template <typename T>
void proto_wrap_vector_of_contact_lifetime(const std::vector<Contact::ContactLifetime>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Contact::ContactHandleHistoryReply::Data& src, Api::Contact::ContactHandleHistoryReply::Data* dst)
{
    proto_wrap(src.contact_handle, dst->mutable_contact_handle());
    proto_wrap_vector_of_contact_lifetime(src.timeline, dst->mutable_timeline());
}

template <typename T>
void proto_wrap_set_of_string(const std::set<std::string>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& str : src)
    {
        *(dst->Add()) = str;
    }
}

void proto_wrap(const Contact::SearchContactReply::Data::Result& src, Api::Contact::SearchContactReply::Data::Result* dst)
{
    proto_wrap(src.contact_id, dst->mutable_object_id());
    proto_wrap_set_of_string(src.matched_items, dst->mutable_matched_items());
}

template <typename T>
void proto_wrap_vector_of_search_contact_result(const std::vector<Contact::SearchContactReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Contact::SearchContactReply::Data::FuzzyValue& src,
                Api::Contact::SearchContactReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

template <typename T>
void proto_wrap_set_of_contact_items(const std::set<Contact::ContactItem>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto item : src)
    {
        *(dst->Add()) = Util::Into<std::string>::from(item);
    }
}

void proto_wrap(const Contact::SearchContactReply::Data& src, Api::Contact::SearchContactReply::Data* dst)
{
    proto_wrap_vector_of_search_contact_result(src.most_similar_contacts, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_contact_items(src.searched_items, dst->mutable_searched_items());
}

template <typename T>
void proto_wrap_vector_of_contact_history_id(
        const std::vector<Contact::ContactHistoryId>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Contact::SearchContactHistoryReply::Data::Result::HistoryPeriod& src,
                Api::Contact::SearchContactHistoryReply::Data::Result::HistoryPeriod* dst)
{
    proto_wrap_set_of_string(src.matched_items, dst->mutable_matched_items());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    proto_wrap_vector_of_contact_history_id(src.contact_history_ids, dst->mutable_contact_history_ids());
}

template <typename T>
void proto_wrap_vector_of_search_contact_history_result_histories(
    const std::vector<Contact::SearchContactHistoryReply::Data::Result::HistoryPeriod>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Contact::SearchContactHistoryReply::Data::Result& src,
                Api::Contact::SearchContactHistoryReply::Data::Result* dst)
{
    proto_wrap(src.object_id, dst->mutable_object_id());
    proto_wrap_vector_of_search_contact_history_result_histories(src.histories, dst->mutable_histories());
}

template <typename T>
void proto_wrap_vector_of_search_contact_history_result(const std::vector<Contact::SearchContactHistoryReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Contact::SearchContactHistoryReply::Data::FuzzyValue& src,
                Api::Contact::SearchContactHistoryReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

void proto_wrap(const Contact::SearchContactHistoryReply::Data& src, Api::Contact::SearchContactHistoryReply::Data* dst)
{
    proto_wrap_vector_of_search_contact_history_result(src.results, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_contact_items(src.searched_items, dst->mutable_searched_items());
}

void proto_wrap(const Contact::ContactStateFlagsInfo& src, Api::Contact::ContactStateFlagsReply::Data* dst)
{
    proto_wrap_contact_state_flags(src, dst->mutable_traits());
}

void proto_wrap(const Contact::ContactLightInfo& src, Api::Contact::ContactId* dst)
{
    proto_wrap(src.id, dst);
}

void proto_wrap(const Contact::ContactLightInfo& src, Api::Contact::ContactRef* dst)
{
    proto_wrap(src.id, dst->mutable_id());
    proto_wrap(src.handle, dst->mutable_handle());
    dst->set_name(src.name);
    dst->set_organization(src.organization);
}

void proto_wrap(const std::string& src, std::string* dst)
{
    *dst = src;
}

template <typename Src, typename Dst>
void wrap_invalid_data_exception(const Src& src, Dst* dst)
{
    auto* const fields = dst->mutable_fields();
    fields->Reserve(src.fields.size());
    std::for_each(begin(src.fields), end(src.fields), [&](auto&& value)
    {
        proto_wrap(value, fields->Add());
    });
}

void proto_wrap(const PaginationReply& src, Api::PaginationReply* dst)
{
    dst->set_next_page_token(src.next_page_token);
    if (src.items_left != boost::none)
    {
        dst->set_items_left(*src.items_left);
    }
}

}//namespace Fred::Registry::{anonymous}

template <>
void proto_wrap(const Contact::ContactLightInfo& src, std::tuple<Api::Contact::ContactRef*, Api::Contact::ContactId*>* dst)
{
    proto_wrap(src.id, std::get<Api::Contact::ContactId*>(*dst));
    proto_wrap(src, std::get<Api::Contact::ContactRef*>(*dst));
}

template <>
void proto_wrap(const Contact::ContactInfoReply::Data& src, Api::Contact::ContactInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::ContactInfoReply::Exception::ContactDoesNotExist&, Api::Contact::ContactInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::ContactIdReply::Data& src, Api::Contact::ContactIdReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::ContactIdReply::Exception::ContactDoesNotExist&, Api::Contact::ContactIdReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::ContactStateFlagsInfo& src, Api::Contact::ContactStateFlagsReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::ContactDataHistoryReply::Data& src, Api::Contact::ContactHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::ContactDataHistoryReply::Exception::ContactDoesNotExist&,
                Api::Contact::ContactHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::ContactDataHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Contact::ContactHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Contact::CreateContactReply::Data& src, Api::Contact::CreateContactReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::CreateContactReply::Exception::InvalidData& src,
                Api::Contact::CreateContactReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Contact::CreateContactReply::Exception::ContactAlreadyExists&,
                Api::Contact::CreateContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_already_exists();
}

template <>
void proto_wrap(const Contact::CreateContactReply::Exception::ContactHandleInProtectionPeriod&,
                Api::Contact::CreateContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_handle_in_protection_period();
}

template <>
void proto_wrap(const Contact::CreateContactReply::Exception::RegistrarDoesNotExist&,
                Api::Contact::CreateContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Contact::ContactHandleHistoryReply::Data& src, Api::Contact::ContactHandleHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::ContactStateReply::Data& src, Api::Contact::ContactStateReply* dst)
{
    proto_wrap(src.contact_id, dst->mutable_data()->mutable_contact_id());
    proto_wrap_contact_state(src.state, dst->mutable_data()->mutable_state()->mutable_flags());
}

template <>
void proto_wrap(const Contact::ContactStateReply::Exception::ContactDoesNotExist&, Api::Contact::ContactStateReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::ContactStateHistoryReply::Data& src, Api::Contact::ContactStateHistoryReply* dst)
{
    proto_wrap(src.contact_id, dst->mutable_data()->mutable_contact_id());
    proto_wrap(src.history, dst->mutable_data()->mutable_history());
}

template <>
void proto_wrap(const Contact::ContactStateHistoryReply::Exception::ContactDoesNotExist&,
                Api::Contact::ContactStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::ContactStateHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Contact::ContactStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Contact::UpdateContactReply::Data& src, Api::Contact::UpdateContactReply* dst)
{
    proto_wrap(src.contact_id, dst->mutable_data()->mutable_contact_id());
    proto_wrap(src.contact_history_id, dst->mutable_data()->mutable_contact_history_id());
    proto_wrap(src.contact_handle, dst->mutable_data()->mutable_contact_handle());
    proto_wrap(src.events, dst->mutable_data()->mutable_events());
}

template <>
void proto_wrap(const Contact::UpdateContactReply::Exception::ContactDoesNotExist&, Api::Contact::UpdateContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::UpdateContactReply::Exception::NotCurrentVersion&, Api::Contact::UpdateContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_not_current_version();
}

std::size_t proto_wrap(
        const Contact::ListContactsReply::Data& src,
        std::size_t offset,
        Api::Contact::ListContactsReply::Data* dst)
{
    static constexpr auto max_number_of_contacts = 10000;
    dst->Clear();
    if (src.contacts.size() <= offset)
    {
        return 0;
    }
    const auto contacts_to_send = src.contacts.size() - offset;
    const auto chunk_size = contacts_to_send < max_number_of_contacts ? contacts_to_send
                                                                      : max_number_of_contacts;
    const auto next_offset = offset + chunk_size;
    auto* const contacts = dst->mutable_contacts();
    contacts->Reserve(chunk_size);
    std::for_each(begin(src.contacts) + offset, begin(src.contacts) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, contacts->Add());
            });
    const bool last_chunk = src.contacts.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

void proto_wrap(const Registrar::RegistrarId& src, Api::Registrar::RegistrarId* dst)
{
    dst->set_value(boost::uuids::to_string(get_raw_value_from(src)));
}

void proto_wrap(const Registrar::RegistrarLightInfo& src, Api::Registrar::RegistrarId* dst)
{
    proto_wrap(src.id, dst);
}

void proto_wrap(const Registrar::RegistrarLightInfo& src, Api::Registrar::RegistrarRef* dst)
{
    proto_wrap(src.id, dst->mutable_id());
    proto_wrap(src.handle, dst->mutable_handle());
    dst->set_name(src.name);
    dst->set_organization(src.organization);
}

template <>
void proto_wrap(const Contact::ListMergeCandidatesReply::Exception::ContactDoesNotExist&, Api::Contact::ListMergeCandidatesReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::ListMergeCandidatesReply::Exception::InvalidData& src, Api::Contact::ListMergeCandidatesReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
struct RegistryTypeTraits<Contact::ListMergeCandidatesReply::Data::MergeCandidate>
{
    using ProtoType = Api::Contact::ListMergeCandidatesReply::Data::MergeCandidate;
};

void proto_wrap(const Contact::ListMergeCandidatesReply::Data::MergeCandidate& src, Api::Contact::ListMergeCandidatesReply::Data::MergeCandidate* dst)
{
    proto_wrap(src.contact, dst->mutable_contact());
    dst->set_domain_count(src.domain_count);
    dst->set_nsset_count(src.nsset_count);
    dst->set_keyset_count(src.keyset_count);
    proto_wrap(src.sponsoring_registrar, dst->mutable_sponsoring_registrar());
}

std::size_t proto_wrap(
        const Contact::ListMergeCandidatesReply::Data& src,
        std::size_t offset,
        Api::Contact::ListMergeCandidatesReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    if (src.merge_candidates.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.merge_candidates.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const merge_candidates = dst->mutable_merge_candidates();
    merge_candidates->Reserve(chunk_size);
    std::for_each(begin(src.merge_candidates) + offset, begin(src.merge_candidates) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, merge_candidates->Add());
            });
    const bool first_chunk = offset == 0;
    if (first_chunk &&
        (src.pagination != boost::none))
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    const bool last_chunk = src.merge_candidates.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Contact::MergeContactsReply::Exception::ContactDoesNotExist&, Api::Contact::MergeContactsReply* dst)
{
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Contact::MergeContactsReply::Exception::InvalidData& src, Api::Contact::MergeContactsReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Contact::UpdateContactStateReply::Data& src, Api::Contact::UpdateContactStateReply* dst)
{
    proto_wrap(src.contact_id, dst->mutable_data()->mutable_contact_id());
    proto_wrap(src.contact_history_id, dst->mutable_data()->mutable_contact_history_id());
    proto_wrap(src.contact_handle, dst->mutable_data()->mutable_contact_handle());
}

template <>
void proto_wrap(const Contact::SearchContactReply::Data& src, Api::Contact::SearchContactReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::SearchContactHistoryReply::Data& src, Api::Contact::SearchContactHistoryReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Contact::SearchContactHistoryReply::Exception::ChronologyViolation&, Api::Contact::SearchContactHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_chronology_violation();
}

namespace {

void proto_wrap(const Domain::Fqdn& src, Api::Domain::Fqdn* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Domain::DomainId& src, Api::Domain::DomainId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Domain::DomainStateFlagRequest& src, Api::Domain::DomainStateFlagRequest* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Domain::DomainHistoryId& src, Api::Domain::DomainHistoryId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

template <typename T>
void proto_wrap_vector_of_contacts(const std::vector<Contact::ContactLightInfo>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::ExpirationTimestamp& src, ::google::protobuf::Timestamp* dst)
{
    proto_wrap(src.timestamp, dst);
}

void proto_wrap(const Domain::DomainLifeCycleMilestone& src, ::google::protobuf::Timestamp* dst)
{
    proto_wrap(src.timestamp, dst);
}

void proto_wrap(const Domain::DomainLifeCycleTimestamp& src, ::google::protobuf::Timestamp* dst)
{
    proto_wrap(src.timestamp, dst);
}

void proto_wrap(const Domain::DomainInfoReply::Data& src, Api::Domain::DomainInfoReply::Data* dst)
{
    proto_wrap(src.fqdn, dst->mutable_fqdn());
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    if (src.keyset != boost::none)
    {
        auto dst_keyset = std::make_tuple(dst->mutable_keyset(), dst->mutable_keyset_old());
        Registry::proto_wrap(*src.keyset, &dst_keyset);
    }
    else
    {
        dst->clear_keyset();
    }
    if (src.nsset != boost::none)
    {
        auto dst_nsset = std::make_tuple(dst->mutable_nsset(), dst->mutable_nsset_old());
        Registry::proto_wrap(*src.nsset, &dst_nsset);
    }
    else
    {
        dst->clear_nsset();
    }
    {
        auto dst_registrant = std::make_tuple(dst->mutable_registrant(), dst->mutable_registrant_old());
        Registry::proto_wrap(src.registrant, &dst_registrant);
    }
    Registry::proto_wrap(src.sponsoring_registrar, dst->mutable_sponsoring_registrar());
    proto_wrap_vector_of_contacts(src.administrative_contacts, dst->mutable_administrative_contacts_old());
    proto_wrap_vector_of_contacts(src.administrative_contacts, dst->mutable_administrative_contacts());
    proto_wrap(src.expires_at, dst->mutable_expires_at());
    proto_wrap(src.representative_events, dst->mutable_events());
    proto_wrap(src.expiration_warning_scheduled_at, dst->mutable_expiration_warning_scheduled_at());
    proto_wrap(src.outzone_unguarded_warning_scheduled_at, dst->mutable_outzone_unguarded_warning_scheduled_at());
    proto_wrap(src.outzone_scheduled_at, dst->mutable_outzone_scheduled_at());
    proto_wrap(src.delete_warning_scheduled_at, dst->mutable_delete_warning_scheduled_at());
    proto_wrap(src.delete_candidate_scheduled_at, dst->mutable_delete_candidate_scheduled_at());
    if (src.delete_candidate_at != boost::none)
    {
        proto_wrap(*src.delete_candidate_at, dst->mutable_delete_candidate_at());
    }
    else
    {
        dst->clear_delete_candidate_at();
    }
    if (src.outzone_at != boost::none)
    {
        proto_wrap(*src.outzone_at, dst->mutable_outzone_at());
    }
    else
    {
        dst->clear_outzone_at();
    }
    if (src.validation_expires_at != boost::none)
    {
        proto_wrap(*src.validation_expires_at, dst->mutable_validation_expires_at());
    }
    else
    {
        dst->clear_validation_expires_at();
    }
}

void proto_wrap(const Domain::DomainIdReply::Data& src, Api::Domain::DomainIdReply::Data* dst)
{
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
}

template <typename T>
void proto_wrap_domain_state_flags(const Domain::DomainStateFlagsInfo& src, T* dst)
{
    for (const auto& flag : src)
    {
        Registry::Api::StateFlag::Traits traits;
        switch (flag.how_to_set)
        {
            case Domain::StateFlagInfo::Manipulation::automatic:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::automatic);
                break;
            case Domain::StateFlagInfo::Manipulation::manual:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::manual);
                break;
        }
        switch (flag.visibility)
        {
            case Domain::StateFlagInfo::Visibility::external:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::external);
                break;
            case Domain::StateFlagInfo::Visibility::internal:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::internal);
                break;
        }
        (*dst)[flag.name] = traits;
    }
}

template <typename T>
void proto_wrap_domain_state(const Domain::DomainState& src, T* dst)
{
    for (const auto& name_presence : src.flag_presents)
    {
        (*dst)[name_presence.first] = name_presence.second;
    }
}

void proto_wrap(
        const Domain::DomainStateHistory::Record& record,
        const std::vector<std::string>& flags_names,
        Api::Domain::DomainStateHistory::Record* dst)
{
    if (record.presents.size() != flags_names.size())
    {
        throw std::runtime_error("number of flags must be equal to the number of flags names");
    }
    proto_wrap(record.valid_from, dst->mutable_valid_from());
    for (std::size_t idx = 0; idx < record.presents.size(); ++idx)
    {
        (*(dst->mutable_flags()))[flags_names[idx]] = record.presents[idx];
    }
}

void proto_wrap(const Domain::DomainStateHistory& src, Api::Domain::DomainStateHistory* dst)
{
    dst->mutable_timeline()->Reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        proto_wrap(record, src.flags_names, dst->mutable_timeline()->Add());
    }
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
}

void proto_wrap(const Domain::DomainDataHistory::Record& src, Api::Domain::DomainHistoryRecord* dst)
{
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    proto_wrap(src.log_entry_id, dst->mutable_log_entry_id());
}

void proto_wrap(const std::vector<Domain::DomainDataHistory::Record>& src,
                decltype(std::declval<Api::Domain::DomainHistoryReply::Data>().mutable_timeline()) dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::DomainDataHistory& src, Api::Domain::DomainHistoryReply::Data* dst)
{
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    proto_wrap(src.timeline, dst->mutable_timeline());
    if (src.valid_to != boost::none)
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    else
    {
        dst->clear_valid_to();
    }
}

void proto_wrap(const Domain::DomainDataHistoryReply::Data& src, Api::Domain::DomainHistoryReply::Data* dst)
{
    proto_wrap(src.history, dst);
}

void proto_wrap(const Domain::DomainLifetime::TimeSpec& src, Api::Domain::DomainLifetime::TimeSpec* dst)
{
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    proto_wrap(src.timestamp, dst->mutable_timestamp());
}

void proto_wrap(const Domain::DomainLifetime& src, Api::Domain::DomainLifetime* dst)
{
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    proto_wrap(src.begin, dst->mutable_registered_from());
    if (src.end != boost::none)
    {
        proto_wrap(*src.end, dst->mutable_registered_to());
    }
    else
    {
        dst->clear_registered_to();
    }
}

template <typename T>
void proto_wrap_vector_of_domain_lifetime(const std::vector<Domain::DomainLifetime>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::FqdnHistoryReply::Data& src, Api::Domain::FqdnHistoryReply::Data* dst)
{
    proto_wrap(src.fqdn, dst->mutable_fqdn());
    proto_wrap_vector_of_domain_lifetime(src.timeline, dst->mutable_timeline());
}

void proto_wrap(const Domain::DomainStateFlagsInfo& src, Api::Domain::DomainStateFlagsReply::Data* dst)
{
    proto_wrap_domain_state_flags(src, dst->mutable_traits());
}

template <typename T>
void proto_wrap_set_of_domain_items(const std::set<Domain::DomainItem>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto item : src)
    {
        *(dst->Add()) = Util::Into<std::string>::from(item);
    }
}

void proto_wrap(const Domain::SearchDomainReply::Data::Result& src, Api::Domain::SearchDomainReply::Data::Result* dst)
{
    proto_wrap(src.domain_id, dst->mutable_object_id());
    proto_wrap_set_of_domain_items(src.matched_items, dst->mutable_matched_items());
}

template <typename T>
void proto_wrap_vector_of_search_domain_result(const std::vector<Domain::SearchDomainReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::SearchDomainReply::Data::FuzzyValue& src,
                Api::Domain::SearchDomainReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

void proto_wrap(const Domain::SearchDomainReply::Data& src, Api::Domain::SearchDomainReply::Data* dst)
{
    proto_wrap_vector_of_search_domain_result(src.most_similar_domains, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_domain_items(src.searched_items, dst->mutable_searched_items());
}

template <typename T>
void proto_wrap_vector_of_domain_history_id(
        const std::vector<Domain::DomainHistoryId>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::SearchDomainHistoryReply::Data::Result::HistoryPeriod& src,
                Api::Domain::SearchDomainHistoryReply::Data::Result::HistoryPeriod* dst)
{
    proto_wrap_set_of_domain_items(src.matched_items, dst->mutable_matched_items());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    proto_wrap_vector_of_domain_history_id(src.domain_history_ids, dst->mutable_domain_history_ids());
}

template <typename T>
void proto_wrap_vector_of_search_domain_history_result_histories(
        const std::vector<Domain::SearchDomainHistoryReply::Data::Result::HistoryPeriod>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::SearchDomainHistoryReply::Data::Result& src,
                Api::Domain::SearchDomainHistoryReply::Data::Result* dst)
{
    proto_wrap(src.object_id, dst->mutable_object_id());
    proto_wrap_vector_of_search_domain_history_result_histories(src.histories, dst->mutable_histories());
}

template <typename T>
void proto_wrap_vector_of_search_domain_history_result(const std::vector<Domain::SearchDomainHistoryReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::SearchDomainHistoryReply::Data::FuzzyValue& src,
                Api::Domain::SearchDomainHistoryReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

void proto_wrap(const Domain::SearchDomainHistoryReply::Data& src, Api::Domain::SearchDomainHistoryReply::Data* dst)
{
    proto_wrap_vector_of_search_domain_history_result(src.results, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_domain_items(src.searched_items, dst->mutable_searched_items());
}

void proto_wrap(const Registrar::RegistrarHandle& src, Api::Registrar::RegistrarHandle* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Url& src, Api::Url* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Registrar::RegistrarCreditReply::Data& src, Api::Registrar::RegistrarCreditReply::Data* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    for (const auto& zone_credit : src.credit_by_zone)
    {
        Api::Registrar::Credit credit;
        if (zone_credit.second != boost::none)
        {
            credit.set_decimal_value(get_raw_value_from(*zone_credit.second));
        }
        dst->mutable_credit_by_zone()->insert({zone_credit.first, credit});
    }
}

void proto_wrap(const Registrar::RegistrarInfoReply::Data& src, Api::Registrar::RegistrarInfoReply::Data* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    proto_wrap_uuid(src.registrar_id, dst->mutable_id());
    dst->set_name(src.name);
    dst->set_organization(src.organization);
    if (src.place != boost::none)
    {
        proto_wrap(*src.place, dst->mutable_place());
    }
    if (src.telephone != boost::none)
    {
        proto_wrap(*src.telephone, dst->mutable_telephone());
    }
    if (src.fax != boost::none)
    {
        proto_wrap(*src.fax, dst->mutable_fax());
    }
    proto_wrap_emails(src.emails, dst->mutable_emails());
    if (src.url != boost::none)
    {
        proto_wrap(*src.url, dst->mutable_url());
    }
    dst->set_is_system_registrar(src.is_system_registrar);
    if (src.company_registration_number != boost::none)
    {
        proto_wrap(*src.company_registration_number, dst->mutable_company_registration_number());
    }
    if (src.vat_identification_number != boost::none)
    {
        proto_wrap(*src.vat_identification_number, dst->mutable_vat_identification_number());
    }
    dst->set_variable_symbol(src.variable_symbol);
    dst->set_payment_memo_regex(src.payment_memo_regex);
    dst->set_is_vat_payer(src.is_vat_payer);
    dst->set_is_internal(src.is_internal);
}

void proto_wrap(const Registrar::RegistrarListReply::Data& src, Api::Registrar::RegistrarListReply::Data* dst)
{
    auto* const dst_handles = dst->mutable_registrar_handles();
    dst_handles->Reserve(src.registrar_handles.size());
    for (const auto& handle : src.registrar_handles)
    {
        proto_wrap(handle, dst_handles->Add());
    }
}

void proto_wrap(const Registrar::Period& src, Api::Registrar::RegistrarZoneAccessReply::Data::Period* dst)
{
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    if (src.valid_to != boost::none)
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    else
    {
        dst->clear_valid_to();
    }
}

void proto_wrap(const Registrar::ZoneAccess& src, Api::Registrar::RegistrarZoneAccessReply::Data::Access* dst)
{
    dst->set_has_access_now(src.has_access_now);
    if (!src.history.empty())
    {
        auto* const history = dst->mutable_history();
        history->Reserve(src.history.size());
        std::for_each(begin(src.history), end(src.history), [&](auto&& period)
        {
            proto_wrap(period, history->Add());
        });
    }
}

void proto_wrap(const Registrar::RegistrarZoneAccessReply::Data& src, Api::Registrar::RegistrarZoneAccessReply::Data* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    for (const auto& zone_access : src.access_by_zone)
    {
        Api::Registrar::RegistrarZoneAccessReply::Data::Access access;
        proto_wrap(zone_access.second, &access);
        dst->mutable_access_by_zone()->insert({zone_access.first, std::move(access)});
    }
}

void proto_wrap(const Registrar::CreateRegistrarReply::Data& src, Api::Registrar::CreateRegistrarReply::Data* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
}

void proto_wrap(const Registrar::UpdateRegistrarReply::Data& src, Api::Registrar::UpdateRegistrarReply::Data* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
}

void proto_wrap(const Registrar::RegistrarEppCredentialsId& src, Api::Registrar::RegistrarEppCredentialsId* dst)
{
    proto_wrap_uuid(src, dst);
}

void proto_wrap(
        const Registrar::SslCertificate& src,
        Api::Registrar::SslCertificate* dst)
{
    std::string& dst_fingerprint = *(dst->mutable_fingerprint());
    dst_fingerprint.reserve(src.fingerprint.size());
    std::transform(begin(src.fingerprint), end(src.fingerprint), std::back_inserter(dst_fingerprint), [](unsigned char byte)
    {
        return static_cast<char>(byte);
    });
    if (src.cert_data_pem.empty())
    {
        dst->clear_cert_data_pem();
    }
    else
    {
        proto_wrap(src.cert_data_pem, dst->mutable_cert_data_pem());
    }
}

void proto_wrap(
        const Registrar::GetRegistrarEppCredentialsReply::Data::Credentials& src,
        Api::Registrar::GetRegistrarEppCredentialsReply::Data::Credentials* dst)
{
    proto_wrap(src.credentials_id, dst->mutable_credentials_id());
    if (src.create_time == boost::none)
    {
        dst->clear_create_time();
    }
    else
    {
        proto_wrap(*src.create_time, dst->mutable_create_time());
    }
    proto_wrap(src.certificate, dst->mutable_certificate());
}

template <typename Dst>
void proto_wrap(const std::vector<Registrar::GetRegistrarEppCredentialsReply::Data::Credentials>& src, Dst* dst)
{
    dst->Reserve(src.size());
    std::for_each(begin(src), end(src), [&](auto&& record)
    {
        proto_wrap(record, dst->Add());
    });
}

void proto_wrap(const Registrar::GetRegistrarEppCredentialsReply::Data& src, Api::Registrar::GetRegistrarEppCredentialsReply::Data* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    proto_wrap(src.credentials, dst->mutable_credentials());
}

void proto_wrap(const Registrar::AddRegistrarEppCredentialsReply::Data& src, Api::Registrar::AddRegistrarEppCredentialsReply::Data* dst)
{
    proto_wrap(src.credentials_id, dst->mutable_credentials_id());
}

void proto_wrap(const Registrar::GetRegistrarGroupsReply::Data& src, Api::Registrar::GetRegistrarGroupsReply::Data* dst)
{
    auto* const dst_groups = dst->mutable_groups();
    dst_groups->Reserve(src.groups.size());
    std::for_each(begin(src.groups), end(src.groups), [&](auto&& group_name)
    {
        *(dst_groups->Add()) = get_raw_value_from(group_name);
    });
}

void proto_wrap(const Registrar::RegistrarGroupsMembership& src, Api::Registrar::RegistrarGroupsMembership* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    auto* const dst_groups = dst->mutable_groups();
    dst_groups->Reserve(src.groups.size());
    std::for_each(begin(src.groups), end(src.groups), [&](auto&& group_name)
    {
        *(dst_groups->Add()) = get_raw_value_from(group_name);
    });
}

void proto_wrap(const Registrar::RegistrarCertification& src, Api::Registrar::RegistrarCertification* dst)
{
    proto_wrap_uuid(src.certification_id, dst->mutable_certification_id());
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    proto_wrap(src.validity.valid_from, dst->mutable_valid_from());
    if (src.validity.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.validity.valid_to, dst->mutable_valid_to());
    }
    dst->set_classification(src.classification);
    proto_wrap_uuid(src.file_id, dst->mutable_file_id());
}

void proto_wrap(const Registrar::GetRegistrarCertificationsReply::Data& src, Api::Registrar::GetRegistrarCertificationsReply::Data* dst)
{
    auto* const dst_certifications = dst->mutable_certifications();
    dst_certifications->Reserve(src.certifications.size());
    std::for_each(begin(src.certifications), end(src.certifications), [&](auto&& certification)
    {
        proto_wrap(certification, dst_certifications->Add());
    });
}

void proto_wrap(const Domain::GetDomainsByContactReply::Data::Domain& src,
                Api::Domain::DomainsByContactReply::Data::Domain* dst)
{
    auto dst_domain = std::make_tuple(dst->mutable_domain(), dst->mutable_domain_id());
    Registry::proto_wrap(src.domain, &dst_domain);
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    dst->set_is_holder(src.is_holder);
    dst->set_is_admin_contact(src.is_admin_contact);
    dst->set_is_keyset_tech_contact(src.is_keyset_tech_contact);
    dst->set_is_nsset_tech_contact(src.is_nsset_tech_contact);
    dst->set_is_deleted(src.is_deleted);
}

template <typename T>
void proto_wrap_vector_of_get_domains_by_contact_reply_data_domains(const std::vector<Domain::GetDomainsByContactReply::Data::Domain>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::GetDomainsNotifyInfoReply::Data::Info& src,
                Api::Domain::GetDomainsNotifyInfoReply::Data::Info* dst)
{
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    dst->set_fqdn(src.fqdn);
    proto_wrap(src.registrant_id, dst->mutable_registrant_id());
    proto_wrap(src.registrant_handle, dst->mutable_registrant_handle());
    dst->set_registrant_name(src.registrant_name);
    dst->set_registrant_organization(src.registrant_organization);
    auto* const contacts = dst->mutable_contacts();
    contacts->Reserve(src.emails.size() + src.phones.size());
    std::for_each(begin(src.emails), end(src.emails), [&](auto&& email)
    {
        contacts->Add()->set_email(get_raw_value_from(email));
    });
    std::for_each(begin(src.phones), end(src.phones), [&](auto&& phone)
    {
        contacts->Add()->set_telephone(get_raw_value_from(phone));
    });
    auto* const additional_contacts = dst->mutable_additional_contacts();
    additional_contacts->Reserve(src.additional_emails.size() + src.additional_phones.size());
    std::for_each(begin(src.additional_emails), end(src.additional_emails), [&](auto&& email)
    {
        additional_contacts->Add()->set_email(get_raw_value_from(email));
    });
    std::for_each(begin(src.additional_phones), end(src.additional_phones), [&](auto&& phone)
    {
        additional_contacts->Add()->set_telephone(get_raw_value_from(phone));
    });
}

template <typename T>
void proto_wrap_vector_of_get_domains_notify_info_reply_data_info(const std::vector<Domain::GetDomainsNotifyInfoReply::Data::Info>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Domain::DomainLightInfo& src, Api::Domain::DomainRef* dst)
{
    proto_wrap(src.id, dst->mutable_id());
    proto_wrap(src.fqdn, dst->mutable_fqdn());
}

}//namespace Fred::Registry::{anonymous}

template <>
void proto_wrap(const Domain::DomainLightInfo& src, std::tuple<Api::Domain::DomainRef*, Api::Domain::DomainId*>* dst)
{
    proto_wrap(src.id, std::get<Api::Domain::DomainId*>(*dst));
    proto_wrap(src, std::get<Api::Domain::DomainRef*>(*dst));
}

template <>
void proto_wrap(const Domain::DomainInfoReply::Data& src, Api::Domain::DomainInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::DomainInfoReply::Exception::DomainDoesNotExist&, Api::Domain::DomainInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
struct RegistryTypeTraits<Domain::DomainInfoReply::Exception>
{
    using ProtoType = Api::Domain::DomainInfoReply::Exception;
};

template <>
void proto_wrap(const Domain::DomainInfoReply::Exception&, Api::Domain::DomainInfoReply::Exception* dst)
{
    dst->mutable_domain_does_not_exist();
}

template <>
struct RegistryTypeTraits<Domain::BatchDomainInfoRequest::Request>
{
    using ProtoType = Api::Domain::DomainInfoRequest;
};

template <>
void proto_wrap(const Domain::BatchDomainInfoRequest::Request& src, Api::Domain::DomainInfoRequest* dst)
{
    dst->mutable_domain_id()->mutable_uuid()->set_value(src.domain_id);
    if (src.domain_history_id.empty())
    {
        dst->clear_domain_history_id();
    }
    else
    {
        dst->mutable_domain_history_id()->mutable_uuid()->set_value(src.domain_history_id);
    }
}

template <>
struct RegistryTypeTraits<Domain::BatchDomainInfoReply::Error>
{
    using ProtoType = Api::Domain::BatchDomainInfoReply::Error;
};

template <>
void proto_wrap(const Domain::BatchDomainInfoReply::Error& src, Api::Domain::BatchDomainInfoReply::Error* dst)
{
    proto_wrap(src.request, dst->mutable_request());
    proto_wrap(src.exception, dst->mutable_exception());
}

template <>
struct RegistryTypeTraits<Domain::BatchDomainInfoReply::Data::BatchReply>
{
    using ProtoType = Api::Domain::BatchDomainInfoReply::Data::BatchReply;
};

template <>
void proto_wrap(
        const Domain::BatchDomainInfoReply::Data::BatchReply& src,
        Api::Domain::BatchDomainInfoReply::Data::BatchReply* dst)
{
    class Wrapper : public boost::static_visitor<void>
    {
    public:
        explicit Wrapper(Api::Domain::BatchDomainInfoReply::Data::BatchReply* dst)
            : dst_{dst}
        { }
        void operator()(const Domain::DomainInfoReply::Data& data) const
        {
            proto_wrap(data, dst_->mutable_data());
        }
        void operator()(const Domain::BatchDomainInfoReply::Error& error) const
        {
            proto_wrap(error, dst_->mutable_error());
        }
    private:
        Api::Domain::BatchDomainInfoReply::Data::BatchReply* dst_;    
    };
    boost::apply_visitor(Wrapper{dst}, src.data_or_error);
}

std::size_t proto_wrap(
        const Domain::BatchDomainInfoReply::Data& src,
        std::size_t offset,
        Api::Domain::BatchDomainInfoReply* dst)
{
    static constexpr auto max_number_of_replies = 1000;
    dst->Clear();
    if (src.replies.size() <= offset)
    {
        return 0;
    }
    const auto replies_to_send = src.replies.size() - offset;
    const auto chunk_size = replies_to_send < max_number_of_replies ? replies_to_send
                                                                    : max_number_of_replies;
    const auto next_offset = offset + chunk_size;
    auto* const replies = dst->mutable_data()->mutable_replies();
    replies->Reserve(chunk_size);
    std::for_each(cbegin(src.replies) + offset, cbegin(src.replies) + next_offset, [&replies](auto&& record)
    {
        proto_wrap(record, replies->Add());
    });
    const bool last_chunk = src.replies.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::DomainIdReply::Data& src, Api::Domain::DomainIdReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::DomainIdReply::Exception::DomainDoesNotExist&, Api::Domain::DomainIdReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
void proto_wrap(const Domain::DomainStateFlagsInfo& src, Api::Domain::DomainStateFlagsReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::DomainDataHistoryReply::Data& src, Api::Domain::DomainHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::DomainDataHistoryReply::Exception::DomainDoesNotExist&,
                Api::Domain::DomainHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
void proto_wrap(const Domain::DomainDataHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Domain::DomainHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Domain::FqdnHistoryReply::Data& src, Api::Domain::FqdnHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::DomainStateReply::Data& src, Api::Domain::DomainStateReply* dst)
{
    proto_wrap(src.domain_id, dst->mutable_data()->mutable_domain_id());
    proto_wrap_domain_state(src.state, dst->mutable_data()->mutable_state()->mutable_flags());
}

template <>
void proto_wrap(const Domain::DomainStateReply::Exception::DomainDoesNotExist&, Api::Domain::DomainStateReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
void proto_wrap(const Domain::DomainStateHistoryReply::Data& src, Api::Domain::DomainStateHistoryReply* dst)
{
    proto_wrap(src.domain_id, dst->mutable_data()->mutable_domain_id());
    proto_wrap(src.history, dst->mutable_data()->mutable_history());
}

template <>
void proto_wrap(const Domain::DomainStateHistoryReply::Exception::DomainDoesNotExist&,
                Api::Domain::DomainStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
void proto_wrap(const Domain::DomainStateHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Domain::DomainStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Domain::SearchDomainReply::Data& src, Api::Domain::SearchDomainReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::SearchDomainHistoryReply::Data& src, Api::Domain::SearchDomainHistoryReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Domain::SearchDomainHistoryReply::Exception::ChronologyViolation&, Api::Domain::SearchDomainHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_chronology_violation();
}

template <>
void proto_wrap(const Domain::BatchDeleteDomainsReply::NotDeleted::Reason& src, Api::Domain::BatchDeleteDomainsReply::NotDeleted* dst)
{
    switch (src)
    {
        case Domain::BatchDeleteDomainsReply::NotDeleted::Reason::undefined:
            dst->set_reason(Api::Domain::BatchDeleteDomainsReply::NotDeleted::undefined);
            return;
        case Domain::BatchDeleteDomainsReply::NotDeleted::Reason::domain_blocked:
            dst->set_reason(Api::Domain::BatchDeleteDomainsReply::NotDeleted::domain_blocked);
            return;
        case Domain::BatchDeleteDomainsReply::NotDeleted::Reason::domain_does_not_exist:
            dst->set_reason(Api::Domain::BatchDeleteDomainsReply::NotDeleted::domain_does_not_exist);
            return;
        case Domain::BatchDeleteDomainsReply::NotDeleted::Reason::domain_history_id_mismatch:
            dst->set_reason(Api::Domain::BatchDeleteDomainsReply::NotDeleted::domain_history_id_mismatch);
            return;
        case Domain::BatchDeleteDomainsReply::NotDeleted::Reason::insufficient_permissions:
            dst->set_reason(Api::Domain::BatchDeleteDomainsReply::NotDeleted::insufficient_permissions);
            return;
    }
    throw std::runtime_error{"unexpected value " + std::to_string(static_cast<int>(src)) + " of Domain::BatchDeleteDomainsReply::NotDeleted::Reason"};
}

template <>
void proto_wrap(const Domain::BatchDeleteDomainsReply::NotDeleted& src, Api::Domain::BatchDeleteDomainsReply::NotDeleted* dst)
{
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    proto_wrap(src.reason, dst);
}

std::size_t proto_wrap(
        const Domain::BatchDeleteDomainsReply::Data& src,
        std::size_t offset,
        Api::Domain::BatchDeleteDomainsReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    if (src.not_deleted_domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.not_deleted_domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_not_deleted_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.not_deleted_domains) + offset, begin(src.not_deleted_domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, domains->Add());
            });
    const bool last_chunk = src.not_deleted_domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::ManageDomainStateFlagsReply::Data::DomainStateFlagRequestInfo& src, Api::Domain::ManageDomainStateFlagsReply::Data::DomainStateFlagRequestInfo* dst)
{
    proto_wrap(src.request_id, dst->mutable_request_id());
    proto_wrap(src.domain_id, dst->mutable_domain_id());
    dst->set_state_flag(src.state_flag);
}

std::size_t proto_wrap(
        const Domain::ManageDomainStateFlagsReply::Data& src,
        std::size_t offset,
        Api::Domain::ManageDomainStateFlagsReply::Data* dst)
{
    static constexpr auto max_number_of_requests = 10000;
    dst->Clear();
    if (src.state_flag_requests_created.size() <= offset)
    {
        return 0;
    }
    const auto requests_to_send = src.state_flag_requests_created.size() - offset;
    const auto chunk_size = requests_to_send < max_number_of_requests ? requests_to_send
                                                                      : max_number_of_requests;
    const auto next_offset = offset + chunk_size;
    auto* const requests = dst->mutable_state_flag_requests_created();
    requests->Reserve(chunk_size);
    std::for_each(begin(src.state_flag_requests_created) + offset, begin(src.state_flag_requests_created) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, requests->Add());
            });
    const bool last_chunk = src.state_flag_requests_created.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::ManageDomainStateFlagsReply::Exception::DomainDoesNotExist&,
                Api::Domain::ManageDomainStateFlagsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
void proto_wrap(const Domain::ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist&,
                Api::Domain::ManageDomainStateFlagsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_request_does_not_exist();
}

template <>
void proto_wrap(const Domain::ManageDomainStateFlagsReply::Exception::DomainStateFlagDoesNotExist&,
                Api::Domain::ManageDomainStateFlagsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_state_flag_does_not_exist();
}

template <>
void proto_wrap(const Domain::ManageDomainStateFlagsReply::Exception::DomainHistoryIdMismatch&,
                Api::Domain::ManageDomainStateFlagsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_domain_history_id_mismatch();
}

template <>
void proto_wrap(const Domain::GetDomainsByContactReply::Data& src, Api::Domain::DomainsByContactReply* dst)
{
    proto_wrap_vector_of_get_domains_by_contact_reply_data_domains(src.domains, dst->mutable_data()->mutable_domains());
    if (src.pagination != boost::none)
    {
        proto_wrap(*src.pagination, dst->mutable_data()->mutable_pagination());
    }
}

std::size_t proto_wrap(
        const Domain::GetDomainsByContactReply::Data& src,
        std::size_t offset,
        Api::Domain::DomainsByContactReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    const bool first_chunk = offset == 0;
    if (first_chunk && src.pagination != boost::none)
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    if (src.domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.domains) + offset, begin(src.domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, domains->Add());
            });
    const bool last_chunk = src.domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::GetDomainsNotifyInfoReply::Data& src, Api::Domain::GetDomainsNotifyInfoReply* dst)
{
    proto_wrap_vector_of_get_domains_notify_info_reply_data_info(src.domains, dst->mutable_data()->mutable_domains());
    if (src.pagination != boost::none)
    {
        proto_wrap(*src.pagination, dst->mutable_data()->mutable_pagination());
    }
}

std::size_t proto_wrap(
        const Domain::GetDomainsNotifyInfoReply::Data& src,
        std::size_t offset,
        Api::Domain::GetDomainsNotifyInfoReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    const bool first_chunk = offset == 0;
    if (first_chunk && src.pagination != boost::none)
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    if (src.domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.domains) + offset, begin(src.domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, domains->Add());
            });
    const bool last_chunk = src.domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::GetDomainsByContactReply::Exception::ContactDoesNotExist&,
                Api::Domain::DomainsByContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Domain::GetDomainsByContactReply::Exception::InvalidOrderBy&,
                Api::Domain::DomainsByContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_order_by();
}

template <>
void proto_wrap(const Domain::ListDomainsReply::Exception::ZoneDoesNotExist&, Api::Domain::ListDomainsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_zone_does_not_exist();
}

std::size_t proto_wrap(
        const Domain::ListDomainsReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    if (src.domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.domains) + offset, begin(src.domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, domains->Add());
            });
    const bool last_chunk = src.domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::ListDomainsByNssetReply::Exception::NssetDoesNotExist&, Api::Domain::ListDomainsByNssetReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_nsset_does_not_exist();
}

template <>
void proto_wrap(const Domain::ListDomainsByNssetReply::Exception::InvalidData& src, Api::Domain::ListDomainsByNssetReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
struct RegistryTypeTraits<Domain::ListDomainsByNssetReply::Data::Domain>
{
    using ProtoType = Api::Domain::ListDomainsByNssetReply::Data::Domain;
};

template <>
void proto_wrap(const Domain::ListDomainsByNssetReply::Data::Domain& src, Api::Domain::ListDomainsByNssetReply::Data::Domain* dst)
{
    proto_wrap(src.domain, dst->mutable_domain());
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    dst->set_is_deleted(src.is_deleted);
}

std::size_t proto_wrap(
        const Domain::ListDomainsByNssetReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsByNssetReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    if (src.domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_domains();
    auto* const deprecated_domains = dst->mutable_deprecated_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.domains) + offset, begin(src.domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record.domain, deprecated_domains->Add());
                proto_wrap(record, domains->Add());
            });
    const bool first_chunk = offset == 0;
    if (first_chunk &&
        (src.pagination != boost::none))
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    const bool last_chunk = src.domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Domain::ListDomainsByKeysetReply::Exception::KeysetDoesNotExist&, Api::Domain::ListDomainsByKeysetReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_keyset_does_not_exist();
}

template <>
void proto_wrap(const Domain::ListDomainsByKeysetReply::Exception::InvalidData& src, Api::Domain::ListDomainsByKeysetReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
struct RegistryTypeTraits<Domain::ListDomainsByKeysetReply::Data::Domain>
{
    using ProtoType = Api::Domain::ListDomainsByKeysetReply::Data::Domain;
};

template <>
void proto_wrap(const Domain::ListDomainsByKeysetReply::Data::Domain& src, Api::Domain::ListDomainsByKeysetReply::Data::Domain* dst)
{
    proto_wrap(src.domain, dst->mutable_domain());
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    dst->set_is_deleted(src.is_deleted);
}

std::size_t proto_wrap(
        const Domain::ListDomainsByKeysetReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsByKeysetReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    if (src.domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.domains) + offset, begin(src.domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, domains->Add());
            });
    const bool first_chunk = offset == 0;
    if (first_chunk &&
        (src.pagination != boost::none))
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    const bool last_chunk = src.domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
struct RegistryTypeTraits<Domain::DomainContactRole>
{
    using ProtoType = Api::Domain::DomainContactRole;
};

Api::Domain::DomainContactRole proto_wrap(const Domain::DomainContactRole& src)
{
    switch (src)
    {
        case Domain::DomainContactRole::unspecified:
            return Api::Domain::DomainContactRole::role_unspecified;
        case Domain::DomainContactRole::registrant:
            return Api::Domain::DomainContactRole::role_registrant;
        case Domain::DomainContactRole::administrative:
            return Api::Domain::DomainContactRole::role_administrative;
        case Domain::DomainContactRole::keyset_technical:
            return Api::Domain::DomainContactRole::role_keyset_technical;
        case Domain::DomainContactRole::nsset_technical:
            return Api::Domain::DomainContactRole::role_nsset_technical;
    }
    throw std::runtime_error{"unexpected value " + std::to_string(static_cast<int>(src)) + " of Domain::DomainContactRole"};
}

template <>
void proto_wrap(const Domain::ListDomainsByContactReply::Exception::ContactDoesNotExist&, Api::Domain::ListDomainsByContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Domain::ListDomainsByContactReply::Exception::InvalidData& src, Api::Domain::ListDomainsByContactReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
struct RegistryTypeTraits<Domain::ListDomainsByContactReply::Data::Domain>
{
    using ProtoType = Api::Domain::ListDomainsByContactReply::Data::Domain;
};

template <>
void proto_wrap(const Domain::ListDomainsByContactReply::Data::Domain& src, Api::Domain::ListDomainsByContactReply::Data::Domain* dst)
{
    proto_wrap(src.domain, dst->mutable_domain());
    proto_wrap(src.domain_history_id, dst->mutable_domain_history_id());
    dst->mutable_roles()->Reserve(src.roles.size());
    for (const auto& role : src.roles)
    {
        *(dst->mutable_roles()->Add()) = static_cast<int>(proto_wrap(role));
    }
    dst->set_is_deleted(src.is_deleted);
}

std::size_t proto_wrap(
        const Domain::ListDomainsByContactReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsByContactReply::Data* dst)
{
    static constexpr auto max_number_of_domains = 10000;
    dst->Clear();
    if (src.domains.size() <= offset)
    {
        return 0;
    }
    const auto domains_to_send = src.domains.size() - offset;
    const auto chunk_size = domains_to_send < max_number_of_domains ? domains_to_send
                                                                    : max_number_of_domains;
    const auto next_offset = offset + chunk_size;
    auto* const domains = dst->mutable_domains();
    domains->Reserve(chunk_size);
    std::for_each(begin(src.domains) + offset, begin(src.domains) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, domains->Add());
            });
    const bool first_chunk = offset == 0;
    if (first_chunk &&
        (src.pagination != boost::none))
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    const bool last_chunk = src.domains.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

void proto_wrap(const Domain::DomainLifeCycleStageReply::Blacklist& src, Api::Domain::DomainLifeCycleStageReply::Data::Blacklist* dst)
{
    dst->mutable_block_ids()->Reserve(src.size());
    std::for_each(begin(src), end(src), [&](auto&& item)
    {
        proto_wrap_uuid(item, dst->mutable_block_ids()->Add());
    });
}

template <>
void proto_wrap(const Domain::DomainLifeCycleStageReply::Data& src, Api::Domain::DomainLifeCycleStageReply* dst)
{
    if (src.domain_id != boost::none)
    {
        proto_wrap(*src.domain_id, dst->mutable_data()->mutable_domain_id());
    }
    else if (src.is_auctioned)
    {
        dst->mutable_data()->mutable_in_auction();
    }
    else if (src.is_delete_candidate)
    {
        dst->mutable_data()->mutable_delete_candidate();
    }
    else if (!src.blacklisted.empty())
    {
        proto_wrap(src.blacklisted, dst->mutable_data()->mutable_blacklisted());
    }
    else
    {
        dst->mutable_data()->mutable_not_registered();
    }
}

template <>
void proto_wrap(const Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::DomainDoesNotExist&,
                Api::Domain::UpdateDomainsAdditionalNotifyInfoReply* dst)
{
    dst->mutable_exception()->mutable_domain_does_not_exist();
}

template <>
void proto_wrap(const Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::InvalidDomainContactInfo& src,
                Api::Domain::UpdateDomainsAdditionalNotifyInfoReply* dst)
{
    dst->mutable_exception()->mutable_invalid_domain_contact_info()->set_invalid_value(src.invalid_value);
}

namespace {

void proto_wrap(const Keyset::KeysetHandle& src, Api::Keyset::KeysetHandle* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Keyset::KeysetId& src, Api::Keyset::KeysetId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Keyset::KeysetHistoryId& src, Api::Keyset::KeysetHistoryId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Keyset::DnsKey& src, Api::Keyset::DnsKey* dst)
{
    dst->set_flags(src.flags);
    dst->set_protocol(src.protocol);
    dst->set_alg(src.alg);
    dst->set_key(src.key);
}

template <typename T>
void proto_wrap_vector_of_dnskeys(const std::vector<Keyset::DnsKey>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::KeysetInfoReply::Data& src, Api::Keyset::KeysetInfoReply::Data* dst)
{
    proto_wrap(src.keyset_handle, dst->mutable_keyset_handle());
    proto_wrap(src.keyset_id, dst->mutable_keyset_id());
    proto_wrap(src.keyset_history_id, dst->mutable_keyset_history_id());
    proto_wrap_vector_of_dnskeys(src.dns_keys, dst->mutable_dns_keys());
    proto_wrap_vector_of_contacts(src.technical_contacts, dst->mutable_technical_contacts());
    proto_wrap(src.representative_events, dst->mutable_events());
    if (src.domains_count != boost::none)
    {
        dst->set_domains_count(*src.domains_count);
    }
    else
    {
        dst->clear_domains_count();
    }
    Registry::proto_wrap(src.sponsoring_registrar, dst->mutable_sponsoring_registrar());
}

void proto_wrap(const Keyset::KeysetIdReply::Data& src, Api::Keyset::KeysetIdReply::Data* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_keyset_id());
    proto_wrap(src.keyset_history_id, dst->mutable_keyset_history_id());
}

template <typename T>
void proto_wrap_keyset_state_flags(const Keyset::KeysetStateFlagsInfo& src, T* dst)
{
    for (const auto& flag : src)
    {
        Registry::Api::StateFlag::Traits traits;
        switch (flag.how_to_set)
        {
            case Keyset::StateFlagInfo::Manipulation::automatic:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::automatic);
                break;
            case Keyset::StateFlagInfo::Manipulation::manual:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::manual);
                break;
        }
        switch (flag.visibility)
        {
            case Keyset::StateFlagInfo::Visibility::external:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::external);
                break;
            case Keyset::StateFlagInfo::Visibility::internal:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::internal);
                break;
        }
        (*dst)[flag.name] = traits;
    }
}

template <typename T>
void proto_wrap_keyset_state(const Keyset::KeysetState& src, T* dst)
{
    for (const auto& name_presence : src.flag_presents)
    {
        (*dst)[name_presence.first] = name_presence.second;
    }
}

void proto_wrap(
        const Keyset::KeysetStateHistory::Record& record,
        const std::vector<std::string>& flags_names,
        Api::Keyset::KeysetStateHistory::Record* dst)
{
    if (record.presents.size() != flags_names.size())
    {
        throw std::runtime_error("number of flags must be equal to the number of flags names");
    }
    proto_wrap(record.valid_from, dst->mutable_valid_from());
    for (std::size_t idx = 0; idx < record.presents.size(); ++idx)
    {
        (*(dst->mutable_flags()))[flags_names[idx]] = record.presents[idx];
    }
}

void proto_wrap(const Keyset::KeysetStateHistory& src, Api::Keyset::KeysetStateHistory* dst)
{
    dst->mutable_timeline()->Reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        proto_wrap(record, src.flags_names, dst->mutable_timeline()->Add());
    }
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
}

void proto_wrap(const Keyset::KeysetDataHistory::Record& src, Api::Keyset::KeysetHistoryRecord* dst)
{
    proto_wrap(src.keyset_history_id, dst->mutable_keyset_history_id());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    proto_wrap(src.log_entry_id, dst->mutable_log_entry_id());
}

template <typename T>
void proto_wrap_vector_of_keyset_data_history_record(const std::vector<Keyset::KeysetDataHistory::Record>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::KeysetDataHistory& src, Api::Keyset::KeysetHistoryReply::Data* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_keyset_id());
    proto_wrap_vector_of_keyset_data_history_record(src.timeline, dst->mutable_timeline());
    if (src.valid_to != boost::none)
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    else
    {
        dst->clear_valid_to();
    }
}

void proto_wrap(const Keyset::KeysetDataHistoryReply::Data& src, Api::Keyset::KeysetHistoryReply::Data* dst)
{
    proto_wrap(src.history, dst);
}

void proto_wrap(const Keyset::KeysetLifetime::TimeSpec& src, Api::Keyset::KeysetLifetime::TimeSpec* dst)
{
    proto_wrap(src.keyset_history_id, dst->mutable_keyset_history_id());
    proto_wrap(src.timestamp, dst->mutable_timestamp());
}

void proto_wrap(const Keyset::KeysetLifetime& src, Api::Keyset::KeysetLifetime* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_keyset_id());
    proto_wrap(src.begin, dst->mutable_registered_from());
    if (src.end != boost::none)
    {
        proto_wrap(*src.end, dst->mutable_registered_to());
    }
    else
    {
        dst->clear_registered_to();
    }
}

template <typename T>
void proto_wrap_vector_of_keyset_lifetime(const std::vector<Keyset::KeysetLifetime>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::KeysetHandleHistoryReply::Data& src, Api::Keyset::KeysetHandleHistoryReply::Data* dst)
{
    proto_wrap(src.keyset_handle, dst->mutable_keyset_handle());
    proto_wrap_vector_of_keyset_lifetime(src.timeline, dst->mutable_timeline());
}

void proto_wrap(const Keyset::KeysetStateFlagsInfo& src, Api::Keyset::KeysetStateFlagsReply::Data* dst)
{
    proto_wrap_keyset_state_flags(src, dst->mutable_traits());
}

void proto_wrap(Keyset::KeysetItem src, std::string& dst)
{
    switch (src)
    {
        case Keyset::KeysetItem::keyset_handle:
            dst = "keyset_handle";
            return;
        case Keyset::KeysetItem::public_key:
            dst = "dns_keys.key";
            return;
    }
    throw std::runtime_error("unexpected value of Keyset::KeysetItem");
}

template <typename T>
void proto_wrap_set_of_keyset_matched_items(const std::set<Keyset::KeysetItem>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto item : src)
    {
        proto_wrap(item, *(dst->Add()));
    }
}

void proto_wrap(const Keyset::SearchKeysetReply::Data::Result& src, Api::Keyset::SearchKeysetReply::Data::Result* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_object_id());
    proto_wrap_set_of_keyset_matched_items(src.matched_items, dst->mutable_matched_items());
}

template <typename T>
void proto_wrap_vector_of_search_keyset_result(const std::vector<Keyset::SearchKeysetReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::SearchKeysetReply::Data::FuzzyValue& src,
                Api::Keyset::SearchKeysetReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

template <typename T>
void proto_wrap_set_of_keyset_items(const std::set<Keyset::KeysetItem>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto item : src)
    {
        *(dst->Add()) = Util::Into<std::string>::from(item);
    }
}

void proto_wrap(const Keyset::SearchKeysetReply::Data& src, Api::Keyset::SearchKeysetReply::Data* dst)
{
    proto_wrap_vector_of_search_keyset_result(src.most_similar_keysets, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_keyset_items(src.searched_items, dst->mutable_searched_items());
}

template <typename T>
void proto_wrap_vector_of_keyset_history_id(
    const std::vector<Keyset::KeysetHistoryId>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::SearchKeysetHistoryReply::Data::Result::HistoryPeriod& src,
                Api::Keyset::SearchKeysetHistoryReply::Data::Result::HistoryPeriod* dst)
{
    proto_wrap_set_of_keyset_items(src.matched_items, dst->mutable_matched_items());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    proto_wrap_vector_of_keyset_history_id(src.keyset_history_ids, dst->mutable_keyset_history_ids());
}

template <typename T>
void proto_wrap_vector_of_search_keyset_history_result_histories(
    const std::vector<Keyset::SearchKeysetHistoryReply::Data::Result::HistoryPeriod>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::SearchKeysetHistoryReply::Data::Result& src,
                Api::Keyset::SearchKeysetHistoryReply::Data::Result* dst)
{
    proto_wrap(src.object_id, dst->mutable_object_id());
    proto_wrap_vector_of_search_keyset_history_result_histories(src.histories, dst->mutable_histories());
}

template <typename T>
void proto_wrap_vector_of_search_keyset_history_result(const std::vector<Keyset::SearchKeysetHistoryReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Keyset::SearchKeysetHistoryReply::Data::FuzzyValue& src,
                Api::Keyset::SearchKeysetHistoryReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

void proto_wrap(const Keyset::SearchKeysetHistoryReply::Data& src, Api::Keyset::SearchKeysetHistoryReply::Data* dst)
{
    proto_wrap_vector_of_search_keyset_history_result(src.results, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_keyset_items(src.searched_items, dst->mutable_searched_items());
}

void proto_wrap(const Keyset::KeysetLightInfo& src, Api::Keyset::KeysetRef* dst)
{
    proto_wrap(src.id, dst->mutable_id());
    proto_wrap(src.handle, dst->mutable_handle());
}

}//namespace Fred::Registry::{anonymous}

template <>
void proto_wrap(const Keyset::KeysetLightInfo& src, std::tuple<Api::Keyset::KeysetRef*, Api::Keyset::KeysetId*>* dst)
{
    proto_wrap(src.id, std::get<Api::Keyset::KeysetId*>(*dst));
    proto_wrap(src.id, std::get<Api::Keyset::KeysetRef*>(*dst)->mutable_id());
    proto_wrap(src.handle, std::get<Api::Keyset::KeysetRef*>(*dst)->mutable_handle());
}

template <>
void proto_wrap(const Keyset::KeysetInfoReply::Data& src, Api::Keyset::KeysetInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::KeysetInfoReply::Exception::InvalidData& src, Api::Keyset::KeysetInfoReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Keyset::KeysetInfoReply::Exception::KeysetDoesNotExist&, Api::Keyset::KeysetInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_keyset_does_not_exist();
}

void proto_wrap(const Keyset::KeysetInfoReply::Result& src, Api::Keyset::KeysetInfoReply* dst)
{
    class Visitor : public boost::static_visitor<void>
    {
    public:
        explicit Visitor(Api::Keyset::KeysetInfoReply* dst)
            : dst_{dst}
        { }
        void operator()(const Keyset::KeysetInfoReply::Data& data) const
        {
            proto_wrap(data, dst_->mutable_data());
        }
        void operator()(const Keyset::KeysetInfoReply::Exception::InvalidData& exception) const
        {
            proto_wrap(exception, dst_);
        }
        void operator()(const Keyset::KeysetInfoReply::Exception::KeysetDoesNotExist& exception) const
        {
            proto_wrap(exception, dst_);
        }
    private:
        Api::Keyset::KeysetInfoReply* dst_;
    };
    boost::apply_visitor(Visitor{dst}, src);
}

template <>
struct RegistryTypeTraits<Keyset::KeysetInfoRequest>
{
    using ProtoType = Api::Keyset::KeysetInfoRequest;
};

template <>
void proto_wrap(const Keyset::KeysetInfoRequest& src, Api::Keyset::KeysetInfoRequest* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_keyset_id());
    if (src.keyset_history_id == boost::none)
    {
        dst->clear_keyset_history_id();
    }
    else
    {
        proto_wrap(*src.keyset_history_id, dst->mutable_keyset_history_id());
    }
    dst->set_include_domains_count(src.include_domains_count);
}

template <>
struct RegistryTypeTraits<Keyset::KeysetInfoReply::Exception>
{
    using ProtoType = Api::Keyset::KeysetInfoReply::Exception;
};

template <>
void proto_wrap(const Keyset::KeysetInfoReply::Exception& src, Api::Keyset::KeysetInfoReply::Exception* dst)
{
    class Wrapper : public boost::static_visitor<void>
    {
    public:
        explicit Wrapper(Api::Keyset::KeysetInfoReply::Exception* dst)
            : dst_{dst}
        { }
        void operator()(const Keyset::KeysetInfoReply::Exception::KeysetDoesNotExist&) const
        {
            dst_->mutable_keyset_does_not_exist();
        }
        void operator()(const Keyset::KeysetInfoReply::Exception::InvalidData& exception) const
        {
            wrap_invalid_data_exception(exception, dst_->mutable_invalid_data());
        }
    private:
        Api::Keyset::KeysetInfoReply::Exception* dst_;    
    };
    boost::apply_visitor(Wrapper{dst}, src.reasons);
}

template <>
struct RegistryTypeTraits<Keyset::BatchKeysetInfoRequest::Request>
{
    using ProtoType = Api::Keyset::KeysetInfoRequest;
};

template <>
void proto_wrap(const Keyset::BatchKeysetInfoRequest::Request& src, Api::Keyset::KeysetInfoRequest* dst)
{
    dst->mutable_keyset_id()->mutable_uuid()->set_value(src.keyset_id);
    if (src.keyset_history_id.empty())
    {
        dst->clear_keyset_history_id();
    }
    else
    {
        dst->mutable_keyset_history_id()->mutable_uuid()->set_value(src.keyset_history_id);
    }
    dst->set_include_domains_count(src.include_domains_count);
}

template <>
struct RegistryTypeTraits<Keyset::BatchKeysetInfoReply::Error>
{
    using ProtoType = Api::Keyset::BatchKeysetInfoReply::Error;
};

template <>
void proto_wrap(const Keyset::BatchKeysetInfoReply::Error& src, Api::Keyset::BatchKeysetInfoReply::Error* dst)
{
    proto_wrap(src.request, dst->mutable_request());
    proto_wrap(src.exception, dst->mutable_exception());
}

template <>
struct RegistryTypeTraits<Keyset::BatchKeysetInfoReply::Data::BatchReply>
{
    using ProtoType = Api::Keyset::BatchKeysetInfoReply::Data::BatchReply;
};

template <>
void proto_wrap(
        const Keyset::BatchKeysetInfoReply::Data::BatchReply& src,
        Api::Keyset::BatchKeysetInfoReply::Data::BatchReply* dst)
{
    class Wrapper : public boost::static_visitor<void>
    {
    public:
        Wrapper(Api::Keyset::BatchKeysetInfoReply::Data::BatchReply* dst)
            : dst_{dst}
        { }
        void operator()(const Keyset::KeysetInfoReply::Data& data) const
        {
            proto_wrap(data, dst_->mutable_data());
        }
        void operator()(const Keyset::BatchKeysetInfoReply::Error& error) const
        {
            proto_wrap(error, dst_->mutable_error());
        }
    private:
        Api::Keyset::BatchKeysetInfoReply::Data::BatchReply* dst_;    
    };
    boost::apply_visitor(Wrapper{dst}, src.data_or_error);
}

std::size_t proto_wrap(
        const Keyset::BatchKeysetInfoReply::Data& src,
        std::size_t offset,
        Api::Keyset::BatchKeysetInfoReply* dst)
{
    static constexpr auto max_number_of_replies = 1000;
    dst->Clear();
    if (src.replies.size() <= offset)
    {
        return 0;
    }
    const auto replies_to_send = src.replies.size() - offset;
    const auto chunk_size = replies_to_send < max_number_of_replies ? replies_to_send
                                                                    : max_number_of_replies;
    const auto next_offset = offset + chunk_size;
    auto* const replies = dst->mutable_data()->mutable_replies();
    replies->Reserve(chunk_size);
    std::for_each(cbegin(src.replies) + offset, cbegin(src.replies) + next_offset, [&replies](auto&& record)
    {
        proto_wrap(record, replies->Add());
    });
    const bool last_chunk = src.replies.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Keyset::KeysetIdReply::Data& src, Api::Keyset::KeysetIdReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::KeysetIdReply::Exception::KeysetDoesNotExist&, Api::Keyset::KeysetIdReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_keyset_does_not_exist();
}

template <>
void proto_wrap(const Keyset::KeysetStateFlagsInfo& src, Api::Keyset::KeysetStateFlagsReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::KeysetDataHistoryReply::Data& src, Api::Keyset::KeysetHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::KeysetDataHistoryReply::Exception::KeysetDoesNotExist&,
                Api::Keyset::KeysetHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_keyset_does_not_exist();
}

template <>
void proto_wrap(const Keyset::KeysetDataHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Keyset::KeysetHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Keyset::KeysetHandleHistoryReply::Data& src, Api::Keyset::KeysetHandleHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::KeysetStateReply::Data& src, Api::Keyset::KeysetStateReply* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_data()->mutable_keyset_id());
    proto_wrap_keyset_state(src.state, dst->mutable_data()->mutable_state()->mutable_flags());
}

template <>
void proto_wrap(const Keyset::KeysetStateReply::Exception::KeysetDoesNotExist&, Api::Keyset::KeysetStateReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_keyset_does_not_exist();
}

template <>
void proto_wrap(const Keyset::KeysetStateHistoryReply::Data& src, Api::Keyset::KeysetStateHistoryReply* dst)
{
    proto_wrap(src.keyset_id, dst->mutable_data()->mutable_keyset_id());
    proto_wrap(src.history, dst->mutable_data()->mutable_history());
}

template <>
void proto_wrap(const Keyset::KeysetStateHistoryReply::Exception::KeysetDoesNotExist&,
                Api::Keyset::KeysetStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_keyset_does_not_exist();
}

template <>
void proto_wrap(const Keyset::KeysetStateHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Keyset::KeysetStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
struct RegistryTypeTraits<Keyset::ListKeysetsByContactReply::Data::Keyset>
{
    using ProtoType = Api::Keyset::ListKeysetsByContactReply::Data::Keyset;
};

template <>
void proto_wrap(const Keyset::ListKeysetsByContactReply::Data::Keyset& src, Api::Keyset::ListKeysetsByContactReply::Data::Keyset* dst)
{
    proto_wrap(src.keyset, dst->mutable_keyset());
    proto_wrap(src.keyset_history_id, dst->mutable_keyset_history_id());
    dst->set_is_deleted(src.is_deleted);
}

std::size_t proto_wrap(
        const Keyset::ListKeysetsByContactReply::Data& src,
        std::size_t offset,
        Api::Keyset::ListKeysetsByContactReply::Data* dst)
{
    static constexpr auto max_number_of_keysets = 1000;
    dst->Clear();
    if (src.keysets.size() <= offset)
    {
        return 0;
    }
    const auto keysets_to_send = src.keysets.size() - offset;
    const auto chunk_size = keysets_to_send < max_number_of_keysets ? keysets_to_send
                                                                  : max_number_of_keysets;
    const auto next_offset = offset + chunk_size;
    auto* const keysets = dst->mutable_keysets();
    keysets->Reserve(chunk_size);
    std::for_each(begin(src.keysets) + offset, begin(src.keysets) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, keysets->Add());
            });
    const bool first_chunk = offset == 0;
    if (first_chunk &&
        (src.pagination != boost::none))
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    const bool last_chunk = src.keysets.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Keyset::ListKeysetsByContactReply::Exception::ContactDoesNotExist&, Api::Keyset::ListKeysetsByContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Keyset::ListKeysetsByContactReply::Exception::InvalidData& src, Api::Keyset::ListKeysetsByContactReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Keyset::SearchKeysetReply::Data& src, Api::Keyset::SearchKeysetReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::SearchKeysetHistoryReply::Data& src, Api::Keyset::SearchKeysetHistoryReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Keyset::SearchKeysetHistoryReply::Exception::ChronologyViolation&, Api::Keyset::SearchKeysetHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_chronology_violation();
}

namespace {

void proto_wrap(const Nsset::NssetHandle& src, Api::Nsset::NssetHandle* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const Nsset::NssetId& src, Api::Nsset::NssetId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Nsset::NssetHistoryId& src, Api::Nsset::NssetHistoryId* dst)
{
    proto_wrap_uuid(src, dst->mutable_uuid());
}

void proto_wrap(const Nsset::DnsHost& src, Api::Nsset::DnsHost* dst)
{
    dst->set_fqdn(src.fqdn);
    for (const auto& ip_address : src.ip_addresses)
    {
        dst->add_ip_addresses(ip_address.to_string());
    }
}

template <typename T>
void proto_wrap_vector_of_dnshosts(const std::vector<Nsset::DnsHost>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::TechnicalCheckLevel& src, Api::Nsset::TechnicalCheckLevel* dst)
{
    dst->set_value(src.technical_check_level);
}

void proto_wrap(const Nsset::NssetInfoReply::Data& src, Api::Nsset::NssetInfoReply::Data* dst)
{
    proto_wrap(src.nsset_handle, dst->mutable_nsset_handle());
    proto_wrap(src.nsset_id, dst->mutable_nsset_id());
    proto_wrap(src.nsset_history_id, dst->mutable_nsset_history_id());
    proto_wrap_vector_of_dnshosts(src.dns_hosts, dst->mutable_dns_hosts());
    proto_wrap_vector_of_contacts(src.technical_contacts, dst->mutable_technical_contacts_old());
    proto_wrap_vector_of_contacts(src.technical_contacts, dst->mutable_technical_contacts());
    if (src.technical_check_level != boost::none)
    {
        proto_wrap(*src.technical_check_level, dst->mutable_technical_check_level());
    }
    else
    {
        dst->clear_technical_check_level();
    }
    proto_wrap(src.representative_events, dst->mutable_events());
    if (src.domains_count != boost::none)
    {
        dst->set_domains_count(*src.domains_count);
    }
    else
    {
        dst->clear_domains_count();
    }
    Registry::proto_wrap(src.sponsoring_registrar, dst->mutable_sponsoring_registrar());
}

void proto_wrap(const Nsset::NssetIdReply::Data& src, Api::Nsset::NssetIdReply::Data* dst)
{
    proto_wrap(src.nsset_id, dst->mutable_nsset_id());
    proto_wrap(src.nsset_history_id, dst->mutable_nsset_history_id());
}

template <typename T>
void proto_wrap_nsset_state_flags(const Nsset::NssetStateFlagsInfo& src, T* dst)
{
    for (const auto& flag : src)
    {
        Registry::Api::StateFlag::Traits traits;
        switch (flag.how_to_set)
        {
            case Nsset::StateFlagInfo::Manipulation::automatic:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::automatic);
                break;
            case Nsset::StateFlagInfo::Manipulation::manual:
                traits.set_how_to_set(Registry::Api::StateFlag::Manipulation::manual);
                break;
        }
        switch (flag.visibility)
        {
            case Nsset::StateFlagInfo::Visibility::external:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::external);
                break;
            case Nsset::StateFlagInfo::Visibility::internal:
                traits.set_visibility(Registry::Api::StateFlag::Visibility::internal);
                break;
        }
        (*dst)[flag.name] = traits;
    }
}

template <typename T>
void proto_wrap_nsset_state(const Nsset::NssetState& src, T* dst)
{
    for (const auto& name_presence : src.flag_presents)
    {
        (*dst)[name_presence.first] = name_presence.second;
    }
}

void proto_wrap(
        const Nsset::NssetStateHistory::Record& record,
        const std::vector<std::string>& flags_names,
        Api::Nsset::NssetStateHistory::Record* dst)
{
    if (record.presents.size() != flags_names.size())
    {
        throw std::runtime_error("number of flags must be equal to the number of flags names");
    }
    proto_wrap(record.valid_from, dst->mutable_valid_from());
    for (std::size_t idx = 0; idx < record.presents.size(); ++idx)
    {
        (*(dst->mutable_flags()))[flags_names[idx]] = record.presents[idx];
    }
}

void proto_wrap(const Nsset::NssetStateHistory& src, Api::Nsset::NssetStateHistory* dst)
{
    dst->mutable_timeline()->Reserve(src.timeline.size());
    for (const auto& record : src.timeline)
    {
        proto_wrap(record, src.flags_names, dst->mutable_timeline()->Add());
    }
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
}

void proto_wrap(const Nsset::NssetDataHistory::Record& src, Api::Nsset::NssetHistoryRecord* dst)
{
    proto_wrap(src.nsset_history_id, dst->mutable_nsset_history_id());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    proto_wrap(src.log_entry_id, dst->mutable_log_entry_id());
}

template <typename T>
void proto_wrap_vector_of_nsset_data_history_record(const std::vector<Nsset::NssetDataHistory::Record>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::NssetDataHistory& src, Api::Nsset::NssetHistoryReply::Data* dst)
{
    proto_wrap(src.nsset_id, dst->mutable_nsset_id());
    proto_wrap_vector_of_nsset_data_history_record(src.timeline, dst->mutable_timeline());
    if (src.valid_to != boost::none)
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    else
    {
        dst->clear_valid_to();
    }
}

void proto_wrap(const Nsset::NssetDataHistoryReply::Data& src, Api::Nsset::NssetHistoryReply::Data* dst)
{
    proto_wrap(src.history, dst);
}

void proto_wrap(const Nsset::NssetLifetime::TimeSpec& src, Api::Nsset::NssetLifetime::TimeSpec* dst)
{
    proto_wrap(src.nsset_history_id, dst->mutable_nsset_history_id());
    proto_wrap(src.timestamp, dst->mutable_timestamp());
}

void proto_wrap(const Nsset::NssetLifetime& src, Api::Nsset::NssetLifetime* dst)
{
    proto_wrap(src.nsset_id, dst->mutable_nsset_id());
    proto_wrap(src.begin, dst->mutable_registered_from());
    if (src.end != boost::none)
    {
        proto_wrap(*src.end, dst->mutable_registered_to());
    }
    else
    {
        dst->clear_registered_to();
    }
}

template <typename T>
void proto_wrap_vector_of_nsset_lifetime(const std::vector<Nsset::NssetLifetime>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::NssetHandleHistoryReply::Data& src, Api::Nsset::NssetHandleHistoryReply::Data* dst)
{
    proto_wrap(src.nsset_handle, dst->mutable_nsset_handle());
    proto_wrap_vector_of_nsset_lifetime(src.timeline, dst->mutable_timeline());
}

void proto_wrap(const Nsset::NssetStateFlagsInfo& src, Api::Nsset::NssetStateFlagsReply::Data* dst)
{
    proto_wrap_nsset_state_flags(src, dst->mutable_traits());
}

void proto_wrap(Nsset::NssetItem src, std::string& dst)
{
    switch (src)
    {
        case Nsset::NssetItem::nsset_handle:
            dst = "nsset_handle";
            return;
        case Nsset::NssetItem::dns_hosts_fqdn:
            dst = "dns_hosts.fqdn";
            return;
        case Nsset::NssetItem::dns_hosts_ip_addresses:
            dst = "dns_hosts.ip_addresses";
            return;
    }
    throw std::runtime_error("unexpected value of Nsset::NssetItem");
}

template <typename T>
void proto_wrap_set_of_nsset_matched_items(const std::set<Nsset::NssetItem>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto item : src)
    {
        proto_wrap(item, *(dst->Add()));
    }
}

void proto_wrap(const Nsset::SearchNssetReply::Data::Result& src, Api::Nsset::SearchNssetReply::Data::Result* dst)
{
    proto_wrap(src.nsset_id, dst->mutable_object_id());
    proto_wrap_set_of_nsset_matched_items(src.matched_items, dst->mutable_matched_items());
}

template <typename T>
void proto_wrap_vector_of_search_nsset_result(const std::vector<Nsset::SearchNssetReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::SearchNssetReply::Data::FuzzyValue& src,
                Api::Nsset::SearchNssetReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

template <typename T>
void proto_wrap_set_of_nsset_items(const std::set<Nsset::NssetItem>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto item : src)
    {
        *(dst->Add()) = Util::Into<std::string>::from(item);
    }
}

void proto_wrap(const Nsset::SearchNssetReply::Data& src, Api::Nsset::SearchNssetReply::Data* dst)
{
    proto_wrap_vector_of_search_nsset_result(src.most_similar_nssets, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_nsset_items(src.searched_items, dst->mutable_searched_items());
}

template <typename T>
void proto_wrap_vector_of_nsset_history_id(
    const std::vector<Nsset::NssetHistoryId>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::SearchNssetHistoryReply::Data::Result::HistoryPeriod& src,
                Api::Nsset::SearchNssetHistoryReply::Data::Result::HistoryPeriod* dst)
{
    proto_wrap_set_of_nsset_items(src.matched_items, dst->mutable_matched_items());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    if (src.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    proto_wrap_vector_of_nsset_history_id(src.nsset_history_ids, dst->mutable_nsset_history_ids());
}

template <typename T>
void proto_wrap_vector_of_search_nsset_history_result_histories(
    const std::vector<Nsset::SearchNssetHistoryReply::Data::Result::HistoryPeriod>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::SearchNssetHistoryReply::Data::Result& src,
                Api::Nsset::SearchNssetHistoryReply::Data::Result* dst)
{
    proto_wrap(src.object_id, dst->mutable_object_id());
    proto_wrap_vector_of_search_nsset_history_result_histories(src.histories, dst->mutable_histories());
}

template <typename T>
void proto_wrap_vector_of_search_nsset_history_result(const std::vector<Nsset::SearchNssetHistoryReply::Data::Result>& src, T* dst)
{
    dst->Reserve(src.size());
    for (const auto& record : src)
    {
        proto_wrap(record, dst->Add());
    }
}

void proto_wrap(const Nsset::SearchNssetHistoryReply::Data::FuzzyValue& src,
                Api::Nsset::SearchNssetHistoryReply::Data::FuzzyValue* dst)
{
    dst->set_lower_estimate(src.lower_estimate);
    dst->set_upper_estimate(src.upper_estimate);
}

void proto_wrap(const Nsset::SearchNssetHistoryReply::Data& src, Api::Nsset::SearchNssetHistoryReply::Data* dst)
{
    proto_wrap_vector_of_search_nsset_history_result(src.results, dst->mutable_results());
    proto_wrap(src.result_count, dst->mutable_estimated_total());
    proto_wrap_set_of_nsset_items(src.searched_items, dst->mutable_searched_items());
}

void proto_wrap(const Nsset::NssetLightInfo& src, Api::Nsset::NssetRef* dst)
{
    proto_wrap(src.id, dst->mutable_id());
    proto_wrap(src.handle, dst->mutable_handle());
}

}//namespace Fred::Registry::{anonymous}

template <>
void proto_wrap(const Nsset::NssetLightInfo& src, std::tuple<Api::Nsset::NssetRef*, Api::Nsset::NssetId*>* dst)
{
    proto_wrap(src.id, std::get<Api::Nsset::NssetId*>(*dst));
    proto_wrap(src.id, std::get<Api::Nsset::NssetRef*>(*dst)->mutable_id());
    proto_wrap(src.handle, std::get<Api::Nsset::NssetRef*>(*dst)->mutable_handle());
}

template <>
void proto_wrap(const Nsset::NssetInfoReply::Data& src, Api::Nsset::NssetInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::NssetInfoReply::Exception::InvalidData& src, Api::Nsset::NssetInfoReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Nsset::NssetInfoReply::Exception::NssetDoesNotExist&, Api::Nsset::NssetInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_nsset_does_not_exist();
}

void proto_wrap(const Nsset::NssetInfoReply::Result& src, Api::Nsset::NssetInfoReply* dst)
{
    class Visitor : boost::static_visitor<void>
    {
    public:
        explicit Visitor(Api::Nsset::NssetInfoReply* dst)
            : dst_{dst}
        { }
        void operator()(const Nsset::NssetInfoReply::Data& data)
        {
            proto_wrap(data, dst_);
        }
        void operator()(const Nsset::NssetInfoReply::Exception::InvalidData& exception)
        {
            proto_wrap(exception, dst_);
        }
        void operator()(const Nsset::NssetInfoReply::Exception::NssetDoesNotExist& exception)
        {
            proto_wrap(exception, dst_);
        }
    private:
        Api::Nsset::NssetInfoReply* dst_;
    };
    boost::apply_visitor(Visitor{dst}, src);
}

template <>
struct RegistryTypeTraits<Nsset::NssetInfoReply::Exception>
{
    using ProtoType = Api::Nsset::NssetInfoReply::Exception;
};

template <>
void proto_wrap(const Nsset::NssetInfoReply::Exception& src, Api::Nsset::NssetInfoReply::Exception* dst)
{
    class Wrapper : public boost::static_visitor<void>
    {
    public:
        explicit Wrapper(Api::Nsset::NssetInfoReply::Exception* dst)
            : dst_{dst}
        { }
        void operator()(const Nsset::NssetInfoReply::Exception::NssetDoesNotExist&) const
        {
            dst_->mutable_nsset_does_not_exist();
        }
        void operator()(const Nsset::NssetInfoReply::Exception::InvalidData& exception) const
        {
            wrap_invalid_data_exception(exception, dst_->mutable_invalid_data());
        }
    private:
        Api::Nsset::NssetInfoReply::Exception* dst_;    
    };
    boost::apply_visitor(Wrapper{dst}, src.reasons);
}

template <>
struct RegistryTypeTraits<Nsset::BatchNssetInfoRequest::Request>
{
    using ProtoType = Api::Nsset::NssetInfoRequest;
};

template <>
void proto_wrap(const Nsset::BatchNssetInfoRequest::Request& src, Api::Nsset::NssetInfoRequest* dst)
{
    dst->mutable_nsset_id()->mutable_uuid()->set_value(src.nsset_id);
    if (src.nsset_history_id.empty())
    {
        dst->clear_nsset_history_id();
    }
    else
    {
        dst->mutable_nsset_history_id()->mutable_uuid()->set_value(src.nsset_history_id);
    }
    dst->set_include_domains_count(src.include_domains_count);
}

template <>
struct RegistryTypeTraits<Nsset::BatchNssetInfoReply::Error>
{
    using ProtoType = Api::Nsset::BatchNssetInfoReply::Error;
};

template <>
void proto_wrap(const Nsset::BatchNssetInfoReply::Error& src, Api::Nsset::BatchNssetInfoReply::Error* dst)
{
    proto_wrap(src.request, dst->mutable_request());
    proto_wrap(src.exception, dst->mutable_exception());
}

template <>
struct RegistryTypeTraits<Nsset::BatchNssetInfoReply::Data::BatchReply>
{
    using ProtoType = Api::Nsset::BatchNssetInfoReply::Data::BatchReply;
};

template <>
void proto_wrap(
        const Nsset::BatchNssetInfoReply::Data::BatchReply& src,
        Api::Nsset::BatchNssetInfoReply::Data::BatchReply* dst)
{
    class Wrapper : public boost::static_visitor<void>
    {
    public:
        Wrapper(Api::Nsset::BatchNssetInfoReply::Data::BatchReply* dst)
            : dst_{dst}
        { }
        void operator()(const Nsset::NssetInfoReply::Data& data) const
        {
            proto_wrap(data, dst_->mutable_data());
        }
        void operator()(const Nsset::BatchNssetInfoReply::Error& error) const
        {
            proto_wrap(error, dst_->mutable_error());
        }
    private:
        Api::Nsset::BatchNssetInfoReply::Data::BatchReply* dst_;    
    };
    boost::apply_visitor(Wrapper{dst}, src.data_or_error);
}

std::size_t proto_wrap(
        const Nsset::BatchNssetInfoReply::Data& src,
        std::size_t offset,
        Api::Nsset::BatchNssetInfoReply* dst)
{
    static constexpr auto max_number_of_replies = 1000;
    dst->Clear();
    if (src.replies.size() <= offset)
    {
        return 0;
    }
    const auto replies_to_send = src.replies.size() - offset;
    const auto chunk_size = replies_to_send < max_number_of_replies ? replies_to_send
                                                                    : max_number_of_replies;
    const auto next_offset = offset + chunk_size;
    auto* const replies = dst->mutable_data()->mutable_replies();
    replies->Reserve(chunk_size);
    std::for_each(cbegin(src.replies) + offset, cbegin(src.replies) + next_offset, [&replies](auto&& record)
    {
        proto_wrap(record, replies->Add());
    });
    const bool last_chunk = src.replies.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Nsset::NssetIdReply::Data& src, Api::Nsset::NssetIdReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::NssetIdReply::Exception::NssetDoesNotExist&, Api::Nsset::NssetIdReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_nsset_does_not_exist();
}

template <>
void proto_wrap(const Nsset::NssetStateFlagsInfo& src, Api::Nsset::NssetStateFlagsReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::NssetDataHistoryReply::Data& src, Api::Nsset::NssetHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::NssetDataHistoryReply::Exception::NssetDoesNotExist&,
                Api::Nsset::NssetHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_nsset_does_not_exist();
}

template <>
void proto_wrap(const Nsset::NssetDataHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Nsset::NssetHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Nsset::NssetHandleHistoryReply::Data& src, Api::Nsset::NssetHandleHistoryReply* dst)
{
    dst->clear_data();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::NssetStateReply::Data& src, Api::Nsset::NssetStateReply* dst)
{
    proto_wrap(src.nsset_id, dst->mutable_data()->mutable_nsset_id());
    proto_wrap_nsset_state(src.state, dst->mutable_data()->mutable_state()->mutable_flags());
}

template <>
void proto_wrap(const Nsset::NssetStateReply::Exception::NssetDoesNotExist&, Api::Nsset::NssetStateReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_nsset_does_not_exist();
}

template <>
void proto_wrap(const Nsset::NssetStateHistoryReply::Data& src, Api::Nsset::NssetStateHistoryReply* dst)
{
    proto_wrap(src.nsset_id, dst->mutable_data()->mutable_nsset_id());
    proto_wrap(src.history, dst->mutable_data()->mutable_history());
}

template <>
void proto_wrap(const Nsset::NssetStateHistoryReply::Exception::NssetDoesNotExist&,
                Api::Nsset::NssetStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_nsset_does_not_exist();
}

template <>
void proto_wrap(const Nsset::NssetStateHistoryReply::Exception::InvalidHistoryInterval&,
                Api::Nsset::NssetStateHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_invalid_history_interval();
}

template <>
void proto_wrap(const Nsset::CheckDnsHostReply::Data& src, Api::Nsset::CheckDnsHostReply* dst)
{
    dst->mutable_data()->set_dns_host_exists(src.dns_host_exists);
}

std::size_t proto_wrap(
        const Nsset::ListNssetsReply::Data& src,
        std::size_t offset,
        Api::Nsset::ListNssetsReply::Data* dst)
{
    static constexpr auto max_number_of_nssets = 10000;
    dst->Clear();
    if (src.nssets.size() <= offset)
    {
        return 0;
    }
    const auto nssets_to_send = src.nssets.size() - offset;
    const auto chunk_size = nssets_to_send < max_number_of_nssets ? nssets_to_send
                                                                  : max_number_of_nssets;
    const auto next_offset = offset + chunk_size;
    auto* const nssets = dst->mutable_nssets();
    nssets->Reserve(chunk_size);
    std::for_each(begin(src.nssets) + offset, begin(src.nssets) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, nssets->Add());
            });
    const bool last_chunk = src.nssets.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
struct RegistryTypeTraits<Nsset::ListNssetsByContactReply::Data::Nsset>
{
    using ProtoType = Api::Nsset::ListNssetsByContactReply::Data::Nsset;
};

template <>
void proto_wrap(const Nsset::ListNssetsByContactReply::Data::Nsset& src, Api::Nsset::ListNssetsByContactReply::Data::Nsset* dst)
{
    proto_wrap(src.nsset, dst->mutable_nsset());
    proto_wrap(src.nsset_history_id, dst->mutable_nsset_history_id());
    dst->set_is_deleted(src.is_deleted);
}

std::size_t proto_wrap(
        const Nsset::ListNssetsByContactReply::Data& src,
        std::size_t offset,
        Api::Nsset::ListNssetsByContactReply::Data* dst)
{
    static constexpr auto max_number_of_nssets = 1000;
    dst->Clear();
    if (src.nssets.size() <= offset)
    {
        return 0;
    }
    const auto nssets_to_send = src.nssets.size() - offset;
    const auto chunk_size = nssets_to_send < max_number_of_nssets ? nssets_to_send
                                                                  : max_number_of_nssets;
    const auto next_offset = offset + chunk_size;
    auto* const nssets = dst->mutable_nssets();
    nssets->Reserve(chunk_size);
    std::for_each(begin(src.nssets) + offset, begin(src.nssets) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, nssets->Add());
            });
    const bool first_chunk = offset == 0;
    if (first_chunk &&
        (src.pagination != boost::none))
    {
        proto_wrap(*src.pagination, dst->mutable_pagination());
    }
    const bool last_chunk = src.nssets.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

template <>
void proto_wrap(const Nsset::ListNssetsByContactReply::Exception::ContactDoesNotExist&, Api::Nsset::ListNssetsByContactReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void proto_wrap(const Nsset::ListNssetsByContactReply::Exception::InvalidData& src, Api::Nsset::ListNssetsByContactReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Nsset::SearchNssetReply::Data& src, Api::Nsset::SearchNssetReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::SearchNssetHistoryReply::Data& src, Api::Nsset::SearchNssetHistoryReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Nsset::SearchNssetHistoryReply::Exception::ChronologyViolation&, Api::Nsset::SearchNssetHistoryReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_chronology_violation();
}


template <>
void proto_wrap(const Registrar::RegistrarCreditReply::Data& src, Api::Registrar::RegistrarCreditReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::RegistrarCreditReply::Exception::RegistrarDoesNotExist&,
                Api::Registrar::RegistrarCreditReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

void proto_wrap(const Nsset::SearchNssetReply::Data& src, Api::Nsset::SearchNssetReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::RegistrarInfoReply::Data& src, Api::Registrar::RegistrarInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::RegistrarInfoReply::Exception::RegistrarDoesNotExist&, Api::Registrar::RegistrarInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::RegistrarListReply::Data& src, Api::Registrar::RegistrarListReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::RegistrarZoneAccess& src, Api::Registrar::RegistrarZoneAccess* dst)
{
    proto_wrap(src.registrar_handle, dst->mutable_registrar_handle());
    proto_wrap(src.zone, dst->mutable_zone());
    proto_wrap(src.access.valid_from, dst->mutable_valid_from());
    if (src.access.valid_to == boost::none)
    {
        dst->clear_valid_to();
    }
    else
    {
        proto_wrap(*src.access.valid_to, dst->mutable_valid_to());
    }
}

template <>
void proto_wrap(const Registrar::RegistrarZoneAccessReply::Data& src, Api::Registrar::RegistrarZoneAccessReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::RegistrarZoneAccessReply::Exception::RegistrarDoesNotExist&,
                Api::Registrar::RegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::CreateRegistrarReply::Data& src, Api::Registrar::CreateRegistrarReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::CreateRegistrarReply::Exception::RegistrarAlreadyExists&, Api::Registrar::CreateRegistrarReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_already_exists();
}

template <>
void proto_wrap(const Registrar::CreateRegistrarReply::Exception::InvalidData& src, Api::Registrar::CreateRegistrarReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarReply::Data& src, Api::Registrar::UpdateRegistrarReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarReply::Exception::RegistrarDoesNotExist&, Api::Registrar::UpdateRegistrarReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarReply::Exception::InvalidData& src, Api::Registrar::UpdateRegistrarReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::AddRegistrarZoneAccessReply::Exception::RegistrarDoesNotExist&, Api::Registrar::AddRegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::AddRegistrarZoneAccessReply::Exception::ZoneDoesNotExist&, Api::Registrar::AddRegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_zone_does_not_exist();
}

template <>
void proto_wrap(const Registrar::AddRegistrarZoneAccessReply::Exception::InvalidData& src, Api::Registrar::AddRegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist&, Api::Registrar::DeleteRegistrarZoneAccessReply* dst)
{
    dst->mutable_exception()->mutable_registrar_zone_access_does_not_exist();
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarZoneAccessReply::Exception::HistoryChangeProhibited&, Api::Registrar::DeleteRegistrarZoneAccessReply* dst)
{
    dst->mutable_exception()->mutable_history_change_prohibited();
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarZoneAccessReply::Exception::InvalidData& src, Api::Registrar::DeleteRegistrarZoneAccessReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist&, Api::Registrar::UpdateRegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_zone_access_does_not_exist();
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarZoneAccessReply::Exception::HistoryChangeProhibited&, Api::Registrar::UpdateRegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_history_change_prohibited();
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarZoneAccessReply::Exception::InvalidData& src, Api::Registrar::UpdateRegistrarZoneAccessReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::GetRegistrarEppCredentialsReply::Data& src, Api::Registrar::GetRegistrarEppCredentialsReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::GetRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist&, Api::Registrar::GetRegistrarEppCredentialsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::AddRegistrarEppCredentialsReply::Data& src, Api::Registrar::AddRegistrarEppCredentialsReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::AddRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist&, Api::Registrar::AddRegistrarEppCredentialsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::AddRegistrarEppCredentialsReply::Exception::InvalidData& src, Api::Registrar::AddRegistrarEppCredentialsReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist&, Api::Registrar::DeleteRegistrarEppCredentialsReply* dst)
{
    dst->mutable_exception()->mutable_registrar_epp_credentials_does_not_exist();
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarEppCredentialsReply::Exception::InvalidData& src, Api::Registrar::DeleteRegistrarEppCredentialsReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist&, Api::Registrar::UpdateRegistrarEppCredentialsReply* dst)
{
    dst->mutable_exception()->mutable_registrar_epp_credentials_does_not_exist();
}

template <>
void proto_wrap(const Registrar::UpdateRegistrarEppCredentialsReply::Exception::InvalidData& src, Api::Registrar::UpdateRegistrarEppCredentialsReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void proto_wrap(const Registrar::GetRegistrarGroupsReply::Data& src, Api::Registrar::GetRegistrarGroupsReply* dst)
{
    proto_wrap(src, dst->mutable_data());
}

template <>
void proto_wrap(const Registrar::GetRegistrarGroupsMembershipReply::Exception::RegistrarDoesNotExist&, Api::Registrar::GetRegistrarGroupsMembershipReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::AddRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist&, Api::Registrar::AddRegistrarGroupMembershipReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::AddRegistrarGroupMembershipReply::Exception::GroupDoesNotExist&, Api::Registrar::AddRegistrarGroupMembershipReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_group_does_not_exist();
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist&, Api::Registrar::DeleteRegistrarGroupMembershipReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void proto_wrap(const Registrar::DeleteRegistrarGroupMembershipReply::Exception::GroupDoesNotExist&, Api::Registrar::DeleteRegistrarGroupMembershipReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_group_does_not_exist();
}

namespace {

void proto_wrap(const ContactRepresentative::ContactRepresentativeId& src, Api::ContactRepresentative::ContactRepresentativeId* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const ContactRepresentative::ContactRepresentativeHistoryId& src, Api::ContactRepresentative::ContactRepresentativeHistoryId* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const ContactRepresentative::ContactId& src, Api::Contact::ContactId* dst)
{
    dst->mutable_uuid()->set_value(get_raw_value_from(src));
}

void proto_wrap(const ContactRepresentative::ContactRepresentativeInfo& src, Api::ContactRepresentative::ContactRepresentativeInfo* dst)
{
    proto_wrap(src.id, dst->mutable_id());
    proto_wrap(src.history_id, dst->mutable_history_id());
    proto_wrap(src.contact_id, dst->mutable_contact_id());
    dst->set_name(src.name);
    dst->set_organization(src.organization);
    proto_wrap(src.email, dst->mutable_email());
    proto_wrap(src.telephone, dst->mutable_telephone());
    proto_wrap(src.place, dst->mutable_place());
}

void proto_wrap(const ContactRepresentative::ContactRepresentativeInfoReply::Data& src, Api::ContactRepresentative::ContactRepresentativeInfoReply::Data* dst)
{
    proto_wrap(src.contact_representative, dst->mutable_contact_representative());
}

void proto_wrap(const ContactRepresentative::CreateContactRepresentativeReply::Data& src, Api::ContactRepresentative::CreateContactRepresentativeReply::Data* dst)
{
    proto_wrap(src.contact_representative, dst->mutable_contact_representative());
}

void proto_wrap(const ContactRepresentative::UpdateContactRepresentativeReply::Data& src, Api::ContactRepresentative::UpdateContactRepresentativeReply::Data* dst)
{
    proto_wrap(src.contact_representative, dst->mutable_contact_representative());
}

void proto_wrap(const DomainBlacklist::BlockId& src, Api::DomainBlacklist::BlockId* dst)
{
    dst->set_value(Util::Into<std::string>::from(get_raw_value_from(src)));
}

void proto_wrap(const DomainBlacklist::RequestedBy& src, Api::DomainBlacklist::RequestedBy* dst)
{
    dst->set_value(get_raw_value_from(src));
}

void proto_wrap(const DomainBlacklist::BlockInfo& src, Api::DomainBlacklist::BlockInfo* dst)
{
    if (src.is_regex)
    {
        dst->set_regex(src.fqdn);
    }
    else
    {
        dst->set_fqdn(src.fqdn);
    }
    proto_wrap(src.requested_by, dst->mutable_requested_by());
    proto_wrap(src.block_id, dst->mutable_id());
    proto_wrap(src.valid_from, dst->mutable_valid_from());
    if (src.valid_to != boost::none)
    {
        proto_wrap(*src.valid_to, dst->mutable_valid_to());
    }
    else
    {
        dst->clear_valid_to();
    }
    dst->set_reason(src.reason);
}

void proto_wrap(const DomainBlacklist::BlockInfoReply::Data& src, Api::DomainBlacklist::BlockInfoReply::Data* dst)
{
    proto_wrap(src.block_info, dst->mutable_block());
}

void proto_wrap(const DomainBlacklist::CreateBlockReply::Data& src, Api::DomainBlacklist::CreateBlockReply::Data* dst)
{
    proto_wrap(src.block_id, dst->mutable_id());
}

void proto_wrap(const DomainBlacklist::DeleteBlockReply::Data& src, Api::DomainBlacklist::DeleteBlockReply::Data* dst)
{
    proto_wrap(src.block_id, dst->mutable_id());
}

void proto_wrap(const DomainBlacklist::BlockRef& src, Api::DomainBlacklist::BlockRef* dst)
{
    proto_wrap(src.block_id, dst->mutable_id());
    if (src.is_regex)
    {
        dst->set_regex(get_raw_value_from(src.fqdn));
    }
    else
    {
        dst->set_fqdn(get_raw_value_from(src.fqdn));
    }
}

} // namespace Fred::Registry::{anonymous}

std::size_t proto_wrap(
        const DomainBlacklist::ListBlocksReply::Data& src,
        std::size_t offset,
        Api::DomainBlacklist::ListBlocksReply::Data* dst)
{
    static constexpr auto max_number_of_blocks = 10000;
    dst->Clear();
    if (src.blocks.size() <= offset)
    {
        return 0;
    }
    const auto blocks_to_send = src.blocks.size() - offset;
    const auto chunk_size = blocks_to_send < max_number_of_blocks ? blocks_to_send
                                                                  : max_number_of_blocks;
    const auto next_offset = offset + chunk_size;
    auto* const blocks = dst->mutable_blocks();
    blocks->Reserve(chunk_size);
    std::for_each(begin(src.blocks) + offset, begin(src.blocks) + next_offset, [&](auto&& record)
            {
                proto_wrap(record, blocks->Add());
            });
    const bool last_chunk = src.blocks.size() <= next_offset;
    if (last_chunk)
    {
        return 0;
    }
    return next_offset;
}

}//namespace Fred::Registry
}//namespace Fred

using namespace Fred::Registry;

void Fred::Registry::proto_wrap(const Registrar::GetRegistrarGroupsMembershipReply::Data& src, Api::Registrar::GetRegistrarGroupsMembershipReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

void Fred::Registry::proto_wrap(const Registrar::AddRegistrarGroupMembershipReply::Data& src, Api::Registrar::AddRegistrarGroupMembershipReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

void Fred::Registry::proto_wrap(const Registrar::DeleteRegistrarGroupMembershipReply::Data& src, Api::Registrar::DeleteRegistrarGroupMembershipReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const Registrar::GetRegistrarCertificationsReply::Data& src, Api::Registrar::GetRegistrarCertificationsReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const Registrar::GetRegistrarCertificationsReply::Exception::RegistrarDoesNotExist&, Api::Registrar::GetRegistrarCertificationsReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

void Fred::Registry::proto_wrap(const Registrar::RegistrarCertification& src, Api::Registrar::AddRegistrarCertificationReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const Registrar::AddRegistrarCertificationReply::Exception::RegistrarDoesNotExist&, Api::Registrar::AddRegistrarCertificationReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_registrar_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const Registrar::AddRegistrarCertificationReply::Exception::InvalidData& src, Api::Registrar::AddRegistrarCertificationReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void Fred::Registry::proto_wrap(const Registrar::DeleteRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist&, Api::Registrar::DeleteRegistrarCertificationReply* dst)
{
    dst->mutable_exception()->mutable_registrar_certification_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const Registrar::DeleteRegistrarCertificationReply::Exception::InvalidData& src, Api::Registrar::DeleteRegistrarCertificationReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

void Fred::Registry::proto_wrap(const Registrar::RegistrarCertification& src, Api::Registrar::UpdateRegistrarCertificationReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const Registrar::UpdateRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist&, Api::Registrar::UpdateRegistrarCertificationReply* dst)
{
    dst->mutable_exception()->mutable_registrar_certification_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const Registrar::UpdateRegistrarCertificationReply::Exception::InvalidData& src, Api::Registrar::UpdateRegistrarCertificationReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::ContactRepresentativeInfoReply::Data& src, Api::ContactRepresentative::ContactRepresentativeInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::CreateContactRepresentativeReply::Data& src, Api::ContactRepresentative::CreateContactRepresentativeReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::UpdateContactRepresentativeReply::Data& src, Api::ContactRepresentative::UpdateContactRepresentativeReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist&, Api::ContactRepresentative::ContactRepresentativeInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_representative_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::CreateContactRepresentativeReply::Exception::ContactRepresentativeAlreadyExists&, Api::ContactRepresentative::CreateContactRepresentativeReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_representative_already_exists();
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::CreateContactRepresentativeReply::Exception::InvalidData& src, Api::ContactRepresentative::CreateContactRepresentativeReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::CreateContactRepresentativeReply::Exception::ContactDoesNotExist&, Api::ContactRepresentative::CreateContactRepresentativeReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist&, Api::ContactRepresentative::UpdateContactRepresentativeReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_contact_representative_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::UpdateContactRepresentativeReply::Exception::InvalidData& src, Api::ContactRepresentative::UpdateContactRepresentativeReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void Fred::Registry::proto_wrap(const ContactRepresentative::DeleteContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist&, Api::ContactRepresentative::DeleteContactRepresentativeReply* dst)
{
    dst->mutable_exception()->mutable_contact_representative_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::BlockInfoReply::Data& src, Api::DomainBlacklist::BlockInfoReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::CreateBlockReply::Data& src, Api::DomainBlacklist::CreateBlockReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::DeleteBlockReply::Data& src, Api::DomainBlacklist::DeleteBlockReply* dst)
{
    dst->clear_exception();
    proto_wrap(src, dst->mutable_data());
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::BlockInfoReply::Exception::BlockDoesNotExist&, Api::DomainBlacklist::BlockInfoReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_block_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::BlockInfoReply::Exception::InvalidData& src, Api::DomainBlacklist::BlockInfoReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::CreateBlockReply::Exception::BlockIdAlreadyExists&, Api::DomainBlacklist::CreateBlockReply* dst)
{
    dst->clear_data();
    dst->mutable_exception()->mutable_block_id_already_exists();
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::CreateBlockReply::Exception::InvalidData& src, Api::DomainBlacklist::CreateBlockReply* dst)
{
    dst->clear_data();
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::DeleteBlockReply::Exception::BlockDoesNotExist&, Api::DomainBlacklist::DeleteBlockReply* dst)
{
    dst->mutable_exception()->mutable_block_does_not_exist();
}

template <>
void Fred::Registry::proto_wrap(const DomainBlacklist::DeleteBlockReply::Exception::InvalidData& src, Api::DomainBlacklist::DeleteBlockReply* dst)
{
    wrap_invalid_data_exception(src, dst->mutable_exception()->mutable_invalid_data());
}
