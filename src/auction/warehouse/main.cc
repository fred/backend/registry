/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/auction/warehouse/cfg.hh"
#include "src/auction/warehouse/fqdn_release.hh"
#include "src/auction/warehouse/process.hh"
#include "src/util/pg_util.hh"

#include "libfred/db_settings.hh"

#include "liblog/liblog.hh"
#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <utility>

namespace {

auto severity_to_level(Fred::Auction::Warehouse::Cfg::LogSeverity severity)
{
    using namespace Fred::Auction::Warehouse;
    switch (severity)
    {
        case Cfg::LogSeverity::emerg:
            return LibLog::Level::critical;
        case Cfg::LogSeverity::alert:
            return LibLog::Level::critical;
        case Cfg::LogSeverity::crit:
            return LibLog::Level::critical;
        case Cfg::LogSeverity::err:
            return LibLog::Level::error;
        case Cfg::LogSeverity::warning:
            return LibLog::Level::warning;
        case Cfg::LogSeverity::notice:
            return LibLog::Level::info;
        case Cfg::LogSeverity::info:
            return LibLog::Level::info;
        case Cfg::LogSeverity::debug:
            return LibLog::Level::debug;
        case Cfg::LogSeverity::trace:
            return LibLog::Level::trace;
    }
    return LibLog::Level::trace;
}

std::string make_connect_string(const Fred::Auction::Warehouse::Cfg::Options::Database& option)
{
    using namespace Fred::Auction::Warehouse;
    std::string result;
    if (option.host != boost::none)
    {
        result = "host=" + *option.host;
    }
    if (option.port != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "port=" + std::to_string(*option.port);
    }
    if (option.user != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "user=" + *option.user;
    }
    if (option.dbname != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "dbname=" + *option.dbname;
    }
    if (option.password != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "password=" + *option.password;
    }
    return result;
}

template <typename Src, typename Dst>
void set_option(const boost::optional<Src>& src, LibStrong::Optional<Dst>& dst)
{
    if (src != boost::none)
    {
        dst = Dst{*src};
    }
}

LibPg::Dsn make_dsn(const Fred::Auction::Warehouse::Cfg::Options::Database& db_options)
{
    LibPg::Dsn result;
    set_option(db_options.host, result.host);
    set_option(db_options.port, result.port);
    set_option(db_options.user, result.user);
    set_option(db_options.dbname, result.db_name);
    set_option(db_options.password, result.password);
    return result;
}

auto init_logging(const Fred::Auction::Warehouse::Cfg::Options::Log& log_options)
{
    LibLog::LogConfig log_config;
    struct Result
    {
        bool log_enabled = false;
    } result;
    if (log_options.console != boost::none)
    {
        LibLog::Sink::ConsoleSinkConfig config;
        const auto& option = *log_options.console;
        config.set_level(severity_to_level(option.min_severity))
            .set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr)
            .set_color_mode(LibLog::ColorMode::automatic);
        log_config.add_sink_config(config);
        result.log_enabled = true;
    }
    if (log_options.file != boost::none)
    {
        const auto& option = *log_options.file;
        LibLog::Sink::FileSinkConfig config{option.file_name};
        config.set_level(severity_to_level(option.min_severity));
        log_config.add_sink_config(config);
        result.log_enabled = true;
    }
    if (log_options.syslog != boost::none)
    {
        const auto& option = *log_options.syslog;
        LibLog::Sink::SyslogSinkConfig config;
        if (!option.ident.empty())
        {
            config.set_syslog_ident(option.ident);
        }
        config.set_level(severity_to_level(option.min_severity));
        config.set_syslog_facility(option.facility);
        log_config.add_sink_config(config);
        result.log_enabled = true;
    }
    if (result.log_enabled)
    {
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
    }
    return result;
}

}//namespace {anonymous}

int main(int argc, char** argv)
{
    using namespace Fred::Auction::Warehouse;
    try
    {
        Cfg::Options::init(argc, argv);
    }
    catch (const Cfg::AllDone& e)
    {
        std::cout << e.what() << std::endl;
        return EXIT_SUCCESS;
    }
    catch (const Cfg::UnknownOption& unknown_option)
    {
        std::cerr << unknown_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::MissingOption& missing_option)
    {
        std::cerr << missing_option.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::InvalidCommand& invalid_command)
    {
        std::cerr << invalid_command.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const Cfg::Exception& e)
    {
        std::cerr << "Configuration problem: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        return EXIT_FAILURE;
    }

    if (init_logging(Cfg::Options::get().log).log_enabled)
    {
        LIBLOG_DEBUG("fred-auction-warehouse configuration successfully loaded");
    }
    else
    {
        std::cout << "fred-auction-warehouse configuration successfully loaded" << std::endl;
    }
    Database::emplace_default_manager<Database::StandaloneManager>(
            make_connect_string(Cfg::Options::get().database));
    struct RunSpecificCommand : boost::static_visitor<int>
    {
        int operator()(const Cfg::Options::NoCommand&) const
        {
            return EXIT_FAILURE;
        }
        int operator()(const Cfg::Options::Process& opt) const
        {
            if (opt.period == boost::none)
            {
                return process_once(opt.filter == Cfg::Options::Process::Filter::on_schedule);
            }
            return process_repeatedly(
                    opt.filter == Cfg::Options::Process::Filter::on_schedule,
                    *opt.period);
        }
        int operator()(const Cfg::Options::FqdnRelease& opt) const
        {
            return fqdn_release(opt.number_of_parts, opt.total_time, opt.finished_before);
        }
    };
    return boost::apply_visitor(RunSpecificCommand{}, Cfg::Options::get().command);
}

using namespace Fred::Registry::Util;

template <typename>
LibPg::Dsn Fred::Registry::Util::make_dsn()
{
    return ::make_dsn(Auction::Warehouse::Cfg::Options::get().database);
}

template LibPg::Dsn Fred::Registry::Util::make_dsn<DbAccess::ReadOnly>();
template LibPg::Dsn Fred::Registry::Util::make_dsn<DbAccess::ReadWrite>();
