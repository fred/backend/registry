/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/auction/warehouse/fqdn_release.hh"
#include "src/auction/warehouse/cfg.hh"
#include "src/util/pg_util.hh"

#include "libfred/registrable_object/domain/auction/release_fqdn.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"

#include "util/random/algorithm/floating_point.hh"
#include "util/random/random.hh"

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <thread>
#include <utility>

namespace {

template <typename Rep>
using Seconds = std::chrono::duration<Rep>;

}//namespace {anonymous}

using namespace Fred::Auction::Warehouse;

int Fred::Auction::Warehouse::fqdn_release(
        int number_of_parts,
        std::chrono::seconds total_time,
        boost::optional<std::chrono::system_clock::time_point> finished_before)
{
    try
    {
        const auto ctx_name = "fqdn_release-" + std::to_string(number_of_parts) +
                                          "-" + std::to_string(total_time.count());
        LibLog::SetContext log_ctx{finished_before == boost::none
                ? ctx_name
                : ctx_name + "-" + std::to_string(std::chrono::duration_cast<std::chrono::seconds>(finished_before->time_since_epoch()).count())};
        LIBLOG_DEBUG("fqdn_release start");
        auto pg_conn = Fred::Registry::Util::make_rw_connection();
        auto unreleased = [&pg_conn, &finished_before]()
        {
            LibPg::PgRoTransaction tx{std::move(pg_conn)};
            auto unreleased = finished_before == boost::none ?
                    LibFred::Domain::Auction::GetUnreleasedFqdn{}.exec(tx)
                  : LibFred::Domain::Auction::GetUnreleasedFqdn{}.exec(tx, *finished_before);
            pg_conn = commit(std::move(tx));
            return unreleased;
        }();
        if (unreleased.empty())
        {
            return EXIT_SUCCESS;
        }
        const auto divide_by = number_of_parts < 1 ? 1 : number_of_parts;
        const auto number_of_fqdns_to_release_now = static_cast<double>(unreleased.size()) / divide_by;
        const auto relative_duration_of_one_release = 1 / number_of_fqdns_to_release_now;
        const bool number_of_fqdns_to_release_now_is_whole_number = (unreleased.size() % divide_by) == 0;
        const auto max_number_of_releases = number_of_fqdns_to_release_now_is_whole_number
                ? static_cast<unsigned>(number_of_fqdns_to_release_now)
                : static_cast<unsigned>(number_of_fqdns_to_release_now) + 1;

        Random::Generator rnd_get;
        rnd_get.shuffle(begin(unreleased), end(unreleased));
        std::vector<double> relative_times_of_releases;
        relative_times_of_releases.reserve(max_number_of_releases);
        for (unsigned idx = 0; idx < max_number_of_releases; ++idx)
        {
            const auto relative_time_of_release = (idx + rnd_get.get(0.0, 1.0)) * relative_duration_of_one_release;
            if (relative_time_of_release <= 1.0)
            {
                relative_times_of_releases.push_back(relative_time_of_release);
            }
        }
        using Clock = std::chrono::high_resolution_clock;
        const auto start_time = Clock::now();
        unsigned idx_of_released_fqdn = 0;
        std::for_each(
                begin(relative_times_of_releases),
                end(relative_times_of_releases),
                [&](auto&& relative_time_of_release)
        {
            const auto time_offset = Seconds<double>{relative_time_of_release * total_time.count()};
            const auto absolute_time_of_release =
                    start_time + std::chrono::duration_cast<Clock::duration>(time_offset);
            std::this_thread::sleep_until(absolute_time_of_release);
            LibPg::PgRwTransaction tx{std::move(pg_conn)};
            bool to_commit = false;
            try
            {
                LibFred::Domain::Auction::release_fqdn(tx, unreleased[idx_of_released_fqdn]);
                to_commit = true;
                const auto get_fqdn = [&]()
                {
                    const auto dbres = exec(tx, LibPg::make_query(
                            "SELECT") << LibPg::item<std::string>() << "fqdn "
                              "FROM domain_auction "
                             "WHERE id = " << LibPg::parameter<LibFred::Domain::Auction::AuctionId>().as_big_int(),
                            unreleased[idx_of_released_fqdn]);
                    if (dbres.size() == LibPg::RowIndex{1})
                    {
                        return dbres.front().template get<std::string>();
                    }
                    return std::string{"???"};
                };
                LIBLOG_INFO("fqdn {} released", get_fqdn());
            }
            catch (const LibFred::Domain::Auction::UnreleasedFqdnNotFound& e)
            {
                LIBLOG_ERROR("fqdn release failed: {}", e.what());
            }
            catch (const std::exception& e)
            {
                LIBLOG_ERROR("fqdn release failed: {}", e.what());
            }
            catch (...)
            {
                LIBLOG_ERROR("fqdn release failed: unknown exception");
            }
            if (to_commit)
            {
                pg_conn = commit(std::move(tx));
            }
            else
            {
                pg_conn = rollback(std::move(tx));
            }
            ++idx_of_released_fqdn;
        });
        return EXIT_SUCCESS;
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
    }
    return EXIT_FAILURE;
}
