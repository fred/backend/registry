/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FQDN_RELEASE_HH_864F762CABCC45222BB954C9E9BA9A87//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define FQDN_RELEASE_HH_864F762CABCC45222BB954C9E9BA9A87

#include <boost/optional.hpp>

#include <chrono>

namespace Fred {
namespace Auction {
namespace Warehouse {

int fqdn_release(
        int number_of_parts,
        std::chrono::seconds total_time,
        boost::optional<std::chrono::system_clock::time_point> finished_before = boost::none);

}//namespace Fred::Auction::Warehouse
}//namespace Fred::Auction
}//namespace Fred

#endif//FQDN_RELEASE_HH_864F762CABCC45222BB954C9E9BA9A87
