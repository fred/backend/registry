/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CFG_HH_52A8A3361952D2AD037F60165943325B//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define CFG_HH_52A8A3361952D2AD037F60165943325B

/**
 * config file example:

#Logging options
[log]
device = file

#Logging into file options
[log.file]
file_name = registry.log
min_severity = trace

#Database options
[database]
host    = localhost
port    = 11112
user    = fred
dbname  = fred
password = password

#Auction server options
[auction]
ssl = on
host = auction.nic.cz
endpoint = /auction
header = Authorization: Basic <credentials>
timeout = 30
 */

#include <boost/optional.hpp>
#include <boost/program_options.hpp>
#include <boost/variant.hpp>

#include <chrono>
#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>

namespace Fred {
namespace Auction {
namespace Warehouse {
namespace Cfg {

struct AllDone : std::exception { };

struct Exception : std::runtime_error
{
    explicit Exception();
};

struct UnknownOption : Exception { };

struct MissingOption : Exception { };

struct InvalidCommand : Exception { };

struct InvalidValue : Exception { };

enum class LogSeverity
{
    emerg,
    alert,
    crit,
    err,
    warning,
    notice,
    info,
    debug,
    trace
};

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(int argc, const char* const* argv);

    boost::optional<std::string> config_file_name;
    struct NoCommand { };
    struct Process
    {
        enum class Filter
        {
            on_schedule,
            all
        } filter = Filter::on_schedule;
        boost::optional<std::chrono::seconds> period = boost::none;
    };
    struct FqdnRelease
    {
        int number_of_parts;
        std::chrono::seconds total_time;
        boost::optional<std::chrono::system_clock::time_point> finished_before = boost::none;
    };
    using Command = boost::variant<NoCommand, Process, FqdnRelease>;
    Command command;
    struct Database
    {
        boost::optional<std::string> host;
        boost::optional<std::uint16_t> port;
        boost::optional<std::string> user;
        boost::optional<std::string> dbname;
        boost::optional<std::string> password;
    } database;
    struct Console
    {
        LogSeverity min_severity;
    };
    struct LogFile
    {
        std::string file_name;
        LogSeverity min_severity;
    };
    struct SysLog
    {
        std::string ident;
        int facility;
        LogSeverity min_severity;
    };
    struct Log
    {
        LogSeverity min_severity;
        boost::optional<Console> console;
        boost::optional<LogFile> file;
        boost::optional<SysLog> syslog;
    } log;
    struct Auction
    {
        bool ssl;
        std::string host;
        std::uint16_t port;
        std::string endpoint;
        std::vector<std::string> header;
        std::chrono::duration<double> timeout;
    } auction;
private:
    Options(int argc, const char* const* argv);
};

}//namespace Fred::Auction::Warehouse::Cfg
}//namespace Fred::Auction::Warehouse
}//namespace Fred::Auction
}//namespace Fred

#endif//CFG_HH_52A8A3361952D2AD037F60165943325B
