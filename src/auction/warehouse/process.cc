/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/auction/warehouse/process.hh"
#include "src/auction/warehouse/auction_api.hh"
#include "src/auction/warehouse/cfg.hh"
#include "src/http/client.hh"
#include "src/util/pg_util.hh"

#include "libfred/registrable_object/domain/auction/finish_auction.hh"
#include "libfred/registrable_object/domain/auction/get_auctions.hh"
#include "libfred/registrable_object/domain/auction/lock_auction.hh"
#include "libfred/registrable_object/domain/auction/update_auction.hh"

#include "liblog/liblog.hh"

#include <boost/certify/https_verification.hpp>

#include <algorithm>
#include <ctime>
#include <functional>
#include <iomanip>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <thread>
#include <utility>

namespace {

LibHttp::Header make_header(const std::vector<std::string>& lines)
{
    static const auto name_value_regex = std::regex{
            "[[:space:]]*([^:]+):[[:space:]]*"                          // Name:
            "(([^[:space:]]+[[:space:]]+)*[^[:space:]]+)[[:space:]]*"}; // Value
    LibHttp::Header header;
    std::for_each(begin(lines), end(lines), [&header](auto&& line)
    {
        std::smatch match;
        if (std::regex_match(line, match, name_value_regex))
        {
            header.push_back({match[1].str(), match[2].str()});
            return;
        }
        LIBLOG_WARNING("\"{}\" is not a valid http header line", line);
    });
    return header;
}

std::string to_string(const std::chrono::system_clock::time_point& t)
{
    const auto c_time = std::chrono::system_clock::to_time_t(t);
    std::ostringstream out;
    out << std::put_time(std::gmtime(&c_time), "%c");
    return out.str();
}

std::string to_string(const boost::optional<std::chrono::system_clock::time_point>& t)
{
    if (t == boost::none)
    {
        return "NULL";
    }
    return to_string(*t);
}

std::string to_string(const Fred::Auction::Warehouse::Win& win)
{
    return "win: [winner " + to_string(*win.winner_id) + ", expire " + to_string(win.expires_at) + "]";
}

std::string to_string(const boost::optional<Fred::Auction::Warehouse::Win>& win)
{
    if (win == boost::none)
    {
        return "win: NULL";
    }
    return to_string(*win);
}

using Exec = std::function<void(const LibFred::OperationContext&)>;

template <LibFred::Domain::Auction::GetAuctions::Property>
Exec process_auction(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info);

template <>
Exec process_auction<LibFred::Domain::Auction::GetAuctions::Property::without_external_auction_id>(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info)
{
    using namespace Fred::Auction::Warehouse;
    const auto result = create_auction(stream, header, CreateAuctionRequest{AuctionItem{"", info.fqdn, AuctionUuid{*info.auction_uuid}}});
    LibFred::Domain::Auction::UpdateAuction update{info.auction_id};
    update.set_external_auction_id(LibFred::Domain::Auction::ExternalAuctionId{*result.auction_id})
          .set_next_event_after(result.next_event_after);
    LIBLOG_INFO("auction without external auction_id {} - {}", *result.auction_id, to_string(result.next_event_after));
    return [update](const LibFred::OperationContext& ctx)
    {
        update.exec(ctx);
    };
}

template <>
Exec process_auction<LibFred::Domain::Auction::GetAuctions::Property::waiting_for_event>(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info)
{
    using namespace Fred::Auction::Warehouse;
    try
    {
        const auto request = GetAuctionInfoRequest{ExternalAuctionId{*info.external_auction_id}};
        const auto result = get_auction_info(stream, header, request);
        LibFred::Domain::Auction::UpdateAuction update{info.auction_id};
        if (result.next_event_after != boost::none)
        {
            update.set_next_event_after(*result.next_event_after);
        }
        else
        {
            update.set_next_event_after(nullptr);
        }
        if (result.win != boost::none)
        {
            update.set_winner_id(*result.win->winner_id)
                  .set_win_expires_at(result.win->expires_at);
        }
        else
        {
            update.set_winner_id(nullptr)
                  .set_win_expires_at(nullptr);
        }
        LIBLOG_INFO("auction waiting for event {} - {}", to_string(result.win), to_string(result.next_event_after));
        return [update](const LibFred::OperationContext& ctx)
        {
            update.exec(ctx);
        };
    }
    catch (const GetAuctionInfoReply::DoesNotExist&)
    {
        LibFred::Domain::Auction::FinishAuction finish{info.auction_id};
        return [finish](const LibFred::OperationContext& ctx)
        {
            finish.exec(ctx, LibFred::Domain::Auction::FinishAuction::Release::later);
        };
    }
}

template <>
Exec process_auction<LibFred::Domain::Auction::GetAuctions::Property::event_anticipated>(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info)
{
    using namespace Fred::Auction::Warehouse;
    try
    {
        const auto request = GetAuctionInfoRequest{ExternalAuctionId{*info.external_auction_id}};
        const auto result = get_auction_info(stream, header, request);
        const auto do_update = (result.next_event_after != boost::none) || (result.win != boost::none);
        if (!do_update)
        {
            static const auto do_nothing = [](const LibFred::OperationContext&) { };
            return do_nothing;
        }
        LibFred::Domain::Auction::UpdateAuction update{info.auction_id};
        if (result.next_event_after != boost::none)
        {
            update.set_next_event_after(*result.next_event_after);
        }
        if (result.win != boost::none)
        {
            update.set_winner_id(*result.win->winner_id)
                  .set_win_expires_at(result.win->expires_at);
        }
        LIBLOG_INFO("auction event anticipated {} - {}", to_string(result.win), to_string(result.next_event_after));
        return [update](const LibFred::OperationContext& ctx)
        {
            update.exec(ctx);
        };
    }
    catch (const GetAuctionInfoReply::DoesNotExist&)
    {
        LibFred::Domain::Auction::FinishAuction finish{info.auction_id};
        return [finish](const LibFred::OperationContext& ctx)
        {
            finish.exec(ctx, LibFred::Domain::Auction::FinishAuction::Release::later);
        };
    }
}

template <>
Exec process_auction<LibFred::Domain::Auction::GetAuctions::Property::waiting_for_registration>(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info)
{
    using namespace Fred::Auction::Warehouse;
    try
    {
        const auto request = GetAuctionInfoRequest{ExternalAuctionId{*info.external_auction_id}};
        const auto result = get_auction_info(stream, header, request);
        LibFred::Domain::Auction::UpdateAuction update{info.auction_id};
        if (result.next_event_after != boost::none)
        {
            update.set_next_event_after(*result.next_event_after);
        }
        else
        {
            update.set_next_event_after(nullptr);
        }
        if (result.win != boost::none)
        {
            update.set_winner_id(*result.win->winner_id)
                  .set_win_expires_at(result.win->expires_at);
        }
        else
        {
            update.set_winner_id(nullptr)
                  .set_win_expires_at(nullptr);
        }
        LIBLOG_INFO("auction waiting for registration {} - {}", to_string(result.win), to_string(result.next_event_after));
        return [update](const LibFred::OperationContext& ctx)
        {
            update.exec(ctx);
        };
    }
    catch (const GetAuctionInfoReply::DoesNotExist&)
    {
        LibFred::Domain::Auction::FinishAuction finish{info.auction_id};
        return [finish](const LibFred::OperationContext& ctx)
        {
            finish.exec(ctx, LibFred::Domain::Auction::FinishAuction::Release::later);
        };
    }
}

template <>
Exec process_auction<LibFred::Domain::Auction::GetAuctions::Property::registration_completed>(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info)
{
    using namespace Fred::Auction::Warehouse;
    try
    {
        const auto request = ItemTakingInfoRequest{
                ExternalAuctionId{*info.external_auction_id},
                AuctionStatus::successful,
                info.domain_crdate};
        const auto result = item_taking_info(stream, header, request);
        if (result.next_event_after != boost::none)
        {
            LIBLOG_WARNING("auction winner succeeded in registering the {} domain before the auction was stopped", info.fqdn);
            LibFred::Domain::Auction::UpdateAuction update{info.auction_id};
            update.set_winner_id()
                  .set_next_event_after(*result.next_event_after);
            LIBLOG_INFO("auction registration completed NULL - {}", to_string(result.next_event_after));
            return [update](const LibFred::OperationContext& ctx)
            {
                update.exec(ctx);
            };
        }
    }
    catch (const ItemTakingInfoReply::DoesNotExist&) { }
    LIBLOG_INFO("auction registration completed {} - finish", *info.auction_id);
    LibFred::Domain::Auction::FinishAuction finish{info.auction_id};
    return [finish](const LibFred::OperationContext& ctx)
    {
        finish.exec(ctx);
    };
}

template <>
Exec process_auction<LibFred::Domain::Auction::GetAuctions::Property::registration_expired>(
        LibHttp::Socket& stream,
        const LibHttp::Header& header,
        const LibFred::Domain::Auction::GetAuctions::AuctionInfo& info)
{
    using namespace Fred::Auction::Warehouse;
    try
    {
        const auto request = ItemTakingInfoRequest{
                ExternalAuctionId{*info.external_auction_id},
                AuctionStatus::expired,
                boost::none};
        const auto result = item_taking_info(stream, header, request);
        if (result.next_event_after != boost::none)
        {
            LibFred::Domain::Auction::UpdateAuction update{info.auction_id};
            update.set_winner_id()
                  .set_next_event_after(*result.next_event_after);
            LIBLOG_INFO("auction registration expired NULL - {}", to_string(result.next_event_after));
            return [update](const LibFred::OperationContext& ctx)
            {
                update.exec(ctx);
            };
        }
        LIBLOG_INFO("auction registration expired {} - finish", *info.auction_id);
        LibFred::Domain::Auction::FinishAuction finish{info.auction_id};
        return [finish](const LibFred::OperationContext& ctx)
        {
            finish.exec(ctx, LibFred::Domain::Auction::FinishAuction::Release::later);
        };
    }
    catch (const ItemTakingInfoReply::DoesNotExist&)
    {
        LIBLOG_INFO("auction registration expired {} - finish", *info.auction_id);
        LibFred::Domain::Auction::FinishAuction finish{info.auction_id};
        return [finish](const LibFred::OperationContext& ctx)
        {
            finish.exec(ctx, LibFred::Domain::Auction::FinishAuction::Release::later);
        };
    }
}

auto get_auctions(bool on_schedule)
{
    const auto filter = [on_schedule]()
    {
        using AuctionProperty = LibFred::Domain::Auction::GetAuctions::Property;
        static constexpr auto no_properties = std::initializer_list<AuctionProperty>{};
        static constexpr auto scheduled_auctions = std::initializer_list<AuctionProperty>
        {
                AuctionProperty::without_external_auction_id,
                AuctionProperty::event_anticipated,
                AuctionProperty::waiting_for_registration,
                AuctionProperty::registration_completed,
                AuctionProperty::registration_expired
        };
        return on_schedule ? scheduled_auctions
                           : no_properties;
    }();
    LibFred::OperationContextCreator ctx;
    auto result = LibFred::Domain::Auction::GetAuctions{}.exec(ctx, filter);
    ctx.commit_transaction();
    return result;
}

}//namespace {anonymous}

using namespace Fred::Auction::Warehouse;

int Fred::Auction::Warehouse::process_once(bool on_schedule)
{
    try
    {
        LibLog::SetContext log_ctx{on_schedule ? "process_scheduled" : "process"};
        LIBLOG_DEBUG("process_once start");
        const auto auctions = get_auctions(on_schedule);
        const auto header = make_header(Cfg::Options::get().auction.header);
        auto asio_ctx = Cfg::Options::get().auction.ssl
                ? LibHttp::make_context(boost::asio::ssl::context::tlsv13_client)
                : LibHttp::make_context();
        const auto make_stream = [&asio_ctx]()
        {
            auto stream = LibHttp::connect(asio_ctx, {Cfg::Options::get().auction.host, Cfg::Options::get().auction.port});
            LibHttp::set_timeout(stream, std::chrono::duration_cast<std::chrono::microseconds>(Cfg::Options::get().auction.timeout));
            return stream;
        };
        auto stream = make_stream();
        auto db_conn = Registry::Util::make_rw_connection();
        std::for_each(begin(auctions), end(auctions), [&](auto&& info)
        {
            LibLog::SetContext log_ctx{(on_schedule ? "process_scheduled-" : "process-") + info.fqdn};
            LIBLOG_DEBUG("auction processing start");
            auto tx = LibPg::PgRwTransaction{std::move(db_conn)};
            try
            {
                const LibFred::OperationContext ctx{tx};
                const LibFred::Domain::Auction::LockAuction locked_auction{ctx, info};
                const auto exec = [&]() -> Exec
                {
                    using Property = LibFred::Domain::Auction::GetAuctions::Property;
                    try
                    {
                        switch (info.property)
                        {
                            case Property::without_external_auction_id:
                                return process_auction<Property::without_external_auction_id>(stream, header, info);
                            case Property::waiting_for_event:
                                return process_auction<Property::waiting_for_event>(stream, header, info);
                            case Property::event_anticipated:
                                return process_auction<Property::event_anticipated>(stream, header, info);
                            case Property::waiting_for_registration:
                                return process_auction<Property::waiting_for_registration>(stream, header, info);
                            case Property::registration_completed:
                                return process_auction<Property::registration_completed>(stream, header, info);
                            case Property::registration_expired:
                                return process_auction<Property::registration_expired>(stream, header, info);
                        }
                        LIBLOG_WARNING("process auction {}:{} failed: unknown auction property", *info.auction_id, info.fqdn);
                    }
                    catch (const boost::system::system_error& e)
                    {
                        LIBLOG_WARNING("process auction {}:{} system_error caught: {}", *info.auction_id, info.fqdn, e.what());
                        if (e.code() == make_error_code(boost::asio::error::basic_errors::broken_pipe) ||
                            e.code() == make_error_code(boost::asio::error::basic_errors::connection_aborted) ||
                            e.code() == make_error_code(boost::asio::error::basic_errors::connection_refused) ||
                            e.code() == make_error_code(boost::asio::error::basic_errors::connection_reset) ||
                            e.code() == make_error_code(boost::asio::error::basic_errors::timed_out))
                        {
                            LIBLOG_DEBUG("reopen http(s) stream");
                            try
                            {
                                LibHttp::close(std::move(stream));
                            }
                            catch (const std::exception& e)
                            {
                                LIBLOG_WARNING("stream close failed: {}", e.what());
                            }
                            catch (...)
                            {
                                LIBLOG_WARNING("stream close failed");
                            }
                            stream = make_stream();
                        }
                    }
                    catch (const LibHttp::Response& r)
                    {
                        LIBLOG_WARNING("process auction {}:{} failed: http response {}", *info.auction_id, info.fqdn, r.status);
                    }
                    catch (const ValidationError& e)
                    {
                        LIBLOG_WARNING("process auction {}:{} invalid request: {} - {}", *info.auction_id, info.fqdn, e.what(), e.data);
                    }
                    catch (const Unauthorized& e)
                    {
                        LIBLOG_WARNING("process auction {}:{} unauthorized request: {} - {}", *info.auction_id, info.fqdn, e.what(), e.data);
                    }
                    catch (const std::exception& e)
                    {
                        LIBLOG_WARNING("process auction {}:{} failed: {}", *info.auction_id, info.fqdn, e.what());
                    }
                    catch (...)
                    {
                        LIBLOG_WARNING("process auction {}:{} failed: unknown exception", *info.auction_id, info.fqdn);
                    }
                    return nullptr;
                }();
                if (exec != nullptr)
                {
                    exec(locked_auction.get_ctx());
                    db_conn = commit(std::move(tx));
                }
                else
                {
                    db_conn = rollback(std::move(tx));
                }
            }
            catch (const LibFred::Domain::Auction::LockAuction::Exception& e)
            {
                LIBLOG_INFO("auction {}:{} lock failed: {}", *info.auction_id, info.fqdn, e.what());
                db_conn = rollback(std::move(tx));
            }
        });
        LibHttp::close(std::move(stream));
        return EXIT_SUCCESS;
    }
    catch (const LibHttp::Response& r)
    {
        LIBLOG_INFO("http response: {}", r.status);
    }
    catch (const ValidationError& e)
    {
        LIBLOG_INFO("invalid request: {} - {}", e.what(), e.data);
    }
    catch (const Unauthorized& e)
    {
        LIBLOG_INFO("unauthorized request: {} - {}", e.what(), e.data);
    }
    catch (const std::exception& e)
    {
        LIBLOG_INFO("std::exception caught: {}", e.what());
    }
    catch (...)
    {
        LIBLOG_INFO("unknown exception caught");
    }
    return EXIT_FAILURE;
}

int Fred::Auction::Warehouse::process_repeatedly(
        bool on_schedule,
        std::chrono::seconds period)
{
    auto result = EXIT_SUCCESS;
    static const auto now = []() { return std::chrono::steady_clock::now(); };
    auto t_next = now() + period;
    while (true)
    {
        try
        {
            result = process_once(on_schedule);
        }
        catch (...)
        {
            result = EXIT_FAILURE;
        }
        auto dt = t_next - now();
        if (std::chrono::seconds{0} < dt)
        {
            std::this_thread::sleep_for(dt);
            t_next += period;
        }
        else
        {
            t_next += period - dt;
        }
    }
    return result;
}
