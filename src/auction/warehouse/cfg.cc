/*
 * Copyright (C) 2018-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "src/auction/warehouse/cfg.hh"
#include "src/util/rfc3339_time.hh"

#include <syslog.h>

#include <algorithm>
#include <initializer_list>
#include <iostream>
#include <fstream>
#include <functional>
#include <sstream>
#include <type_traits>
#include <utility>

#ifndef DEFAULT_FRED_AUCTION_WAREHOUSE_CONFIG_FILE
#define DEFAULT_FRED_AUCTION_WAREHOUSE_CONFIG_FILE "fred-auction-warehouse.conf"
#endif

namespace Fred {
namespace Auction {
namespace Warehouse {
namespace Cfg {

namespace {

template <typename Exception, typename MsgGen>
[[noreturn]] void raise(MsgGen msg_gen)
{
    struct Error : Exception
    {
        explicit Error(MsgGen msg_gen)
            : Exception{},
              msg_gen{std::move(msg_gen)}
        { }
        const char* what() const noexcept override { return msg_gen(); }
        MsgGen msg_gen;
    };
    throw Error{std::move(msg_gen)};
}

LogSeverity make_severity(const std::string& str)
{
    if (str == "emerg")
    {
        return LogSeverity::emerg;
    }
    if (str == "alert")
    {
        return LogSeverity::alert;
    }
    if (str == "crit")
    {
        return LogSeverity::crit;
    }
    if (str == "err")
    {
        return LogSeverity::err;
    }
    if (str == "warning")
    {
        return LogSeverity::warning;
    }
    if (str == "notice")
    {
        return LogSeverity::notice;
    }
    if (str == "info")
    {
        return LogSeverity::info;
    }
    if (str == "debug")
    {
        return LogSeverity::debug;
    }
    if (str == "trace")
    {
        return LogSeverity::trace;
    }
    raise<Exception>([msg = "not a valid severity \"" + str + "\""]() { return msg.c_str(); });
}

void required_one_of(
        const boost::program_options::variables_map& variables_map,
        std::initializer_list<const char*> variables)
{
    if (std::all_of(
                begin(variables),
                end(variables),
                [&variables_map](auto&& variable) { return variables_map.count(variable) == 0; }))
    {
        std::string msg;
        std::for_each(begin(variables), end(variables), [&msg](auto&& variable)
        {
            if (msg.empty())
            {
                msg = variable;
            }
            else
            {
                msg += std::string{", "} + variable;
            }
        });
        raise<MissingOption>([msg = "missing one of options: '" + msg + "'"]() { return msg.c_str(); });
    }
}

void required_all_of(
        const boost::program_options::variables_map& variables_map,
        std::initializer_list<const char*> variables)
{
    if (std::any_of(
                begin(variables),
                end(variables),
                [&variables_map](auto&& variable) { return variables_map.count(variable) == 0; }))
    {
        std::string msg;
        std::for_each(begin(variables), end(variables), [&msg](auto&& variable)
        {
            if (msg.empty())
            {
                msg = variable;
            }
            else
            {
                msg += std::string{", "} + variable;
            }
        });
        raise<MissingOption>([msg = "missing any of options: '" + msg + "'"]() { return msg.c_str(); });
    }
}

constexpr int invalid_argc = -1;
constexpr const char* const invalid_argv[0] = { };

static constexpr unsigned terminal_width = 120;

struct HasNoCommand : boost::static_visitor<bool>
{
    template <typename T>
    constexpr bool operator()(T&&) const noexcept
    {
        return std::is_same<std::decay_t<T>, Options::NoCommand>::value;
    }
};

template <typename Cmd>
struct GetCommand : boost::static_visitor<Cmd&>
{
    Cmd& operator()(Cmd& value) const noexcept
    {
        return value;
    }
    template <typename T>
    Cmd& operator()(const T& value) const
    {
        throw value;
    }
};

template <typename Cmd>
Cmd& set_command(Options::Command& command)
{
    if (boost::apply_visitor(HasNoCommand{}, command))
    {
        command = Cmd{};
    }
    return boost::apply_visitor(GetCommand<Cmd>{}, command);
}

class CheckRequiredOptions : boost::static_visitor<void>
{
public:
    explicit CheckRequiredOptions(
            std::function<void()> check_process,
            std::function<void()> check_fqdn_release)
        : check_process_command_{check_process},
          check_fqdn_release_command_{check_fqdn_release}
    { }
    void operator()(const Options::NoCommand&) const { }
    void operator()(const Options::Process&) const { check_process_command_(); }
    void operator()(const Options::FqdnRelease&) const { check_fqdn_release_command_(); }
private:
    std::function<void()> check_process_command_;
    std::function<void()> check_fqdn_release_command_;
};

}//namespace Fred::Auction::Warehouse::Cfg::{anonymous}

const Options& Options::get()
{
    return init(invalid_argc, invalid_argv);
}

const Options& Options::init(int argc, const char* const* argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (argc != invalid_argc) || (argv != invalid_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            throw std::runtime_error("First call of Cfg::Options::init must contain valid arguments");
        }
        static const Options singleton(argc, argv);
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        throw std::runtime_error("Only first call of Cfg::Options::init can contain valid arguments");
    }
    return *singleton_ptr;
}

Options::Options(int argc, const char* const* argv)
    : command{NoCommand{}}
{
    boost::program_options::options_description generic_options("Generic options", terminal_width);
    std::string opt_config_file_name;
    generic_options.add_options()
        ("help,h", "produce help message")
        ("version,V", "display version information")
        ("config,c",
#ifdef DEFAULT_FRED_AUCTION_WAREHOUSE_CONFIG_FILE
         boost::program_options::value<std::string>(&opt_config_file_name)->default_value(DEFAULT_FRED_AUCTION_WAREHOUSE_CONFIG_FILE),
#else
         boost::program_options::value<std::string>(&opt_config_file_name),
#endif
         "name of a configuration file.");

    boost::program_options::options_description command_options("Command options", terminal_width);
    command_options.add_options()
        (
            "cmd",
            boost::program_options::value<std::string>()->notifier([this](auto&& value)
            {
                try
                {
                    if (value == "process")
                    {
                        set_command<Process>(command);
                        return;
                    }
                    if (value == "fqdn_release")
                    {
                        set_command<FqdnRelease>(command);
                        return;
                    }
                }
                catch (const Process&)
                {
                    raise<InvalidCommand>([msg = "invalid option for \"" + value + "\" command"]() { return msg.c_str(); });
                }
                catch (const FqdnRelease&)
                {
                    raise<InvalidCommand>([msg = "invalid option for \"" + value + "\" command"]() { return msg.c_str(); });
                }
                raise<InvalidCommand>([msg = "invalid command \"" + value + "\""]() { return msg.c_str(); });
            }),
            "run specific command; possible value is process and fqdn_release");
    boost::program_options::positional_options_description pos_options;
    pos_options.add("cmd", -1);

    boost::program_options::options_description process_options{"process command related options", terminal_width};
    process_options.add_options()
        (
            "filter",
            boost::program_options::value<std::string>()->notifier([this](auto&& value)
            {
                try
                {
                    set_command<Process>(command).filter = [&value]()
                    {
                        if (value == "on_schedule")
                        {
                            return Process::Filter::on_schedule;
                        }
                        if (value == "all")
                        {
                            return Process::Filter::all;
                        }
                        raise<InvalidValue>([]() { return "option filter can be on_schedule or all"; });
                    }();
                }
                catch (const FqdnRelease&)
                {
                    raise<UnknownOption>([]() { return "option filter not belong to \"fqdn_release\" command"; });
                }
            }),
            "process given set of auctions; possible values: on_schedule, all; default value: on_schedule")
        (
            "period",
            boost::program_options::value<std::chrono::seconds::rep>()->notifier([this](auto&& value)
            {
                set_command<Process>(command).period = std::chrono::seconds{value};
            }),
            "process repeatedly with given period in seconds");

    boost::program_options::options_description fqdn_release_options{"fqdn_release command related options", terminal_width};
    fqdn_release_options.add_options()
        (
            "number_of_parts",
            boost::program_options::value<int>()->notifier([this](auto&& value)
            {
                try
                {
                    set_command<FqdnRelease>(command).number_of_parts = value;
                }
                catch (const Process&)
                {
                    raise<UnknownOption>([]() { return "option number_of_parts not belong to \"process\" command"; });
                }
            }),
            "how many parts is the fqdns release divided into; only one part will be released in this run")
        (
            "total_time",
            boost::program_options::value<int>()->notifier([this](auto&& value)
            {
                try
                {
                    set_command<FqdnRelease>(command).total_time = std::chrono::seconds{value};
                }
                catch (const Process&)
                {
                    raise<UnknownOption>([]() { return "option total_time not belong to \"process\" command"; });
                }
            }),
            "spread the release of the remaining fqdns over the total time specified in seconds")
        (
            "finished_before",
            boost::program_options::value<std::string>()->notifier([this](auto&& value)
            {
                try
                {
                    set_command<FqdnRelease>(command).finished_before =
                            Registry::Util::seconds_from_rfc3339_formated_string(value);
                }
                catch (const Process&)
                {
                    raise<UnknownOption>([]() { return "option finished_before not belong to \"process\" command"; });
                }
                catch (const Registry::Util::NotRfc3339Compliant& e)
                {
                    raise<InvalidValue>([msg = std::string{"invalid value of finished_before option: "} + e.what()]() { return msg.c_str(); });
                }
            }),
            "limit on fqdns from auctions finished before specified time");

    boost::program_options::options_description database_options("Database options", terminal_width);
    std::string host;
    std::uint16_t port;
    std::string user;
    std::string dbname;
    std::string password;
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>(&host), "name of host to connect to")
        ("database.port", boost::program_options::value<std::uint16_t>(&port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>(&user), "PostgreSQL user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>(&dbname), "the database name")
        ("database.password", boost::program_options::value<std::string>(&password), "password used for password authentication.");

    boost::program_options::options_description log_options("Logging options", terminal_width);
    std::vector<std::string> devices;
    std::string default_min_severity;
    log_options.add_options()
        ("log.device", boost::program_options::value<std::vector<std::string>>(&devices), "where to log (console/file/syslog)")
        ("log.min_severity", boost::program_options::value<std::string>(&default_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_console_options("Logging on console options", terminal_width);
    std::string log_console_min_severity;
    log_console_options.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>(&log_console_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_file_options("Logging into file options", terminal_width);
    std::string log_file_name;
    std::string log_file_min_severity;
    log_file_options.add_options()
        ("log.file.file_name",
         boost::program_options::value<std::string>(&log_file_name),
         "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>(&log_file_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description log_syslog_options("Logging into syslog options", terminal_width);
    std::string ident;
    int facility_local_offset;
    std::string log_syslog_min_severity;
    log_syslog_options.add_options()
        ("log.syslog.ident", boost::program_options::value<std::string>(&ident), "what ident to log with (default is empty string)")
        ("log.syslog.facility",
         boost::program_options::value<int>(&facility_local_offset),
         "what LOG_LOCALx facility to log with (x in range 0..7, default means facility LOG_USER)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>(&log_syslog_min_severity),
         "do not log more trivial events; "
         "severity in descending order: emerg/alert/crit/err/warning/notice/info/debug/trace");

    boost::program_options::options_description server_options("Auction server options", terminal_width);
    static constexpr auto default_api_call_timeout = std::chrono::duration<double>{30.0};
    server_options.add_options()
        ("auction.ssl", boost::program_options::value<bool>(&auction.ssl)->default_value(false), "turn on SSL")
        ("auction.host", boost::program_options::value<std::string>(&auction.host), "host part of url")
        ("auction.port", boost::program_options::value<std::uint16_t>(&auction.port), "port part of url (default 80/443 depends on ssl)")
        ("auction.endpoint", boost::program_options::value<std::string>(&auction.endpoint),
         "subdirectory part of url")
        ("auction.header", boost::program_options::value<std::vector<std::string>>(&auction.header),
         "additional header lines of http request")
        ("auction.timeout", boost::program_options::value<double>()->notifier([this](auto&& value)
            {
                auction.timeout = std::chrono::duration<double>{value};
            })->default_value(default_api_call_timeout.count()),
         "timeout in seconds on communication with auction server");

    boost::program_options::options_description command_line_options("fred-auction-warehouse options");
    command_line_options
        .add(generic_options)
        .add(command_options)
        .add(process_options)
        .add(fqdn_release_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(server_options);
    boost::program_options::options_description config_file_options;
    config_file_options
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options)
        .add(database_options)
        .add(server_options);

    boost::program_options::variables_map variables_map;
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(argc, argv)
                    .options(command_line_options)
                    .positional(pos_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        raise<UnknownOption>([msg = out.str()]() { return msg.c_str(); });
    }
    boost::program_options::notify(variables_map);

    if (has_option("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        raise<AllDone>([msg = out.str()]() { return msg.c_str(); });
    }
    if (has_option("version"))
    {
        std::ostringstream out;
        out << "Version: " << PACKAGE_VERSION << std::endl;
        out << "LibFred Version: " << LIBFRED_VERSION << std::endl;
        raise<AllDone>([msg = out.str()]() { return msg.c_str(); });
    }
    required_one_of(variables_map, {"cmd"});
    boost::apply_visitor(
            CheckRequiredOptions{
                    []() { },
                    [&variables_map]() { required_all_of(variables_map, {"number_of_parts", "total_time"}); }
            },
            command);
    const bool config_file_name_presents = has_option("config");
    if (config_file_name_presents)
    {
        this->config_file_name = opt_config_file_name;
        std::ifstream config_file(opt_config_file_name);
        if (!config_file)
        {
            raise<Exception>([msg = "can not open config file \"" + opt_config_file_name + "\""]() { return msg.c_str(); });
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            raise<UnknownOption>([msg = out.str()]() { return msg.c_str(); });
        }
        boost::program_options::notify(variables_map);
    }

    if (has_option("database.host"))
    {
        this->database.host = std::move(host);
    }
    if (has_option("database.port"))
    {
        this->database.port = port;
    }
    if (has_option("database.user"))
    {
        this->database.user = std::move(user);
    }
    if (has_option("database.dbname"))
    {
        this->database.dbname = std::move(dbname);
    }
    if (has_option("database.password"))
    {
        this->database.password = std::move(password);
    }

    if (has_option("log.device"))
    {
        boost::optional<LogSeverity> log_min_severity = LogSeverity::emerg;
        if (has_option("log.min_severity"))
        {
            log_min_severity = make_severity(default_min_severity);
        }
        else
        {
            log_min_severity = boost::none;
        }
        std::for_each(begin(devices), end(devices), [&](auto&& device)
        {
            if (device == "console")
            {
                required_one_of(variables_map, {"log.min_severity", "log.console.min_severity"});
                this->log.console = Console{
                        .min_severity = has_option("log.console.min_severity")
                            ? make_severity(log_console_min_severity)
                            : *log_min_severity};
            }
            else if (device == "file")
            {
                required_one_of(variables_map, {"log.file.file_name"});
                required_one_of(variables_map, {"log.min_severity", "log.file.min_severity"});
                this->log.file = LogFile{
                        .file_name = log_file_name,
                        .min_severity = has_option("log.file.min_severity")
                            ? make_severity(log_file_min_severity)
                            : *log_min_severity};
            }
            else if (device == "syslog")
            {
                static constexpr int default_facility = LOG_USER;
                static const auto make_facility = [](int offset)
                {
                    switch (offset)
                    {
                        case 0: return LOG_LOCAL0;
                        case 1: return LOG_LOCAL1;
                        case 2: return LOG_LOCAL2;
                        case 3: return LOG_LOCAL3;
                        case 4: return LOG_LOCAL4;
                        case 5: return LOG_LOCAL5;
                        case 6: return LOG_LOCAL6;
                        case 7: return LOG_LOCAL7;
                    }
                    raise<Exception>([]() { return "option 'log.syslog.facility' out of range [0, 7]"; });
                };
                required_one_of(variables_map, {"log.min_severity", "log.syslog.min_severity"});
                this->log.syslog = SysLog{
                        .ident = has_option("log.syslog.ident") ? ident
                                                                : std::string{},
                        .facility = has_option("log.syslog.facility") ? make_facility(facility_local_offset)
                                                                      : default_facility,
                        .min_severity = has_option("log.syslog.min_severity")
                            ? make_severity(log_syslog_min_severity)
                            : *log_min_severity};
            }
            else
            {
                raise<Exception>([]() { return "Invalid value of log.device"; });
            }
        });
    }
    required_one_of(variables_map, {"auction.host"});
    required_one_of(variables_map, {"auction.endpoint"});
    if (!has_option("auction.port"))
    {
        static constexpr std::uint16_t default_http_port = 80;
        static constexpr std::uint16_t default_https_port = 443;
        this->auction.port = this->auction.ssl ? default_https_port
                                               : default_http_port;
    }
}

Exception::Exception()
    : std::runtime_error{std::string{}}
{ }

}//namespace Fred::Auction::Warehouse::Cfg
}//namespace Fred::Auction::Warehouse
}//namespace Fred::Auction
}//namespace Fred
