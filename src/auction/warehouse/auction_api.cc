/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/auction/warehouse/auction_api.hh"
#include "src/auction/warehouse/cfg.hh"

#include "liblog/liblog.hh"

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/string_generator.hpp>

#include <nlohmann/json.hpp>

#include <ctime>
#include <iomanip>
#include <regex>
#include <sstream>
#include <utility>

namespace {

template <typename> struct BoostTimeResolutionTraits;

template <>
struct BoostTimeResolutionTraits<boost::date_time::milli_res>
{
    using StdChronoResolution = std::chrono::milliseconds;
};

template <>
struct BoostTimeResolutionTraits<boost::date_time::micro_res>
{
    using StdChronoResolution = std::chrono::microseconds;
};

template <>
struct BoostTimeResolutionTraits<boost::date_time::nano_res>
{
    using StdChronoResolution = std::chrono::nanoseconds;
};

std::chrono::system_clock::time_point chrono_from_iso_string(const std::string& iso_time)
{
    struct BadLexicalCast : std::exception
    {
        const char* what() const noexcept override { return "not an ISO 8601 formatted time"; }
    };
    static const auto iso_time_regex = std::regex{"([^T]+T[^-+Z]+)(Z|([-+])([0-9]{2}):?([0-9]{2})?)?"};
    std::smatch match;
    if (!std::regex_match(iso_time, match, iso_time_regex))
    {
        throw BadLexicalCast{};
    }
    const auto offset = [&match]()
    {
        const auto has_numerical_offset = 1 < match.length(2);
        if (!has_numerical_offset)
        {
            return std::chrono::seconds{0};
        }
        const auto positive = match[3].str() == "+";
        static const auto to_int = [](const std::string& dd) { return 10 * (dd[0] - '0') + dd[1] - '0'; };
        const auto has_minutes = match.length(5) == 2;
        auto result = std::chrono::hours{to_int(match[4])} +
                      std::chrono::minutes{has_minutes ? to_int(match[5]) : 0};
        return std::chrono::duration_cast<std::chrono::seconds>(positive ? result : -result);
    }();
    const auto time = [](const std::string& t)
    {
        try
        {
            return boost::posix_time::from_iso_extended_string(t);
        }
        catch (...)
        {
            throw BadLexicalCast{};
        }
    }(match[1].str());
    const auto time_since_epoch = time - boost::posix_time::from_time_t(0);
    using FractionalSeconds = typename BoostTimeResolutionTraits<typename boost::posix_time::time_duration::traits_type>::StdChronoResolution;
    return
            std::chrono::system_clock::time_point{std::chrono::seconds{time_since_epoch.total_seconds()} - offset} +
            std::chrono::duration_cast<std::chrono::system_clock::duration>(
                    FractionalSeconds{time_since_epoch.fractional_seconds()});
}

std::string append_to_url(std::string url, const std::string& data)
{
    if (data.empty())
    {
        return url;
    }
    if (url.empty() || (url.back() != '/'))
    {
        url.reserve(url.length() + 1 + data.length());
        url.append("/");
    }
    url.append(data);
    return url;
}

std::string append_slash_to_url(std::string url)
{
    if (url.empty() || (url.back() != '/'))
    {
        url.append("/");
    }
    return url;
}

std::string to_string(Fred::Auction::Warehouse::AuctionStatus status)
{
    switch (status)
    {
        case Fred::Auction::Warehouse::AuctionStatus::expired: return "expired";
        case Fred::Auction::Warehouse::AuctionStatus::successful: return "successful";
    }
    struct UnknownStatus : std::exception
    {
        const char* what() const noexcept override { return "unknown auction status"; }
    };
    throw UnknownStatus{};
}

std::string chrono_to_iso_string(const std::chrono::system_clock::time_point& t)
{
    const auto c_time = std::chrono::system_clock::to_time_t(t);
    std::ostringstream out;
    out << std::put_time(std::gmtime(&c_time), "%Y-%m-%dT%H:%M:%S");
    const auto dt = t.time_since_epoch() - std::chrono::seconds{c_time};
    out << "." << std::setw(6) << std::setfill('0') << std::right
        << std::chrono::duration_cast<std::chrono::microseconds>(dt).count() << 'Z';
    return out.str();
}

}//namespace {anonymous}

using namespace Fred::Auction::Warehouse;

Unauthorized::Unauthorized(std::string data)
    : data{std::move(data)}
{ }

ValidationError::ValidationError(std::string data)
    : data{std::move(data)}
{ }

/*
POST <endpoint>/ HTTP/1.1
Host: auction.nic.cz
Content-Type: application/json
Accept: application/json
Accept-Charset: utf-8

{
  "item":
  {
    "description": null | <string>,
    "name": <string> # domain fqdn
  }
}

# response
{
  "auction_id": <string>,
  "next_event_after": <iso-timestamp> # e.g. "2023-09-18T08:00:00Z"
}
*/
CreateAuctionReply Fred::Auction::Warehouse::create_auction(
        LibHttp::Socket& socket,
        const LibHttp::Header& header,
        const CreateAuctionRequest& request)
{
    std::stringstream data;
    data << [&request]()
    {
        using json = nlohmann::json;
        json query;
        query["item"] = [&request]()
        {
            json item;
            if (request.item.description.empty())
            {
                item["description"] = nullptr;
            }
            else
            {
                item["description"] = request.item.description;
            }
            item["name"] = request.item.name;
            item["auction_id"] = to_string(*request.item.auction_uuid);
            return item;
        }();
        return query;
    }();
    LIBLOG_DEBUG("create_auction [{}] POST", request.item.name);
    const auto http_response = LibHttp::post(
            socket,
            Cfg::Options::get().auction.host,
            append_slash_to_url(Cfg::Options::get().auction.endpoint),
            data,
            header);
    LIBLOG_DEBUG("create_auction [{}]: {} - {}", request.item.name, http_response.status, http_response.body);
    switch (http_response.status)
    {
        case 200:
        case 202:
            break;
        case 401:
            {
                struct BadCredentials : Unauthorized
                {
                    BadCredentials(std::string data)
                        : Unauthorized{std::move(data)}
                    { }
                    const char* what() const noexcept override { return "bad credentials in create_auction request"; }
                };
                throw BadCredentials{http_response.body};
            }
        case 422:
            {
                struct InvalidData : ValidationError
                {
                    InvalidData(std::string data)
                        : ValidationError{std::move(data)}
                    { }
                    const char* what() const noexcept override { return "invalid data in create_auction request"; }
                };
                throw InvalidData{http_response.body};
            }
        default:
            LIBLOG_INFO("reply: {} - {}", http_response.status, http_response.body);
            throw http_response;
    }
    return [&text = http_response.body]()
    {
        using json = nlohmann::json;
        const auto j_body = json::parse(text);
        return CreateAuctionReply
        {
            ExternalAuctionId{j_body["auction_id"]},
            chrono_from_iso_string(j_body["next_event_after"])
        };
    }();
}

/*
GET <endpoint>/<auction_id>/ HTTP/1.1
Host: auction.nic.cz
Content-Type: application/json
Accept: application/json
Accept-Charset: utf-8

# response if auction is still in progress
{
  "next_event_after": null | <iso-timestamp>
  "winner": {
    "id": <uuid>,
    "expires_at": <iso-timestamp>
  } | null
}

# response if auction finished without a winner
HTTP status code: 404
*/
GetAuctionInfoReply Fred::Auction::Warehouse::get_auction_info(
        LibHttp::Socket& socket,
        const LibHttp::Header& header,
        const GetAuctionInfoRequest& request)
{
    LIBLOG_DEBUG("get_auction_info [{}] GET", *request.auction_id);
    const auto http_response = LibHttp::get(
            socket,
            Cfg::Options::get().auction.host,
            append_slash_to_url(append_to_url(Cfg::Options::get().auction.endpoint, *request.auction_id)),
            header);
    LIBLOG_DEBUG("get_auction_info [{}]: {} - {}", *request.auction_id, http_response.status, http_response.body);
    switch (http_response.status)
    {
        case 200:
            break;
        case 401:
            {
                struct BadCredentials : Unauthorized
                {
                    BadCredentials(std::string data)
                        : Unauthorized{std::move(data)}
                    { }
                    const char* what() const noexcept override { return "bad credentials in get_auction_info request"; }
                };
                throw BadCredentials{http_response.body};
            }
        case 404:
            {
                struct NotFound : GetAuctionInfoReply::DoesNotExist
                {
                    const char* what() const noexcept override { return "auction does not exist"; }
                };
                throw NotFound{};
            }
        case 422:
            {
                struct InvalidData : ValidationError
                {
                    InvalidData(std::string data)
                        : ValidationError{std::move(data)}
                    { }
                    const char* what() const noexcept override { return "invalid data in get_auction_info request"; }
                };
                throw InvalidData{http_response.body};
            }
        default:
            LIBLOG_INFO("reply: {} - {}", http_response.status, http_response.body);
            throw http_response;
    }
    return [&text = http_response.body]()
    {
        using json = nlohmann::json;
        const auto j_body = json::parse(text);
        return GetAuctionInfoReply{
            [&next_event_after = j_body["next_event_after"]]() -> boost::optional<std::chrono::system_clock::time_point>
            {
                if (next_event_after.is_null())
                {
                    return boost::none;
                }
                return chrono_from_iso_string(next_event_after);
            }(),
            [&winner = j_body["winner"]]() -> boost::optional<Win>
            {
                if (winner.is_null())
                {
                    return boost::none;
                }
                return Win
                {
                    WinnerId{[&winner]()
                    {
                        std::string str;
                        winner["id"].get_to(str);
                        return boost::uuids::string_generator{}(str);
                    }()},
                    chrono_from_iso_string(winner["expires_at"])
                };
            }()
        };
    }();
}

/*
PUT <endpoint>/<auction_id>/ HTTP/1.1
Host: auction.nic.cz
Content-Type: application/json
Accept: application/json
Accept-Charset: utf-8

{
  "status": "[expired|successful]"
}

# response
{
  "next_event_after": null | <iso-timestamp> # if the auction continues with another round
}
*/
ItemTakingInfoReply Fred::Auction::Warehouse::item_taking_info(
        LibHttp::Socket& socket,
        const LibHttp::Header& header,
        const ItemTakingInfoRequest& request)
{
    std::stringstream data;
    const auto status = to_string(request.status);
    const auto collect_datetime = request.collect_datetime == boost::none ? std::string{}
                                                                          : chrono_to_iso_string(*request.collect_datetime);
    data << [&status, &collect_datetime]()
    {
        using json = nlohmann::json;
        json query;
        query["status"] = status;
        if (!collect_datetime.empty())
        {
            query["collect_datetime"] = collect_datetime;
        }
        return query;
    }();
    if (request.status == AuctionStatus::successful)
    {
        LIBLOG_DEBUG("item_taking_info [{} - {} at {}] PUT", *request.auction_id, status, collect_datetime);
    }
    else
    {
        LIBLOG_DEBUG("item_taking_info [{} - {}] PUT", *request.auction_id, status);
    }
    const auto http_response = LibHttp::put(
            socket,
            Cfg::Options::get().auction.host,
            append_slash_to_url(append_to_url(Cfg::Options::get().auction.endpoint, *request.auction_id)),
            data,
            header);
    LIBLOG_DEBUG("item_taking_info [{} - {}]: {} - {}", *request.auction_id, status, http_response.status, http_response.body);
    switch (http_response.status)
    {
        case 200:
            break;
        case 401:
            {
                struct BadCredentials : Unauthorized
                {
                    BadCredentials(std::string data)
                        : Unauthorized{std::move(data)}
                    { }
                    const char* what() const noexcept override { return "bad credentials in item_taking_info request"; }
                };
                throw BadCredentials{http_response.body};
            }
        case 404:
            {
                LIBLOG_INFO("reply: {} - {}", http_response.status, http_response.body);
                struct NotFound : ItemTakingInfoReply::DoesNotExist
                {
                    const char* what() const noexcept override { return "auction does not exist"; }
                };
                throw NotFound{};
            }
        case 409:
            {
                LIBLOG_INFO("reply: {} - {}", http_response.status, http_response.body);
                struct BadState : ItemTakingInfoReply::UnmetPreconditions
                {
                    const char* what() const noexcept override { return "auction not in expected state"; }
                };
                throw BadState{};
            }
        case 422:
            {
                struct InvalidData : ValidationError
                {
                    InvalidData(std::string data)
                        : ValidationError{std::move(data)}
                    { }
                    const char* what() const noexcept override { return "invalid data in item_taking_info request"; }
                };
                throw InvalidData{http_response.body};
            }
        default:
            LIBLOG_INFO("reply: {} - {}", http_response.status, http_response.body);
            throw http_response;
    }
    return [&text = http_response.body]()
    {
        using json = nlohmann::json;
        const auto j_body = json::parse(text);
        return ItemTakingInfoReply{
            [&next_event_after = j_body["next_event_after"]]() -> boost::optional<std::chrono::system_clock::time_point>
            {
                if (next_event_after.is_null())
                {
                    return boost::none;
                }
                return chrono_from_iso_string(next_event_after);
            }()
        };
    }();
}
