/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROCESS_HH_8C2BDE1F4E67D14F5ACACA7D9F17BF68//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PROCESS_HH_8C2BDE1F4E67D14F5ACACA7D9F17BF68

#include <chrono>

namespace Fred {
namespace Auction {
namespace Warehouse {

int process_once(bool on_schedule);

int process_repeatedly(bool on_schedule, std::chrono::seconds period);

}//namespace Fred::Auction::Warehouse
}//namespace Fred::Auction
}//namespace Fred

#endif//PROCESS_HH_8C2BDE1F4E67D14F5ACACA7D9F17BF68
