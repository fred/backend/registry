/*
 * Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUCTION_API_HH_098B2C709FEE031756B93B3268C303E5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define AUCTION_API_HH_098B2C709FEE031756B93B3268C303E5

#include "src/http/client.hh"

#include "libstrong/type.hh"

#include <boost/optional.hpp>
#include <boost/uuid/uuid.hpp>

#include <chrono>
#include <exception>
#include <string>

namespace Fred {
namespace Auction {
namespace Warehouse {

struct Unauthorized : std::exception
{
    explicit Unauthorized(std::string);
    std::string data;
};

struct ValidationError : std::exception
{
    explicit ValidationError(std::string);
    std::string data;
};

using AuctionUuid = LibStrong::Type<boost::uuids::uuid, struct AuctionUuidTag_>;

struct AuctionItem
{
    std::string description;
    std::string name;
    AuctionUuid auction_uuid;
};

struct CreateAuctionRequest
{
    AuctionItem item;
};

using ExternalAuctionId = LibStrong::Type<std::string, struct ExternalAuctionIdTag_>;

struct CreateAuctionReply
{
    ExternalAuctionId auction_id;
    std::chrono::system_clock::time_point next_event_after;
};

CreateAuctionReply create_auction(
        LibHttp::Socket& socket,
        const LibHttp::Header& header,
        const CreateAuctionRequest& request);

struct GetAuctionInfoRequest
{
    ExternalAuctionId auction_id;
};

using WinnerId = LibStrong::Type<boost::uuids::uuid, struct WinnerIdTag_>;

struct Win
{
    WinnerId winner_id;
    std::chrono::system_clock::time_point expires_at;
};

struct GetAuctionInfoReply
{
    boost::optional<std::chrono::system_clock::time_point> next_event_after;
    boost::optional<Win> win;
    struct DoesNotExist : std::exception { };
};

GetAuctionInfoReply get_auction_info(
        LibHttp::Socket& socket,
        const LibHttp::Header& header,
        const GetAuctionInfoRequest& request);

enum class AuctionStatus
{
    expired,
    successful
};

struct ItemTakingInfoRequest
{
    ExternalAuctionId auction_id;
    AuctionStatus status;
    boost::optional<std::chrono::system_clock::time_point> collect_datetime;
};

struct ItemTakingInfoReply
{
    boost::optional<std::chrono::system_clock::time_point> next_event_after;
    struct DoesNotExist : std::exception { };
    struct UnmetPreconditions : std::exception { };
};

ItemTakingInfoReply item_taking_info(
        LibHttp::Socket& socket,
        const LibHttp::Header& header,
        const ItemTakingInfoRequest& request);

}//namespace Fred::Auction::Warehouse
}//namespace Fred::Auction
}//namespace Fred

#endif//AUCTION_API_HH_098B2C709FEE031756B93B3268C303E5