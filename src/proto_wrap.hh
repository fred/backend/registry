/*
 * Copyright (C) 2019-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_WRAP_HH_DFD8AE33A7CF17F901177987E210122D//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PROTO_WRAP_HH_DFD8AE33A7CF17F901177987E210122D

#include "src/common_types.hh"

#include "src/contact/contact_data_history.hh"
#include "src/contact/contact_handle_history.hh"
#include "src/contact/contact_info.hh"
#include "src/contact/contact_state.hh"
#include "src/contact/contact_state_flags.hh"
#include "src/contact/contact_state_history.hh"
#include "src/contact/create_contact.hh"
#include "src/contact/list_contacts_reply.hh"
#include "src/contact/list_merge_candidates_reply.hh"
#include "src/contact/merge_contacts_reply.hh"
#include "src/contact/search_contact.hh"
#include "src/contact/update_contact.hh"
#include "src/contact/update_contact_state.hh"

#include "src/contact_representative/contact_representative_info.hh"
#include "src/contact_representative/create_contact_representative.hh"
#include "src/contact_representative/delete_contact_representative.hh"
#include "src/contact_representative/update_contact_representative.hh"

#include "src/domain/batch_delete_domains_reply.hh"
#include "src/domain/domain_data_history.hh"
#include "src/domain/domain_info_reply.hh"
#include "src/domain/domain_life_cycle_stage_reply.hh"
#include "src/domain/domain_state.hh"
#include "src/domain/domain_state_flags.hh"
#include "src/domain/domain_state_history.hh"
#include "src/domain/fqdn_history.hh"
#include "src/domain/get_domains_by_contact_reply.hh"
#include "src/domain/get_domains_notify_info_reply.hh"
#include "src/domain/list_domains_reply.hh"
#include "src/domain/list_domains_by_contact_reply.hh"
#include "src/domain/list_domains_by_keyset_reply.hh"
#include "src/domain/list_domains_by_nsset_reply.hh"
#include "src/domain/manage_domain_state_flags_reply.hh"
#include "src/domain/search_domain.hh"
#include "src/domain/update_domains_additional_notify_info_reply.hh"

#include "src/domain_blacklist/block_info.hh"
#include "src/domain_blacklist/create_block.hh"
#include "src/domain_blacklist/delete_block.hh"
#include "src/domain_blacklist/list_blocks.hh"

#include "src/keyset/keyset_data_history.hh"
#include "src/keyset/keyset_info_reply.hh"
#include "src/keyset/keyset_handle_history.hh"
#include "src/keyset/keyset_state.hh"
#include "src/keyset/keyset_state_flags.hh"
#include "src/keyset/keyset_state_history.hh"
#include "src/keyset/keyset_info.hh"
#include "src/keyset/list_keysets_by_contact_reply.hh"
#include "src/keyset/search_keyset.hh"

#include "src/nsset/check_dns_host_reply.hh"
#include "src/nsset/list_nssets_reply.hh"
#include "src/nsset/list_nssets_by_contact_reply.hh"
#include "src/nsset/nsset_data_history.hh"
#include "src/nsset/nsset_info_reply.hh"
#include "src/nsset/nsset_handle_history.hh"
#include "src/nsset/nsset_state.hh"
#include "src/nsset/nsset_state_flags.hh"
#include "src/nsset/nsset_state_history.hh"
#include "src/nsset/nsset_info.hh"
#include "src/nsset/search_nsset.hh"

#include "src/registrar/add_registrar_certification.hh"
#include "src/registrar/add_registrar_epp_credentials.hh"
#include "src/registrar/add_registrar_group_membership.hh"
#include "src/registrar/add_registrar_zone_access.hh"
#include "src/registrar/create_registrar.hh"
#include "src/registrar/delete_registrar_certification.hh"
#include "src/registrar/delete_registrar_epp_credentials.hh"
#include "src/registrar/delete_registrar_group_membership.hh"
#include "src/registrar/delete_registrar_zone_access.hh"
#include "src/registrar/registrar_certifications.hh"
#include "src/registrar/registrar_credit.hh"
#include "src/registrar/registrar_epp_credentials.hh"
#include "src/registrar/registrar_groups.hh"
#include "src/registrar/registrar_groups_membership.hh"
#include "src/registrar/registrar_info.hh"
#include "src/registrar/registrar_list.hh"
#include "src/registrar/registrar_zone_access.hh"
#include "src/registrar/update_registrar_certification.hh"
#include "src/registrar/update_registrar_epp_credentials.hh"
#include "src/registrar/update_registrar.hh"
#include "src/registrar/update_registrar_zone_access.hh"

#include "src/registry_type_traits.hh"

#include "fred_api/registry/common_types.pb.h"

#include "fred_api/registry/contact/contact_common_types.pb.h"
#include "fred_api/registry/contact/contact_handle_history_types.pb.h"
#include "fred_api/registry/contact/contact_history_types.pb.h"
#include "fred_api/registry/contact/contact_info_types.pb.h"
#include "fred_api/registry/contact/contact_state_flags_types.pb.h"
#include "fred_api/registry/contact/contact_state_history_types.pb.h"
#include "fred_api/registry/contact/contact_state_types.pb.h"
#include "fred_api/registry/contact/update_contact_types.pb.h"
#include "fred_api/registry/contact/update_contact_state_types.pb.h"
#include "fred_api/registry/contact/service_contact_grpc.grpc.pb.h"
#include "fred_api/registry/contact/service_contact_grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.pb.h"
#include "fred_api/registry/contact/service_search_contact_grpc.grpc.pb.h"

#include "fred_api/registry/contact_representative/contact_representative_common_types.pb.h"
#include "fred_api/registry/contact_representative/service_contact_representative_grpc.grpc.pb.h"

#include "fred_api/registry/domain/domain_common_types.pb.h"
#include "fred_api/registry/domain/domain_history_types.pb.h"
#include "fred_api/registry/domain/domain_info_types.pb.h"
#include "fred_api/registry/domain/domain_state_flags_types.pb.h"
#include "fred_api/registry/domain/domain_state_history_types.pb.h"
#include "fred_api/registry/domain/domain_state_types.pb.h"
#include "fred_api/registry/domain/fqdn_history_types.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.pb.h"
#include "fred_api/registry/domain/service_admin_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.pb.h"
#include "fred_api/registry/domain/service_domain_grpc.grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.pb.h"
#include "fred_api/registry/domain/service_search_domain_grpc.grpc.pb.h"

#include "fred_api/registry/domain_blacklist/domain_blacklist_common_types.pb.h"
#include "fred_api/registry/domain_blacklist/service_domain_blacklist_grpc.grpc.pb.h"

#include "fred_api/registry/keyset/keyset_common_types.pb.h"
#include "fred_api/registry/keyset/keyset_handle_history_types.pb.h"
#include "fred_api/registry/keyset/keyset_history_types.pb.h"
#include "fred_api/registry/keyset/keyset_info_types.pb.h"
#include "fred_api/registry/keyset/keyset_state_flags_types.pb.h"
#include "fred_api/registry/keyset/keyset_state_history_types.pb.h"
#include "fred_api/registry/keyset/keyset_state_types.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.grpc.pb.h"
#include "fred_api/registry/keyset/service_keyset_grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.pb.h"
#include "fred_api/registry/keyset/service_search_keyset_grpc.grpc.pb.h"

#include "fred_api/registry/nsset/nsset_common_types.pb.h"
#include "fred_api/registry/nsset/nsset_handle_history_types.pb.h"
#include "fred_api/registry/nsset/nsset_history_types.pb.h"
#include "fred_api/registry/nsset/nsset_info_types.pb.h"
#include "fred_api/registry/nsset/nsset_state_flags_types.pb.h"
#include "fred_api/registry/nsset/nsset_state_history_types.pb.h"
#include "fred_api/registry/nsset/nsset_state_types.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.pb.h"
#include "fred_api/registry/nsset/service_nsset_grpc.grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.pb.h"
#include "fred_api/registry/nsset/service_search_nsset_grpc.grpc.pb.h"

#include "fred_api/registry/registrar/registrar_common_types.pb.h"
#include "fred_api/registry/registrar/service_admin_grpc.pb.h"
#include "fred_api/registry/registrar/service_admin_grpc.grpc.pb.h"
#include "fred_api/registry/registrar/service_registrar_grpc.pb.h"
#include "fred_api/registry/registrar/service_registrar_grpc.grpc.pb.h"

namespace Fred {
namespace Registry {

template <typename T>
void proto_wrap(const T& src, typename RegistryTypeTraits<T>::ProtoType* dst);

template <>
struct RegistryTypeTraits<LogEntryId>
{
    using ProtoType = Api::LogEntryId;
};

template <>
struct RegistryTypeTraits<Contact::ContactInfoReply::Data>
{
    using ProtoType = Api::Contact::ContactInfoReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactInfoReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::ContactInfoReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactIdReply::Data>
{
    using ProtoType = Api::Contact::ContactIdReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactIdReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::ContactIdReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactDataHistoryReply::Data>
{
    using ProtoType = Api::Contact::ContactHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactDataHistoryReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::ContactHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactDataHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Contact::ContactHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::CreateContactReply::Data>
{
    using ProtoType = Api::Contact::CreateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::CreateContactReply::Exception::InvalidData>
{
    using ProtoType = Api::Contact::CreateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::CreateContactReply::Exception::ContactAlreadyExists>
{
    using ProtoType = Api::Contact::CreateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::CreateContactReply::Exception::ContactHandleInProtectionPeriod>
{
    using ProtoType = Api::Contact::CreateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::CreateContactReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Contact::CreateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactHandleHistoryReply::Data>
{
    using ProtoType = Api::Contact::ContactHandleHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactStateFlagsInfo>
{
    using ProtoType = Api::Contact::ContactStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactStateReply::Data>
{
    using ProtoType = Api::Contact::ContactStateReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactStateReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::ContactStateReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactStateHistoryReply::Data>
{
    using ProtoType = Api::Contact::ContactStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactStateHistoryReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::ContactStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::ContactStateHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Contact::ContactStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::UpdateContactReply::Data>
{
    using ProtoType = Api::Contact::UpdateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::UpdateContactReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::UpdateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::UpdateContactReply::Exception::NotCurrentVersion>
{
    using ProtoType = Api::Contact::UpdateContactReply;
};

template <>
struct RegistryTypeTraits<Contact::UpdateContactStateReply::Data>
{
    using ProtoType = Api::Contact::UpdateContactStateReply;
};

std::size_t proto_wrap(
        const Contact::ListContactsReply::Data& src,
        std::size_t offset,
        Api::Contact::ListContactsReply::Data* dst);

std::size_t proto_wrap(
        const Contact::ListMergeCandidatesReply::Data& src,
        std::size_t offset,
        Api::Contact::ListMergeCandidatesReply::Data* dst);

template <>
struct RegistryTypeTraits<Contact::ListMergeCandidatesReply::Exception::InvalidData>
{
    using ProtoType = Api::Contact::ListMergeCandidatesReply;
};

template <>
struct RegistryTypeTraits<Contact::ListMergeCandidatesReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::ListMergeCandidatesReply;
};

template <>
struct RegistryTypeTraits<Contact::MergeContactsReply::Exception::InvalidData>
{
    using ProtoType = Api::Contact::MergeContactsReply;
};

template <>
struct RegistryTypeTraits<Contact::MergeContactsReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Contact::MergeContactsReply;
};

template <>
struct RegistryTypeTraits<Contact::SearchContactReply::Data>
{
    using ProtoType = Api::Contact::SearchContactReply;
};

template <>
struct RegistryTypeTraits<Contact::SearchContactHistoryReply::Data>
{
    using ProtoType = Api::Contact::SearchContactHistoryReply;
};

template <>
struct RegistryTypeTraits<Contact::SearchContactHistoryReply::Exception::ChronologyViolation>
{
    using ProtoType = Api::Contact::SearchContactHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetInfoReply::Data>
{
    using ProtoType = Api::Keyset::KeysetInfoReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetInfoReply::Result>
{
    using ProtoType = Api::Keyset::KeysetInfoReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetInfoReply::Exception::InvalidData>
{
    using ProtoType = Api::Keyset::KeysetInfoReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetInfoReply::Exception::KeysetDoesNotExist>
{
    using ProtoType = Api::Keyset::KeysetInfoReply;
};

std::size_t proto_wrap(
        const Keyset::BatchKeysetInfoReply::Data& src,
        std::size_t offset,
        Api::Keyset::BatchKeysetInfoReply* dst);

template <>
struct RegistryTypeTraits<Keyset::KeysetIdReply::Data>
{
    using ProtoType = Api::Keyset::KeysetIdReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetIdReply::Exception::KeysetDoesNotExist>
{
    using ProtoType = Api::Keyset::KeysetIdReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetDataHistoryReply::Data>
{
    using ProtoType = Api::Keyset::KeysetHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetDataHistoryReply::Exception::KeysetDoesNotExist>
{
    using ProtoType = Api::Keyset::KeysetHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetDataHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Keyset::KeysetHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetHandleHistoryReply::Data>
{
    using ProtoType = Api::Keyset::KeysetHandleHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetStateFlagsInfo>
{
    using ProtoType = Api::Keyset::KeysetStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetStateReply::Data>
{
    using ProtoType = Api::Keyset::KeysetStateReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetStateReply::Exception::KeysetDoesNotExist>
{
    using ProtoType = Api::Keyset::KeysetStateReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetStateHistoryReply::Data>
{
    using ProtoType = Api::Keyset::KeysetStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetStateHistoryReply::Exception::KeysetDoesNotExist>
{
    using ProtoType = Api::Keyset::KeysetStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::KeysetStateHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Keyset::KeysetStateHistoryReply;
};

std::size_t proto_wrap(
        const Keyset::ListKeysetsByContactReply::Data& src,
        std::size_t offset,
        Api::Keyset::ListKeysetsByContactReply::Data* dst);

template <>
struct RegistryTypeTraits<Keyset::ListKeysetsByContactReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Keyset::ListKeysetsByContactReply;
};

template <>
struct RegistryTypeTraits<Keyset::ListKeysetsByContactReply::Exception::InvalidData>
{
    using ProtoType = Api::Keyset::ListKeysetsByContactReply;
};

template <>
struct RegistryTypeTraits<Keyset::SearchKeysetReply::Data>
{
    using ProtoType = Api::Keyset::SearchKeysetReply;
};

template <>
struct RegistryTypeTraits<Keyset::SearchKeysetHistoryReply::Data>
{
    using ProtoType = Api::Keyset::SearchKeysetHistoryReply;
};

template <>
struct RegistryTypeTraits<Keyset::SearchKeysetHistoryReply::Exception::ChronologyViolation>
{
    using ProtoType = Api::Keyset::SearchKeysetHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetInfoReply::Data>
{
    using ProtoType = Api::Nsset::NssetInfoReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetInfoReply::Result>
{
    using ProtoType = Api::Nsset::NssetInfoReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetInfoReply::Exception::InvalidData>
{
    using ProtoType = Api::Nsset::NssetInfoReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetInfoReply::Exception::NssetDoesNotExist>
{
    using ProtoType = Api::Nsset::NssetInfoReply;
};

std::size_t proto_wrap(
        const Nsset::BatchNssetInfoReply::Data& src,
        std::size_t offset,
        Api::Nsset::BatchNssetInfoReply* dst);

template <>
struct RegistryTypeTraits<Nsset::NssetIdReply::Data>
{
    using ProtoType = Api::Nsset::NssetIdReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetIdReply::Exception::NssetDoesNotExist>
{
    using ProtoType = Api::Nsset::NssetIdReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetDataHistoryReply::Data>
{
    using ProtoType = Api::Nsset::NssetHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetDataHistoryReply::Exception::NssetDoesNotExist>
{
    using ProtoType = Api::Nsset::NssetHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetDataHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Nsset::NssetHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetHandleHistoryReply::Data>
{
    using ProtoType = Api::Nsset::NssetHandleHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetStateFlagsInfo>
{
    using ProtoType = Api::Nsset::NssetStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetStateReply::Data>
{
    using ProtoType = Api::Nsset::NssetStateReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetStateReply::Exception::NssetDoesNotExist>
{
    using ProtoType = Api::Nsset::NssetStateReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetStateHistoryReply::Data>
{
    using ProtoType = Api::Nsset::NssetStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetStateHistoryReply::Exception::NssetDoesNotExist>
{
    using ProtoType = Api::Nsset::NssetStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::NssetStateHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Nsset::NssetStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::CheckDnsHostReply::Data>
{
    using ProtoType = Api::Nsset::CheckDnsHostReply;
};

std::size_t proto_wrap(
        const Nsset::ListNssetsReply::Data& src,
        std::size_t offset,
        Api::Nsset::ListNssetsReply::Data* dst);

std::size_t proto_wrap(
        const Nsset::ListNssetsByContactReply::Data& src,
        std::size_t offset,
        Api::Nsset::ListNssetsByContactReply::Data* dst);

template <>
struct RegistryTypeTraits<Nsset::ListNssetsByContactReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Nsset::ListNssetsByContactReply;
};

template <>
struct RegistryTypeTraits<Nsset::ListNssetsByContactReply::Exception::InvalidData>
{
    using ProtoType = Api::Nsset::ListNssetsByContactReply;
};

template <>
struct RegistryTypeTraits<Nsset::SearchNssetReply::Data>
{
    using ProtoType = Api::Nsset::SearchNssetReply;
};

template <>
struct RegistryTypeTraits<Nsset::SearchNssetHistoryReply::Data>
{
    using ProtoType = Api::Nsset::SearchNssetHistoryReply;
};

template <>
struct RegistryTypeTraits<Nsset::SearchNssetHistoryReply::Exception::ChronologyViolation>
{
    using ProtoType = Api::Nsset::SearchNssetHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainInfoReply::Data>
{
    using ProtoType = Api::Domain::DomainInfoReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainInfoReply::Exception::InvalidData>
{
    using ProtoType = Api::Domain::DomainInfoReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainInfoReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::DomainInfoReply;
};

std::size_t proto_wrap(
        const Domain::BatchDomainInfoReply::Data& src,
        std::size_t offset,
        Api::Domain::BatchDomainInfoReply* dst);

template <>
struct RegistryTypeTraits<Domain::DomainIdReply::Data>
{
    using ProtoType = Api::Domain::DomainIdReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainIdReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::DomainIdReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainDataHistoryReply::Data>
{
    using ProtoType = Api::Domain::DomainHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainDataHistoryReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::DomainHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainDataHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Domain::DomainHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::FqdnHistoryReply::Data>
{
    using ProtoType = Api::Domain::FqdnHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainStateFlagsInfo>
{
    using ProtoType = Api::Domain::DomainStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainStateReply::Data>
{
    using ProtoType = Api::Domain::DomainStateReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainStateReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::DomainStateReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainStateHistoryReply::Data>
{
    using ProtoType = Api::Domain::DomainStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainStateHistoryReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::DomainStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainStateHistoryReply::Exception::InvalidHistoryInterval>
{
    using ProtoType = Api::Domain::DomainStateHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::SearchDomainReply::Data>
{
    using ProtoType = Api::Domain::SearchDomainReply;
};

template <>
struct RegistryTypeTraits<Domain::SearchDomainHistoryReply::Data>
{
    using ProtoType = Api::Domain::SearchDomainHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::SearchDomainHistoryReply::Exception::ChronologyViolation>
{
    using ProtoType = Api::Domain::SearchDomainHistoryReply;
};

template <>
struct RegistryTypeTraits<Domain::BatchDeleteDomainsReply::NotDeleted::Reason>
{
    using ProtoType = Api::Domain::BatchDeleteDomainsReply::NotDeleted;
};

template <>
struct RegistryTypeTraits<Domain::BatchDeleteDomainsReply::NotDeleted>
{
    using ProtoType = Api::Domain::BatchDeleteDomainsReply::NotDeleted;
};

std::size_t proto_wrap(
        const Domain::BatchDeleteDomainsReply::Data& src,
        std::size_t offset,
        Api::Domain::BatchDeleteDomainsReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::ManageDomainStateFlagsReply::Data::DomainStateFlagRequestInfo>
{
    using ProtoType = Api::Domain::ManageDomainStateFlagsReply::Data::DomainStateFlagRequestInfo;
};

std::size_t proto_wrap(
        const Domain::ManageDomainStateFlagsReply::Data& src,
        std::size_t offset,
        Api::Domain::ManageDomainStateFlagsReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::ManageDomainStateFlagsReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::ManageDomainStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Domain::ManageDomainStateFlagsReply::Exception::DomainStateFlagDoesNotExist>
{
    using ProtoType = Api::Domain::ManageDomainStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Domain::ManageDomainStateFlagsReply::Exception::DomainStateFlagRequestDoesNotExist>
{
    using ProtoType = Api::Domain::ManageDomainStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Domain::ManageDomainStateFlagsReply::Exception::DomainHistoryIdMismatch>
{
    using ProtoType = Api::Domain::ManageDomainStateFlagsReply;
};

template <>
struct RegistryTypeTraits<Domain::GetDomainsByContactReply::Data>
{
    using ProtoType = Api::Domain::DomainsByContactReply;
};

std::size_t proto_wrap(
        const Domain::GetDomainsByContactReply::Data& src,
        std::size_t offset,
        Api::Domain::DomainsByContactReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::GetDomainsByContactReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Domain::DomainsByContactReply;
};

template <>
struct RegistryTypeTraits<Domain::GetDomainsByContactReply::Exception::InvalidOrderBy>
{
    using ProtoType = Api::Domain::DomainsByContactReply;
};

template <>
struct RegistryTypeTraits<Domain::DomainLifeCycleStageReply::Data>
{
    using ProtoType = Api::Domain::DomainLifeCycleStageReply;
};

template <>
struct RegistryTypeTraits<Domain::GetDomainsNotifyInfoReply::Data>
{
    using ProtoType = Api::Domain::GetDomainsNotifyInfoReply;
};

std::size_t proto_wrap(
        const Domain::GetDomainsNotifyInfoReply::Data& src,
        std::size_t offset,
        Api::Domain::GetDomainsNotifyInfoReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::DomainDoesNotExist>
{
    using ProtoType = Api::Domain::UpdateDomainsAdditionalNotifyInfoReply;
};

template <>
struct RegistryTypeTraits<Domain::UpdateDomainsAdditionalNotifyInfoReply::Exception::InvalidDomainContactInfo>
{
    using ProtoType = Api::Domain::UpdateDomainsAdditionalNotifyInfoReply;
};

std::size_t proto_wrap(
        const Domain::ListDomainsReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::ListDomainsReply::Exception::ZoneDoesNotExist>
{
    using ProtoType = Api::Domain::ListDomainsReply;
};

std::size_t proto_wrap(
        const Domain::ListDomainsByNssetReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsByNssetReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::ListDomainsByNssetReply::Exception::InvalidData>
{
    using ProtoType = Api::Domain::ListDomainsByNssetReply;
};

template <>
struct RegistryTypeTraits<Domain::ListDomainsByNssetReply::Exception::NssetDoesNotExist>
{
    using ProtoType = Api::Domain::ListDomainsByNssetReply;
};

std::size_t proto_wrap(
        const Domain::ListDomainsByKeysetReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsByKeysetReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::ListDomainsByKeysetReply::Exception::InvalidData>
{
    using ProtoType = Api::Domain::ListDomainsByKeysetReply;
};

template <>
struct RegistryTypeTraits<Domain::ListDomainsByKeysetReply::Exception::KeysetDoesNotExist>
{
    using ProtoType = Api::Domain::ListDomainsByKeysetReply;
};

std::size_t proto_wrap(
        const Domain::ListDomainsByContactReply::Data& src,
        std::size_t offset,
        Api::Domain::ListDomainsByContactReply::Data* dst);

template <>
struct RegistryTypeTraits<Domain::ListDomainsByContactReply::Exception::InvalidData>
{
    using ProtoType = Api::Domain::ListDomainsByContactReply;
};

template <>
struct RegistryTypeTraits<Domain::ListDomainsByContactReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::Domain::ListDomainsByContactReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarCreditReply::Data>
{
    using ProtoType = Api::Registrar::RegistrarCreditReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarCreditReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::RegistrarCreditReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarInfoReply::Data>
{
    using ProtoType = Api::Registrar::RegistrarInfoReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarInfoReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::RegistrarInfoReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarListReply::Data>
{
    using ProtoType = Api::Registrar::RegistrarListReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarZoneAccess>
{
    using ProtoType = Api::Registrar::RegistrarZoneAccess;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarZoneAccessReply::Data>
{
    using ProtoType = Api::Registrar::RegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::RegistrarZoneAccessReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::RegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::CreateRegistrarReply::Data>
{
    using ProtoType = Api::Registrar::CreateRegistrarReply;
};

template <>
struct RegistryTypeTraits<Registrar::CreateRegistrarReply::Exception::RegistrarAlreadyExists>
{
    using ProtoType = Api::Registrar::CreateRegistrarReply;
};

template <>
struct RegistryTypeTraits<Registrar::CreateRegistrarReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::CreateRegistrarReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarReply::Data>
{
    using ProtoType = Api::Registrar::UpdateRegistrarReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::UpdateRegistrarReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::UpdateRegistrarReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarZoneAccessReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::AddRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarZoneAccessReply::Exception::ZoneDoesNotExist>
{
    using ProtoType = Api::Registrar::AddRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarZoneAccessReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::AddRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist>
{
    using ProtoType = Api::Registrar::DeleteRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarZoneAccessReply::Exception::HistoryChangeProhibited>
{
    using ProtoType = Api::Registrar::DeleteRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarZoneAccessReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::DeleteRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarZoneAccessReply::Exception::RegistrarZoneAccessDoesNotExist>
{
    using ProtoType = Api::Registrar::UpdateRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarZoneAccessReply::Exception::HistoryChangeProhibited>
{
    using ProtoType = Api::Registrar::UpdateRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarZoneAccessReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::UpdateRegistrarZoneAccessReply;
};

template <>
struct RegistryTypeTraits<Registrar::GetRegistrarEppCredentialsReply::Data>
{
    using ProtoType = Api::Registrar::GetRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::GetRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::GetRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarEppCredentialsReply::Data>
{
    using ProtoType = Api::Registrar::AddRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarEppCredentialsReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::AddRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarEppCredentialsReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::AddRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist>
{
    using ProtoType = Api::Registrar::DeleteRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarEppCredentialsReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::DeleteRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarEppCredentialsReply::Exception::RegistrarEppCredentialsDoesNotExist>
{
    using ProtoType = Api::Registrar::UpdateRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarEppCredentialsReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::UpdateRegistrarEppCredentialsReply;
};

template <>
struct RegistryTypeTraits<Registrar::GetRegistrarGroupsReply::Data>
{
    using ProtoType = Api::Registrar::GetRegistrarGroupsReply;
};

void proto_wrap(
        const Registrar::GetRegistrarGroupsMembershipReply::Data& src,
        Fred::Registry::Api::Registrar::GetRegistrarGroupsMembershipReply* dst);

template <>
struct RegistryTypeTraits<Registrar::GetRegistrarGroupsMembershipReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::GetRegistrarGroupsMembershipReply;
};

void proto_wrap(
        const Registrar::AddRegistrarGroupMembershipReply::Data& src,
        Fred::Registry::Api::Registrar::AddRegistrarGroupMembershipReply* dst);

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::AddRegistrarGroupMembershipReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarGroupMembershipReply::Exception::GroupDoesNotExist>
{
    using ProtoType = Api::Registrar::AddRegistrarGroupMembershipReply;
};

void proto_wrap(
        const Registrar::DeleteRegistrarGroupMembershipReply::Data& src,
        Fred::Registry::Api::Registrar::DeleteRegistrarGroupMembershipReply* dst);

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarGroupMembershipReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::DeleteRegistrarGroupMembershipReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarGroupMembershipReply::Exception::GroupDoesNotExist>
{
    using ProtoType = Api::Registrar::DeleteRegistrarGroupMembershipReply;
};

void proto_wrap(
        const Registrar::GetRegistrarGroupsMembershipReply::Data& src,
        Fred::Registry::Api::Registrar::GetRegistrarGroupsMembershipReply* dst);

template <>
struct RegistryTypeTraits<Registrar::GetRegistrarCertificationsReply::Data>
{
    using ProtoType = Api::Registrar::GetRegistrarCertificationsReply;
};

template <>
struct RegistryTypeTraits<Registrar::GetRegistrarCertificationsReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::GetRegistrarCertificationsReply;
};

void proto_wrap(const Registrar::RegistrarCertification&, Api::Registrar::AddRegistrarCertificationReply*);

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarCertificationReply::Exception::RegistrarDoesNotExist>
{
    using ProtoType = Api::Registrar::AddRegistrarCertificationReply;
};

template <>
struct RegistryTypeTraits<Registrar::AddRegistrarCertificationReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::AddRegistrarCertificationReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist>
{
    using ProtoType = Api::Registrar::DeleteRegistrarCertificationReply;
};

template <>
struct RegistryTypeTraits<Registrar::DeleteRegistrarCertificationReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::DeleteRegistrarCertificationReply;
};

void proto_wrap(const Registrar::RegistrarCertification&, Api::Registrar::UpdateRegistrarCertificationReply*);

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarCertificationReply::Exception::RegistrarCertificationDoesNotExist>
{
    using ProtoType = Api::Registrar::UpdateRegistrarCertificationReply;
};

template <>
struct RegistryTypeTraits<Registrar::UpdateRegistrarCertificationReply::Exception::InvalidData>
{
    using ProtoType = Api::Registrar::UpdateRegistrarCertificationReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::ContactRepresentativeInfoReply::Data>
{
    using ProtoType = Api::ContactRepresentative::ContactRepresentativeInfoReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::CreateContactRepresentativeReply::Data>
{
    using ProtoType = Api::ContactRepresentative::CreateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::UpdateContactRepresentativeReply::Data>
{
    using ProtoType = Api::ContactRepresentative::UpdateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::CreateContactRepresentativeReply::Exception::ContactRepresentativeAlreadyExists>
{
    using ProtoType = Api::ContactRepresentative::CreateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::ContactRepresentativeInfoReply::Exception::ContactRepresentativeDoesNotExist>
{
    using ProtoType = Api::ContactRepresentative::ContactRepresentativeInfoReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::CreateContactRepresentativeReply::Exception::ContactDoesNotExist>
{
    using ProtoType = Api::ContactRepresentative::CreateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::CreateContactRepresentativeReply::Exception::InvalidData>
{
    using ProtoType = Api::ContactRepresentative::CreateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::UpdateContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist>
{
    using ProtoType = Api::ContactRepresentative::UpdateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::UpdateContactRepresentativeReply::Exception::InvalidData>
{
    using ProtoType = Api::ContactRepresentative::UpdateContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<ContactRepresentative::DeleteContactRepresentativeReply::Exception::ContactRepresentativeDoesNotExist>
{
    using ProtoType = Api::ContactRepresentative::DeleteContactRepresentativeReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::BlockInfoReply::Data>
{
    using ProtoType = Api::DomainBlacklist::BlockInfoReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::CreateBlockReply::Data>
{
    using ProtoType = Api::DomainBlacklist::CreateBlockReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::DeleteBlockReply::Data>
{
    using ProtoType = Api::DomainBlacklist::DeleteBlockReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::ListBlocksReply::Data>
{
    using ProtoType = Api::DomainBlacklist::ListBlocksReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::BlockInfoReply::Exception::BlockDoesNotExist>
{
    using ProtoType = Api::DomainBlacklist::BlockInfoReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::BlockInfoReply::Exception::InvalidData>
{
    using ProtoType = Api::DomainBlacklist::BlockInfoReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::CreateBlockReply::Exception::BlockIdAlreadyExists>
{
    using ProtoType = Api::DomainBlacklist::CreateBlockReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::CreateBlockReply::Exception::InvalidData>
{
    using ProtoType = Api::DomainBlacklist::CreateBlockReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::DeleteBlockReply::Exception::BlockDoesNotExist>
{
    using ProtoType = Api::DomainBlacklist::DeleteBlockReply;
};

template <>
struct RegistryTypeTraits<DomainBlacklist::DeleteBlockReply::Exception::InvalidData>
{
    using ProtoType = Api::DomainBlacklist::DeleteBlockReply;
};

std::size_t proto_wrap(
        const DomainBlacklist::ListBlocksReply::Data& src,
        std::size_t offset,
        Api::DomainBlacklist::ListBlocksReply::Data* dst);

}//namespace Fred::Registry
}//namespace Fred

#endif//PROTO_WRAP_HH_DFD8AE33A7CF17F901177987E210122D
