/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PAGINATION_REQUEST_HH_40D8161E4F9BEEEAE53D4547797305B5//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define PAGINATION_REQUEST_HH_40D8161E4F9BEEEAE53D4547797305B5

#include "src/util/printable.hh"

#include <cstdint>
#include <string>


namespace Fred {
namespace Registry {

struct PaginationRequest : Util::Printable<PaginationRequest>
{
    std::int32_t page_size;
    std::string page_token;
    std::string to_string() const;
};

} // namespace Fred::Registry
} // namespace Fred

#endif//PAGINATION_REQUEST_HH_40D8161E4F9BEEEAE53D4547797305B5
