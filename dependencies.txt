# <repository> <commitish> <clone-path>
fred/api/registry 6.0.0 external/api/registry
fred/libfred 9.5.0 external/lib/libfred
fred/libpg 2.2.1 external/lib/libpg
fred/libdiagnostics 1.2.0 external/lib/libdiagnostics
https://github.com/djarek/certify.git 97f5eebfd99a5d6e99d07e4820240994e4e59787 3rd_party/lib/certify
